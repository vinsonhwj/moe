﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Westminster.MOE.DAL.Booking;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Booking;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Lib;
using hwj.DBUtility.MSSQL;
using hwj.DBUtility.Interface;
namespace Westminster.MOE.BLL.Booking
{
    public class CloneBooking : IDisposable
    {
        public Config Config { get; private set; }
        ICloneBooking da = null;
        private IConnection Conn { get; set; }

        public CloneBooking(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos], 30, hwj.DBUtility.Enums.LockType.None, config.IsDevelopments))
        { }

        public CloneBooking(Config config, IConnection conn)
            : this(config, new DACloneBooking(conn))
        {
            Conn = conn;
        }

        public CloneBooking(Config config, ICloneBooking dataAccess)
        {
            Config = config;
            this.da = dataAccess;
        }

        public bool DoWork(string companyCode, string sourceBkGref, string staffCode, string login, out string cloneBkGref, out string msg)
        {
            msg = string.Empty;
            cloneBkGref = string.Empty;
            try
            {
                Conn.BeginTransaction();
                CloneBookingInfo cbi = da.GetCloneBookingInfo(companyCode, sourceBkGref, staffCode, login);
                if (cbi.Master == null)
                {
                    msg = Properties.Resource_en_US.Invalid_Booking;
                    Conn.RollbackTransaction();
                    return false;
                }
                if (cbi.Login == null)
                {
                    msg = Properties.Resource_en_US.Invalid_User;
                    Conn.RollbackTransaction();
                    return false;
                }
                if (cbi.Staff == null)
                {
                    msg = Properties.Resource_en_US.Invalid_Staff;
                    Conn.RollbackTransaction();
                    return false;
                }
                cloneBkGref = da.GetNewBkGrefKey(cbi.Staff.BRANCHCODE, companyCode);

                cbi.NyRef = GetPeonyRef(
                                      cbi.Master,
                                      Properties.Resource_en_US.ActionCode,
                                      string.Format(Properties.Resource_en_US.Msg_CloneBooking, sourceBkGref, cloneBkGref),
                                      cbi.Login.STAFFCODE,
                                      cbi.Login.TEAMCODE
                                      );
                cbi.Master = ExchangeMaster(cbi.Master, cloneBkGref, staffCode, cbi.Staff.TEAMCODE, login);  //Copy Mstr
                cbi.PaxList = ExchangePaxs(cbi.PaxList, cloneBkGref, login);   //Copy Pax
                cbi.AirList = ExchangeAirs(cbi.AirList, cloneBkGref, login);  //Copy Air
                cbi.AirDetailList = ExchangeAirDetails(cbi.AirDetailList, cloneBkGref, login); // AirDetail
                cbi.HtlList = ExchangeHtls(cbi.HtlList, cloneBkGref, login); //Copy Htl
                cbi.HtlDetailList = ExchangeHtlDetails(cbi.HtlDetailList, cloneBkGref, login);   // HtlDetail
                cbi.OtherList = ExchangeOthers(cbi.OtherList, cloneBkGref, login);   //Copy Other
                cbi.OtherDetailList = ExchangeOtherDetails(cbi.OtherDetailList, cloneBkGref, login);  //  OtherDetail


                da.CopyBooking(cbi, companyCode, cloneBkGref, login);

                Conn.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                cloneBkGref = string.Empty;
                msg = ex.Message;
                Conn.RollbackTransaction();
                return false;
            }
        }

        #region Private Function

        private tbPEOMSTR ExchangeMaster(tbPEOMSTR sourceMaster, string cloneBkGref, string staffCode, string teamCode, string login)
        {
            sourceMaster.BKGREF = cloneBkGref;
            sourceMaster.STAFFCODE = staffCode;
            sourceMaster.TEAMCODE = teamCode;
            sourceMaster.MASTERPNR = cloneBkGref;
            sourceMaster.TTLSELLAMT = 0;
            sourceMaster.TTLSELLTAX = 0;
            sourceMaster.TTLCOSTAMT = 0;
            sourceMaster.TTLCOSTTAX = 0;
            sourceMaster.INVAMT = 0;
            sourceMaster.INVTAX = 0;
            sourceMaster.DOCAMT = 0;
            sourceMaster.DOCTAX = 0;
            sourceMaster.TTLSELLGST = 0;
            sourceMaster.TTLCOSTGST = 0;
            sourceMaster.TTLINVGST = 0;
            sourceMaster.TTLDOCGST = 0;
            sourceMaster.INVCOUNT = 0;
            sourceMaster.DOCCOUNT = 0;
            sourceMaster.ReptAmt = 0;
            sourceMaster.ReptTax = 0;
            sourceMaster.ReptCount = 0;
            sourceMaster.CREATEBY = login;
            sourceMaster.UPDATEBY = login;
            sourceMaster.CREATEON = DateTime.Now;
            sourceMaster.UPDATEON = sourceMaster.CREATEON;
            return sourceMaster;
        }
        private tbPEOPAXs ExchangePaxs(tbPEOPAXs sourcePaxs, string cloneBkGref, string login)
        {
            foreach (tbPEOPAX pax in sourcePaxs)
            {
                pax.BKGREF = cloneBkGref;
                pax.SourcePnr = cloneBkGref;
                pax.CREATEBY = login;
                pax.UPDATEBY = login;
                pax.CREATEON = DateTime.Now;
                pax.UPDATEON = pax.CREATEON;
            }
            return sourcePaxs;
        }
        private tbPEOAIRs ExchangeAirs(tbPEOAIRs sourceAirs, string cloneBkGref, string login)
        {
            foreach (tbPEOAIR air in sourceAirs)
            {
                air.BkgRef = cloneBkGref;
                air.SourcePnr = cloneBkGref;
                air.TOTALSELL = air.TOTALCOST;
                air.TOTALCOST = 0;
                air.TOTALSTAX = air.TOTALCTAX;
                air.TOTALCTAX = 0;
                air.CreateBy = login;
                air.UpdateBy = login;
                air.CreateOn = DateTime.Now;
                air.UpdateOn = air.CreateOn;
                air.IssuedDoc = "N";
            }
            return sourceAirs;
        }
        private tbPEOAIRDETAILs ExchangeAirDetails(tbPEOAIRDETAILs sourceAirDetails, string cloneBkGref, string login)
        {
            foreach (tbPEOAIRDETAIL costDetail in sourceAirDetails)
            {
                if (MOEFormats.ToTrim(costDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.COST.ToString())
                {
                    foreach (tbPEOAIRDETAIL sellDetail in sourceAirDetails)
                    {
                        if (MOEFormats.ToTrim(sellDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.SELL.ToString() && costDetail.SegNum == sellDetail.SegNum && int.Parse(sellDetail.SEQNUM) == int.Parse(costDetail.SEQNUM) - 1)
                        {
                            sellDetail.BkgRef = costDetail.BkgRef = cloneBkGref;
                            sellDetail.AMT = costDetail.AMT;
                            costDetail.AMT = 0;
                            sellDetail.AMTFEE = costDetail.AMTFEE;
                            costDetail.AMTFEE = 0;
                            sellDetail.TAXAMT = costDetail.TAXAMT;
                            costDetail.TAXAMT = 0;
                            sellDetail.DISCOUNT_MARKUP_PER = costDetail.DISCOUNT_MARKUP_PER;
                            costDetail.DISCOUNT_MARKUP_PER = 0;
                            sellDetail.INVNETTGROSS = costDetail.INVNETTGROSS;
                            costDetail.INVNETTGROSS = 0;
                            costDetail.CREATEON = DateTime.Now;
                            costDetail.UPDATEON = costDetail.CREATEON;
                            sellDetail.CREATEON = costDetail.CREATEON;
                            sellDetail.UPDATEON = costDetail.CREATEON;
                            costDetail.CREATEBY = login;
                            costDetail.UPDATEBY = costDetail.CREATEBY;
                            sellDetail.CREATEBY = costDetail.CREATEBY;
                            sellDetail.UPDATEBY = costDetail.CREATEBY;
                            break;
                        }
                    }
                }
            }
            return sourceAirDetails;
        }
        private tbPEOHTLs ExchangeHtls(tbPEOHTLs sourceHtls, string cloneBkGref, string login)
        {
            foreach (tbPEOHTL htl in sourceHtls)
            {
                htl.BKGREF = cloneBkGref;
                htl.SourcePnr = cloneBkGref;
                htl.TOTALSELL = htl.TOTALCOST;
                htl.TOTALCOST = 0;
                htl.TOTALSTAX = htl.TOTALCTAX;
                htl.TOTALCTAX = 0;
                htl.CREATEBY = login;
                htl.UPDATEBY = login;
                htl.CREATEON = DateTime.Now;
                htl.UPDATEON = htl.CREATEON;
                htl.IssuedDoc = "N";
            }
            return sourceHtls;
        }
        private tbPeoHtlDetails ExchangeHtlDetails(tbPeoHtlDetails sourceHtlDetails, string cloneBkGref, string login)
        {
            foreach (tbPeoHtlDetail costDetail in sourceHtlDetails)
            {
                if (MOEFormats.ToTrim(costDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.COST.ToString())
                {
                    foreach (tbPeoHtlDetail sellDetail in sourceHtlDetails)
                    {
                        if (MOEFormats.ToTrim(sellDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.SELL.ToString() && costDetail.SEGNUM == sellDetail.SEGNUM && int.Parse(sellDetail.SEQNUM) == int.Parse(costDetail.SEQNUM) - 1)
                        {
                            sellDetail.BKGREF = costDetail.BKGREF = cloneBkGref;
                            sellDetail.AMT = costDetail.AMT;
                            costDetail.AMT = 0;
                            sellDetail.TOTALAMT = costDetail.TOTALAMT;
                            costDetail.TOTALAMT = 0;
                            sellDetail.TAXAMT = costDetail.TAXAMT;
                            costDetail.TAXAMT = 0;
                            sellDetail.UnitPrice = costDetail.UnitPrice;
                            costDetail.UnitPrice = 0;
                            sellDetail.UnitTax = costDetail.UnitTax;
                            costDetail.UnitTax = 0;
                            sellDetail.AMTFEE = costDetail.AMTFEE;
                            costDetail.AMTFEE = 0;
                            costDetail.CREATEON = DateTime.Now;
                            costDetail.UPDATEON = costDetail.CREATEON;
                            sellDetail.CREATEON = costDetail.CREATEON;
                            sellDetail.UPDATEON = costDetail.CREATEON;
                            costDetail.CREATEBY = login;
                            costDetail.UPDATEBY = costDetail.CREATEBY;
                            sellDetail.CREATEBY = costDetail.CREATEBY;
                            sellDetail.UPDATEBY = costDetail.CREATEBY;
                            break;
                        }
                    }
                }
            }
            return sourceHtlDetails;
        }
        private tbPEOOTHERs ExchangeOthers(tbPEOOTHERs sourceOthers, string cloneBkGref, string login)
        {
            foreach (tbPEOOTHER other in sourceOthers)
            {
                other.BKGREF = cloneBkGref;
                other.SourcePnr = cloneBkGref;
                other.TOTALSTAX = other.TOTALCTAX;
                other.TOTALCTAX = 0;
                other.TOTALSELL = other.TOTALCOST;
                other.TOTALCOST = 0;
                other.CREATEBY = login;
                other.CREATEON = DateTime.Now;
                other.UPDATEBY = login;
                other.UPDATEON = other.CREATEON;
                other.IssuedDoc = "N";
            }
            return sourceOthers;
        }
        private tbPEOOTHERDETAILs ExchangeOtherDetails(tbPEOOTHERDETAILs sourceOtherDetails, string cloneBkGref, string login)
        {
            foreach (tbPEOOTHERDETAIL costDetail in sourceOtherDetails)
            {
                if (MOEFormats.ToTrim(costDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.COST.ToString())
                {
                    foreach (tbPEOOTHERDETAIL sellDetail in sourceOtherDetails)
                    {
                        if (MOEFormats.ToTrim(sellDetail.SEQTYPE).ToUpper() == MOEEnums.SeqType.SELL.ToString() && costDetail.SEGNUM.Equals(sellDetail.SEGNUM) && int.Parse(sellDetail.SEQNUM).Equals(int.Parse(costDetail.SEQNUM) - 1))
                        {
                            sellDetail.BKGREF = costDetail.BKGREF = cloneBkGref;
                            sellDetail.AMT = costDetail.AMT;
                            costDetail.AMT = 0;
                            sellDetail.AMTFEE = costDetail.AMTFEE;
                            costDetail.AMTFEE = 0;
                            sellDetail.TAXAMT = costDetail.TAXAMT;
                            costDetail.TAXAMT = 0;
                            costDetail.CREATEON = DateTime.Now;
                            costDetail.UPDATEON = costDetail.CREATEON;
                            sellDetail.CREATEON = costDetail.CREATEON;
                            sellDetail.UPDATEON = costDetail.CREATEON;
                            costDetail.CREATEBY = login;
                            costDetail.UPDATEBY = costDetail.CREATEBY;
                            sellDetail.CREATEBY = costDetail.CREATEBY;
                            sellDetail.UPDATEBY = costDetail.CREATEBY;
                            break;
                        }
                    }
                }
            }
            return sourceOtherDetails;
        }
        private tbPEONYREF GetPeonyRef(tbPEOMSTR master, string actionCode, string desc, string staffCode, string teamCode)
        {
            tbPEONYREF peonyRef = new tbPEONYREF();
            peonyRef.COMPANYCODE = master.COMPANYCODE;
            peonyRef.BKGREF = master.BKGREF;
            peonyRef.DocNum = master.BKGREF;
            peonyRef.ACTIONCODE = actionCode;
            peonyRef.CLTCODE = master.CLTODE;
            peonyRef.SellCurr = master.TTLSELLCURR;
            peonyRef.SellAmt = master.TTLSELLAMT;
            peonyRef.SellTaxCurr = master.TTLSELLTAXCURR;
            peonyRef.SellTaxAmt = master.TTLSELLTAX;
            peonyRef.CostCurr = master.TTLCOSTCURR;
            peonyRef.CostAmt = master.TTLCOSTAMT;
            peonyRef.CostTaxCurr = master.TTLCOSTTAXCURR;
            peonyRef.CostTaxAmt = master.TTLCOSTTAX;
            peonyRef.Description = desc;
            peonyRef.CREATEON = master.CREATEON;
            peonyRef.CREATEBY = master.CREATEBY;
            peonyRef.STAFFCODE = staffCode == null ? master.STAFFCODE : staffCode;
            peonyRef.TEAMCODE = teamCode == null ? master.TEAMCODE : teamCode;

            return peonyRef;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (Conn != null)
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        #endregion
    }
}
