﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using System.Text.RegularExpressions;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using System.IO;
using System.Xml.Serialization;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Element;



namespace Westminster.MOE.BLL.Document
{
    public class Xo
    {
        public Config Config { get; private set; }

        private IDAL.Document.IDAXo da { get; set; }

        public Xo(Config config)
        {
            Config = config;
            da = new DAL.Document.DAXo(config.DatabaseList[MOEEnums.DALConnectionType.eMos]);
        }

        public Xo(Config config, IDAL.Document.IDAXo dataAccess)
        {
            Config = config;
            da = dataAccess;
        }

        #region Public Method

        //public bool CreateXo(XoUIInfo uiXo)
        //{
        //    string errMsg = string.Empty;
        //    return CreateXo(uiXo, out  errMsg);
        //}

        //public bool CreateXo(XoUIInfo uiXo, out string errMsg)
        //{
        //    // Serialize(uiXo);
        //    // XoUIInfo ux = new XoUIInfo();
        //    //  ux = DeSerialize();
        //    //errMsg = string.Empty;
        //    //if (GetXoPreview(uiXo) == null)
        //    //{
        //    //    return false;
        //    //}

        //   return SaveData(uiXo, out errMsg);



        //}

        public XoPreview GetXoPreview(XoPreviewInput uiInput)
        {
            if (uiInput == null)
                return null;

            XoPreview uiPreview = new XoPreview();
            uiPreview.PreviewMsg = new XoPreviewMsg();

            string msg = string.Empty;
            if (CheckXoPreviewInput(uiInput, out msg))
            {
                uiPreview.PreviewMsg.Status = XoPreviewMsg.XoStatusType.Success;
            }
            else
            {
                uiPreview.PreviewMsg.Status = XoPreviewMsg.XoStatusType.Error;
            }
            uiPreview.PreviewMsg.Msg = msg;

            if (uiPreview.PreviewMsg.Status == XoPreviewMsg.XoStatusType.Error)
            {
                return uiPreview;
            }

            XoDBForPreview dbPreview = da.GetXoDBForPreview((XoUIInfo)uiInput);
            if (dbPreview == null)
            {
                return uiPreview;
            }
            else
            {
                FillXoPreview(dbPreview, ref uiPreview);
            }

            return uiPreview;
        }

        public XoSave SaveXo(XoSaveInput saveInput,XoPreview uiPreview, XoDBForPreview dbPreview,out string msg)
        {
           if (!ValidSaveData (uiPreview ,dbPreview ,out  msg))
           {
               return null;
           }
           return null;
        }

        #endregion

        #region Private Method

        #region Preview

        private void FillXoPreview(XoDBForPreview dbPreview, ref XoPreview uiPreview)
        {
            if (uiPreview == null)
            {
                uiPreview = new XoPreview();
            }
            if (dbPreview == null || dbPreview.UIXoInfo == null)
            {
                return;
            }
            uiPreview.CompanyCode = dbPreview.UIXoInfo.CompanyCode;
            uiPreview.BookingNum = dbPreview.UIXoInfo.BookingNum;
            uiPreview.ClientName = dbPreview.Supplier.SUPPNAME;
            uiPreview.ClientCode = dbPreview.Supplier.SUPPCODE;
            uiPreview.Address1 = dbPreview.Supplier.ADDR1;
            uiPreview.Address2 = dbPreview.Supplier.ADDR2;
            uiPreview.Address3 = dbPreview.Supplier.ADDR3;
            uiPreview.Address4 = dbPreview.Supplier.ADDR4;
            uiPreview.Attn = dbPreview.UIXoInfo.Attn;
            uiPreview.FormofPayment = dbPreview.UIXoInfo.FormPay;
            if (!string.IsNullOrEmpty(dbPreview.UIXoInfo.CustomerRef1) || !string.IsNullOrEmpty(dbPreview.UIXoInfo.CustomerRef2) || !string.IsNullOrEmpty(dbPreview.UIXoInfo.CustomerRef3))
            {
                uiPreview.Ref = string.Format("{0}/{1}/{2}", dbPreview.UIXoInfo.CustomerRef1, dbPreview.UIXoInfo.CustomerRef2, dbPreview.UIXoInfo.CustomerRef3);
            }
            else
            {
                uiPreview.Ref = string.Empty;
            }
            uiPreview.Consultant = string.Format("{0}/{1}", dbPreview.Staff.STAFFNAME, dbPreview.Staff.TEAMCODE);
            uiPreview.PrintedDate = VeriFun.FormatDateDDMMMYYYY(dbPreview.UIXoInfo.PrintedDate);

            string Tel = dbPreview.Supplier.PHONE;
            if (!string.IsNullOrEmpty(Tel))
            {
                for (int i = 0; i < 2; i++)
                {
                    if (Tel.StartsWith("()"))
                    {
                        Tel = Tel.Substring(2);
                    }
                }
            }
            uiPreview.Tel = Tel;

            string Fax = dbPreview.Supplier.FAX;
            if (!string.IsNullOrEmpty(Fax))
            {
                for (int i = 0; i < 2; i++)
                {
                    if (Fax.StartsWith("()"))
                    {
                        Fax = Fax.Substring(2);
                    }
                }
            }

            uiPreview.Fax = Fax;

            string Phone = dbPreview.Staff.PHONE;
            if (!string.IsNullOrEmpty(Phone))
            {
                for (int i = 0; i < 2; i++)
                {
                    if (Phone.StartsWith("()"))
                    {
                        Phone = Phone.Substring(2);
                    }
                }
            }
            uiPreview.Phone = Phone;

            uiPreview.PassengerList = XoPreviewPax.Parse(dbPreview.Passengers);
            uiPreview.AirList = XoPreviewAir.Parse(dbPreview.Airs, dbPreview.AirDetails);
            uiPreview.HotelList = XoPreviewHotel.Parse(dbPreview.Hotels, dbPreview.HtlDetails);
            uiPreview.OtherList = XoPreviewOther.Parse(dbPreview.Others, dbPreview.OthDetails);

            uiPreview.DefaultCurr = dbPreview.Company.LOCALCURR;
            string defaultCurr = GetDefaultCurr(dbPreview);
            uiPreview.AirCurr = defaultCurr;
            uiPreview.HtlCurr = defaultCurr;
            uiPreview.OthCurr = defaultCurr;
            uiPreview.GrandTotalCurr = defaultCurr;

            decimal totalAirAmt = decimal.Zero;
            foreach (XoPreviewAir a in uiPreview.AirList)
            {
                foreach (XoPreviewAirDetail d in a.Detail)
                {
                    totalAirAmt = totalAirAmt + (d.Amt + d.TaxAmt) * d.QTY;

                }
            }
            uiPreview.AirTotalAmount = totalAirAmt;

            decimal totalHtlAmt = decimal.Zero;
            foreach (XoPreviewHotel h in uiPreview.HotelList)
            {
                foreach (XoPreviewHotelDetail d in h.Detail)
                {
                    totalHtlAmt += d.CostAmt + d.TaxAmt;
                }

            }

            uiPreview.HtlTotalAmount = totalHtlAmt;

            decimal totalOthAmt = decimal.Zero;
            foreach (XoPreviewOther o in uiPreview.OtherList)
            {
                foreach (XoPreviewOtherDetail d in o.Detail)
                {
                    totalOthAmt = totalOthAmt + (d.Amt + d.TaxAmt) * d.QTY;
                }
            }

            uiPreview.OthTotalAmount = totalOthAmt;
            uiPreview.GrandTotalAmount = totalAirAmt + totalHtlAmt + totalOthAmt;
        }

        #endregion

        #region Save

        private bool ValidSaveData(XoPreview uiPreview, XoDBForPreview dbPreview, out string msg)
        {
            msg = string.Empty;
            if (!CheckXoSellingBalanceStatus(uiPreview, dbPreview))
            {
                msg = "The total document amount cannot be greater than total booking cost amount.";
                return false;
            }
            return true;
        }

        #endregion

        private bool CheckXoPreviewInput(XoPreviewInput uiXo, out string msg)
        {
            msg = string.Empty;

            StringBuilder sbMsg = new StringBuilder();
            sbMsg.AppendLine();
            sbMsg.ToString();

            XoVerifyInfo xoVerifyInfo = da.GetXoVerifyInfo(uiXo);
            if (xoVerifyInfo == null)
            {
                return false;
            }

            //if (!CustRemarkIsRequired(uiXo, xoVerifyInfo))
            //{
            //    msg = "Remark3 incorrect formatting";
            //    return false;
            //}

            //if (xoVerifyInfo.PeoInvCount == 0)
            //{
            //    if (uiXo.AnaylsisCode == "")
            //    {
            //        msg = "Please input Analysis Code!";
            //        return false;
            //    }
            //    else if (ValidAnalysiscode(uiXo, xoVerifyInfo))
            //    {
            //        msg = "Invalid Analysis Code!";
            //        return false;
            //    }
            //}
            if (!ValidXoRemark(uiXo))
            {
                msg = "XO remarks cannot exceed 70 characters.";
                return false;
            }
            if (!isAirSegmentTypeMatch(xoVerifyInfo))
            {
                msg = "Air segment age type not match";
                return false;
            }

            if (!CheckMultiCurrency(xoVerifyInfo))
            {
                msg = "PromptNotSameCurr";
                return false;
            }
            if (!MatchAirAgeType(xoVerifyInfo))
            {
                msg = "PromptNotMatchAgeType";
                return false;
            }
            if (!MatchOtherAgeType(xoVerifyInfo))
            {
                msg = "PromptNotMatchAgeType";
                return false;
            }
            if (!CheckPax(uiXo))
            {
                msg = "No select Pax";
                return false;
            }
            //if (CheckXoSellingBalanceStatus())
            //{
            //    msg = "The Invoice Amounts greater than Selling Amounts!";
            //    return false;
            //}
            //if (!PassProfitMarginChecking())
            //{
            //    sbMsg.Append("--" + Properties.Resource_en_US.VOCAB_DOC_PMQALFORREASON);
            //    sbMsg.AppendLine();

            //}
            if (!ValidSuppCode(uiXo, xoVerifyInfo))
            {
                msg = "Valid SupplierCode";
                return false;
            }

            return true;
        }

        private bool ValidXoRemark(XoUIInfo uiXo)
        {
            if (uiXo != null && uiXo.XoRemarks != null)
            {
                if (uiXo.XoRemarks.Length > 70)
                {
                    return false;
                }
            }
            return true;
        }

        private bool isAirSegmentTypeMatch(XoVerifyInfo xoVerifyInfo)
        {
            #region Emos sql
            //myDBConn.MySQLCommand.CommandText = " SELECT a.* FROM "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) a, "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) b "
            //   myDBConn.MySQLCommand.CommandText &= " WHERE (a.SegNum<>b.SegNum and a.AgeType=b.AgeType and a.Qty<>b.Qty) OR "
            //   myDBConn.MySQLCommand.CommandText &= " ((select count(*) from "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) c "
            //   myDBConn.MySQLCommand.CommandText &= " where c.SegNum=b.SegNum and c.AgeType=a.AgeType) = 0 "
            //   myDBConn.MySQLCommand.CommandText &= ")"
            #endregion
            if (xoVerifyInfo.tbAirSegments_AgeTypes.Count > 0)
            {
                return true;
            }

            return false;
        }

        private bool CheckMultiCurrency()
        {
            #region EMos code
            //       Dim strReturn As String = objConn.MultiCurrency(MyConn, strBkgREF, where_airwai, where_hotelair, where_otherair, "SELL", str_companycode)

            //If strReturn = "YES" Or strReturn = "NO" Then
            //    errorstr = objConn.ReadResourceValue("PromptNotSameCurr", Session("rm"), strEncoding)
            //    Response.Write("<Script language=javascript>window.alert('" + errorstr + "');")
            //    Response.Write("eval(history.go(-1));")
            //    Response.Write("</script>")
            //    Panel_SHOW.Visible = False
            //    SELECT_PANEL.Visible = True
            //    Exit Sub
            //Else
            //    Me.lb_defaultcurr_title.Text = strReturn
            //    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            //    Me.lb_air_curr.Text = strReturn
            //    Me.lb_hotel_curr.Text = strReturn
            //    Me.lb_hotel_curr_GST.Text = strReturn
            //    Me.lb_other_curr1.Text = strReturn
            //    Me.lb_other_curr2.Text = strReturn
            //    Me.lb_other__GST_curr.Text = lb_air_curr.Text
            //End If
            #endregion

            return true;
        }

        private bool MatchOtherAgeType(XoVerifyInfo xoVerifyInfo)
        {
            if (xoVerifyInfo.PeoOthMatchAgeType == null)
            {
                return false;
            }
            return true;
        }

        private bool MatchAirAgeType(XoVerifyInfo xoVerifyInfo)
        {
            if (xoVerifyInfo.PeoAirMatchAgeType == null)
            {
                return false;
            }

            return true;
        }

        private bool CheckPax(XoUIInfo uiXo)
        {
            if (uiXo != null && uiXo.Passengers != null && uiXo.Passengers.Count > 0)
                return true;
            else
                return false;

        }

        private bool CheckXoSellingBalanceStatus(XoPreview uiPreview, XoDBForPreview dbPreview)
        {
            //    balance = New invoiceSellingBalanceHandler(str_companycode, strBkgREF, lb_other_curr2.Text, GrandInvoiceAmounts, eMosCommonLibrary.ConnectStringHandler.ConnectString(Me.Request).ToString)

            //' check for the object if it is working in reliable status
            //If balance.checkMyStatus Then
            //    ' check for violation of invoice amount VS selling
            //    If balance.isViolated Then
            //        Response.Write("<script>alert('The Invoice Amounts greater than Selling Amounts!');")
            //        Response.Write("eval(history.go(-1));")
            //        Response.Write("</script>")
            //    Else
            //        'Response.Write("<hr>NO violation can be detected")
            //    End If
            //Else
            decimal dTotalCostAmt = decimal.Zero;
            decimal dIssuedTotalDocAmt = decimal.Zero;
            decimal dAmt = decimal.Zero;

            dTotalCostAmt = dbPreview.PeoMstr.TTLCOSTAMT + dbPreview.PeoMstr.TTLCOSTTAX;
            dAmt = calculateAMT(uiPreview.GrandTotalAmount, dbPreview);
            dIssuedTotalDocAmt = dAmt + dbPreview.PeoMstr.DOCAMT + dbPreview.PeoMstr.DOCTAX;
            if (dIssuedTotalDocAmt > dTotalCostAmt)
            {
                return false;
            }
            return true;
        }

        private bool PassProfitMarginChecking()
        {
            //     Dim bPassProfitMarginChecking As Boolean
            //If (dbHandler.CompanyHandler.getCompanyPreference(str_companycode, dbHandler.CompanyHandler.CompanyPreference.PMQ_EXTAX) = "Y") Then
            //    bPassProfitMarginChecking = eMosCommonLibrary.DataAccess.DAPeoMstr.isExceedProfitMarginQuene(str_companycode, strBkgREF, dAmt - taxAmt, taxAmt)
            //Else
            //    bPassProfitMarginChecking = eMosCommonLibrary.DataAccess.DAPeoMstr.isExceedProfitMarginQuene(str_companycode, strBkgREF, dAmt)
            //End If
            return true;
        }

        private bool ValidSuppCode(XoUIInfo uiXo, XoVerifyInfo xoVerifyInfo)
        {
            int otherCounter = 0;
            int htlCounter = 0;
            string sSelectedSupplier = string.Empty;
            bool isValidSupp;
            isValidSupp = true;
            string inputsupplier = uiXo.SuppCode;
            string sAirSupplier = string.Empty;
            string sOtherSupplier = string.Empty;
            string sHotelSupplier = string.Empty;

            if (xoVerifyInfo.tbAirSegments != null && xoVerifyInfo.tbAirSegNotSelected[0].suppcode != null)
            {
                sAirSupplier = xoVerifyInfo.tbAirSegNotSelected[0].suppcode;
            }
            if (xoVerifyInfo.tbHotelSegments != null)
            {
                foreach (tbHotelSegment h in xoVerifyInfo.tbHotelSegments)
                {
                    if (h != null)
                    {
                        htlCounter += 1;
                        if (h.suppcode == null)
                        {
                            isValidSupp = false;
                        }
                        else
                        {
                            if (sHotelSupplier == "")
                            {
                                sHotelSupplier = h.suppcode;
                            }
                            else
                            {
                                if (sHotelSupplier != h.suppcode)
                                {
                                    isValidSupp = false;
                                }
                            }
                        }
                    }
                }
            }
            if (xoVerifyInfo.tbOtherSegments != null)
            {
                foreach (tbOtherSegment o in xoVerifyInfo.tbOtherSegments)
                {
                    if (o != null)
                    {
                        otherCounter += 1;
                        if (o.suppliercode == null)
                        {
                            isValidSupp = false;
                        }
                        else
                        {
                            if (sOtherSupplier == "")
                            {
                                sOtherSupplier = o.suppliercode;
                            }
                            else
                            {
                                if (sOtherSupplier != o.suppliercode)
                                {
                                    isValidSupp = false;
                                }
                            }
                        }
                    }
                }
            }
            if (sAirSupplier != "" && htlCounter == 0 && otherCounter == 0)
            {
                if (inputsupplier == "")
                {
                    inputsupplier = sAirSupplier;

                }
            }
            if (htlCounter == 0 && otherCounter == 0 && inputsupplier == "")
            {
                isValidSupp = false;
            }
            else
            {
                if (htlCounter != 0 && otherCounter != 0 && sOtherSupplier != sHotelSupplier)
                {
                    isValidSupp = false;
                }
                else
                {
                    if (inputsupplier == "")
                    {
                        if (htlCounter == 0)
                        {
                            sSelectedSupplier = sOtherSupplier;
                        }
                        else
                        {
                            sSelectedSupplier = sHotelSupplier;
                        }
                    }
                    else
                    {
                        sSelectedSupplier = inputsupplier;
                    }
                }

            }

            return isValidSupp;
        }

        private bool CheckMultiCurrency(XoVerifyInfo xoVerifyInfo)
        {
            if (xoVerifyInfo.tbMultiCurrs.Count > 1)
            {
                return false;
            }
            return true;
        }

        private string GetDefaultCurr(XoDBForPreview dbPreview)
        {
            string DefaultCurr = string.Empty;
            if (dbPreview.AirDetails != null && dbPreview.AirDetails.Count > 0)
            {
                foreach (sqlXoAirDetail d in dbPreview.AirDetails)
                {
                    DefaultCurr = d.curr;
                }
            }
            if (dbPreview.HtlDetails != null && dbPreview.HtlDetails.Count > 0)
            {
                foreach (sqlXoHotelDetail d in dbPreview.HtlDetails)
                {
                    DefaultCurr = d.COSTCURR;
                }
            }
            if (dbPreview.OthDetails != null && dbPreview.OthDetails.Count > 0)
            {
                foreach (sqlXoOtherDetail d in dbPreview.OthDetails)
                {
                    DefaultCurr = d.curr;
                }
            }
            return DefaultCurr;
        }

        private decimal calculateAMT(decimal amt, XoDBForPreview dbPreview)
        {
            decimal total = decimal.Zero;
            total = amt / dbPreview.Exrate.EXRATE;
            return total;
        }

        #endregion



    }
}
