﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Westminster.MOE;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Element;
using Westminster.MOE.BLL.Document.Comm;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice;

namespace Westminster.MOE.BLL.Document
{
    public class Invoice
    {
        public Config Config { get; private set; }
        private IDAL.Document.IDAInvoice da { get; set; }
        private IDAL.Maintain.IDACurrency daCurr { get; set; }
        private IDAL.Maintain.IDAShortFormProd daShortFormProd;

        private IConnection Conn;

        public Invoice(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos], 30, hwj.DBUtility.Enums.LockType.None, config.IsDevelopments))
        {
        }
        public Invoice(Config config, IConnection conn)
            : this(config, new DAL.Document.DAInvoice(conn), new DAL.Maintain.DACurrency(conn), new DAL.Maintain.DAShortFormProd(conn))
        {
            Conn = conn;
        }
        public Invoice(Config config, IDAL.Document.IDAInvoice dataAccess, IDAL.Maintain.IDACurrency dataCurrency, IDAL.Maintain.IDAShortFormProd dataShortFormProd)
        {
            Config = config;
            da = dataAccess;
            daCurr = dataCurrency;
            daShortFormProd = dataShortFormProd;
        }

        #region Public Method
        public InvoicePreview GetInvoicePreview(InvoicePreviewInput uiInput)
        {
            if (uiInput == null)
                return null;

            InvoicePreview uiPreview = new InvoicePreview();
            uiPreview.PreviewMsg = new InvMsg();

            #region Check InvPreviewInput

            string msg = string.Empty;
            InvoiceVerifyInfo invVerifyInfo = da.GetInvoiceVerifyInfo(uiInput);

            if (CheckInvPreviewInput(uiInput, invVerifyInfo, out msg))
            {
                uiPreview.PreviewMsg.Status = InvMsg.StatusType.Error;
            }
            else
            {
                uiPreview.PreviewMsg.Status = InvMsg.StatusType.Success;
            }
            uiPreview.PreviewMsg.Msg = msg;
            if (uiPreview.PreviewMsg.Status == InvMsg.StatusType.Error)
            {
                return uiPreview;
            }
            #endregion

            #region Fill InvoicePreview

            InvoiceDBForPreview dbPreview = da.GetInvoiceDBForPreview((InvoiceUIForPreview)uiInput);
            if (dbPreview == null)
            {
                return uiPreview;
            }
            else
            {
                FillInvoicePreview(dbPreview, ref uiPreview);
            }

            #endregion

            if (!CheckInvoiceSellingBalanceStatus(dbPreview.PeoMstr, uiInput.CompanyCode, uiInput.BookingNum, uiPreview.OtherCurr, uiPreview.GrandTotalamountvle))
            {
                uiPreview.PreviewMsg.Msg = "The Invoice Amounts greater than Selling Amounts!";
                uiPreview.PreviewMsg.Status = InvMsg.StatusType.Error;
            }

            if (!PassProfitMarginChecking(invVerifyInfo.Company, invVerifyInfo.PeoMstrEntity, invVerifyInfo.PeoMstrsForMasterPNR, invVerifyInfo.PMQ_Extax, invVerifyInfo.Customer, invVerifyInfo.PeoStaff, uiInput.CompanyCode, uiInput.BookingNum, (uiPreview.GrandTotalamountvle) * uiInput.CurrencySelectedItem, uiPreview.GrandTAXamt, "INV"))
            {
                uiPreview.PreviewMsg.Msg = uiPreview.PreviewMsg.Msg + "\n" + "--" + Properties.Resource_en_US.Msg_ProfitMargin_Reason;
            }

            return uiPreview;
        }

        public InvoiceSave SaveInvoice(InvoiceSaveInput saveInput)
        {
            InvoiceSave invSave = new InvoiceSave();
            invSave.SaveMsg = new InvMsg();
            InvoicePreview preview = new InvoicePreview();
            FillInvoicePreview(da.GetInvoiceDBForPreview(saveInput.PreviewInput), ref preview);

            preview.PreviewMsg = null;
            saveInput.Preview.PreviewMsg = null;

            string n = hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(preview);
            string o = hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(saveInput.Preview);
            if (n != o)
            {
                invSave.SaveMsg.Msg = "The booking was changed!"; ;
                invSave.SaveMsg.Status = InvMsg.StatusType.Error;
                return invSave;
            }
            string msg = string.Empty;

            if (!CheckInvPreviewInput(saveInput, out  msg))
            {
                invSave.SaveMsg.Msg = msg;
                invSave.SaveMsg.Status = InvMsg.StatusType.Error;
                return invSave;
            }

            InvoiceDBForSave dbForSave = GetInvoiceDBForSave(saveInput);

            if (!da.InvoiceSaveToDB(dbForSave, out msg))
            {
                invSave.SaveMsg.Msg = msg;
                invSave.SaveMsg.Status = InvMsg.StatusType.Error;
            }
            else
            {
                invSave.SaveMsg.Msg = string.Empty;
                invSave.SaveMsg.Status = InvMsg.StatusType.Success;
                invSave.BkgRef = dbForSave.PeoInv.BKGREF;
                invSave.CompanyCode = dbForSave.PeoInv.COMPANYCODE;
                invSave.InvoiceNum = dbForSave.PeoInv.INVNUM;
            }

            return invSave;
        }

        public InvoiceView GetInvoiceView(string companyCode, string invNum)
        {
            InvoiceView inv = new InvoiceView();

            InvoiceDBForView db = da.GetInvoiceDBForView(companyCode, invNum);
            if (db != null && db.PeoInv != null)
            {
                inv.Info = GetInvViewInfo(db);
                inv.PassengerList = GetPassengerList(db.PeoInvPaxList);

                inv.AirList = GetAirList(db.PeoInvSegList, db.PeoInvDetailList, db.PeoInv.IsBkgClass);
                inv.HtlList = GetHtlList(db);
                inv.OtherList = GetOthList(db);

                inv.TicketList = GetInvViewTicketList(db.PeoTKTList, db.PeoMisTKTList);

                inv.CreditCardList = GetCreditCard(db);
                inv.CombineInv = GetInvViewForCombine(db.PeoCombineInv);
            }
            return inv;
        }
        #endregion

        #region Private Method

        #region Preview
        private void FillInvoicePreview(InvoiceDBForPreview dbPreview, ref InvoicePreview uiPreview)
        {
            if (uiPreview == null)
            {
                uiPreview = new InvoicePreview();
            }
            if (dbPreview == null || dbPreview.UIInfo == null)
            {
                return;
            }
            uiPreview.CompanyCode = dbPreview.UIInfo.CompanyCode;
            uiPreview.BookingNum = dbPreview.UIInfo.BookingNum;
            uiPreview.ClientCode = dbPreview.PeoMstr.CLTODE;
            uiPreview.ContactPerson = dbPreview.PeoMstr.CONTACTPERSON;
            uiPreview.ClientName = dbPreview.UIInfo.CltName;
            uiPreview.Address1 = dbPreview.UIInfo.Address1;
            uiPreview.Address2 = dbPreview.UIInfo.Address2;
            uiPreview.Address3 = dbPreview.UIInfo.Address3;
            uiPreview.Address4 = dbPreview.UIInfo.Address4;

            uiPreview.BookingHolder = dbPreview.PeoStaff.STAFFNAME.Trim().Length > 21 ? dbPreview.PeoStaff.STAFFNAME.Trim().Substring(0, 21) : dbPreview.PeoStaff.STAFFNAME.Trim()
                + "/" + dbPreview.PeoMstr.TEAMCODE.ToUpper().Trim();


            string tel = dbPreview.PeoStaff.PHONE;
            if (!string.IsNullOrEmpty(tel))
            {
                for (int i = 0; i < 2; i++)
                {
                    if (tel.StartsWith("()"))
                    {
                        tel = tel.Substring(2);
                    }
                }
            }
            uiPreview.Tel = tel;
            uiPreview.PrintedDate = VeriFun.FormatDateDDMMMYYYY(dbPreview.UIInfo.InvoiceDate);

            string defaultCurr = GetDefaultCurr(dbPreview);
            uiPreview.DefaultCurr = defaultCurr;
            uiPreview.Attn = dbPreview.UIInfo.Attn;

            if (!string.IsNullOrEmpty(dbPreview.UIInfo.CustomerRef1) || !string.IsNullOrEmpty(dbPreview.UIInfo.CustomerRef2) || !string.IsNullOrEmpty(dbPreview.UIInfo.CustomerRef3))
            {
                uiPreview.Ref = string.Format("{0}/{1}/{2}", dbPreview.UIInfo.CustomerRef1, dbPreview.UIInfo.CustomerRef2, dbPreview.UIInfo.CustomerRef3);
            }
            else
            {
                uiPreview.Ref = string.Empty;
            }
            uiPreview.AirRemark1 = dbPreview.UIInfo.RemaiksAir1;
            uiPreview.AirRemark2 = dbPreview.UIInfo.RemaiksAir2;
            uiPreview.AirCurr = defaultCurr;
            //  uiPreview.AirCurr2 = defaultCurr;

            uiPreview.HotelRemark1 = dbPreview.UIInfo.RemaiksHtl1;
            uiPreview.HotelRemark2 = dbPreview.UIInfo.RemaiksHtl2;
            uiPreview.HotelCurr = defaultCurr;
            //  uiPreview.HotelCurr2 = defaultCurr;

            uiPreview.OtherRemark1 = dbPreview.UIInfo.RemaiksOth1;
            uiPreview.OtherRemark2 = dbPreview.UIInfo.RemaiksOth2;
            uiPreview.OtherCurr = defaultCurr;
            //   uiPreview.OtherCurr2 = defaultCurr;

            uiPreview.Consultant = dbPreview.UIInfo.IssuedBy;
            uiPreview.GSTTaxCurr = defaultCurr;
            uiPreview.GrandTotalCurr = defaultCurr;
            //    uiPreview.GrandTotalCurr2 = defaultCurr;

            uiPreview.FormPayment = dbPreview.UIInfo.InvType;
            if (dbPreview.UIInfo.ReceiptInfo != null)
            {
                uiPreview.Cash = dbPreview.UIInfo.ReceiptInfo.PaymentRemarks;
                uiPreview.ChequeNum = dbPreview.UIInfo.ReceiptInfo.ChequeNum;
                uiPreview.ChequeIssue = dbPreview.UIInfo.ReceiptInfo.IssueDate;
                uiPreview.ChequePayer = dbPreview.UIInfo.ReceiptInfo.Payer;
                uiPreview.ChequeBankCode = dbPreview.UIInfo.ReceiptInfo.BankCode;
            }

            uiPreview.PassengerList = InvPreviewPax.Parse(dbPreview.Passengers);

            uiPreview.AirList = InvPreviewAir.Parse(dbPreview.Airs, dbPreview.AirDetails);

            uiPreview.HotelList = InvPreviewHotel.Parse(dbPreview.Hotels);
            uiPreview.OtherList = InvPreviewOther.Parse(dbPreview.Others, dbPreview.OtherDetails);
            uiPreview.TicketList = InvPreviewTicket.Parse(dbPreview.Tickets);

            decimal grandTaxamt = decimal.Zero;
            decimal totalAirAmt = decimal.Zero;
            foreach (InvPreviewAir a in uiPreview.AirList)
            {
                foreach (InvPreviewAirDetail d in a.Detail)
                {
                    totalAirAmt += d.Amt + d.TaxAmt + d.AmtFee * d.Qty;
                    grandTaxamt += d.TaxAmt * d.Qty;// CDbl((MyDS.Tables("showair").Rows(j)("TAXAMT")) * MyDS.Tables("showair").Rows(j)("QTY"))
                }
            }
            uiPreview.totalamount4vle = totalAirAmt;

            decimal totalHtlAmt = decimal.Zero;
            foreach (InvPreviewHotel h in uiPreview.HotelList)
            {
                foreach (InvPreviewHotelDetail d in h.Detail)
                {
                    totalHtlAmt += d.SellAmt + d.TaxAmt;
                    grandTaxamt += d.TaxAmt; //(CDbl(MyDS.Tables("showhotel" & i).Rows(j)("taxAMT")))
                }
            }
            uiPreview.Totalamount5vle = totalHtlAmt;

            decimal totalOthAmt = decimal.Zero;
            foreach (InvPreviewOther o in uiPreview.OtherList)
            {
                foreach (InvPreviewOtherDetail d in o.Detail)
                {
                    totalOthAmt += d.Amt * d.QTY + d.TaxAmt * d.QTY;
                    grandTaxamt += d.TaxAmt * d.QTY;// CDbl(MyDS.Tables("showother").DefaultView.Item(j)("TAXAMT") * checkQTY(MyDS.Tables("showother").DefaultView.Item(j)("qty")))
                }
            }
            uiPreview.Totalamount6_vle = totalOthAmt;
            uiPreview.GrandTAXamt = grandTaxamt;
            uiPreview.GrandTotalamountvle = totalAirAmt + totalHtlAmt + totalOthAmt;
        }
        private bool CheckInvPreviewInput(InvoicePreviewInput uiInput, InvoiceVerifyInfo invVerifyInfo, out string msg)
        {
            msg = string.Empty;

            StringBuilder sbMsg = new StringBuilder();
            sbMsg.AppendLine();
            sbMsg.ToString();


            if (invVerifyInfo == null)
            {
                return false;
            }

            if (!CustRemarkIsRequired(uiInput, invVerifyInfo))
            {
                msg = "Remark3 incorrect formatting";
                return false;
            }

            if (invVerifyInfo.PeoInvCount == 0)
            {
                if (uiInput.AnaylsisCode == "")
                {
                    msg = "Please input Analysis Code!";
                    return false;
                }
                else if (ValidAnalysiscode(uiInput, invVerifyInfo))
                {
                    msg = "Invalid Analysis Code!";
                    return false;
                }
            }

            if (!isAirSegmentTypeMatch(uiInput, invVerifyInfo))
            {
                msg = "Air segment age type not match";
                return false;
            }
            string c = CheckMultiCurrency(invVerifyInfo);
            if (c == "YES" || c == "NO")
            {
                msg = "PromptNotSameCurr";
                return false;
            }
            if (!MatchAirAgeType(invVerifyInfo))
            {
                msg = "PromptNotMatchAgeType";
                return false;
            }
            if (!MatchOtherAgeType(invVerifyInfo))
            {
                msg = "PromptNotMatchAgeType";
                return false;
            }
            if (!CheckPax(uiInput))
            {
                msg = "No select Pax";
                return false;
            }

            return true;
        }

        private bool CustRemarkIsRequired(InvoiceUIForPreview uiInvoice, InvoiceVerifyInfo invVerifyInfo)
        {
            tbTEAM team = invVerifyInfo.PeoMstrTeam;
            if (team != null && team.IsRequireRef != null && team.IsRequireRef.ToUpper() == "Y")
            {
                Regex re = new Regex(@"^(\d{1,})P(\d{1,})N$");
                if (uiInvoice.CustomerRef3 == null)
                    return false;
                return re.IsMatch(uiInvoice.CustomerRef3.ToUpper());
            }
            return true;
        }

        private bool ValidAnalysiscode(InvoiceUIForPreview uiInvoice, InvoiceVerifyInfo invVerifyInfo)
        {
            return invVerifyInfo.AnalysisCodeCount > 0;
        }

        private bool isAirSegmentTypeMatch(InvoiceUIForPreview uiInvoice, InvoiceVerifyInfo invVerifyInfo)
        {
            #region Emos sql
            //myDBConn.MySQLCommand.CommandText = " SELECT a.* FROM "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) a, "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) b "
            //   myDBConn.MySQLCommand.CommandText &= " WHERE (a.SegNum<>b.SegNum and a.AgeType=b.AgeType and a.Qty<>b.Qty) OR "
            //   myDBConn.MySQLCommand.CommandText &= " ((select count(*) from "
            //   myDBConn.MySQLCommand.CommandText &= " (select SegNum, AgeType, Qty from peoairdetail where bkgref='" & sBkgRef & "' and CompanyCode='" & sCompanyCode & "' "
            //   myDBConn.MySQLCommand.CommandText &= " and SeqType='SELL' and SegNum In " & sAirSQL & " ) c "
            //   myDBConn.MySQLCommand.CommandText &= " where c.SegNum=b.SegNum and c.AgeType=a.AgeType) = 0 "
            //   myDBConn.MySQLCommand.CommandText &= ")"
            #endregion
            return invVerifyInfo.isAirSegmentTypeMatch;
        }

        private string CheckMultiCurrency(InvoiceVerifyInfo invVerifyInfo)
        {
            #region EMos code
            //       Dim strReturn As String = objConn.MultiCurrency(MyConn, strBkgREF, where_airwai, where_hotelair, where_otherair, "SELL", str_companycode)

            //If strReturn = "YES" Or strReturn = "NO" Then
            //    errorstr = objConn.ReadResourceValue("PromptNotSameCurr", Session("rm"), strEncoding)
            //    Response.Write("<Script language=javascript>window.alert('" + errorstr + "');")
            //    Response.Write("eval(history.go(-1));")
            //    Response.Write("</script>")
            //    Panel_SHOW.Visible = False
            //    SELECT_PANEL.Visible = True
            //    Exit Sub
            //Else
            //    Me.lb_defaultcurr_title.Text = strReturn
            //    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            //    Me.lb_air_curr.Text = strReturn
            //    Me.lb_hotel_curr.Text = strReturn
            //    Me.lb_hotel_curr_GST.Text = strReturn
            //    Me.lb_other_curr1.Text = strReturn
            //    Me.lb_other_curr2.Text = strReturn
            //    Me.lb_other__GST_curr.Text = lb_air_curr.Text
            //End If
            #endregion
            if (invVerifyInfo.CurrList == null || invVerifyInfo.CurrList.Count == 0)
            {
                return "NO";
            }
            else if (invVerifyInfo.CurrList.Count == 1)
            {
                return invVerifyInfo.CurrList[0];
            }
            else
            {
                return "YES";
            }
        }

        private bool MatchAirAgeType(InvoiceVerifyInfo invVerifyInfo)
        {
            // strReturn = objConn.MatchAgeType(MyConn, strBkgREF, where_pax, where_airwai, "air", "SELL")
            return invVerifyInfo.MatchAirAgeType;

        }

        private bool MatchOtherAgeType(InvoiceVerifyInfo invVerifyInfo)
        {
            //  strReturn = objConn.MatchAgeType(MyConn, strBkgREF, where_pax, where_otherair, "Other", "SELL")

            return invVerifyInfo.MatchOtherAgeType;
        }

        private bool CheckPax(InvoiceUIForPreview uiInv)
        {
            if (uiInv != null && uiInv.Passengers != null && uiInv.Passengers.Count > 0)
                return true;
            else
                return false;

        }

        private bool CheckInvoiceSellingBalanceStatus(tbPEOMSTR peoMstr, string companyCode, string bookingNum, string invCurr, decimal invAmt)
        {
            //    balance = New invoiceSellingBalanceHandler(str_companycode, strBkgREF, lb_other_curr2.Text, GrandInvoiceAmounts, eMosCommonLibrary.ConnectStringHandler.ConnectString(Me.Request).ToString)

            //' check for the object if it is working in reliable status
            //If balance.checkMyStatus Then
            //    ' check for violation of invoice amount VS selling
            //    If balance.isViolated Then
            //        Response.Write("<script>alert('The Invoice Amounts greater than Selling Amounts!');")
            //        Response.Write("eval(history.go(-1));")
            //        Response.Write("</script>")
            //    Else
            //        'Response.Write("<hr>NO violation can be detected")
            //    End If
            //Else


            InvSellingBalance balance = new InvSellingBalance(peoMstr, Config, companyCode, bookingNum, invCurr, invAmt, daCurr);

            if (balance.CheckMyStatus())
            {
                if (balance.isViolated)
                {
                    return false;
                }
            }
            return true;
        }

        private bool PassProfitMarginChecking(tbCOMPANY company, tbPEOMSTR peoMstr, tbPEOMSTRs peoMstrsForMasterPNR, bool pmq_extax, tbCustomer customer, tbPEOSTAFF peoStaff, string companyCode, string bkgRef, decimal currDocAmt, decimal currTaxAmt, string docType)
        {
            //     Dim bPassProfitMarginChecking As Boolean
            //If (dbHandler.CompanyHandler.getCompanyPreference(str_companycode, dbHandler.CompanyHandler.CompanyPreference.PMQ_EXTAX) = "Y") Then
            //    bPassProfitMarginChecking = eMosCommonLibrary.DataAccess.DAPeoMstr.isExceedProfitMarginQuene(str_companycode, strBkgREF, dAmt - taxAmt, taxAmt)
            //Else
            //    bPassProfitMarginChecking = eMosCommonLibrary.DataAccess.DAPeoMstr.isExceedProfitMarginQuene(str_companycode, strBkgREF, dAmt)
            //End If


            bool bPassProfitMarginChecking;

            if (pmq_extax)
            {
                bPassProfitMarginChecking = isExceedProfitMarginQuene(company, peoMstr, peoMstrsForMasterPNR, pmq_extax, customer, peoStaff, companyCode, bkgRef, currDocAmt - currTaxAmt, "INV");
            }
            else
            {
                bPassProfitMarginChecking = isExceedProfitMarginQuene(company, peoMstr, peoMstrsForMasterPNR, pmq_extax, customer, peoStaff, companyCode, bkgRef, currDocAmt, "INV");
            }
            return bPassProfitMarginChecking;
        }

        private string GetDefaultCurr(InvoiceDBForPreview dbPreview)
        {
            if (dbPreview.AirDetails != null && dbPreview.AirDetails.Count > 0)
            {
                return dbPreview.AirDetails[0].curr;
            }
            if (dbPreview.OtherDetails != null && dbPreview.OtherDetails.Count > 0)
            {
                return dbPreview.OtherDetails[0].curr;
            }
            if (dbPreview.Hotels != null && dbPreview.Hotels.Count > 0)
            {
                return dbPreview.Hotels[0].Details[0].SELLCURR;
            }
            return string.Empty;
        }
        #endregion

        #region Save

        private bool CheckInvPreviewInput(InvoiceSaveInput saveInput, out string msg)
        {
            msg = string.Empty;
            InvoiceVerifyInfoForSave viSave = da.GetInvoiceVerifyInfoForSave((InvoiceUIForSave)saveInput);
            tbPMQTrace pmqTraceEntity = new tbPMQTrace();
            bool myEnableEinvoice = viSave.E_EINV;

            decimal dAmt = saveInput.Preview.totalamount4vle * saveInput.CurrencySelectedItem;
            decimal taxAmt = saveInput.Preview.GrandTAXamt;

            bool bPassProfitMarginChecking;

            if (viSave.PMQ_EXTAX)
            {
                bPassProfitMarginChecking = isExceedProfitMarginQuene(viSave, saveInput.PreviewInput.CompanyCode, saveInput.PreviewInput.BookingNum, dAmt - taxAmt, taxAmt, "INV", ref pmqTraceEntity);
            }
            else
            {
                bPassProfitMarginChecking = isExceedProfitMarginQuene(viSave, saveInput.PreviewInput.CompanyCode, saveInput.PreviewInput.BookingNum, dAmt, "INV", ref pmqTraceEntity);
            }

            pmqTraceEntity.REASON = saveInput.ProfitMarginReason;
            pmqTraceEntity.PRGEXCEED = bPassProfitMarginChecking ? "Y" : "N";
            pmqTraceEntity.CREATEBY = saveInput.PreviewInput.UserCode;

            if (!bPassProfitMarginChecking && string.IsNullOrEmpty(saveInput.ProfitMarginReason) && viSave.Company.enablePMC == "1")
            {
                if (saveInput.ProfitMarginReasonVisible)
                {
                    msg = Properties.Resource_en_US.Msg_ProfitMargin_Reason;
                }
                else
                {
                    msg = Properties.Resource_en_US.Msg_ProfitMargin_AnotherReason;
                }
                return false;
            }
            else
            {

                if (!CheckInvoiceSellingBalanceStatus(viSave.PeoMstr, saveInput.PreviewInput.CompanyCode, saveInput.PreviewInput.BookingNum, saveInput.Preview.OtherCurr, saveInput.Preview.GrandTotalamountvle))
                {
                    msg = "The Invoice Amounts greater than Selling Amounts!";
                    return false;
                }

                if (viSave.Customer == null)
                {
                    msg = Properties.Resource_en_US.Invalid_Customer;
                    return false;
                }
            }

            return true;
        }
        public bool isExceedProfitMarginQuene(InvoiceVerifyInfoForSave viSave, string companyCode, string bkgRef, decimal currDocAmt, decimal currTaxAmt, string docType, ref tbPMQTrace pmqTrace)
        {

            decimal sellamt = 0;
            if (viSave.PMQ_EXTAX)
            {
                sellamt = currDocAmt;
            }
            else
            {
                sellamt = (currDocAmt + currTaxAmt);
            }
            return isExceedProfitMarginQuene(viSave, companyCode, bkgRef, sellamt, docType, ref pmqTrace); //*/
        }

        private bool isExceedProfitMarginQuene(tbCOMPANY company, tbPEOMSTR peoMstr, tbPEOMSTRs peoMstrsForMasterPNR, bool pmq_extax, tbCustomer customer, tbPEOSTAFF peoStaff, string companyCode, string bkgRef, decimal currDocAmt, string docType)
        {
            tbPMQTrace pmqTrace = new tbPMQTrace();
            return isExceedProfitMarginQuene(company, peoMstr, peoMstrsForMasterPNR, pmq_extax, customer, peoStaff, companyCode, bkgRef, currDocAmt, docType, ref  pmqTrace);
        }
        private bool isExceedProfitMarginQuene(tbCOMPANY company, tbPEOMSTR peoMstr, tbPEOMSTRs peoMstrsForMasterPNR, bool pmq_extax, tbCustomer customer, tbPEOSTAFF peoStaff, string companyCode, string bkgRef, decimal currDocAmt, decimal currTaxAmt, string docType)
        {
            decimal sellamt = 0;
            if (pmq_extax)
            {
                sellamt = currDocAmt;
            }
            else
            {
                sellamt = (currDocAmt + currTaxAmt);
            }

            return isExceedProfitMarginQuene(company, peoMstr, peoMstrsForMasterPNR, pmq_extax, customer, peoStaff, companyCode, bkgRef, sellamt, docType);
        }

        private bool isExceedProfitMarginQuene(InvoiceVerifyInfoForSave viSave, string companyCode, string bkgRef, decimal currDocAmt, string docType, ref tbPMQTrace pmqTrace)
        {
            return isExceedProfitMarginQuene(viSave.Company, viSave.PeoMstr, viSave.PeoMstrsForMasterPNR, viSave.PMQ_EXTAX, viSave.Customer, viSave.PeoStaff, companyCode, bkgRef, currDocAmt, docType, ref  pmqTrace);
        }
        private bool isExceedProfitMarginQuene(tbCOMPANY company, tbPEOMSTR peoMstr, tbPEOMSTRs peoMstrsForMasterPNR, bool pmq_extax, tbCustomer customer, tbPEOSTAFF peoStaff, string companyCode, string bkgRef, decimal currDocAmt, string docType, ref tbPMQTrace pmqTrace)
        {
            bool bPassProfitMarginChecking = false;
            int bStaffExceed = 0;
            int bCltExceed = 0;
            string myCltcode = "";
            string myStaffcode = "";
            decimal myStaffUp = decimal.Zero;
            decimal myStaffDown = decimal.Zero;
            decimal myCltUp = decimal.Zero;
            decimal myCltDown = decimal.Zero;
            decimal myYield = decimal.Zero;
            decimal myTtlinvamt = decimal.Zero;
            decimal myTtldocamt = decimal.Zero;


            if (company.enablePMC == "1")
            {
                if (currDocAmt != 0)
                {
                    tbPEOMSTR peoMstrForPnr = GetPeoMstrByBkgRefForPMQ(peoMstr, peoMstrsForMasterPNR, docType);
                    if (peoMstrForPnr != null)
                    {
                        if (pmq_extax)
                        {
                            peoMstrForPnr.INVTAX = 0;
                            peoMstrForPnr.DOCTAX = 0;
                        }

                        if ((peoMstrForPnr.DOCCOUNT > 0 && (docType == "INV" || docType == "CRN")) || (peoMstrForPnr.INVCOUNT > 0 & docType == "DOC"))
                        {
                            decimal dYield = decimal.Zero;
                            if (docType == "INV" | docType == "CRN")
                            {
                                currDocAmt = (docType == "CRN" ? currDocAmt * -1 : currDocAmt);
                                if (peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX + currDocAmt != 0)
                                {
                                    dYield = ((peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX + currDocAmt) - (peoMstrForPnr.DOCAMT + peoMstrForPnr.DOCTAX)) / (peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX + currDocAmt) * 100;
                                }
                                else
                                {
                                    if (peoMstrForPnr.DOCAMT > 0)
                                    {
                                        dYield = -100;
                                    }
                                    else
                                    {
                                        //Invoice & Document = 0
                                        bPassProfitMarginChecking = true;
                                    }
                                }

                            }
                            else
                            {
                                //Document (XO, VCH, TKT, MCO Issuance)
                                if ((peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX) != 0)
                                {
                                    dYield = ((peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX) - (peoMstrForPnr.DOCAMT + peoMstrForPnr.DOCTAX + currDocAmt)) / (peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX) * 100;
                                }
                                else
                                {
                                    if ((peoMstrForPnr.DOCAMT + peoMstrForPnr.DOCTAX + currDocAmt) > 0)
                                    {
                                        dYield = -100;
                                    }
                                    else
                                    {
                                        bPassProfitMarginChecking = true;
                                    }
                                }


                            }
                            myStaffcode = peoMstrForPnr.STAFFCODE.Trim();
                            myYield = dYield;
                            myTtlinvamt = peoMstrForPnr.INVAMT + peoMstrForPnr.INVTAX;
                            myTtldocamt = peoMstrForPnr.DOCAMT + peoMstrForPnr.DOCTAX;


                            if (!bPassProfitMarginChecking)
                            {
                                if (customer != null)
                                {
                                    myCltcode = customer.CLTCODE;
                                    myCltUp = customer.PRFTUP;
                                    myCltDown = customer.PRFTDOWN;

                                    if (myCltUp == 0 & myCltDown == 0)
                                    {
                                        if (peoStaff != null)
                                        {
                                            myStaffUp = peoStaff.UPLIMIT;
                                            myStaffDown = peoStaff.LOWLIMIT;

                                            if (dYield >= myStaffDown & dYield <= myStaffUp)
                                            {
                                                bStaffExceed = 1;
                                                bPassProfitMarginChecking = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (dYield >= myCltDown & dYield <= myCltUp & customer.CLTCODE.ToUpper().Trim() == peoMstrForPnr.CLTODE.ToUpper().Trim())
                                        {
                                            bCltExceed = 1;
                                            bPassProfitMarginChecking = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (peoStaff != null)
                                    {
                                        myStaffUp = peoStaff.UPLIMIT;
                                        myStaffDown = peoStaff.LOWLIMIT;

                                        if (dYield >= myStaffDown & dYield <= myStaffUp)
                                        {
                                            bStaffExceed = 3;
                                            bPassProfitMarginChecking = true;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            bPassProfitMarginChecking = true;
                        }
                    }
                    else
                    {
                        bPassProfitMarginChecking = true;
                    }
                }
            }
            else
            {
                bPassProfitMarginChecking = true;
            }
            //-------------------------------------

            //Added by JNY on 20 JAN 2008 (Mantis: 316)
            if ((pmqTrace != null))
            {
                pmqTrace.COMPANYCODE = companyCode;
                pmqTrace.BKGREF = bkgRef;
                pmqTrace.STAFFCODE = myStaffcode;
                pmqTrace.CLTCODE = myCltcode;
                pmqTrace.YIELD = myYield;
                pmqTrace.STAFFEXCEED = bStaffExceed.ToString();
                pmqTrace.CLTEXCEED = bCltExceed.ToString();
                pmqTrace.CLTUP = myCltUp;
                pmqTrace.CLTDOWN = myCltDown;
                pmqTrace.STAFFUP = myStaffUp;
                pmqTrace.STAFFDOWN = myStaffDown;
                pmqTrace.YIELD = myYield;
                pmqTrace.TTLINVAMT = myTtlinvamt;
                pmqTrace.TTLDOCAMT = myTtldocamt;
                pmqTrace.DOCAMT = currDocAmt;
            }
            return bPassProfitMarginChecking;
        }
        private tbPEOMSTR GetPeoMstrByBkgRefForPMQ(tbPEOMSTR peoMstr, tbPEOMSTRs peoMstrsForMasterPNR, string docType)
        {
            tbPEOMSTR mstr = new tbPEOMSTR();

            mstr.BKGREF = peoMstr.BKGREF;
            mstr.MASTERPNR = peoMstr.MASTERPNR;
            mstr.STAFFCODE = peoMstr.STAFFCODE;
            mstr.CLTODE = peoMstr.CLTODE;

            decimal InvAmt = decimal.Zero;
            decimal InvTax = decimal.Zero;
            decimal DocAmt = decimal.Zero;
            decimal DocTax = decimal.Zero;
            int InvCount = 0;
            int DocCount = 0;
            decimal TTLCostAmt = decimal.Zero;
            decimal TTLCostTax = decimal.Zero;


            foreach (tbPEOMSTR m in peoMstrsForMasterPNR)
            {
                InvAmt += m.INVAMT;
                InvTax += m.INVTAX;
                DocAmt += m.DOCAMT;
                DocTax += m.DOCTAX;
                InvCount += m.INVCOUNT;
                DocCount += m.DOCCOUNT;
                TTLCostAmt += m.TTLCOSTAMT;
                TTLCostTax += m.TTLCOSTTAX;
                if (docType == "DOC")
                {
                    if (m.BKGREF == m.MASTERPNR)
                    {
                        mstr.CLTODE = m.CLTODE;
                    }
                }
            }
            mstr.INVAMT = InvAmt;
            mstr.INVTAX = InvTax;
            mstr.DOCAMT = DocAmt;
            mstr.DOCTAX = DocTax;
            mstr.INVCOUNT = InvCount;
            mstr.DOCCOUNT = DocCount;
            mstr.TTLCOSTAMT = TTLCostAmt;
            mstr.TTLCOSTTAX = TTLCostTax;

            return mstr;
        }


        private InvoiceDBForSave GetInvoiceDBForSave(InvoiceSaveInput saveInput)
        {
            InvoiceUIForPreview previewInput = saveInput.PreviewInput;
            InvoicePreview preview = saveInput.Preview;

            InvoiceDBRefForSave dbRef = da.GetInvoiceDBRefForSave(saveInput);

            InvoiceDBForSave db = new InvoiceDBForSave();
            db.PeoInv = GetPeoInv(dbRef, saveInput);
            db.UpdatePeoMstr = GetUpdatePeoMstr(saveInput.PreviewInput.AnaylsisCode, saveInput.CompanyCode, saveInput.BkgNum);
            string sinAgeType = string.Empty;
            db.PeoInPaxList = GetPeoInPaxList(dbRef, saveInput, out  sinAgeType);

            tbPEOINVSEGs invSegs = new tbPEOINVSEGs();
            tbPEOINVDETAILs invDetails = new tbPEOINVDETAILs();

            decimal gstTotAmt = decimal.Zero;
            string sellCurr = string.Empty;
            int segnum = 1;
            GetDetailsForAir(dbRef, saveInput, ref  invSegs, ref  invDetails, ref segnum);
            GetDetailsForHotel(dbRef, saveInput, ref  invSegs, ref  invDetails, ref segnum, ref gstTotAmt);
            GetDetailsForOther(dbRef, saveInput, ref  invSegs, ref  invDetails, ref segnum, ref gstTotAmt, out sellCurr);

            db.PeoInv.gstamt = gstTotAmt;

            if (saveInput.BlSinFlag)
            {
                db.PeoOther = GetPeoOther(dbRef, saveInput);
                db.PeoOtherDetailList = GetPeoOtherDetailList(dbRef, saveInput, sinAgeType, sellCurr);
                db.PeoInvSegList.Add(GetPeoInvSegForOther(dbRef, saveInput, segnum));
            }
            db.PeoNyRef = GetPeoNyRef(db.PeoInv, saveInput, dbRef.ComRef_INV_CCARD);

            return db;
        }

        private tbPEOINV GetPeoInv(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput)
        {
            tbPEOINV inv = new tbPEOINV();
            inv.COMPANYCODE = saveInput.CompanyCode;
            inv.BKGREF = saveInput.BkgNum;
            inv.INVNUM = dbRef.InvoiceNo;
            inv.COD1 = saveInput.PreviewInput.CustomerRef1;
            inv.COD2 = saveInput.PreviewInput.CustomerRef2;
            inv.CLTCODE = saveInput.PreviewInput.CltCode;
            inv.CLTNAME = saveInput.PreviewInput.CltName;
            inv.CLTADDR = string.Format("{0}<br>{1}<br>{2}<br>{3}", saveInput.PreviewInput.Address1, saveInput.PreviewInput.Address2, saveInput.PreviewInput.Address3, saveInput.PreviewInput.Address4);
            if (inv.CLTADDR.Length > 160)
                inv.CLTADDR.Substring(0, 160);
            inv.STAFFCODE = dbRef.PeoMstr.STAFFCODE;
            inv.TEAMCODE = dbRef.PeoMstr.TEAMCODE;
            inv.CONTACTPERSON = saveInput.PreviewInput.CONTACTPERSON;
            inv.RMK1 = saveInput.Preview.AirRemark1;
            inv.RMK2 = saveInput.Preview.AirRemark2;
            inv.RMK3 = saveInput.Preview.HotelRemark1;
            inv.RMK4 = saveInput.Preview.HotelRemark2;
            inv.RMK5 = saveInput.Preview.OtherRemark1;
            inv.RMK6 = saveInput.Preview.OtherRemark2;
            inv.COD3 = saveInput.PreviewInput.CustomerRef3;
            inv.CREATEON = DateTime.Parse(saveInput.PreviewInput.InvoiceDate + DateTime.Now.ToLongTimeString());
            inv.CREATEBY = saveInput.PreviewInput.UserCode;
            inv.UPDATEON = DateTime.Now;
            inv.UPDATEBY = saveInput.PreviewInput.UserCode;
            inv.INVTYPE = saveInput.PreviewInput.RecInv == "RPT" ? saveInput.PreviewInput.ReceiptInfo.FormsOfPayment : "CSH";
            inv.ANALYSISCODE = saveInput.PreviewInput.AnaylsisCode;
            inv.ATTN = saveInput.Preview.Attn;
            inv.CUSTREF = saveInput.Preview.Ref;
            inv.SELLCURR = saveInput.Preview.OtherCurr;
            inv.SellExcludeTax = saveInput.Preview.GrandTotalamountvle - saveInput.Preview.GrandTAXamt;
            inv.TAXCURR = saveInput.Preview.OtherCurr;
            inv.TAXAMT = saveInput.Preview.GrandTAXamt;
            inv.HKDAMT = 0;
            inv.RECINV = saveInput.PreviewInput.RecInv;
            inv.PAYER = saveInput.PreviewInput.ReceiptInfo.Payer;
            inv.CHQ_ISSUEDATE = DateTime.Parse(saveInput.PreviewInput.ReceiptInfo.IssueDate);
            inv.BANKCODE = saveInput.PreviewInput.ReceiptInfo.BankCode;
            inv.PAY_RMK = saveInput.PreviewInput.ReceiptInfo.PaymentRemarks;
            inv.ChqNo = saveInput.PreviewInput.ReceiptInfo.ChequeNum;
            inv.gstpct = saveInput.GSTPCT;
            inv.gstamt = 0;
            inv.invdtcurr = saveInput.Preview.OtherCurr;
            inv.invdtsum = saveInput.Preview.GrandTotalamountvle;
            inv.INVFORMAT = saveInput.PreviewInput.InvFormat;
            inv.mstrinvnum = dbRef.ComRef_INV_CCARD ? dbRef.InvoiceNo : "";

            string InvTC = "";
            if (!string.IsNullOrEmpty(saveInput.Conditionsof))
            {
                InvTC = saveInput.Conditionsof.Trim();
            }
            if (saveInput.IsOthersConditions & !string.IsNullOrEmpty(saveInput.OthersConditions))
            {
                InvTC += "<br><br>Others conditions<br><br>" + saveInput.OthersConditions.Trim();
            }
            inv.InvTC = InvTC.Replace(Environment.NewLine, "<br>");


            return inv;
        }

        private tbPEOMSTR GetUpdatePeoMstr(string AnalysisCode, string CompanyCode, string Bkgref)
        {
            tbPEOMSTR m = new tbPEOMSTR();
            m.ANALYSISCODE = AnalysisCode;
            m.COMPANYCODE = CompanyCode;
            m.BKGREF = Bkgref;
            return m;
        }

        private tbPEOINPAXs GetPeoInPaxList(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, out string sinAgeType)
        {
            sinAgeType = string.Empty;
            tbPEOINPAXs paxs = new tbPEOINPAXs();

            if (saveInput.Preview.PassengerList != null)
            {
                Int16 paxseq = 1;
                foreach (InvPreviewPax p in saveInput.Preview.PassengerList)
                {
                    if (paxseq == 1)
                    {
                        sinAgeType = p.PaxOther;
                    }
                    tbPEOINPAX pax = new tbPEOINPAX();
                    pax.COMPANYCODE = p.CompanyCode;
                    pax.INVNUM = dbRef.InvoiceNo;
                    pax.SEQNUM = "00001";
                    pax.PAXSEQ = paxseq;
                    string paxname = string.Format("{0}/{1}/{2}/{3}/{4}/{5}", p.PaxLName, p.PaxFName, p.PaxTitle, p.PaxType, p.PaxAir, p.PaxOther);
                    pax.PAXNAME = paxname.Length > 100 ? paxname.Substring(0, 100) : paxname;
                    pax.INVTYPE = "I";
                    paxs.Add(pax);

                    paxseq++;
                }
            }
            return paxs;
        }

        private void GetDetailsForAir(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, ref tbPEOINVSEGs invSegs, ref tbPEOINVDETAILs invDetails, ref int segnum)
        {
            if (saveInput.Preview.AirList != null)
            {
                foreach (InvPreviewAir a in saveInput.Preview.AirList)
                {
                    string servname_seg = a.Header.Flight.PadRight(9, ' ') + a.Header.Class.PadRight(6, ' ') + VeriFun.FormatDateDDMMMYYYY(a.Header.DepartDATE).PadRight(15, ' ') + a.Header.DepartTime.PadRight(7, ' ') + a.Header.DepartCity.PadRight(6, ' ') + a.Header.ArrivalCity.PadRight(6, ' ');
                    string servdesc_seg = a.Header.ArrTime.PadRight(7, ' ') + VeriFun.FormatDateDDMMMYYYY(a.Header.ArrDATE).PadRight(15, ' ') + a.Header.PNR.PadRight(6, ' ');
                    tbPEOINVSEG invseg = new tbPEOINVSEG();

                    invseg.COMPANYCODE = saveInput.CompanyCode;
                    invseg.INVNUM = dbRef.InvoiceNo;
                    invseg.SEGNUM = segnum.ToString().PadRight(5, '0');
                    invseg.SERVTYPE = "AIR";
                    invseg.SERVNAME = servname_seg;
                    invseg.SERVDESC = servdesc_seg;
                    invseg.CREATEON = DateTime.Now;
                    invseg.CREATEBY = saveInput.PreviewInput.UserCode;
                    invseg.GSTAMT = 0;
                    invseg.SEGTYPE = "AIR";
                    invseg.ReasonCode = a.Header.ReasonCode;

                    invSegs.Add(invseg);

                    int seqnum = 1;
                    foreach (InvPreviewAirDetail d in a.Detail)
                    {
                        tbPEOINVDETAIL invdel = new tbPEOINVDETAIL();
                        invdel.COMPANYCODE = d.CompanyCode;
                        invdel.INVNUM = dbRef.InvoiceNo;
                        invdel.SEGNUM = segnum.ToString().PadRight(5, '0');
                        invdel.SEQNUM = seqnum.ToString().PadRight(5, '0');
                        invdel.DETLTYPE = d.AgeType;
                        invdel.ITEMNAME = d.AgeType;
                        invdel.RATENAME = "";
                        invdel.UNITPRICE = 0;
                        invdel.QTY = d.Qty;
                        invdel.SELLCURR = d.Curr;
                        invdel.SELLAMT = d.Amt;
                        invdel.TAXCURR = d.Curr;
                        invdel.TAXAMT = d.TaxAmt;
                        if (d.Curr == saveInput.PreviewInput.LocalCurr)
                        {
                            invdel.HKDAMT = d.Amt + d.TaxAmt;
                        }
                        else
                        {
                            BLL.Maintain.Currency curr = new Westminster.MOE.BLL.Maintain.Currency(Config, daCurr);
                            invdel.HKDAMT = curr.CalculateAmount(d.CompanyCode, saveInput.PreviewInput.LocalCurr, d.Curr, d.Amt + d.TaxAmt);
                        }
                        invdel.CREATEON = DateTime.Now;
                        invdel.CREATEBY = saveInput.PreviewInput.UserCode;
                        invdel.SHOWDETAILFEE = d.ShowDetailFee;
                        invdel.AMTFEE = d.AmtFee;
                        invDetails.Add(invdel);
                        seqnum++;
                    }
                    segnum++;
                }
            }
        }
        private void GetDetailsForHotel(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, ref tbPEOINVSEGs invSegs, ref tbPEOINVDETAILs invDetails, ref int segnum, ref decimal gstTotAmt)
        {
            if (saveInput.Preview.HotelList != null)
            {
                foreach (InvPreviewHotel a in saveInput.Preview.HotelList)
                {
                    string servname_seg = a.Header.HotelName + a.Header.CityCode + a.Header.CountryCode;
                    servname_seg = servname_seg.Length > 100 ? servname_seg.Substring(0, 100) : servname_seg;

                    string servdesc_seg = VeriFun.FormatDateDDMMMYYYY(a.Header.ArrDate) + " - " + VeriFun.FormatDateDDMMMYYYY(a.Header.DepartDate);
                    tbPEOINVSEG invseg = new tbPEOINVSEG();

                    invseg.COMPANYCODE = saveInput.CompanyCode;
                    invseg.INVNUM = dbRef.InvoiceNo;
                    invseg.SEGNUM = segnum.ToString().PadRight(5, '0');
                    invseg.SERVTYPE = "HTL";
                    invseg.SERVNAME = servname_seg;
                    invseg.SERVDESC = servdesc_seg;
                    invseg.CREATEON = DateTime.Now;
                    invseg.CREATEBY = saveInput.PreviewInput.UserCode;
                    //  invseg.GSTAMT = 0;
                    invseg.SEGTYPE = "HTL";
                    invseg.ReasonCode = a.Header.ReasonCode;

                    decimal GSTAMT = decimal.Zero;

                    int seqnum = 1;
                    foreach (InvPreviewHotelDetail d in a.Detail)
                    {
                        tbPEOINVDETAIL invdel = new tbPEOINVDETAIL();
                        invdel.COMPANYCODE = d.CompanyCode;
                        invdel.INVNUM = dbRef.InvoiceNo;
                        invdel.SEGNUM = segnum.ToString().PadRight(5, '0');
                        invdel.SEQNUM = seqnum.ToString().PadRight(5, '0');
                        // invdel.DETLTYPE = d.AgeType;
                        string itemname = ShowHotelDetail(ShowHotelDetail(d.RoomType, convertdateformat2(d.ItemType, d.ArrDate, d.DepartDate)), convertdateformat2(d.ItemType, d.ArrDate, d.DepartDate));
                        itemname += " " + convertdateformat2(d.ItemType, d.ArrDate, d.DepartDate);

                        invdel.ITEMNAME = itemname;
                        invdel.RATENAME = d.RateName;
                        invdel.UNITPRICE = 0;
                        invdel.QTY = d.Qty;

                        decimal rmnts = 1;
                        if (chkItemType(d.ItemType, d.RMNTS.ToString()).Length > 0)
                        {
                            rmnts = d.RMNTS;
                        }
                        invdel.RMNTS = rmnts;

                        GSTAMT = CalGSTNET(d.SellAmt + d.TaxAmt, d.GSTTaxRate, d.GSTTax);
                        gstTotAmt += GSTAMT;

                        invdel.SELLCURR = d.SellCurr;
                        invdel.SELLAMT = d.SellAmt;
                        invdel.TAXCURR = saveInput.PreviewInput.LocalCurr;
                        invdel.TAXAMT = d.TaxAmt;
                        if (d.SellCurr == saveInput.PreviewInput.LocalCurr)
                        {
                            invdel.HKDAMT = d.SellAmt + d.TaxAmt;
                        }
                        else
                        {
                            BLL.Maintain.Currency curr = new Westminster.MOE.BLL.Maintain.Currency(Config, daCurr);
                            invdel.HKDAMT = curr.CalculateAmount(d.CompanyCode, saveInput.PreviewInput.LocalCurr, d.SellCurr, d.SellAmt + d.TaxAmt);
                        }
                        invdel.CREATEON = DateTime.Now;
                        invdel.CREATEBY = saveInput.PreviewInput.UserCode;
                        invDetails.Add(invdel);
                        seqnum++;
                    }
                    invseg.GSTAMT = GSTAMT;
                    invSegs.Add(invseg);
                    segnum++;
                }
            }
        }
        private void GetDetailsForOther(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, ref tbPEOINVSEGs invSegs, ref tbPEOINVDETAILs invDetails, ref int segnum, ref decimal gstTotAmt, out string sellCurr)
        {
            sellCurr = string.Empty;
            if (saveInput.Preview.OtherList != null)
            {
                foreach (InvPreviewOther a in saveInput.Preview.OtherList)
                {
                    string servname_seg = "";

                    string servdesc_seg = a.Header.MasterDesc.Length > 1000 ? a.Header.MasterDesc.Substring(0, 1000) : a.Header.MasterDesc;
                    tbPEOINVSEG invseg = new tbPEOINVSEG();

                    invseg.COMPANYCODE = saveInput.CompanyCode;
                    invseg.INVNUM = dbRef.InvoiceNo;
                    invseg.SEGNUM = segnum.ToString().PadRight(5, '0');
                    invseg.SERVTYPE = "OTH";
                    invseg.SERVNAME = servname_seg;
                    invseg.SERVDESC = servdesc_seg;
                    invseg.CREATEON = DateTime.Now;
                    invseg.CREATEBY = saveInput.PreviewInput.UserCode;
                    //  invseg.GSTAMT = 0;
                    invseg.SEGTYPE = "OTH";
                    // invseg.ReasonCode = a.Header.ReasonCode;

                    decimal GSTAMT = decimal.Zero;
                    string othSegType = string.Empty;

                    int seqnum = 1;
                    foreach (InvPreviewOtherDetail d in a.Detail)
                    {
                        tbPEOINVDETAIL invdel = new tbPEOINVDETAIL();
                        invdel.COMPANYCODE = d.CompanyCode;
                        invdel.INVNUM = dbRef.InvoiceNo;
                        invdel.SEGNUM = segnum.ToString().PadRight(5, '0');
                        invdel.SEQNUM = seqnum.ToString().PadRight(5, '0');
                        invdel.DETLTYPE = d.AgeType;

                        invdel.ITEMNAME = d.AgeType;
                        invdel.RATENAME = "";
                        invdel.UNITPRICE = 0;
                        invdel.QTY = d.QTY;


                        GSTAMT = CalGSTNET((d.Amt + d.TaxAmt) * d.QTY, d.GSTTax, d.GSTTaxShow);
                        gstTotAmt += GSTAMT;
                        othSegType = d.SegType;

                        invdel.SELLCURR = d.Curr;
                        sellCurr = d.Curr;

                        invdel.SELLAMT = d.Amt;
                        invdel.TAXCURR = saveInput.PreviewInput.LocalCurr;
                        invdel.TAXAMT = d.TaxAmt;
                        if (d.Curr == saveInput.PreviewInput.LocalCurr)
                        {
                            invdel.HKDAMT = d.Amt + d.TaxAmt;
                        }
                        else
                        {
                            BLL.Maintain.Currency curr = new Westminster.MOE.BLL.Maintain.Currency(Config, daCurr);
                            invdel.HKDAMT = curr.CalculateAmount(d.CompanyCode, saveInput.PreviewInput.LocalCurr, d.Curr, d.Amt + d.TaxAmt);
                        }
                        invdel.CREATEON = DateTime.Now;
                        invdel.CREATEBY = saveInput.PreviewInput.UserCode;
                        invDetails.Add(invdel);
                        seqnum++;
                    }
                    invseg.SEGTYPE = othSegType;
                    invseg.GSTAMT = GSTAMT;
                    invSegs.Add(invseg);
                    segnum++;
                }
            }
        }

        private tbPEOOTHER GetPeoOther(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput)
        {
            tbPEOOTHER oth = new tbPEOOTHER();
            oth.COMPANYCODE = saveInput.CompanyCode;
            oth.BKGREF = saveInput.BkgNum;

            if (string.IsNullOrEmpty(dbRef.OtherMaxSegNum))
            {
                oth.SEGNUM = "00001";
            }
            else
            {
                int i = 0;
                int.TryParse(dbRef.OtherMaxSegNum, out i);
                oth.SEGNUM = i.ToString("0000#");
            }
            oth.SEGTYPE = "GST";
            oth.OTHERDATE = DateTime.Now;
            oth.DESCRIPT1 = "GST Tax";
            oth.DESCRIPT2 = "";
            oth.DESCRIPT3 = "";
            oth.DESCRIPT4 = "";
            oth.CITYCODE = saveInput.PreviewInput.CityCode;
            oth.COUNTRYCODE = saveInput.PreviewInput.CountryCode;
            oth.SOURCESYSTEM = "EMOS";
            oth.CREATEON = DateTime.Now;
            oth.CREATEBY = saveInput.PreviewInput.UserCode;
            return oth;
        }
        private tbPEOOTHERDETAILs GetPeoOtherDetailList(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, string sinAgeType, string sellCurr)
        {
            tbPEOOTHERDETAILs list = new tbPEOOTHERDETAILs();
            tbPEOOTHERDETAIL d1 = new tbPEOOTHERDETAIL();
            d1.COMPANYCODE = saveInput.CompanyCode;
            d1.BKGREF = saveInput.BkgNum;
            d1.SEGNUM = dbRef.OtherMaxSegNum;
            d1.SEQNUM = "00001";
            d1.SEQTYPE = "SELL";
            d1.AGETYPE = sinAgeType;
            d1.QTY = 1;
            d1.CURR = sellCurr;
            d1.AMT = 0;
            d1.TAXCURR = sellCurr;
            d1.TAXAMT = saveInput.GSTTax;
            d1.SOURCESYSTEM = "EMOS";
            d1.CREATEON = DateTime.Now;
            d1.CREATEBY = saveInput.PreviewInput.UserCode;
            list.Add(d1);

            tbPEOOTHERDETAIL d2 = new tbPEOOTHERDETAIL();
            d2.COMPANYCODE = saveInput.CompanyCode;
            d2.BKGREF = saveInput.BkgNum;
            d2.SEGNUM = dbRef.OtherMaxSegNum;
            d2.SEQNUM = "00002";
            d2.SEQTYPE = "COST";
            d2.AGETYPE = sinAgeType;
            d2.QTY = 1;
            d2.CURR = sellCurr;
            d2.AMT = 0;
            d2.TAXCURR = sellCurr;
            d2.TAXAMT = saveInput.GSTTax;
            d2.SOURCESYSTEM = "EMOS";
            d2.CREATEON = DateTime.Now;
            d2.CREATEBY = saveInput.PreviewInput.UserCode;
            list.Add(d2);
            return list;
        }
        private tbPEOINVSEG GetPeoInvSegForOther(InvoiceDBRefForSave dbRef, InvoiceSaveInput saveInput, int segnum)
        {
            tbPEOINVSEG seg = new tbPEOINVSEG();
            seg.COMPANYCODE = saveInput.CompanyCode;
            seg.INVNUM = dbRef.InvoiceNo;
            seg.SEGNUM = segnum.ToString().PadRight(5, '0');
            seg.SERVTYPE = "GST";
            seg.SERVNAME = "GST Tax";
            seg.SERVDESC = "GST Tax";
            seg.CREATEON = DateTime.Now;
            seg.CREATEBY = saveInput.PreviewInput.UserCode;

            return seg;
        }

        private tbPEONYREF GetPeoNyRef(tbPEOINV inv, InvoiceSaveInput saveInput, bool comRef_INV_CCARD)
        {
            tbPEONYREF ny = new tbPEONYREF();

            ny.COMPANYCODE = inv.COMPANYCODE;
            ny.BKGREF = inv.BKGREF;
            if (saveInput.PreviewInput.RecInv != null && saveInput.PreviewInput.RecInv.Trim().Length < 2)
            {
                ny.ACTIONCODE = "CRN";
            }
            else
            {
                ny.ACTIONCODE = saveInput.PreviewInput.RecInv;
            }
            ny.CLTCODE = inv.CLTCODE;
            ny.SUPPCODE = "";
            ny.DocNum = inv.INVNUM;
            ny.STAFFCODE = saveInput.PreviewInput.UserCode;
            ny.TEAMCODE = saveInput.PreviewInput.TeamCode;
            ny.SellCurr = inv.SELLCURR;
            ny.SellAmt = inv.SellExcludeTax;
            ny.SellTaxCurr = inv.TAXCURR;
            ny.SellTaxAmt = inv.TAXAMT;
            ny.CostCurr = inv.SELLCURR;
            ny.CostAmt = 0;
            ny.CostTaxCurr = inv.SELLCURR;
            ny.CostTaxAmt = 0;

            if (inv.gstamt == 0)
            {
                ny.Description = string.Format("(Sell {0}{1} + Tax {2}{3} / {4})", inv.SELLCURR, inv.SellExcludeTax.ToString(), inv.TAXCURR, inv.TAXAMT.ToString(), inv.CLTCODE.TrimEnd());
            }
            else
            {
                ny.Description = string.Format("(Sell {0}{1} + Tax {2}{3} + GST {4}{5} / {6})", inv.SELLCURR, inv.SellExcludeTax.ToString(), inv.TAXCURR, inv.TAXAMT.ToString(), inv.SELLCURR, inv.gstamt.ToString(), inv.CLTCODE.TrimEnd());
            }

            ny.CREATEON = inv.CREATEON;
            ny.CREATEBY = inv.CREATEBY;

            if (saveInput.PreviewInput.RecInv == "RPT")
            {
                ny.InvTypeCode = "RNOR";
            }
            else
            {
                if (comRef_INV_CCARD)
                {
                    ny.InvTypeCode = "UTCP";
                }
                else
                {
                    ny.InvTypeCode = "INOR";
                }
            }
            return ny;
        }

        private string convertdateformat2(string sItemType, DateTime ddd1, DateTime ddd2)
        {
            if (sItemType == "S" || sItemType == "R")
            {
                return "(" + VeriFun.FormatDateMMDDCCYYYY_Trim(ddd1, ddd2) + ")";
            }
            else
            {
                return "";
            }
        }
        private string ShowHotelDetail(string sString1, string sString2)
        {
            string sReturnString = "";

            return sReturnString = sString1.Substring(0, sString2.Length > 49 ? 0 : 50 - sString2.Length - 1);// Strings.Left(sString1, 50 - Strings.Len(sString2) - 1);
            // sReturnString = sReturnString.EndsWith ("'") ? objConn.FixQuotes(sReturnString) : sReturnString);

            //  return sReturnString;
        }
        private string chkItemType(string itemType, string val)
        {
            if (itemType == "S" | itemType == "R")
            {
                return val;
            }
            else
            {
                return "";
            }
        }
        private decimal CalGSTNET(decimal amt, decimal gstRate, string gst)
        {
            if (gst == "Y")
            {
                return (amt / (1 + (1 - gstRate)) * (1 - gstRate));
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region View

        private InvViewInfo GetInvViewInfo(InvoiceDBForView db)
        {
            InvViewInfo info = new InvViewInfo();
            if (db.PeoInv != null)
            {
                info.InvFormat = string.IsNullOrEmpty(db.PeoInv.INVFORMAT) ? "D" : db.PeoInv.INVFORMAT;
                info.void2JBA = db.PeoInv.VOID2JBA == null ? "" : db.PeoInv.VOID2JBA;
                info.COMPANYCODE = db.PeoInv.COMPANYCODE;
                info.BKGREF = db.PeoInv.BKGREF;
                info.INVNUM = db.PeoInv.INVNUM;
                info.RMK1 = db.PeoInv.RMK1;
                info.RMK2 = db.PeoInv.RMK2;
                info.RMK3 = db.PeoInv.RMK3;
                info.RMK4 = db.PeoInv.RMK4;
                info.RMK5 = db.PeoInv.RMK5;
                info.RMK6 = db.PeoInv.RMK6;
                info.CLTCODE = db.PeoInv.CLTCODE;
                info.CLTNAME = db.PeoInv.CLTNAME;


                string[] CltAddrArray = db.PeoInv.CLTADDR.Split(new string[] { "<br>" }, StringSplitOptions.None);
                if (CltAddrArray != null)
                {
                    info.CLTADDR1 = CltAddrArray.Length > 0 ? CltAddrArray[0] : string.Empty;
                    info.CLTADDR2 = CltAddrArray.Length > 1 ? CltAddrArray[1] : string.Empty;
                    info.CLTADDR3 = CltAddrArray.Length > 2 ? CltAddrArray[2] : string.Empty;
                    info.CLTADDR4 = CltAddrArray.Length > 3 ? CltAddrArray[3] : string.Empty;
                }

                info.PHONE = db.PeoInv.PHONE;
                info.FAX = db.PeoInv.FAX;
                info.CUSTREF = db.PeoInv.CUSTREF;
                info.invtype = db.PeoInv.INVTYPE;
                info.analysiscode = db.PeoInv.ANALYSISCODE;
                info.ATTN = db.PeoInv.ATTN;
                info.voidby = db.PeoInv.VOIDBY;
                info.voidreason = db.PeoInv.VoidREASON == null ? "" : db.PeoInv.VoidREASON;
                info.chq_dte = db.PeoInv.CHQ_ISSUEDATE;
                info.payer = db.PeoInv.PAYER;
                info.bankcode = db.PeoInv.BANKCODE;
                info.pay_rmk = db.PeoInv.PAY_RMK;
                info.chqNo = db.PeoInv.ChqNo;
                info.createOn = db.PeoInv.CREATEON;
                info.createBy = db.PeoInv.CREATEBY;
                info.updateon = db.PeoInv.UPDATEON;
                info.PAYDESC = db.PeoInv.PAYDESC;
                info.sellCurr = db.PeoInv.SELLCURR;
                info.sellamt = db.PeoInv.SELLAMT;
                info.invdtcurr = db.PeoInv.invdtcurr;
                info.invdtsum = db.PeoInv.invdtsum;
                info.CNReason = db.PeoInv.CNReason;
                info.InvTC = db.PeoInv.InvTC == null ? "" : db.PeoInv.InvTC;
                info.TeamCode = db.PeoInv.TEAMCODE;
                info.IsPackage = db.PeoInv.IsPackage;
                info.PackageDesc1 = db.PeoInv.PackageDesc1;
                info.PackageDesc2 = db.PeoInv.PackageDesc2;
                info.IsMICE = db.PeoInv.IsMICE;
                info.MICEDesc = db.PeoInv.MICEDesc;
                info.IsBkgClass = db.PeoInv.IsBkgClass;
                info.GstAmt = db.PeoInv.gstamt;
                info.GstPct = db.PeoInv.gstpct;
                info.DueDay = db.DueDay;

                if (db.PeoStaffForInv != null && db.Login != null)
                {
                    info.IssuedBy = db.PeoStaffForInv.STAFFNAME + '/' + db.Login.TEAMCODE;
                    info.PrintedBy = db.PeoStaffForInv.STAFFNAME;
                }
                else
                {
                    info.IssuedBy = db.Customer == null ? null : db.Customer.TEAMCODE;
                }

                if (db.PeoStaffForMstr != null)
                {
                    info.StaffPhone = db.PeoStaffForMstr.PHONE;
                    info.Consultant = ((db.PeoStaffForMstr.STAFFNAME.Length > 12 ? db.PeoStaffForMstr.STAFFNAME.Substring(0, 12) : db.PeoStaffForMstr.STAFFNAME) + "/" + db.PeoStaffForMstr.TEAMCODE).ToUpper();
                }

                if (db.PeoNyRef != null)
                {
                    info.InvTypeCode = db.PeoNyRef.InvTypeCode;
                    info.InvoiceTitle = GetInvoiceTitle(db.PeoNyRef.InvTypeCode, db.PeoInv.INVTYPE, db.PeoNyRef.ACTIONCODE, db.PeoInv.INVNUM, db.PeoInv.gstamt, db.PeoInv.VOIDON == DateTime.MinValue ? false : true);
                }



            }

            return info;
        }
        private List<InvViewCombine> GetInvViewForCombine(tbPEOINVs combineinv)
        {
            List<InvViewCombine> combineList = new List<InvViewCombine>();
            if (combineinv != null)
            {
                foreach (tbPEOINV c in combineinv)
                {
                    InvViewCombine cb = new InvViewCombine();
                    cb.InvNum = c.INVNUM;
                    cb.CardType = c.CR_card_type;
                    cb.CardHolder = c.CR_card_holder;
                    cb.CardAmt = c.CR_amt;
                    cb.CardExpiry = c.CR_card_expiry;
                    cb.CardNum = c.CR_card_no;
                    combineList.Add(cb);
                }
            }
            return combineList;
        }

        private InvViewPaxs GetPassengerList(tbPEOINPAXs invPaxs)
        {
            if (invPaxs == null)
                return null;

            InvViewPaxs paxs = new InvViewPaxs();
            foreach (tbPEOINPAX p in invPaxs)
            {

                int i_pos1 = 0;
                int i_pos2 = 0;
                int i_pos3 = 0;
                int i_pos4 = 0;
                int i_pos5 = 0;
                int i_len = 0;
                string str_pax = string.Empty;
                string str_temp = string.Empty;
                string str_lname = string.Empty;
                string str_fname = string.Empty;
                string str_title = string.Empty;
                string strAgeType = string.Empty;
                string strAgeAir = string.Empty;
                string strAgeOther = string.Empty;

                str_pax = p.PAXNAME;
                i_len = str_pax.Length;
                i_pos1 = str_pax.IndexOf("/", 0);

                if (i_pos1 > 0)
                {
                    str_lname = str_pax.Substring(0, i_pos1);
                    str_temp = str_pax.Substring(i_pos1, i_len - i_pos1);
                    i_len = str_temp.Length;
                    str_temp = str_temp.Substring(1, i_len - 1);
                    i_len = str_temp.Length;
                    i_pos2 = str_temp.IndexOf("/", 0);
                    if (i_pos2 > 0)
                    {
                        str_fname = str_temp.Substring(0, i_pos2);
                        str_temp = str_temp.Substring(i_pos2 + 1, i_len - i_pos2 - 1);
                        i_len = str_temp.Length;
                        str_temp = str_temp.Substring(0, i_len);
                        i_len = str_temp.Length;

                        i_pos3 = str_temp.IndexOf("/", 0);
                        if (i_pos3 > 0)
                        {
                            str_title = str_temp.Substring(0, i_pos3);
                            str_temp = str_temp.Substring(i_pos3 + 1, i_len - i_pos3 - 1);
                            i_len = str_temp.Length;
                            str_temp = str_temp.Substring(0, i_len);
                            i_len = str_temp.Length;

                            i_pos4 = str_temp.IndexOf("/", 0);
                            if (i_pos4 > 0)
                            {
                                strAgeType = str_temp.Substring(0, i_pos4);
                                str_temp = str_temp.Substring(i_pos4 + 1, i_len - i_pos4 - 1);
                                if (str_temp.IndexOf("/", 0) > 0)
                                {

                                    strAgeAir = str_temp.Substring(0, str_temp.IndexOf("/", 0));

                                    strAgeOther = str_temp.Substring(str_temp.IndexOf("/", 0) + 1);
                                }
                                else
                                {
                                    strAgeAir = str_temp;
                                }
                            }
                            i_pos5 = str_temp.IndexOf("/", 0);
                        }
                    }
                }
                InvViewPax pax = new InvViewPax();
                pax.PAXLNAME = str_lname;
                pax.PAXFNAME = str_fname;
                pax.PAXTITLE = str_title;
                pax.PAXagetype = strAgeType;
                pax.PAXageAir = strAgeAir;
                pax.PAXageOther = strAgeOther;
                paxs.Add(pax);

            }
            return paxs;
        }
        private List<InvViewCreditCard> GetCreditCard(InvoiceDBForView db)
        {
            List<InvViewCreditCard> creditcard = new List<InvViewCreditCard>();
            if (db.PeoCreditCardList == null)
            {
                return creditcard;
            }
            foreach (tbPEOINV c in db.PeoCreditCardList)
            {
                InvViewCreditCard card = new InvViewCreditCard();
                card.CardType = c.CR_card_type;
                card.CardNum = c.CR_card_no;
                card.Expiry_dte = c.CR_card_expiry;
                card.CardHolder = c.CR_card_holder;
                creditcard.Add(card);
            }

            return creditcard;
        }

        private InvViewAirSegments GetAirList(tbPEOINVSEGs peoInvSegList, tbPEOINVDETAILs peoInvDetailList, string isBkgClass)
        {
            InvViewAirSegments airlist = new InvViewAirSegments();
            if (peoInvSegList != null)
            {
                foreach (tbPEOINVSEG s in peoInvSegList)
                {
                    if (s.SERVTYPE == "AIR" && s.SEGTYPE != "SVC")
                    {
                        InvViewAirSegment seg = new InvViewAirSegment();
                        InvViewAirHeader header = new InvViewAirHeader();
                        #region header
                        header.INVNUM = s.INVNUM;
                        header.SEGNUM = s.SEGNUM;
                        header.GroupId = s.GroupID;
                        if (s.SERVNAME != null)
                        {
                            header.Airline = s.SERVNAME.Substring(0, 2).Trim();
                            header.Flight = s.SERVNAME.Length > 2 ? s.SERVNAME.Substring(2, 7).Trim() : "  ";
                            if (isBkgClass == "Y")
                            {
                                header.AirClass= s.SERVNAME.Substring(9, 6).Trim().ToUpper();
                            }
                            else
                            {
                                header.AirClass = daShortFormProd.GetAirClassName(s.SERVNAME.Substring(0, 2).Trim(), s.SERVNAME.Substring(9, 6).Trim()).ToUpper();
                            }
                            header.DepartDate = s.SERVNAME.Substring(14, 14).Trim();
                            header.DepartCity = daShortFormProd.GetCityName(s.SERVNAME.Substring(36, 5).Trim());
                            header.ArrivalCity = daShortFormProd.GetCityName(s.SERVNAME.Substring(41, 5).Trim());
                        }
                        seg.Header = header;
                        #endregion

                        if (peoInvDetailList != null)
                        {
                            List<tbPEOINVDETAIL> ds = new List<tbPEOINVDETAIL>();
                            if (s.SEGTYPE != "DOC")
                            {
                                ds = peoInvDetailList.FindAll(d => d.COMPANYCODE == s.COMPANYCODE && d.INVNUM == s.INVNUM && d.SEGNUM == s.SEGNUM);
                            }
                            if (ds != null && ds.Count > 0)
                            {
                                InvViewAirDetails details = new InvViewAirDetails();
                                foreach (tbPEOINVDETAIL d in ds)
                                {
                                    InvViewAirDetail detail = new InvViewAirDetail();

                                    #region detail
                                    detail.ShowDetailFee = d.SHOWDETAILFEE == null ? "N" : d.SHOWDETAILFEE;
                                    detail.AmtFee = d.AMTFEE;
                                    detail.INVNUM = d.INVNUM;
                                    detail.SEGNUM = d.SEGNUM;
                                    detail.SEQNUM = d.SEQNUM;
                                    detail.ItemName = d.ITEMNAME;
                                    detail.Qty = d.QTY;
                                    detail.SellCurr = d.SELLCURR;
                                    detail.SellAmt = d.SELLAMT;
                                    detail.TaxCurr = d.TAXCURR;
                                    detail.TaxAmt = d.TAXAMT;
                                    #endregion

                                    details.Add(detail);
                                }
                                seg.Details = details;
                            }
                        }
                        airlist.Add(seg);
                    }
                }
            }
            return airlist;
        }

        private InvViewHtlSegments GetHtlList(InvoiceDBForView db)
        {
            InvViewHtlSegments htllist = new InvViewHtlSegments();

            if (db == null)
                return htllist;
            if (db.PeoInvSegList == null)
                return htllist;

            if (db.PeoInvSegList != null)
            {
                foreach (tbPEOINVSEG s in db.PeoInvSegList)
                {
                    if (s.SERVTYPE == "HTL")
                    {
                        InvViewHtlSegment seg = new InvViewHtlSegment();
                        InvViewHtlHeader header = new InvViewHtlHeader();
                        #region header
                        header.INVNUM = s.INVNUM;
                        header.SEGNUM = s.SEGNUM;
                        header.ServName = s.SERVNAME;
                        string t = string.Empty;
                        if (!string.IsNullOrEmpty(s.WithBkf) || !string.IsNullOrEmpty(s.WithTfr))
                        {
                            if (!string.IsNullOrEmpty(s.WithBkf))
                            {
                                t = "With Breakfast ";
                            }
                            if (!string.IsNullOrEmpty(s.WithTfr))
                            {
                                if (s.WithTfr == "R")
                                {
                                    t += " With Car Transfer One Way";
                                }
                                else
                                {
                                    t += " With Car Transfer Round Trip";
                                }
                            }
                        }
                        header.WithBkfAndTfr = t;
                        header.WithBkfAndTfrVisible = !string.IsNullOrEmpty(t.Trim());
                        seg.Header = header;
                        #endregion

                        if (db.PeoInvDetailList != null)
                        {
                            List<tbPEOINVDETAIL> ds = new List<tbPEOINVDETAIL>();

                            ds = db.PeoInvDetailList.FindAll(d => d.COMPANYCODE == s.COMPANYCODE && d.INVNUM == s.INVNUM && d.SEGNUM == s.SEGNUM);

                            if (ds != null && ds.Count > 0)
                            {
                                InvViewHtlDetails details = new InvViewHtlDetails();
                                foreach (tbPEOINVDETAIL d in ds)
                                {
                                    InvViewHtlDetail detail = new InvViewHtlDetail();
                                    tbPEOINV inv;
                                    if (db.PeoInv.INVNUM == s.INVNUM)
                                    {
                                        inv = db.PeoInv;
                                    }
                                    else
                                    {
                                        inv = db.PeoInvCombinPlus;
                                    }

                                    #region detail
                                    detail.INVNUM = d.INVNUM;
                                    detail.SEGNUM = d.SEGNUM;
                                    detail.SEQNUM = d.SEQNUM;
                                    detail.GstPct = inv.gstpct;
                                    detail.GstAmt = s.GSTAMT;
                                    detail.ItemName = d.ITEMNAME;
                                    detail.Qty = d.QTY;
                                    detail.Rmnts = d.RMNTS;
                                    detail.SellCurr = d.SELLCURR;
                                    detail.SellAmt = d.SELLAMT;
                                    detail.TaxAmt = d.TAXAMT;
                                    detail.TotalAmt = d.SELLAMT + d.TAXAMT;

                                    #endregion

                                    details.Add(detail);
                                }
                                seg.Details = details;
                            }
                        }
                        htllist.Add(seg);
                    }
                }
            }
            return htllist;
        }

        private InvViewOthSegments GetOthList(InvoiceDBForView db)
        {
            InvViewOthSegments othlist = new InvViewOthSegments();
            if (db == null)
                return othlist;
            if (db.PeoInvSegList == null)
                return othlist;

            if (db.PeoInvSegList != null)
            {
                foreach (tbPEOINVSEG s in db.PeoInvSegList)
                {
                    if (s.SERVTYPE == "OTH" && (((db.PeoNyRef.InvTypeCode == "IUFS" || db.PeoNyRef.InvTypeCode == "RUFS") && s.SERVDESC != "Fee") || (db.PeoNyRef.InvTypeCode != "IUFS" && db.PeoNyRef.InvTypeCode != "RUFS")))
                    {
                        InvViewOthSegment seg = new InvViewOthSegment();
                        InvViewOthHeader header = new InvViewOthHeader();
                        #region header
                        header.INVNUM = s.INVNUM;
                        header.SEGNUM = s.SEGNUM;
                        header.ServDesc = s.SERVDESC;
                        seg.Header = header;


                        //#region header
                        //seg.INVNUM = s.INVNUM;
                        //seg.SEGNUM = s.SEGNUM;
                        //seg.ServDesc = s.SERVDESC;

                        #endregion

                        if (db.PeoInvDetailList != null)
                        {
                            List<tbPEOINVDETAIL> ds = new List<tbPEOINVDETAIL>();
                            ds = db.PeoInvDetailList.FindAll(d => d.COMPANYCODE == s.COMPANYCODE && d.INVNUM == s.INVNUM && d.SEGNUM == s.SEGNUM);

                            if (ds != null && ds.Count > 0)
                            {
                                InvViewOthDetails details = new InvViewOthDetails();
                                foreach (tbPEOINVDETAIL d in ds)
                                {
                                    if ((d.SELLAMT + d.TAXAMT) * d.QTY == 0)
                                        continue;

                                    InvViewOthDetail detail = new InvViewOthDetail();
                                    tbPEOINV inv;
                                    if (db.PeoInv.INVNUM == s.INVNUM)
                                    {
                                        inv = db.PeoInv;
                                    }
                                    else
                                    {
                                        inv = db.PeoInvCombinPlus;
                                    }

                                    #region detail
                                    detail.gstPct = inv.gstpct;
                                    detail.gstamt = s.GSTAMT;
                                    detail.INVNUM = d.INVNUM;
                                    detail.SEGNUM = d.SEGNUM;
                                    detail.SEQNUM = d.SEQNUM;
                                    detail.ItemName = d.ITEMNAME;
                                    detail.qty = d.QTY;
                                    detail.sellcurr = d.SELLCURR;
                                    detail.SELLAMT = d.SELLAMT;
                                    detail.taxamt = d.TAXAMT;
                                    #endregion

                                    details.Add(detail);

                                }
                                seg.Details = details;
                            }
                        }
                        othlist.Add(seg);
                    }
                }
            }
            return othlist;
        }

        #region bak


        //private void GetInvViewSegmentList(InvoiceDBForView db, out List<InvViewSegment> airList, out List<InvViewSegment> htlList, out List<InvViewSegment> otherList)
        //{
        //    airList = new List<InvViewSegment>();
        //    htlList = new List<InvViewSegment>();
        //    otherList = new List<InvViewSegment>();

        //    if (db == null)
        //        return;
        //    if (db.PeoInvSegList == null)
        //        return;
        //    foreach (tbPEOINVSEG s in db.PeoInvSegList)
        //    {
        //        InvViewSegment seg = new InvViewSegment();
        //        InvViewSegHeader header = new InvViewSegHeader();
        //        #region header
        //        header.INVNUM = s.INVNUM;
        //        header.SEGNUM = s.SEGNUM;
        //        header.SERVTYPE = s.SERVTYPE;
        //        header.segType = s.SEGTYPE;
        //        header.SERVNAME = s.SERVNAME;
        //        header.SERVDESC = s.SERVDESC;
        //        header.WithBkf = s.WithBkf;
        //        header.WithTfr = s.WithTfr;
        //        header.GroupId = s.GroupID;

        //        if (s.SERVTYPE == "AIR")
        //        {
        //            header.Ext = daShortFormProd.GetAirClassName(s.SERVNAME.Substring(0, 2).Trim(), s.SERVNAME.Substring(9, 6).Trim()).ToUpper();
        //            header.Ext = header.Ext + "|" + daShortFormProd.GetCityName(s.SERVNAME.Substring(36, 5).Trim());
        //            header.Ext = header.Ext + "|" + daShortFormProd.GetCityName(s.SERVNAME.Substring(41, 5).Trim());
        //        }

        //        #endregion
        //        seg.Header = header;
        //        if (db.PeoInvDetailList != null)
        //        {
        //            List<tbPEOINVDETAIL> ds = new List<tbPEOINVDETAIL>();
        //            if ((s.SERVTYPE == "AIR" && s.SEGTYPE != "SVC" && s.SEGTYPE != "DOC") || s.SERVTYPE == "HTL" || s.SERVTYPE == "OTH")
        //            {
        //                ds = db.PeoInvDetailList.FindAll(d => d.COMPANYCODE == s.COMPANYCODE && d.INVNUM == s.INVNUM && d.SEGNUM == s.SEGNUM);
        //            }
        //            if (ds != null && ds.Count > 0)
        //            {
        //                List<InvViewSegDetail> details = new List<InvViewSegDetail>();
        //                foreach (tbPEOINVDETAIL d in ds)
        //                {
        //                    InvViewSegDetail detail = new InvViewSegDetail();

        //                    tbPEOINV inv;
        //                    if (db.PeoInv.INVNUM == s.INVNUM)
        //                    {
        //                        inv = db.PeoInv;
        //                    }
        //                    else
        //                    {
        //                        inv = db.PeoInvCombinPlus;
        //                    }
        //                    #region detail
        //                    detail.gstPct = inv.gstpct;
        //                    detail.segType = s.SEGTYPE;
        //                    detail.gstamt = s.GSTAMT;
        //                    detail.showdetailfee = d.SHOWDETAILFEE == null ? "N" : d.SHOWDETAILFEE;
        //                    detail.amtfee = d.AMTFEE;
        //                    detail.COMPANYCODE = d.COMPANYCODE;
        //                    detail.INVNUM = d.INVNUM;
        //                    detail.SEGNUM = d.SEGNUM;
        //                    detail.SEQNUM = d.SEQNUM;
        //                    detail.ItemName = d.ITEMNAME;
        //                    detail.RateName = d.RATENAME;
        //                    detail.itemtype = d.ITEMTYPE;
        //                    detail.UNITPRICE = d.UNITPRICE;
        //                    detail.qty = d.QTY;
        //                    detail.rmnts = d.RMNTS;
        //                    detail.sellcurr = d.SELLCURR;
        //                    detail.SELLAMT = d.SELLAMT;
        //                    detail.taxcurr = d.TAXCURR;
        //                    detail.taxamt = d.TAXAMT;
        //                    detail.showamt = detail.showdetailfee == "N" ? d.AMTFEE + d.SELLAMT : d.SELLAMT;
        //                    detail.HKDAMT = d.HKDAMT;
        //                    detail.TotalAMT = s.SERVTYPE == "HTL" ? (d.SELLAMT + d.TAXAMT) : (d.SELLAMT + d.TAXAMT) * d.QTY;
        //                    detail.servtype = s.SERVTYPE;

        //                    #endregion

        //                    if (!(s.SERVTYPE == "OTH" && detail.TotalAMT == 0))
        //                    {
        //                        details.Add(detail);
        //                    }
        //                }
        //                seg.Details = details;
        //            }
        //        }
        //        if (s.SERVTYPE == "AIR" && s.SEGTYPE != "SVC")
        //        {
        //            airList.Add(seg);
        //        }
        //        else if (s.SERVTYPE == "HTL")
        //        {
        //            htlList.Add(seg);
        //        }
        //        else if (s.SERVTYPE == "OTH")
        //        {
        //            if (db.PeoNyRef.InvTypeCode == "IUFS" || db.PeoNyRef.InvTypeCode == "RUFS")
        //            {
        //                if (s.SERVDESC == "Fee")
        //                    otherList.Add(seg);
        //            }
        //            else
        //            {
        //                otherList.Add(seg);
        //            }
        //        }

        //    }
        //}
        #endregion

        private List<InvViewTicket> GetInvViewTicketList(tbPEOTKTs PeoTKTList, tbPEOMISTKTs PeoMisTKTList)
        {
            List<InvViewTicket> tkts = new List<InvViewTicket>();

            if (PeoTKTList != null)
            {
                foreach (tbPEOTKT t in PeoTKTList)
                {
                    InvViewTicket tkt = new InvViewTicket();
                    tkt.AirCode = t.AirCode;
                    tkt.PaxName = t.PaxName;
                    tkt.Ticket = t.Ticket;

                    tkts.Add(tkt);
                }
            }
            if (PeoMisTKTList != null)
            {
                foreach (tbPEOMISTKT t in PeoMisTKTList)
                {
                    InvViewTicket tkt = new InvViewTicket();
                    tkt.AirCode = t.AirCode;
                    tkt.PaxName = t.PaxName;
                    tkt.Ticket = t.Ticket;

                    tkts.Add(tkt);
                }
            }
            return tkts;
        }

        private string GetInvoiceTitle(string invTypeCode, string invType, string actionCode, string docnum, decimal gstAmt, bool isVoid)
        {
            string invTitle = string.Empty;

            if (invType == "UAT" && (invTypeCode == "UTCP" || invTypeCode == "UCCP"))
            {
                if (invTypeCode == "UTCP")
                {
                    invTitle = "Credit Card Sales";
                }
                else
                {
                    if (docnum.Substring(1, 1) == "C")
                    {
                        invTitle = "UATP Credit Note";
                    }
                    else
                    {
                        invTitle = "UATP Invoice";
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(invTypeCode))
                {
                    invTypeCode = "INOR";
                }
                if (string.IsNullOrEmpty(actionCode))
                {
                    actionCode = "INV";
                }

                string tmpInvTypeCode3 = invTypeCode.PadLeft(3, ' ').Substring(invTypeCode.PadLeft(3, ' ').Length - 3, 3).Trim().ToUpper();
                string tmpActionCode3 = actionCode.PadLeft(3, ' ').Substring(actionCode.PadLeft(3, ' ').Length - 3, 3).Trim().ToUpper();

                #region InvTypeCode

                switch (tmpInvTypeCode3)
                {
                    case "CCR":
                        invTitle = "Credit Card Sales ";
                        break;
                    case "CCP":
                        invTitle = "Credit Card Sales ";
                        break;
                    case "UAT":
                        invTitle = "UATP ";
                        break;
                    case "UFS":
                        invTitle = "UATP (C)";
                        break;
                    case "NCC":
                        invTitle = "NRCC ";
                        break;
                    case "BAL":
                        invTitle = "Balance Payment ";
                        break;
                    case "PAR":
                        invTitle = "Partial Payment  ";
                        break;
                    default:
                        invTitle = "";
                        break;
                }
                #endregion

                #region ActionCode

                switch (tmpActionCode3)
                {
                    case "INV":
                    case "DEP":
                        if (docnum.Substring(1, 1) == "I")
                        {
                            if (tmpInvTypeCode3 == "UFS")
                            {
                                invTitle = invTitle.Replace("(C)", "Invoice (C)");
                            }
                            else
                            {
                                if (tmpActionCode3 == "DEP")
                                {
                                    invTitle = "Deposit Invoice";
                                }
                                else
                                {
                                    invTitle += "Invoice";
                                }
                            }
                        }
                        else if (docnum.Substring(1, 1) == "C")
                        {
                            if (tmpActionCode3 == "DEP")
                            {
                                invTitle = "Deposit Credit Note";
                            }
                            else
                            {
                                invTitle += "Credit Note";
                            }
                        }

                        break;
                    case "RPT":
                        if (tmpInvTypeCode3 == "UFS")
                        {
                            invTitle = invTitle.Replace("(C)", "Receipt (C)");
                        }
                        else
                        {
                            invTitle += "Receipt";
                        }
                        break;
                    case "CRN":
                    case "DPC":
                        if (docnum.Substring(1, 1) == "I")
                        {
                            if (tmpActionCode3 == "DPC")
                            {
                                invTitle = "Deposit Invoice";
                            }
                            else
                            {
                                invTitle = "Invoice";
                            }

                        }
                        else if (docnum.Substring(1, 1) == "C")
                        {
                            if (tmpActionCode3 == "DPC")
                            {
                                invTitle = "Deposit Credit Note";
                            }
                            else
                            {
                                invTitle = "Credit Note";
                            }
                        }
                        break;
                    default:
                        break;
                }

                #endregion
            }
            if (isVoid)
            {
                invTitle += " (Void)";
            }
            if (gstAmt > 0)
            {
                invTitle = invTitle.Replace("Invoice", "Tax Invoice").Replace("Receipt", "Tax Receipt");
            }
            return invTitle;
        }


        #endregion

        #endregion
    }
}