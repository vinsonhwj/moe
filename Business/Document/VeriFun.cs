﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.IDAL.Maintain;
using Westminster.MOE.Lib;

namespace Westminster.MOE.BLL.Document
{
    public class VeriFun
    {
        public static string FormatDateDDMMMYYYY(object InDate)
        {
            if (InDate == null)
            {
                return "";
            }
            if (InDate is DateTime)
            {
                DateTime d = Convert.ToDateTime(InDate);

                return d.ToString("DDMMMYYYY");
            }

            DateTime d1 = DateTime.MinValue;
            DateTime.TryParse(InDate.ToString(), out d1);
            if (d1 == DateTime.MinValue)
            {
                return string.Empty;
            }
            else
            {
                return d1.ToString("DDMMMYYYY");
            }

        }

        public static string FormatDateMMDDCCYYYY_Trim(DateTime BeginDate, DateTime EndDate)
        {

            if (BeginDate == DateTime.MinValue && EndDate == DateTime.MinValue)
            {
                return " ";
            }

            if (EndDate == DateTime.MinValue)
            {
                return FormatDateMMDDCCYY(BeginDate);
            }

            if (BeginDate == DateTime.MinValue)
            {
                return FormatDateMMDDCCYY(EndDate);
            }

            string sBeginDate = FormatDateMMDDCCYY(BeginDate);
            string sEndDate = FormatDateMMDDCCYY(EndDate);

            if (BeginDate.Year == EndDate.Year)
            {
                return FormatDateMMDDCCYY(BeginDate).Substring(0, 6).Replace(" ", "") + "-" + FormatDateMMDDCCYY(EndDate).Replace(" ", "");
            }
            else
            {
                return FormatDateMMDDCCYY(BeginDate).Replace(" ", "") + "-" + FormatDateMMDDCCYY(EndDate).Replace(" ", "");
            }
        }

        //get Apr 02, 03
        public static string FormatDateMMDDCCYY(DateTime InDate)
        {
            if (InDate == DateTime.MinValue)
                return string.Empty;


            string strReturn = GetMonthName(InDate.Month);
            if (InDate.Day.ToString().Length == 1)
            {
                strReturn = strReturn + " 0";
            }
            else
            {
                strReturn = strReturn + " ";
            }
            strReturn = strReturn + InDate.Day.ToString() + ", " + InDate.Year.ToString().Substring(2, 2);
            return strReturn;
        }

        public static string GetMonthName(int Monthnum)
        {
            string strMonthName = null;
            switch (Monthnum)
            {
                case 1:
                    strMonthName = "Jan";
                    break;
                case 2:
                    strMonthName = "Feb";
                    break;
                case 3:
                    strMonthName = "Mar";
                    break;
                case 4:
                    strMonthName = "Apr";
                    break;
                case 5:
                    strMonthName = "May";
                    break;
                case 6:
                    strMonthName = "Jun";
                    break;
                case 7:
                    strMonthName = "Jul";
                    break;
                case 8:
                    strMonthName = "Aug";
                    break;
                case 9:
                    strMonthName = "Sep";
                    break;
                case 10:
                    strMonthName = "Oct";
                    break;
                case 11:
                    strMonthName = "Nov";
                    break;
                case 12:
                    strMonthName = "Dec";
                    break;
                default:
                    strMonthName = "";
                    break;
            }
            return strMonthName;
        }

        public static bool IsMatchAgeTypeBySeg(tbPEOPAXs paxs, tbPEOOTHERDETAILs otherDetails, tbPEOAIRDETAILs airDetails, tbPEOTKTs tickets)
        {
            //strSQL = "select paxother from peopax where bkgref='" & strBkgRef & "' and SEGNUM in  " & _
            //"" & Where_Pax & " and paxother not in (select distinct agetype from peootherdetail where bkgref='" & strBkgRef & "' " & _
            //" and SEGNUM in " & strWhere & " and seqtype='" & strSeqType & "')"
            //if (paxs != null || oths != null)
            //{
            //    foreach (BookingOther o in oths)
            //    {
            //        if (o.Details != null)
            //        {
            //            foreach (tbPEOPAX p in paxs)
            //            {
            //                if (!o.Details.Exists(x => x.AGETYPE == p.PAXOTHER))
            //                {
            //                    return false;
            //                }
            //            }
            //        }
            //    }
            //}

            if (paxs != null)
            {
                foreach (tbPEOPAX p in paxs)
                {
                    if (otherDetails != null && !otherDetails.Exists(x => x.AGETYPE == p.PAXOTHER))
                    {
                        return false;
                    }
                    if (airDetails != null && !airDetails.Exists(x => x.AGETYPE == p.PAXAIR))
                    {
                        return false;
                    }
                    if (tickets != null && !tickets.Exists(x => x.PAXAIR == p.PAXAIR))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool IsMultCurr(tbPEOAIRDETAILs airDetails, tbPeoHtlDetails hotelDetails, tbPEOOTHERDETAILs otherDetails)
        {
            return IsMultCurr(MOEEnums.SeqType.None, airDetails, hotelDetails, otherDetails);
        }
        public static bool IsMultCurr(MOEEnums.SeqType seqType, tbPEOAIRDETAILs airDetails, tbPeoHtlDetails hotelDetails, tbPEOOTHERDETAILs otherDetails)
        {
            string tmpCurr = string.Empty;
            if (airDetails != null)
            {
                foreach (tbPEOAIRDETAIL s in seqType == MOEEnums.SeqType.None ? airDetails : airDetails.FindAll(x => x.SEQTYPE == seqType.ToString()))
                {
                    if (tmpCurr == string.Empty)
                    {
                        tmpCurr = s.CURR;
                    }
                    else if (tmpCurr != s.CURR)
                    {
                        return true;
                    }
                }
            }
            if (hotelDetails != null)
            {
                foreach (tbPeoHtlDetail s in seqType == MOEEnums.SeqType.None ? hotelDetails : hotelDetails.FindAll(x => x.SEQTYPE == seqType.ToString()))
                {
                    if (tmpCurr == string.Empty)
                    {
                        tmpCurr = s.CURR;
                    }
                    else if (tmpCurr != s.CURR)
                    {
                        return true;
                    }
                }
            }
            if (hotelDetails != null)
            {
                foreach (tbPEOOTHERDETAIL s in seqType == MOEEnums.SeqType.None ? otherDetails : otherDetails.FindAll(x => x.SEQTYPE == seqType.ToString()))
                {
                    if (tmpCurr == string.Empty)
                    {
                        tmpCurr = s.CURR;
                    }
                    else if (tmpCurr != s.CURR)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsSameSupplier(tbPEOHTLs htls, tbPEOOTHERs others, out string suppCode, out string msg)
        {
            msg = string.Empty;
            suppCode = string.Empty;
            bool isSame = true;

            if (htls != null)
            {
                foreach (tbPEOHTL h in htls)
                {
                    if (string.IsNullOrEmpty(h.SuppCode))
                    {
                        isSame = false;
                    }
                    else
                    {
                        if (suppCode == string.Empty)
                        {
                            suppCode = h.SuppCode;
                        }
                        else if (suppCode != h.SuppCode)
                        {
                            isSame = false;
                        }
                    }
                }
            }

            if (others != null)
            {
                foreach (tbPEOOTHER o in others)
                {

                    if (string.IsNullOrEmpty(o.suppliercode))
                    {
                        isSame = false;
                    }
                    else
                    {
                        if (suppCode == string.Empty)
                        {
                            suppCode = o.suppliercode;
                        }
                        else if (suppCode != o.suppliercode)
                        {
                            isSame = false;
                        }
                    }

                }
            }

            if (!isSame)
            {
                msg = Properties.Resource_en_US.Invalid_Supplier;
                return false;
            }
            return true;
        }

        public static bool IsInvalidSupplier(string companyCode, string suppCode, IDASupplier da, out string msg)
        {
            msg = string.Empty;

            if (da.IsRestrictedSupplier(companyCode, suppCode))
            {
                msg = Properties.Resource_en_US.Invalid_Supplier + "\r\n" + string.Format(Properties.Resource_en_US.Msg_CantUseRestrictedSupplier, suppCode);
                return false;
            }
            if (!da.IsActiveSupplier(companyCode, suppCode))
            {
                msg = Properties.Resource_en_US.Invalid_Supplier + "\r\n" + string.Format(Properties.Resource_en_US.Msg_IsInactiveSupplier, suppCode);
                return false;
            }
            return true;
        }

        public static bool IsPassProfitMargin(tbPEOMSTR pmq, tbCustomer pmqCustomer, tbPEOSTAFF pmqStaff, MOEEnums.DocType docType, string companyCode, decimal exTaxAmt, decimal taxAmt, IDACompanyPreference daCompanyPreference)
        {
            if (pmq != null)
            {
                decimal amt = 0;
                bool IsExcludeTax = daCompanyPreference.GetPreferenceEnable(companyCode, MOEEnums.CompanyPreference.PMQ_EXTAX);
                // tbPEOMSTR pmq = db.PMQMaster.Clone();

                if (IsExcludeTax)
                {
                    amt = exTaxAmt;
                    pmq.INVTAX = 0;
                    pmq.DOCTAX = 0;
                }
                else
                {
                    amt = exTaxAmt + taxAmt;
                }

                if ((pmq.DOCCOUNT > 0 && (docType == MOEEnums.DocType.INV || docType == MOEEnums.DocType.CRN)) || (pmq.INVCOUNT > 0 && docType == MOEEnums.DocType.DOC))
                {
                    decimal yield = 0;
                    if (docType == MOEEnums.DocType.INV || docType == MOEEnums.DocType.CRN)
                    {
                        if (docType == MOEEnums.DocType.CRN)
                        {
                            amt = amt * -1;
                        }
                        if ((pmq.INVAMT + pmq.INVTAX + amt) != 0)
                        {
                            yield = ((pmq.INVAMT + pmq.INVTAX + amt) - (pmq.DOCAMT + pmq.DOCTAX)) / (pmq.INVAMT + pmq.INVTAX + amt) * 100;
                        }
                        else
                        {
                            if (pmq.DOCAMT > 0)
                            {
                                yield = -100;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        //Document (XO, VCH, TKT, MCO Issuance)
                        if ((pmq.INVAMT + pmq.INVTAX) != 0)
                        {
                            yield = (pmq.INVAMT + pmq.INVTAX - pmq.DOCAMT + pmq.DOCTAX) / (pmq.INVAMT + pmq.INVTAX) * 100;
                        }
                        else
                        {
                            if ((pmq.DOCAMT + pmq.DOCTAX + amt) > 0)
                            {
                                yield = -100;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }

                    if (pmqCustomer != null && pmqCustomer.PRFTUP != 0 && pmqCustomer.PRFTDOWN != 0)
                    {
                        if (yield >= pmqCustomer.PRFTDOWN && yield <= pmqCustomer.PRFTUP)
                        {
                            return true;
                        }
                    }
                    else if (pmqStaff != null)
                    {
                        if (yield >= pmqStaff.LOWLIMIT && yield <= pmqStaff.UPLIMIT)
                        {
                            return true;
                        }
                    }
                }
            }
            return true;
        }
    }
}

