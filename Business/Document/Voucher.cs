﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE;
using Westminster.MOE.DAL.Document;
using Westminster.MOE.DAL.Maintain;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Element;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Voucher;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.DALEntity.Document.Voucher;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Maintain;
using Westminster.MOE.Lib;

namespace Westminster.MOE.BLL.Document
{
    public class Voucher : IDisposable
    {
        public Config Config { get; private set; }
        private BLL.Maintain.Currency BOCurr { get; set; }
        private IDAL.Document.IDAVoucher da { get; set; }
        private IConnection Conn { get; set; }

        public Voucher(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos], 30, hwj.DBUtility.Enums.LockType.None, config.IsDevelopments))
        {
        }
        private Voucher(Config config, IConnection conn)
            : this(config, new DAVoucher(conn), new DACurrency(conn))
        {
            Conn = conn;
        }
        public Voucher(Config config, IDAL.Document.IDAVoucher dataAccess, IDAL.Maintain.IDACurrency curr)
        {
            Config = config;
            da = dataAccess;
            Conn = da.InnerConnection;
            BOCurr = new Westminster.MOE.BLL.Maintain.Currency(config, curr);
        }

        #region Public Method
        public VCHPreview GetPreview(VCHSelected input)
        {
            string msg = string.Empty;
            VCHPreview ui = new VCHPreview();
            if (input != null)
            {
                if (!CheckInput(input, out msg))
                {
                    ui.Result = new WSResult(MOEEnums.ResultStatus.InvalidData, msg);
                    return ui;
                }

                VCHPreviewDB db = da.GetDBForPreview(input);

                if (!CheckInputForDB(input, db, out msg))
                {
                    ui.Result = new WSResult(MOEEnums.ResultStatus.InvalidData, msg);
                    return ui;
                }

                ui = GetPreviewObj(input, db, out msg);

                if (ui == null)
                {
                    ui = new VCHPreview();
                    ui.Result = new WSResult(MOEEnums.ResultStatus.InvalidData, msg);
                }
                else
                {
                    ui.Result = new WSResult(MOEEnums.ResultStatus.Success, msg);
                }

                return ui;
            }
            else
            {
                ui.Result = new WSResult(MOEEnums.ResultStatus.InvalidData, Properties.Resource_en_US.Invalid_Data);
                return ui;
            }
        }
        public WSResult CreateVoucher(VCHCreate save)
        {
            WSResult rs = new WSResult();
            string msg = string.Empty;

            Conn.BeginTransaction();
            try
            {
                VCHSaveDB savedata = GetSaveData(save, out msg);

                if (!string.IsNullOrEmpty(msg))
                {
                    rs.Msg = msg;
                    rs.Status = MOEEnums.ResultStatus.InvalidData;
                    return rs;
                }

                da.SaveData(savedata);

                rs.Ext1 = savedata.Header.VCHNUM;

                //Conn.RollbackTransaction();
                Conn.CommitTransaction();
                rs.Status = MOEEnums.ResultStatus.Success;
                rs.Ext1 = savedata.Header.VCHNUM;
            }
            catch (Exception ex)
            {
                Conn.RollbackTransaction();
                rs.Msg = ex.Message;
                rs.Status = MOEEnums.ResultStatus.Error;
            }
            return rs;
        }
        #endregion

        #region Preview Private Method
        private VCHPreview GetPreviewObj(VCHSelected input, VCHPreviewDB db, out string msg)
        {
            VCHPreview ui = new VCHPreview();
            ui.PreviewMD5 = db.MD5;
            ui.Passengers = VCHPassenger.Parse(db.Passengers);
            if (ui.Passengers != null)
            {
                foreach (VCHPassenger p in ui.Passengers)
                {

                    if (p.PaxType != null && (p.PaxType == MOEEnums.PaxType.CHD.ToString() || p.PaxType == MOEEnums.PaxType.INF.ToString()))
                    {
                        ui.Child += 1;
                    }
                    else
                    {
                        ui.Adult += 1;
                    }

                }
            }
            ui.CompanyCode = input.CompanyCode;
            ui.BookingNum = input.BkgRef;
            ui.Payment = input.Payment;
            ui.CltCode = db.Master.CLTODE;
            ui.ChangePaxCount = da.GetPreferenceEnable(input.CompanyCode, MOEEnums.CompanyPreference.VCH_ADTCHD);
            ui.HtlServiceDetails = db.ServiceDetails;

            foreach (tbPEOHTL bkgHtl in db.PeoHtls)
            {
                tbPEOHOTEL htlInfo = db.PeoHotels.Find(c => c.HOTELCODE == bkgHtl.HOTELCODE);
                VCHHotel htl = new VCHHotel();
                htl.HtlCode = bkgHtl.HOTELCODE;
                htl.SegNum = bkgHtl.SEGNUM;

                string address = FormatAddress(htlInfo.ADDR1, htlInfo.ADDR2, htlInfo.ADDR3, htlInfo.ADDR4);
                string address_CN = FormatAddress(htlInfo.ADDR1_ZHCN, htlInfo.ADDR2_ZHCN, htlInfo.ADDR3_ZHCN, htlInfo.ADDR4_ZHCN);
                string address_TW = FormatAddress(htlInfo.ADDR1_ZHTW, htlInfo.ADDR2_ZHTW, htlInfo.ADDR3_ZHTW, htlInfo.ADDR4_ZHTW);
                htl.HtlAddress = FormatAddressCNTW(htlInfo.COUNTRYCODE, address, address_CN, address_TW);

                htl.HtlName = MOEFormats.ToTrim(htlInfo.HOTELNAME);
                htl.HtlName_CN = MOEFormats.ToTrim(htlInfo.HOTELNAME_ZHCN);
                htl.HtlName_TW = MOEFormats.ToTrim(htlInfo.HOTELNAME_ZHTW);

                htl.HtlTelephone = htlInfo.GeneralPhone;
                htl.HtlFax = htlInfo.GeneralFax;
                htl.HtlCfmBy = bkgHtl.HTLCFmby;

                htl.HtlCountryCode = MOEFormats.ToTrim(htlInfo.COUNTRYCODE);
                tbcountry country = db.Countrys.Find(c => c.COUNTRYCODE == htlInfo.COUNTRYCODE);
                if (country != null)
                {
                    htl.CountryName = MOEFormats.ToTrim(country.COUNTRYNAME);
                }
                tbcity city = db.Citys.Find(c => c.CITYCODE == htlInfo.CITYCODE);
                if (city != null)
                {
                    htl.CityName = MOEFormats.ToTrim(city.CITYNAME);
                }

                htl.SuppCode = bkgHtl.SuppCode;
                htl.SuppName = da.GetSuppName(ui.CompanyCode, bkgHtl.SuppCode);

                htl.HtlETA = bkgHtl.ETA;
                htl.HtlArrDate = bkgHtl.ARRDATE;
                htl.HtlArrTime = bkgHtl.ARRTIME;

                htl.HtlETD = bkgHtl.ETD;
                htl.HtlDapartDate = bkgHtl.DEPARTDATE;
                htl.HtlDapartTime = bkgHtl.DEPARTTIME;

                htl.HtlReference = bkgHtl.HTLREFERENCE;

                string withBreakfast = GetBreakfast(bkgHtl.WithBkf);
                string withTransfer = GetTransfer(bkgHtl.WithBkf);
                htl.HtlSpecialRequest = FormatSpecialRequest(MOEFormats.ToTrim(bkgHtl.SPCL_REQUEST), withBreakfast, withTransfer);

                htl.HtlRemark1 = bkgHtl.VCHRMK1;
                htl.HtlRemark2 = bkgHtl.VCHRMK2;
                htl.HtlRateName = bkgHtl.RateTypeCode;
                htl.OurRef = string.Format("{0}/{1}/{2}/{3}", ui.CltCode, bkgHtl.SuppCode, input.UserCode.Trim(), MOEFormats.ToDate(db.ServerDateTime, MOEEnums.DateFormats.MMMddyyyy));

                ui.Hotels.Add(htl);
            }

            foreach (tbPEOOTHER dbOTH in db.PeoOthers)
            {
                VCHOther oth = new VCHOther();
                oth.SegNum = dbOTH.SEGNUM;
                oth.SegType = dbOTH.SEGTYPE;
                oth.MasterDesc = MOEFormats.ToTrim(dbOTH.masterDesc);
                ui.Others.Add(oth);
            }

            decimal excludeTaxAmt = 0;
            decimal taxAmt = 0;

            if (!TryParseAmount(input, db, out excludeTaxAmt, out taxAmt, out msg))
            {
                return null;
            }

            ui.ExcludeTaxAmount = excludeTaxAmt;
            ui.TaxAmount = taxAmt;
            ui.TotalAmount = ui.ExcludeTaxAmount + ui.TaxAmount;

            ui.PassPMQ = VeriFun.IsPassProfitMargin(db.PMQMaster, db.PMQCustomer, db.PMQStaff, MOEEnums.DocType.DOC, db.Master.COMPANYCODE, ui.ExcludeTaxAmount, ui.TaxAmount, da);

            ui.Result.Status = MOEEnums.ResultStatus.Success;
            return ui;
        }
        private bool CheckInput(VCHSelected input, out string msg)
        {
            msg = string.Empty;

            if (string.IsNullOrEmpty(input.VoucherType))
            {
                msg = string.Format(Properties.Resource_en_US.Invalid_Format, "Voucher Type");
                return false;
            }

            if (!IsValidHotelCount(input, out msg))
            {
                return false;
            }

            return true;
        }
        private bool CheckInputForDB(VCHSelected input, VCHPreviewDB db, out string msg)
        {
            msg = string.Empty;

            if (!IsValidSupplier(db, out msg))
            {
                return false;
            }

            if (!IsValidCurrency(db))
            {
                msg = Properties.Resource_en_US.Invalid_MultiCurrency;
                return false;
            }
            if (da.GetPreferenceEnable(input.CompanyCode, MOEEnums.CompanyPreference.DOC_VCHCHK))
            {
                if (!IsValidDocAmount(db))
                {
                    msg = Properties.Resource_en_US.Invalid_DocAmtGreateThanCostAmt;
                    return false;
                }
            }

            return true;
        }

        private string GetBreakfast(string withBreakfast)
        {
            if (!string.IsNullOrEmpty(withBreakfast) && withBreakfast.Trim().ToUpper() == "B")
            {
                return Properties.Resource_en_US.Inf_WithBreakfast;
            }
            return string.Empty;
        }
        private string GetTransfer(string withTransfer)
        {
            if (!string.IsNullOrEmpty(withTransfer))
            {
                if (withTransfer.Trim().ToUpper() == "R")
                {
                    return Properties.Resource_en_US.Inf_WithCarTransferRoundTrip;
                }
                else if (withTransfer.Trim().ToUpper() == "W")
                {
                    return Properties.Resource_en_US.Inf_WithCarTransferOneWay;
                }
            }

            return string.Empty;
        }
        private string GetRateName(List<Hotel> htls)
        {
            StringBuilder sb = new StringBuilder();
            string tmpStr = string.Empty;

            if (htls != null)
            {
                foreach (Hotel h in htls)
                {
                    if (h.BookingHotel != null && h.BookingHotel.Details != null)
                    {
                        List<tbPeoHtlDetail> dtls = h.BookingHotel.Details.FindAll(d => d.SEQTYPE == MOEEnums.SeqType.COST.ToString());
                        foreach (tbPeoHtlDetail d in dtls)
                        {
                            if (d != null && !string.IsNullOrEmpty(d.RATENAME))
                            {
                                if (string.IsNullOrEmpty(tmpStr))
                                {
                                    tmpStr = d.RATENAME;
                                    sb.AppendLine(tmpStr);
                                }
                                else if (tmpStr != d.RATENAME)
                                {
                                    tmpStr = d.RATENAME;
                                    sb.AppendLine(tmpStr);
                                }
                            }
                        }
                    }
                }
            }

            return sb.ToString().Replace("\r\n", "");
        }

        private bool TryParseAmount(VCHSelected input, VCHPreviewDB db, out decimal excludeTaxAmt, out decimal taxAmt, out string msg)
        {
            msg = string.Empty;
            excludeTaxAmt = 0;
            taxAmt = 0;

            try
            {
                if (db.ServiceDetails != null)
                {
                    foreach (sqlVchHotel h in db.ServiceDetails)
                    {
                        excludeTaxAmt += BOCurr.CalculateAmount(input.CompanyCode, input.CurrCode, h.CURR, h.AMT);
                        taxAmt += BOCurr.CalculateAmount(input.CompanyCode, input.CurrCode, h.CURR, h.TAXAMT);
                    }
                }

                if (db.PeoOtherDetails != null)
                {
                    List<tbPEOOTHERDETAIL> dtls = db.PeoOtherDetails.FindAll(d => d.SEQTYPE == MOEEnums.SeqType.COST.ToString());
                    foreach (tbPEOOTHERDETAIL d in dtls)
                    {
                        excludeTaxAmt += BOCurr.CalculateAmount(input.CompanyCode, input.CurrCode, d.CURR, d.AMT);
                        taxAmt += BOCurr.CalculateAmount(input.CompanyCode, input.CurrCode, d.CURR, d.TAXAMT);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return false;
        }

        private bool IsValidHotelCount(VCHSelected input, out string msg)
        {
            msg = string.Empty;
            if (input != null && input.Segments != null)
            {
                int htlCount = input.Segments.FindAll(s => s.Type == Segment.SegmentType.Hotel).Count;

                if (htlCount > 1)
                {
                    msg = Properties.Resource_en_US.Invalid_VCH_MultiHotelSeg;
                    return false;
                }
                else if (htlCount == 1)
                {
                    return true;
                }
            }
            msg = Properties.Resource_en_US.Msg_SelectHotelSegment;
            return false;
        }
        private bool IsValidSupplier(VCHPreviewDB db, out string msg)
        {
            msg = string.Empty;
            string tmpSuppCode = string.Empty;
            bool isSame = true;

            if (db.PeoHtls != null)
            {
                foreach (tbPEOHTL h in db.PeoHtls)
                {
                    if (string.IsNullOrEmpty(h.SuppCode))
                    {
                        isSame = false;
                    }
                    else
                    {
                        if (tmpSuppCode == string.Empty)
                        {
                            tmpSuppCode = h.SuppCode;
                        }
                        else if (tmpSuppCode != h.SuppCode)
                        {
                            isSame = false;
                        }
                    }
                }
            }

            if (db.PeoOthers != null)
            {
                foreach (tbPEOOTHER o in db.PeoOthers)
                {
                    if (o != null)
                    {
                        if (string.IsNullOrEmpty(o.suppliercode))
                        {
                            isSame = false;
                        }
                        else
                        {
                            if (tmpSuppCode == string.Empty)
                            {
                                tmpSuppCode = o.suppliercode;
                            }
                            else if (tmpSuppCode != o.suppliercode)
                            {
                                isSame = false;
                            }
                        }
                    }
                }
            }

            if (!isSame)
            {
                msg = Properties.Resource_en_US.Invalid_Supplier;
                return false;
            }
            else
            {
                if (da.IsRestrictedSupplier(db.Master.COMPANYCODE, tmpSuppCode))
                {
                    return VeriFun.IsInvalidSupplier(db.Master.COMPANYCODE, tmpSuppCode, da, out msg);
                }
                if (!da.IsActiveSupplier(db.Master.COMPANYCODE, tmpSuppCode))
                {
                    msg = Properties.Resource_en_US.Invalid_Supplier + "\r\n" + string.Format(Properties.Resource_en_US.Msg_IsInactiveSupplier, tmpSuppCode);
                    return false;
                }
            }
            return true;
        }
        private bool IsValidCurrency(VCHPreviewDB db)
        {
            if (VeriFun.IsMultCurr(MOEEnums.SeqType.COST, null, db.PeoHtlDetails, db.PeoOtherDetails))
            {
                return false;
            }
            return true;
        }
        private bool IsValidAgeType(VCHPreviewDB db)
        {
            if (db != null)
            {
                return VeriFun.IsMatchAgeTypeBySeg(db.Passengers, db.PeoOtherDetails, null, null);
            }
            return true;
        }
        private bool IsValidDocAmount(VCHPreviewDB db)
        {
            if (db.Master != null)
            {
                decimal mstrCostAmt = db.Master.TTLCOSTAMT + db.Master.TTLCOSTTAX;
                decimal mstrDocAmt = db.TotalAmount + db.Master.DOCAMT + db.Master.DOCTAX;

                if (mstrDocAmt <= mstrCostAmt)
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsPassProfitMargin(VCHPreviewDB db, MOEEnums.DocType docType, decimal exTaxAmt, decimal taxAmt)
        {
            if (db.PMQMaster != null)
            {
                decimal amt = 0;
                bool IsExcludeTax = da.GetPreferenceEnable(db.Master.COMPANYCODE, MOEEnums.CompanyPreference.PMQ_EXTAX);
                tbPEOMSTR pmq = db.PMQMaster.Clone();

                if (IsExcludeTax)
                {
                    amt = exTaxAmt;
                    pmq.INVTAX = 0;
                    pmq.DOCTAX = 0;
                }
                else
                {
                    amt = exTaxAmt + taxAmt;
                }

                if ((pmq.DOCCOUNT > 0 && (docType == MOEEnums.DocType.INV || docType == MOEEnums.DocType.CRN)) || (pmq.INVCOUNT > 0 && docType == MOEEnums.DocType.DOC))
                {
                    decimal yield = 0;
                    if (docType == MOEEnums.DocType.INV || docType == MOEEnums.DocType.CRN)
                    {
                        if (docType == MOEEnums.DocType.CRN)
                        {
                            amt = amt * -1;
                        }
                        if ((pmq.INVAMT + pmq.INVTAX + amt) != 0)
                        {
                            yield = ((pmq.INVAMT + pmq.INVTAX + amt) - (pmq.DOCAMT + pmq.DOCTAX)) / (pmq.INVAMT + pmq.INVTAX + amt) * 100;
                        }
                        else
                        {
                            if (pmq.DOCAMT > 0)
                            {
                                yield = -100;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        //Document (XO, VCH, TKT, MCO Issuance)
                        if ((pmq.INVAMT + pmq.INVTAX) != 0)
                        {
                            yield = (pmq.INVAMT + pmq.INVTAX - pmq.DOCAMT + pmq.DOCTAX) / (pmq.INVAMT + pmq.INVTAX) * 100;
                        }
                        else
                        {
                            if ((pmq.DOCAMT + pmq.DOCTAX + amt) > 0)
                            {
                                yield = -100;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }

                    if (db.PMQCustomer != null && db.PMQCustomer.PRFTUP != 0 && db.PMQCustomer.PRFTDOWN != 0)
                    {
                        if (yield >= db.PMQCustomer.PRFTDOWN && yield <= db.PMQCustomer.PRFTUP)
                        {
                            return true;
                        }
                    }
                    else if (db.PMQStaff != null)
                    {
                        if (yield >= db.PMQStaff.LOWLIMIT && yield <= db.PMQStaff.UPLIMIT)
                        {
                            return true;
                        }
                    }
                }
            }
            return true;
        }

        private string FormatSpecialRequest(string specialRequest, string withBreakfast, string withTransfer)
        {
            string tmp = specialRequest;
            if (!string.IsNullOrEmpty(withBreakfast))
            {
                tmp += string.Format("<BR>{0}", withBreakfast);
            }

            if (!string.IsNullOrEmpty(withTransfer))
            {
                tmp += string.Format("<BR>{0}", withTransfer);
            }

            if (string.IsNullOrEmpty(tmp))
            {
                return "Nil";
            }
            else
            {
                return tmp;
            }
        }
        private string FormatAddressCNTW(string countryCode, string add, string add_CN, string add_TW)
        {
            string tmp = add;
            countryCode = countryCode.ToUpper().Trim();
            if (countryCode == "CN")
            {
                if (!string.IsNullOrEmpty(add_CN))
                {
                    add = string.Format("{0}<BR>{1}", add, add_CN);
                }
            }
            else if (countryCode == "JP" || countryCode == "TW")
            {
                if (!string.IsNullOrEmpty(add_TW))
                {
                    add = string.Format("{0}<BR>{1}", add, add_TW);
                }
            }
            return add;
        }
        private string FormatAddress(string add1, string add2, string add3, string add4)
        {
            string fmt = "<BR> {0}";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(add1))
            {
                sb.Append(add1.Trim());
            }

            add2 = MOEFormats.ToTrim(add2);
            if (!string.IsNullOrEmpty(add2))
            {
                sb.AppendFormat(fmt, add2.Trim());
            }

            add3 = MOEFormats.ToTrim(add3);
            if (!string.IsNullOrEmpty(add3))
            {
                sb.AppendFormat(fmt, add3.Trim());
            }

            add4 = MOEFormats.ToTrim(add4);
            if (!string.IsNullOrEmpty(add4))
            {
                sb.AppendFormat(fmt, add4.Trim());
            }

            return sb.ToString();
        }
        #endregion

        #region Create Private Method
        private VCHSaveDB GetSaveData(VCHCreate input, out string msg)
        {
            msg = string.Empty;
            VCHSaveDB saveObj = new VCHSaveDB(true);
            if (input != null && input.SelectedKey != null)
            {
                VCHCreateDB db = da.GetCreateDB(input.SelectedKey);

                if (db != null)
                {
                    if (string.IsNullOrEmpty(input.PreviewMD5) || db.PreviewDB.MD5 != input.PreviewMD5)
                    {
                        msg = Properties.Resource_en_US.Msg_RecordChanged;
                        return null;
                    }
                    VCHPreview pv = GetPreviewObj(input.SelectedKey, db.PreviewDB, out msg);

                    if (pv.Result.Status != MOEEnums.ResultStatus.Success)
                    {
                        return null;
                    }

                    saveObj.Header = GetPeoVch(input, pv, db, pv.Hotels[0]);
                    saveObj.Banner = GetDocBanner(input, db);

                    #region Segment/Detail
                    bool isVCH_HTIHTO = da.GetPreferenceEnable(input.SelectedKey.CompanyCode, MOEEnums.CompanyPreference.VCH_HTIHTO);
                    int segNum = 0;
                    foreach (Segment seg in input.SelectedKey.Segments)
                    {
                        segNum++;
                        if (seg.Type == Segment.SegmentType.Hotel)
                        {
                            VCHHotel htl = pv.Hotels.Find(c => c.SegNum == seg.Data.SegNum);
                            tbPEOVCHSEG vchSeg = GetSegmentByHotel(input, htl, db, isVCH_HTIHTO, segNum);
                            saveObj.Segments.Add(vchSeg);

                            tbPEOVCHDETAIL vchDtl = GetDetailByHotel(input, pv, htl, db, segNum);
                            saveObj.Details.Add(vchDtl);
                        }
                        else if (seg.Type == Segment.SegmentType.Other)
                        {
                            VCHOther oth = pv.Others.Find(c => c.SegNum == seg.Data.SegNum);
                            tbPEOVCHSEG vchSeg = GetSegmentByOther(input, oth, db, segNum);
                            saveObj.Segments.Add(vchSeg);

                            tbPEOVCHDETAIL vchDtl = GetDetailByOther(input, oth, db, segNum);
                            saveObj.Details.Add(vchDtl);
                        }
                    }
                    #endregion

                    #region Set Passenger
                    int seqNum = 0;
                    foreach (VCHPassenger pax in pv.Passengers)
                    {
                        seqNum++;
                        tbPEOINPAX inpax = GetPassenger(db, pax, seqNum);
                        saveObj.Passengers.Add(inpax);
                    }
                    #endregion

                    CalculateAmount(input, ref saveObj);

                    saveObj.History = GetHistory(input, saveObj.Header);
                    return saveObj;
                }
            }
            msg = Properties.Resource_en_US.Invalid_Data;
            return null;
        }

        private tbPEOVCH GetPeoVch(VCHCreate input, VCHPreview preview, VCHCreateDB db, VCHHotel htl)
        {
            tbPEOVCH vch = new tbPEOVCH();
            vch.COMPANYCODE = db.CompanyCode;
            vch.VCHNUM = db.VouchNum;
            vch.BKGREF = db.BkgRef;
            vch.STAFFCODE = db.Master.STAFFCODE;
            vch.TEAMCODE = db.Master.TEAMCODE;
            vch.SUPPCODE = htl.SuppCode;
            vch.SUPPNAME = htl.SuppName;

            vch.PHONE = htl.HtlTelephone;
            vch.FAX = htl.HtlFax;

            vch.HOTELCODE = htl.HtlCode;
            vch.HOTELNAME = GetHotelName(htl);

            vch.HOTELADDR = htl.HtlAddress;
            vch.HOTELTEL = htl.HtlTelephone;
            vch.HOTELFAX = htl.HtlFax;
            vch.CFMBY = MOEFormats.ToTrim(htl.HtlCfmBy);
            vch.FORMPAY = string.Empty;
            vch.HKDAMT = 0;
            vch.VCHREF = String.Format("{0}/{1}/{2}", db.Mis.COD1, db.Mis.COD2, db.Mis.COD3);
            vch.COD1 = db.Mis.COD1;
            vch.COD2 = db.Mis.COD2;
            vch.RATENAME = htl.HtlRateName;
            vch.RMK1 = MOEFormats.Left(20, "-", input.AirRemark1);
            vch.RMK2 = MOEFormats.Left(20, "-", input.AirRemark2);
            vch.RMK3 = MOEFormats.Left(20, "-", input.HotelRemark1);
            vch.RMK4 = MOEFormats.Left(20, "-", input.HotelRemark2);
            vch.RMK5 = MOEFormats.Left(20, "-", input.OtherRemark1);
            vch.RMK6 = MOEFormats.Left(20, "-", input.OtherRemark2);

            vch.SPCL_REQUEST = htl.HtlSpecialRequest;

            vch.ARRDATE = htl.HtlArrDate;
            vch.DEPARTDATE = htl.HtlDapartDate;
            vch.COSTCURR = db.Company.LOCALCURR;
            vch.CostExcludeTax = 0;
            vch.TAXCURR = db.Company.LOCALCURR;
            vch.TAXAMT = 0;
            vch.staffname = String.Format("{0}/{1}", db.Staff.STAFFNAME, db.Staff.TEAMCODE);
            vch.stafftel = MOEFormats.ToTrim(db.Staff.PHONE).Replace("(", " ").Replace(")", " ");
            vch.vchremark = String.Format("{0}/{1}", htl.HtlRemark1, htl.HtlRemark2);
            vch.hotelref = htl.HtlReference;
            vch.Attn = string.Empty;
            vch.VCHTYPE = input.SelectedKey.VoucherType.ToUpper();
            vch.REMARK2 = MOEFormats.Left(20, db.Master.FIRSTPAX);
            vch.ARRDTL = String.Format("{0}/{1}", htl.HtlETA, htl.HtlArrTime);
            vch.DEPDTL = String.Format("{0}/{1}", htl.HtlETD, htl.HtlDapartTime);
            vch.OurRef = htl.OurRef;
            vch.ADULTNUM = preview.Adult;
            vch.CHILDNUM = preview.Child;
            vch.Payment = preview.Payment;

            vch.CREATEON = db.ServerDateTime;
            vch.CREATEBY = input.SelectedKey.UserCode;
            vch.UPDATEON = vch.CREATEON;
            vch.UPDATEBY = input.SelectedKey.UserCode;

            return vch;
        }
        private tbDocumentBanner GetDocBanner(VCHCreate input, VCHCreateDB db)
        {
            tbDocumentBanner tb = new tbDocumentBanner();

            tb.companycode = input.SelectedKey.CompanyCode;
            tb.bkgref = input.SelectedKey.BkgRef;
            tb.docnum = db.VouchNum;
            tb.bannertype = input.SelectedKey.CompanyCode;
            tb.createon = db.ServerDateTime;
            tb.createby = input.SelectedKey.UserCode;
            tb.updateon = tb.createon;
            tb.updateby = tb.createby;

            return tb;
        }

        private tbPEOVCHSEG GetSegmentByHotel(VCHCreate input, VCHHotel htl, VCHCreateDB db, bool isVCH_HTIHTO, int segNum)
        {
            tbPEOVCHSEG seg = new tbPEOVCHSEG();
            seg.COMPANYCODE = db.CompanyCode;
            seg.VCHNUM = db.VouchNum;
            seg.SEGNUM = MOEFormats.GetSeqNum(segNum);

            seg.SERVTYPE = MOEEnums.ServerType.HTL.ToString();
            if (isVCH_HTIHTO)
            {
                if (db.Company.COUNTRYCODE.ToUpper().Trim() == htl.HtlCountryCode.ToUpper().Trim())
                {
                    seg.SERVTYPE = MOEEnums.ServerType.HTI.ToString();
                }
                else
                {
                    seg.SERVTYPE = MOEEnums.ServerType.HTO.ToString();
                }
            }

            seg.SERVNAME = MOEFormats.Left(100, string.Format("{0},{1},{2}", GetHotelName(htl), htl.CityName, htl.CountryName));
            seg.SERVDESC = string.Format("{0}/{1}", MOEFormats.ToDate(htl.HtlArrDate, MOEEnums.DateFormats.MMMddyy), MOEFormats.ToDate(htl.HtlDapartDate, MOEEnums.DateFormats.MMMddyy));

            seg.CREATEON = db.ServerDateTime;
            seg.CREATEBY = input.SelectedKey.UserCode;
            return seg;
        }
        private tbPEOVCHSEG GetSegmentByOther(VCHCreate input, VCHOther oth, VCHCreateDB db, int segNum)
        {
            segNum++;
            tbPEOVCHSEG seg = new tbPEOVCHSEG();
            seg.COMPANYCODE = db.CompanyCode;
            seg.VCHNUM = db.VouchNum;
            seg.SEGNUM = MOEFormats.GetSeqNum(segNum);
            seg.SEGTYPE = oth.SegType;

            seg.SERVTYPE = MOEEnums.ServerType.OTH.ToString();
            seg.SERVNAME = string.Empty;
            seg.SERVDESC = oth.MasterDesc;

            seg.CREATEON = db.ServerDateTime;
            seg.CREATEBY = input.SelectedKey.UserCode;
            return seg;
        }
        private tbPEOVCHDETAIL GetDetailByHotel(VCHCreate input, VCHPreview pv, VCHHotel htl, VCHCreateDB db, int segNum)
        {
            sqlVchHotel d = pv.HtlServiceDetails.Find(c => c.SEGNUM == htl.SegNum);
            tbPEOVCHDETAIL dtl = new tbPEOVCHDETAIL();
            dtl.COMPANYCODE = db.CompanyCode;
            dtl.VCHNUM = db.VouchNum;
            dtl.SEGNUM = MOEFormats.GetSeqNum(segNum);
            dtl.SEQNUM = MOEFormats.GetSeqNum(1);
            dtl.DETLTYPE = MOEEnums.PaxType.ADT.ToString();
            dtl.QTY = d.QTYs;
            dtl.RMNTS = d.RMNTS;
            dtl.RATENAME = d.RATENAME;
            dtl.ITEMNAME = d.ITEMNAME;

            string itemType = d.itemtype.Trim().ToUpper();
            if (itemType != "M" && itemType != "B" && itemType != "T" && itemType != "O")
            {
                if (d.ARRDATE != DateTime.MinValue && d.DEPARTDATE != DateTime.MinValue)
                {
                    string tmpItemName = MOEFormats.DateFromTo(d.ARRDATE, d.DEPARTDATE, MOEEnums.DateFormats.MMMddyy);
                    if (!string.IsNullOrEmpty(tmpItemName))
                    {
                        dtl.ITEMNAME += MOEFormats.Left(150, String.Format("({0})", tmpItemName));
                    }
                }
            }

            dtl.COSTCURR = d.CURR;
            dtl.COSTAMT = d.AMT;
            dtl.TAXCURR = d.TAXCURR;
            dtl.TAXAMT = d.TAXAMT;
            dtl.HKDAMT = BOCurr.CalculateAmount(db.CompanyCode, db.Company.LOCALCURR, d.CURR, d.AMT + d.TAXAMT);

            dtl.CREATEON = db.ServerDateTime;
            dtl.CREATEBY = input.SelectedKey.UserCode;
            return dtl;
        }
        private tbPEOVCHDETAIL GetDetailByOther(VCHCreate input, VCHOther oth, VCHCreateDB db, int segNum)
        {
            tbPEOOTHERDETAIL t = db.PreviewDB.PeoOtherDetails.Find(c => c.SEGNUM == oth.SegNum && c.SEQTYPE == MOEEnums.SeqType.COST.ToString());
            tbPEOVCHDETAIL dtl = new tbPEOVCHDETAIL();
            dtl.COMPANYCODE = db.CompanyCode;
            dtl.VCHNUM = db.VouchNum;
            dtl.SEGNUM = MOEFormats.GetSeqNum(segNum);
            dtl.SEQNUM = MOEFormats.GetSeqNum(1);
            dtl.QTY = t.QTY;
            dtl.RMNTS = 1;
            dtl.RATENAME = string.Empty;
            dtl.ITEMNAME = t.AGETYPE;

            dtl.COSTCURR = t.CURR;
            dtl.COSTAMT = t.AMT;
            dtl.TAXCURR = t.TAXCURR;
            dtl.TAXAMT = t.TAXAMT;
            dtl.HKDAMT = BOCurr.CalculateAmount(db.CompanyCode, db.Company.LOCALCURR, t.CURR, t.AMT + t.TAXAMT);

            dtl.CREATEON = db.ServerDateTime;
            dtl.CREATEBY = input.SelectedKey.UserCode;
            return dtl;
        }
        private tbPEOINPAX GetPassenger(VCHCreateDB db, VCHPassenger pax, int seqNum)
        {
            tbPEOINPAX inpax = new tbPEOINPAX();
            inpax.COMPANYCODE = db.CompanyCode;
            inpax.INVNUM = db.VouchNum;
            inpax.SEQNUM = MOEFormats.GetSeqNum(seqNum);
            inpax.PAXSEQ = (Int16)seqNum;
            inpax.PAXNAME = String.Format("{0}/{1}/{2}/{3}", pax.PaxLName, pax.PaxFName, pax.PaxTitle, pax.PaxType);
            inpax.INVTYPE = "V";
            return inpax;
        }
        private void CalculateAmount(VCHCreate input, ref VCHSaveDB saveObj)
        {
            decimal costExcludeTax = 0;
            decimal taxAMT = 0;
            decimal hkdAMT = 0;
            bool isHTL_SUM = da.GetPreferenceEnable(input.SelectedKey.CompanyCode, MOEEnums.CompanyPreference.HTL_SUM);
            if (isHTL_SUM)
            {
                foreach (tbPEOVCHDETAIL dtl in saveObj.Details)
                {
                    tbPEOVCHSEG seg = saveObj.Segments.Find(c => c.SEGNUM == dtl.SEGNUM);
                    if (seg.SERVTYPE == MOEEnums.ServerType.HTL.ToString())
                    {
                        costExcludeTax = dtl.COSTAMT;
                        taxAMT = dtl.TAXAMT;
                        hkdAMT = (dtl.COSTAMT + dtl.TAXAMT);
                    }
                    else
                    {
                        costExcludeTax = dtl.QTY * dtl.COSTAMT;
                        taxAMT = dtl.QTY * dtl.TAXAMT;
                        hkdAMT = dtl.QTY * (dtl.COSTAMT + dtl.TAXAMT);
                    }

                    saveObj.Header.CostExcludeTax += BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, costExcludeTax);
                    saveObj.Header.TAXAMT += BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, taxAMT);
                    saveObj.Header.HKDAMT = BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, hkdAMT);
                }
            }
            else
            {
                foreach (tbPEOVCHDETAIL dtl in saveObj.Details)
                {
                    tbPEOVCHSEG seg = saveObj.Segments.Find(c => c.SEGNUM == dtl.SEGNUM);
                    if (seg.SERVTYPE == MOEEnums.ServerType.HTL.ToString() || seg.SERVTYPE == MOEEnums.ServerType.HTO.ToString() || seg.SERVTYPE == MOEEnums.ServerType.HTI.ToString())
                    {
                        costExcludeTax = dtl.QTY * dtl.RMNTS * dtl.COSTAMT;
                        taxAMT = dtl.QTY * dtl.RMNTS * dtl.TAXAMT;
                        hkdAMT = dtl.QTY * dtl.RMNTS * (dtl.COSTAMT + dtl.TAXAMT);
                    }
                    else
                    {
                        costExcludeTax = dtl.QTY * dtl.COSTAMT;
                        taxAMT = dtl.QTY * dtl.TAXAMT;
                        hkdAMT = dtl.QTY * (dtl.COSTAMT + dtl.TAXAMT);
                    }

                    saveObj.Header.CostExcludeTax += BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, costExcludeTax);
                    saveObj.Header.TAXAMT += BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, taxAMT);
                    saveObj.Header.HKDAMT = BOCurr.CalculateAmount(input.SelectedKey.CompanyCode, input.SelectedKey.CurrCode, dtl.COSTCURR, hkdAMT);
                }
            }
            saveObj.Header.COSTAMT = saveObj.Header.CostExcludeTax + saveObj.Header.TAXAMT;

        }
        private tbPEONYREF GetHistory(VCHCreate input, tbPEOVCH vch)
        {
            tbPEONYREF his = new tbPEONYREF();

            his.COMPANYCODE = vch.COMPANYCODE;
            his.BKGREF = vch.BKGREF;
            his.ACTIONCODE = MOEEnums.DocType.VCH.ToString();
            his.CLTCODE = string.Empty;
            his.SUPPCODE = vch.SUPPCODE;
            his.DocNum = vch.VCHNUM;
            his.STAFFCODE = input.SelectedKey.UserCode;
            his.TEAMCODE = input.SelectedKey.TeamCode;

            his.SellCurr = vch.COSTCURR;
            his.SellAmt = 0;
            his.SellTaxCurr = vch.COSTCURR;
            his.SellTaxAmt = 0;
            his.CostCurr = vch.COSTCURR;
            his.CostAmt = vch.CostExcludeTax;
            his.CostTaxCurr = vch.TAXCURR;
            his.CostTaxAmt = vch.TAXAMT;
            //transferdate = Me.txt_bacdat.Text.Trim & " " & Now.ToLongTimeString()
            // If Date.Compare(CDate(transferdate), Date.Today) < 1 Then
            //    myPeonyRefDescForBackDate = " + ' - Back Date to " + CDate(transferdate).ToShortDateString() + "' "
            //End If
            //strSql &= ", case when gstamt is null or gstamt = 0 then "
            //strSql &= "'(Cost ' + isnull(costCURR,'') + convert(varchar(16),isnull(costexcludetax,0)) +' + Tax '+isnull(TAXCURR,'') +convert(varchar(16),isnull(TAXAMT,0)) +' / '+RTRIM(ISNULL(suppcode,''))+')'" & myPeonyRefDescForBackDate
            //strSql &= "when gstamt >0 then "
            //strSql &= "'(Cost ' + isnull(costCURR,'') + convert(varchar(16),isnull(costexcludetax,0)) +' + Tax '+isnull(TAXCURR,'') +convert(varchar(16),isnull(TAXAMT,0)) + ' + GST '+isnull(costcurr,'') +convert(varchar(16),isnull(gstamt,0)) +' / '+RTRIM(ISNULL(suppcode,''))+')'" & myPeonyRefDescForBackDate
            //strSql &= " end"
            string strBackDate = string.Empty;
            if (input.SelectedKey.BackDate.CompareTo(DateTime.Today) < 1)
            {
                strBackDate = string.Format("- Back Date to {0}", input.SelectedKey.BackDate.ToShortDateString());
            }
            string strGSTAmt = string.Empty;
            if (vch.gstamt > 0)
            {
                strGSTAmt = string.Format(" GST {0} {1}", vch.COSTCURR, vch.gstamt);
            }
            his.Description = string.Format("(Cost {0} {1} + Tax {2} {3}{4} / {5}) {6}", vch.COSTCURR, MOEFormats.ToAmount(vch.CostExcludeTax), vch.TAXCURR, MOEFormats.ToAmount(vch.TAXAMT), strGSTAmt, vch.SUPPCODE, strBackDate);
            his.CREATEON = vch.UPDATEON;
            his.CREATEBY = vch.UPDATEBY;

            return his;
        }
        private string GetHotelName(VCHHotel htl)
        {
            string name = string.Empty;
            if (string.IsNullOrEmpty(htl.HtlName_CN))
            {
                name = htl.HtlName;
            }
            else
            {
                name = string.Format("{0}/{1}", htl.HtlName, htl.HtlName_CN);
            }
            return name;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (Conn != null)
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        #endregion
    }
}
