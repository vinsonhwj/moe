﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.BLL.Document.Comm
{
    public class Comm
    {
        public static string FormatDateDDMMMYYYY(object InDate)
        {
            if (InDate == null)
            {
                return "";
            }
            if (InDate is DateTime)
            {
                DateTime d = Convert.ToDateTime(InDate);

                return d.ToString("DDMMMYYYY");
            }

            DateTime d1 = DateTime.MinValue;
            DateTime.TryParse(InDate.ToString(), out d1);
            if (d1 == DateTime.MinValue)
            {
                return string.Empty;
            }
            else
            {
                return d1.ToString("DDMMMYYYY");
            }

        }

        public static string FormatDateMMDDCCYYYY_Trim(DateTime BeginDate, DateTime EndDate)
        {

            if (BeginDate == DateTime.MinValue && EndDate == DateTime.MinValue)
            {
                return " ";
            }

            if (EndDate == DateTime.MinValue)
            {
                return FormatDateMMDDCCYY(BeginDate);
            }

            if (BeginDate == DateTime.MinValue)
            {
                return FormatDateMMDDCCYY(EndDate);
            }

            string sBeginDate = FormatDateMMDDCCYY(BeginDate);
            string sEndDate = FormatDateMMDDCCYY(EndDate);

            if (BeginDate.Year == EndDate.Year)
            {
                return FormatDateMMDDCCYY(BeginDate).Substring(0, 6).Replace(" ", "") + "-" + FormatDateMMDDCCYY(EndDate).Replace(" ", "");
            }
            else
            {
                return FormatDateMMDDCCYY(BeginDate).Replace(" ", "") + "-" + FormatDateMMDDCCYY(EndDate).Replace(" ", "");
            }
        }

        //get Apr 02, 03
        public static string FormatDateMMDDCCYY(DateTime InDate)
        {
            if (InDate == DateTime.MinValue)
                return string.Empty;


            string strReturn = GetMonthName(InDate.Month);
            if (InDate.Day.ToString().Length == 1)
            {
                strReturn = strReturn + " 0";
            }
            else
            {
                strReturn = strReturn + " ";
            }
            strReturn = strReturn + InDate.Day.ToString() + ", " + InDate.Year.ToString().Substring(2, 2);
            return strReturn;
        }

        public static string GetMonthName(int Monthnum)
        {
            string strMonthName = null;
            switch (Monthnum)
            {
                case 1:
                    strMonthName = "Jan";
                    break;
                case 2:
                    strMonthName = "Feb";
                    break;
                case 3:
                    strMonthName = "Mar";
                    break;
                case 4:
                    strMonthName = "Apr";
                    break;
                case 5:
                    strMonthName = "May";
                    break;
                case 6:
                    strMonthName = "Jun";
                    break;
                case 7:
                    strMonthName = "Jul";
                    break;
                case 8:
                    strMonthName = "Aug";
                    break;
                case 9:
                    strMonthName = "Sep";
                    break;
                case 10:
                    strMonthName = "Oct";
                    break;
                case 11:
                    strMonthName = "Nov";
                    break;
                case 12:
                    strMonthName = "Dec";
                    break;
                default:
                    strMonthName = "";
                    break;
            }
            return strMonthName;
        }


    }
}
