﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DAL.Transfer;
using Westminster.MOE.Entity.BLLEntity.Transfer;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Transfer;
using Westminster.MOE.Lib;
using Westminster.MOE.Lib.Configuration;

namespace Westminster.MOE.BLL.Transfer
{
    public class AutoDocument
    {
        public Config Config { get; private set; }
        private AutoInvoice autoInv { get; set; }
        private IAutoDocument da;
        //private IConnection Conn { get; set; }

        public AutoDocument(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos]))
        { }
        public AutoDocument(Config config, IConnection connection)
            : this(config, new DAAutoDocument(connection, config.WebServiceList[MOEEnums.BindingName.IURSoap]))
        { }
        public AutoDocument(Config config, IAutoDocument autoDocument)
        {
            this.Config = config;
            da = autoDocument;
        }

        public RetrieveIURBooking RetrieveBooking_IUR(RetrieveBooking_IURParam param)
        {
            RetrieveIURBooking booking = new RetrieveIURBooking();
            try
            {
                booking.CompanyCode = param.CompanyCode;
                booking.PNR = param.PNR;
                booking.InvType = param.InvType;

                switch (param.GDSType)
                {
                    case GDS_Types.Abacus:
                        booking.SourceSystem = "IUR";
                        break;
                    case GDS_Types.Galileo:
                        booking.SourceSystem = "GLO";
                        break;
                    case GDS_Types.Amadeus:
                        booking.SourceSystem = "AIR";
                        break;
                    case GDS_Types.TravelSky:
                        booking.SourceSystem = "TS";
                        break;
                    case GDS_Types.Worldspan:
                        booking.SourceSystem = "WORLDSPAN";
                        break;
                }

                Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur = null;
                bool isValid = ValidateData(booking, param.Tickets, param.CltCode, param.TSANumber, out iur);

                if (isValid)
                {
                    RetrieveData(param, iur, ref booking);
                }

                return booking;
            }
            catch (Exception ex)
            {
                booking.Message = AutoBookingMessages.Unknown_Error;
                booking.Exception = ex.Message;
                return booking;
            }
        }
        public AutoBookingResponse AutoBooking(AutoBookingParam param)// RetrieveIURBooking booking, eMosParameter eMosParam, List<AirlineSegment> airSegment4Invoice, string invrmk, string staffcode, string teamcode, bool isAllowInvoice, bool isAllowEInvoice, string recipient, Invoice_Types invType)
        {
            //Log.Info(result);

            AutoBookingResponse response = new AutoBookingResponse();
            try
            {
                eMosBooking myeMosBooking = new eMosBooking(param.Booking.CompanyCode, param.Booking.Bkgref, true);
                IUR2eMos_Object(param, ref myeMosBooking);
                SyncDataIUR2eMos(param, myeMosBooking);

                if (param.Booking.Status == 1 && param.IsAllowInvoice)
                {
                    //string recipient = GetRecipient(myeMosBooking, param.IsAllowEInvoice, param.Recipient);
                    response.Invnum = AutoInvoice(myeMosBooking, param, ref response);//.AirSegment4Invoice, param.IsAllowEInvoice, param.InvRemark, param.StaffCode, param.TeamCode, recipient, response, param.InvType, param.eMosParam);
                }

                foreach (eMosAirlineSegment myeMosAirlineSegment in myeMosBooking.AirlineSegments)
                {
                    //myeMosAirlineSegment.UO_PEOAIR.Insert(myConn, myTran);
                    da.CallRecal_Air(param.Booking.CompanyCode, param.Booking.Bkgref, myeMosAirlineSegment.PeoAir.SegNum);
                }
                da.CallPEOMSTRsummary_trigger(param.Booking.CompanyCode, param.Booking.Bkgref);

                response.Status = param.Booking.Status;
                //response.Message = param.Booking.Message;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = 9;
                response.Exception = ex.Message;
                response.Message = AutoBookingMessages.Unknown_Error;
                //Log.Error(ex);
                return response;
            }
        }
        //public string AutoInvoice(eMosBooking booking, List<AirlineSegment> airSegment4Invoice, bool isAllowEInvoice, string invrmk, string staffcode, string teamcode, string recipient, AutoBookingResponse response, Invoice_Types invType, eMosParameter eMosParam)
        public string AutoInvoice(eMosBooking booking, AutoBookingParam param, ref AutoBookingResponse response)
        {
            string recipient = GetRecipient(booking, param.IsAllowEInvoice, param.Recipient);
            string invNum = string.Empty;
            autoInv = new AutoInvoice(Config);
            autoInv.InvType = param.InvType;
            autoInv.TSAno = param.eMosParam != null ? param.eMosParam.TSAno : string.Empty;
            autoInv.eMosParam = param.eMosParam;
            invNum = autoInv.IssueAutoInvoice4IUR(booking, param.InvRemark, param.StaffCode, param.TeamCode, param.AirSegment4Invoice);
            try
            {
                if (param.IsAllowEInvoice == true && invNum != "")
                {
                    SendEInvoice(booking, recipient, invNum);
                }
            }
            catch (Exception ex)
            {
                response.Message = AutoBookingMessages.EInvoice_Exception;
                response.Exception = ex.Message;
                // Log.Error(ex);
            }
            return invNum;
        }

        //public static TsfrHotel TransferAdcom2eMos(string teamcode, string loginuser, string txno, string cltcode, string cltname, string cltaddr, string staffcode, string companycode, string type, string bkgref, string localcurrcode)
        //{
        //    TsfrHotel myBooking = new TsfrHotel();
        //    //check whether the adcom no is existed 
        //    //if (BO_PeoMstr.GetObject(companycode, txno) != null)
        //    //{
        //    //    //sync peohotel_to_Tsfr
        //    //    BO_Peohotel_to_Tsfr.GetDataSet(teamcode, loginuser, txno, cltcode, cltname, cltaddr, staffcode, companycode, type, bkgref, localcurrcode);
        //    //    //get Tsfr pax htl htlDetail
        //    //    DO_PeoPax.UOList_PeoPax eMos4TsfrPaxList = GeteMos4TsfrPaxList(companycode, bkgref);
        //    //    DO_PeoHtl.UOList_PeoHtl eMos4TsfrHtlList = GeteMos4TsfrHtlList(companycode, bkgref);
        //    //    eMosDbLibrary.DataObject.eMos4Tsfr.DO_PeoHtlDetail.UOList_PeoHtlDetail eMos4TsfrHtlDetailList = GeteMos4TsfrHtlDetailList(companycode, bkgref);
        //    //    //Tsfr Object to Emos Object
        //    //    eMos4TsfrPax2eMosPax(myBooking, eMos4TsfrPaxList, txno);
        //    //    eMos4TsfrHtl2eMosHtl(myBooking, eMos4TsfrHtlList);
        //    //    eMos4TsfrHtlDetail2eMosHtlDetail(myBooking, eMos4TsfrHtlDetailList);
        //    //    //sync
        //    //    Sync2eMos(myBooking);
        //    //}
        //    return myBooking;
        //}

        #region Private Member
        private bool ValidateData(RetrieveIURBooking booking, string[] tickets, string cltCode, string TSANumber, out Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur)
        {
            iur = null;
            booking.Status = 2;
            if (string.IsNullOrEmpty(TSANumber) == false && string.IsNullOrEmpty(booking.PNR))
            {
                tbPEOMISs myUOList_PEOMIS = da.GetPEOMISList(booking.CompanyCode, TSANumber);
                if (myUOList_PEOMIS.Count > 0)
                {
                    booking.Bkgref = myUOList_PEOMIS[0].BKGREF;
                }
            }

            tbPEOMSTR master = new tbPEOMSTR();
            master.COMPANYCODE = booking.CompanyCode;
            if (!string.IsNullOrEmpty(booking.PNR))
            {
                master.PNR = booking.PNR;
            }
            else
            {
                master.BKGREF = booking.Bkgref;
            }
            master.SOURCESYSTEM = booking.SourceSystem;

            iur = da.GetIURObjectsByPNR(master.COMPANYCODE, master.PNR, master.BKGREF, booking.SourceSystem, DateTime.Now.AddMonths(-6));
            if (iur.PEOMSTR != null)
            {
                booking.PNR = iur.PEOMSTR.PNR;
            }
            if (iur.PEOMSTR == null || iur.PEOMSTR.PNR.Trim() == string.Empty)
            {
                booking.Message = AutoBookingMessages.Warning_Pnr_Not_Found;
                //Log.Info(string.Format(AutoBookingMessages.Warning_Pnr_Not_Found.ToString() + ":PNR({0})", booking.PNR.Trim()));
                return false;
            }
            //tbPEOMSTRs masterLst =    da.GetMasterListByObject(master,DateTime.Now.AddMonths(-6));

            //if (masterLst.Count > 0)
            //{
            //    booking.PNR = masterLst[0].PNR;
            //}
            //if (booking.PNR.Trim() == "" || masterLst.Count == 0)
            //{
            //    booking.Message = AutoBookingMessages.Warning_Pnr_Not_Found;
            //    //Log.Info(string.Format(AutoBookingMessages.Warning_Pnr_Not_Found.ToString() + ":PNR({0})", booking.PNR.Trim()));
            //    return;
            //}
            //else
            //{
            //    booking.Bkgref = masterLst[0].BKGREF;
            //}

            //booking.Peomstr.CLTODE = cltCode;
            tbCustomer MOEcustomer = da.GetCustomer(booking.CompanyCode, cltCode);

            booking.Customer = ConvertMOEtoEMOS.ToEMOS_Customer(MOEcustomer);

            if (booking.Customer == null)
            {
                booking.Message = AutoBookingMessages.Error_Client_Not_Found;
                //Log.Info(string.Format(AutoBookingMessages.Error_Client_Not_Found.ToString() + ":CltCode({0})", cltCode.Trim()));
                return false;
            }
            else
            {
                booking.Peomstr.CLTNAME = booking.Customer.CLTNAME;
            }

            if (tickets != null && tickets.Length > 0)
            {
                foreach (string ticket in tickets)
                {
                    if (ticket.Trim() != string.Empty)
                    {
                        bool exist = false;
                        foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT tkt in iur.TicketList)
                        {
                            if (tkt.Ticket == ticket.Trim() && tkt.CreateOn > DateTime.Now.AddDays(-1))
                            {
                                exist = true;
                                break;
                            }
                        }
                        if (!exist)
                        {
                            booking.Message = AutoBookingMessages.Warning_Ticket_Not_Found;
                            //Log.Info(string.Format(AutoBookingMessages.Warning_Ticket_Not_Found.ToString() + ":Ticket({0})", ticket.Trim()));
                            return false;
                        }

                        bool isTicketInoviced = false;
                        if (isTicketInoviced == false)
                        {
                            isTicketInoviced = da.ExistUnVoidInvoicedTickets(booking.CompanyCode, booking.Bkgref, ticket.Trim());
                        }
                        if (isTicketInoviced == false)
                        {
                            isTicketInoviced = da.ExistUnVoidInvoicedTicketRefs(booking.CompanyCode, booking.Bkgref, ticket.Trim());
                        }
                        if (isTicketInoviced)
                        {
                            booking.Message = AutoBookingMessages.Error_Ticket_Invoiced;
                            //Log.Info(string.Format(AutoBookingMessages.Error_Ticket_Invoiced.ToString() + ":Ticket({0})", ticket.Trim()));
                            return false;
                        }
                    }
                }
            }

            booking.Status = 1;
            return true;

        }
        private void RetrieveData(RetrieveBooking_IURParam param, Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, ref RetrieveIURBooking booking)
        {
            RetrieveMstr(iur, param.TourCode, param.TSANumber, ref booking);
            RetrieveAir(iur, ref booking);
            RetrievePax(iur, ref booking);
            RetrieveTicket(iur, param.Tickets, ref booking);
            //if (!string.IsNullOrEmpty(param.ADComTxNum))
            //{
            //TsfrHotel hotel = eMosLibrary.Booking.TsfrHotel.TransferAdcom2eMos(booking.Peomstr.TEAMCODE, loginUser, adcomTxno, booking.Peomstr.CLTODE, booking.Peomstr.CLTNAME, booking.Peomstr.CLTADDR,
            //                                                loginUser, booking.Peomstr.COMPANYCODE, "CHECK", booking.Peomstr.BKGREF, booking.Peomstr.INVCURR);
            //RetrieveHotel(booking, hotel);
            //}

        }

        private void RetrieveMstr(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string tourCode, string TSANumber, ref RetrieveIURBooking booking)
        {
            UO_PeoMstr myUO_PeoMstr = new UO_PeoMstr();
            myUO_PeoMstr = ConvertMOEtoEMOS.ToEMOS_PeoMstr(iur.PEOMSTR);
            myUO_PeoMstr.TOURCODE = tourCode;

            booking.Peomstr = myUO_PeoMstr;

            if (string.IsNullOrEmpty(booking.PNR) && !string.IsNullOrEmpty(TSANumber))
            {
                tbPEOMIS mis = da.GetPEOMIS(booking.Bkgref, booking.CompanyCode, "00001");
                if (mis != null)
                {
                    booking.PNR = mis.COD1;
                }
            }

            if (booking.Customer != null)
            {
                booking.Peomstr.CLTODE = booking.Customer.CLTCODE;
                booking.Peomstr.CLTNAME = booking.Customer.CLTNAME;
                booking.Peomstr.CONTACTPERSON = ((booking.Customer.CONTACT1 == null) ? string.Empty : booking.Customer.CONTACT1);
            }

            booking.Bkgref = myUO_PeoMstr.BKGREF;
        }
        private void RetrieveAir(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, ref RetrieveIURBooking booking)
        {
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR air in iur.AirList)
            {
                AirlineSegment airSegment = new AirlineSegment();
                airSegment.UO_PEOAIR = ConvertMOEtoEMOS.ToEMOS_PeoAir(air);

                foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL airDt in iur.AirDetailList)
                {
                    if (air.CompanyCode == airDt.CompanyCode && air.BkgRef == airDt.BkgRef && air.SegNum == airDt.SegNum)
                    {
                        airSegment.UOList_PEOAIRDETAIL.Add(ConvertMOEtoEMOS.ToEMOS_PeoAirDetail(airDt));
                    }
                }

                booking.AirlineSegmentsList.Add(airSegment);
            }
        }
        private void RetrievePax(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, ref RetrieveIURBooking booking)
        {
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX pax in iur.PaxList)
            {
                booking.PassengersList.Add(ConvertMOEtoEMOS.ToEMOS_PeoPax(pax));
            }
        }
        private void RetrieveTicket(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string[] tickets, ref RetrieveIURBooking booking)
        {
            if (tickets != null && tickets.Length > 0)
            {
                foreach (string ticket in tickets)
                {
                    if (!string.IsNullOrEmpty(ticket))
                    {
                        foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT tkt in iur.TicketList)
                        {
                            if (tkt.Ticket == ticket)
                            {
                                TicketSegment ticketSegment = new TicketSegment();

                                ticketSegment.UO_PeoTkt = ConvertMOEtoEMOS.ToEMOS_PeoTkt(tkt);

                                foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAIL tktdt in iur.TicketDetailList)
                                {
                                    if (tkt.Ticket == tktdt.Ticket)
                                    {
                                        ticketSegment.UOList_PeoTktDetail.Add(ConvertMOEtoEMOS.ToEMOS_PeoTktDt(tktdt));
                                    }
                                }
                                booking.TicketSegmentsList.Add(ticketSegment);
                            }
                        }
                    }
                }
            }
        }

        private void IUR2eMos_Object(AutoBookingParam param, ref eMosBooking myeMosBooking)
        {
            IUR2eMos_Mstr(param.Booking, param.StaffCode, param.TeamCode, ref myeMosBooking);
            IUR2eMos_Mis(param.Booking, param.eMosParam, ref myeMosBooking);
            IUR2eMos_Air(param.Booking, ref myeMosBooking);
            IUR2eMos_Pax(param.Booking, ref myeMosBooking);
            IUR2eMos_VoidTicket(param.Booking, ref myeMosBooking);
            IUR2eMos_Ticket(param.Booking, param.StaffCode, ref myeMosBooking);
            //IUR2eMos_Hotel(booking, myeMosBooking);
            IUR2eMos_Other(param.eMosParam, ref myeMosBooking);
        }
        private void SyncDataIUR2eMos(AutoBookingParam param, eMosBooking myeMosBooking)
        {
            try
            {
                bool succeed = da.InserteMosObjects(myeMosBooking, param.StaffCode);
                param.Booking.Status = 1;
            }
            catch (Exception ex)
            {
                param.Booking.Status = 0;
                param.Booking.Message = AutoBookingMessages.Unknown_Error;
            }
        }

        private void IUR2eMos_Mstr(RetrieveIURBooking booking, string staffcode, string teamCode, ref eMosBooking myeMosBooking)
        {
            myeMosBooking.PeoMstr.agency_arc_iata_num = booking.Peomstr.agency_arc_iata_num;
            //myeMosBooking.PeoMstr.ANALYSISCODE = booking.Peomstr.BKGREF;
            //myeMosBooking.PeoMstr.ApproveBy = booking.Peomstr.BKGREF;
            //myeMosBooking.PeoMstr.ApproveOn = booking.Peomstr.BKGREF;
            //myeMosBooking.PeoMstr.BookingNo = booking.Peomstr.BKGREF;
            myeMosBooking.PeoMstr.BKGREF = booking.Peomstr.BKGREF;
            myeMosBooking.PeoMstr.CLTADDR = booking.Peomstr.CLTADDR;
            tbCustomer customer = da.GetCustomer(booking.Peomstr.COMPANYCODE, booking.Peomstr.CLTODE);
            if (customer != null)
            {
                myeMosBooking.PeoMstr.CLTNAME = customer.CLTNAME;
            }
            myeMosBooking.PeoMstr.CLTODE = booking.Peomstr.CLTODE;
            myeMosBooking.PeoMstr.COD1 = booking.Peomstr.COD1;
            myeMosBooking.PeoMstr.COD2 = booking.Peomstr.COD2;
            myeMosBooking.PeoMstr.COD3 = booking.Peomstr.COD3;
            myeMosBooking.PeoMstr.COMPANYCODE = booking.Peomstr.COMPANYCODE;
            myeMosBooking.PeoMstr.CONTACTPERSON = booking.Peomstr.CONTACTPERSON;
            myeMosBooking.PeoMstr.CREATEBY = booking.Peomstr.CREATEBY;

            myeMosBooking.PeoMstr.CREATEON = booking.Peomstr.CREATEON;
            myeMosBooking.PeoMstr.CRSSINEIN = booking.Peomstr.CRSSINEIN;
            myeMosBooking.PeoMstr.CRSSINEIN_inv = booking.Peomstr.CRSSINEIN_inv;
            myeMosBooking.PeoMstr.DEADLINE = booking.Peomstr.DEADLINE;
            myeMosBooking.PeoMstr.DEPARTDATE = booking.Peomstr.DEPARTDATE;

            myeMosBooking.PeoMstr.DOCAMT = booking.Peomstr.DOCAMT;
            myeMosBooking.PeoMstr.DOCCOUNT = booking.Peomstr.DOCCOUNT;
            myeMosBooking.PeoMstr.DOCCURR = booking.Peomstr.DOCCURR;
            myeMosBooking.PeoMstr.DOCTAX = booking.Peomstr.DOCTAX;
            myeMosBooking.PeoMstr.DOCTAXCURR = booking.Peomstr.DOCTAXCURR;

            myeMosBooking.PeoMstr.EMAIL = booking.Peomstr.EMAIL;
            myeMosBooking.PeoMstr.FAX = booking.Peomstr.FAX;
            myeMosBooking.PeoMstr.FIRSTPAX = booking.Peomstr.FIRSTPAX;
            myeMosBooking.PeoMstr.INVAMT = booking.Peomstr.INVAMT;
            myeMosBooking.PeoMstr.INVCOUNT = booking.Peomstr.INVCOUNT;

            myeMosBooking.PeoMstr.INVCURR = booking.Peomstr.INVCURR;
            myeMosBooking.PeoMstr.INVTAX = booking.Peomstr.INVTAX;
            myeMosBooking.PeoMstr.INVTAXCURR = booking.Peomstr.INVTAXCURR;
            myeMosBooking.PeoMstr.iss_off_code = booking.Peomstr.iss_off_code;
            //myeMosBooking.PeoMstr.ISTOUR = booking.Peomstr.ISTOUR;

            myeMosBooking.PeoMstr.MASTERPNR = booking.Peomstr.MASTERPNR;
            //myeMosBooking.PeoMstr.MstrRemark = booking.Peomstr.MstrRemark;
            myeMosBooking.PeoMstr.PHONE = booking.Peomstr.PHONE;
            myeMosBooking.PeoMstr.PNR = booking.Peomstr.PNR;
            myeMosBooking.PeoMstr.pseudo = booking.Peomstr.pseudo;

            myeMosBooking.PeoMstr.pseudo_inv = booking.Peomstr.pseudo_inv;
            myeMosBooking.PeoMstr.RMK1 = booking.Peomstr.RMK1;
            myeMosBooking.PeoMstr.RMK2 = booking.Peomstr.RMK2;
            myeMosBooking.PeoMstr.RMK3 = booking.Peomstr.RMK3;
            myeMosBooking.PeoMstr.RMK4 = booking.Peomstr.RMK4;

            //myeMosBooking.PeoMstr.salesperson = booking.Peomstr.salesperson;
            myeMosBooking.PeoMstr.SOURCE = booking.Peomstr.SOURCE;
            //myeMosBooking.PeoMstr.SOURCEREF = booking.Peomstr.SOURCEREF;
            myeMosBooking.PeoMstr.SOURCESYSTEM = booking.Peomstr.SOURCESYSTEM;
            if (!string.IsNullOrEmpty(staffcode))
            {
                myeMosBooking.PeoMstr.STAFFCODE = staffcode;
            }
            else
            {
                myeMosBooking.PeoMstr.STAFFCODE = booking.Peomstr.STAFFCODE;
            }

            if (!string.IsNullOrEmpty(teamCode))
            {
                myeMosBooking.PeoMstr.TEAMCODE = teamCode;
            }
            else
            {
                myeMosBooking.PeoMstr.TEAMCODE = booking.Peomstr.TEAMCODE;
            }
            myeMosBooking.PeoMstr.TOURCODE = booking.Peomstr.TOURCODE;
            myeMosBooking.PeoMstr.TTLCOSTAMT = booking.Peomstr.TTLCOSTAMT;
            myeMosBooking.PeoMstr.TTLCOSTCURR = booking.Peomstr.TTLCOSTCURR;

            myeMosBooking.PeoMstr.TTLCOSTTAX = booking.Peomstr.TTLCOSTTAX;
            myeMosBooking.PeoMstr.TTLCOSTTAXCURR = booking.Peomstr.TTLCOSTTAXCURR;
            myeMosBooking.PeoMstr.TTLSELLAMT = booking.Peomstr.TTLSELLAMT;

            myeMosBooking.PeoMstr.TTLSELLCURR = booking.Peomstr.TTLSELLCURR;
            myeMosBooking.PeoMstr.TTLSELLTAX = booking.Peomstr.TTLSELLTAX;
            myeMosBooking.PeoMstr.TTLSELLTAXCURR = booking.Peomstr.TTLSELLTAXCURR;
            if (!string.IsNullOrEmpty(staffcode))
            {
                myeMosBooking.PeoMstr.UPDATEBY = staffcode;
            }
            else
            {
                myeMosBooking.PeoMstr.UPDATEBY = booking.Peomstr.UPDATEBY;
            }

            myeMosBooking.PeoMstr.UPDATEON = booking.Peomstr.UPDATEON;


            if (!string.IsNullOrEmpty(staffcode))
            {
                myeMosBooking.PeoMstr.UPDATEBY = staffcode;
            }
        }
        private void IUR2eMos_Mis(RetrieveIURBooking booking, eMosParameter eMosParam, ref eMosBooking myeMosBooking)
        {
            if (eMosParam != null && !string.IsNullOrEmpty(eMosParam.TSAno))
            {
                myeMosBooking.PeoMis.BKGREF = booking.Peomstr.BKGREF;
                myeMosBooking.PeoMis.CLTCODE = eMosParam.CltCode;
                myeMosBooking.PeoMis.COD1 = eMosParam.TSAno;
                myeMosBooking.PeoMis.COMPANYCODE = booking.Peomstr.COMPANYCODE;
                myeMosBooking.PeoMis.CREATEBY = booking.Peomstr.CREATEBY;
                myeMosBooking.PeoMis.CREATEON = booking.Peomstr.CREATEON;
                myeMosBooking.PeoMis.SEGNUM = "00001";
            }
        }
        private void IUR2eMos_Air(RetrieveIURBooking booking, ref eMosBooking myeMosBooking)
        {
            tbPEOAIRDETAILs eMosDBFirstDetailList = da.GetFirstAirDetailList(myeMosBooking.CompanyCode, myeMosBooking.Bkgref);

            foreach (AirlineSegment airSeg in booking.AirlineSegmentsList)
            {
                eMosAirlineSegment myeMosAirlineSegment = new eMosAirlineSegment();
                #region eMosAirlineSegment

                myeMosAirlineSegment.PeoAir.CompanyCode = airSeg.UO_PEOAIR.CompanyCode;
                myeMosAirlineSegment.PeoAir.BkgRef = airSeg.UO_PEOAIR.BkgRef;
                myeMosAirlineSegment.PeoAir.SegNum = airSeg.UO_PEOAIR.SegNum;
                myeMosAirlineSegment.PeoAir.Flight = airSeg.UO_PEOAIR.Flight;
                myeMosAirlineSegment.PeoAir.Class = airSeg.UO_PEOAIR.Class;
                myeMosAirlineSegment.PeoAir.DepartCity = airSeg.UO_PEOAIR.DepartCity;
                myeMosAirlineSegment.PeoAir.ArrivalCity = airSeg.UO_PEOAIR.ArrivalCity;
                myeMosAirlineSegment.PeoAir.Status = airSeg.UO_PEOAIR.Status;
                myeMosAirlineSegment.PeoAir.NumOfPax = airSeg.UO_PEOAIR.NumOfPax;
                myeMosAirlineSegment.PeoAir.DepartDATE = airSeg.UO_PEOAIR.DepartDATE;
                myeMosAirlineSegment.PeoAir.DepartTime = airSeg.UO_PEOAIR.DepartTime;
                myeMosAirlineSegment.PeoAir.ArrDATE = airSeg.UO_PEOAIR.ArrDATE;
                myeMosAirlineSegment.PeoAir.ArrTime = airSeg.UO_PEOAIR.ArrTime;
                myeMosAirlineSegment.PeoAir.ElapsedTime = airSeg.UO_PEOAIR.ElapsedTime;
                myeMosAirlineSegment.PeoAir.SSR = airSeg.UO_PEOAIR.SSR;
                myeMosAirlineSegment.PeoAir.Seat = airSeg.UO_PEOAIR.Seat;
                myeMosAirlineSegment.PeoAir.ReasonCode = airSeg.UO_PEOAIR.ReasonCode;
                myeMosAirlineSegment.PeoAir.StopOverNum = airSeg.UO_PEOAIR.StopOverNum;
                myeMosAirlineSegment.PeoAir.StopOverCity = airSeg.UO_PEOAIR.StopOverCity;
                myeMosAirlineSegment.PeoAir.Pcode = airSeg.UO_PEOAIR.Pcode;
                myeMosAirlineSegment.PeoAir.SpecialRQ = airSeg.UO_PEOAIR.SpecialRQ;
                myeMosAirlineSegment.PeoAir.RMK1 = airSeg.UO_PEOAIR.RMK1;
                myeMosAirlineSegment.PeoAir.RMK2 = airSeg.UO_PEOAIR.RMK2;
                myeMosAirlineSegment.PeoAir.INVRMK1 = airSeg.UO_PEOAIR.INVRMK1;
                myeMosAirlineSegment.PeoAir.INVRMK2 = airSeg.UO_PEOAIR.INVRMK2;
                myeMosAirlineSegment.PeoAir.VCHRMK1 = airSeg.UO_PEOAIR.VCHRMK1;
                myeMosAirlineSegment.PeoAir.VCHRMK2 = airSeg.UO_PEOAIR.VCHRMK2;
                myeMosAirlineSegment.PeoAir.SELLCURR = airSeg.UO_PEOAIR.SELLCURR;
                myeMosAirlineSegment.PeoAir.TOTALSELL = airSeg.UO_PEOAIR.TOTALSELL;
                myeMosAirlineSegment.PeoAir.TOTALSTAX = airSeg.UO_PEOAIR.TOTALSTAX;
                myeMosAirlineSegment.PeoAir.COSTCURR = airSeg.UO_PEOAIR.COSTCURR;
                myeMosAirlineSegment.PeoAir.TOTALCOST = airSeg.UO_PEOAIR.TOTALCOST;
                myeMosAirlineSegment.PeoAir.TOTALCTAX = airSeg.UO_PEOAIR.TOTALCTAX;
                myeMosAirlineSegment.PeoAir.PNR = airSeg.UO_PEOAIR.PNR;
                myeMosAirlineSegment.PeoAir.SourceSystem = airSeg.UO_PEOAIR.SourceSystem;
                myeMosAirlineSegment.PeoAir.CreateOn = airSeg.UO_PEOAIR.CreateOn;
                myeMosAirlineSegment.PeoAir.CreateBy = airSeg.UO_PEOAIR.CreateBy;
                myeMosAirlineSegment.PeoAir.UpdateOn = airSeg.UO_PEOAIR.UpdateOn;
                myeMosAirlineSegment.PeoAir.UpdateBy = airSeg.UO_PEOAIR.UpdateBy;
                myeMosAirlineSegment.PeoAir.VoidOn = airSeg.UO_PEOAIR.VoidOn;
                myeMosAirlineSegment.PeoAir.VoidBy = airSeg.UO_PEOAIR.VoidBy;
                //myeMosAirlineSegment.PeoAir.GSTTAX = airlineSegment.UO_PEOAIR.GSTTAX;
                myeMosAirlineSegment.PeoAir.DepartTerminal = airSeg.UO_PEOAIR.DepartTerminal;
                myeMosAirlineSegment.PeoAir.ArrivalTerminal = airSeg.UO_PEOAIR.ArrivalTerminal;
                myeMosAirlineSegment.PeoAir.OtherAirlinePNR = airSeg.UO_PEOAIR.OtherAirlinePNR;
                myeMosAirlineSegment.PeoAir.EQP = airSeg.UO_PEOAIR.EQP;
                myeMosAirlineSegment.PeoAir.Service = airSeg.UO_PEOAIR.Service;
                myeMosAirlineSegment.PeoAir.suppcode = airSeg.UO_PEOAIR.Suppcode;
                #endregion

                #region list
                foreach (UO_PEOAIRDETAIL detail in airSeg.UOList_PEOAIRDETAIL)
                {
                    if (detail.SegNum != myeMosAirlineSegment.PeoAir.SegNum)
                        continue;

                    bool exist = false;

                    if (myeMosAirlineSegment.PeoAir.BkgRef == myeMosBooking.Bkgref && detail.SegNum == "00001" && detail.SEQTYPE == "SELL")
                    {
                        foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL airDt in eMosDBFirstDetailList)
                        {
                            if (detail.AGETYPE == airDt.AGETYPE)
                            {
                                myeMosAirlineSegment.PeoAirDetails.Add(airDt);
                                exist = true;
                                break;
                            }
                        }
                    }
                    if (exist)
                        continue;

                    tbPEOAIRDETAIL eMosAirDetail = new tbPEOAIRDETAIL();

                    eMosAirDetail.CompanyCode = detail.CompanyCode;
                    eMosAirDetail.BkgRef = detail.BkgRef;
                    eMosAirDetail.SegNum = detail.SegNum;
                    eMosAirDetail.SEQNUM = detail.SEQNUM;
                    eMosAirDetail.SEQTYPE = detail.SEQTYPE;
                    eMosAirDetail.AGETYPE = detail.AGETYPE;
                    eMosAirDetail.QTY = detail.QTY;
                    eMosAirDetail.CURR = detail.CURR;
                    eMosAirDetail.AMT = detail.AMT;
                    eMosAirDetail.TAXCURR = detail.TAXCURR;
                    eMosAirDetail.TAXAMT = detail.TAXAMT;
                    eMosAirDetail.CREATEON = detail.CREATEON;
                    eMosAirDetail.CREATEBY = detail.CREATEBY;
                    eMosAirDetail.UPDATEON = detail.UPDATEON;
                    eMosAirDetail.UPDATEBY = detail.UPDATEBY;

                    myeMosAirlineSegment.PeoAirDetails.Add(eMosAirDetail);
                }
                #endregion
                myeMosBooking.AirlineSegments.Add(myeMosAirlineSegment);
            }
        }
        private void IUR2eMos_Pax(RetrieveIURBooking booking, ref eMosBooking myeMosBooking)
        {
            tbPEOPAXs dbPax = da.GetAllPaxByBkgref(myeMosBooking.CompanyCode, myeMosBooking.Bkgref);
            foreach (UO_PeoPax pax in booking.PassengersList)
            {
                tbPEOPAX eMosPax = new tbPEOPAX();
                eMosPax.COMPANYCODE = pax.COMPANYCODE;
                eMosPax.BKGREF = pax.BKGREF;
                eMosPax.PAXFNAME = pax.PAXFNAME;
                eMosPax.PAXLNAME = pax.PAXLNAME;
                eMosPax.PAXTYPE = pax.PAXTYPE;

                if (booking.TicketSegmentsList.Count == 0 || dbPax.FindIndex(p => p.PAXFNAME == eMosPax.PAXFNAME && p.PAXLNAME == eMosPax.PAXLNAME && p.PAXTYPE == eMosPax.PAXTYPE) < 0)
                {
                    eMosPax.SOURCETYPE = pax.SOURCESYSTEM.PadRight(1, ' ').Substring(0, 1);
                    eMosPax.BKGREF = pax.BKGREF;
                    eMosPax.COMPANYCODE = pax.COMPANYCODE;
                    eMosPax.SEGNUM = pax.SEGNUM;
                    eMosPax.PAXNUM = pax.PAXNUM;
                    eMosPax.PAXLNAME = pax.PAXLNAME;
                    eMosPax.PAXFNAME = pax.PAXFNAME;
                    eMosPax.PAXTITLE = pax.PAXTITLE;
                    eMosPax.PAXTYPE = pax.PAXTYPE;
                    eMosPax.PAXAIR = pax.PAXAIR;
                    eMosPax.PAXHOTEL = pax.PAXHOTEL;
                    eMosPax.PAXOTHER = pax.PAXOTHER;
                    eMosPax.PAXAGE = pax.PAXAGE;
                    eMosPax.RMK1 = pax.RMK1;
                    eMosPax.RMK2 = pax.RMK2;
                    eMosPax.VOIDON = pax.VOIDON;
                    eMosPax.VOIDBY = pax.VOIDBY;
                    eMosPax.SOURCESYSTEM = pax.SOURCESYSTEM;
                    eMosPax.CREATEON = pax.CREATEON;
                    eMosPax.CREATEBY = pax.CREATEBY;
                    eMosPax.UPDATEON = pax.UPDATEON;
                    eMosPax.UPDATEBY = pax.UPDATEBY;

                    myeMosBooking.PeoPaxList.Add(eMosPax);
                }
            }
        }
        private void IUR2eMos_VoidTicket(RetrieveIURBooking booking, ref eMosBooking myeMosBooking)
        {
            tbPEOTKTs eMosDBTicketList = da.GetTicketList(booking.CompanyCode, booking.PNR);
            foreach (TicketSegment iurticket in booking.TicketSegmentsList)
            {
                bool isVoid = false;
                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT dbtkt in eMosDBTicketList)
                {
                    if (dbtkt.Ticket == iurticket.UO_PeoTkt.Ticket && dbtkt.Airline == iurticket.UO_PeoTkt.Airline)
                    {
                        if (dbtkt.VoidOn == DateTime.MinValue && iurticket.UO_PeoTkt.VoidOn != DateTime.MinValue && dbtkt.SOURCESYSTEM == iurticket.UO_PeoTkt.SOURCESYSTEM)
                            isVoid = true;
                    }
                }
                if (isVoid)
                {
                    Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT eMosTicket = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT(true);

                    eMosTicket.CompanyCode = iurticket.UO_PeoTkt.CompanyCode;
                    eMosTicket.Ticket = iurticket.UO_PeoTkt.Ticket;
                    eMosTicket.TktSeq = iurticket.UO_PeoTkt.TktSeq;
                    eMosTicket.Airline = iurticket.UO_PeoTkt.Airline;

                    eMosTicket.VoidOn = iurticket.UO_PeoTkt.VoidOn;
                    eMosTicket.VoidBy = iurticket.UO_PeoTkt.VoidBy;
                    eMosTicket.voidteam = iurticket.UO_PeoTkt.voidTeam;

                    myeMosBooking.VoidTicket.Add(eMosTicket);
                }
            }
        }
        private void IUR2eMos_Ticket(RetrieveIURBooking booking, string staffcode, ref eMosBooking myeMosBooking)
        {
            tbPEOTKTs eMosDBTicketList = da.GetTicketList(booking.CompanyCode, booking.PNR);
            bool isPNR_TKSELL = da.Com_PreferenceIsY(booking.CompanyCode, "PNR_TKSELL");
            bool isHWT_Comm = da.Com_PreferenceIsY(booking.CompanyCode, "HWT_Comm");

            foreach (TicketSegment ticketSegment in booking.TicketSegmentsList)
            {
                bool exist = false;

                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT dbtkt in eMosDBTicketList)
                {
                    if (dbtkt.Ticket == ticketSegment.UO_PeoTkt.Ticket && dbtkt.Airline == ticketSegment.UO_PeoTkt.Airline)
                    {
                        exist = true;
                        if (dbtkt.VoidOn != DateTime.MinValue && ticketSegment.UO_PeoTkt.VoidOn == DateTime.MinValue)
                        {
                            exist = false;
                        }
                        break;
                    }
                }
                if (exist)
                    continue;

                eMosTicketSegment myeMosTicketSegment = new eMosTicketSegment();
                #region eMosTicketSegment
                myeMosTicketSegment.PeoTkt.CompanyCode = ticketSegment.UO_PeoTkt.CompanyCode;
                myeMosTicketSegment.PeoTkt.Ticket = ticketSegment.UO_PeoTkt.Ticket;
                myeMosTicketSegment.PeoTkt.TktSeq = ticketSegment.UO_PeoTkt.TktSeq;
                myeMosTicketSegment.PeoTkt.BKGREF = ticketSegment.UO_PeoTkt.BKGREF;
                myeMosTicketSegment.PeoTkt.PNR = ticketSegment.UO_PeoTkt.PNR;
                myeMosTicketSegment.PeoTkt.CltCode = ticketSegment.UO_PeoTkt.CltCode;
                myeMosTicketSegment.PeoTkt.SuppCode = ticketSegment.UO_PeoTkt.SuppCode;
                myeMosTicketSegment.PeoTkt.InvNum = ticketSegment.UO_PeoTkt.InvNum;
                //myeMosTicketSegment.PeoTkt.Jqty = ticketSegment.UO_PeoTkt.Jqty;
                //eMosTkt.peoTKT.Jqty = ((IURTkt.Peotkt.Jqty == 0) ? 1 : IURTkt.Peotkt.Jqty);
                myeMosTicketSegment.PeoTkt.Jqty = (ticketSegment.UO_PeoTkt.Jqty == 0 ? 1 : ticketSegment.UO_PeoTkt.Jqty);

                myeMosTicketSegment.PeoTkt.AirCode = ticketSegment.UO_PeoTkt.AirCode;
                myeMosTicketSegment.PeoTkt.Airline = ticketSegment.UO_PeoTkt.Airline;
                myeMosTicketSegment.PeoTkt.PaxName = ticketSegment.UO_PeoTkt.PaxName;
                myeMosTicketSegment.PeoTkt.DepartOn = ticketSegment.UO_PeoTkt.DepartOn;
                myeMosTicketSegment.PeoTkt.COD1 = ticketSegment.UO_PeoTkt.COD1;
                myeMosTicketSegment.PeoTkt.COD2 = ticketSegment.UO_PeoTkt.COD2;
                myeMosTicketSegment.PeoTkt.COD3 = ticketSegment.UO_PeoTkt.COD3;
                myeMosTicketSegment.PeoTkt.Commission = ticketSegment.UO_PeoTkt.Commission;
                myeMosTicketSegment.PeoTkt.LocalFareCurr = ticketSegment.UO_PeoTkt.LocalFareCurr;
                myeMosTicketSegment.PeoTkt.LocalFareAmt = ticketSegment.UO_PeoTkt.LocalFareAmt;
                myeMosTicketSegment.PeoTkt.ForeignFareCurr = ticketSegment.UO_PeoTkt.ForeignFareCurr;
                myeMosTicketSegment.PeoTkt.ForeignFareAmt = ticketSegment.UO_PeoTkt.ForeignFareAmt;
                myeMosTicketSegment.PeoTkt.Discount = ticketSegment.UO_PeoTkt.Discount;
                myeMosTicketSegment.PeoTkt.FormPay = ticketSegment.UO_PeoTkt.FormPay;
                myeMosTicketSegment.PeoTkt.Routing = GetRouting(ticketSegment);
                myeMosTicketSegment.PeoTkt.FullCurr = ticketSegment.UO_PeoTkt.FullCurr;
                myeMosTicketSegment.PeoTkt.FullFare = ticketSegment.UO_PeoTkt.FullFare;
                myeMosTicketSegment.PeoTkt.PaidCurr = ticketSegment.UO_PeoTkt.PaidCurr;
                myeMosTicketSegment.PeoTkt.PaidFare = ticketSegment.UO_PeoTkt.PaidFare;
                myeMosTicketSegment.PeoTkt.LowCurr = ticketSegment.UO_PeoTkt.LowCurr;
                myeMosTicketSegment.PeoTkt.LowFare = ticketSegment.UO_PeoTkt.LowFare;
                myeMosTicketSegment.PeoTkt.NetCurr = ticketSegment.UO_PeoTkt.NetCurr;
                myeMosTicketSegment.PeoTkt.NetFare = ticketSegment.UO_PeoTkt.NetFare;
                myeMosTicketSegment.PeoTkt.FareBasis = ticketSegment.UO_PeoTkt.FareBasis;

                string myFareType = ticketSegment.UO_PeoTkt.FareType;
                switch (ticketSegment.UO_PeoTkt.SOURCESYSTEM)
                {
                    case "AIR":
                        if (ticketSegment.UO_PeoTkt.FareType == "")
                            myFareType = "N";
                        else
                            myFareType = "S";
                        break;
                    case "IUR":
                        if (ticketSegment.UO_PeoTkt.FareType == "" || ticketSegment.UO_PeoTkt.FareType == "2")
                            myFareType = "N";
                        else
                            myFareType = "S";
                        break;
                    default:
                        break;
                }
                myeMosTicketSegment.PeoTkt.FareType = myFareType;

                myeMosTicketSegment.PeoTkt.TotalTax = ticketSegment.UO_PeoTkt.TotalTax;
                myeMosTicketSegment.PeoTkt.Security = ticketSegment.UO_PeoTkt.Security;
                myeMosTicketSegment.PeoTkt.QSurcharge = ticketSegment.UO_PeoTkt.QSurcharge;
                myeMosTicketSegment.PeoTkt.BranchCode = ticketSegment.UO_PeoTkt.BranchCode;
                myeMosTicketSegment.PeoTkt.TeamCode = ticketSegment.UO_PeoTkt.TeamCode;
                myeMosTicketSegment.PeoTkt.IssueOn = ticketSegment.UO_PeoTkt.IssueOn;
                myeMosTicketSegment.PeoTkt.IssueBy = ticketSegment.UO_PeoTkt.IssueBy;
                myeMosTicketSegment.PeoTkt.VoidOn = ticketSegment.UO_PeoTkt.VoidOn;
                myeMosTicketSegment.PeoTkt.VoidBy = ticketSegment.UO_PeoTkt.VoidBy;
                myeMosTicketSegment.PeoTkt.voidteam = ticketSegment.UO_PeoTkt.voidTeam;
                myeMosTicketSegment.PeoTkt.voidreason = ticketSegment.UO_PeoTkt.voidreason;
                myeMosTicketSegment.PeoTkt.CreateOn = ticketSegment.UO_PeoTkt.CreateOn;
                myeMosTicketSegment.PeoTkt.CreateBy = ticketSegment.UO_PeoTkt.CreateBy;
                myeMosTicketSegment.PeoTkt.UpdateOn = ticketSegment.UO_PeoTkt.UpdateOn;
                myeMosTicketSegment.PeoTkt.UpdateBy = ticketSegment.UO_PeoTkt.UpdateBy;
                myeMosTicketSegment.PeoTkt.TaxAmount1 = ticketSegment.UO_PeoTkt.TaxAmount1;
                myeMosTicketSegment.PeoTkt.TaxID1 = ticketSegment.UO_PeoTkt.TaxID1;
                myeMosTicketSegment.PeoTkt.TaxAmount2 = ticketSegment.UO_PeoTkt.TaxAmount2;
                myeMosTicketSegment.PeoTkt.TaxID2 = ticketSegment.UO_PeoTkt.TaxID2;
                myeMosTicketSegment.PeoTkt.TaxAmount3 = ticketSegment.UO_PeoTkt.TaxAmount3;
                myeMosTicketSegment.PeoTkt.TaxID3 = ticketSegment.UO_PeoTkt.TaxID3;
                myeMosTicketSegment.PeoTkt.FareCalType = ticketSegment.UO_PeoTkt.FareCalType;
                myeMosTicketSegment.PeoTkt.CompressPrintInd = ticketSegment.UO_PeoTkt.CompressPrintInd;
                myeMosTicketSegment.PeoTkt.FareCalData = ticketSegment.UO_PeoTkt.FareCalData;
                myeMosTicketSegment.PeoTkt.pseudo = ticketSegment.UO_PeoTkt.pseudo;
                myeMosTicketSegment.PeoTkt.sinein = ticketSegment.UO_PeoTkt.sinein;
                //myeMosTicketSegment.PeoTkt.gstpct = ticketSegment.UO_PeoTkt.gstpct;
                //myeMosTicketSegment.PeoTkt.gstamt = ticketSegment.UO_PeoTkt.gstamt;
                myeMosTicketSegment.PeoTkt.nst = ticketSegment.UO_PeoTkt.nst;
                myeMosTicketSegment.PeoTkt.SOURCESYSTEM = ticketSegment.UO_PeoTkt.SOURCESYSTEM;
                myeMosTicketSegment.PeoTkt.commission_per = ticketSegment.UO_PeoTkt.commission_per;
                myeMosTicketSegment.PeoTkt.add_payment = ticketSegment.UO_PeoTkt.add_payment;
                myeMosTicketSegment.PeoTkt.iss_off_code = ticketSegment.UO_PeoTkt.iss_off_code;
                myeMosTicketSegment.PeoTkt.tour_code = ticketSegment.UO_PeoTkt.tour_code;
                myeMosTicketSegment.PeoTkt.org_iss = ticketSegment.UO_PeoTkt.org_iss;
                myeMosTicketSegment.PeoTkt.iss_in_exchange = ticketSegment.UO_PeoTkt.iss_in_exchange;
                myeMosTicketSegment.PeoTkt.ControlNum = ticketSegment.UO_PeoTkt.ControlNum;
                //myeMosTicketSegment.PeoTkt.MISpaidfare = ticketSegment.UO_PeoTkt.MISpaidfare;
                myeMosTicketSegment.PeoTkt.ENDORSEMENTS = ticketSegment.UO_PeoTkt.ENDORSEMENTS;
                myeMosTicketSegment.PeoTkt.Destination = GetTicketDestination(Convert.ToString(ticketSegment.UO_PeoTkt.Routing + "").Trim(), Convert.ToString(ticketSegment.UO_PeoTkt.Destination + ""));
                myeMosTicketSegment.PeoTkt.AirReason = ticketSegment.UO_PeoTkt.AirReason;
                myeMosTicketSegment.PeoTkt.INTDOM = ticketSegment.UO_PeoTkt.INTDOM;
                myeMosTicketSegment.PeoTkt.PAXAIR = ticketSegment.UO_PeoTkt.PAXAIR;
                myeMosTicketSegment.PeoTkt.Class = ticketSegment.UO_PeoTkt.Class;
                myeMosTicketSegment.PeoTkt.tkttype = ticketSegment.UO_PeoTkt.tkttype;
                //myeMosTicketSegment.PeoTkt.FullFareTax = ticketSegment.UO_PeoTkt.FullFareTax;
                myeMosTicketSegment.PeoTkt.SellCurr = ticketSegment.UO_PeoTkt.SellCurr;
                myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.SellFare;

                if (isPNR_TKSELL)
                {
                    if (myeMosTicketSegment.PeoTkt.Commission != 0 && string.IsNullOrEmpty(myeMosTicketSegment.PeoTkt.iss_in_exchange))
                    {
                        myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.SellFare;
                    }
                    else
                    {
                        myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.NetFare;
                    }
                }
                if (isHWT_Comm)
                {
                    if (myeMosTicketSegment.PeoTkt.Commission != 0)
                    {
                        myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.NetFare;
                    }
                    else
                    {
                        if (myeMosTicketSegment.PeoTkt.TeamCode == "HWT" && myeMosTicketSegment.PeoTkt.FormPay == "INVAGT")
                        {
                            myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.NetFare;
                        }
                        else
                        {
                            myeMosTicketSegment.PeoTkt.SellFare = ticketSegment.UO_PeoTkt.FullFare;
                        }
                    }
                }

                myeMosTicketSegment.PeoTkt.FullFareWOTax = ticketSegment.UO_PeoTkt.FullFare;
                myeMosTicketSegment.PeoTkt.PaidFareWOTax = myeMosTicketSegment.PeoTkt.SellFare;


                myeMosTicketSegment.PeoTkt.CorpCurr = ticketSegment.UO_PeoTkt.CorpCurr;
                myeMosTicketSegment.PeoTkt.CorpFare = ticketSegment.UO_PeoTkt.CorpFare;
                myeMosTicketSegment.PeoTkt.COD4 = ticketSegment.UO_PeoTkt.COD4;
                myeMosTicketSegment.PeoTkt.COD5 = ticketSegment.UO_PeoTkt.COD5;
                myeMosTicketSegment.PeoTkt.COD6 = ticketSegment.UO_PeoTkt.COD6;

                #endregion

                #region list
                foreach (UO_PeoTktDetail detail in ticketSegment.UOList_PeoTktDetail)
                {
                    tbPEOTKTDETAIL eMosTicketDetail = new tbPEOTKTDETAIL();

                    eMosTicketDetail.CompanyCode = detail.CompanyCode;
                    eMosTicketDetail.Ticket = detail.Ticket;
                    eMosTicketDetail.AirCode = detail.AirCode;
                    eMosTicketDetail.TktSeq = detail.TktSeq;
                    eMosTicketDetail.SegNum = detail.SegNum;
                    eMosTicketDetail.AirLine = detail.Airline;
                    eMosTicketDetail.Flight = detail.Flight;
                    eMosTicketDetail.Class = detail.Class;
                    eMosTicketDetail.Seat = detail.Seat;
                    eMosTicketDetail.DepartCity = detail.DepartCity;
                    eMosTicketDetail.ArrivalCity = detail.ArrivalCity;
                    eMosTicketDetail.DepartDATE = detail.DepartDATE;
                    eMosTicketDetail.DepartTime = detail.DepartTime;
                    eMosTicketDetail.ArrDATE = detail.ArrDATE;
                    eMosTicketDetail.ArrTime = detail.ArrTime;
                    eMosTicketDetail.ElapsedTime = detail.ElapsedTime;
                    eMosTicketDetail.StopOverNum = detail.StopOverNum;
                    eMosTicketDetail.StopOverCity = detail.StopOverCity;
                    eMosTicketDetail.FareAmt = detail.FareAmt;
                    eMosTicketDetail.FareBasic = detail.FareBasic;
                    eMosTicketDetail.Commission = detail.Commission;
                    eMosTicketDetail.ValidStart = detail.ValidStart;
                    eMosTicketDetail.ValidEnd = detail.ValidEnd;
                    eMosTicketDetail.CreateOn = detail.CreateOn;
                    eMosTicketDetail.CreateBy = detail.CreateBy;

                    eMosTicketDetail.UpdateOn = detail.UpdateOn;
                    eMosTicketDetail.UpdateBy = string.IsNullOrEmpty(staffcode) ? detail.UpdateBy : staffcode;
                    //eMosTicketDetail.GSTAMT = detail.GSTAMT;
                    eMosTicketDetail.EQP = detail.EQP;
                    eMosTicketDetail.service = detail.Service;
                    eMosTicketDetail.status = detail.status;
                    eMosTicketDetail.fare_basis_code = detail.fare_basis_code;
                    eMosTicketDetail.DepartTerm = detail.DepartTerm;
                    eMosTicketDetail.ArrivalTerm = detail.ArrivalTerm;
                    eMosTicketDetail.freq_fight_num = detail.freq_fight_num;
                    eMosTicketDetail.AirlinePNR = detail.AirlinePNR;

                    myeMosTicketSegment.PeoTktDetails.Add(eMosTicketDetail);
                }
                #endregion
                myeMosBooking.TicketSegments.Add(myeMosTicketSegment);
            }

        }
        private void IUR2eMos_Other(eMosParameter eMosParam, ref eMosBooking myeMosBooking)
        {
            if (eMosParam != null && myeMosBooking.OtherSegments != null && eMosParam.OtherSegments != null)
            {
                tbPEOOTHER eMosOther = new tbPEOOTHER();
                eMosOther.COMPANYCODE = myeMosBooking.PeoMstr.COMPANYCODE;
                eMosOther.BKGREF = myeMosBooking.PeoMstr.BKGREF;
                int otherCount = da.GetOtherCount(eMosOther.COMPANYCODE, eMosOther.BKGREF);
                //int otherCount = eMosDbLibrary.BusinessObject.EMOS.BO_PEOOTHER.GetRecordsCount("", myDO_PEOOTHER);
                foreach (UO_Other myUO_Other in eMosParam.OtherSegments)
                {
                    eMosOtherSegment myeMosOtherSegment = new eMosOtherSegment();
                    myeMosOtherSegment.PeoOther.COMPANYCODE = myeMosBooking.PeoMstr.COMPANYCODE;
                    myeMosOtherSegment.PeoOther.BKGREF = myeMosBooking.PeoMstr.BKGREF;
                    myeMosOtherSegment.PeoOther.SEGNUM = Convert.ToString(otherCount + 1).PadLeft(5, '0');
                    myeMosOtherSegment.PeoOther.SEGTYPE = myUO_Other.Segtype;
                    myeMosOtherSegment.PeoOther.masterDesc = myUO_Other.Servdesc;
                    myeMosOtherSegment.PeoOther.OTHERDATE = myUO_Other.OtherDate;
                    myeMosOtherSegment.PeoOther.CREATEON = myeMosBooking.PeoMstr.CREATEON;
                    myeMosOtherSegment.PeoOther.CREATEBY = myeMosBooking.PeoMstr.CREATEBY;
                    myeMosOtherSegment.PeoOther.suppliercode = myUO_Other.Suppliercode;

                    foreach (UO_OtherDetail myUO_OtherDetail in myUO_Other.List_OtherDetail)
                    {
                        tbPEOOTHERDETAIL eMosOtherDetail = new tbPEOOTHERDETAIL();
                        eMosOtherDetail.COMPANYCODE = myeMosBooking.PeoMstr.COMPANYCODE;
                        eMosOtherDetail.BKGREF = myeMosBooking.PeoMstr.BKGREF;
                        eMosOtherDetail.SEGNUM = myeMosOtherSegment.PeoOther.SEGNUM;
                        eMosOtherDetail.SEQNUM = myUO_OtherDetail.Seqnum;
                        eMosOtherDetail.SEQTYPE = myUO_OtherDetail.SEQTYPE;
                        eMosOtherDetail.QTY = myUO_OtherDetail.QTY;
                        eMosOtherDetail.TAXAMT = myUO_OtherDetail.TAXAMT;
                        eMosOtherDetail.AMT = myUO_OtherDetail.AMT;
                        eMosOtherDetail.SOURCESYSTEM = GetWebJetGDS_Types((GDS_Types)eMosParam.SourceSystem);
                        eMosOtherDetail.CURR = eMosParam.Currency;
                        eMosOtherDetail.AGETYPE = myUO_OtherDetail.AGETYPE;
                        eMosOtherDetail.CREATEON = myeMosBooking.PeoMstr.CREATEON;
                        eMosOtherDetail.CREATEBY = myeMosBooking.PeoMstr.CREATEBY;

                        myeMosOtherSegment.PeoOtherDetails.Add(eMosOtherDetail);
                    }
                    myeMosBooking.OtherSegments.Add(myeMosOtherSegment);
                }
                //myeMosBooking.eMosOtherSegmentsList.AddRange(xeMosBooking.eMosOtherSegmentsList);
            }
        }

        private string GetRecipient(eMosBooking booking, bool isAllowEInvoice, string recipient)
        {
            if (isAllowEInvoice == false) return "";
            if (Config.TsfrSettings.eInvoice_SendToCustomer)
            // if (System.Configuration.ConfigurationManager.AppSettings["eInvoice_SendToCustomer"] + "" == "0")
            {
                recipient = "";
            }
            return recipient;
        }
        private void SendEInvoice(eMosBooking myeMosBooking, string recipient, string invNum)
        {
            WebServiceConfig ws = Config.WebServiceList[MOEEnums.BindingName.ServiceSoap];
            Westminster.MOE.DAL.eInvoiceSvc.ServiceSoapClient eInvoice = new Westminster.MOE.DAL.eInvoiceSvc.ServiceSoapClient(ws.Name, ws.Url);
            //eInvoice.Url = System.Configuration.ConfigurationManager.AppSettings["eInvoiceServiceUrl"];
            eInvoice.SendEmail(myeMosBooking.CompanyCode, invNum, recipient, string.Empty, string.Empty, new Random().Next(100000, 999999).ToString(), Config.TsfrSettings.eInvoice_SendNow, da.GetDueDate(myeMosBooking.CompanyCode, invNum));
            //eInvoice.SendEmail(myeMosBooking.PEOMSTR.COMPANYCODE, invNum, recipient, "", "", new Random().Next(100000, 999999).ToString(), System.Configuration.ConfigurationManager.AppSettings["eInvoice_SendNow"] + "" == "1" ? true : false, eMosDbLibrary.BusinessObject.EMOS.BO_PEOINV.GetDueDate(myeMosBooking.PEOMSTR.COMPANYCODE, invNum));

        }
        //private void IUR2eMos_Hotel(RetrieveIURBooking booking, eMosBooking myeMosBooking)
        //{

        //}
        private string GetRouting(TicketSegment ts)
        {
            string routing = "";
            string dst = "";

            if (ts.UOList_PeoTktDetail != null && ts.UOList_PeoTktDetail.Count > 0)
            {
                foreach (UO_PeoTktDetail tktdetail in ts.UOList_PeoTktDetail)
                {
                    if (routing == "")
                    {
                        routing = tktdetail.DepartCity + "/" + tktdetail.ArrivalCity;
                    }
                    else
                    {
                        if (dst == tktdetail.DepartCity)
                            routing += "/" + tktdetail.ArrivalCity;
                        else
                            routing += "-" + tktdetail.DepartCity + "/" + tktdetail.ArrivalCity;
                    }

                    dst = tktdetail.ArrivalCity;
                }
            }
            return routing;

        }
        private string GetTicketDestination(string xRouting, string xDestination)
        {
            string myDestination = xDestination;
            string myFirstDest = "";
            string myLastDest = "";

            if (myDestination.Length == 0 && xRouting.Length > 6)
            {
                myFirstDest = xRouting.Substring(0, 3);
                myLastDest = xRouting.Substring(xRouting.Length - 3, 3);

                if (xRouting.Length == 11)
                {

                    if (myFirstDest == myLastDest)
                    {
                        myDestination = xRouting.Substring(4, 3);
                    }
                    else
                    {
                        myDestination = myLastDest;
                    }
                }
                else
                {
                    if (myFirstDest != myLastDest)
                    {
                        myDestination = myLastDest;
                    }

                }
            }

            return myDestination;

        }
        private string GetWebJetGDS_Types(GDS_Types gds)
        {
            string strEnum = "";
            switch (gds)
            {
                case GDS_Types.Abacus:
                    strEnum = "IUR";
                    break;
                case GDS_Types.Galileo:
                    strEnum = "GLO";
                    break;
                case GDS_Types.TravelSky:
                    strEnum = "TS";
                    break;
            }
            return strEnum;
        }
        #endregion
    }
}
