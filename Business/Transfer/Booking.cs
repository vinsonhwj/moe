﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Transfer;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DAL.Transfer;
using Westminster.MOE.Entity.BLLEntity.Segment;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.BLL.Transfer
{
    public class Booking : IDisposable
    {
        public Config Config { get; private set; }
        private ILinkPNR da { get; set; }
        private IConnection Conn { get; set; }

        public Booking(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos]))
        {

        }
        private Booking(Config config, IConnection conn)
            : this(config, new DALinkPNR(conn, config.WebServiceList[MOEEnums.BindingName.IURSoap]), conn)
        {
        }
        public Booking(Config config, ILinkPNR dataAccess, IConnection conn)
        {
            Config = config;
            Conn = conn;
            da = dataAccess;
        }

        //public bool CreateBooking(Westminster.MOE.Entity.DALEntity.Gateway.IURObjects iur, string staffcode, string teamcode, bool updateIURFlag)
        //{
        //    eMosObjects eMos = new eMosObjects();
        //    try
        //    {
        //        if (iur != null && iur.PEOMSTR != null)
        //        {
        //            eMos.isCreate = true;
        //            eMos.CompanyCode = iur.PEOMSTR.COMPANYCODE;
        //            eMos.BkgRef = iur.PEOMSTR.BKGREF;
        //            eMos.PEOMSTR = GetMaster(iur.PEOMSTR, staffcode, teamcode);
        //            eMos.PaxList = GetPassengers(iur.PaxList, iur.PEOMSTR.COMPANYCODE, iur.PEOMSTR.BKGREF);

        //            eMosObjects bkgInfo = da.GeteeMosObjects(eMos.CompanyCode, eMos.BkgRef, eMos.BkgRef);
        //            BookingAirSegmentList airSegList = GetAir(iur, eMos.BkgRef, eMos.BkgRef, iur.PEOMSTR.PNR, bkgInfo.AirList, bkgInfo.AirDetailList);
        //            eMos.AirList = airSegList.AirList;
        //            eMos.AirDetailList = airSegList.AirDetailList;

        //            eMos.TicketList = GetTickets(iur.TicketList, iur.PEOMSTR.COMPANYCODE, iur.PEOMSTR.PNR);
        //            eMos.TicketDetailList = GetTicketDetails(iur.TicketDetailList, staffcode, eMos.TicketList);

        //            bool succeed = da.InserteMosObjects(eMos);
        //            if (updateIURFlag)
        //            {
        //                da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, succeed);
        //            }
        //            return succeed;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch
        //    {
        //        da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, false);
        //        throw;
        //    }
        //}
        //public bool UpdateBooking(Westminster.MOE.Entity.DALEntity.Gateway.IURObjects iur, string staffcode, bool updateIURFlag)
        //{
        //    eMosObjects eMos = new eMosObjects();
        //    try
        //    {
        //        if (iur != null && iur.PEOMSTR != null)
        //        {
        //            MOELog.Info("[Status]:Init eMos data");
        //            eMos.isCreate = false;
        //            eMos.CompanyCode = iur.PEOMSTR.COMPANYCODE;
        //            eMos.BkgRef = iur.PEOMSTR.BKGREF;
        //            eMos.PaxList = GetPassengers(iur.PaxList, eMos.CompanyCode, eMos.BkgRef);

        //            eMosObjects bkgInfo = da.GeteeMosObjects(eMos.CompanyCode, eMos.BkgRef, eMos.BkgRef);
        //            BookingAirSegmentList asl = GetAir(iur, eMos.BkgRef, eMos.BkgRef, iur.PEOMSTR.PNR, bkgInfo.AirList, bkgInfo.AirDetailList);
        //            eMos.AirList = asl.AirList;
        //            eMos.AirDetailList = asl.AirDetailList;

        //            eMos.UpdateVoidTicketList = GetTicketByVoid(iur.TicketList, eMos.CompanyCode, iur.PEOMSTR.PNR);
        //            eMos.TicketList = GetTickets(iur.TicketList, eMos.CompanyCode, iur.PEOMSTR.PNR);
        //            eMos.TicketDetailList = GetTicketDetails(iur.TicketDetailList, staffcode, eMos.TicketList);

        //            MOELog.Info("[Status]:Insert eMos database");s
        //            bool succeed = da.InserteMosObjects(eMos);
        //            if (updateIURFlag)
        //            {
        //                MOELog.Info("[Status]:Update IUR flag");
        //                da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, succeed);
        //            }
        //            return succeed;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch
        //    {
        //        da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, false);
        //        throw;
        //    }
        //}

        public bool Create(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string staffCode, string teamCode, out WSResult wsResult)
        {
            wsResult = new WSResult();
            return SyncBooking(iur, staffCode, teamCode, true, out wsResult);
        }
        public bool Update(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string staffCode, out WSResult wsResult)
        {
            wsResult = new WSResult();
            return SyncBooking(iur, staffCode, null, false, out wsResult);
        }
        public eMosObjects GeteMosBooking(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string companyCode, string eMosbookingRef, string IURBkgRef, out WSResult wsResult)
        {
            wsResult = new WSResult();
            string error = string.Empty;
            if (iur != null && iur.PEOMSTR != null)
            {
                if (da.ExistBooking(companyCode, eMosbookingRef))
                {
                    eMosObjects bkgInfo = da.GeteeMosObjects(companyCode, eMosbookingRef, IURBkgRef);
                    bkgInfo.SourcrPnr = iur.PEOMSTR.BKGREF;
                    bkgInfo.PaxList = GetPassenger(iur, bkgInfo.BkgRef, bkgInfo.PaxList);


                    tbPEOAIRs airs = new tbPEOAIRs();
                    tbPEOAIRDETAILs airDetails = new tbPEOAIRDETAILs();
                    GetAir2(iur, eMosbookingRef, IURBkgRef, bkgInfo.PEOMSTR.PNR, bkgInfo.AirList, bkgInfo.AirDetailList, false, out airs, out airDetails);

                    bkgInfo.AirList = airs;
                    bkgInfo.AirDetailList = airDetails;

                    bkgInfo.TicketList = GetTickets(iur.TicketList, iur.PEOMSTR.COMPANYCODE, iur.PEOMSTR.PNR, eMosbookingRef, out error);
                    foreach (tbPEOTKT tkt in bkgInfo.TicketList)
                    {
                        tkt.BKGREF = eMosbookingRef;
                    }
                    bkgInfo.TicketDetailList = GetTicketDetails(iur.TicketDetailList, bkgInfo.PEOMSTR.STAFFCODE, bkgInfo.TicketList);
                    wsResult.Status = MOEEnums.ResultStatus.Success;
                    wsResult.Msg = error;
                    return bkgInfo;
                }
            }
            return null;
        }

        #region Private Members
        private bool SyncBooking(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string staffCode, string teamCode, bool isCreate, out WSResult wsResult)
        {
            wsResult = new WSResult();
            string error = string.Empty;

            eMosObjects eMos = new eMosObjects(true);
            try
            {
                if (iur != null && iur.PEOMSTR != null)
                {
                    MOELog.Info("[Status]:Init eMos data");
                    eMos.isCreate = isCreate;
                    eMos.CompanyCode = iur.PEOMSTR.COMPANYCODE;
                    eMos.BkgRef = iur.PEOMSTR.BKGREF;
                    if (isCreate)
                    {
                        eMos.PEOMSTR = GetMaster(iur.PEOMSTR, staffCode, teamCode);
                    }
                    else
                    {
                        eMos.UpdateVoidTicketList = GetTicketByVoid(iur.TicketList, eMos.CompanyCode, iur.PEOMSTR.PNR);
                    }
                    eMos.PaxList = GetPassengers(iur.PaxList, iur.PEOMSTR.COMPANYCODE, iur.PEOMSTR.BKGREF);

                    eMosObjects bkgInfo = da.GeteeMosObjects(eMos.CompanyCode, eMos.BkgRef, eMos.BkgRef);
                    tbPEOAIRs airs = new tbPEOAIRs();
                    tbPEOAIRDETAILs airDetails = new tbPEOAIRDETAILs();
                    GetAir2(iur, eMos.BkgRef, eMos.BkgRef, iur.PEOMSTR.PNR, bkgInfo.AirList, bkgInfo.AirDetailList, isCreate, out airs, out airDetails);
                    eMos.AirList = airs;
                    eMos.AirDetailList = airDetails;

                    eMos.TicketList = GetTickets(iur.TicketList, iur.PEOMSTR.COMPANYCODE, iur.PEOMSTR.PNR, eMos.BkgRef, out error);
                    eMos.TicketDetailList = GetTicketDetails(iur.TicketDetailList, staffCode, eMos.TicketList);
                    wsResult.Msg = error;


                    MOELog.Info("[Status]:Insert eMos database");
                    Conn.BeginTransaction();
                    da.InserteMosObjects(eMos);
                    Conn.CommitTransaction();

                    MOELog.Info("[Status]:Update IUR flag");
                    da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, true);

                    wsResult.Status = MOEEnums.ResultStatus.Success;
                    return true;
                }
                else
                {
                    wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                    return false;
                }
            }
            catch
            {
                wsResult.Status = MOEEnums.ResultStatus.Error;
                if (Conn.TransactionState == hwj.DBUtility.Enums.TransactionState.Begin)
                {
                    Conn.RollbackTransaction();
                }
                da.UpdateFlag(eMos.CompanyCode, eMos.BkgRef, false);
                throw;
            }
        }
        private tbPEOMSTR GetMaster(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOMSTR iurMaster, string staffCode, string teamCode)
        {
            Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOMSTR eMosMaster = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOMSTR(true);
            eMosMaster.agency_arc_iata_num = iurMaster.agency_arc_iata_num;

            eMosMaster.BKGREF = iurMaster.BKGREF;


            tbCustomer tbCUSTOMER = da.GetCustomer(iurMaster.COMPANYCODE, iurMaster.CLTODE);
            if (tbCUSTOMER != null)
            {
                eMosMaster.CLTNAME = tbCUSTOMER.CLTNAME;
                eMosMaster.CLTODE = iurMaster.CLTODE;
            }
            else
            {
                eMosMaster.CLTODE = string.Empty;
                eMosMaster.CLTNAME = string.Empty;
            }
            eMosMaster.CLTADDR = string.Empty;
            eMosMaster.COD1 = iurMaster.COD1;
            eMosMaster.COD2 = iurMaster.COD2;
            eMosMaster.COD3 = iurMaster.COD3;
            eMosMaster.COMPANYCODE = iurMaster.COMPANYCODE;
            eMosMaster.CONTACTPERSON = iurMaster.CONTACTPERSON;
            eMosMaster.CREATEBY = iurMaster.CREATEBY;

            eMosMaster.CREATEON = iurMaster.CREATEON;
            eMosMaster.CRSSINEIN = iurMaster.CRSSINEIN;
            eMosMaster.CRSSINEIN_inv = iurMaster.CRSSINEIN_inv;
            eMosMaster.DEADLINE = iurMaster.DEADLINE;
            eMosMaster.DEPARTDATE = iurMaster.DEPARTDATE;

            eMosMaster.DOCAMT = iurMaster.DOCAMT;
            eMosMaster.DOCCOUNT = iurMaster.DOCCOUNT;
            eMosMaster.DOCCURR = iurMaster.DOCCURR;
            eMosMaster.DOCTAX = iurMaster.DOCTAX;
            eMosMaster.DOCTAXCURR = iurMaster.DOCTAXCURR;

            eMosMaster.EMAIL = iurMaster.EMAIL;
            eMosMaster.FAX = iurMaster.FAX;
            eMosMaster.FIRSTPAX = iurMaster.FIRSTPAX;
            eMosMaster.INVAMT = iurMaster.INVAMT;
            eMosMaster.INVCOUNT = iurMaster.INVCOUNT;

            eMosMaster.INVCURR = iurMaster.INVCURR;
            eMosMaster.INVTAX = iurMaster.INVTAX;
            eMosMaster.INVTAXCURR = iurMaster.INVTAXCURR;
            eMosMaster.iss_off_code = iurMaster.iss_off_code;
            //myeMosBooking.ISTOUR = booking.ISTOUR;

            eMosMaster.MASTERPNR = iurMaster.MASTERPNR;
            //myeMosBooking.MstrRemark = booking.MstrRemark;
            eMosMaster.PHONE = iurMaster.PHONE;
            eMosMaster.PNR = iurMaster.PNR;
            eMosMaster.pseudo = iurMaster.pseudo;

            eMosMaster.pseudo_inv = iurMaster.pseudo_inv;
            eMosMaster.RMK1 = iurMaster.RMK1;
            eMosMaster.RMK2 = iurMaster.RMK2;
            eMosMaster.RMK3 = iurMaster.RMK3;
            eMosMaster.RMK4 = iurMaster.RMK4;

            //myeMosBooking.salesperson = booking.salesperson;
            eMosMaster.SOURCE = iurMaster.SOURCE;
            //myeMosBooking.SOURCEREF = booking.SOURCEREF;
            eMosMaster.SOURCESYSTEM = iurMaster.SOURCESYSTEM;
            eMosMaster.STAFFCODE = string.IsNullOrEmpty(staffCode) ? iurMaster.STAFFCODE : staffCode;

            eMosMaster.TEAMCODE = string.IsNullOrEmpty(teamCode) ? iurMaster.TEAMCODE : teamCode;
            eMosMaster.TOURCODE = iurMaster.TOURCODE;
            eMosMaster.TTLCOSTAMT = iurMaster.TTLCOSTAMT;
            eMosMaster.TTLCOSTCURR = iurMaster.TTLCOSTCURR;
            //myeMosBooking.TTLCOSTGST = booking.TTLCOSTGST;

            eMosMaster.TTLCOSTTAX = iurMaster.TTLCOSTTAX;
            eMosMaster.TTLCOSTTAXCURR = iurMaster.TTLCOSTTAXCURR;
            //myeMosBooking.TTLDOCGST = booking.TTLDOCGST;
            //myeMosBooking.TTLINVGST = booking.TTLINVGST;
            eMosMaster.TTLSELLAMT = iurMaster.TTLSELLAMT;

            eMosMaster.TTLSELLCURR = iurMaster.TTLSELLCURR;
            //myeMosBooking.TTLSELLGST = booking.TTLSELLGST;
            eMosMaster.TTLSELLTAX = iurMaster.TTLSELLTAX;
            eMosMaster.TTLSELLTAXCURR = iurMaster.TTLSELLTAXCURR;
            eMosMaster.UPDATEBY = string.IsNullOrEmpty(staffCode) ? iurMaster.UPDATEBY : staffCode;
            eMosMaster.UPDATEON = DateTime.Now;

            eMosMaster.ReptCurr = eMosMaster.TTLCOSTCURR;
            eMosMaster.ReptTaxCurr = eMosMaster.TTLCOSTTAXCURR;
            return eMosMaster;
        }
        private tbPEOTKTs GetTickets(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTs iurTicketList, string companycode, string pnr, string eMosbookingRef, out string error)
        {
            error = string.Empty;
            StringBuilder sb = new StringBuilder();
            tbPEOTKTs eMosTicketList = new tbPEOTKTs();

            List<string> list = new List<string>();
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT t in iurTicketList)
            {
                list.Add(t.Ticket);
            }
            tbPEOTKTs anotherTickets = da.GetAnotherBKGTicket(companycode, eMosbookingRef, list);

            tbPEOTKTs eMosDBTicketList = da.GetTicketList(companycode, pnr);

            bool isPNR_TKSELL = da.GetPreferenceEnable(companycode, MOEEnums.CompanyPreference.PNR_TKSELL);
            bool isHWT_Comm = da.GetPreferenceEnable(companycode, MOEEnums.CompanyPreference.HWT_Comm);
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT iurticket in iurTicketList)
            {
                tbPEOTKT tkt = anotherTickets.Find(c => c.PNR == iurticket.PNR && c.CompanyCode == iurticket.CompanyCode && c.Ticket == iurticket.Ticket);
                if (tkt != null)
                {
                    sb.AppendFormat("-Ticket:{0} already exist in bkg: {1} ", tkt.Ticket, tkt.BKGREF);
                }
                bool exist = false;

                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT dbtkt in eMosDBTicketList)
                {
                    if (dbtkt.Ticket == iurticket.Ticket && dbtkt.Airline == iurticket.Airline)
                    {
                        exist = true;
                    }
                }
                if (exist)
                    continue;

                Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT eMosTicket = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT(true);

                eMosTicket.CompanyCode = iurticket.CompanyCode;
                eMosTicket.Ticket = iurticket.Ticket;
                eMosTicket.TktSeq = iurticket.TktSeq;
                eMosTicket.BKGREF = iurticket.BKGREF;
                eMosTicket.PNR = iurticket.PNR;
                eMosTicket.CltCode = iurticket.CltCode;
                eMosTicket.SuppCode = iurticket.SuppCode;
                eMosTicket.InvNum = iurticket.InvNum;
                //eMosTicket.Jqty = iurticket.Jqty;
                eMosTicket.Jqty = (iurticket.Jqty == 0 ? 1 : iurticket.Jqty);

                eMosTicket.AirCode = iurticket.AirCode;
                eMosTicket.Airline = iurticket.Airline;
                eMosTicket.PaxName = iurticket.PaxName;
                eMosTicket.DepartOn = iurticket.DepartOn;
                eMosTicket.COD1 = iurticket.COD1;
                eMosTicket.COD2 = iurticket.COD2;
                eMosTicket.COD3 = iurticket.COD3;
                eMosTicket.Commission = iurticket.Commission;
                eMosTicket.LocalFareCurr = iurticket.LocalFareCurr;
                eMosTicket.LocalFareAmt = iurticket.LocalFareAmt;
                eMosTicket.ForeignFareCurr = iurticket.ForeignFareCurr;
                eMosTicket.ForeignFareAmt = iurticket.ForeignFareAmt;
                eMosTicket.Discount = iurticket.Discount;
                eMosTicket.FormPay = iurticket.FormPay;

                eMosTicket.FullCurr = iurticket.FullCurr;
                eMosTicket.FullFare = iurticket.FullFare;
                eMosTicket.PaidCurr = iurticket.PaidCurr;
                eMosTicket.PaidFare = iurticket.PaidFare;
                eMosTicket.LowCurr = iurticket.LowCurr;
                eMosTicket.LowFare = iurticket.LowFare;
                eMosTicket.NetCurr = iurticket.NetCurr;
                eMosTicket.NetFare = iurticket.NetFare;
                eMosTicket.FareBasis = iurticket.FareBasis;
                //eMosTicket.FareType = iurticket.FareType;
                //Dim myFareType As String = Me.FareType
                //   Select Case Me.SOURCESYSTEM
                //       Case "AIR"
                //           If Me.FareType = "" Then
                //               myFareType = "N"
                //           Else
                //               myFareType = "S"
                //           End If
                //       Case "IUR"
                //           If Me.FareType = "" Or Me.FareType = "2" Then
                //               myFareType = "N"
                //           ElseIf Me.FareType = "3" Or Me.FareType = "N" Then
                //               myFareType = "S"
                //           End If
                //   End Select

                string myFareType = iurticket.FareType;
                switch (iurticket.SOURCESYSTEM)
                {
                    case "AIR":
                        if (iurticket.FareType == "")
                            myFareType = "N";
                        else
                            myFareType = "S";
                        break;
                    case "IUR":
                        if (iurticket.FareType == "" || iurticket.FareType == "2")
                            myFareType = "N";
                        else if (iurticket.FareType == "3" || iurticket.FareType == "N")
                            myFareType = "S";
                        break;
                    default:
                        break;
                }
                eMosTicket.FareType = myFareType;

                eMosTicket.TotalTax = iurticket.TotalTax;
                eMosTicket.Security = iurticket.Security;
                eMosTicket.QSurcharge = iurticket.QSurcharge;
                eMosTicket.BranchCode = iurticket.BranchCode;
                eMosTicket.TeamCode = iurticket.TeamCode;
                eMosTicket.IssueOn = iurticket.IssueOn;
                eMosTicket.IssueBy = iurticket.IssueBy;
                eMosTicket.VoidOn = iurticket.VoidOn;
                eMosTicket.VoidBy = iurticket.VoidBy;
                eMosTicket.voidteam = iurticket.voidTeam;
                eMosTicket.voidreason = iurticket.voidreason;
                eMosTicket.CreateOn = iurticket.CreateOn;
                eMosTicket.CreateBy = iurticket.CreateBy;
                eMosTicket.UpdateOn = DateTime.Now;
                eMosTicket.UpdateBy = iurticket.UpdateBy;
                eMosTicket.TaxAmount1 = iurticket.TaxAmount1;
                eMosTicket.TaxID1 = iurticket.TaxID1;
                eMosTicket.TaxAmount2 = iurticket.TaxAmount2;
                eMosTicket.TaxID2 = iurticket.TaxID2;
                eMosTicket.TaxAmount3 = iurticket.TaxAmount3;
                eMosTicket.TaxID3 = iurticket.TaxID3;
                eMosTicket.FareCalType = iurticket.FareCalType;
                eMosTicket.CompressPrintInd = iurticket.CompressPrintInd;
                eMosTicket.FareCalData = iurticket.FareCalData;
                eMosTicket.pseudo = iurticket.pseudo;
                eMosTicket.sinein = iurticket.sinein;
                //eMosTicket.gstpct = iurticket.gstpct;
                //eMosTicket.gstamt = iurticket.gstamt;
                eMosTicket.nst = iurticket.nst;
                eMosTicket.SOURCESYSTEM = iurticket.SOURCESYSTEM;
                eMosTicket.commission_per = iurticket.commission_per;
                eMosTicket.add_payment = iurticket.add_payment;
                eMosTicket.iss_off_code = iurticket.iss_off_code;
                eMosTicket.tour_code = iurticket.tour_code;
                eMosTicket.org_iss = iurticket.org_iss;
                eMosTicket.iss_in_exchange = iurticket.iss_in_exchange;
                eMosTicket.ControlNum = iurticket.ControlNum;
                //eMosTicket.MISpaidfare = iurticket.MISpaidfare;
                eMosTicket.ENDORSEMENTS = iurticket.ENDORSEMENTS;
                eMosTicket.Destination = GetTicketDestination(Convert.ToString(iurticket.Routing + "").Trim(), Convert.ToString(iurticket.Destination + ""));
                eMosTicket.Routing = iurticket.Routing;
                eMosTicket.AirReason = iurticket.AirReason;
                eMosTicket.INTDOM = string.IsNullOrEmpty(iurticket.INTDOM) ? "I" : iurticket.INTDOM;
                eMosTicket.PAXAIR = iurticket.PAXAIR;
                eMosTicket.Class = iurticket.Class;
                eMosTicket.tkttype = iurticket.tkttype;
                //eMosTicket.FullFareTax = iurticket.FullFareTax;
                eMosTicket.SellCurr = iurticket.SellCurr;
                eMosTicket.SellFare = iurticket.SellFare;

                if (isPNR_TKSELL)
                {
                    if (eMosTicket.Commission != 0 && (eMosTicket.iss_in_exchange == null || string.IsNullOrEmpty(eMosTicket.iss_in_exchange.Replace("\r", "").Replace("\n", "").Trim())))
                    {
                        eMosTicket.SellFare = iurticket.FullFare;
                    }
                    else
                    {
                        eMosTicket.SellFare = iurticket.NetFare;
                    }
                }
                else
                {
                    eMosTicket.SellFare = iurticket.SellFare;
                }

                if (isHWT_Comm)
                {
                    if (eMosTicket.Commission == 0)
                    {
                        eMosTicket.SellFare = iurticket.NetFare;
                    }
                    else
                    {
                        if (eMosTicket.TeamCode == "HWT" && eMosTicket.FormPay == "INVAGT")
                        {
                            eMosTicket.SellFare = iurticket.NetFare;
                        }
                        else
                        {
                            eMosTicket.SellFare = iurticket.FullFare;
                        }
                    }
                }

                eMosTicket.FullFareWOTax = iurticket.FullFare;
                eMosTicket.PaidFareWOTax = eMosTicket.SellFare;


                eMosTicket.CorpCurr = iurticket.CorpCurr;
                eMosTicket.CorpFare = iurticket.CorpFare;
                eMosTicket.COD4 = iurticket.COD4;
                eMosTicket.COD5 = iurticket.COD5;
                eMosTicket.COD6 = iurticket.COD6;
                //eMosTicket.PaidFareWOTax = iurticket.PaidFareWOTax;
                //eMosTicket.LowFareWOTax = iurticket.LowFareWOTax;
                //eMosTicket.CorpFareWOTax = iurticket.CorpFareWOTax;
                //eMosTicket.GemsCode = iurticket.GemsCode;
                //eMosTicket.TransactionFee = iurticket.TransactionFee;
                //eMosTicket.FullFareWOTax = iurticket.FullFareWOTax;
                //eMosTicket.AirlineTicket = iurticket.AirlineTicket;
                eMosTicketList.Add(eMosTicket);


            }
            error = sb.ToString();
            return eMosTicketList;
        }
        private tbPEOTKTs GetTicketByVoid(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTs iurTicketList, string companycode, string pnr)
        {
            tbPEOTKTs eMosTicketList = new tbPEOTKTs();
            tbPEOTKTs eMosDBTicketList = da.GetTicketList(companycode, pnr);
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT iurticket in iurTicketList)
            {
                bool isVoid = false;
                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT dbtkt in eMosDBTicketList)
                {
                    if (dbtkt.Ticket == iurticket.Ticket && dbtkt.Airline == iurticket.Airline)
                    {
                        if (dbtkt.VoidOn == DateTime.MinValue && iurticket.VoidOn != DateTime.MinValue && dbtkt.SOURCESYSTEM == iurticket.SOURCESYSTEM)
                            isVoid = true;
                    }
                }
                if (isVoid)
                {
                    tbPEOTKT eMosTicket = new tbPEOTKT(true);

                    eMosTicket.CompanyCode = iurticket.CompanyCode;
                    eMosTicket.Ticket = iurticket.Ticket;
                    eMosTicket.TktSeq = iurticket.TktSeq;
                    eMosTicket.Airline = iurticket.Airline;

                    eMosTicket.VoidOn = iurticket.VoidOn;
                    eMosTicket.VoidBy = iurticket.VoidBy;
                    eMosTicket.voidteam = iurticket.voidTeam;

                    eMosTicketList.Add(eMosTicket);
                }
            }
            return eMosTicketList;
        }
        private tbPEOTKTDETAILs GetTicketDetails(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAILs iureMosTicketDtList, string staffCode, tbPEOTKTs emosTkt)
        {
            tbPEOTKTDETAILs eMosTicketDtList = new tbPEOTKTDETAILs();

            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAIL iurDetail in iureMosTicketDtList)
            {
                bool exist = true;
                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKT tkt in emosTkt)
                {
                    if (tkt.Ticket == iurDetail.Ticket && !string.IsNullOrEmpty(tkt.PNR))
                    {
                        exist = false;
                        break;
                    }
                }
                if (exist)
                    continue;

                Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKTDETAIL eMosTicketDt = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOTKTDETAIL(true);

                eMosTicketDt.CompanyCode = iurDetail.CompanyCode;
                eMosTicketDt.Ticket = iurDetail.Ticket;
                eMosTicketDt.AirCode = iurDetail.AirCode;
                eMosTicketDt.TktSeq = iurDetail.TktSeq;
                eMosTicketDt.SegNum = iurDetail.SegNum;
                eMosTicketDt.AirLine = iurDetail.Airline;
                eMosTicketDt.Flight = iurDetail.Flight;
                eMosTicketDt.Class = iurDetail.Class;
                eMosTicketDt.Seat = iurDetail.Seat;
                eMosTicketDt.DepartCity = iurDetail.DepartCity;
                eMosTicketDt.ArrivalCity = iurDetail.ArrivalCity;
                eMosTicketDt.DepartDATE = iurDetail.DepartDATE;
                eMosTicketDt.DepartTime = iurDetail.DepartTime;
                eMosTicketDt.ArrDATE = iurDetail.ArrDATE;
                eMosTicketDt.ArrTime = iurDetail.ArrTime;
                eMosTicketDt.ElapsedTime = iurDetail.ElapsedTime;
                eMosTicketDt.StopOverNum = iurDetail.StopOverNum;
                eMosTicketDt.StopOverCity = iurDetail.StopOverCity;
                eMosTicketDt.FareAmt = iurDetail.FareAmt;
                eMosTicketDt.FareBasic = iurDetail.FareBasic;
                eMosTicketDt.Commission = iurDetail.Commission;
                eMosTicketDt.ValidStart = iurDetail.ValidStart;
                eMosTicketDt.ValidEnd = iurDetail.ValidEnd;
                eMosTicketDt.CreateOn = iurDetail.CreateOn;
                eMosTicketDt.CreateBy = iurDetail.CreateBy;

                eMosTicketDt.UpdateOn = DateTime.Now;
                eMosTicketDt.UpdateBy = string.IsNullOrEmpty(staffCode) ? iurDetail.UpdateBy : staffCode;
                //myUO_PEOTKTDETAIL.GSTAMT = detail.GSTAMT;
                eMosTicketDt.EQP = iurDetail.EQP;
                eMosTicketDt.service = iurDetail.Service;
                eMosTicketDt.status = iurDetail.status;
                eMosTicketDt.fare_basis_code = iurDetail.fare_basis_code;
                eMosTicketDt.DepartTerm = iurDetail.DepartTerm;
                eMosTicketDt.ArrivalTerm = iurDetail.ArrivalTerm;
                eMosTicketDt.freq_fight_num = iurDetail.freq_fight_num;
                eMosTicketDt.AirlinePNR = iurDetail.AirlinePNR;

                eMosTicketDtList.Add(eMosTicketDt);
            }
            return eMosTicketDtList;
        }

        private tbPEOPAXs GetPassengers(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAXs iurPaxList, string companycode, string bkgref)
        {
            tbPEOPAXs eMosPaxList = new tbPEOPAXs();

            tbPEOPAXs eMosPaxDBList = da.GetPaxList(bkgref, companycode);
            int index = eMosPaxDBList.Count + 1;
            //foreach (tbPEOPAX db in eMosPaxDBList)
            //{
            //    db.SEGNUM = (++index).ToString().PadLeft(5, '0');
            //    eMosPaxList.Add(db);
            //}
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX iurPax in iurPaxList)
            {
                bool exist = false;
                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX obj in eMosPaxDBList)
                {
                    if (iurPax.PAXFNAME == obj.PAXFNAME && iurPax.PAXLNAME == obj.PAXLNAME && iurPax.PAXTYPE == obj.PAXTYPE)
                        exist = true;
                }
                if (exist)
                    continue;

                Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX eMosPax = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX(true);

                eMosPax.COMPANYCODE = iurPax.COMPANYCODE;

                eMosPax.PAXFNAME = iurPax.PAXFNAME;
                eMosPax.PAXLNAME = iurPax.PAXLNAME;
                eMosPax.PAXTYPE = iurPax.PAXTYPE;

                eMosPax.BKGREF = iurPax.BKGREF;
                eMosPax.SourcePnr = iurPax.BKGREF;

                eMosPax.SOURCETYPE = iurPax.SOURCESYSTEM.PadRight(1, ' ').Substring(0, 1);

                eMosPax.COMPANYCODE = iurPax.COMPANYCODE;
                eMosPax.SEGNUM = (index).ToString().PadLeft(5, '0');
                eMosPax.PAXNUM = (index++).ToString().PadLeft(2, '0');
                eMosPax.PAXLNAME = iurPax.PAXLNAME;
                eMosPax.PAXFNAME = iurPax.PAXFNAME;
                eMosPax.PAXTITLE = iurPax.PAXTITLE;
                eMosPax.PAXTYPE = iurPax.PAXTYPE;
                eMosPax.PAXAIR = iurPax.PAXAIR;
                eMosPax.PAXHOTEL = iurPax.PAXHOTEL;
                eMosPax.PAXOTHER = iurPax.PAXOTHER;
                eMosPax.PAXAGE = iurPax.PAXAGE;
                eMosPax.RMK1 = iurPax.RMK1;
                eMosPax.RMK2 = iurPax.RMK2;
                eMosPax.VOIDON = iurPax.VOIDON;
                eMosPax.VOIDBY = iurPax.VOIDBY;
                eMosPax.SOURCESYSTEM = iurPax.SOURCESYSTEM;
                eMosPax.CREATEON = iurPax.CREATEON;
                eMosPax.CREATEBY = iurPax.CREATEBY;
                eMosPax.UPDATEON = DateTime.Now;
                eMosPax.UPDATEBY = iurPax.UPDATEBY;
                eMosPax.PAXNAME1 = string.Empty;
                eMosPaxList.Add(eMosPax);
            }
            return eMosPaxList;
        }
        private tbPEOPAXs GetPassenger(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string bkgref, tbPEOPAXs dbPaxList)
        {
            tbPEOPAXs eMosPaxList = new tbPEOPAXs();
            int index = 0;
            foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX db in dbPaxList)
            {
                db.SEGNUM = (++index).ToString().PadLeft(5, '0');
                eMosPaxList.Add(db);
            }
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX iurPax in iur.PaxList)
            {
                Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX eMosPax = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOPAX();

                eMosPax.COMPANYCODE = iurPax.COMPANYCODE;

                eMosPax.PAXFNAME = iurPax.PAXFNAME;
                eMosPax.PAXLNAME = iurPax.PAXLNAME;
                eMosPax.PAXTYPE = iurPax.PAXTYPE;

                eMosPax.BKGREF = bkgref;
                eMosPax.SourcePnr = iurPax.BKGREF;

                eMosPax.SOURCETYPE = iurPax.SOURCESYSTEM.PadRight(1, ' ').Substring(0, 1);

                eMosPax.COMPANYCODE = iurPax.COMPANYCODE;
                eMosPax.SEGNUM = (++index).ToString().PadLeft(5, '0');
                eMosPax.PAXNUM = (index).ToString().PadLeft(2, '0');
                eMosPax.PAXLNAME = iurPax.PAXLNAME;
                eMosPax.PAXFNAME = iurPax.PAXFNAME;
                eMosPax.PAXTITLE = iurPax.PAXTITLE;
                eMosPax.PAXTYPE = iurPax.PAXTYPE;
                eMosPax.PAXAIR = iurPax.PAXAIR;
                eMosPax.PAXHOTEL = iurPax.PAXHOTEL;
                eMosPax.PAXOTHER = iurPax.PAXOTHER;
                eMosPax.PAXAGE = iurPax.PAXAGE;
                eMosPax.RMK1 = iurPax.RMK1;
                eMosPax.RMK2 = iurPax.RMK2;
                eMosPax.VOIDON = iurPax.VOIDON;
                eMosPax.VOIDBY = iurPax.VOIDBY;
                eMosPax.SOURCESYSTEM = iurPax.SOURCESYSTEM;
                eMosPax.CREATEON = iurPax.CREATEON;
                eMosPax.CREATEBY = iurPax.CREATEBY;
                eMosPax.UPDATEON = DateTime.Now;
                eMosPax.UPDATEBY = iurPax.UPDATEBY;
                eMosPax.PAXNAME1 = string.Empty;
                eMosPaxList.Add(eMosPax);
            }
            return eMosPaxList;
        }
        private bool GetAir2(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string eMosbookingRef, string IURBkgRef, string PNR, tbPEOAIRs dbAirList, tbPEOAIRDETAILs dbAirDetailList, bool isCreate, out tbPEOAIRs airList, out tbPEOAIRDETAILs airDetailList)
        {
            airDetailList = new tbPEOAIRDETAILs();
            airList = new tbPEOAIRs();
            List<BookingAir> beforeAir = new List<BookingAir>();
            List<BookingAir> newAir = new List<BookingAir>();
            List<BookingAir> afterAir = new List<BookingAir>();

            List<BookingAir> DBBookingAir = new List<BookingAir>();//这个不简单 自己个source 果堆
            //tbPEOAIRs oriIURAir = new tbPEOAIRs();

            // tbPEOAIRDETAILs eMosDBFirstDetailList = da.GetFirstAirDetailList(iur.PEOMSTR.COMPANYCODE, eMosbookingRef);

            int index = 1;
            int startIndex1 = dbAirList.FindIndex(c => c.SourcePnr == IURBkgRef && c.SourceSystem == iur.PEOMSTR.SOURCESYSTEM);

            if (startIndex1 == -1)
                startIndex1 = dbAirList.Count;

            foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR db in dbAirList)
            {
                BookingAir a = new BookingAir();
                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL dbDetail in dbAirDetailList)
                {
                    if (db.SegNum == dbDetail.SegNum)
                    {
                        a.Details.Add(dbDetail);
                    }
                }
                a.Header = db;

                if (db.SourcePnr == IURBkgRef && db.SourceSystem == iur.PEOMSTR.SOURCESYSTEM)
                {
                    DBBookingAir.Add(a);
                    continue;
                }


                if (beforeAir.Count >= startIndex1)
                    afterAir.Add(a);
                else
                    beforeAir.Add(a);
            }
            DateTime lastUpdateOn = DateTime.MinValue;
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR iurAir in iur.AirList)
            {
                if (iurAir.UpdateOn > lastUpdateOn)
                    lastUpdateOn = iurAir.UpdateOn;
            }
            foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR iurAir in iur.AirList)
            {
                if (!isCreate && lastUpdateOn != DateTime.MinValue && iurAir.UpdateOn < lastUpdateOn.AddSeconds(-5))
                    continue;
                if (iurAir.DepartDATE == DateTime.MinValue || iurAir.ArrDATE == DateTime.MinValue)
                    continue;

                BookingAir a = new BookingAir();
                Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR eMosAir = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR(true);

                eMosAir.CompanyCode = iurAir.CompanyCode;

                eMosAir.SegNum = (index++).ToString().PadLeft(5, '0');

                eMosAir.PNR = iurAir.PNR;
                eMosAir.BkgRef = eMosbookingRef;
                eMosAir.SourcePnr = iurAir.BkgRef;

                eMosAir.Flight = iurAir.Flight;
                eMosAir.Class = iurAir.Class;
                eMosAir.DepartCity = iurAir.DepartCity;
                eMosAir.ArrivalCity = iurAir.ArrivalCity;
                eMosAir.Status = iurAir.Status;
                eMosAir.NumOfPax = iurAir.NumOfPax;
                eMosAir.DepartDATE = iurAir.DepartDATE;
                eMosAir.DepartTime = iurAir.DepartTime;
                eMosAir.ArrDATE = iurAir.ArrDATE;
                eMosAir.ArrTime = iurAir.ArrTime;
                eMosAir.ElapsedTime = iurAir.ElapsedTime;
                eMosAir.SSR = iurAir.SSR;
                eMosAir.Seat = iurAir.Seat;
                eMosAir.ReasonCode = iurAir.ReasonCode;
                eMosAir.StopOverNum = iurAir.StopOverNum;
                eMosAir.StopOverCity = iurAir.StopOverCity;
                eMosAir.Pcode = iurAir.Pcode;
                eMosAir.SpecialRQ = iurAir.SpecialRQ;
                eMosAir.RMK1 = MOEFormats.EmptyString(iurAir.RMK1);
                eMosAir.RMK2 = MOEFormats.EmptyString(iurAir.RMK2);
                eMosAir.INVRMK1 = MOEFormats.EmptyString(iurAir.INVRMK1);
                eMosAir.INVRMK2 = MOEFormats.EmptyString(iurAir.INVRMK2);
                eMosAir.VCHRMK1 = MOEFormats.EmptyString(iurAir.VCHRMK1);
                eMosAir.VCHRMK2 = MOEFormats.EmptyString(iurAir.VCHRMK2);
                eMosAir.SELLCURR = iurAir.SELLCURR;
                eMosAir.TOTALSELL = iurAir.TOTALSELL;
                eMosAir.TOTALSTAX = iurAir.TOTALSTAX;
                eMosAir.COSTCURR = iurAir.COSTCURR;
                eMosAir.TOTALCOST = iurAir.TOTALCOST;
                eMosAir.TOTALCTAX = iurAir.TOTALCTAX;
                //eMosAir.PNR = iurAir.PNR;
                eMosAir.SourceSystem = iurAir.SourceSystem;
                eMosAir.CreateOn = iurAir.CreateOn;
                eMosAir.CreateBy = iurAir.CreateBy;
                eMosAir.UpdateOn = DateTime.Now;
                eMosAir.UpdateBy = iurAir.UpdateBy;
                eMosAir.VoidOn = iurAir.VoidOn;
                eMosAir.VoidBy = iurAir.VoidBy;
                //eMosAir.GSTTAX = iurAir.GSTTAX;
                eMosAir.DepartTerminal = iurAir.DepartTerminal;
                eMosAir.ArrivalTerminal = iurAir.ArrivalTerminal;
                eMosAir.OtherAirlinePNR = iurAir.OtherAirlinePNR;
                //eMosAir.showDetailFee = iurAir.showDetailFee;
                eMosAir.EQP = iurAir.EQP;
                eMosAir.Service = iurAir.Service;
                eMosAir.suppcode = iurAir.Suppcode;
                //eMosAir.isRecalable = iurAir.isRecalable;

                eMosAir.IssuedDoc = GetIssuedDoc(dbAirList, iurAir);

                //eMosAirSeqLst.AirList.Insert(startIndex1++, eMosAir);
                a.Header = eMosAir;
                foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL iurAirDt in iur.AirDetailList)
                {
                    if (iurAirDt.SegNum != iurAir.SegNum)
                        continue;

                    //bool exist = false;

                    //if (iurAir.BkgRef == eMosbookingRef && iurAirDt.SegNum == "00001" && iurAirDt.SEQNUM == "00001" && iurAirDt.SEQTYPE == "SELL")
                    //{
                    //    foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL airDt in eMosDBFirstDetailList)
                    //    {
                    //        if (iurAirDt.AGETYPE == airDt.AGETYPE)
                    //        {
                    //            a.Details.Add(airDt);
                    //            exist = true;
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (exist)
                    //    continue;



                    Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL eMosAirDt = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL(true);

                    eMosAirDt.CompanyCode = iurAirDt.CompanyCode;

                    eMosAirDt.SegNum = (index).ToString().PadLeft(5, '0');
                    eMosAirDt.SEQNUM = iurAirDt.SEQNUM;
                    eMosAirDt.SEQTYPE = iurAirDt.SEQTYPE;

                    eMosAirDt.BkgRef = eMosbookingRef;

                    eMosAirDt.AGETYPE = iurAirDt.AGETYPE;
                    eMosAirDt.QTY = iurAirDt.QTY;
                    eMosAirDt.CURR = iurAirDt.CURR;
                    eMosAirDt.AMT = iurAirDt.AMT;
                    eMosAirDt.TAXCURR = iurAirDt.TAXCURR;
                    eMosAirDt.TAXAMT = iurAirDt.TAXAMT;
                    eMosAirDt.CREATEON = iurAirDt.CREATEON;
                    eMosAirDt.CREATEBY = iurAirDt.CREATEBY;
                    eMosAirDt.UPDATEON = DateTime.Now;
                    eMosAirDt.UPDATEBY = iurAirDt.UPDATEBY;
                    //eMosAirDt.AMTFEE = iurAirDt.AMTFEE;
                    //eMosAirDt.DISCOUNT_MARKUP_PER = iurAirDt.DISCOUNT_MARKUP_PER;
                    //eMosAirDt.INVNETTGROSS = iurAirDt.INVNETTGROSS;	

                    //eMosAirSeqLst.AirDetailList.Insert(startIndex2++, eMosAirDt);
                    a.Details.Add(eMosAirDt);
                }
                newAir.Add(a);
            }

            List<BookingAir> leftA = SameFliter(newAir, DBBookingAir);
            // BookingAirSegmentList eMosAirSeqLst = CombinList(beforeA, leftA, afterA);
            beforeAir.AddRange(leftA);
            beforeAir.AddRange(afterAir);
            GetList(beforeAir, out airList, out airDetailList);
            return true;
        }

        //private bool GetAir(Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur, string eMosbookingRef, string IURBkgRef, string PNR, tbPEOAIRs dbAirList, tbPEOAIRDETAILs dbAirDetailList, bool isCreate, out tbPEOAIRs airList, out tbPEOAIRDETAILs airDetailList)
        //{
        //    airDetailList = new tbPEOAIRDETAILs();
        //    airList = new tbPEOAIRs();
        //    List<BookingAir> beforeAir = new List<BookingAir>();
        //    List<BookingAir> newAir = new List<BookingAir>();
        //    List<BookingAir> afterAir = new List<BookingAir>();

        //    //tbPEOAIRs oriIURAir = new tbPEOAIRs();

        //    tbPEOAIRDETAILs eMosDBFirstDetailList = da.GetFirstAirDetailList(iur.PEOMSTR.COMPANYCODE, eMosbookingRef);

        //    int index = 1;
        //    int startIndex1 = dbAirList.FindIndex(c => c.SourcePnr == IURBkgRef && c.SourceSystem == iur.PEOMSTR.SOURCESYSTEM);

        //    if (startIndex1 == -1)
        //        startIndex1 = dbAirList.Count;

        //    foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR db in dbAirList)
        //    {
        //        if (db.SourcePnr == IURBkgRef && db.SourceSystem == iur.PEOMSTR.SOURCESYSTEM)
        //        {
        //            //oriIURAir.Add(db);
        //            continue;
        //        }
        //        BookingAir a = new BookingAir();

        //        foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL dbDetail in dbAirDetailList)
        //        {
        //            if (db.SegNum == dbDetail.SegNum)
        //            {
        //                a.Details.Add(dbDetail);
        //            }
        //        }
        //        a.Header = db;



        //        if (beforeAir.Count >= startIndex1)
        //            afterAir.Add(a);
        //        else
        //            beforeAir.Add(a);
        //    }
        //    DateTime lastUpdateOn = DateTime.MinValue;
        //    foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR iurAir in iur.AirList)
        //    {
        //        if (iurAir.UpdateOn > lastUpdateOn)
        //            lastUpdateOn = iurAir.UpdateOn;
        //    }
        //    foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR iurAir in iur.AirList)
        //    {
        //        if (!isCreate && iurAir.UpdateOn != lastUpdateOn)
        //            continue;
        //        if (iurAir.DepartDATE == DateTime.MinValue || iurAir.ArrDATE == DateTime.MinValue)
        //            continue;

        //        BookingAir a = new BookingAir();
        //        Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR eMosAir = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIR(true);

        //        eMosAir.CompanyCode = iurAir.CompanyCode;

        //        eMosAir.SegNum = (index++).ToString().PadLeft(5, '0');

        //        eMosAir.PNR = iurAir.PNR;
        //        eMosAir.BkgRef = eMosbookingRef;
        //        eMosAir.SourcePnr = iurAir.BkgRef;

        //        eMosAir.Flight = iurAir.Flight;
        //        eMosAir.Class = iurAir.Class;
        //        eMosAir.DepartCity = iurAir.DepartCity;
        //        eMosAir.ArrivalCity = iurAir.ArrivalCity;
        //        eMosAir.Status = iurAir.Status;
        //        eMosAir.NumOfPax = iurAir.NumOfPax;
        //        eMosAir.DepartDATE = iurAir.DepartDATE;
        //        eMosAir.DepartTime = iurAir.DepartTime;
        //        eMosAir.ArrDATE = iurAir.ArrDATE;
        //        eMosAir.ArrTime = iurAir.ArrTime;
        //        eMosAir.ElapsedTime = iurAir.ElapsedTime;
        //        eMosAir.SSR = iurAir.SSR;
        //        eMosAir.Seat = iurAir.Seat;
        //        eMosAir.ReasonCode = iurAir.ReasonCode;
        //        eMosAir.StopOverNum = iurAir.StopOverNum;
        //        eMosAir.StopOverCity = iurAir.StopOverCity;
        //        eMosAir.Pcode = iurAir.Pcode;
        //        eMosAir.SpecialRQ = iurAir.SpecialRQ;
        //        eMosAir.RMK1 = iurAir.RMK1;
        //        eMosAir.RMK2 = iurAir.RMK2;
        //        eMosAir.INVRMK1 = iurAir.INVRMK1;
        //        eMosAir.INVRMK2 = iurAir.INVRMK2;
        //        eMosAir.VCHRMK1 = string.IsNullOrEmpty(iurAir.VCHRMK1) ? string.Empty : iurAir.VCHRMK1;
        //        eMosAir.VCHRMK2 = string.IsNullOrEmpty(iurAir.VCHRMK2) ? string.Empty : iurAir.VCHRMK2;
        //        eMosAir.SELLCURR = iurAir.SELLCURR;
        //        eMosAir.TOTALSELL = iurAir.TOTALSELL;
        //        eMosAir.TOTALSTAX = iurAir.TOTALSTAX;
        //        eMosAir.COSTCURR = iurAir.COSTCURR;
        //        eMosAir.TOTALCOST = iurAir.TOTALCOST;
        //        eMosAir.TOTALCTAX = iurAir.TOTALCTAX;
        //        //eMosAir.PNR = iurAir.PNR;
        //        eMosAir.SourceSystem = iurAir.SourceSystem;
        //        eMosAir.CreateOn = iurAir.CreateOn;
        //        eMosAir.CreateBy = iurAir.CreateBy;
        //        eMosAir.UpdateOn = DateTime.Now;
        //        eMosAir.UpdateBy = iurAir.UpdateBy;
        //        eMosAir.VoidOn = iurAir.VoidOn;
        //        eMosAir.VoidBy = iurAir.VoidBy;
        //        //eMosAir.GSTTAX = iurAir.GSTTAX;
        //        eMosAir.DepartTerminal = iurAir.DepartTerminal;
        //        eMosAir.ArrivalTerminal = iurAir.ArrivalTerminal;
        //        eMosAir.OtherAirlinePNR = iurAir.OtherAirlinePNR;
        //        //eMosAir.showDetailFee = iurAir.showDetailFee;
        //        eMosAir.EQP = iurAir.EQP;
        //        eMosAir.Service = iurAir.Service;
        //        eMosAir.suppcode = iurAir.Suppcode;
        //        //eMosAir.isRecalable = iurAir.isRecalable;

        //        eMosAir.IssuedDoc = GetIssuedDoc(dbAirList, iurAir);

        //        //eMosAirSeqLst.AirList.Insert(startIndex1++, eMosAir);
        //        a.Header = eMosAir;
        //        foreach (Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL iurAirDt in iur.AirDetailList)
        //        {
        //            if (iurAirDt.SegNum != iurAir.SegNum)
        //                continue;

        //            bool exist = false;

        //            if (iurAir.BkgRef == eMosbookingRef && iurAirDt.SegNum == "00001" && iurAirDt.SEQNUM == "00001" && iurAirDt.SEQTYPE == "SELL")
        //            {
        //                foreach (Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL airDt in eMosDBFirstDetailList)
        //                {
        //                    if (iurAirDt.AGETYPE == airDt.AGETYPE)
        //                    {
        //                        a.Details.Add(airDt);
        //                        exist = true;
        //                        break;
        //                    }
        //                }
        //            }
        //            if (exist)
        //                continue;



        //            Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL eMosAirDt = new Westminster.MOE.Entity.eMosDBEntity.Table.tbPEOAIRDETAIL(true);

        //            eMosAirDt.CompanyCode = iurAirDt.CompanyCode;

        //            eMosAirDt.SegNum = (index).ToString().PadLeft(5, '0');
        //            eMosAirDt.SEQNUM = iurAirDt.SEQNUM;
        //            eMosAirDt.SEQTYPE = iurAirDt.SEQTYPE;

        //            eMosAirDt.BkgRef = eMosbookingRef;

        //            eMosAirDt.AGETYPE = iurAirDt.AGETYPE;
        //            eMosAirDt.QTY = iurAirDt.QTY;
        //            eMosAirDt.CURR = iurAirDt.CURR;
        //            eMosAirDt.AMT = iurAirDt.AMT;
        //            eMosAirDt.TAXCURR = iurAirDt.TAXCURR;
        //            eMosAirDt.TAXAMT = iurAirDt.TAXAMT;
        //            eMosAirDt.CREATEON = iurAirDt.CREATEON;
        //            eMosAirDt.CREATEBY = iurAirDt.CREATEBY;
        //            eMosAirDt.UPDATEON = DateTime.Now;
        //            eMosAirDt.UPDATEBY = iurAirDt.UPDATEBY;
        //            //eMosAirDt.AMTFEE = iurAirDt.AMTFEE;
        //            //eMosAirDt.DISCOUNT_MARKUP_PER = iurAirDt.DISCOUNT_MARKUP_PER;
        //            //eMosAirDt.INVNETTGROSS = iurAirDt.INVNETTGROSS;	

        //            //eMosAirSeqLst.AirDetailList.Insert(startIndex2++, eMosAirDt);
        //            a.Details.Add(eMosAirDt);
        //        }
        //        newAir.Add(a);
        //    }

        //    List<BookingAir> leftA = SameFliter(newAir);
        //    // BookingAirSegmentList eMosAirSeqLst = CombinList(beforeA, leftA, afterA);
        //    beforeAir.AddRange(leftA);
        //    beforeAir.AddRange(afterAir);
        //    GetList(beforeAir, out airList, out airDetailList);
        //    return true;
        //}

        private string GetIssuedDoc(tbPEOAIRs oriIURAir, Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR iurAir)
        {
            return oriIURAir.Exists(c => c.Flight == iurAir.Flight && c.Class == iurAir.Class && c.DepartCity == iurAir.DepartCity && c.ArrivalCity == iurAir.ArrivalCity && c.SourcePnr == iurAir.BkgRef && c.IssuedDoc == "Y") ? "Y" : "N";
        }

        private void GetList(List<BookingAir> beforeA, out tbPEOAIRs airList, out tbPEOAIRDETAILs airDetailList)
        {
            airDetailList = new tbPEOAIRDETAILs();
            airList = new tbPEOAIRs();
            int index = 1;

            foreach (BookingAir a in beforeA)
            {
                a.Header.SegNum = (index).ToString().PadLeft(5, '0');
                foreach (tbPEOAIRDETAIL ad in a.Details)
                {
                    ad.SegNum = (index).ToString().PadLeft(5, '0');
                }
                airDetailList.AddRange(a.Details);
                airList.Add(a.Header);
                index++;
            }

        }

        //private BookingAirSegmentList CombinList(List<BookingAir> beforeA, List<BookingAir> leftA, List<BookingAir> afterA)
        //{
        //    BookingAirSegmentList eMosAirSeqLst = new BookingAirSegmentList();
        //    beforeA.AddRange(leftA);
        //    beforeA.AddRange(afterA);

        //    int index = 1;

        //    foreach (BookingAir a in beforeA)
        //    {
        //        a.Air.SegNum = (index).ToString().PadLeft(5, '0');
        //        foreach (tbPEOAIRDETAIL ad in a.AirDetailList)
        //        {
        //            ad.SegNum = (index).ToString().PadLeft(5, '0');
        //        }

        //        eMosAirSeqLst.AirList.Add(a.Air);
        //        eMosAirSeqLst.AirDetailList.AddRange(a.AirDetailList);
        //        index++;
        //    }
        //    return eMosAirSeqLst;
        //}
        private List<BookingAir> SameFliter(List<BookingAir> newA, List<BookingAir> DBBookingAir)
        {
            //  List<BookingAir> leftA = new List<BookingAir>();
            List<BookingAir> ignoreList = new List<BookingAir>();
            foreach (BookingAir airSeg in newA)
            {
                if (ignoreList.Contains(airSeg))
                    continue;
                DateTime updateon = DateTime.MinValue;
                BookingAir keep = null;

                List<BookingAir> sameAirs = newA.FindAll(c => (
                                                                               c.Header.ArrivalCity == airSeg.Header.ArrivalCity
                                                                            && c.Header.BkgRef == airSeg.Header.BkgRef
                                                                            && MOEFormats.EqualsIgnoreCase(c.Header.Class, airSeg.Header.Class)
                                                                            && c.Header.CompanyCode == airSeg.Header.CompanyCode
                                                                            && c.Header.DepartCity == airSeg.Header.DepartCity
                                                                            && c.Header.DepartDATE == airSeg.Header.DepartDATE
                                                                            && MOEFormats.EqualsIgnoreCase(c.Header.Flight, airSeg.Header.Flight)
                                                                          ));

                foreach (BookingAir bas in sameAirs)
                {
                    if (bas.Header.UpdateOn > updateon)
                    {
                        updateon = bas.Header.CreateOn;
                        keep = bas;
                    }
                    ignoreList.Add(bas);
                }

                if (keep.Equals(airSeg))
                {
                    BookingAir dbair = DBBookingAir.Find(
                               c => MOEFormats.EqualsIgnoreCase(c.Header.SourcePnr, keep.Header.SourcePnr)
                            && MOEFormats.EqualsIgnoreCase(c.Header.Flight, keep.Header.Flight)
                            && MOEFormats.EqualsIgnoreCase(c.Header.Class, keep.Header.Class)
                            );
                    if (dbair != null)
                    {
                        foreach (tbPEOAIRDETAIL dt in keep.Details)
                        {
                            if (dbair.Details.Exists(
                                d => d.AGETYPE == dt.AGETYPE
                                && d.SEQTYPE == dt.SEQTYPE
                                ))
                            {
                                continue;
                            }
                            else
                            {
                                dt.SEQNUM = (dbair.Details.Count + 1).ToString().PadLeft(5, '0');
                                dbair.Details.Add(dt);
                            }
                        }
                    }
                    else
                    {
                        DBBookingAir.Add(keep);
                    }
                }
                //   leftA.Add(keep);
            }
            //    return leftA;
            return DBBookingAir;
        }
        private string GetTicketDestination(string xRouting, string xDestination)
        {
            string myDestination = xDestination;
            string myFirstDest = "";
            string myLastDest = "";

            if (myDestination.Length == 0 && xRouting.Length > 6)
            {
                myFirstDest = xRouting.Substring(0, 3);
                myLastDest = xRouting.Substring(xRouting.Length - 3, 3);

                if (xRouting.Length == 11)
                {

                    if (myFirstDest == myLastDest)
                    {
                        myDestination = xRouting.Substring(4, 3);
                    }
                    else
                    {
                        myDestination = myLastDest;
                    }
                }
                else
                {
                    if (myFirstDest != myLastDest)
                    {
                        myDestination = myLastDest;
                    }

                }
            }

            return myDestination;

        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (Conn != null)
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        #endregion
    }
}
