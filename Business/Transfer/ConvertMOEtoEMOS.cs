﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.BLL.Transfer
{
    public class ConvertMOEtoEMOS
    {
        public static UO_CUSTOMER ToEMOS_Customer(tbCustomer moeObj)
        {
            if (moeObj == null)
                return null;
            UO_CUSTOMER emosObj = new UO_CUSTOMER();

            emosObj.ADDR1 = moeObj.ADDR1;
            emosObj.ADDR2 = moeObj.ADDR2;
            emosObj.ADDR3 = moeObj.ADDR3;
            emosObj.ADDR4 = moeObj.ADDR4;
            emosObj.ADDR5 = moeObj.ADDR5;
            emosObj.BILLADDR1 = moeObj.BILLADDR1;
            emosObj.BILLADDR2 = moeObj.BILLADDR2;
            emosObj.BILLADDR3 = moeObj.BILLADDR3;
            emosObj.BILLADDR4 = moeObj.BILLADDR4;
            emosObj.BILLADDR5 = moeObj.BILLADDR5;
            emosObj.BILLCITY = moeObj.BILLCITY;
            emosObj.BILLCOUNTRY = moeObj.BILLCOUNTRY;
            emosObj.BILLPOSTAL = moeObj.BILLPOSTAL;
            emosObj.BRANCHCODE = moeObj.BRANCHCODE;
            emosObj.CITY = moeObj.CITY;
            emosObj.CLTCODE = moeObj.CLTCODE;
            emosObj.CLTNAME = moeObj.CLTNAME;
            emosObj.CLTNAME1 = string.Empty;
            emosObj.COMPANYCODE = moeObj.COMPANYCODE;
            emosObj.CONTACT1 = moeObj.CONTACT1;
            emosObj.CONTACT2 = moeObj.CONTACT2;
            emosObj.COUNTRY = moeObj.COUNTRY;
            emosObj.CREATEBY = moeObj.CREATEBY;
            emosObj.CREATEON = moeObj.CREATEON;
            emosObj.CRTERMS = moeObj.CRTERMS;
            emosObj.CURRENCY = moeObj.CURRENCY;
            emosObj.CustRemarks = moeObj.CustRemarks;
            emosObj.email = moeObj.email;
            emosObj.FAX1 = moeObj.FAX1;
            emosObj.GEMSCODE = moeObj.GEMSCODE;
            emosObj.JOBTITLE1 = moeObj.JOBTITLE1;
            emosObj.JOBTITLE2 = moeObj.JOBTITLE2;
            emosObj.MSTRCLT = moeObj.MSTRCLT;
            emosObj.PFDAY = moeObj.PFDAY;
            emosObj.PHONE1 = moeObj.PHONE1;
            emosObj.POSTAL = moeObj.POSTAL;
            emosObj.PRFTDOWN = moeObj.PRFTDOWN;
            emosObj.PRFTUP = moeObj.PRFTUP;
            emosObj.STATUS = moeObj.STATUS;
            emosObj.TEAMCODE = moeObj.TEAMCODE;
            emosObj.UPDATEBY = moeObj.UPDATEBY;
            emosObj.UPDATEON = moeObj.UPDATEON;
            return emosObj;
        }
        public static UO_PeoMstr ToEMOS_PeoMstr(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOMSTR moeObj)
        {
            UO_PeoMstr emosObj = new UO_PeoMstr();
         
            emosObj.COMPANYCODE = moeObj.COMPANYCODE;
            emosObj.BKGREF = moeObj.BKGREF;
            emosObj.PNR = moeObj.PNR;
            emosObj.CRSSINEIN = moeObj.CRSSINEIN;
            emosObj.COD1 = moeObj.COD1;
            emosObj.COD2 = moeObj.COD2;
            emosObj.CLTODE = moeObj.CLTODE;
            emosObj.CLTNAME = moeObj.CLTNAME;
            emosObj.CLTADDR = moeObj.CLTADDR;
            emosObj.STAFFCODE = moeObj.STAFFCODE;
            emosObj.TEAMCODE = moeObj.TEAMCODE;
            emosObj.FIRSTPAX = moeObj.FIRSTPAX;
            emosObj.PHONE = moeObj.PHONE;
            emosObj.FAX = moeObj.FAX;
            emosObj.EMAIL = moeObj.EMAIL;
            emosObj.MASTERPNR = moeObj.MASTERPNR;
            emosObj.TOURCODE = moeObj.TOURCODE;
            emosObj.CONTACTPERSON = moeObj.CONTACTPERSON;
            emosObj.DEPARTDATE = moeObj.DEPARTDATE;
            emosObj.TTLSELLCURR = moeObj.TTLSELLCURR;
            emosObj.TTLSELLAMT = moeObj.TTLSELLAMT;
            emosObj.TTLSELLTAXCURR = moeObj.TTLSELLTAXCURR;
            emosObj.TTLSELLTAX = moeObj.TTLSELLTAX;
            emosObj.TTLCOSTCURR = moeObj.TTLCOSTCURR;
            emosObj.TTLCOSTAMT = moeObj.TTLCOSTAMT;
            emosObj.TTLCOSTTAXCURR = moeObj.TTLCOSTTAXCURR;
            emosObj.TTLCOSTTAX = moeObj.TTLCOSTTAX;
            emosObj.INVCURR = moeObj.INVCURR;
            emosObj.INVAMT = moeObj.INVAMT;
            emosObj.INVTAXCURR = moeObj.INVTAXCURR;
            emosObj.INVTAX = moeObj.INVTAX;
            emosObj.DOCCURR = moeObj.DOCCURR;
            emosObj.DOCAMT = moeObj.DOCAMT;
            emosObj.DOCTAXCURR = moeObj.DOCTAXCURR;
            emosObj.DOCTAX = moeObj.DOCTAX;
            emosObj.INVCOUNT = moeObj.INVCOUNT;
            emosObj.DOCCOUNT = moeObj.DOCCOUNT;
            emosObj.RMK1 = moeObj.RMK1;
            emosObj.RMK2 = moeObj.RMK2;
            emosObj.RMK3 = moeObj.RMK3;
            emosObj.RMK4 = moeObj.RMK4;
            emosObj.DEADLINE = moeObj.DEADLINE;
            emosObj.SOURCESYSTEM = moeObj.SOURCESYSTEM;
            emosObj.SOURCE = moeObj.SOURCE;
            emosObj.CREATEON = moeObj.CREATEON;
            emosObj.CREATEBY = moeObj.CREATEBY;
            emosObj.UPDATEON = moeObj.UPDATEON;
            emosObj.UPDATEBY = moeObj.UPDATEBY;
            emosObj.COD3 = moeObj.COD3;
            emosObj.INSFLAG = moeObj.INSFLAG;
            emosObj.TOEMOSON = moeObj.TOEMOSON;
            emosObj.TOEMOSBY = moeObj.TOEMOSBY;
            emosObj.EMOSBKGREF = moeObj.EMOSBKGREF;
            emosObj.IURFLAG = moeObj.IURFLAG;
            emosObj.pseudo = moeObj.pseudo;
            emosObj.pseudo_inv = moeObj.pseudo_inv;
            emosObj.CRSSINEIN_inv = moeObj.CRSSINEIN_inv;
            emosObj.agency_arc_iata_num = moeObj.agency_arc_iata_num;
            emosObj.iss_off_code = moeObj.iss_off_code;
            emosObj.tranflag = moeObj.tranflag;

            return emosObj;
        }
        public static UO_PEOAIR ToEMOS_PeoAir(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR moeObj)
        {
            UO_PEOAIR emosObj = new UO_PEOAIR();
            emosObj.CompanyCode = moeObj.CompanyCode;
            emosObj.BkgRef = moeObj.BkgRef;
            emosObj.SegNum = moeObj.SegNum;
            emosObj.PNR = moeObj.PNR;
            emosObj.Flight = moeObj.Flight;
            emosObj.Class = moeObj.Class;
            emosObj.DepartCity = moeObj.DepartCity;
            emosObj.ArrivalCity = moeObj.ArrivalCity;
            emosObj.Status = moeObj.Status;
            emosObj.NumOfPax = moeObj.NumOfPax;
            emosObj.DepartDATE = moeObj.DepartDATE;
            emosObj.DepartTime = moeObj.DepartTime;
            emosObj.ArrDATE = moeObj.ArrDATE;
            emosObj.ArrTime = moeObj.ArrTime;
            emosObj.ElapsedTime = moeObj.ElapsedTime;
            emosObj.SSR = moeObj.SSR;
            emosObj.Seat = moeObj.Seat;
            emosObj.ReasonCode = moeObj.ReasonCode;
            emosObj.StopOverNum = moeObj.StopOverNum;
            emosObj.StopOverCity = moeObj.StopOverCity;
            emosObj.Pcode = moeObj.Pcode;
            emosObj.SpecialRQ = moeObj.SpecialRQ;
            emosObj.RMK1 = moeObj.RMK1;
            emosObj.RMK2 = moeObj.RMK2;
            emosObj.INVRMK1 = moeObj.INVRMK1;
            emosObj.INVRMK2 = moeObj.INVRMK2;
            emosObj.VCHRMK1 = moeObj.VCHRMK1;
            emosObj.VCHRMK2 = moeObj.VCHRMK2;
            emosObj.SELLCURR = moeObj.SELLCURR;
            emosObj.TOTALSELL = moeObj.TOTALSELL;
            emosObj.TOTALSTAX = moeObj.TOTALSTAX;
            emosObj.COSTCURR = moeObj.COSTCURR;
            emosObj.TOTALCOST = moeObj.TOTALCOST;
            emosObj.TOTALCTAX = moeObj.TOTALCTAX;
            emosObj.SourceSystem = moeObj.SourceSystem;
            emosObj.CreateOn = moeObj.CreateOn;
            emosObj.CreateBy = moeObj.CreateBy;
            emosObj.UpdateOn = moeObj.UpdateOn;
            emosObj.UpdateBy = moeObj.UpdateBy;
            emosObj.VoidOn = moeObj.VoidOn;
            emosObj.VoidBy = moeObj.VoidBy;
            emosObj.AirSeg = moeObj.AirSeg;
            emosObj.DepartTerminal = moeObj.DepartTerminal;
            emosObj.ArrivalTerminal = moeObj.ArrivalTerminal;
            emosObj.OtherAirlinePNR = moeObj.OtherAirlinePNR;
            emosObj.EQP = moeObj.EQP;
            emosObj.Service = moeObj.Service;
            emosObj.Suppcode = moeObj.Suppcode;

            return emosObj;
        }
        public static UO_PEOAIRDETAIL ToEMOS_PeoAirDetail(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL moeObj)
        {
            UO_PEOAIRDETAIL emosObj = new UO_PEOAIRDETAIL();

            emosObj.CompanyCode = moeObj.CompanyCode;
            emosObj.BkgRef = moeObj.BkgRef;
            emosObj.SegNum = moeObj.SegNum;
            emosObj.SEQNUM = moeObj.SEQNUM;
            emosObj.SEQTYPE = moeObj.SEQTYPE;
            emosObj.QTY = moeObj.QTY;
            emosObj.CURR = moeObj.CURR;
            emosObj.AMT = moeObj.AMT;
            emosObj.TAXCURR = moeObj.TAXCURR;
            emosObj.TAXAMT = moeObj.TAXAMT;
            emosObj.CREATEON = moeObj.CREATEON;
            emosObj.CREATEBY = moeObj.CREATEBY;
            emosObj.UPDATEON = moeObj.UPDATEON;
            emosObj.UPDATEBY = moeObj.UPDATEBY;
            emosObj.AGETYPE = moeObj.AGETYPE;

            return emosObj;
        }
        public static UO_PeoPax ToEMOS_PeoPax(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX moeObj)
        {
            UO_PeoPax emosObj = new UO_PeoPax();

            emosObj.BKGREF = moeObj.BKGREF;
            emosObj.COMPANYCODE = moeObj.COMPANYCODE;
            emosObj.SEGNUM = moeObj.SEGNUM;
            emosObj.PAXNUM = moeObj.PAXNUM;
            emosObj.PAXLNAME = moeObj.PAXLNAME;
            emosObj.PAXFNAME = moeObj.PAXFNAME;
            emosObj.PAXTITLE = moeObj.PAXTITLE;
            emosObj.PAXTYPE = moeObj.PAXTYPE;
            emosObj.PAXAIR = moeObj.PAXAIR;
            emosObj.PAXHOTEL = moeObj.PAXHOTEL;
            emosObj.PAXOTHER = moeObj.PAXOTHER;
            emosObj.PAXAGE = moeObj.PAXAGE;
            emosObj.RMK1 = moeObj.RMK1;
            emosObj.RMK2 = moeObj.RMK2;
            emosObj.VOIDON = moeObj.VOIDON;
            emosObj.VOIDBY = moeObj.VOIDBY;
            emosObj.SOURCESYSTEM = moeObj.SOURCESYSTEM;
            emosObj.CREATEON = moeObj.CREATEON;
            emosObj.CREATEBY = moeObj.CREATEBY;
            emosObj.UPDATEON = moeObj.UPDATEON;
            emosObj.UPDATEBY = moeObj.UPDATEBY;

            return emosObj;
        }

        public static UO_PeoTkt ToEMOS_PeoTkt(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT moeObj)
        {
            UO_PeoTkt emosObj = new UO_PeoTkt();
            emosObj.CompanyCode = moeObj.CompanyCode;
            emosObj.Ticket = moeObj.Ticket;
            emosObj.TktSeq = moeObj.TktSeq;
            emosObj.BKGREF = moeObj.BKGREF;
            emosObj.PNR = moeObj.PNR;
            emosObj.CltCode = moeObj.CltCode;
            emosObj.SuppCode = moeObj.SuppCode;
            emosObj.InvNum = moeObj.InvNum;
            emosObj.Jqty = moeObj.Jqty;
            emosObj.AirCode = moeObj.AirCode;
            emosObj.Airline = moeObj.Airline;
            emosObj.PaxName = moeObj.PaxName;
            emosObj.DepartOn = moeObj.DepartOn;
            emosObj.COD1 = moeObj.COD1;
            emosObj.COD2 = moeObj.COD2;
            emosObj.COD3 = moeObj.COD3;
            //emosObj.COD7 = moeObj.COD7;
            emosObj.Commission = moeObj.Commission;
            emosObj.LocalFareCurr = moeObj.LocalFareCurr;
            emosObj.LocalFareAmt = moeObj.LocalFareAmt;
            emosObj.ForeignFareCurr = moeObj.ForeignFareCurr;
            emosObj.ForeignFareAmt = moeObj.ForeignFareAmt;
            emosObj.Discount = moeObj.Discount;
            emosObj.FormPay = moeObj.FormPay;
            emosObj.Routing = moeObj.Routing;
            emosObj.FullCurr = moeObj.FullCurr;
            emosObj.FullFare = moeObj.FullFare;
            emosObj.PaidCurr = moeObj.PaidCurr;
            emosObj.PaidFare = moeObj.PaidFare;
            emosObj.LowCurr = moeObj.LowCurr;
            emosObj.LowFare = moeObj.LowFare;
            emosObj.NetCurr = moeObj.NetCurr;
            emosObj.NetFare = moeObj.NetFare;
            emosObj.FareBasis = moeObj.FareBasis;
            emosObj.FareType = moeObj.FareType;
            emosObj.TotalTax = moeObj.TotalTax;
            emosObj.Security = moeObj.Security;
            emosObj.QSurcharge = moeObj.QSurcharge;
            emosObj.BranchCode = moeObj.BranchCode;
            emosObj.TeamCode = moeObj.TeamCode;
            emosObj.IssueOn = moeObj.IssueOn;
            emosObj.IssueBy = moeObj.IssueBy;
            emosObj.CreateOn = moeObj.CreateOn;
            emosObj.CreateBy = moeObj.CreateBy;
            emosObj.UpdateOn = moeObj.UpdateOn;
            emosObj.UpdateBy = moeObj.UpdateBy;
            emosObj.VoidOn = moeObj.VoidOn;
            emosObj.VoidBy = moeObj.VoidBy;
            emosObj.voidTeam = moeObj.voidTeam;
            emosObj.voidreason = moeObj.voidreason;
            emosObj.TaxAmount1 = moeObj.TaxAmount1;
            emosObj.TaxID1 = moeObj.TaxID1;
            emosObj.TaxAmount2 = moeObj.TaxAmount2;
            emosObj.TaxID2 = moeObj.TaxID2;
            emosObj.TaxAmount3 = moeObj.TaxAmount3;
            emosObj.TaxID3 = moeObj.TaxID3;
            emosObj.FareCalType = moeObj.FareCalType;
            emosObj.CompressPrintInd = moeObj.CompressPrintInd;
            emosObj.FareCalData = moeObj.FareCalData;
            emosObj.pseudo = moeObj.pseudo;
            emosObj.sinein = moeObj.sinein;
            emosObj.AirSeg = moeObj.AirSeg;
            emosObj.TransferFlag = moeObj.TransferFlag;
            emosObj.nst = moeObj.nst;
            emosObj.SOURCESYSTEM = moeObj.SOURCESYSTEM;
            emosObj.commission_per = moeObj.commission_per;
            emosObj.add_payment = moeObj.add_payment;
            emosObj.iss_off_code = moeObj.iss_off_code;
            emosObj.tour_code = moeObj.tour_code;
            emosObj.org_iss = moeObj.org_iss;
            emosObj.iss_in_exchange = moeObj.iss_in_exchange;
            emosObj.ControlNum = moeObj.ControlNum;
            emosObj.ENDORSEMENTS = moeObj.ENDORSEMENTS;
            emosObj.AirReason = moeObj.AirReason;
            emosObj.Destination = moeObj.Destination;
            emosObj.INTDOM = moeObj.INTDOM;
            emosObj.PAXAIR = moeObj.PAXAIR;
            emosObj.Class = moeObj.Class;
            emosObj.tkttype = moeObj.tkttype;
            emosObj.SellCurr = moeObj.SellCurr;
            emosObj.SellFare = moeObj.SellFare;
            emosObj.CorpCurr = moeObj.CorpCurr;
            emosObj.CorpFare = moeObj.CorpFare;
            emosObj.COD4 = moeObj.COD4;
            emosObj.COD5 = moeObj.COD5;
            emosObj.COD6 = moeObj.COD6;
            emosObj.tranflag = moeObj.tranflag;

            return emosObj;
        }

        public static UO_PeoTktDetail ToEMOS_PeoTktDt(Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAIL moeObj)
        {
            UO_PeoTktDetail emosObj = new UO_PeoTktDetail();

            emosObj.CompanyCode = moeObj.CompanyCode;
            emosObj.Ticket = moeObj.Ticket;
            emosObj.AirCode = moeObj.AirCode;
            emosObj.TktSeq = moeObj.TktSeq;
            emosObj.SegNum = moeObj.SegNum;
            emosObj.Airline = moeObj.Airline;
            emosObj.Flight = moeObj.Flight;
            emosObj.Class = moeObj.Class;
            emosObj.Seat = moeObj.Seat;
            emosObj.DepartCity = moeObj.DepartCity;
            emosObj.ArrivalCity = moeObj.ArrivalCity;
            emosObj.DepartDATE = moeObj.DepartDATE;
            emosObj.DepartTime = moeObj.DepartTime;
            emosObj.ArrDATE = moeObj.ArrDATE;
            emosObj.ArrTime = moeObj.ArrTime;
            emosObj.ElapsedTime = moeObj.ElapsedTime;
            emosObj.StopOverNum = moeObj.StopOverNum;
            emosObj.StopOverCity = moeObj.StopOverCity;
            emosObj.FareAmt = moeObj.FareAmt;
            emosObj.FareBasic = moeObj.FareBasic;
            emosObj.Commission = moeObj.Commission;
            emosObj.ValidStart = moeObj.ValidStart;
            emosObj.ValidEnd = moeObj.ValidEnd;
            emosObj.CreateOn = moeObj.CreateOn;
            emosObj.CreateBy = moeObj.CreateBy;
            emosObj.UpdateOn = moeObj.UpdateOn;
            emosObj.UpdateBy = moeObj.UpdateBy;
            emosObj.EQP = moeObj.EQP;
            emosObj.Service = moeObj.Service;
            emosObj.status = moeObj.status;
            emosObj.fare_basis_code = moeObj.fare_basis_code;
            emosObj.DepartTerm = moeObj.DepartTerm;
            emosObj.ArrivalTerm = moeObj.ArrivalTerm;
            emosObj.freq_fight_num = moeObj.freq_fight_num;
            emosObj.AirlinePNR = moeObj.AirlinePNR;

            return emosObj;
        }

       
    }
}
