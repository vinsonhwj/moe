﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using Westminster.MOE.Entity.BLLEntity.Transfer;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Transfer;
using Westminster.MOE.Lib;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DAL.Transfer;
using Westminster.MOE.IDAL.Maintain;
using Westminster.MOE.DAL.Maintain;
namespace Westminster.MOE.BLL.Transfer
{
    public class AutoInvoice
    {
        public Config config { get; set; }
        private IAutoDocument da { get; set; }
        private BLL.Maintain.Currency BOCurr { get; set; }

        public InvoiceTypeEnum InvoiceType = InvoiceTypeEnum.Normal;
        public Invoice_Types InvType = Invoice_Types.Normal;
        public eMosParameter eMosParam = new eMosParameter();

        public string TSAno = "";
        private int invSegnum = 0;

        public AutoInvoice(Config config)
            : this(config, new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos]))
        {

        }
        public AutoInvoice(Config config, IConnection conn)
            : this(config, new DAAutoDocument(conn, config.WebServiceList[MOEEnums.BindingName.IURSoap]), new DACurrency(conn))
        {

        }
        public AutoInvoice(Config config, IAutoDocument autoDocument, IDACurrency curr)
        {
            this.config = config;
            da = autoDocument;
            BOCurr = new Westminster.MOE.BLL.Maintain.Currency(config, curr);
        }
        public string IssueAutoInvoice4IUR(eMosBooking booking, string invrmk, string staffcode, string teamcode, List<AirlineSegment> airSegment4Invoice)
        {

            if (config.TsfrSettings.IsForWebjet && da.IsExistedInvoice(booking.CompanyCode, booking.Bkgref))
            {
                return "";
            }
            else
            {
                Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice = ConstructInvoice4AutoBooking(booking, invrmk, staffcode, teamcode, airSegment4Invoice);
                IssueAutoInvoice(invoice, InvoiceType, InvType);
                return invoice.PeoInv.INVNUM;
            }
        }
        public string IssueAutoInvoice(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, InvoiceTypeEnum InvoiceType, Invoice_Types InvType)
        {
            if (invoice == null || invoice.PeoInv == null) return "";
            List<string> listInvoice = new List<string>();
            string myInvoiceNo = da.InsertTsfrInvoice(invoice, InvoiceType, InvType);

            string myTeamCode = invoice.PeoInv.TEAMCODE + "";
            listInvoice.Add(myInvoiceNo);
            da.CallProfitMarginChecking(invoice.PeoInv.COMPANYCODE, invoice.PeoInv.BKGREF, invoice.PeoInv.STAFFCODE, myTeamCode);
            da.CallRecalTicketPaidFare(listInvoice, invoice.PeoInv.COMPANYCODE);

            return myInvoiceNo;

        }
        public Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice ConstructInvoice4AutoBooking(eMosBooking booking, string invrmk, string staffcode, string teamcode, List<AirlineSegment> airSegment4Invoice)
        {
            Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice = new Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice();
            SetPeoinv(invoice, booking, invrmk, staffcode, teamcode);
            SetPeoPax(invoice, booking);
            SetAirSegment4IUR(invoice, booking, airSegment4Invoice);
            // SetHotelSegment(invoice, booking);
            if (InvType == Invoice_Types.UATP)
            {
                SetTkt(invoice, booking);
            }
            else
            {
                SetTktref(invoice, booking);
            }
            SetOtherSegment(invoice, booking);
            return invoice;
        }

        public void SetOtherSegment(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking)
        {
            if (booking == null || booking.OtherSegments == null) return;

            tbPEOINV UO_PEOINV = invoice.PeoInv;
            tbPEOINVSEG myUO_PEOINVSEG = null;
            tbPEOINVDETAIL myUO_PEOINVDETAIL = null;
            tbPEOINVDETAILs myUOList_PEOINVDETAIL = null;
            string mySellCurr = UO_PEOINV.SELLCURR;
            string localcurr = UO_PEOINV.SELLCURR;
            int seqNum = 0;

            foreach (eMosOtherSegment myeMosOtherSegment in booking.OtherSegments)
            {
                myUO_PEOINVSEG = new tbPEOINVSEG();
                myUO_PEOINVSEG.COMPANYCODE = UO_PEOINV.COMPANYCODE;
                myUO_PEOINVSEG.INVNUM = "";
                myUO_PEOINVSEG.SEGNUM = Convert.ToString(++invSegnum).PadLeft(5, '0');
                myUO_PEOINVSEG.SERVTYPE = "OTH";
                myUO_PEOINVSEG.SERVNAME = "";
                myUO_PEOINVSEG.SERVDESC = myeMosOtherSegment.PeoOther.masterDesc;

                myUO_PEOINVSEG.CREATEON = UO_PEOINV.CREATEON;
                myUO_PEOINVSEG.CREATEBY = UO_PEOINV.CREATEBY;
                myUO_PEOINVSEG.GSTAMT = 0;
                myUO_PEOINVSEG.SEGTYPE = myeMosOtherSegment.PeoOther.SEGTYPE;
                //myUO_PEOINVSEG.ReasonCode = myeMosHotelSegment.PEOHTL.ReasonCode + "";
                invoice.PEOINVSEG.Add(myUO_PEOINVSEG);

                seqNum = 0;
                if (myeMosOtherSegment.PeoOtherDetails != null)
                {
                    foreach (tbPEOOTHERDETAIL myUO_PEOOTHERDETAIL in myeMosOtherSegment.PeoOtherDetails)
                    {
                        if (Convert.ToString(myUO_PEOOTHERDETAIL.SEQTYPE + "").Trim().ToUpper() == "SELL")
                        {
                            myUO_PEOINVDETAIL = new tbPEOINVDETAIL();
                            myUO_PEOINVDETAIL.COMPANYCODE = UO_PEOINV.COMPANYCODE;
                            myUO_PEOINVDETAIL.CREATEBY = UO_PEOINV.CREATEBY;
                            myUO_PEOINVDETAIL.CREATEON = UO_PEOINV.CREATEON;
                            myUO_PEOINVDETAIL.DETLTYPE = myUO_PEOOTHERDETAIL.AGETYPE;
                            myUO_PEOINVDETAIL.ITEMNAME = myUO_PEOOTHERDETAIL.AGETYPE;
                            myUO_PEOINVDETAIL.INVNUM = "";
                            myUO_PEOINVDETAIL.QTY = myUO_PEOOTHERDETAIL.QTY;
                            //myUO_PEOINVDETAIL.RATENAME = myUO_PEOOTHERDETAIL.RATENAME + "";
                            //myUO_PEOINVDETAIL.RMNTS = Convert.ToInt32(myUO_PEOOTHERDETAIL.RMNTS);
                            myUO_PEOINVDETAIL.SEGNUM = myUO_PEOINVSEG.SEGNUM + "";
                            myUO_PEOINVDETAIL.SEQNUM = Convert.ToInt32(++seqNum).ToString().PadLeft(5, '0');
                            myUO_PEOINVDETAIL.SELLAMT = myUO_PEOOTHERDETAIL.AMT;
                            myUO_PEOINVDETAIL.SELLCURR = myUO_PEOOTHERDETAIL.CURR + "";
                            //myUO_PEOINVDETAIL.SHOWDETAILFEE = myUO_PEOOTHERDETAIL.ShowDetailFee;
                            myUO_PEOINVDETAIL.TAXAMT = myUO_PEOOTHERDETAIL.TAXAMT;
                            myUO_PEOINVDETAIL.TAXCURR = UO_PEOINV.SELLCURR + "";
                            //myUO_PEOINVDETAIL.UNITPRICE = 0;
                            myUO_PEOINVDETAIL.UPDATEBY = UO_PEOINV.CREATEBY;
                            myUO_PEOINVDETAIL.UPDATEON = UO_PEOINV.CREATEON;
                            if (mySellCurr.Trim() != localcurr.Trim())
                            {
                                myUO_PEOINVDETAIL.HKDAMT = BOCurr.CalculateAmount(UO_PEOINV.COMPANYCODE, localcurr, mySellCurr, myUO_PEOINVDETAIL.SELLAMT + myUO_PEOINVDETAIL.TAXAMT);
                                //myUO_PEOINVDETAIL.HKDAMT = BO_EXRATE.Exrate(UO_PEOINV.COMPANYCODE, mySellCurr, myUO_PEOINVDETAIL.SELLAMT + myUO_PEOINVDETAIL.TAXAMT, localcurr);
                            }
                            else
                            {
                                myUO_PEOINVDETAIL.HKDAMT = myUO_PEOINVDETAIL.SELLAMT + myUO_PEOINVDETAIL.TAXAMT;
                            }
                            invoice.PEOINVDETAIL.Add(myUO_PEOINVDETAIL);

                            UO_PEOINV.TAXAMT += myUO_PEOINVDETAIL.TAXAMT;
                            UO_PEOINV.SELLAMT += myUO_PEOINVDETAIL.SELLAMT + myUO_PEOINVDETAIL.TAXAMT;
                            UO_PEOINV.SellExcludeTax = UO_PEOINV.SELLAMT - UO_PEOINV.TAXAMT;
                            UO_PEOINV.HKDAMT += myUO_PEOINVDETAIL.HKDAMT;
                            UO_PEOINV.invdtsum = UO_PEOINV.SELLAMT;
                        }
                    }
                }
            }
        }

        public void SetTktref(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking)
        {
            if (booking == null) return;
            tbPEOINV uO_PEOINV = invoice.PeoInv;
            if (booking.TicketSegments != null)
            {
                foreach (eMosTicketSegment myeMosTicketSegment in booking.TicketSegments)
                {
                    tbPEOINVTKTref myUO_PEOINVTKTref = new tbPEOINVTKTref();
                    myUO_PEOINVTKTref.CompanyCode = uO_PEOINV.COMPANYCODE;
                    myUO_PEOINVTKTref.InvNum = "";
                    myUO_PEOINVTKTref.BkgRef = uO_PEOINV.BKGREF;
                    if (myeMosTicketSegment.PeoTkt != null)
                    {
                        myUO_PEOINVTKTref.Ticket = myeMosTicketSegment.PeoTkt.Ticket;
                        myUO_PEOINVTKTref.TktSeq = myeMosTicketSegment.PeoTkt.TktSeq;
                    }
                    else
                    {
                        myUO_PEOINVTKTref.Ticket = "";
                        myUO_PEOINVTKTref.TktSeq = "";
                    }
                    myUO_PEOINVTKTref.CreateOn = uO_PEOINV.CREATEON;
                    myUO_PEOINVTKTref.CreateBy = uO_PEOINV.CREATEBY;
                    myUO_PEOINVTKTref.UpdateOn = uO_PEOINV.CREATEON;
                    myUO_PEOINVTKTref.UpdateBy = uO_PEOINV.CREATEBY;
                    invoice.PEOINVTKTref.Add(myUO_PEOINVTKTref);
                }
            }
        }

        public void SetTkt(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking)
        {
            if (booking == null) return;
            tbPEOINV uO_PEOINV = invoice.PeoInv;
            if (booking.TicketSegments != null)
            {
                foreach (eMosTicketSegment myeMosTicketSegment in booking.TicketSegments)
                {
                    tbPEOINVTKT myUO_PEOINVTKT = new tbPEOINVTKT();
                    myUO_PEOINVTKT.CompanyCode = uO_PEOINV.COMPANYCODE;
                    myUO_PEOINVTKT.InvNum = "";
                    myUO_PEOINVTKT.BkgRef = uO_PEOINV.BKGREF;
                    if (myeMosTicketSegment.PeoTkt != null)
                    {
                        myUO_PEOINVTKT.Ticket = myeMosTicketSegment.PeoTkt.Ticket;
                        myUO_PEOINVTKT.TktSeq = myeMosTicketSegment.PeoTkt.TktSeq;
                    }
                    else
                    {
                        myUO_PEOINVTKT.Ticket = "";
                        myUO_PEOINVTKT.TktSeq = "";
                    }
                    myUO_PEOINVTKT.CreateOn = uO_PEOINV.CREATEON;
                    myUO_PEOINVTKT.CreateBy = uO_PEOINV.CREATEBY;
                    myUO_PEOINVTKT.UpdateOn = uO_PEOINV.CREATEON;
                    myUO_PEOINVTKT.UpdateBy = uO_PEOINV.CREATEBY;
                    invoice.PEOINVTKT.Add(myUO_PEOINVTKT);
                }
            }
        }

        private void SetAirSegment4IUR(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking, List<AirlineSegment> airSegment4Invoice)
        {
            if (airSegment4Invoice == null) return;
            string myServType = "AIR";
            string myServNameForSegment;
            string myServDescForSegment;
            //string myItemName = "";
            tbPEOINV uO_PEOINV = invoice.PeoInv;
            string myTaxCurr = uO_PEOINV.SELLCURR;
            string mySellCurr = uO_PEOINV.SELLCURR;
            string localcurr = uO_PEOINV.SELLCURR;
            string myDate = "";
            int seqNum = 0;
            string agetTypes = "";
            tbPEOINVSEG myPeoInvSeg = null;
            tbPEOINVDETAIL myPeoInvDetail = null;
            tbPEOINVDETAILs myUOList_PEOINVDETAIL = invoice.PEOINVDETAIL;

            foreach (AirlineSegment myAirlineSegment in airSegment4Invoice)
            {
                if (myAirlineSegment.UO_PEOAIR == null) continue;
                myServType = "AIR";
                myDate = myAirlineSegment.UO_PEOAIR.DepartDATE != null ? myAirlineSegment.UO_PEOAIR.DepartDATE.ToString("ddMMMyyyy") : System.DateTime.MinValue.ToString("ddMMMyyyy");

                myServNameForSegment = Convert.ToString(myAirlineSegment.UO_PEOAIR.Flight + "").PadRight(9, ' ') +
                                        Convert.ToString(myAirlineSegment.UO_PEOAIR.Class + "").PadRight(6, ' ') +
                                        myDate.PadRight(15, ' ') +
                                        myAirlineSegment.UO_PEOAIR.DepartTime.PadRight(7, ' ') +
                                        Convert.ToString(myAirlineSegment.UO_PEOAIR.DepartCity + "").PadRight(6, ' ') +
                                        Convert.ToString(myAirlineSegment.UO_PEOAIR.ArrivalCity + "").PadRight(6, ' ');


                myServDescForSegment = myAirlineSegment.UO_PEOAIR.ArrTime.PadRight(7, ' ') +
                                        myDate.PadRight(15, ' ') +
                                        Convert.ToString(booking.PeoMstr.PNR + "").PadRight(6, ' ');

                myPeoInvSeg = new tbPEOINVSEG();
                myPeoInvSeg.COMPANYCODE = uO_PEOINV.COMPANYCODE;
                myPeoInvSeg.INVNUM = "";
                myPeoInvSeg.SEGNUM = Convert.ToString(++invSegnum).PadLeft(5, '0');
                myPeoInvSeg.SERVTYPE = myServType;
                myPeoInvSeg.SERVNAME = myServNameForSegment;
                myPeoInvSeg.SERVDESC = myServDescForSegment;
                myPeoInvSeg.SEGTYPE = myServType;
                myPeoInvSeg.CREATEON = uO_PEOINV.CREATEON;
                myPeoInvSeg.CREATEBY = uO_PEOINV.CREATEBY;
                invoice.PEOINVSEG.Add(myPeoInvSeg);

                seqNum = 0;

                #region detail list
                if (myAirlineSegment.UOList_PEOAIRDETAIL == null) continue;
                foreach (UO_PEOAIRDETAIL myUO_PEOAIRDETAIL in myAirlineSegment.UOList_PEOAIRDETAIL)
                {
                    if (Convert.ToString(myUO_PEOAIRDETAIL.SEQTYPE + "").Trim().ToUpper() == "SELL")
                    {
                        if (agetTypes.IndexOf(myUO_PEOAIRDETAIL.AGETYPE + "") == -1)
                        {
                            agetTypes += "," + myUO_PEOAIRDETAIL.AGETYPE;
                            myPeoInvDetail = new tbPEOINVDETAIL();
                            myPeoInvDetail.COMPANYCODE = uO_PEOINV.COMPANYCODE;
                            myPeoInvDetail.INVNUM = "";
                            myPeoInvDetail.SEGNUM = myPeoInvSeg.SEGNUM + "";
                            myPeoInvDetail.SEQNUM = Convert.ToInt32(++seqNum).ToString().PadLeft(5, '0');
                            myPeoInvDetail.DETLTYPE = myUO_PEOAIRDETAIL.AGETYPE + "";
                            myPeoInvDetail.ITEMNAME = myUO_PEOAIRDETAIL.AGETYPE + "";
                            myPeoInvDetail.QTY = myUO_PEOAIRDETAIL.QTY;
                            myPeoInvDetail.SELLCURR = mySellCurr;
                            myPeoInvDetail.SELLAMT = myUO_PEOAIRDETAIL.AMT;
                            myPeoInvDetail.TAXCURR = myTaxCurr;
                            myPeoInvDetail.TAXAMT = myUO_PEOAIRDETAIL.TAXAMT;
                            //myPeoInvDetail.HKDAMT = myHKDAmt;
                            myPeoInvDetail.CREATEON = uO_PEOINV.CREATEON;
                            myPeoInvDetail.CREATEBY = uO_PEOINV.CREATEBY;
                            myPeoInvDetail.SHOWDETAILFEE = "N";
                            //myPeoInvDetail.AMTFEE = myUO_PEOAIRDETAIL.AMTFEE;
                            if (mySellCurr.Trim() != localcurr.Trim())
                            {
                                myPeoInvDetail.HKDAMT = BOCurr.CalculateAmount(uO_PEOINV.COMPANYCODE, localcurr, mySellCurr, myPeoInvDetail.SELLAMT + myPeoInvDetail.TAXAMT);
                                //myPeoInvDetail.HKDAMT = BO_EXRATE.Exrate(uO_PEOINV.COMPANYCODE, mySellCurr, myPeoInvDetail.SELLAMT + myPeoInvDetail.TAXAMT, localcurr);
                            }
                            else
                            {
                                myPeoInvDetail.HKDAMT = myPeoInvDetail.SELLAMT + myPeoInvDetail.TAXAMT;
                            }
                            myUOList_PEOINVDETAIL.Add(myPeoInvDetail);
                        }
                        else
                        {
                            if (myUOList_PEOINVDETAIL != null)
                            {
                                foreach (tbPEOINVDETAIL detail in myUOList_PEOINVDETAIL)
                                {
                                    if (detail.DETLTYPE == myUO_PEOAIRDETAIL.AGETYPE)
                                    {
                                        myPeoInvDetail.SELLAMT += myUO_PEOAIRDETAIL.AMT;
                                        myPeoInvDetail.TAXAMT += myUO_PEOAIRDETAIL.TAXAMT;
                                        myPeoInvDetail.HKDAMT = myPeoInvDetail.SELLAMT + myPeoInvDetail.TAXAMT;
                                    }
                                }
                            }
                        }

                    }
                }
                #endregion
            }
            if (myUOList_PEOINVDETAIL != null)
            {
                foreach (tbPEOINVDETAIL detail in myUOList_PEOINVDETAIL)
                {
                    uO_PEOINV.TAXAMT += detail.TAXAMT * detail.QTY;
                    uO_PEOINV.SELLAMT += (detail.SELLAMT + detail.TAXAMT) * detail.QTY;
                    uO_PEOINV.SellExcludeTax = uO_PEOINV.SELLAMT - uO_PEOINV.TAXAMT;
                    uO_PEOINV.HKDAMT += detail.HKDAMT * detail.QTY;
                    uO_PEOINV.invdtsum = uO_PEOINV.SELLAMT;
                }
            }
        }

        private void SetPeoPax(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking)
        {
            if (booking == null || invoice.PeoInv == null || invoice.PEOINPAX == null) return;
            int count = 0;
            tbPEOINV uO_PEOINV = invoice.PeoInv;
            tbPEOINPAXs paxList = invoice.PEOINPAX;
            tbPEOPAXs eMosPaxList = da.GetAllPaxByBkgref(booking.CompanyCode, booking.CompanyCode);

            //if existed ticket, then using ticket's pax, or use all pax
            if (booking.TicketSegments != null && booking.TicketSegments.Count > 0)
            {
                foreach (eMosTicketSegment myeMosTicketSegment in booking.TicketSegments)
                {
                    if (myeMosTicketSegment.PeoTkt != null)
                    {
                        tbPEOINPAX myUO_PEOINPAX = new tbPEOINPAX();
                        myUO_PEOINPAX.PAXNAME = Convert.ToString(myeMosTicketSegment.PeoTkt.PaxName + "").Trim() + "/" + myeMosTicketSegment.PeoTkt.PAXAIR + "/" + myeMosTicketSegment.PeoTkt.PAXAIR + "/" + myeMosTicketSegment.PeoTkt.PAXAIR;
                        myUO_PEOINPAX.COMPANYCODE = uO_PEOINV.COMPANYCODE;
                        myUO_PEOINPAX.INVNUM = "";
                        myUO_PEOINPAX.PAXSEQ = Convert.ToInt16(++count);
                        myUO_PEOINPAX.SEQNUM = Convert.ToString(count).PadLeft(5, '0');
                        myUO_PEOINPAX.INVTYPE = "I";
                        paxList.Add(myUO_PEOINPAX);
                    }
                }
            }
            else
            {
                foreach (tbPEOPAX mypax in booking.PeoPaxList)
                {
                    tbPEOINPAX myUO_PEOINPAX = new tbPEOINPAX();
                    myUO_PEOINPAX.PAXNAME = Convert.ToString(mypax.PAXLNAME + "/" + mypax.PAXFNAME + "/" + mypax.PAXTITLE + "/" + mypax.PAXAIR + "/" + mypax.PAXAIR + "/" + mypax.PAXAIR);
                    myUO_PEOINPAX.COMPANYCODE = uO_PEOINV.COMPANYCODE;
                    myUO_PEOINPAX.INVNUM = "";
                    myUO_PEOINPAX.PAXSEQ = Convert.ToInt16(++count);
                    myUO_PEOINPAX.SEQNUM = Convert.ToString(count).PadLeft(5, '0');
                    myUO_PEOINPAX.INVTYPE = "I";
                    paxList.Add(myUO_PEOINPAX);
                }
            }
        }


        public void SetPeoinv(Westminster.MOE.Entity.BLLEntity.Transfer.TsfrInvoice invoice, eMosBooking booking, string invrmk, string staffcode, string teamcode)
        {
            if (booking == null || booking.PeoMstr == null) return;
            tbPEOINV eMosInvoice = invoice.PeoInv;
            string companycode = booking.PeoMstr.COMPANYCODE + "";
            string bkgref = booking.PeoMstr.BKGREF + "";
            if (string.IsNullOrEmpty(staffcode) == true)
            {
                staffcode = booking.PeoMstr.STAFFCODE + "";
            }
            if (string.IsNullOrEmpty(teamcode) == true)
            {
                teamcode = booking.PeoMstr.TEAMCODE + "";
            }

            tbPEOMSTR uoPeomstr = da.GetPEOMSTR(bkgref, companycode);
            if (uoPeomstr == null) return;

            tbPEOMISs mis = da.GetUnvoidPeoMisList(companycode, bkgref);
            if (mis.Count != 0)
            {
                eMosInvoice.COD1 = mis[0].COD1 + "";
                eMosInvoice.COD2 = mis[0].COD2 + "";
                eMosInvoice.COD3 = mis[0].COD3 + "";
            }

            if (invrmk != "")
            {
                eMosInvoice.CUSTREF = invrmk;
                string[] cods = invrmk.Split('/');
                if (cods.Length > 1)
                {
                    eMosInvoice.COD1 = cods[0];
                    eMosInvoice.COD2 = cods[1];
                }
            }

            eMosInvoice.INVNUM = "";
            eMosInvoice.COMPANYCODE = companycode;
            eMosInvoice.BKGREF = bkgref;

            eMosInvoice.INVTYPE = GetDBInvType(InvType);
            eMosInvoice.mstrinvnum = "";
            eMosInvoice.CLTCODE = booking.PeoMstr.CLTODE + "";

            tbCustomer customer = da.GetCustomer(companycode, booking.PeoMstr.CLTODE);
            //DO_CUSTOMER.UO_CUSTOMER myUO_CUSTOMER = BO_CUSTOMER.GetObject("", companycode, booking.PEOMSTR.CLTODE + "");
            if (customer != null)
            {
                eMosInvoice.CLTNAME = customer.CLTNAME + "";
                eMosInvoice.CLTNAME = eMosInvoice.CLTNAME.Length > 40 ? eMosInvoice.CLTNAME.Substring(0, 40) : eMosInvoice.CLTNAME;
                eMosInvoice.CLTADDR = customer.ADDR1 + "<br>" + customer.ADDR2 + "<br>" + customer.ADDR3 + "<br>" + customer.ADDR4;
                eMosInvoice.CLTADDR = eMosInvoice.CLTADDR.Length > 160 ? eMosInvoice.CLTADDR.Substring(0, 160) : eMosInvoice.CLTADDR;

                eMosInvoice.CONTACTPERSON = customer.CONTACT1;
            }
            eMosInvoice.STAFFCODE = staffcode;
            eMosInvoice.TEAMCODE = teamcode;
            eMosInvoice.CONTACTPERSON = string.Empty;

            eMosInvoice.ANALYSISCODE = config.TsfrSettings.AnalysisCode;
            eMosInvoice.CUSTREF = eMosInvoice.COD1 + "/" + eMosInvoice.COD2 + "/" + eMosInvoice.COD3;
            eMosInvoice.CUSTREF = eMosInvoice.CUSTREF.Length > 47 ? eMosInvoice.CUSTREF.Substring(0, 47) : eMosInvoice.CUSTREF;
            eMosInvoice.SELLCURR = booking.PeoMstr.INVCURR + "";
            eMosInvoice.SellExcludeTax = 0;
            eMosInvoice.TAXCURR = booking.PeoMstr.INVCURR + "";
            eMosInvoice.TAXAMT = 0;
            eMosInvoice.HKDAMT = 0;
            eMosInvoice.SELLAMT = 0;
            eMosInvoice.RECINV = GetDBRecinv(InvType);  //"INV";
            eMosInvoice.invdtcurr = booking.PeoMstr.INVCURR + "";
            eMosInvoice.invdtsum = 0;
            eMosInvoice.CREATEON = DateTime.Now;
            eMosInvoice.CREATEBY = staffcode;
            eMosInvoice.UPDATEON = eMosInvoice.CREATEON;
            eMosInvoice.UPDATEBY = staffcode;
            if (InvType == Invoice_Types.UATP || InvType == Invoice_Types.CreditCardSales)
            {
                eMosInvoice.PAYMENT = GetDBPayment(InvType);
                if (eMosParam.Tickets != null && eMosParam.Tickets.Count > 0)
                {
                    eMosInvoice.CR_card_type = eMosParam.Tickets[0].UATP_Card_Type;
                    eMosInvoice.CR_card_no = eMosParam.Tickets[0].UATP_Card;
                    //DateTime CR_card_expiry;
                    //uO_PEOINV.CR_card_expiry = DateTime.TryParse(eMosParam.Tickets[0].UATP_CardExpireDate, out CR_card_expiry) ? CR_card_expiry : DateTime.Parse("1901-1-1");

                    DateTime CR_card_expiry;
                    DateTime CR_card_expiry_output;

                    string testDate = eMosParam.Tickets[0].UATP_CardExpireDate;
                    CR_card_expiry_output = DateTime.TryParse(testDate, out CR_card_expiry) ? CR_card_expiry : DateTime.Parse("1901-1-1");
                    if (CR_card_expiry_output == DateTime.Parse("1901-1-1") && testDate.Length >= 4)
                    {
                        testDate = "20" + testDate.Substring(2, 2) + "-" + testDate.Substring(0, 2) + "-01";
                        CR_card_expiry_output = DateTime.TryParse(testDate, out CR_card_expiry) ? CR_card_expiry : DateTime.Parse("1901-1-1");

                    }
                    eMosInvoice.CR_card_expiry = CR_card_expiry_output;
                }
            }
            if (this.TSAno != "") eMosInvoice.COD1 = this.TSAno;
        }

        public string GetDBInvType(Invoice_Types invType)
        {
            switch (invType)
            {
                case Invoice_Types.Normal: return "INV";
                case Invoice_Types.UATP: return "UAT";
                case Invoice_Types.CreditNotes: return "CRN";
                case Invoice_Types.DepositCreditNote: return "DEP";
                case Invoice_Types.Deposit: return "DEP";
                //case Invoice_Types.NRCC_INVOICE: return "INV";
                case Invoice_Types.CreditCardSales: return "CCR";
            }
            return "";
        }
        public string GetDBRecinv(Invoice_Types invType)
        {
            switch (invType)
            {
                case Invoice_Types.Normal: return "INV";
                case Invoice_Types.UATP: return "INV";
                case Invoice_Types.Combined: return "INV";
                //case Invoice_Types.CreditNotes: return "CRN";
                //case Invoice_Types.DepositCreditNote: return "DEP";
                //case Invoice_Types.Deposit: return "DEP";
                //case Invoice_Types.NRCC_INVOICE: return "INV";
                case Invoice_Types.CreditCardSales: return "CCR";
            }
            return "";
        }
        public string GetDBPayment(Invoice_Types invType)
        {
            switch (invType)
            {
                case Invoice_Types.Normal: return "CSH";
                case Invoice_Types.UATP: return "UAT";
                case Invoice_Types.Combined: return "UAT";

                //case Invoice_Types.CreditNotes: return "CRN";
                //case Invoice_Types.DepositCreditNote: return "DEP";
                //case Invoice_Types.Deposit: return "DEP";
                //case Invoice_Types.NRCC_INVOICE: return "INV";
                case Invoice_Types.CreditCardSales: return "CCR";
            }
            return "CSH";
        }
    }

}
