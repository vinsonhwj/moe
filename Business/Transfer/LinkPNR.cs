﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Transfer;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;



namespace Westminster.MOE.BLL.Transfer
{
    public class LinkPNR : IDisposable
    {
        public Config Config { get; private set; }
        private IDAL.Transfer.ILinkPNR da { get; set; }
        public IConnection Conn;
        private Booking BOBooking;

        public LinkPNR(Config config)
            : this(config, new DAL.Transfer.DALinkPNR(new DbConnection(config.DatabaseList[MOEEnums.DALConnectionType.eMos]), config.WebServiceList[MOEEnums.BindingName.IURSoap]))
        {

        }
        public LinkPNR(Config config, ILinkPNR dataAccess)
        {
            Config = config;
            da = dataAccess;
            Conn = da.InnerConnection;

            BOBooking = new Booking(config, da, Conn);
        }

        public bool UpdateBooking4Gateway(string companyCode, string IURBkgRef, out WSResult wsResult)
        {
            wsResult = new WSResult();
              if (da.ExistBooking(companyCode, IURBkgRef))
            {
                MOELog.Info("[Status]:Get IUR object");
                Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur = da.GetIURObjects(companyCode, IURBkgRef);
                if (iur == null)
                {
                    return false;
                }
                //MOELog.Info("[Status]:Update Booking");
                return BOBooking.Update(iur, null, out wsResult);
            }
            return false;
        }
        public WSResult LinkPNR4emos(string companyCode, string eMosBkgRef, string IURBkgRef, string staffCode, string teamCode, out WSResult wsResult)
        {
            wsResult = new WSResult();
            // string errmsg = string.Empty;
            eMosBkgRef = eMosBkgRef.ToUpper();
            IURBkgRef = IURBkgRef.ToUpper();

            try
            {
                if (eMosBkgRef == IURBkgRef)
                {
                    Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur = da.GetIURObjects(companyCode, eMosBkgRef);
                    if (iur == null || iur.PEOMSTR == null)
                    {
                        wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                        wsResult.Msg = BLL.Properties.Resource_en_US.Invalid_Booking;
                        return wsResult;
                    }
                    if (da.ExistBooking(companyCode, IURBkgRef))
                    {
                        BOBooking.Update(iur, null, out wsResult);
                    }
                    else
                    {
                        BOBooking.Create(iur, staffCode, teamCode, out wsResult);
                    }

                    //da.UpdateFlag(companyCode, eMosBkgRef, true);
                }
                else
                {
                    Westminster.MOE.Entity.DALEntity.Gateway.IURObject iur;
                    if (!da.ExistBooking(companyCode, eMosBkgRef))
                    {
                        iur = da.GetIURObjects(companyCode, eMosBkgRef);
                        if (iur == null || iur.PEOMSTR == null)
                        {
                            wsResult.Msg = BLL.Properties.Resource_en_US.InvalidTargetBooking;
                            return wsResult;
                        }
                        BOBooking.Create(iur, staffCode, teamCode, out wsResult);
                    }
                    iur = da.GetIURObjects(companyCode, IURBkgRef);
                    eMosObjects emosBooking = BOBooking.GeteMosBooking(iur, companyCode, eMosBkgRef, IURBkgRef, out wsResult);
                    if (emosBooking != null)
                    {
                        try
                        {
                            Conn.BeginTransaction();
                            da.InserteMosObjects(emosBooking);
                            Conn.CommitTransaction();
                            wsResult.Status = MOEEnums.ResultStatus.Success;
                        }
                        catch (Exception ex)
                        {
                            wsResult.Msg = ex.Message;
                            wsResult.Status = MOEEnums.ResultStatus.Error;
                            Conn.RollbackTransaction();
                        }
                    }
                    else
                    {
                        wsResult.Msg = BLL.Properties.Resource_en_US.InvalidSourceBooking;
                        wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                    }


                }
                return wsResult;
            }
            catch
            {
                if (Conn.TransactionState == hwj.DBUtility.Enums.TransactionState.Begin)
                {
                    Conn.RollbackTransaction();
                }
                throw;
            }
        }
        public bool CreateBookingByIUR(string xml, out WSResult wsResult)
        {
            wsResult = new WSResult();
            string bkgref = string.Empty;

            Westminster.MOE.DAL.GatewayWebSvc.IURObject iur = hwj.CommonLibrary.Object.SerializationHelper.FromXml<Westminster.MOE.DAL.GatewayWebSvc.IURObject>(xml, true);

            if (iur == null)
            {
                wsResult.Msg = Properties.Resource_en_US.Invalid_IUR;
                wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                return false;
            }
            else if (iur != null && iur.Master == null)
            {
                wsResult.Msg = Properties.Resource_en_US.Invalid_Booking;
                wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                return false;
            }

            if (da.ExistBooking(iur.Master.COMPANYCODE, iur.Master.BKGREF))
            {
                wsResult.Msg = string.Format("PeoMstr({0},{1}) already exist", iur.Master.COMPANYCODE, iur.Master.BKGREF);
                wsResult.Status = MOEEnums.ResultStatus.InvalidData;
                return false;
            }
            try
            {
                Westminster.MOE.Entity.DALEntity.Gateway.IURObject moeIur = Westminster.MOE.DAL.GatewayWebSvc.ConvertToGateway.ToMOEObject(iur);
                return BOBooking.Create(moeIur, Config.Settings.AutoBookingOwner, Config.Settings.AutoBookingOwnerTeamCode, out wsResult);
            }
            catch (Exception ex)
            {
                wsResult.Msg = ex.Message;
                wsResult.Status = MOEEnums.ResultStatus.Error;
                return false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Conn != null)
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        #endregion
    }
}
