﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.BLL.Maintain
{
    public class Currency
    {
        public Config Config { get; private set; }
        private IDAL.Maintain.IDACurrency da { get; set; }

        public Currency(Config config)
            : this(config, new DAL.Maintain.DACurrency(config.DatabaseList[MOEEnums.DALConnectionType.eMos]))
        {
        }
        public Currency(Config config, IDAL.Maintain.IDACurrency dataAccess)
        {
            Config = config;
            da = dataAccess;
        }
        public decimal CalculateAmount(string companyCode, string targetCurr, string sourceCurr, decimal amount)
        {
            return CalculateAmount(companyCode, Config.Settings.ExchageRateOperator, targetCurr, sourceCurr, amount);
        }
        private decimal CalculateAmount(string companyCode, string exchageRateOperator, string targetCurr, string sourceCurr, decimal amount)
        {
            if (targetCurr != null || sourceCurr != null)
            {
                if (targetCurr.Trim() == sourceCurr.Trim())
                {
                    return amount;
                }
                else
                {
                    tbEXRATE exRate = da.GetExRateEntity(companyCode, targetCurr, sourceCurr);

                    if (exRate != null)
                    {
                        if (exchageRateOperator == "*")
                        {
                            return amount * exRate.EXRATE;
                        }
                        else
                        {
                            return amount / exRate.EXRATE;
                        }
                    }
                }
            }

            throw new Exception(Properties.Resource_en_US.Invalid_Currency);
        }
    }
}
