﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:TEAM]
    /// </summary>
    public partial class DATEAM : DALDependency<tbTEAM, tbTEAMs>
    {
        public DATEAM(string connectionString)
            : base(connectionString)
        {
            TableName = tbTEAM.DBTableName;
        }
        public DATEAM(IConnection conn)
            : base(conn)
        {
           // TableName = tbPEOINV.DBTableName;
        }
        public bool Exists(string companycode, string branchcode, string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbTEAM entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbTEAM updateEntity, string companycode, string branchcode, string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string branchcode, string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbTEAM GetEntity(string companycode, string branchcode, string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbTEAM GetEntity(string companycode,  string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbTEAMs GetList(string companycode, string branchcode, string teamcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbTEAMs GetList(string companycode, string branchcode, string teamcode, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbTEAM.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.BRANCHCODE, branchcode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbTEAM.Fields.TEAMCODE, teamcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbTEAMPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbTEAMPage page = new tbTEAMPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbTEAM.Fields.COMPANYCODE);
            pk.Add(tbTEAM.Fields.BRANCHCODE);
            pk.Add(tbTEAM.Fields.TEAMCODE);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

