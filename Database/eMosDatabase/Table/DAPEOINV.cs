﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINV]
    /// </summary>
    public partial class DAPEOINV : DALDependency<tbPEOINV, tbPEOINVs>
    {
        public DAPEOINV(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINV.DBTableName;
        }
        public DAPEOINV(IConnection conn)
            : base(conn)
        {
            TableName = tbPEOINV.DBTableName;
        }
        public bool Exists(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }
        public bool ExistsInvoice(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.VOID2JBA, null, Enums.Relation.IsNull, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }
        public bool Add(tbPEOINV entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINV updateEntity, string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }
        public int RecordCount(string companyCode, string bkgRef)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            return this.RecordCount(fp);
        }
        public tbPEOINV GetEntity(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEOINV GetEntityForPeoInvCombinPlus(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.mstrinvnum, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Unequal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEOINVs GetEntityForPeoInvCombineInv(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.mstrinvnum, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Unequal, Enums.Expression.AND);
         
            return base.GetList(null, fp);
        }
        public tbPEOINVs GetEntityForPeoInvCombineInvList(string companycode, List<string> InvNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, InvNum, Enums.Relation.IN, Enums.Expression.AND);
        

            return base.GetList(null, fp);
        }


        public tbPEOINVs GetList(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINVs GetList(string companycode, string invnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }
        public tbPEOINV GetListForUFS(string companycode, string invnum,string mstrinvnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINV.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.INVNUM, invnum, Enums.Relation.Unequal, Enums.Expression.AND);
            fp.AddParam(tbPEOINV.Fields.mstrinvnum, mstrinvnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public static SqlList Insert(tbPEOINV entity)
        {
            SqlList sqlList = new SqlList();
            sqlList.Add(AddSqlEntity(entity));
            return sqlList;
        }



    }
}

