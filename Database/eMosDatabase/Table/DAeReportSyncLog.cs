﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:eReportSyncLog]
    /// </summary>
    public partial class DAeReportSyncLog : DALDependency<tbeReportSyncLog, tbeReportSyncLogs>
    {
        public DAeReportSyncLog(string connectionString)
            : base(connectionString)
        {
            TableName = tbeReportSyncLog.DBTableName;
        }

        public bool Exists(int id)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbeReportSyncLog entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbeReportSyncLog updateEntity, int id)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(int id)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbeReportSyncLog GetEntity(int id)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbeReportSyncLogs GetList(int id)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbeReportSyncLogs GetList(int id, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.id, id, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbeReportSyncLogs GetList(string companyCode, string docNum, string syncTable, int syncStatus)
        {
            DisplayFields dp = new DisplayFields();
            dp.Add(tbeReportSyncLog.Fields.Docnum);
            FilterParams fp = new FilterParams();
            fp.AddParam(tbeReportSyncLog.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbeReportSyncLog.Fields.Docnum, docNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbeReportSyncLog.Fields.SyncTable, syncTable, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbeReportSyncLog.Fields.SyncStatus, syncStatus, Enums.Relation.Equal, Enums.Expression.None);
            return base.GetList(dp, fp, null);
        }
        public tbeReportSyncLogPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbeReportSyncLogPage page = new tbeReportSyncLogPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbeReportSyncLog.Fields.id);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

