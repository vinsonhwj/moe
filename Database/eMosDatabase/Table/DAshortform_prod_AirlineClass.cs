﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:shortform_prod_AirlineClass]
    /// </summary>
    public partial class DAshortform_prod_AirlineClass : DALDependency<tbshortform_prod_AirlineClass, tbshortform_prod_AirlineClasss>
    {
        public DAshortform_prod_AirlineClass(IConnection conn)
            : base(conn)
        { }


        public bool Add(tbshortform_prod_AirlineClass entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbshortform_prod_AirlineClass updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbshortform_prod_AirlineClass GetEntity(string aircode, string classcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbshortform_prod_AirlineClass.Fields.Airline, aircode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbshortform_prod_AirlineClass.Fields.Class, classcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbshortform_prod_AirlineClasss GetList()
        {
            return base.GetList();
        }

        public tbshortform_prod_AirlineClassPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbshortform_prod_AirlineClassPage page = new tbshortform_prod_AirlineClassPage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

