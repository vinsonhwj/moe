﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:country]
    /// </summary>
    public partial class DAcountry : DALDependency<tbcountry, tbcountrys>
    {
        public DAcountry(string connectionString)
            : base(connectionString)
        {
            TableName = tbcountry.DBTableName;
        }
        public DAcountry(IConnection conn)
            : base(conn)
        { }

        //public bool Exists(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcountry entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcountry updateEntity, string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcountry GetEntity(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcountrys GetList(List<string> countryCodeList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry.Fields.COUNTRYCODE, countryCodeList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

