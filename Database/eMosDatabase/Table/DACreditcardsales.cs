﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:Creditcardsales]
    /// </summary>
    public partial class DACreditcardsales : DALDependency<tbCreditcardsales, tbCreditcardsaless>
    {
        public DACreditcardsales(string connectionString)
            : base(connectionString)
        {
            TableName = tbCreditcardsales.DBTableName;
        }


        public bool Add(tbCreditcardsales entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbCreditcardsales updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        //public tbCreditcardsales GetEntity()
        //{
        //    return base.GetEntity();
        //}

        public tbCreditcardsaless GetList()
        {
            return base.GetList();
        }

        public tbCreditcardsaless GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

