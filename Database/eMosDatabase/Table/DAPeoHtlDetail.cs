﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PeoHtlDetail]
    /// </summary>
    public partial class DAPeoHtlDetail : DALDependency<tbPeoHtlDetail, tbPeoHtlDetails>
    {
        public DAPeoHtlDetail(string connectionString)
            : base(connectionString)
        {
            TableName = tbPeoHtlDetail.DBTableName;
        }
        public DAPeoHtlDetail(IConnection conn)
            : base(conn)
        {

        }

        //public bool Exists(string bkgref, string companycode, string seqtype, string segnum, string seqnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}
        public bool Add(tbPeoHtlDetail entity)
        {
            return base.Add(entity);
        }
        //public bool Update(tbPeoHtlDetail updateEntity, string bkgref, string companycode, string seqtype, string segnum, string seqnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}
        //public bool Delete(string bkgref, string companycode, string seqtype, string segnum, string seqnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}
        //public tbPeoHtlDetail GetEntity(string bkgref, string companycode, string seqtype, string segnum, string seqnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.GetEntity(fp);
        //}

        public tbPeoHtlDetails GetList(string companyCode, string bkgRef)
        {
            return GetList(companyCode, bkgRef, null);
        }
        public tbPeoHtlDetails GetList(string companyCode, string bkgRef, List<string> segNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);

            if (segNumList != null && segNumList.Count > 0)
            {
                fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segNumList, Enums.Relation.IN, Enums.Expression.AND);
            }

            SortParams sp = new SortParams();
            sp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, Enums.OrderBy.Ascending);
            sp.AddParam(tbPeoHtlDetail.Fields.BKGREF, Enums.OrderBy.Ascending);
            sp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, Enums.OrderBy.Ascending);
            sp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, Enums.OrderBy.Ascending);
            sp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, Enums.OrderBy.Ascending);

            return base.GetList(null, fp, sp);
        }
        public tbPeoHtlDetails GetList(string bkgref, string companycode, string seqtype, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoHtlDetail.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoHtlDetail.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoHtlDetail.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoHtlDetail.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoHtlDetail.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

