﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOAIRDETAIL]
    /// </summary>
    public partial class DAPEOAIRDETAIL : DALDependency<tbPEOAIRDETAIL, tbPEOAIRDETAILs>
    {
        public DAPEOAIRDETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOAIRDETAIL.DBTableName;
        }
        public DAPEOAIRDETAIL(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string companyCode, string bkgRef, string segNum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOAIRDETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOAIRDETAIL updateEntity, string companyCode, string bkgRef, string segNum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string bkgRef, string segNum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOAIRDETAIL GetEntity(string companyCode, string bkgRef, string segNum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOAIRDETAILs GetList(string companyCode, string bkgRef)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            SortParams sp = new SortParams();
            sp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, Enums.OrderBy.Ascending);
            return base.GetList(null, fp, sp);
        }

        public tbPEOAIRDETAILs GetListForSell(string companyCode, string bkgRef, List<string> segNums)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQTYPE, "SELL", Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNums, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOAIRDETAILs GetEntity(string companyCode, string bkgRef, List<string> segNums, string seqtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNums, Enums.Relation.IN, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQTYPE, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);

        }

        public tbPEOAIRDETAILs GetList(string companyCode, string bkgRef, string segNum, string seqnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbPEOAIRDETAILs GetListByBKGREF(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOAIRDETAIL.Fields.SEQNUM, Enums.OrderBy.Ascending);

            return base.GetList(null, fp, sp);
        }

        public static SqlEntity DeleteByBkgref(string companyCode, string bkgref, List<string> segnumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            return DeleteSqlEntity(fp);
        }

        public int GetMaxSeg(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, Enums.OrderBy.Descending);

            tbPEOAIRDETAIL tb = base.GetEntity(null, fp, sp);
            return tb == null ? 0 : Convert.ToInt32(tb.SegNum);
        }

        public tbPEOAIRDETAILs GetPaxIgnoreOriginal(string companyCode, string bkgRef, List<string> segnumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, segnumList, Enums.Relation.IN_InsertSQL, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOAIRDETAILs GetFirstList(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SegNum, "00001", Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL.Fields.SEQTYPE, "SELL", Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetList(null, fp);

        }
    }
}

