﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOTKTDETAIL]
    /// </summary>
    public partial class DAPEOTKTDETAIL : DALDependency<tbPEOTKTDETAIL, tbPEOTKTDETAILs>
    {
        public DAPEOTKTDETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOTKTDETAIL.DBTableName;
        }
        public DAPEOTKTDETAIL(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string companyCode, string ticket, string tktSeq, string segNum, string airLine)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOTKTDETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOTKTDETAIL updateEntity, string companyCode, string ticket, string tktSeq, string segNum, string airLine)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq, string segNum, string airLine)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOTKTDETAIL GetEntity(string companyCode, string ticket, string tktSeq, string segNum, string airLine)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOTKTDETAILs GetList(string companyCode, string ticket, string tktSeq, string segNum, string airLine)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOTKTDETAILs GetList(string companyCode, string ticket, string tktSeq, string segNum, string airLine, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.AirLine, airLine, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }


        public tbPEOTKTDETAILs GetList(string companyCode, List<string> tktLst)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKTDETAIL.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKTDETAIL.Fields.Ticket, tktLst, Enums.Relation.IN, Enums.Expression.AND);

            return base.GetList(null, fp);
        }
    }
}

