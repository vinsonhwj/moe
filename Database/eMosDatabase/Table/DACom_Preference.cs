﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:Com_Preference]
    /// </summary>
    public partial class DACom_Preference : DALDependency<tbCom_Preference, tbCom_Preferences>
    {
        public DACom_Preference(string connectionString)
            : base(connectionString)
        {
            TableName = tbCom_Preference.DBTableName;
        }
        public DACom_Preference(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string preferenceid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbCom_Preference entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbCom_Preference updateEntity, string companycode, string preferenceid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string preferenceid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbCom_Preference GetEntity(string companycode, string preferenceid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbCom_Preferences GetList(string companycode, string preferenceid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbCom_Preferences GetList(string companycode, string preferenceid, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, preferenceid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public bool IsY(string COMPANYCODE, string PREFERENCEID)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCom_Preference.Fields.COMPANYCODE, COMPANYCODE, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCom_Preference.Fields.PREFERENCEID, PREFERENCEID, Enums.Relation.Equal, Enums.Expression.AND);
            tbCom_Preference com = base.GetEntity(fp);
            if (com != null)
            {
                return com.ENABLE.Trim() == "Y";
            }
            else
                return false;
        }
    }
}

