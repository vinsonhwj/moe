﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOVCHSEG]
    /// </summary>
    public partial class DAPEOVCHSEG : DALDependency<tbPEOVCHSEG, tbPEOVCHSEGs>
    {
        public DAPEOVCHSEG(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOVCHSEG.DBTableName;
        }
        public DAPEOVCHSEG(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string vchnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOVCHSEG entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOVCHSEG updateEntity, string companycode, string vchnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string vchnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOVCHSEG GetEntity(string companycode, string vchnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOVCHSEGs GetList(string companycode, string vchnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOVCHSEGs GetList(string companycode, string vchnum, string segnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

