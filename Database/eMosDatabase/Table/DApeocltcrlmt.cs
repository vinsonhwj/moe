﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:peocltcrlmt]
    /// </summary>
    public partial class DApeocltcrlmt : DALDependency<tbpeocltcrlmt, tbpeocltcrlmts>
    {
        public DApeocltcrlmt(IConnection conn)
            : base(conn)
        {
            TableName = tbpeocltcrlmt.DBTableName;
        }


        public bool Add(tbpeocltcrlmt entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbpeocltcrlmt updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbpeocltcrlmts GetList()
        {
            return base.GetList();
        }

        public tbpeocltcrlmts GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

        public tbpeocltcrlmtPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbpeocltcrlmtPage page = new tbpeocltcrlmtPage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }


        public tbpeocltcrlmt GetEntity(string companyCode, string cltcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeocltcrlmt.Fields.companycode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeocltcrlmt.Fields.cltcode, cltcode, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetEntity(fp);
        }
    }
}

