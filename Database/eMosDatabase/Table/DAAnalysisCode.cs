﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:AnalysisCode]
    /// </summary>
    public partial class DAAnalysisCode : DALDependency<tbAnalysisCode, tbAnalysisCodes>
    {
        public DAAnalysisCode(string connectionString)
            : base(connectionString)
        {
            TableName = tbAnalysisCode.DBTableName;
        }
        public DAAnalysisCode(IConnection conn)
            : base(conn)
        {
            // TableName = tbPEOINV.DBTableName;
        }
        public bool Exists(string companycode, string analysiscode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbAnalysisCode entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbAnalysisCode updateEntity, string companycode, string analysiscode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string analysiscode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }


        public int RecordCount(string companyCode, string analysisCode, DateTime startDate)
        {
            // sql = "SELECT count(*) as amt FROM AnalysisCode WHERE companycode=@cc AND Analysiscode = @Analysiscode AND STARTDATE <= @txnDate AND ENDDATE >= @txnDate AND suspended = 0 "
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysisCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.SUSPENDED, 0, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.STARTDATE, startDate, Enums.Relation.LessThanOrEqual, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ENDDATE, startDate, Enums.Relation.GreaterThanOrEqual, Enums.Expression.AND);

            return this.RecordCount(fp);
        }

        public tbAnalysisCode GetEntity(string companycode, string analysiscode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbAnalysisCodes GetList(string companycode, string analysiscode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbAnalysisCodes GetList(string companycode, string analysiscode, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbAnalysisCode.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbAnalysisCode.Fields.ANALYSISCODE, analysiscode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbAnalysisCodePage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbAnalysisCodePage page = new tbAnalysisCodePage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbAnalysisCode.Fields.COMPANYCODE);
            pk.Add(tbAnalysisCode.Fields.ANALYSISCODE);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

