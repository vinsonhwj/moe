﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:country_EN_US]
    /// </summary>
    public partial class DAcountry_EN_US : DALDependency<tbcountry_EN_US, tbcountry_EN_USs>
    {
        public DAcountry_EN_US(string connectionString)
            : base(connectionString)
        {
            TableName = tbcountry_EN_US.DBTableName;
        }
        public DAcountry_EN_US(IConnection conn)
            : base(conn)
        { }

        //public bool Exists(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcountry_EN_US entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcountry_EN_US updateEntity, string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcountry_EN_US GetEntity(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcountry_EN_USs GetList(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

