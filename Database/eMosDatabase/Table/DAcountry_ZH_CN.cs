﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:country_ZH_CN]
    /// </summary>
    public partial class DAcountry_ZH_CN : DALDependency<tbcountry_ZH_CN, tbcountry_ZH_CNs>
    {
        public DAcountry_ZH_CN(string connectionString)
            : base(connectionString)
        {
            TableName = tbcountry_ZH_CN.DBTableName;
        }
        public DAcountry_ZH_CN(IConnection conn)
            : base(conn)
        { }

        //public bool Exists(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcountry_ZH_CN entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcountry_ZH_CN updateEntity, string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcountry_ZH_CN GetEntity(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcountry_ZH_CNs GetList(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

