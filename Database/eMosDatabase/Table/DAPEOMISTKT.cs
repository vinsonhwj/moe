﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOMISTKT]
    /// </summary>
    public partial class DAPEOMISTKT : DALDependency<tbPEOMISTKT, tbPEOMISTKTs>
    {
        public DAPEOMISTKT(IConnection conn)
            : base(conn)
        { }

        public bool Exists(string companyCode, string ticket, string tktSeq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOMISTKT entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOMISTKT updateEntity, string companyCode, string ticket, string tktSeq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOMISTKT GetEntity(string companyCode, string ticket, string tktSeq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOMISTKTs GetList(string companyCode, string ticket, string tktSeq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOMISTKTs GetList(string companyCode, List<string> ticketList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMISTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMISTKT.Fields.Ticket, ticketList, Enums.Relation.IN , Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.Add(new SortParam(tbPEOMISTKT.Fields.Ticket, Enums.OrderBy.Ascending));
            sp.Add(new SortParam(tbPEOMISTKT.Fields.PaxName, Enums.OrderBy.Ascending));

            return base.GetList(null, fp,sp);
        }

        public tbPEOMISTKTPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbPEOMISTKTPage page = new tbPEOMISTKTPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbPEOMISTKT.Fields.CompanyCode);
            pk.Add(tbPEOMISTKT.Fields.Ticket);
            pk.Add(tbPEOMISTKT.Fields.TktSeq);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

