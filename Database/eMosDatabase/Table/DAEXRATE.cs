﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:EXRATE]
    /// </summary>
    public partial class DAEXRATE : DALDependency<tbEXRATE, tbEXRATEs>
    {
        public DAEXRATE(string connectionString)
            : base(connectionString)
        {
            TableName = tbEXRATE.DBTableName;
        }

        public DAEXRATE(IConnection conn)
            : base(conn)
        {

        }
        public bool Add(tbEXRATE entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbEXRATE updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public tbEXRATE GetEntity(string companyCode, string targetCurr, string sourceCurr)
        {

            FilterParams fp = new FilterParams();
            fp.AddParam(tbEXRATE.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbEXRATE.Fields.TARGETCURR, targetCurr, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbEXRATE.Fields.SOURCECURR, sourceCurr, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

    }
}

