﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:SUPPLIER]
    /// </summary>
    public partial class DASUPPLIER : DALDependency<tbSUPPLIER, tbSUPPLIERs>
    {
        public DASUPPLIER(string connectionString)
            : base(connectionString)
        {
            TableName = tbSUPPLIER.DBTableName;
        }
        public DASUPPLIER(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string suppcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbSUPPLIER entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbSUPPLIER updateEntity, string companycode, string suppcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string suppcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbSUPPLIER GetEntity(string companycode, string suppcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbSUPPLIERs GetList(string companycode, string suppcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public bool IsActive(string companyCode, string suppCode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbSUPPLIER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.SUPPCODE, suppCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbSUPPLIER.Fields.STATUS, "1", Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

    }
}

