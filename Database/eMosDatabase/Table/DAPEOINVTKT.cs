﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINVTKT]
    /// </summary>
    public partial class DAPEOINVTKT : DALDependency<tbPEOINVTKT, tbPEOINVTKTs>
    {
        public DAPEOINVTKT(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINVTKT.DBTableName;
        }

        public DAPEOINVTKT(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string companyCode, string ticket, string tktSeq, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOINVTKT entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINVTKT updateEntity, string companyCode, string ticket, string tktSeq, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOINVTKT GetEntity(string companyCode, string ticket, string tktSeq, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOINVTKTs GetList(string companyCode, string ticket, string tktSeq, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINVTKTs GetList(string companyCode, string ticket, string tktSeq, string invNum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbPEOINVTKTs GetList(string companyCode, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKT.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
    }
}

