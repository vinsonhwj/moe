﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOAIR]
    /// </summary>
    public partial class DAPEOAIR : DALDependency<tbPEOAIR, tbPEOAIRs>
    {
        public DAPEOAIR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOAIR.DBTableName;
        }

        public DAPEOAIR(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companyCode, string bkgRef, string segNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOAIR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOAIR updateEntity, string companyCode, string bkgRef, string segNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string bkgRef, string segNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOAIR GetEntity(string companyCode, string bkgRef, string segNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, segNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEOAIRs GetEntity(string companyCode, string bkgRef, List<string> airSegNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, airSegNumList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOAIRs GetList(string companyCode, string bkgRef)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOAIRs GetList(string companyCode, string bkgRef, List<string> segNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, segNumList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOAIRs GetListByBKGREF(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOAIR.Fields.SegNum, Enums.OrderBy.Ascending);

            return base.GetList(null, fp, sp);
        }

        public static SqlEntity DeleteByBkgref(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            return DeleteSqlEntity(fp);
        }

        public int GetMaxSeg(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOAIR.Fields.SegNum, Enums.OrderBy.Descending);

            tbPEOAIR tb = base.GetEntity(null, fp, sp);
            return tb == null ? 0 : Convert.ToInt32(tb.SegNum);
        }

        public tbPEOAIRs GetOriginalList(string companyCode, string bkgref, string sourcrPnr)
        {
            DisplayFields df = new DisplayFields();
            df.Add(tbPEOAIR.Fields.SegNum);

            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SourcePnr, sourcrPnr, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetList(df, fp);
        }
        public tbPEOAIRs GetListIgnoreSourcePnr(string companyCode, string bkgref, string sourcrPnr)
        {
            DisplayFields df = new DisplayFields();
            df.Add(tbPEOAIR.Fields.SegNum);

            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SourcePnr, sourcrPnr, Enums.Relation.Unequal, Enums.Expression.AND);

            return base.GetList(df, fp);
        }

        public tbPEOAIRs GetPaxIgnoreOriginal(string companyCode, string bkgRef, string sourcePnr)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SourcePnr, sourcePnr, Enums.Relation.Unequal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }



        public bool IsFirstIUR(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR.Fields.SegNum, "00001", Enums.Relation.Equal, Enums.Expression.AND);

            fp.AddParam(tbPEOAIR.Fields.SourceSystem, "IUR", Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(null, fp) != null;

        }
    }
}

