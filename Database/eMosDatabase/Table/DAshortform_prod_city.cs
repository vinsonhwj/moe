﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:shortform_prod_city]
    /// </summary>
    public partial class DAshortform_prod_city : DALDependency<tbshortform_prod_city, tbshortform_prod_citys>
    {
        public DAshortform_prod_city(IConnection conn)
            : base(conn)
        { }


        public bool Add(tbshortform_prod_city entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbshortform_prod_city updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbshortform_prod_city GetEntity(string code)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbshortform_prod_city.Fields.code, code, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbshortform_prod_citys GetList()
        {
            return base.GetList();
        }

        public tbshortform_prod_cityPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbshortform_prod_cityPage page = new tbshortform_prod_cityPage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

