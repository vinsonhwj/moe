﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:Customer]
    /// </summary>
    public partial class DACustomer : DALDependency<tbCustomer, tbCustomers>
    {
        public DACustomer(string connectionString)
            : base(connectionString)
        {
            TableName = tbCustomer.DBTableName;
        }
        public DACustomer(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string companyCode, string cltCode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCustomer.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCustomer.Fields.CLTCODE, cltCode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbCustomer entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbCustomer updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public tbCustomer GetEntity(string companycode, string cltcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCustomer.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCustomer.Fields.CLTCODE, cltcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
    }
}

