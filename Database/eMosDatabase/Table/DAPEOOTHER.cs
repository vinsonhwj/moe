﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOOTHER]
    /// </summary>
    public partial class DAPEOOTHER : DALDependency<tbPEOOTHER, tbPEOOTHERs>
    {
        public DAPEOOTHER(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOOTHER.DBTableName;
        }
        public DAPEOOTHER(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOOTHER entity)
        {
            return base.Add(entity);
        }
        public bool Update(tbPEOOTHER updateEntity, string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }
        public bool Delete(string companycode, string bkgref, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOOTHER GetEntity(string companycode, string bkgref, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOOTHERs GetListForOther(string companycode, string bkgref, List<string> OtherSegNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, OtherSegNumList, Enums.Relation.IN, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.VOIDON, null, Enums.Relation.IsNull, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOOTHERs GetList(string companyCode, string bkGref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkGref, Enums.Relation.Equal, Enums.Expression.None);
            return base.GetList(null, fp);
        }
        public tbPEOOTHERs GetList(string companyCode, string bkgRef, List<string> otherSegNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.SEGNUM, otherSegNumList, Enums.Relation.IN, Enums.Expression.AND);

            return base.GetList(null, fp);
        }

        public string GetMaxSegNum(string companyCode, string bkgRef)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.Add(new SortParam(tbPEOOTHER.Fields.SEGNUM, Enums.OrderBy.Descending));

            tbPEOOTHER o = GetEntity(null, fp, sp);
            if (o == null)
                return string.Empty;
            else
                return o.SEGNUM;

        }

        public static SqlList Insert(tbPEOOTHER entity)
        {
            SqlList sqlList = new SqlList();
            if (entity != null)
            {
                sqlList.Add(AddSqlEntity(entity));
            }
            return sqlList;
        }

        public int GetCountByBkgref(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHER.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetList(null, fp).Count;
        }
    }
}

