﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:tbDocumentBanner]
    /// </summary>
    public partial class DADocumentBanner : DALDependency<tbDocumentBanner, tbDocumentBanners>
    {
        public DADocumentBanner(string connectionString)
            : base(connectionString)
        {
            TableName = tbDocumentBanner.DBTableName;
        }
        public DADocumentBanner(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string bkgref, string docnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbDocumentBanner entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbDocumentBanner updateEntity, string companycode, string bkgref, string docnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string bkgref, string docnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbDocumentBanner GetEntity(string companycode, string bkgref, string docnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbDocumentBanners GetList(string companycode, string bkgref, string docnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbDocumentBanners GetList(string companycode, string bkgref, string docnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbDocumentBanner.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbDocumentBanner.Fields.docnum, docnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbDocumentBanners GetAllList()
        {
            return base.GetList();
        }

        public tbDocumentBannerPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbDocumentBannerPage page = new tbDocumentBannerPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbDocumentBanner.Fields.companycode);
            pk.Add(tbDocumentBanner.Fields.bkgref);
            pk.Add(tbDocumentBanner.Fields.docnum);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

