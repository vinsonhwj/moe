﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOMSTR]
    /// </summary>
    public partial class DAPEOMSTR : DALDependency<tbPEOMSTR, tbPEOMSTRs>
    {
        public DAPEOMSTR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOMSTR.DBTableName;
        }

        public DAPEOMSTR(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOMSTR entity)
        {
            return base.Add(entity);
        }
        public bool Update(tbPEOMSTR updateEntity, string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }
        public static SqlList UpdateForInv(tbPEOMSTR updateEntity)
        {
            SqlList sqlList = new SqlList();

            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, updateEntity.COMPANYCODE, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, updateEntity.BKGREF, Enums.Relation.Equal, Enums.Expression.AND);
            UpdateParam up = new UpdateParam();
            sqlList.Add(UpdateSqlEntity(up, fp));

            return sqlList;
        }
        public bool Delete(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOMSTR GetEntity(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetEntity(fp);
        }
        public tbPEOMSTR GetEntityForPMQ(string companyCode, string masterPNR)
        {
            string sql = @"SELECT SUM(ISNULL(INVAMT, 0)) AS INVAMT ,SUM(ISNULL(INVTAX, 0)) AS INVTAX ,
                                 SUM(ISNULL(DOCAMT, 0)) AS DOCAMT ,SUM(ISNULL(DOCTAX, 0)) AS DOCTAX ,
                                 SUM(ISNULL(INVCOUNT, 0)) AS INVCOUNT ,SUM(ISNULL(DOCCOUNT, 0)) AS DOCCOUNT ,
                                 SUM(ISNULL(TTLCOSTAMT, 0)) AS TTLCOSTAMT ,SUM(ISNULL(TTLCOSTTAX, 0)) AS TTLCOSTTAX ,
                                 (SELECT cltode FROM Peomstr WHERE CompanyCode = m.CompanyCode AND bkgref = m.masterpnr) AS cltcode
                                 FROM PeoMstr M WHERE CompanyCode = @CompanyCode
                                 AND MasterPNR = @MasterPNR
                                 GROUP BY M.Companycode ,M.masterpnr";

            List<IDbDataParameter> paramLst = new List<IDbDataParameter>();
            paramLst.Add(new SqlParameter("@CompanyCode", companyCode));
            paramLst.Add(new SqlParameter("@MasterPNR", masterPNR));
            return base.GetEntity(sql, paramLst);
        }

        public tbPEOMSTRs GetList(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOMSTRs GetListForMasterPNR(string companycode, string masterpnr)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.MASTERPNR, masterpnr, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOMSTRs GetListByObject(tbPEOMSTR master, DateTime createOn)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR.Fields.COMPANYCODE, master.COMPANYCODE, Enums.Relation.Equal, Enums.Expression.AND);
            if (!string.IsNullOrEmpty(master.PNR))
                fp.AddParam(tbPEOMSTR.Fields.PNR, master.PNR, Enums.Relation.Equal, Enums.Expression.AND);
            if (!string.IsNullOrEmpty(master.BKGREF))
                fp.AddParam(tbPEOMSTR.Fields.BKGREF, master.BKGREF, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR.Fields.CREATEON, createOn, Enums.Relation.Greater, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

