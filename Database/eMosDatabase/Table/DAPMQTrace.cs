﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PMQTrace]
    /// </summary>
    public partial class DAPMQTrace : DALDependency<tbPMQTrace, tbPMQTraces>
    {
        public DAPMQTrace(string connectionString)
            : base(connectionString)
        {
            TableName = tbPMQTrace.DBTableName;
        }

        public bool Exists(string companycode, string bkgref, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPMQTrace entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPMQTrace updateEntity, string companycode, string bkgref, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string bkgref, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPMQTrace GetEntity(string companycode, string bkgref, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPMQTraces GetList(string companycode, string bkgref, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPMQTraces GetList(string companycode, string bkgref, string seqnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPMQTrace.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPMQTrace.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbPMQTracePage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbPMQTracePage page = new tbPMQTracePage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbPMQTrace.Fields.COMPANYCODE);
            pk.Add(tbPMQTrace.Fields.BKGREF);
            pk.Add(tbPMQTrace.Fields.SEQNUM);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

