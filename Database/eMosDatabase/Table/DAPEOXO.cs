﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOXO]
    /// </summary>
    public partial class DAPEOXO : DALDependency<tbPEOXO, tbPEOXOs>
    {
        public DAPEOXO(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOXO.DBTableName;
        }

        public bool Exists(string companycode, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOXO entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOXO updateEntity, string companycode, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOXO GetEntity(string companycode, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOXOs GetList(string companycode, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOXOs GetList(string companycode, string xonum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXO.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXO.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

