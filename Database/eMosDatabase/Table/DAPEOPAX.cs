﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOPAX]
    /// </summary>
    public partial class DAPEOPAX : DALDependency<tbPEOPAX, tbPEOPAXs>
    {
        public DAPEOPAX(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOPAX.DBTableName;
        }
        public DAPEOPAX(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOPAX entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOPAX updateEntity, string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOPAX GetEntity(string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEOPAX GetEntityForAgeMatchOth(string bkgref, string companycode, List<string> segnumList, string paxother)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnumList, Enums.Relation.IN, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.PAXOTHER, paxother, Enums.Relation.Unequal, Enums.Expression.AND);

            DisplayFields dp = new DisplayFields();
            dp.Add(tbPEOPAX.Fields.BKGREF);
            dp.Add(tbPEOPAX.Fields.COMPANYCODE);
            dp.Add(tbPEOPAX.Fields.SEGNUM);
            dp.Add(tbPEOPAX.Fields.PAXOTHER);

            return base.GetEntity(dp, fp);
        }
        public tbPEOPAX GetEntityForAgeMatchAir(string bkgref, string companycode, List<string> segnumList, string paxair)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnumList, Enums.Relation.IN, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.PAXOTHER, paxair, Enums.Relation.Unequal, Enums.Expression.AND);

            DisplayFields dp = new DisplayFields();
            dp.Add(tbPEOPAX.Fields.BKGREF);
            dp.Add(tbPEOPAX.Fields.COMPANYCODE);
            dp.Add(tbPEOPAX.Fields.SEGNUM);
            dp.Add(tbPEOPAX.Fields.PAXAIR);

            return base.GetEntity(dp, fp);
        }
        public tbPEOPAXs GetList(string bkgref, string companycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOPAXs GetList(string bkgref, string companycode, List<string> segnumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnumList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }


        public tbPEOPAXs GetListByBKGREF(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOPAX.Fields.SEGNUM, Enums.OrderBy.Descending);

            return base.GetList(null, fp, sp);
        }

        public static SqlEntity DeleteByBkgref(string companyCode, string bkgref, string sourcrPnr)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            return DAPEOPAX.DeleteSqlEntity(fp);
        }

        public int GetMaxSeg(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOPAX.Fields.SEGNUM, Enums.OrderBy.Descending);

            tbPEOPAX tb = base.GetEntity(null, fp, sp);
            return tb == null ? 0 : Convert.ToInt32(tb.SEGNUM);
        }

        public tbPEOPAXs GetPaxIgnoreOriginal(string companyCode, string bkgRef, string sourcePnr)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SourcePnr, sourcePnr, Enums.Relation.Unequal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public int RecordCount(string companyCode, string bkgRef, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return this.RecordCount(fp);
        }

        public tbPEOPAXs GetListSortBySEGNUM(string companyCode, string bkgRef, List<string> selectedPax)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX.Fields.SEGNUM, selectedPax, Enums.Relation.IN, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.AddParam(tbPEOPAX.Fields.SEGNUM, Enums.OrderBy.Ascending);

            return base.GetList(null, fp, sp);

        }
    }
}

