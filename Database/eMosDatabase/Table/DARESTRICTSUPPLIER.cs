﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:RESTRICTSUPPLIER]
    /// </summary>
    public partial class DARESTRICTSUPPLIER : DALDependency<tbRESTRICTSUPPLIER, tbRESTRICTSUPPLIERs>
    {
        public DARESTRICTSUPPLIER(string connectionString)
            : base(connectionString)
        {
            TableName = tbRESTRICTSUPPLIER.DBTableName;
        }
        public DARESTRICTSUPPLIER(IConnection conn)
            : base(conn)
        {

        }



        public bool Add(tbRESTRICTSUPPLIER entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbRESTRICTSUPPLIER updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public bool Exists(string companyCode, string suppCode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbRESTRICTSUPPLIER.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbRESTRICTSUPPLIER.Fields.SUPPCODE, suppCode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public tbRESTRICTSUPPLIERs GetList()
        {
            return base.GetList();
        }
    }
}

