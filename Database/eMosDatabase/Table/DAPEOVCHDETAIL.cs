﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOVCHDETAIL]
    /// </summary>
    public partial class DAPEOVCHDETAIL : DALDependency<tbPEOVCHDETAIL, tbPEOVCHDETAILs>
    {
        public DAPEOVCHDETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOVCHDETAIL.DBTableName;
        }
        public DAPEOVCHDETAIL(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string vchnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOVCHDETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOVCHDETAIL updateEntity, string companycode, string vchnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string vchnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOVCHDETAIL GetEntity(string companycode, string vchnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOVCHDETAILs GetList(string companycode, string vchnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOVCHDETAILs GetList(string companycode, string vchnum, string segnum, string seqnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCHDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCHDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

