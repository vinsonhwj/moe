﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOTKT]
    /// </summary>
    public partial class DAPEOTKT : DALDependency<tbPEOTKT, tbPEOTKTs>
    {
        public DAPEOTKT(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOTKT.DBTableName;
        }
        public DAPEOTKT(IConnection conn)
            : base(conn)
        { }

        public bool Exists(string companyCode, string ticket, string tktSeq, string airline)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOTKT entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOTKT updateEntity, string companyCode, string ticket, string tktSeq, string airline)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq, string airline)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOTKT GetEntity(string companyCode, string ticket, string tktSeq, string airline)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOTKTs GetList(string companyCode, string ticket, string tktSeq, string airline)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOTKTs GetList(string companyCode, string ticket, string tktSeq, string airline, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Airline, airline, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }


        public tbPEOTKTs GetList(string companycode, string pnr)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.PNR, pnr, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOTKTs GetList(string companycode, List<string> ticketList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticketList, Enums.Relation.IN, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.Add(new SortParam(tbPEOTKT.Fields.Ticket, Enums.OrderBy.Ascending));
            sp.Add(new SortParam(tbPEOTKT.Fields.PaxName, Enums.OrderBy.Ascending));
            return base.GetList(null, fp, sp);
        }
        public tbPEOTKTs GetAnotherBKGTicket(string companycode, string bkg, List<string> list)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.BKGREF, bkg, Enums.Relation.Unequal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, list, Enums.Relation.IN, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.Add(new SortParam(tbPEOTKT.Fields.Ticket, Enums.OrderBy.Ascending));
            sp.Add(new SortParam(tbPEOTKT.Fields.PaxName, Enums.OrderBy.Ascending));
            return base.GetList(null, fp, sp);
        }

        public tbPEOTKTs GetUnVoidInvoicedList(string companyCode, string bkgref, string ticket)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.InvNum, "%I%", Enums.Relation.Like, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.VoidOn, null, Enums.Relation.IsNull, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOTKTs GetListByBKGREF(string companycode, string BKGREF)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT.Fields.CompanyCode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT.Fields.BKGREF, BKGREF, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }


    }
}

