﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINVTKTref]
    /// </summary>
    public partial class DAPEOINVTKTref :DALDependency<tbPEOINVTKTref, tbPEOINVTKTrefs>
    {
        public DAPEOINVTKTref(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINVTKTref.DBTableName;
        }
        public DAPEOINVTKTref(IConnection conn)
            : base(conn)
        { }

        public bool Exists(string companyCode, string ticket, string tktSeq, string bkgRef, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOINVTKTref entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINVTKTref updateEntity, string companyCode, string ticket, string tktSeq, string bkgRef, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq, string bkgRef, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOINVTKTref GetEntity(string companyCode, string ticket, string tktSeq, string bkgRef, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOINVTKTrefs GetList(string companyCode, string ticket, string tktSeq, string bkgRef, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINVTKTrefs GetList(string companyCode,string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINVTKTrefs GetList(string companyCode, string ticket, string tktSeq, string bkgRef, string invNum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVTKTref.Fields.InvNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

