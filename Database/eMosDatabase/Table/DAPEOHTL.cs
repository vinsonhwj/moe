﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOHTL]
    /// </summary>
    public partial class DAPEOHTL : DALDependency<tbPEOHTL, tbPEOHTLs>
    {
        public DAPEOHTL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOHTL.DBTableName;
        }
        public DAPEOHTL(IConnection conn)
            : base(conn)
        {

        }

        //public bool Exists(string bkgref, string companycode, string segnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPEOHTL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}
        public bool Add(tbPEOHTL entity)
        {
            return base.Add(entity);
        }
        //public bool Update(tbPEOHTL updateEntity, string bkgref, string companycode, string segnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPEOHTL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}
        //public bool Delete(string bkgref, string companycode, string segnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPEOHTL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOHTL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbPEOHTL GetEntity(string companycode, string bkgref, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOHTL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOHTL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOHTLs GetList(string companyCode, string bkgRef, List<string> segNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOHTL.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOHTL.Fields.SEGNUM, segNumList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOHTLs GetList(string companyCode, string bkGref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHTL.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOHTL.Fields.BKGREF, bkGref, Enums.Relation.Equal, Enums.Expression.None);
            return base.GetList(null, fp);
        }

    }
}

