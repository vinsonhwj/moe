﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOOTHERDETAIL]
    /// </summary>
    public partial class DAPEOOTHERDETAIL : DALDependency<tbPEOOTHERDETAIL, tbPEOOTHERDETAILs>
    {
        public DAPEOOTHERDETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOOTHERDETAIL.DBTableName;
        }
        public DAPEOOTHERDETAIL(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string bkgref, string companycode, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOOTHERDETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOOTHERDETAIL updateEntity, string bkgref, string companycode, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string bkgref, string companycode, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOOTHERDETAIL GetEntity(string companycode, string bkgref, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEOOTHERDETAILs GetEntity(string bkgref, string companycode, List<string> segnumList, string seqtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segnumList, Enums.Relation.IN, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, seqtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOOTHERDETAILs GetList(string companyCode, string bkgRef, List<string> segNumList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, segNumList, Enums.Relation.IN, Enums.Expression.AND);

            return base.GetList(null, fp);
        }

        public tbPEOOTHERDETAILs GetList(string companyCode, string bkGref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, bkGref, Enums.Relation.Equal, Enums.Expression.None);
            SortParams sp = new SortParams();
            sp.AddParam(tbPEOOTHERDETAIL.Fields.COMPANYCODE, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOOTHERDETAIL.Fields.BKGREF, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOOTHERDETAIL.Fields.SEGNUM, Enums.OrderBy.Ascending);
            sp.AddParam(tbPEOOTHERDETAIL.Fields.SEQNUM, Enums.OrderBy.Ascending);
            return base.GetList(null, fp, sp);
        }
        public static SqlList InsertList(tbPEOOTHERDETAILs list)
        {

            SqlList sqlList = new SqlList();
            if (list != null)
            {
                foreach (tbPEOOTHERDETAIL d in list)
                {
                    sqlList.Add(AddSqlEntity(d));
                }
            }
            return sqlList;
        }
    }
}

