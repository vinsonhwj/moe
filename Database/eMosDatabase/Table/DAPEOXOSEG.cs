﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOXOSEG]
    /// </summary>
    public partial class DAPEOXOSEG : DALDependency<tbPEOXOSEG, tbPEOXOSEGs>
    {
        public DAPEOXOSEG(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOXOSEG.DBTableName;
        }

        public bool Exists(string companycode, string xonum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOXOSEG entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOXOSEG updateEntity, string companycode, string xonum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string xonum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOXOSEG GetEntity(string companycode, string xonum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOXOSEGs GetList(string companycode, string xonum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOXOSEGs GetList(string companycode, string xonum, string segnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

