﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOVCH]
    /// </summary>
    public partial class DAPEOVCH : DALDependency<tbPEOVCH, tbPEOVCHs>
    {
        public DAPEOVCH(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOVCH.DBTableName;
        }
        public DAPEOVCH(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string vchnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOVCH entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOVCH updateEntity, string companycode, string vchnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string vchnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOVCH GetEntity(string companycode, string vchnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOVCHs GetList(string companycode, string vchnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOVCHs GetList(string companycode, string vchnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOVCH.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOVCH.Fields.VCHNUM, vchnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

