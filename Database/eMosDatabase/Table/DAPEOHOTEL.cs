﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOHOTEL]
    /// </summary>
    public partial class DAPEOHOTEL : DALDependency<tbPEOHOTEL, tbPEOHOTELs>
    {
        public DAPEOHOTEL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOHOTEL.DBTableName;
        }
        public DAPEOHOTEL(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string hotelcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHOTEL.Fields.HOTELCODE, hotelcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOHOTEL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOHOTEL updateEntity, string hotelcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHOTEL.Fields.HOTELCODE, hotelcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string hotelcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHOTEL.Fields.HOTELCODE, hotelcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOHOTEL GetEntity(string hotelCode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHOTEL.Fields.HOTELCODE, hotelCode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOHOTELs GetList(List<string> hotelCodeList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOHOTEL.Fields.HOTELCODE, hotelCodeList, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }



    }
}

