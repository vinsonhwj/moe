﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:LOGIN]
    /// </summary>
    public partial class DALOGIN : DALDependency<tbLOGIN, tbLOGINs>
    {
        public DALOGIN(string connectionString)
            : base(connectionString)
        {
            TableName = tbLOGIN.DBTableName;
        }
        public DALOGIN(IConnection conn)
            : base(conn)
        {

        }
        public bool Exists(string companycode, string login)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbLOGIN entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbLOGIN updateEntity, string companycode, string login)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string login)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbLOGIN GetEntity(string companycode, string login)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbLOGINs GetList(string companycode, string login)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbLOGINs GetList(string companycode, string login, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbLOGIN.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbLOGIN.Fields.LOGIN, login, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbLOGINPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbLOGINPage page = new tbLOGINPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbLOGIN.Fields.COMPANYCODE);
            pk.Add(tbLOGIN.Fields.LOGIN);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

