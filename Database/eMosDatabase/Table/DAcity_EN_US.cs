﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:city_EN_US]
    /// </summary>
    public partial class DAcity_EN_US : DALDependency<tbcity_EN_US, tbcity_EN_USs>
    {
        public DAcity_EN_US(string connectionString)
            : base(connectionString)
        {
            TableName = tbcity_EN_US.DBTableName;
        }

        public DAcity_EN_US(IConnection conn)
            : base(conn)
        { }

        //public bool Exists(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_EN_US.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcity_EN_US entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcity_EN_US updateEntity, string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_EN_US.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_EN_US.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcity_EN_US GetEntity(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_EN_US.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcity_EN_USs GetList(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_EN_US.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_EN_US.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

