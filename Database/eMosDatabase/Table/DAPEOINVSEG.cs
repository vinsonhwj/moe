﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINVSEG]
    /// </summary>
    public partial class DAPEOINVSEG : DALDependency<tbPEOINVSEG, tbPEOINVSEGs>
    {
        public DAPEOINVSEG(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINVSEG.DBTableName;
        }
        public DAPEOINVSEG(IConnection conn)
            : base(conn)
        {
            // TableName = tbPEOINV.DBTableName;
        }
        public bool Exists(string companycode, string invnum, string segnum, string servtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOINVSEG entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINVSEG updateEntity, string companycode, string invnum, string segnum, string servtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string invnum, string segnum, string servtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOINVSEG GetEntity(string companycode, string invnum, string segnum, string servtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOINVSEGs GetList(string companycode, string invnum, string segnum, string servtype)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOINVSEGs GetList(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);

            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetList(null, fp);
        }

        public tbPEOINVSEGs GetList(string companycode, List<string> invnums)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);

            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnums, Enums.Relation.IN , Enums.Expression.AND);

            return base.GetList(null, fp);
        }

        public tbPEOINVSEGs GetList(string companycode, string invnum, string segnum, string servtype, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVSEG.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVSEG.Fields.SERVTYPE, servtype, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public static SqlList InsertList(tbPEOINVSEGs list)
        {

            SqlList sqlList = new SqlList();
            if (list != null)
            {
                foreach (tbPEOINVSEG s in list)
                {
                    sqlList.Add(AddSqlEntity(s));
                }
            }
            return sqlList;
        }
    }
}

