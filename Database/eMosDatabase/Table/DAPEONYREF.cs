﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEONYREF]
    /// </summary>
    public partial class DAPEONYREF : DALDependency<tbPEONYREF, tbPEONYREFs>
    {
        public DAPEONYREF(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEONYREF.DBTableName;
        }
        public DAPEONYREF(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(decimal refid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEONYREF entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEONYREF updateEntity, decimal refid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(decimal refid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEONYREF GetEntity(decimal refid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }
        public tbPEONYREF GetFristEntity(string companyCode, string invNum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEONYREF.Fields.DocNum, invNum, Enums.Relation.Equal, Enums.Expression.AND);

            SortParams sp = new SortParams();
            sp.Add(new SortParam(tbPEONYREF.Fields.REFID, Enums.OrderBy.Ascending));

            return base.GetEntity(null, fp, sp);
        }

        public tbPEONYREFs GetList(decimal refid)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEONYREFs GetList(decimal refid, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEONYREF.Fields.REFID, refid, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }
        public static SqlList Insert(tbPEONYREF entity)
        {
            SqlList sqlList = new SqlList();
            if (entity != null)
            {
                sqlList.Add(AddSqlEntity(entity));

            }
            return sqlList;
        }

    }
}

