﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINPAX]
    /// </summary>
    public partial class DAPEOINPAX : DALDependency<tbPEOINPAX, tbPEOINPAXs>
    {
        public DAPEOINPAX(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINPAX.DBTableName;
        }
        public DAPEOINPAX(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string invnum, string seqnum, int paxseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOINPAX entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINPAX updateEntity, string companycode, string invnum, string seqnum, int paxseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string invnum, string seqnum, int paxseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOINPAX GetEntity(string companycode, string invnum, string seqnum, int paxseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOINPAXs GetList(string companycode, string invnum, string seqnum, int paxseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOINPAXs GetList(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINPAXs GetList(string companycode, string invnum, string seqnum, int paxseq, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINPAX.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINPAX.Fields.PAXSEQ, paxseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public tbPEOINPAXPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbPEOINPAXPage page = new tbPEOINPAXPage();
            DisplayFields pk = new DisplayFields();
            pk.Add(tbPEOINPAX.Fields.COMPANYCODE);
            pk.Add(tbPEOINPAX.Fields.INVNUM);
            pk.Add(tbPEOINPAX.Fields.SEQNUM);
            pk.Add(tbPEOINPAX.Fields.PAXSEQ);
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

        public static SqlList InsertList(tbPEOINPAXs list)
        {
          
            SqlList sqlList = new SqlList();
            if (list != null)
            {
                foreach (tbPEOINPAX p in list)
                {
                    sqlList.Add(AddSqlEntity(p));
                }
            }
            return sqlList;
        }

    }
}

