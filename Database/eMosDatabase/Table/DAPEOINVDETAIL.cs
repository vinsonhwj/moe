﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOINVDETAIL]
    /// </summary>
    public partial class DAPEOINVDETAIL : DALDependency<tbPEOINVDETAIL, tbPEOINVDETAILs>
    {
        public DAPEOINVDETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOINVDETAIL.DBTableName;
        }
        public DAPEOINVDETAIL(IConnection conn)
            : base(conn)
        {
            // TableName = tbPEOINV.DBTableName;
        }
        public bool Exists(string companycode, string invnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOINVDETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOINVDETAIL updateEntity, string companycode, string invnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string invnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOINVDETAIL GetEntity(string companycode, string invnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOINVDETAILs GetList(string companycode, string invnum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOINVDETAILs GetList(string companycode, string invnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOINVDETAILs GetList(string companycode, List<string> invnums)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnums, Enums.Relation.IN, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOINVDETAILs GetList(string companycode, string invnum, string segnum, string seqnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOINVDETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.INVNUM, invnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOINVDETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

        public static SqlList InsertList(tbPEOINVDETAILs list)
        {

            SqlList sqlList = new SqlList();
            if (list != null)
            {
                foreach (tbPEOINVDETAIL d in list)
                {
                    sqlList.Add(AddSqlEntity(d));
                }
            }
            return sqlList;
        }
    }
}

