﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:city]
    /// </summary>
    public partial class DAcity : DALDependency<tbcity, tbcitys>
    {
        public DAcity(string connectionString)
            : base(connectionString)
        {
            TableName = tbcity.DBTableName;
        }
        public DAcity(IConnection conn)
            : base(conn)
        { }
        //public bool Exists(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}
        //public bool Add(tbcity entity)
        //{
        //    return base.Add(entity);
        //}
        //public bool Update(tbcity updateEntity, string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}
        //public bool Delete(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcity GetEntity(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcitys GetList(List<string> cityCodeList)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity.Fields.CITYCODE, cityCodeList, Enums.Relation.IN, Enums.Expression.AND);

            return base.GetList(null, fp);
        }
        

    }
}

