﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:city_ZH_TW]
    /// </summary>
    public partial class DAcity_ZH_TW : DALDependency<tbcity_ZH_TW, tbcity_ZH_TWs>
    {
        public DAcity_ZH_TW(IConnection conn)
            : base(conn)
        { }

        public tbcity_ZH_TW GetEntity(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_ZH_TW.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcity_ZH_TWs GetList(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_ZH_TW.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

