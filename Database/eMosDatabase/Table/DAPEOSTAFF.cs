﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOSTAFF]
    /// </summary>
    public partial class DAPEOSTAFF : DALDependency<tbPEOSTAFF, tbPEOSTAFFs>
    {
        public DAPEOSTAFF(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOSTAFF.DBTableName;
        }
        public DAPEOSTAFF(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string companycode, string staffcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOSTAFF.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOSTAFF.Fields.STAFFCODE, staffcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOSTAFF entity)
        {
            return base.Add(entity);
        }
        public bool Update(tbPEOSTAFF updateEntity, string companycode, string staffcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOSTAFF.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOSTAFF.Fields.STAFFCODE, staffcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }
        public bool Delete(string companycode, string staffcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOSTAFF.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOSTAFF.Fields.STAFFCODE, staffcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOSTAFF GetEntity(string companycode, string staffcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOSTAFF.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOSTAFF.Fields.STAFFCODE, staffcode, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetEntity(fp);
        }
        public tbPEOSTAFFs GetList(string companycode, string staffcode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOSTAFF.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOSTAFF.Fields.STAFFCODE, staffcode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public string GetBranchCodeByStaffCode(string companyCode, string staffCode, string bkgRef)
        {
            if (!string.IsNullOrEmpty(bkgRef))
            {
                tbPEOMSTR mstr = new DAPEOMSTR(this.ConnectionString).GetEntity(companyCode, bkgRef);
                if (mstr != null)
                {
                    staffCode = mstr.STAFFCODE;
                }
            }
            tbPEOSTAFF staff = GetEntity(companyCode, staffCode);

            if (staff != null)
            {
                return staff.BRANCHCODE;
            }
            else
            {
                return string.Empty;
            }
        }

    }
}

