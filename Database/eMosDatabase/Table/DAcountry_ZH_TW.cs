﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:country_ZH_TW]
    /// </summary>
    public partial class DAcountry_ZH_TW : DALDependency<tbcountry_ZH_TW, tbcountry_ZH_TWs>
    {
        public DAcountry_ZH_TW(string connectionString)
            : base(connectionString)
        {
            TableName = tbcountry_ZH_TW.DBTableName;
        }
        public DAcountry_ZH_TW(IConnection conn)
            : base(conn)
        { }
        //public bool Exists(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcountry_ZH_TW entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcountry_ZH_TW updateEntity, string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcountry_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcountry_ZH_TW GetEntity(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcountry_ZH_TWs GetList(string countrycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry_ZH_TW.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }


    }
}

