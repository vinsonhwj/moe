﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:shortform_prod_AirClassType]
    /// </summary>
    public partial class DAshortform_prod_AirClassType : DALDependency<tbshortform_prod_AirClassType, tbshortform_prod_AirClassTypes>
    {
        public DAshortform_prod_AirClassType(IConnection conn)
            : base(conn)
        { }


        public bool Add(tbshortform_prod_AirClassType entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbshortform_prod_AirClassType updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbshortform_prod_AirClassType GetEntity(int classType)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbshortform_prod_AirClassType.Fields.ClassType, classType, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbshortform_prod_AirClassTypes GetList()
        {
            return base.GetList();
        }

        public tbshortform_prod_AirClassTypePage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbshortform_prod_AirClassTypePage page = new tbshortform_prod_AirClassTypePage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = base.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

