﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOMIS]
    /// </summary>
    public partial class DAPEOMIS : DALDependency<tbPEOMIS, tbPEOMISs>
    {
        public DAPEOMIS(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOMIS.DBTableName;
        }
        public DAPEOMIS(IConnection conn)
            : base(conn)
        {

        }

        public bool Exists(string bkgref, string companycode, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        //public bool Add(tbPEOMIS entity)
        //{
        //    return base.Add(entity);
        //}
        //public bool Update(tbPEOMIS updateEntity, string bkgref, string companycode, string segnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOMIS.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}
        //public bool Delete(string bkgref, string companycode, string segnum)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbPEOMIS.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbPEOMIS GetEntityWithUnvoid(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.VOIDON, null, Enums.Relation.IsNull, Enums.Expression.AND);

            return base.GetEntity(fp);
        }
        public tbPEOMIS GetEntity(string companycode, string bkgref, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOMISs GetList(string companyCode, string docNum)
        {
            DisplayFields dp = new DisplayFields();
            dp.Add(tbPEOMIS.Fields.BKGREF);
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.BKGREF, docNum, Enums.Relation.Equal, Enums.Expression.None);
            return base.GetList(dp, fp, null);
        }

        public tbPEOMISs GetListByTSANumber(string companyCode, string TSANumber)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.COD1, TSANumber, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
        public tbPEOMISs GetListWithUnvoid(string companyCode, string bkgRef)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMIS.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMIS.Fields.BKGREF, bkgRef, Enums.Relation.Equal, Enums.Expression.None);
            fp.AddParam(tbPEOMIS.Fields.VOIDON, null, Enums.Relation.IsNull, Enums.Expression.None);
            return base.GetList(null, fp);
        }
    }
}

