﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:COMPANY]
    /// </summary>
    public partial class DACOMPANY : DALDependency<tbCOMPANY, tbCOMPANYs>
    {
        public DACOMPANY(string connectionString)
            : base(connectionString)
        {
            TableName = tbCOMPANY.DBTableName;
        }
        public DACOMPANY(IConnection conn)
            : base(conn)
        {

        }

        public tbCOMPANY GetEntity(string companycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCOMPANY.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);

            return base.GetEntity(fp);
        }
        public tbCOMPANYs GetList(string companycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCOMPANY.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public bool IsProfitMarginEnable(string companyCode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbCOMPANY.Fields.COMPANYCODE, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbCOMPANY.Fields.enablePMC, 1, Enums.Relation.Equal, Enums.Expression.AND);

            return base.RecordCount(fp) > 0;
        }
    }
}

