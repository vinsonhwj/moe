﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:city_ZH_CN]
    /// </summary>
    public partial class DAcity_ZH_CN : DALDependency<tbcity_ZH_CN, tbcity_ZH_CNs>
    {
        public DAcity_ZH_CN(string connectionString)
            : base(connectionString)
        {
            TableName = tbcity_ZH_CN.DBTableName;
        }
        public DAcity_ZH_CN(IConnection conn)
            : base(conn)
        { }

        //public bool Exists(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_ZH_CN.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.RecordCount(fp) > 0;
        //}

        //public bool Add(tbcity_ZH_CN entity)
        //{
        //    return base.Add(entity);
        //}

        //public bool Update(tbcity_ZH_CN updateEntity, string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_ZH_CN.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Update(updateEntity, fp);
        //}

        //public bool Delete(string countrycode, string citycode)
        //{
        //    FilterParams fp = new FilterParams();
        //    fp.AddParam(tbcity_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    fp.AddParam(tbcity_ZH_CN.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
        //    return base.Delete(fp);
        //}

        public tbcity_ZH_CN GetEntity(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_ZH_CN.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbcity_ZH_CNs GetList(string countrycode, string citycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity_ZH_CN.Fields.COUNTRYCODE, countrycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbcity_ZH_CN.Fields.CITYCODE, citycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

