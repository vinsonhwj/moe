﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOXODETAIL]
    /// </summary>
    public partial class DAPEOXODETAIL : DALDependency<tbPEOXODETAIL, tbPEOXODETAILs>
    {
        public DAPEOXODETAIL(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOXODETAIL.DBTableName;
        }

        public bool Exists(string companycode, string xonum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOXODETAIL entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOXODETAIL updateEntity, string companycode, string xonum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string xonum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOXODETAIL GetEntity(string companycode, string xonum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOXODETAILs GetList(string companycode, string xonum, string segnum, string seqnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOXODETAILs GetList(string companycode, string xonum, string segnum, string seqnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXODETAIL.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.XONUM, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXODETAIL.Fields.SEQNUM, seqnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

