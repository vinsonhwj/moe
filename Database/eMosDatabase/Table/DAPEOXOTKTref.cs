﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.DAL.eMosDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOXOTKTref]
    /// </summary>
    public partial class DAPEOXOTKTref : DALDependency<tbPEOXOTKTref, tbPEOXOTKTrefs>
    {
        public DAPEOXOTKTref(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOXOTKTref.DBTableName;
        }

        public bool Exists(string companyCode, string ticket, string tktSeq, string bkgRef, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOXOTKTref entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOXOTKTref updateEntity, string companyCode, string ticket, string tktSeq, string bkgRef, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companyCode, string ticket, string tktSeq, string bkgRef, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOXOTKTref GetEntity(string companyCode, string ticket, string tktSeq, string bkgRef, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOXOTKTrefs GetList(string companyCode, string ticket, string tktSeq, string bkgRef, string xonum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOXOTKTrefs GetList(string companyCode, string ticket, string tktSeq, string bkgRef, string xonum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOXOTKTref.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.Ticket, ticket, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.TktSeq, tktSeq, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.BkgRef, bkgRef, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOXOTKTref.Fields.XONum, xonum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

