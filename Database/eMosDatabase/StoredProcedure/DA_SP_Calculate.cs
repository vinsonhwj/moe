﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using hwj.DBUtility.TableMapping;
using hwj.DBUtility.Interface;
namespace Westminster.MOE.DAL.eMosDB.StoredProcedure
{
    public class DA_SP_Calculate
    {
        public IConnection Conn { get; set; }
        public DA_SP_Calculate(IConnection conn)
        {
            Conn = conn;
        }
        public void CalculatePeomstrSummary(string companyCode, string bkgRef, string staffCode)
        {
            //SqlEntity se = new SqlEntity();
            //se.CommandText = "exec sp_CalPeomstrSummary @companyCode,@bkg,@Login";
            //List<IDbDataParameter> sqlParamList = new List<IDbDataParameter>();
            //sqlParamList.Add(new SqlParameter(@"companycode", companyCode));
            //sqlParamList.Add(new SqlParameter(@"bkg", bkgRef));
            //sqlParamList.Add(new SqlParameter(@"Login", staffCode));
            //se.Parameters = sqlParamList;
            //return se;

            IDbCommand cmd = new SqlCommand("sp_CalPeomstrSummary");
            cmd.Transaction = Conn.InnerTransaction;
            cmd.Connection = Conn.InnerConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@companycode", companyCode));
            cmd.Parameters.Add(new SqlParameter("@bkg", bkgRef));
            cmd.Parameters.Add(new SqlParameter("@Login", staffCode));
            if (cmd.Connection.State == ConnectionState.Closed)
            {
                cmd.Connection.Open();
            }
            cmd.ExecuteScalar();
            return;

        }
    }
}
