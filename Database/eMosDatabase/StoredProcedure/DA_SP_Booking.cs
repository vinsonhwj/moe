﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using hwj.DBUtility.MSSQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.StoredProcedure
{
    public class DA_SP_Booking
    {
        public enum ModuleType
        {
            VCHNUM,
            BKGREF,
            CRNNUM,
        }
        private IConnection conn { get; set; }
        public DA_SP_Booking(IConnection conn)
        {
            this.conn = conn;
        }
        //public string GetNewBkgRefKey(string branchCode, string companyCode, string moduleCode, int numOfKey)
        //{
        //    using (SqlConnection conn = new SqlConnection(ConnectionString))
        //    {

        //        SqlCommand cmd = new SqlCommand("SP_CREATEBKGKEY");
        //        cmd.Connection = conn;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@IN_MODULECODE", moduleCode));
        //        cmd.Parameters.Add(new SqlParameter("@IN_BRANCHCODE", branchCode));
        //        cmd.Parameters.Add(new SqlParameter("@IN_NUMOFKEY", numOfKey));
        //        cmd.Parameters.Add(new SqlParameter("@IN_COMPANYCODE", companyCode));

        //        SqlParameter sp = new SqlParameter("@OUT_PEONYREF", SqlDbType.VarChar, 10);
        //        sp.Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add(sp);
        //        if (conn.State == ConnectionState.Closed)
        //        {
        //            conn.Open();
        //        }
        //        cmd.ExecuteScalar();
        //        conn.Close();
        //        return sp.Value.ToString();
        //    }
        //}
        public string GetNum(string companyCode, string branchCode, ModuleType module, int numOfKey)
        {
            IDbCommand cmd = new SqlCommand("SP_CREATEBKGKEY");
            cmd.Transaction = conn.InnerTransaction;
            cmd.Connection = conn.InnerConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@IN_MODULECODE", module.ToString()));
            cmd.Parameters.Add(new SqlParameter("@IN_BRANCHCODE", branchCode));
            cmd.Parameters.Add(new SqlParameter("@IN_NUMOFKEY", numOfKey));
            cmd.Parameters.Add(new SqlParameter("@IN_COMPANYCODE", companyCode));

            SqlParameter sp = new SqlParameter("@OUT_PEONYREF", SqlDbType.VarChar, 10);
            sp.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(sp);
            if (cmd.Connection.State == ConnectionState.Closed)
            {
                cmd.Connection.Open();
            }
            cmd.ExecuteScalar();
            return sp.Value.ToString();

        }
    }
}
