﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using System.Data;
namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlXoHotel : SelectDALDependency<sqlXoHotel, sqlXoHotels>
    {
        public DASqlXoHotel(string connectionString)
            : base(connectionString)
        {
            CommandText = sqlXoHotel.CommandText;
        }

        public sqlXoHotels GetList(string companyCode, string bkgRef, string segnum)
        {
            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companyCode", companyCode));
            paramList.Add(new SqlParameter("@bkgRef", bkgRef));
            paramList.Add(new SqlParameter("@SegNum", segnum));

            return base.GetList(CommandText, paramList, 300);
        }
    }
}
