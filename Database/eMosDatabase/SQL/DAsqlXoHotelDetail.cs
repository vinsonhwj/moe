﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAsqlXoHotelDetail : SelectDALDependency<sqlXoHotelDetail, sqlXoHotelDetails>
    {
        public DAsqlXoHotelDetail(string connectionString)
            : base(connectionString)
        {
            CommandText = sqlXoHotelDetail.CommandText;
        }
        public sqlXoHotelDetails GetList(string BkgRef, string HotelSegNum)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@bkgRef", BkgRef));
            param.Add(new SqlParameter("@hotelSegNumWai", HotelSegNum));

            return base.GetList(CommandText, param, 300);
        }

    }
}
