﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAsqlXoPeoAirDetail : SelectDALDependency<sqlXoPeoAirDetail ,sqlXoPeoAirDetails>
    {
        public DAsqlXoPeoAirDetail(string connectionString)
            : base(connectionString)
        {
            CommandText = sqlXoPeoAirDetail.CommandText;
        }
        public sqlXoPeoAirDetails GetList(string bkgRef, string paxsegnumList,string airsegnumList)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BkgRef", bkgRef));
            param.Add(new SqlParameter("@PaxSegNumList", paxsegnumList));
            param.Add(new SqlParameter("@AirSegNumList", airsegnumList));

            return this.GetList(CommandText, param, 300);
        }
            
    }
}
