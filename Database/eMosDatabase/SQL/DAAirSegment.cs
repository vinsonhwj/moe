﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAAirSegment : BaseSqlDAL<tbAirSegment, tbAirSegments>
    {
        public DAAirSegment(string connectionString)
            : base(connectionString)
        {
            CommandText = tbAirSegment.CommandText;
        }
        public tbAirSegments GetList(string bkgRef, string segnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@bkgRef", bkgRef));
            param.Add(new SqlParameter("@SegNum", segnumList));

            return base.GetList(CommandText, param, 300);
        }
    }
}
