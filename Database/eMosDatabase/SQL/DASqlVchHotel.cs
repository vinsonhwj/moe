﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    /// <summary>
    /// DataAccess [SP:sqlVchHotel]
    /// </summary>
    public partial class DAsqlVchHotel : SelectDALDependency<sqlVchHotel, sqlVchHotels>
    {
        public DAsqlVchHotel(string connectionString)
            : this(new DbConnection(connectionString))
        {

        }
        public DAsqlVchHotel(IConnection conn)
            : base(conn)
        {

        }

        public sqlVchHotels GetList(string BkgRef, string Companycode, string GSTTAXRate, string SegNum, string SeqType)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@BkgRef", BkgRef));
            param.Add(new SqlParameter("@Companycode", Companycode));
            param.Add(new SqlParameter("@GSTTAXRate", GSTTAXRate));
            param.Add(new SqlParameter("@SegNum", SegNum));
            param.Add(new SqlParameter("@SeqType", SeqType));
            return base.GetList(CommandText, param);
        }
    }
}

