﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public partial class DAsqlVchHotelDetail : SelectDALDependency<sqlVchHotelDetail, sqlVchHotelDetails>
    {
        public DAsqlVchHotelDetail(string connectionString)
            : base(connectionString)
        {
            //CommandText = sqlVchHotelDetail.CommandText;
        }

        public sqlVchHotelDetails GetSqlHotelDetails(string companyCode, string bkgRef, string segNum)
        {
            string sql =
                @"select CASE 
		                WHEN QTY > 2 
			                THEN 2 
		                WHEN QTY <= 2 
			                THEN QTY 
		                END AS QtyOfHotel, 
		                SEGNUM,
		                SEQNUM,
		                QTY,
		                ISNULL(ARRDATE,'') as ARRDATE,
		                ISNULL(DEPARTDATE,'') as DEPARTDATE,
		                isnull(RMNTS,0) as rmnts, 
		                ITEMNAME,
		                RATENAME,
		                isnull(CURR,'') as COSTCURR  ,
		                isnull(AMT,0)  as COSTAMT ,
		                isnull(TAXCURR,'') as TAXCURR,
		                isnull(TAXAMT,0) as TAXamt,
		                isnull(TOTALAMT,0) as TOTALAMT, 
		                IsNull(ItemType, '') As ItemType 
	                from 	peohtldetail 
	                where 	SEQTYPE='COST' 
		                AND bkgref = @bkgref 
		                and SEGNUM = @segnum
		                and companycode = @companycode 
	                order by seqnum";
            

            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));
            paramList.Add(new SqlParameter("@segnum",segNum));


            return this.GetList(sql, paramList);
            }

        }
        
}

