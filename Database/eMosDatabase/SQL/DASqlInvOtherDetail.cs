﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvOtherDetail : SelectDALDependency<sqlInvOtherDetail, sqlInvOtherDetails>
    {
        public DASqlInvOtherDetail(string connectionString)
            : base(connectionString)
        {
        }
        public DASqlInvOtherDetail(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvOtherDetails GetSqlOtherDetails(bool isAllowGroupInsurance, string companyCode, string bkgRef, List<string> otherSegNumList, List<string> paxSegNumList, string gstTax)
        {
            string sql = string.Empty;

            string sql_other = "N''";
            if (otherSegNumList != null)
            {
                foreach (string n in otherSegNumList)
                {
                    sql_other = string.Format("{0},N'{1}'", sql_other, n);
                }
            }

            string sql_pax = "N''";
            if (paxSegNumList != null)
            {
                foreach (string n in paxSegNumList)
                {
                    sql_pax = string.Format("{0},N'{1}'", sql_pax, n);
                }
            }

            if (isAllowGroupInsurance)
            {
                #region SQL

                sql = @" SELECT *
                         FROM   ( SELECT TOP 100 PERCENT
                                            {0} AS GSTTAX ,
                                            a.agetype ,
                                            a.COMPANYCODE ,
                                            a.BKGREF ,
                                            a.segnum ,
                                            a.curr ,
                                            SUM(ISNULL(amt, 0)) AS amt ,
                                            a.taxcurr ,
                                            SUM(ISNULL(a.taxamt, 0)) AS taxamt ,
                                            ISNULL(MAX(c.gsttax), '') AS gsttaxshow ,
                                            CASE WHEN b.num > a.qty THEN a.qty
                                                 ELSE b.num
                                            END AS QTY ,
                                            c.segType
                                  FROM      peootherdetail a ,
                                            ( SELECT    paxother AS agetype ,
                                                        COUNT(paxother) AS num
                                              FROM      peopax
                                              WHERE     bkgref =@bkgref
                                                        AND SEGNUM IN ({1})
                                              GROUP BY  paxother
                                            ) b ,
                                            peoother AS c
                                  WHERE     a.agetype = b.agetype
                                            AND a.bkgref =@bkgref
                                            AND a.companycode = @companycode
                                            AND a.SEGNUM IN ({2})
                                            AND a.seqtype = 'SELL'
                                            AND c.bkgref = a.bkgref
                                            AND c.segnum = a.segnum
                                            AND a.companycode = c.companycode
                                  GROUP BY  a.segnum ,
                                            a.agetype ,
                                            a.COMPANYCODE ,
                                            a.BKGREF ,
                                            a.curr ,
                                            a.qty ,
                                            a.taxcurr ,
                                            b.num ,
                                            c.segType
                                ) AS detail
                         UNION
                         SELECT *
                         FROM   ( SELECT    0.9300 AS GSTTAX ,
                                            paxother AS agetype ,
                                            @companycode AS COMPANYCODE ,
                                            @bkgref AS BKGREF ,
                                            '' AS segnum ,
                                            '' AS curr ,
                                            0 AS amt ,
                                            '' AS taxcurr ,
                                            0 AS taxamt ,
                                            'N' AS gsttaxshow ,
                                            COUNT(paxother) AS num ,
                                            'INS' AS segType
                                  FROM      peopax
                                  WHERE     bkgref =@bkgref
                                            AND companycode = @companycode
                                            AND SEGNUM IN ({1})
                                  GROUP BY  paxother
                                ) AS otherIns
                         WHERE  SEGNUM IN ({2})
                         ORDER BY segnum ,
                                agetype ,
                                COMPANYCODE ,
                                BKGREF ,
                                curr ,
                                taxcurr ,
                                qty ";
                #endregion
            }
            else
            {
                #region SQL
                sql = @"SELECT  {0} AS GSTTAX ,
                                a.agetype ,
                                a.COMPANYCODE ,
                                a.BKGREF ,
                                a.segnum ,
                                a.curr ,
                                SUM(ISNULL(amt, 0)) AS amt ,
                                a.taxcurr ,
                                SUM(ISNULL(a.taxamt, 0)) AS taxamt ,
                                ISNULL(MAX(c.gsttax), '') AS gsttaxshow ,
                                CASE WHEN b.num > a.qty THEN a.qty
                                     ELSE b.num
                                END AS QTY ,
                                c.segType
                        FROM    peootherdetail a ,
                                ( SELECT    paxother AS agetype ,
                                            COUNT(paxother) AS num
                                  FROM      peopax
                                  WHERE     bkgref =@bkgref
                                            AND SEGNUM IN ({1})
                                  GROUP BY  paxother
                                ) b ,
                                peoother AS c
                        WHERE   a.agetype = b.agetype
                                AND a.bkgref = @bkgref
                                AND a.companycode = @companycode
                                AND a.SEGNUM IN ({2})
                                AND a.seqtype = 'SELL'
                                AND c.bkgref = a.bkgref
                                AND c.segnum = a.segnum
                                AND a.companycode = c.companycode
                        GROUP BY a.segnum ,
                                a.agetype ,
                                a.COMPANYCODE ,
                                a.BKGREF ,
                                a.curr ,
                                a.qty ,
                                a.taxcurr ,
                                b.num ,
                                c.segType
                        ORDER BY a.segnum ,
                                a.agetype ,
                                a.COMPANYCODE ,
                                a.BKGREF ,
                                a.curr ,
                                a.taxcurr ,
                                b.num ";
                #endregion
            }

            sql = string.Format(sql, gstTax, sql_pax, sql_other);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            return base.GetList(sql, paramList);
        }
    }
}
