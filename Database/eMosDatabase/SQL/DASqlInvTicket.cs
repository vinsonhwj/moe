﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvTicket : SelectDALDependency<sqlInvTicket, sqlInvTickets>
    {
        public DASqlInvTicket(string connectionString)
            : base(connectionString)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public DASqlInvTicket(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvTickets GetSqlInvTicket(string companyCode, string bkgRef)
        {
            string sql = @"SELECT  CONVERT(INT, tktseq) AS seq ,
                                    ISNULL(a.paidFare, 0) AS paidFare ,
                                    a.tktseq ,
                                    a.routing ,
                                    a.ticket ,
                                    ISNULL(a.totaltax, 0) AS totaltax ,
                                    a.fullcurr ,
                                    ISNULL(a.fullfare, 0) AS fullfare ,
                                    a.AirCode AS AirCode ,
                                    '' AS TicketType ,
                                    FormPay ,
                                    ISNULL(iss_in_exchange, '') AS iss_in_exchange
                            FROM    peotkt a
                            WHERE   a.bkgref = @bkgref
                                    AND a.companyCode =@companyCode
                                    AND a.voidOn IS NULL
                            UNION
                            SELECT  CONVERT(INT, tktseq) AS seq ,
                                    ISNULL(a.paidFare, 0) AS paidFare ,
                                    a.tktseq ,
                                    a.routing ,
                                    a.ticket ,
                                    ISNULL(a.totaltax, 0) AS totaltax ,
                                    a.fullcurr ,
                                    ISNULL(a.fullfare, 0) AS fullfare ,
                                    a.AirCode AS AirCode ,
                                    '*' AS TicketType ,
                                    FormPay ,
                                    ISNULL(iss_in_exchange, '') AS iss_in_exchange
                            FROM    peomistkt a
                            WHERE   a.bkgref = @bkgref
                                    AND a.companyCode =@companyCode
                                    AND a.voidOn IS NULL";

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companyCode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            return base.GetList(sql, paramList);
        }
    }
}
