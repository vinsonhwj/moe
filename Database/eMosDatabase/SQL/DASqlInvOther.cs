﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvOther : SelectDALDependency<sqlInvOther, sqlInvOthers>
    {
        public DASqlInvOther(string connectionString)
            : base(connectionString)
        {
        }
        public DASqlInvOther(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvOthers GetSqlOthers(bool isAllowGroupInsurance, string companyCode, string bkgRef, string otherInsSegnum, string otherInsCurrency, string userCode, List<string> otherSegNumList)
        {
            string sql = string.Empty;
            int o = 0;
            int.TryParse(otherInsSegnum, out o);

            string sql_other = "N''";
            if (otherSegNumList != null)
            {
                foreach (string n in otherSegNumList)
                {
                    sql_other = string.Format("{0},N'{1}'", sql_other, n);
                }
            }

            if (isAllowGroupInsurance)
            {

                #region SQL

                sql = @"SELECT  *
                            FROM    ( SELECT    CONVERT(INT, segnum) AS seq ,
                                                BKGREF ,
                                                COMPANYCODE ,
                                                SEGNUM ,
                                                SEGTYPE ,
                                                OTHERDATE ,
                                                DESCRIPT1 ,
                                                DESCRIPT2 ,
                                                DESCRIPT3 ,
                                                DESCRIPT4 ,
                                                CITYCODE ,
                                                COUNTRYCODE ,
                                                RMK1 ,
                                                RMK2 ,
                                                INVRMK1 ,
                                                INVRMK2 ,
                                                VCHRMK1 ,
                                                VCHRMK2 ,
                                                SELLCURR ,
                                                TOTALSELL ,
                                                TOTALSTAX ,
                                                COSTCURR ,
                                                TOTALCOST ,
                                                TOTALCTAX ,
                                                PCODE ,
                                                VOIDON ,
                                                VOIDBY ,
                                                SOURCESYSTEM ,
                                                suppliercode ,
                                                CREATEON ,
                                                CREATEBY ,
                                                UPDATEON ,
                                                UPDATEBY ,
                                                GSTTAX ,
                                                showDetailFee ,
                                                masterDesc ,
                                                IsRecalable
                                      FROM      peoother
                                      UNION ALL
                                      SELECT    {0} AS seq ,
                                                @BkgRef,
                                                @CompanyCode ,
                                                @OtherInsSegnum AS SEGNUM ,
                                                'INS' ,
                                                GETDATE() ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                '' ,
                                                @OtherInsCurrency ,
                                                0 ,
                                                0 ,
                                                @OtherInsCurrency ,
                                                0 ,
                                                0 ,
                                                '' ,
                                                NULL ,
                                                NULL ,
                                                'EMOS' ,
                                                '' ,
                                                GETDATE() ,
                                                @UserCode ,
                                                NULL ,
                                                NULL ,
                                                'N' ,
                                                '' ,
                                                (SELECT TOP 1 preferencevalue FROM dbo.Com_Preference WHERE COMPANYCODE =@CompanyCode and PREFERENCEID ='INV_GINS') ,
                                                NULL
                                    ) AS other
                            WHERE   bkgref =@BkgRef
                                    AND companycode =@CompanyCode
                                    AND voidon IS NULL
                                    AND SEGNUM IN ({1})
                            ORDER BY OTHERDATE ASC ,
                                    SEGNUM ASC";
                #endregion

                sql = string.Format(sql, o, sql_other);

                List<IDbDataParameter> paramList = new List<IDbDataParameter>();
                paramList.Add(new SqlParameter("@CompanyCode", companyCode));
                paramList.Add(new SqlParameter("@BkgRef", bkgRef));
                paramList.Add(new SqlParameter("@OtherInsSegnum", otherInsSegnum));
                paramList.Add(new SqlParameter("@OtherInsCurrency", otherInsCurrency));
                paramList.Add(new SqlParameter("@UserCode", userCode));

                return base.GetList(sql, paramList);

            }
            else
            {
                #region SQL
                sql = @"SELECT  CONVERT(INT, segnum) AS seq ,
                                BKGREF ,
                                COMPANYCODE ,
                                SEGNUM ,
                                SEGTYPE ,
                                OTHERDATE ,
                                DESCRIPT1 ,
                                DESCRIPT2 ,
                                DESCRIPT3 ,
                                DESCRIPT4 ,
                                CITYCODE ,
                                COUNTRYCODE ,
                                RMK1 ,
                                RMK2 ,
                                INVRMK1 ,
                                INVRMK2 ,
                                VCHRMK1 ,
                                VCHRMK2 ,
                                SELLCURR ,
                                TOTALSELL ,
                                TOTALSTAX ,
                                COSTCURR ,
                                TOTALCOST ,
                                TOTALCTAX ,
                                PCODE ,
                                VOIDON ,
                                VOIDBY ,
                                SOURCESYSTEM ,
                                suppliercode ,
                                CREATEON ,
                                CREATEBY ,
                                UPDATEON ,
                                UPDATEBY ,
                                GSTTAX ,
                                showDetailFee ,
                                masterDesc ,
                                IsRecalable
                FROM    peoother
                WHERE   bkgref =@BkgRef
                        AND companycode =@CompanyCode
                        AND voidon IS NULL
                        AND SEGNUM IN ({0})
                ORDER BY OTHERDATE ASC ,
                        SEGNUM ASC";
                #endregion

                sql = string.Format(sql, sql_other);

                List<IDbDataParameter> paramList = new List<IDbDataParameter>();
                paramList.Add(new SqlParameter("@CompanyCode", companyCode));
                paramList.Add(new SqlParameter("@BkgRef", bkgRef));

                return base.GetList(sql, paramList);
            }
        }
    }
}
