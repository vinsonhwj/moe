﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAsqlXoOtherDetail : SelectDALDependency<sqlXoOtherDetail, sqlXoOtherDetails>
    {
        public DAsqlXoOtherDetail(string connectionString)
            : base(connectionString)
        {
            CommandText = sqlXoOtherDetail.CommandText;
        }
        public sqlXoOtherDetails GetList(string BkgRef, string PaxNumList, string OtherSegNumList)
        {
            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@BkgRef", BkgRef));
            paramList.Add(new SqlParameter("@PaxNumList", PaxNumList));
            paramList.Add(new SqlParameter("@OtherSegNumList", OtherSegNumList));

            return base.GetList(CommandText, paramList, 300);
        }
    }
}
