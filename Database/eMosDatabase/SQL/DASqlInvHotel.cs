﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvHotel : SelectDALDependency<sqlInvHotel, sqlInvHotels>
    {
        public DASqlInvHotel(string connectionString)
            : base(connectionString)
        {
            // CommandText = sqlHotel.CommandText;
        }
        public DASqlInvHotel(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvHotels GetSqlHotels(string companyCode, string bkgRef, List<string> htlSegNumList, string dbTableSuffix)
        {
            string sql =
                @"SELECT  CONVERT(INT, segnum) AS seq ,
                        a.SEGNUM ,
                        a.COUNTRYCODE ,
                        a.CITYCODE ,
                        ISNULL(a.ARRDATE, '') AS ARRDATE ,
                        a.ARRTIME ,
                        a.eta ,
                        ISNULL(a.departdate, '') AS departdate ,
                        a.departtime ,
                        a.etd ,
                        ISNULL(a.ReasonCode, '') AS ReasonCode ,
                        a.rmk1 ,
                        a.rmk2 ,
                        a.spcl_request ,
                        a.HTLREFERENCE ,
                        a.HTLCFMBY ,
                        a.hotelcode ,
                        ISNULL(b.hotelname, ISNULL(a.hotelname, '')) AS hotelname ,
                        c.countryname AS newcountryname ,
                        d.cityname AS newcityname
                FROM    peohtl a
                        LEFT JOIN peohotel b ON a.hotelcode = b.hotelcode
                        JOIN country{1} c ON a.countrycode = c.countrycode
                        JOIN city{1} d ON a.citycode = d.citycode
                WHERE   a.companycode =@companycode
                        AND bkgref =@bkgref
                        AND SEGNUM IN ({0})
                ORDER BY a.departdate ASC ,
                        a.SEGNUM ASC 
                ";


            string sql_htl = "N''";
            if (htlSegNumList != null)
            {
                foreach (string n in htlSegNumList)
                {
                    sql_htl = string.Format("{0},N'{1}'", sql_htl, n);
                }
            }

            sql = string.Format(sql, sql_htl, dbTableSuffix);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            return base.GetList(sql, paramList);
        }
    }
}
