﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAHotelSegment : BaseSqlDAL<tbHotelSegment, tbHotelSegments>
    {
        public DAHotelSegment(string connectionString)
            : base(connectionString)
        {
            CommandText = tbHotelSegment.CommandText;

        }
        public tbHotelSegments GetList(string companyCode, string bkgRef, string segnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@companyCode", companyCode));
            param.Add(new SqlParameter("@bkgRef", bkgRef));
            param.Add(new SqlParameter("@SegNum", segnumList));

            return base.GetList(CommandText, param, 300);
        }
    }
}
