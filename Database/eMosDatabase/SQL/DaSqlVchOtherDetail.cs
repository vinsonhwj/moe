﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    /// <summary>
    /// DataAccess [SQL:sqlVchOtherDetail]
    /// </summary>
    public partial class DAsqlVchOtherDetail : SelectDALDependency<sqlVchOtherDetail, sqlVchOtherDetails>
    {
        public DAsqlVchOtherDetail(string connectionString)
            : base(connectionString)
        {
            //CommandText = sqlVchOtherDetail.CommandText;
        }

        public sqlVchOtherDetails GetSqlOtherDetails(string companyCode, string bkgRef, List<string> otherSegNumList, List<string> paxSegNumList)
        {
            string othersegnums = "N''";
            if (otherSegNumList != null)
            {
                foreach (string n in otherSegNumList)
                {
                    othersegnums = string.Format("{0},N'{1}'", othersegnums, n);
                }
            }

            string paxsegnums = "N''";
            if (paxSegNumList != null)
            {
                foreach (string n in paxSegNumList)
                {
                    paxsegnums = string.Format("{0},N'{1}'", paxsegnums, n);
                }
            }
            string sql =
                @"	select 	a.agetype,
		                    a.COMPANYCODE,
		                    a.BKGREF,
		                    a.segnum,
		                    a.curr,
		                    sum(isnull(amt,0.00)) as amt,
		                    a.taxcurr, 
		                    sum(isnull(a.taxamt,0.00)) as taxamt ,
		                    b.num as QTY 
                    from  	peootherdetail a, 
	                ( select paxother as agetype,count(paxother) as num 
                    from 	peopax  
                    where 	bkgref=@bkgRef
                            companycode = @companycode
                    and 	SEGNUM in  {0}
                    group 	by paxother) b 	
                    where 	a.agetype=b.agetype  
                    and 	a.bkgref=@bkgRef 
                    and 	a.SEGNUM in  {1}
                    and 	a.seqtype='COST' 
                    group 	by a.segnum,
	                    a.agetype,
	                    a.COMPANYCODE,
	                    a.BKGREF,
	                    a.curr,
	                    a.taxcurr,
	                    b.num 	
                    order 	by a.segnum,
	                    a.agetype,
	                    a.COMPANYCODE,
	                    a.BKGREF,
	                    a.curr,
	                    a.taxcurr,
	                    b.num ";
            sql = string.Format(sql, paxsegnums, othersegnums);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            return base.GetList(sql, paramList);
        }

    }
}

