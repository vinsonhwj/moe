﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    /// <summary>
    /// DataAccess [SQL:sqlVchTitle]
    /// </summary>
    public partial class DAsqlVchTitle : SelectDALDependency<sqlVchTitle, sqlVchTitles>
    {
        public DAsqlVchTitle(string connectionString)
            : base(connectionString)
        {
            //CommandText = sqlVchTitle.CommandText;
        }
        public sqlVchTitles GetSqlTitles(string companyCode,string bkgRef,List<string> hotelSegNumList) 
        {
            string hotelsegnums = "N''";
            if (hotelSegNumList != null)
            {
                foreach (string n in hotelSegNumList)
                {
                    hotelsegnums = string.Format("{0},N'{1}'",hotelsegnums,n);
                }
            }
            string sql =
                @"	select 	a.HOTELCODE,
		                    isnull(b.HOTELNAME,isnull(a.hotelname,'')) as HOTELNAME,
		                    b.ADDR1,
		                    b.ADDR2,
		                    b.ADDR3,
		                    b.ADDR4,
		                    b.HTLDESC,
		                    b.GeneralPhone,
		                    b.GeneralFax,
		                    isnull(a.suppcode,'') as suppcode,
		                    b.countrycode,
		                    HotelName_Zhcn, 
		                    Addr1_Zhcn, 
		                    Addr2_Zhcn, 
		                    Addr3_Zhcn, 
		                    Addr4_Zhcn,
		                    HotelName_ZhTW, 
		                    Addr1_ZhTW, 
		                    Addr2_ZhTW, 
		                    Addr3_ZhTW, 
		                    Addr4_ZhTW 
	                    from 	peohtl a 
	                    left 	join peohotel b 
	                    on 	a.hotelcode=b.hotelcode 
	                    where  	a.bkgref=@bkgref 
	                    and 	a.SEGNUM 
	                    in ({0})
	                    and 	companycode=@companycode";

            sql = string.Format(sql, hotelsegnums);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            return base.GetList(sql, paramList);
        }

    }
}


