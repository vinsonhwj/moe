﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvAirDetail : SelectDALDependency<sqlInvAirDetail, sqlInvAirDetails>
    {
        public DASqlInvAirDetail(string connectionString)
            : base(connectionString)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public DASqlInvAirDetail(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvAirDetails GetSqlAirDetails(string companyCode, string bkgRef, List<string> airSegNumList, List<string> paxSegNumList)
        {
            string sql =
                @"SELECT  ( SELECT TOP 1
                                    ISNULL(showdetailfee, 'N') AS showdetailfee
                          FROM      peoair
                          WHERE     bkgref = @BkgRef
                                    AND SEGNUM IN ({0} )
                          ORDER BY  showdetailfee DESC
                        ) AS showdetailfee ,
                        a.agetype ,
                        a.COMPANYCODE ,
                        a.BKGREF ,
                        MAX(a.segnum) AS segnum ,
                        a.curr ,
                        SUM(ISNULL(amtfee, 0)) AS amtfee ,
                        SUM(ISNULL(amt, 0)) AS amt ,
                        a.taxcurr ,
                        SUM(ISNULL(a.taxamt, 0)) AS taxamt ,
                        b.num AS User_QTY ,
                        a.qty AS AIR_QTY ,
                        CASE WHEN b.num > a.qty THEN a.qty
                             ELSE b.num
                        END AS QTY ,
                        CASE WHEN ( SELECT TOP 1
                                            ISNULL(showdetailfee, 'N') AS showdetailfee
                                    FROM    peoair
                                    WHERE   bkgref = @BkgRef
                                            AND SEGNUM IN ({0})
                                    ORDER BY showdetailfee DESC
                                  ) = 'Y' THEN SUM(ISNULL(amt, 0))
                             ELSE SUM(ISNULL(amt, 0)) + SUM(ISNULL(amtfee, 0))
                        END AS showamt
                FROM    peoairdetail a ,
                        ( SELECT    paxair AS agetype ,
                                    COUNT(paxair) AS num
                          FROM      peopax
                          WHERE     bkgref = @BkgRef
                                    AND companycode = @CompanyCode
                                    AND SEGNUM IN ({1})
                          GROUP BY  paxair
                        ) b
                WHERE   a.agetype = b.agetype
                        AND a.bkgref = @BkgRef
                        AND a.companycode = @CompanyCode
                        AND a.SEGNUM IN ( {0} )
                        AND a.seqtype = 'SELL'
                        AND a.companycode = @CompanyCode
                GROUP BY a.agetype ,
                        a.COMPANYCODE ,
                        a.BKGREF ,
                        a.curr ,
                        a.taxcurr ,
                        b.num ,
                        a.qty
                ORDER BY a.agetype ,
                        a.segnum ,
                        a.COMPANYCODE ,
                        a.BKGREF ,
                        a.curr ,
                        a.taxcurr ,
                        b.num ";


            string sql_air = "N''";
            if (airSegNumList != null)
            {
                foreach (string n in airSegNumList)
                {
                    sql_air = string.Format("{0},N'{1}'", sql_air, n);
                }
            }

            string sql_pax = "N''";
            if (paxSegNumList != null)
            {
                foreach (string n in paxSegNumList)
                {
                    sql_pax = string.Format("{0},N'{1}'", sql_pax, n);
                }
            }

            sql = string.Format(sql, sql_air, sql_pax);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@CompanyCode", companyCode));
            paramList.Add(new SqlParameter("@BkgRef", bkgRef));

            return base.GetList(sql, paramList);
        }
    }
}
