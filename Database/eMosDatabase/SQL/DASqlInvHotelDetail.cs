﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DASqlInvHotelDetail : SelectDALDependency<sqlInvHotelDetail, sqlInvHotelDetails>
    {
        public DASqlInvHotelDetail(string connectionString)
            : base(connectionString)
        {
            //  CommandText = sqlHotelDetail.CommandText;
        }
        public DASqlInvHotelDetail(IConnection conn)
            : base(conn)
        {
            //  TableName = tbCustomer.DBTableName;
        }
        public sqlInvHotelDetails GetSqlHotelDetails(string companyCode, string bkgRef, string segNum, string gstTAXRate)
        {
            string sql =
                @"SELECT  {0} AS GSTTAXRate ,
                            b.GSTTAX ,
                            a.coMPANYCODE ,
                            a.BKGREF ,
                            a.SEGNUM ,
                            a.SEQNUM ,
                            a.SEQTYPE ,
                            ISNULL(RMNTS, 0) AS RMNTS ,
                            a.ARRDATE ,
                            a.DEPARTDATE ,
                            ISNULL(QTY, 1) AS QTY ,
                            RATENAME ,
                            ITEMNAME AS ROOMTYPE ,
                            ISNULL(CURR, '') AS SELLCURR ,
                            ISNULL(AMT, 0) AS SELLAMT ,
                            ISNULL(TAXCURR, '') AS TAXCURR ,
                            ISNULL(a.TAXAMT, 0) AS TAXAMT ,
                            ISNULL(a.itemtype, '') AS itemtype
                    FROM    peohtldetail AS a ,
                            peohtl AS b
                    WHERE   b.companycode =@companycode
                            AND a.bkgref = @bkgref
                            AND a.SEQTYPE = 'SELL'
                            AND a.SEGNUM = @segnum
                            AND a.bkgref = b.bkgref
                            AND a.companycode = b.companycode
                            AND a.segnum = b.segnum
                    ORDER BY a.seqnum 
                    ";


            sql = string.Format(sql, gstTAXRate);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));
            paramList.Add(new SqlParameter("@segnum", segNum));

            return base.GetList(sql, paramList);
        }
    }
}
