﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    /// <summary>
    /// DataAccess [SQL:sqlVchPax]
    /// </summary>
    public class DAsqlVchPax : SelectDALDependency<sqlVchPax, sqlVchPaxs>
    {
        public DAsqlVchPax(string connectionString)
            : base(connectionString)
        {
            //CommandText = sqlVchPax.CommandText;
        }

        public sqlVchPaxs GetSqlPax(string bkgref, string companycode, List<string> segNumList)
        {
            string sql =
                @"select BKGREF,
                        COMPANYCODE,
                        SEGNUM,
                        PAXNUM,
                        PAXLNAME,
                        PAXFNAME,
                        PAXTITLE,
                        PAXTYPE,
                        ISNULL(PAXAIR,'') AS PAXAIR,
                        ISNULL(PAXHOTEL,'') AS PAXHOTEL,
                        ISNULL(PAXOTHER,'') AS PAXOTHER,
                        Isnull(paxage,'') as paxage 
                    from peopax (NOLOCK) 
                    where bkgref = @bkgref and SEGNUM 
                    in @segnum
                    order by segnum
                    ";
            //string sql = "select BKGREF,COMPANYCODE,SEGNUM,PAXNUM,PAXLNAME,PAXFNAME,PAXTITLE,PAXTYPE,ISNULL(PAXAIR,'') AS PAXAIR,ISNULL(PAXHOTEL,'') AS PAXHOTEL,ISNULL(PAXOTHER,'') AS PAXOTHER,Isnull(paxage,'') as paxage from peopax (NOLOCK) where bkgref=N'000001689' and SEGNUM in (N'00001',N'00002', '') order by segnum ";
            //sql.Replace("{0}", bkgref);
            //sql.Replace("{1}", segnum);
            //sql = string.Format(sql, bkgref, segnum);
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@segnum", segNumList));
            paramList.Add(new SqlParameter("@bkgref", bkgref));

            return this.GetList(sql, paramList);

            //FilterParams fp = new FilterParams();
            //fp.AddParam(sqlVchPax.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            //fp.AddParam(sqlVchPax.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            //fp.AddParam(sqlVchPax.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            //return base.GetEntity(fp);
        }
    }
}

