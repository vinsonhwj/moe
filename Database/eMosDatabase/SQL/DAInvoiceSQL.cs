﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DAL.eMosDB.SQL;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Lib;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAInvoiceSQL
    {
        DAPEOINV daPeoInv;
        public DAInvoiceSQL(IConnection conn)
        {
            daPeoInv = new DAPEOINV(conn);
        }
      
        public bool CheckAirSegmentTypeMatch(string companyCode, string bkgRef, List<string> SegNumList)
        {
            string sql = @" SELECT count(*) as count
                             FROM   ( SELECT    SegNum ,
                                                AgeType ,
                                                Qty
                                      FROM      peoairdetail
                                      WHERE     bkgref =@bkgref
                                                AND CompanyCode =@companycode
                                                AND SeqType = 'SELL'
                                                AND SegNum IN ({0})
                                    ) a ,
                                    ( SELECT    SegNum ,
                                                AgeType ,
                                                Qty
                                      FROM      peoairdetail
                                      WHERE     bkgref =@bkgref
                                                AND CompanyCode =@companycode
                                                AND SeqType = 'SELL'
                                                AND SegNum IN ({0})
                                    ) b
                             WHERE  ( a.SegNum <> b.SegNum
                                      AND a.AgeType = b.AgeType
                                      AND a.Qty <> b.Qty
                                    )
                                    OR ( ( SELECT   COUNT(*)
                                           FROM     ( SELECT    SegNum ,
                                                                AgeType ,
                                                                Qty
                                                      FROM      peoairdetail
                                                      WHERE     bkgref =@bkgref
                                                                AND CompanyCode  =@companycode
                                                                AND SeqType = 'SELL'
                                                                AND SegNum IN ({0})
                                                    ) c
                                           WHERE    c.SegNum = b.SegNum
                                                    AND c.AgeType = a.AgeType
                                         ) = 0 )";

            string sql_seg = "N''";
            if (SegNumList != null)
            {
                foreach (string n in SegNumList)
                {
                    sql_seg = string.Format("{0},N'{1}'", sql_seg, n);
                }
            }
            sql = string.Format(sql, sql_seg);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@companycode", companyCode));
            paramList.Add(new SqlParameter("@bkgref", bkgRef));

            int i = Convert.ToInt32(daPeoInv.ExecuteScalar(sql, paramList));
            if (i > 0)
            {
                return false;
            }
            return true;


        }

        public List<string> MultiCurr(string companyCode, string bkgRef, string seqType, List<string> airSegNumList, List<string> hotalSegNumList, List<string> otherSegNumList)
        {

            string sql = @"SELECT DISTINCT
                                    ISNULL(curr, '')curr
                            FROM    peoairdetail
                            WHERE   bkgref =@BkgRef
                                    AND segnum IN ({0})
                                    AND seqtype =@SeqType
                                    AND CompanyCode =@CompanyCode
                            UNION
                            SELECT DISTINCT
                                    ISNULL(curr, '')curr
                            FROM    peohtldetail
                            WHERE   bkgref =@BkgRef
                                    AND segnum IN ({1})
                                    AND seqtype =@SeqType
                                    AND CompanyCode =@CompanyCode
                            UNION
                            SELECT DISTINCT
                                    ISNULL(curr, '')curr
                            FROM    peootherdetail
                            WHERE   bkgref =@BkgRef
                                    AND segnum IN ({2})
                                    AND seqtype =@SeqType
                                    AND CompanyCode =@CompanyCode
                                    ";

            string sql_air = "N''";
            string sql_htl = "N''";
            string sql_oth = "N''";

            if (airSegNumList != null)
            {
                foreach (string n in airSegNumList)
                {
                    sql_air = string.Format("{0},N'{1}'", sql_air, n);
                }
            }
            if (hotalSegNumList != null)
            {
                foreach (string n in hotalSegNumList)
                {
                    sql_htl = string.Format("{0},N'{1}'", sql_htl, n);
                }
            }
            if (otherSegNumList != null)
            {
                foreach (string n in otherSegNumList)
                {
                    sql_oth = string.Format("{0},N'{1}'", sql_oth, n);
                }
            }
            sql = string.Format(sql, sql_air, sql_htl, sql_oth);

            List<IDbDataParameter> paramList = new List<IDbDataParameter>();
            paramList.Add(new SqlParameter("@CompanyCode", companyCode));
            paramList.Add(new SqlParameter("@BkgRef", bkgRef));
            paramList.Add(new SqlParameter("@SeqType", seqType));

            IDataReader reader = daPeoInv.ExecuteReader(sql, paramList);

            List<string> l = new List<string>();
            while (reader.Read())
            {
                if (reader["curr"] != null)
                {
                    l.Add(reader["curr"].ToString());
                }
            }
            return l;
        }

        public bool MatchAgeType(string companyCode, string bkgRef, List<string> paxSegNumList, List<string> segNumList, MOEEnums.BkgDetailType type, string seqType)
        {
            string sql_seg = "N''";
            if (segNumList != null)
            {
                foreach (string s in segNumList)
                {
                    sql_seg = string.Format("{0},N''", sql_seg, s);
                }
            }
            string sql_pax = "N''";
            if (paxSegNumList != null)
            {
                foreach (string s in paxSegNumList)
                {
                    sql_pax = string.Format("{0},N''", sql_pax, s);
                }
            }

            string strSQL = "";
            try
            {
                if (sql_seg != "N''")
                {
                    switch (type)
                    {
                        case MOEEnums.BkgDetailType.Air:

                            strSQL = "select paxair from peopax where COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and SEGNUM in ( " + sql_pax + " ) and paxair not in (select distinct agetype from peoairdetail where COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and SEGNUM in (" + sql_seg + ") and seqtype='" + seqType + "')";

                            break;
                        case MOEEnums.BkgDetailType.Other:

                            strSQL = "select paxother from peopax where COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and SEGNUM in ( " + sql_pax + " )  and paxother not in (select distinct agetype from peootherdetail where COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and SEGNUM in (" + sql_seg + ") and seqtype='" + seqType + "')";

                            break;
                        case MOEEnums.BkgDetailType.Ticket:

                            strSQL = "select paxair from peopax where  COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and SEGNUM in ( " + sql_pax + " ) and paxair not in (select distinct paxair from peotkt where  COMPANYCODE='" + companyCode + "' bkgref='" + bkgRef + "' and ticket in (" + sql_seg + ")  and voidon is null ";

                            break;
                    }
                }
                if (!string.IsNullOrEmpty(strSQL))
                {
                    IDataReader reader = daPeoInv.ExecuteReader(strSQL);
                    if (reader.Read())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

    }
}
