﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAAirSegment_AgeType : BaseSqlDAL<tbAirSegment_AgeType, tbAirSegments_AgeTypes>
    {
        public DAAirSegment_AgeType(string connectionString)
            : base(connectionString)
        {
            CommandText = tbAirSegment_AgeType.CommandText;
        }
        public tbAirSegments_AgeTypes GetList(string companycode, string bkgRef, string segnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@companyCode", companycode));
            param.Add(new SqlParameter("@bkgRef", bkgRef));
            param.Add(new SqlParameter("@SegNum", segnumList));

            return this.GetList(CommandText, param, 300);
        }
    }
}
