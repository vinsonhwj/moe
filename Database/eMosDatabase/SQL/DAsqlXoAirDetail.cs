﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAsqlXoAirDetail : SelectDALDependency<sqlXoAirDetail ,sqlXoAirDetails>
    {
        public DAsqlXoAirDetail(string connectionString)
            : base(connectionString)
        {
            CommandText = sqlXoAirDetail.CommandText;
        }
        public sqlXoAirDetails GetList(string bkgRef, string paxsegnumList,string airsegnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@BkgRef", bkgRef));
            param.Add(new SqlParameter("@PaxSegNumList", paxsegnumList));
            param.Add(new SqlParameter("@AirSegNumList", airsegnumList));

            return this.GetList(CommandText, param, 300);
        }
            
    }
}
