﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAMultiCurr : BaseSqlDAL<tbMultiCurr, tbMultiCurrs>
    {
        public DAMultiCurr(string connectionString)
            : base(connectionString)
        {
            CommandText = tbMultiCurr.CommandText;
        }
        public tbMultiCurrs GetList(string bkgRef, string airsegnumList, string hotelsegnumList, string othersegnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@bkgRef", bkgRef));
            param.Add(new SqlParameter("@AirSegNumList", airsegnumList));
            param.Add(new SqlParameter("@HotelSegNumList", hotelsegnumList));
            param.Add(new SqlParameter("@OhterSegNumList", othersegnumList));

            return base.GetList(CommandText, param, 300);
        }
    }
}
