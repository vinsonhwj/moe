﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using hwj.DBUtility.MSSQL;
using System.Data;

namespace Westminster.MOE.DAL.eMosDB.SQL
{
    public class DAOtherSegment : BaseSqlDAL<tbOtherSegment, tbOtherSegments>
    {
        public DAOtherSegment(string connectionString)
            : base(connectionString)
        {
            CommandText = tbOtherSegment.CommandText;
        }
        public tbOtherSegments GetList(string bkgRef, string segnumList)
        {
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@bkgRef", bkgRef));
            param.Add(new SqlParameter("@SegNum", segnumList));

            return base.GetList(CommandText, param, 300);
        }
    }
}
