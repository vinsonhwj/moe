﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DALEntity.IUR.Table;

namespace Westminster.MOE.DAL.IUR.Table
{
    /// <summary>
    /// DataAccess [Table:EXRATE]
    /// </summary>
    public partial class DAEXRATE : BaseDAL<tbEXRATE, tbEXRATEs>
    {
        public DAEXRATE(string connectionString)
            : base(connectionString)
        {
            TableName = tbEXRATE.DBTableName;
        }


        public bool Add(tbEXRATE entity)
        {
            return this.Add(entity);
        }

        public bool Update(tbEXRATE updateEntity)
        {
            return this.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return this.Delete();
        }

        public tbEXRATE GetEntity()
        {
            return this.GetEntity();
        }

        public tbEXRATEs GetList()
        {
            return this.GetList();
        }

        public tbEXRATEs GetList(int top)
        {
            return this.GetList(null, null, null, top);
        }

        public tbEXRATEPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbEXRATEPage page = new tbEXRATEPage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = this.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

