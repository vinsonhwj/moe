﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PeoHtlDetail]
    /// </summary>
    public partial class DAPeoHtlDetail_IUR : DALDependency<tbPeoHtlDetail_IUR, tbPeoHtlDetail_IURs>
    {
        public DAPeoHtlDetail_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPeoHtlDetail_IUR.DBTableName;
        }


        public bool Add(tbPeoHtlDetail_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPeoHtlDetail_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPeoHtlDetail_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPeoHtlDetail_IURs GetList()
        {
            return base.GetList();
        }

        public tbPeoHtlDetail_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

