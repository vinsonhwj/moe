﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOAIRDETAIL]
    /// </summary>
    public partial class DAPEOAIRDETAIL_IUR : DALDependency<tbPEOAIRDETAIL_IUR, tbPEOAIRDETAIL_IURs>
    {
        public DAPEOAIRDETAIL_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOAIRDETAIL_IUR.DBTableName;
        }


        public bool Add(tbPEOAIRDETAIL_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOAIRDETAIL_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOAIRDETAIL_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOAIRDETAIL_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOAIRDETAIL_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

        public tbPEOAIRDETAIL_IURs GetListByBKGREF(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIRDETAIL_IUR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIRDETAIL_IUR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
    }
}

