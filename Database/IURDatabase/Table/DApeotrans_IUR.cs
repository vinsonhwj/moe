﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;


namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:peotrans]
    /// </summary>
    public partial class DApeotrans_IUR : DALDependency<tbpeotrans, tbpeotranss>
    {
        public DApeotrans_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbpeotrans_IUR.DBTableName;
        }

        public bool Exists(string bkgref, string companycode, string paxnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbpeotrans entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbpeotrans updateEntity, string bkgref, string companycode, string paxnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string bkgref, string companycode, string paxnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbpeotrans GetEntity(string bkgref, string companycode, string paxnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbpeotranss GetList(string bkgref, string companycode, string paxnum, string segnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbpeotranss GetList(string bkgref, string companycode, string paxnum, string segnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbpeotrans_IUR.Fields.bkgref, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.companycode, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbpeotrans_IUR.Fields.segnum, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

