﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOTKT]
    /// </summary>
    public partial class DAPEOTKT_IUR : DALDependency<tbPEOTKT_IUR, tbPEOTKT_IURs>
    {
        public DAPEOTKT_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOTKT_IUR.DBTableName;
        }


        public bool Add(tbPEOTKT_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOTKT_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOTKT_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOTKT_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOTKT_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

        public tbPEOTKT_IURs GetTicketNums(string companyCode, string bkgref)
        {
            DisplayFields df = new DisplayFields();
            df.Add(tbPEOTKT_IUR.Fields.Ticket);

            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOTKT_IUR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOTKT_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);

        }
    }
}

