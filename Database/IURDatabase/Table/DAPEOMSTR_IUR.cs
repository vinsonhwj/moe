﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOMSTR]
    /// </summary>
    public partial class DAPEOMSTR_IUR : DALDependency<tbPEOMSTR_IUR, tbPEOMSTR_IURs>
    {
        public DAPEOMSTR_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOMSTR_IUR.DBTableName;
        }

        public bool Exists(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPEOMSTR_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOMSTR_IUR updateEntity, string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPEOMSTR_IUR GetEntity(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPEOMSTR_IURs GetList(string companycode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPEOMSTR_IURs GetList(string companycode, string bkgref, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }


        public string GetFirstVirginBkgref(string companyCode, DateTime from, DateTime to)
        {
            string sql = "SELECT top 1 BKGREF FROM dbo.PEOMSTR WHERE CompanyCode = @CompanyCode AND CreateOn >= @from AND CreateOn <= @to AND tranflag IS NULL AND BKGREF IN (SELECT BKGREF FROM dbo.PEOTKT GROUP BY BKGREF )";
            List<SqlParameter> ps = new List<SqlParameter>();
            ps.Add(new SqlParameter("@CompanyCode", companyCode));
            ps.Add(new SqlParameter("@from", from));
            ps.Add(new SqlParameter("@to", to));

            tbPEOMSTR_IUR tb = base.GetEntity(sql, ps);
            if (tb != null)
                return tb.BKGREF;
            else
                return string.Empty;
        }

        public tbPEOMSTR_IURs GetList(string companycode)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public bool UpdateFlag(string companycode, string bkgref, bool succeed)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOMSTR_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOMSTR_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);

            UpdateParam up = new UpdateParam();
            up.AddParam(tbPEOMSTR_IUR.Fields.tranflag, succeed ? "S" : "F");

            return base.Update(up, fp);
        }
    }
}

