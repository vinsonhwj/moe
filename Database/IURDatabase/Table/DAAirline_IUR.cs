﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:Airline]
    /// </summary>
    public partial class DAAirline_IUR : DALDependency<tbAirline_IUR, tbAirline_IURs>
    {
        public DAAirline_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbAirline_IUR.DBTableName;
        }


        public bool Add(tbAirline_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbAirline_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbAirline_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbAirline_IURs GetList()
        {
            return base.GetList();
        }

        public tbAirline_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

