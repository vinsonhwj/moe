﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOTKTDETAIL]
    /// </summary>
    public partial class DAPEOTKTDETAIL_IUR : DALDependency<tbPEOTKTDETAIL_IUR, tbPEOTKTDETAIL_IURs>
    {
        public DAPEOTKTDETAIL_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOTKTDETAIL_IUR.DBTableName;
        }


        public bool Add(tbPEOTKTDETAIL_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOTKTDETAIL_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOTKTDETAIL_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOTKTDETAIL_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOTKTDETAIL_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

