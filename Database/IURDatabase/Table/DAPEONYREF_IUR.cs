﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEONYREF]
    /// </summary>
    public partial class DAPEONYREF_IUR : DALDependency<tbPEONYREF_IUR, tbPEONYREF_IURs>
    {
        public DAPEONYREF_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEONYREF_IUR.DBTableName;
        }


        public bool Add(tbPEONYREF_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEONYREF_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEONYREF_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEONYREF_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEONYREF_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

