﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOAIR]
    /// </summary>
    public partial class DAPEOAIR_IUR : DALDependency<tbPEOAIR_IUR, tbPEOAIR_IURs>
    {
        public DAPEOAIR_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOAIR_IUR.DBTableName;
        }


        public bool Add(tbPEOAIR_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOAIR_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOAIR_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOAIR_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOAIR_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

        public tbPEOAIR_IURs GetListByBKGREF(string companyCode, string bkgref)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOAIR_IUR.Fields.CompanyCode, companyCode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOAIR_IUR.Fields.BkgRef, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }
    }
}

