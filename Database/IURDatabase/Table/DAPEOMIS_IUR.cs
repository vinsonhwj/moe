﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOMIS]
    /// </summary>
    public partial class DAPEOMIS_IUR : DALDependency<tbPEOMIS_IUR, tbPEOMIS_IURs>
    {
        public DAPEOMIS_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOMIS_IUR.DBTableName;
        }


        public bool Add(tbPEOMIS_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOMIS_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOMIS_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOMIS_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOMIS_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

