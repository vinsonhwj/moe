﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOOTHER]
    /// </summary>
    public partial class DAPEOOTHER_IUR : DALDependency<tbPEOOTHER_IUR, tbPEOOTHER_IURs>
    {
        public DAPEOOTHER_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOOTHER_IUR.DBTableName;
        }


        public bool Add(tbPEOOTHER_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOOTHER_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOOTHER_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOOTHER_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOOTHER_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

