﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PeoAirPax]
    /// </summary>
    public partial class DAPeoAirPax_IUR : DALDependency<tbPeoAirPax_IUR, tbPeoAirPax_IURs>
    {
        public DAPeoAirPax_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPeoAirPax_IUR.DBTableName;
        }

        public bool Exists(string companycode, string bkgref, string segnum, string paxnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPeoAirPax_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPeoAirPax_IUR updateEntity, string companycode, string bkgref, string segnum, string paxnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string bkgref, string segnum, string paxnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPeoAirPax_IUR GetEntity(string companycode, string bkgref, string segnum, string paxnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPeoAirPax_IURs GetList(string companycode, string bkgref, string segnum, string paxnum)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPeoAirPax_IURs GetList(string companycode, string bkgref, string segnum, string paxnum, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoAirPax_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.BKGREF, bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.SEGNUM, segnum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoAirPax_IUR.Fields.PAXNUM, paxnum, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

