﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOHTL]
    /// </summary>
    public partial class DAPEOHTL_IUR : DALDependency<tbPEOHTL_IUR, tbPEOHTL_IURs>
    {
        public DAPEOHTL_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOHTL_IUR.DBTableName;
        }


        public bool Add(tbPEOHTL_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOHTL_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOHTL_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOHTL_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOHTL_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

