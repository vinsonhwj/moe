﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PeoMCO]
    /// </summary>
    public partial class DAPeoMCO_IUR : DALDependency<tbPeoMCO_IUR, tbPeoMCO_IURs>
    {
        public DAPeoMCO_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPeoMCO_IUR.DBTableName;
        }

        public bool Exists(string companycode, string mconum, string mcoseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.RecordCount(fp) > 0;
        }

        public bool Add(tbPeoMCO_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPeoMCO_IUR updateEntity, string companycode, string mconum, string mcoseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Update(updateEntity, fp);
        }

        public bool Delete(string companycode, string mconum, string mcoseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.Delete(fp);
        }

        public tbPeoMCO_IUR GetEntity(string companycode, string mconum, string mcoseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetEntity(fp);
        }

        public tbPeoMCO_IURs GetList(string companycode, string mconum, string mcoseq)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

        public tbPeoMCO_IURs GetList(string companycode, string mconum, string mcoseq, int top)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPeoMCO_IUR.Fields.COMPANYCODE, companycode, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCONUM, mconum, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPeoMCO_IUR.Fields.MCOSEQ, mcoseq, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp, null, top);
        }

    }
}

