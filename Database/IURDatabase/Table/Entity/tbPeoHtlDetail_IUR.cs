﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PeoHtlDetail
    /// </summary>
    [Serializable]
    public class tbPeoHtlDetail_IUR : BaseTable<tbPeoHtlDetail_IUR>
    {
        public tbPeoHtlDetail_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PeoHtlDetail";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEQTYPE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            RMNTS,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ITEMNAME,
            /// <summary>
            ///
            /// </summary>
            STATUS,
            /// <summary>
            ///
            /// </summary>
            CURR,
            /// <summary>
            ///
            /// </summary>
            AMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            TOTALAMT,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _seqtype;
        private String _segnum;
        private String _seqnum;
        private Int32 _qty;
        private DateTime _arrdate;
        private DateTime _departdate;
        private Decimal _rmnts;
        private String _ratename;
        private String _itemname;
        private String _status;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _totalamt;
        private String _createby;
        private DateTime _createon;
        private String _updateby;
        private DateTime _updateon;
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQTYPE", DbType.String, 5)]
        public String SEQTYPE
        {
            set { AddAssigned("SEQTYPE"); _seqtype = value; }
            get { return _seqtype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { AddAssigned("QTY"); _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { AddAssigned("ARRDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("RMNTS", DbType.Decimal, 4)]
        public Decimal RMNTS
        {
            set { AddAssigned("RMNTS"); _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { AddAssigned("RATENAME"); _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ITEMNAME", DbType.String, 100)]
        public String ITEMNAME
        {
            set { AddAssigned("ITEMNAME"); _itemname = value; }
            get { return _itemname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("STATUS", DbType.String, 2)]
        public String STATUS
        {
            set { AddAssigned("STATUS"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CURR", DbType.String, 3)]
        public String CURR
        {
            set { AddAssigned("CURR"); _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("AMT", DbType.Decimal, 14)]
        public Decimal AMT
        {
            set { AddAssigned("AMT"); _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 50)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("TOTALAMT", DbType.Decimal, 18)]
        public Decimal TOTALAMT
        {
            set { AddAssigned("TOTALAMT"); _totalamt = value; }
            get { return _totalamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPeoHtlDetail_IURs : BaseList<tbPeoHtlDetail_IUR, tbPeoHtlDetail_IURs> { }
    public class tbPeoHtlDetail_IURPage : PageResult<tbPeoHtlDetail_IUR, tbPeoHtlDetail_IURs> { }
}

