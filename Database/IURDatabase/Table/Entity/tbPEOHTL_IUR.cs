﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PEOHTL
    /// </summary>
    [Serializable]
    public class tbPEOHTL_IUR : BaseTable<tbPEOHTL_IUR>
    {
        public tbPEOHTL_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOHTL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            ADCOMTXNO,
            /// <summary>
            ///
            /// </summary>
            ADCOMSEQNUM,
            /// <summary>
            ///
            /// </summary>
            HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            ARRTIME,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTTIME,
            /// <summary>
            ///
            /// </summary>
            ETA,
            /// <summary>
            ///
            /// </summary>
            ETD,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALSELL,
            /// <summary>
            ///
            /// </summary>
            TOTALSTAX,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALCOST,
            /// <summary>
            ///
            /// </summary>
            TOTALCTAX,
            /// <summary>
            ///
            /// </summary>
            FORMPAY,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            INVRMK1,
            /// <summary>
            ///
            /// </summary>
            INVRMK2,
            /// <summary>
            ///
            /// </summary>
            VCHRMK1,
            /// <summary>
            ///
            /// </summary>
            VCHRMK2,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            SPCL_REQUEST,
            /// <summary>
            ///
            /// </summary>
            HTLREFERENCE,
            /// <summary>
            ///
            /// </summary>
            HTLCFMBY,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _countrycode;
        private String _citycode;
        private String _adcomtxno;
        private String _adcomseqnum;
        private String _hotelcode;
        private DateTime _arrdate;
        private String _arrtime;
        private DateTime _departdate;
        private String _departtime;
        private String _eta;
        private String _etd;
        private String _sellcurr;
        private Decimal _totalsell;
        private Decimal _totalstax;
        private String _costcurr;
        private Decimal _totalcost;
        private Decimal _totalctax;
        private String _formpay;
        private String _rmk1;
        private String _rmk2;
        private String _invrmk1;
        private String _invrmk2;
        private String _vchrmk1;
        private String _vchrmk2;
        private String _sourcesystem;
        private String _spcl_request;
        private String _htlreference;
        private String _htlcfmby;
        private String _voidby;
        private DateTime _voidon;
        private String _createby;
        private DateTime _createon;
        private String _updateby;
        private DateTime _updateon;
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.String, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.String, 3)]
        public String CITYCODE
        {
            set { AddAssigned("CITYCODE"); _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("ADCOMTXNO", DbType.String, 12)]
        public String ADCOMTXNO
        {
            set { AddAssigned("ADCOMTXNO"); _adcomtxno = value; }
            get { return _adcomtxno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("ADCOMSEQNUM", DbType.String, 5)]
        public String ADCOMSEQNUM
        {
            set { AddAssigned("ADCOMSEQNUM"); _adcomseqnum = value; }
            get { return _adcomseqnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("HOTELCODE", DbType.String, 8)]
        public String HOTELCODE
        {
            set { AddAssigned("HOTELCODE"); _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { AddAssigned("ARRDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("ARRTIME", DbType.String, 6)]
        public String ARRTIME
        {
            set { AddAssigned("ARRTIME"); _arrtime = value; }
            get { return _arrtime; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("DEPARTTIME", DbType.String, 6)]
        public String DEPARTTIME
        {
            set { AddAssigned("DEPARTTIME"); _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("ETA", DbType.String, 14)]
        public String ETA
        {
            set { AddAssigned("ETA"); _eta = value; }
            get { return _eta; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("ETD", DbType.String, 14)]
        public String ETD
        {
            set { AddAssigned("ETD"); _etd = value; }
            get { return _etd; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSELL", DbType.Decimal, 14)]
        public Decimal TOTALSELL
        {
            set { AddAssigned("TOTALSELL"); _totalsell = value; }
            get { return _totalsell; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSTAX", DbType.Decimal, 14)]
        public Decimal TOTALSTAX
        {
            set { AddAssigned("TOTALSTAX"); _totalstax = value; }
            get { return _totalstax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("TOTALCOST", DbType.Decimal, 18)]
        public Decimal TOTALCOST
        {
            set { AddAssigned("TOTALCOST"); _totalcost = value; }
            get { return _totalcost; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("TOTALCTAX", DbType.Decimal, 18)]
        public Decimal TOTALCTAX
        {
            set { AddAssigned("TOTALCTAX"); _totalctax = value; }
            get { return _totalctax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FORMPAY", DbType.String, 50)]
        public String FORMPAY
        {
            set { AddAssigned("FORMPAY"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 20)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 20)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("INVRMK1", DbType.String, 20)]
        public String INVRMK1
        {
            set { AddAssigned("INVRMK1"); _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("INVRMK2", DbType.String, 20)]
        public String INVRMK2
        {
            set { AddAssigned("INVRMK2"); _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("VCHRMK1", DbType.String, 20)]
        public String VCHRMK1
        {
            set { AddAssigned("VCHRMK1"); _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("VCHRMK2", DbType.String, 20)]
        public String VCHRMK2
        {
            set { AddAssigned("VCHRMK2"); _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("SPCL_REQUEST", DbType.String, 255)]
        public String SPCL_REQUEST
        {
            set { AddAssigned("SPCL_REQUEST"); _spcl_request = value; }
            get { return _spcl_request; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("HTLREFERENCE", DbType.String, 10)]
        public String HTLREFERENCE
        {
            set { AddAssigned("HTLREFERENCE"); _htlreference = value; }
            get { return _htlreference; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("HTLCFMBY", DbType.String, 30)]
        public String HTLCFMBY
        {
            set { AddAssigned("HTLCFMBY"); _htlcfmby = value; }
            get { return _htlcfmby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOHTL_IURs : BaseList<tbPEOHTL_IUR, tbPEOHTL_IURs> { }
    public class tbPEOHTL_IURPage : PageResult<tbPEOHTL_IUR, tbPEOHTL_IURs> { }
}

