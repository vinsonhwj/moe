﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:Airline
    /// </summary>
    [Serializable]
    public class tbAirline_IUR : BaseTable<tbAirline_IUR>
    {
        public tbAirline_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "Airline";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            AIRLINE,
            /// <summary>
            ///
            /// </summary>
            AIRCODE,
            /// <summary>
            ///
            /// </summary>
            AIRLINENAME,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
        }

        #region Model
        private String _airline;
        private String _aircode;
        private String _airlinename;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("AIRLINE", DbType.StringFixedLength, 3)]
        public String AIRLINE
        {
            set { AddAssigned("AIRLINE"); _airline = value; }
            get { return _airline; }
        }
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("AIRCODE", DbType.StringFixedLength, 2)]
        public String AIRCODE
        {
            set { AddAssigned("AIRCODE"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("AIRLINENAME", DbType.String, 70)]
        public String AIRLINENAME
        {
            set { AddAssigned("AIRLINENAME"); _airlinename = value; }
            get { return _airlinename; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.StringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.StringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbAirline_IURs : BaseList<tbAirline_IUR, tbAirline_IURs> { }
    public class tbAirline_IURPage : PageResult<tbAirline_IUR, tbAirline_IURs> { }
}

