﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PeoMCO
    /// </summary>
    [Serializable]
    public class tbPeoMCO_IUR : BaseTable<tbPeoMCO_IUR>
    {
        public tbPeoMCO_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PeoMCO";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            MCONUM,
            /// <summary>
            ///
            /// </summary>
            MCOSEQ,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            CltCode,
            /// <summary>
            ///
            /// </summary>
            SuppCode,
            /// <summary>
            ///
            /// </summary>
            Jqty,
            /// <summary>
            ///
            /// </summary>
            Airline,
            /// <summary>
            ///
            /// </summary>
            AirCode,
            /// <summary>
            ///
            /// </summary>
            branchcode,
            /// <summary>
            ///
            /// </summary>
            PaxName,
            /// <summary>
            ///
            /// </summary>
            DepartOn,
            /// <summary>
            ///
            /// </summary>
            Commission,
            /// <summary>
            ///
            /// </summary>
            LocalFareCurr,
            /// <summary>
            ///
            /// </summary>
            LocalFareAmt,
            /// <summary>
            ///
            /// </summary>
            ForeignFareCurr,
            /// <summary>
            ///
            /// </summary>
            ForeignFareAmt,
            /// <summary>
            ///
            /// </summary>
            Discount,
            /// <summary>
            ///
            /// </summary>
            FormPay,
            /// <summary>
            ///
            /// </summary>
            BANK,
            /// <summary>
            ///
            /// </summary>
            CHQNUM,
            /// <summary>
            ///
            /// </summary>
            CHQISSUEON,
            /// <summary>
            ///
            /// </summary>
            Routing,
            /// <summary>
            ///
            /// </summary>
            FullCurr,
            /// <summary>
            ///
            /// </summary>
            FullFare,
            /// <summary>
            ///
            /// </summary>
            PaidCurr,
            /// <summary>
            ///
            /// </summary>
            PaidFare,
            /// <summary>
            ///
            /// </summary>
            LowCurr,
            /// <summary>
            ///
            /// </summary>
            LowFare,
            /// <summary>
            ///
            /// </summary>
            NetCurr,
            /// <summary>
            ///
            /// </summary>
            NetFare,
            /// <summary>
            ///
            /// </summary>
            FareBasis,
            /// <summary>
            ///
            /// </summary>
            FareType,
            /// <summary>
            ///
            /// </summary>
            TotalTax,
            /// <summary>
            ///
            /// </summary>
            Security,
            /// <summary>
            ///
            /// </summary>
            Qsurcharge,
            /// <summary>
            ///
            /// </summary>
            VoidOn,
            /// <summary>
            ///
            /// </summary>
            VoidBy,
            /// <summary>
            ///
            /// </summary>
            TaxAmount1,
            /// <summary>
            ///
            /// </summary>
            TaxID1,
            /// <summary>
            ///
            /// </summary>
            TaxAmount2,
            /// <summary>
            ///
            /// </summary>
            TaxID2,
            /// <summary>
            ///
            /// </summary>
            TaxAmount3,
            /// <summary>
            ///
            /// </summary>
            TaxID3,
            /// <summary>
            ///
            /// </summary>
            FareCalType,
            /// <summary>
            ///
            /// </summary>
            TktType,
            /// <summary>
            ///
            /// </summary>
            FareCalData,
            /// <summary>
            ///
            /// </summary>
            BookingPseudo,
            /// <summary>
            ///
            /// </summary>
            BookingSignin,
            /// <summary>
            ///
            /// </summary>
            TicketPseudo,
            /// <summary>
            ///
            /// </summary>
            TicketSignin,
            /// <summary>
            ///
            /// </summary>
            AirSeg,
            /// <summary>
            ///
            /// </summary>
            Nst,
            /// <summary>
            ///
            /// </summary>
            Commission_per,
            /// <summary>
            ///
            /// </summary>
            Add_payment,
            /// <summary>
            ///
            /// </summary>
            Iss_off_code,
            /// <summary>
            ///
            /// </summary>
            Tour_code,
            /// <summary>
            ///
            /// </summary>
            Org_Iss,
            /// <summary>
            ///
            /// </summary>
            Iss_in_exchange,
            /// <summary>
            ///
            /// </summary>
            ControlNum,
            /// <summary>
            ///
            /// </summary>
            Endorsements,
            /// <summary>
            ///
            /// </summary>
            BookCRS,
            /// <summary>
            ///
            /// </summary>
            AIRDST,
            /// <summary>
            ///
            /// </summary>
            AIRREASON,
            /// <summary>
            ///
            /// </summary>
            TRIPTYPE,
            /// <summary>
            ///
            /// </summary>
            class_first_seg,
            /// <summary>
            ///
            /// </summary>
            CorpFareCurr,
            /// <summary>
            ///
            /// </summary>
            CorpFare,
            /// <summary>
            ///
            /// </summary>
            IntDom,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            COD4,
            /// <summary>
            ///
            /// </summary>
            COD5,
            /// <summary>
            ///
            /// </summary>
            COD6,
            /// <summary>
            ///
            /// </summary>
            create_date,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            SELLFARE,
            /// <summary>
            ///
            /// </summary>
            GSTPCT,
            /// <summary>
            ///
            /// </summary>
            GSTAMT,
            /// <summary>
            ///
            /// </summary>
            MCOType,
            /// <summary>
            ///
            /// </summary>
            Remark1,
            /// <summary>
            ///
            /// </summary>
            Remark2,
            /// <summary>
            ///
            /// </summary>
            Remark3,
            /// <summary>
            ///
            /// </summary>
            Description,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            teamcode,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            sourcesystem,
            /// <summary>
            ///
            /// </summary>
            TRANFLAG,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _mconum;
        private String _mcoseq;
        private String _pnr;
        private String _cltcode;
        private String _suppcode;
        private Int32 _jqty;
        private String _airline;
        private String _aircode;
        private String _branchcode;
        private String _paxname;
        private DateTime _departon;
        private Decimal _commission;
        private String _localfarecurr;
        private Decimal _localfareamt;
        private String _foreignfarecurr;
        private Decimal _foreignfareamt;
        private Decimal _discount;
        private String _formpay;
        private String _bank;
        private String _chqnum;
        private DateTime _chqissueon;
        private String _routing;
        private String _fullcurr;
        private Decimal _fullfare;
        private String _paidcurr;
        private Decimal _paidfare;
        private String _lowcurr;
        private Decimal _lowfare;
        private String _netcurr;
        private Decimal _netfare;
        private String _farebasis;
        private String _faretype;
        private Decimal _totaltax;
        private Decimal _security;
        private Decimal _qsurcharge;
        private DateTime _voidon;
        private String _voidby;
        private Decimal _taxamount1;
        private String _taxid1;
        private Decimal _taxamount2;
        private String _taxid2;
        private Decimal _taxamount3;
        private String _taxid3;
        private String _farecaltype;
        private String _tkttype;
        private String _farecaldata;
        private String _bookingpseudo;
        private String _bookingsignin;
        private String _ticketpseudo;
        private String _ticketsignin;
        private Int32 _airseg;
        private String _nst;
        private Decimal _commission_per;
        private Decimal _add_payment;
        private String _iss_off_code;
        private String _tour_code;
        private String _org_iss;
        private String _iss_in_exchange;
        private String _controlnum;
        private String _endorsements;
        private String _bookcrs;
        private String _airdst;
        private String _airreason;
        private String _triptype;
        private String _class_first_seg;
        private String _corpfarecurr;
        private Decimal _corpfare;
        private String _intdom;
        private String _cod1;
        private String _cod2;
        private String _cod3;
        private String _cod4;
        private String _cod5;
        private String _cod6;
        private DateTime _create_date;
        private String _sellcurr;
        private Decimal _sellfare;
        private Decimal _gstpct;
        private Decimal _gstamt;
        private String _mcotype;
        private String _remark1;
        private String _remark2;
        private String _remark3;
        private String _description;
        private DateTime _createon;
        private String _createby;
        private String _teamcode;
        private DateTime _updateon;
        private String _updateby;
        private String _sourcesystem;
        private String _tranflag;
        /// <summary>
        /// [PK/Un-Null/char(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.AnsiStringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/char(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.AnsiStringFixedLength, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/varchar(20)]
        /// </summary>
        [FieldMapping("MCONUM", DbType.AnsiString, 20)]
        public String MCONUM
        {
            set { AddAssigned("MCONUM"); _mconum = value; }
            get { return _mconum; }
        }
        /// <summary>
        /// [PK/Un-Null/char(5)]
        /// </summary>
        [FieldMapping("MCOSEQ", DbType.AnsiStringFixedLength, 5)]
        public String MCOSEQ
        {
            set { AddAssigned("MCOSEQ"); _mcoseq = value; }
            get { return _mcoseq; }
        }
        /// <summary>
        /// [Allow Null/char(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.AnsiStringFixedLength, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/varchar(8)]
        /// </summary>
        [FieldMapping("CltCode", DbType.AnsiString, 8)]
        public String CltCode
        {
            set { AddAssigned("CltCode"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/varchar(6)]
        /// </summary>
        [FieldMapping("SuppCode", DbType.AnsiString, 6)]
        public String SuppCode
        {
            set { AddAssigned("SuppCode"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("Jqty", DbType.Int32, 10)]
        public Int32 Jqty
        {
            set { AddAssigned("Jqty"); _jqty = value; }
            get { return _jqty; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("Airline", DbType.AnsiStringFixedLength, 3)]
        public String Airline
        {
            set { AddAssigned("Airline"); _airline = value; }
            get { return _airline; }
        }
        /// <summary>
        /// [Allow Null/char(2)]
        /// </summary>
        [FieldMapping("AirCode", DbType.AnsiStringFixedLength, 2)]
        public String AirCode
        {
            set { AddAssigned("AirCode"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("branchcode", DbType.AnsiStringFixedLength, 1)]
        public String branchcode
        {
            set { AddAssigned("branchcode"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [Allow Null/varchar(29)]
        /// </summary>
        [FieldMapping("PaxName", DbType.AnsiString, 29)]
        public String PaxName
        {
            set { AddAssigned("PaxName"); _paxname = value; }
            get { return _paxname; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DepartOn", DbType.DateTime, 8)]
        public DateTime DepartOn
        {
            set { AddAssigned("DepartOn"); _departon = value; }
            get { return _departon; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Commission", DbType.Decimal, 14)]
        public Decimal Commission
        {
            set { AddAssigned("Commission"); _commission = value; }
            get { return _commission; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("LocalFareCurr", DbType.AnsiStringFixedLength, 3)]
        public String LocalFareCurr
        {
            set { AddAssigned("LocalFareCurr"); _localfarecurr = value; }
            get { return _localfarecurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("LocalFareAmt", DbType.Decimal, 14)]
        public Decimal LocalFareAmt
        {
            set { AddAssigned("LocalFareAmt"); _localfareamt = value; }
            get { return _localfareamt; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("ForeignFareCurr", DbType.AnsiStringFixedLength, 3)]
        public String ForeignFareCurr
        {
            set { AddAssigned("ForeignFareCurr"); _foreignfarecurr = value; }
            get { return _foreignfarecurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("ForeignFareAmt", DbType.Decimal, 14)]
        public Decimal ForeignFareAmt
        {
            set { AddAssigned("ForeignFareAmt"); _foreignfareamt = value; }
            get { return _foreignfareamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Discount", DbType.Decimal, 14)]
        public Decimal Discount
        {
            set { AddAssigned("Discount"); _discount = value; }
            get { return _discount; }
        }
        /// <summary>
        /// [Allow Null/varchar(44)]
        /// </summary>
        [FieldMapping("FormPay", DbType.AnsiString, 44)]
        public String FormPay
        {
            set { AddAssigned("FormPay"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/varchar(320)]
        /// </summary>
        [FieldMapping("BANK", DbType.AnsiString, 320)]
        public String BANK
        {
            set { AddAssigned("BANK"); _bank = value; }
            get { return _bank; }
        }
        /// <summary>
        /// [Allow Null/varchar(40)]
        /// </summary>
        [FieldMapping("CHQNUM", DbType.AnsiString, 40)]
        public String CHQNUM
        {
            set { AddAssigned("CHQNUM"); _chqnum = value; }
            get { return _chqnum; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CHQISSUEON", DbType.DateTime, 8)]
        public DateTime CHQISSUEON
        {
            set { AddAssigned("CHQISSUEON"); _chqissueon = value; }
            get { return _chqissueon; }
        }
        /// <summary>
        /// [Allow Null/varchar(128)]
        /// </summary>
        [FieldMapping("Routing", DbType.AnsiString, 128)]
        public String Routing
        {
            set { AddAssigned("Routing"); _routing = value; }
            get { return _routing; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("FullCurr", DbType.AnsiStringFixedLength, 3)]
        public String FullCurr
        {
            set { AddAssigned("FullCurr"); _fullcurr = value; }
            get { return _fullcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("FullFare", DbType.Decimal, 14)]
        public Decimal FullFare
        {
            set { AddAssigned("FullFare"); _fullfare = value; }
            get { return _fullfare; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("PaidCurr", DbType.AnsiStringFixedLength, 3)]
        public String PaidCurr
        {
            set { AddAssigned("PaidCurr"); _paidcurr = value; }
            get { return _paidcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("PaidFare", DbType.Decimal, 14)]
        public Decimal PaidFare
        {
            set { AddAssigned("PaidFare"); _paidfare = value; }
            get { return _paidfare; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("LowCurr", DbType.AnsiStringFixedLength, 3)]
        public String LowCurr
        {
            set { AddAssigned("LowCurr"); _lowcurr = value; }
            get { return _lowcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("LowFare", DbType.Decimal, 14)]
        public Decimal LowFare
        {
            set { AddAssigned("LowFare"); _lowfare = value; }
            get { return _lowfare; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("NetCurr", DbType.AnsiStringFixedLength, 3)]
        public String NetCurr
        {
            set { AddAssigned("NetCurr"); _netcurr = value; }
            get { return _netcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("NetFare", DbType.Decimal, 14)]
        public Decimal NetFare
        {
            set { AddAssigned("NetFare"); _netfare = value; }
            get { return _netfare; }
        }
        /// <summary>
        /// [Allow Null/varchar(14)]
        /// </summary>
        [FieldMapping("FareBasis", DbType.AnsiString, 14)]
        public String FareBasis
        {
            set { AddAssigned("FareBasis"); _farebasis = value; }
            get { return _farebasis; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("FareType", DbType.AnsiStringFixedLength, 1)]
        public String FareType
        {
            set { AddAssigned("FareType"); _faretype = value; }
            get { return _faretype; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TotalTax", DbType.Decimal, 14)]
        public Decimal TotalTax
        {
            set { AddAssigned("TotalTax"); _totaltax = value; }
            get { return _totaltax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Security", DbType.Decimal, 14)]
        public Decimal Security
        {
            set { AddAssigned("Security"); _security = value; }
            get { return _security; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Qsurcharge", DbType.Decimal, 14)]
        public Decimal Qsurcharge
        {
            set { AddAssigned("Qsurcharge"); _qsurcharge = value; }
            get { return _qsurcharge; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VoidOn", DbType.DateTime, 8)]
        public DateTime VoidOn
        {
            set { AddAssigned("VoidOn"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/varchar(10)]
        /// </summary>
        [FieldMapping("VoidBy", DbType.AnsiString, 10)]
        public String VoidBy
        {
            set { AddAssigned("VoidBy"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TaxAmount1", DbType.Decimal, 14)]
        public Decimal TaxAmount1
        {
            set { AddAssigned("TaxAmount1"); _taxamount1 = value; }
            get { return _taxamount1; }
        }
        /// <summary>
        /// [Allow Null/char(2)]
        /// </summary>
        [FieldMapping("TaxID1", DbType.AnsiStringFixedLength, 2)]
        public String TaxID1
        {
            set { AddAssigned("TaxID1"); _taxid1 = value; }
            get { return _taxid1; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TaxAmount2", DbType.Decimal, 14)]
        public Decimal TaxAmount2
        {
            set { AddAssigned("TaxAmount2"); _taxamount2 = value; }
            get { return _taxamount2; }
        }
        /// <summary>
        /// [Allow Null/char(2)]
        /// </summary>
        [FieldMapping("TaxID2", DbType.AnsiStringFixedLength, 2)]
        public String TaxID2
        {
            set { AddAssigned("TaxID2"); _taxid2 = value; }
            get { return _taxid2; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TaxAmount3", DbType.Decimal, 14)]
        public Decimal TaxAmount3
        {
            set { AddAssigned("TaxAmount3"); _taxamount3 = value; }
            get { return _taxamount3; }
        }
        /// <summary>
        /// [Allow Null/char(2)]
        /// </summary>
        [FieldMapping("TaxID3", DbType.AnsiStringFixedLength, 2)]
        public String TaxID3
        {
            set { AddAssigned("TaxID3"); _taxid3 = value; }
            get { return _taxid3; }
        }
        /// <summary>
        /// [Allow Null/char(10)]
        /// </summary>
        [FieldMapping("FareCalType", DbType.AnsiStringFixedLength, 10)]
        public String FareCalType
        {
            set { AddAssigned("FareCalType"); _farecaltype = value; }
            get { return _farecaltype; }
        }
        /// <summary>
        /// [Un-Null/char(1)]
        /// </summary>
        [FieldMapping("TktType", DbType.AnsiStringFixedLength, 1)]
        public String TktType
        {
            set { AddAssigned("TktType"); _tkttype = value; }
            get { return _tkttype; }
        }
        /// <summary>
        /// [Allow Null/varchar(244)]
        /// </summary>
        [FieldMapping("FareCalData", DbType.AnsiString, 244)]
        public String FareCalData
        {
            set { AddAssigned("FareCalData"); _farecaldata = value; }
            get { return _farecaldata; }
        }
        /// <summary>
        /// [Un-Null/char(4)]
        /// </summary>
        [FieldMapping("BookingPseudo", DbType.AnsiStringFixedLength, 4)]
        public String BookingPseudo
        {
            set { AddAssigned("BookingPseudo"); _bookingpseudo = value; }
            get { return _bookingpseudo; }
        }
        /// <summary>
        /// [Un-Null/char(7)]
        /// </summary>
        [FieldMapping("BookingSignin", DbType.AnsiStringFixedLength, 7)]
        public String BookingSignin
        {
            set { AddAssigned("BookingSignin"); _bookingsignin = value; }
            get { return _bookingsignin; }
        }
        /// <summary>
        /// [Un-Null/char(4)]
        /// </summary>
        [FieldMapping("TicketPseudo", DbType.AnsiStringFixedLength, 4)]
        public String TicketPseudo
        {
            set { AddAssigned("TicketPseudo"); _ticketpseudo = value; }
            get { return _ticketpseudo; }
        }
        /// <summary>
        /// [Un-Null/char(7)]
        /// </summary>
        [FieldMapping("TicketSignin", DbType.AnsiStringFixedLength, 7)]
        public String TicketSignin
        {
            set { AddAssigned("TicketSignin"); _ticketsignin = value; }
            get { return _ticketsignin; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("AirSeg", DbType.Int32, 10)]
        public Int32 AirSeg
        {
            set { AddAssigned("AirSeg"); _airseg = value; }
            get { return _airseg; }
        }
        /// <summary>
        /// [Allow Null/char(4)]
        /// </summary>
        [FieldMapping("Nst", DbType.AnsiStringFixedLength, 4)]
        public String Nst
        {
            set { AddAssigned("Nst"); _nst = value; }
            get { return _nst; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Commission_per", DbType.Decimal, 14)]
        public Decimal Commission_per
        {
            set { AddAssigned("Commission_per"); _commission_per = value; }
            get { return _commission_per; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Add_payment", DbType.Decimal, 14)]
        public Decimal Add_payment
        {
            set { AddAssigned("Add_payment"); _add_payment = value; }
            get { return _add_payment; }
        }
        /// <summary>
        /// [Allow Null/char(8)]
        /// </summary>
        [FieldMapping("Iss_off_code", DbType.AnsiStringFixedLength, 8)]
        public String Iss_off_code
        {
            set { AddAssigned("Iss_off_code"); _iss_off_code = value; }
            get { return _iss_off_code; }
        }
        /// <summary>
        /// [Allow Null/varchar(14)]
        /// </summary>
        [FieldMapping("Tour_code", DbType.AnsiString, 14)]
        public String Tour_code
        {
            set { AddAssigned("Tour_code"); _tour_code = value; }
            get { return _tour_code; }
        }
        /// <summary>
        /// [Allow Null/char(8)]
        /// </summary>
        [FieldMapping("Org_Iss", DbType.AnsiStringFixedLength, 8)]
        public String Org_Iss
        {
            set { AddAssigned("Org_Iss"); _org_iss = value; }
            get { return _org_iss; }
        }
        /// <summary>
        /// [Allow Null/varchar(14)]
        /// </summary>
        [FieldMapping("Iss_in_exchange", DbType.AnsiString, 14)]
        public String Iss_in_exchange
        {
            set { AddAssigned("Iss_in_exchange"); _iss_in_exchange = value; }
            get { return _iss_in_exchange; }
        }
        /// <summary>
        /// [Allow Null/varchar(17)]
        /// </summary>
        [FieldMapping("ControlNum", DbType.AnsiString, 17)]
        public String ControlNum
        {
            set { AddAssigned("ControlNum"); _controlnum = value; }
            get { return _controlnum; }
        }
        /// <summary>
        /// [Allow Null/varchar(127)]
        /// </summary>
        [FieldMapping("Endorsements", DbType.AnsiString, 127)]
        public String Endorsements
        {
            set { AddAssigned("Endorsements"); _endorsements = value; }
            get { return _endorsements; }
        }
        /// <summary>
        /// [Un-Null/char(2)]
        /// </summary>
        [FieldMapping("BookCRS", DbType.AnsiStringFixedLength, 2)]
        public String BookCRS
        {
            set { AddAssigned("BookCRS"); _bookcrs = value; }
            get { return _bookcrs; }
        }
        /// <summary>
        /// [Allow Null/varchar(100)]
        /// </summary>
        [FieldMapping("AIRDST", DbType.AnsiString, 100)]
        public String AIRDST
        {
            set { AddAssigned("AIRDST"); _airdst = value; }
            get { return _airdst; }
        }
        /// <summary>
        /// [Allow Null/varchar(100)]
        /// </summary>
        [FieldMapping("AIRREASON", DbType.AnsiString, 100)]
        public String AIRREASON
        {
            set { AddAssigned("AIRREASON"); _airreason = value; }
            get { return _airreason; }
        }
        /// <summary>
        /// [Allow Null/varchar(100)]
        /// </summary>
        [FieldMapping("TRIPTYPE", DbType.AnsiString, 100)]
        public String TRIPTYPE
        {
            set { AddAssigned("TRIPTYPE"); _triptype = value; }
            get { return _triptype; }
        }
        /// <summary>
        /// [Allow Null/varchar(2)]
        /// </summary>
        [FieldMapping("class_first_seg", DbType.AnsiString, 2)]
        public String class_first_seg
        {
            set { AddAssigned("class_first_seg"); _class_first_seg = value; }
            get { return _class_first_seg; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("CorpFareCurr", DbType.AnsiStringFixedLength, 3)]
        public String CorpFareCurr
        {
            set { AddAssigned("CorpFareCurr"); _corpfarecurr = value; }
            get { return _corpfarecurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("CorpFare", DbType.Decimal, 16)]
        public Decimal CorpFare
        {
            set { AddAssigned("CorpFare"); _corpfare = value; }
            get { return _corpfare; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IntDom", DbType.AnsiStringFixedLength, 1)]
        public String IntDom
        {
            set { AddAssigned("IntDom"); _intdom = value; }
            get { return _intdom; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD1", DbType.AnsiStringFixedLength, 15)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD2", DbType.AnsiStringFixedLength, 15)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD3", DbType.AnsiStringFixedLength, 15)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD4", DbType.AnsiStringFixedLength, 15)]
        public String COD4
        {
            set { AddAssigned("COD4"); _cod4 = value; }
            get { return _cod4; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD5", DbType.AnsiStringFixedLength, 15)]
        public String COD5
        {
            set { AddAssigned("COD5"); _cod5 = value; }
            get { return _cod5; }
        }
        /// <summary>
        /// [Allow Null/char(15)]
        /// </summary>
        [FieldMapping("COD6", DbType.AnsiStringFixedLength, 15)]
        public String COD6
        {
            set { AddAssigned("COD6"); _cod6 = value; }
            get { return _cod6; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("create_date", DbType.DateTime, 8)]
        public DateTime create_date
        {
            set { AddAssigned("create_date"); _create_date = value; }
            get { return _create_date; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.AnsiStringFixedLength, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SELLFARE", DbType.Decimal, 14)]
        public Decimal SELLFARE
        {
            set { AddAssigned("SELLFARE"); _sellfare = value; }
            get { return _sellfare; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("GSTPCT", DbType.Decimal, 14)]
        public Decimal GSTPCT
        {
            set { AddAssigned("GSTPCT"); _gstpct = value; }
            get { return _gstpct; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("GSTAMT", DbType.Decimal, 14)]
        public Decimal GSTAMT
        {
            set { AddAssigned("GSTAMT"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("MCOType", DbType.AnsiStringFixedLength, 3)]
        public String MCOType
        {
            set { AddAssigned("MCOType"); _mcotype = value; }
            get { return _mcotype; }
        }
        /// <summary>
        /// [Allow Null/varchar(110)]
        /// </summary>
        [FieldMapping("Remark1", DbType.AnsiString, 110)]
        public String Remark1
        {
            set { AddAssigned("Remark1"); _remark1 = value; }
            get { return _remark1; }
        }
        /// <summary>
        /// [Allow Null/varchar(110)]
        /// </summary>
        [FieldMapping("Remark2", DbType.AnsiString, 110)]
        public String Remark2
        {
            set { AddAssigned("Remark2"); _remark2 = value; }
            get { return _remark2; }
        }
        /// <summary>
        /// [Allow Null/varchar(110)]
        /// </summary>
        [FieldMapping("Remark3", DbType.AnsiString, 110)]
        public String Remark3
        {
            set { AddAssigned("Remark3"); _remark3 = value; }
            get { return _remark3; }
        }
        /// <summary>
        /// [Allow Null/varchar(330)]
        /// </summary>
        [FieldMapping("Description", DbType.AnsiString, 330)]
        public String Description
        {
            set { AddAssigned("Description"); _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/varchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.AnsiString, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/char(10)]
        /// </summary>
        [FieldMapping("teamcode", DbType.AnsiStringFixedLength, 10)]
        public String teamcode
        {
            set { AddAssigned("teamcode"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/varchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.AnsiString, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/char(10)]
        /// </summary>
        [FieldMapping("sourcesystem", DbType.AnsiStringFixedLength, 10)]
        public String sourcesystem
        {
            set { AddAssigned("sourcesystem"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("TRANFLAG", DbType.AnsiStringFixedLength, 1)]
        public String TRANFLAG
        {
            set { AddAssigned("TRANFLAG"); _tranflag = value; }
            get { return _tranflag; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPeoMCO_IURs : BaseList<tbPeoMCO_IUR, tbPeoMCO_IURs> { }
    public class tbPeoMCO_IURPage : PageResult<tbPeoMCO_IUR, tbPeoMCO_IURs> { }
}

