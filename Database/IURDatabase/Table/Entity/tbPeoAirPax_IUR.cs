﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PeoAirPax
    /// </summary>
    [Serializable]
    public class tbPeoAirPax_IUR : BaseTable<tbPeoAirPax_IUR>
    {
        public tbPeoAirPax_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PeoAirPax";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            PAXNUM,
            /// <summary>
            ///
            /// </summary>
            Freq_Fight_num,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _paxnum;
        private String _freq_fight_num;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PAXNUM", DbType.String, 5)]
        public String PAXNUM
        {
            set { AddAssigned("PAXNUM"); _paxnum = value; }
            get { return _paxnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("Freq_Fight_num", DbType.String, 20)]
        public String Freq_Fight_num
        {
            set { AddAssigned("Freq_Fight_num"); _freq_fight_num = value; }
            get { return _freq_fight_num; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPeoAirPax_IURs : BaseList<tbPeoAirPax_IUR, tbPeoAirPax_IURs> { }
    public class tbPeoAirPax_IURPage : PageResult<tbPeoAirPax_IUR, tbPeoAirPax_IURs> { }
}

