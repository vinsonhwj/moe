﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:IURLog
    /// </summary>
    [Serializable]
    public class tbIURLog_IUR : BaseTable<tbIURLog_IUR>
    {
        public tbIURLog_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "IURLog";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            IURLOGID,
            /// <summary>
            ///
            /// </summary>
            URFilename,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            Description,
            /// <summary>
            ///
            /// </summary>
            Status,
            /// <summary>
            ///
            /// </summary>
            Remarks,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
            /// <summary>
            ///
            /// </summary>
            CreateBy,
        }

        #region Model
        private Decimal _iurlogid;
        private String _urfilename;
        private String _pnr;
        private String _description;
        private String _status;
        private String _remarks;
        private DateTime _createon;
        private String _createby;
        /// <summary>
        /// [Un-Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("IURLOGID", DbType.Decimal, 18, Enums.DataHandle.UnInsert, Enums.DataHandle.UnUpdate)]
        public Decimal IURLOGID
        {
            set { AddAssigned("IURLOGID"); _iurlogid = value; }
            get { return _iurlogid; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("URFilename", DbType.String, 200)]
        public String URFilename
        {
            set { AddAssigned("URFilename"); _urfilename = value; }
            get { return _urfilename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 10)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Description", DbType.String, 50)]
        public String Description
        {
            set { AddAssigned("Description"); _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Status", DbType.String, 50)]
        public String Status
        {
            set { AddAssigned("Status"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4000)]
        /// </summary>
        [FieldMapping("Remarks", DbType.String, 4000)]
        public String Remarks
        {
            set { AddAssigned("Remarks"); _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("CreateBy", DbType.String, 20)]
        public String CreateBy
        {
            set { AddAssigned("CreateBy"); _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbIURLog_IURs : BaseList<tbIURLog_IUR, tbIURLog_IURs> { }
    public class tbIURLog_IURPage : PageResult<tbIURLog_IUR, tbIURLog_IURs> { }
}

