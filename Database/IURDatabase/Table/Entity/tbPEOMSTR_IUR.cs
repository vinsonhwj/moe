﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PEOMSTR
    /// </summary>
    [Serializable]
    public class tbPEOMSTR_IUR : BaseTable<tbPEOMSTR_IUR>
    {
        public tbPEOMSTR_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOMSTR";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            CRSSINEIN,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            CLTODE,
            /// <summary>
            ///
            /// </summary>
            CLTNAME,
            /// <summary>
            ///
            /// </summary>
            CLTADDR,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            FIRSTPAX,
            /// <summary>
            ///
            /// </summary>
            PHONE,
            /// <summary>
            ///
            /// </summary>
            FAX,
            /// <summary>
            ///
            /// </summary>
            EMAIL,
            /// <summary>
            ///
            /// </summary>
            MASTERPNR,
            /// <summary>
            ///
            /// </summary>
            TOURCODE,
            /// <summary>
            ///
            /// </summary>
            CONTACTPERSON,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            TTLSELLCURR,
            /// <summary>
            ///
            /// </summary>
            TTLSELLAMT,
            /// <summary>
            ///
            /// </summary>
            TTLSELLTAXCURR,
            /// <summary>
            ///
            /// </summary>
            TTLSELLTAX,
            /// <summary>
            ///
            /// </summary>
            TTLCOSTCURR,
            /// <summary>
            ///
            /// </summary>
            TTLCOSTAMT,
            /// <summary>
            ///
            /// </summary>
            TTLCOSTTAXCURR,
            /// <summary>
            ///
            /// </summary>
            TTLCOSTTAX,
            /// <summary>
            ///
            /// </summary>
            INVCURR,
            /// <summary>
            ///
            /// </summary>
            INVAMT,
            /// <summary>
            ///
            /// </summary>
            INVTAXCURR,
            /// <summary>
            ///
            /// </summary>
            INVTAX,
            /// <summary>
            ///
            /// </summary>
            DOCCURR,
            /// <summary>
            ///
            /// </summary>
            DOCAMT,
            /// <summary>
            ///
            /// </summary>
            DOCTAXCURR,
            /// <summary>
            ///
            /// </summary>
            DOCTAX,
            /// <summary>
            ///
            /// </summary>
            INVCOUNT,
            /// <summary>
            ///
            /// </summary>
            DOCCOUNT,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            RMK3,
            /// <summary>
            ///
            /// </summary>
            RMK4,
            /// <summary>
            ///
            /// </summary>
            DEADLINE,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            SOURCE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            INSFLAG,
            /// <summary>
            ///
            /// </summary>
            TOEMOSON,
            /// <summary>
            ///
            /// </summary>
            TOEMOSBY,
            /// <summary>
            ///
            /// </summary>
            EMOSBKGREF,
            /// <summary>
            ///
            /// </summary>
            IURFLAG,
            /// <summary>
            ///
            /// </summary>
            pseudo,
            /// <summary>
            ///
            /// </summary>
            pseudo_inv,
            /// <summary>
            ///
            /// </summary>
            CRSSINEIN_inv,
            /// <summary>
            ///
            /// </summary>
            agency_arc_iata_num,
            /// <summary>
            ///
            /// </summary>
            iss_off_code,
            /// <summary>
            ///
            /// </summary>
            tranflag,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _pnr;
        private String _crssinein;
        private String _cod1;
        private String _cod2;
        private String _cltode;
        private String _cltname;
        private String _cltaddr;
        private String _staffcode;
        private String _teamcode;
        private String _firstpax;
        private String _phone;
        private String _fax;
        private String _email;
        private String _masterpnr;
        private String _tourcode;
        private String _contactperson;
        private DateTime _departdate;
        private String _ttlsellcurr;
        private Decimal _ttlsellamt;
        private String _ttlselltaxcurr;
        private Decimal _ttlselltax;
        private String _ttlcostcurr;
        private Decimal _ttlcostamt;
        private String _ttlcosttaxcurr;
        private Decimal _ttlcosttax;
        private String _invcurr;
        private Decimal _invamt;
        private String _invtaxcurr;
        private Decimal _invtax;
        private String _doccurr;
        private Decimal _docamt;
        private String _doctaxcurr;
        private Decimal _doctax;
        private Int32 _invcount;
        private Int32 _doccount;
        private String _rmk1;
        private String _rmk2;
        private String _rmk3;
        private String _rmk4;
        private DateTime _deadline;
        private String _sourcesystem;
        private String _source;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _cod3;
        private String _insflag;
        private DateTime _toemoson;
        private String _toemosby;
        private String _emosbkgref;
        private String _iurflag;
        private String _pseudo;
        private String _pseudo_inv;
        private String _crssinein_inv;
        private String _agency_arc_iata_num;
        private String _iss_off_code;
        private String _tranflag;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("CRSSINEIN", DbType.String, 4)]
        public String CRSSINEIN
        {
            set { AddAssigned("CRSSINEIN"); _crssinein = value; }
            get { return _crssinein; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 20)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 20)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(25)]
        /// </summary>
        [FieldMapping("CLTODE", DbType.String, 25)]
        public String CLTODE
        {
            set { AddAssigned("CLTODE"); _cltode = value; }
            get { return _cltode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(25)]
        /// </summary>
        [FieldMapping("CLTNAME", DbType.String, 25)]
        public String CLTNAME
        {
            set { AddAssigned("CLTNAME"); _cltname = value; }
            get { return _cltname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(160)]
        /// </summary>
        [FieldMapping("CLTADDR", DbType.String, 160)]
        public String CLTADDR
        {
            set { AddAssigned("CLTADDR"); _cltaddr = value; }
            get { return _cltaddr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.String, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("FIRSTPAX", DbType.String, 40)]
        public String FIRSTPAX
        {
            set { AddAssigned("FIRSTPAX"); _firstpax = value; }
            get { return _firstpax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE", DbType.String, 20)]
        public String PHONE
        {
            set { AddAssigned("PHONE"); _phone = value; }
            get { return _phone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("FAX", DbType.String, 20)]
        public String FAX
        {
            set { AddAssigned("FAX"); _fax = value; }
            get { return _fax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("EMAIL", DbType.String, 100)]
        public String EMAIL
        {
            set { AddAssigned("EMAIL"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("MASTERPNR", DbType.String, 10)]
        public String MASTERPNR
        {
            set { AddAssigned("MASTERPNR"); _masterpnr = value; }
            get { return _masterpnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("TOURCODE", DbType.String, 15)]
        public String TOURCODE
        {
            set { AddAssigned("TOURCODE"); _tourcode = value; }
            get { return _tourcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("CONTACTPERSON", DbType.String, 40)]
        public String CONTACTPERSON
        {
            set { AddAssigned("CONTACTPERSON"); _contactperson = value; }
            get { return _contactperson; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("TTLSELLCURR", DbType.StringFixedLength, 3)]
        public String TTLSELLCURR
        {
            set { AddAssigned("TTLSELLCURR"); _ttlsellcurr = value; }
            get { return _ttlsellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLSELLAMT", DbType.Decimal, 14)]
        public Decimal TTLSELLAMT
        {
            set { AddAssigned("TTLSELLAMT"); _ttlsellamt = value; }
            get { return _ttlsellamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("TTLSELLTAXCURR", DbType.StringFixedLength, 3)]
        public String TTLSELLTAXCURR
        {
            set { AddAssigned("TTLSELLTAXCURR"); _ttlselltaxcurr = value; }
            get { return _ttlselltaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLSELLTAX", DbType.Decimal, 14)]
        public Decimal TTLSELLTAX
        {
            set { AddAssigned("TTLSELLTAX"); _ttlselltax = value; }
            get { return _ttlselltax; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("TTLCOSTCURR", DbType.StringFixedLength, 3)]
        public String TTLCOSTCURR
        {
            set { AddAssigned("TTLCOSTCURR"); _ttlcostcurr = value; }
            get { return _ttlcostcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLCOSTAMT", DbType.Decimal, 14)]
        public Decimal TTLCOSTAMT
        {
            set { AddAssigned("TTLCOSTAMT"); _ttlcostamt = value; }
            get { return _ttlcostamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("TTLCOSTTAXCURR", DbType.StringFixedLength, 3)]
        public String TTLCOSTTAXCURR
        {
            set { AddAssigned("TTLCOSTTAXCURR"); _ttlcosttaxcurr = value; }
            get { return _ttlcosttaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLCOSTTAX", DbType.Decimal, 14)]
        public Decimal TTLCOSTTAX
        {
            set { AddAssigned("TTLCOSTTAX"); _ttlcosttax = value; }
            get { return _ttlcosttax; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("INVCURR", DbType.StringFixedLength, 3)]
        public String INVCURR
        {
            set { AddAssigned("INVCURR"); _invcurr = value; }
            get { return _invcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("INVAMT", DbType.Decimal, 14)]
        public Decimal INVAMT
        {
            set { AddAssigned("INVAMT"); _invamt = value; }
            get { return _invamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("INVTAXCURR", DbType.StringFixedLength, 3)]
        public String INVTAXCURR
        {
            set { AddAssigned("INVTAXCURR"); _invtaxcurr = value; }
            get { return _invtaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("INVTAX", DbType.Decimal, 14)]
        public Decimal INVTAX
        {
            set { AddAssigned("INVTAX"); _invtax = value; }
            get { return _invtax; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("DOCCURR", DbType.StringFixedLength, 3)]
        public String DOCCURR
        {
            set { AddAssigned("DOCCURR"); _doccurr = value; }
            get { return _doccurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("DOCAMT", DbType.Decimal, 14)]
        public Decimal DOCAMT
        {
            set { AddAssigned("DOCAMT"); _docamt = value; }
            get { return _docamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("DOCTAXCURR", DbType.StringFixedLength, 3)]
        public String DOCTAXCURR
        {
            set { AddAssigned("DOCTAXCURR"); _doctaxcurr = value; }
            get { return _doctaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("DOCTAX", DbType.Decimal, 14)]
        public Decimal DOCTAX
        {
            set { AddAssigned("DOCTAX"); _doctax = value; }
            get { return _doctax; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("INVCOUNT", DbType.Int32, 10)]
        public Int32 INVCOUNT
        {
            set { AddAssigned("INVCOUNT"); _invcount = value; }
            get { return _invcount; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("DOCCOUNT", DbType.Int32, 10)]
        public Int32 DOCCOUNT
        {
            set { AddAssigned("DOCCOUNT"); _doccount = value; }
            get { return _doccount; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 20)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 20)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK3", DbType.String, 20)]
        public String RMK3
        {
            set { AddAssigned("RMK3"); _rmk3 = value; }
            get { return _rmk3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK4", DbType.String, 20)]
        public String RMK4
        {
            set { AddAssigned("RMK4"); _rmk4 = value; }
            get { return _rmk4; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEADLINE", DbType.DateTime, 8)]
        public DateTime DEADLINE
        {
            set { AddAssigned("DEADLINE"); _deadline = value; }
            get { return _deadline; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCE", DbType.String, 10)]
        public String SOURCE
        {
            set { AddAssigned("SOURCE"); _source = value; }
            get { return _source; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD3", DbType.String, 20)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("INSFLAG", DbType.StringFixedLength, 1)]
        public String INSFLAG
        {
            set { AddAssigned("INSFLAG"); _insflag = value; }
            get { return _insflag; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("TOEMOSON", DbType.DateTime, 8)]
        public DateTime TOEMOSON
        {
            set { AddAssigned("TOEMOSON"); _toemoson = value; }
            get { return _toemoson; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("TOEMOSBY", DbType.String, 20)]
        public String TOEMOSBY
        {
            set { AddAssigned("TOEMOSBY"); _toemosby = value; }
            get { return _toemosby; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("EMOSBKGREF", DbType.String, 10)]
        public String EMOSBKGREF
        {
            set { AddAssigned("EMOSBKGREF"); _emosbkgref = value; }
            get { return _emosbkgref; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("IURFLAG", DbType.StringFixedLength, 1)]
        public String IURFLAG
        {
            set { AddAssigned("IURFLAG"); _iurflag = value; }
            get { return _iurflag; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("pseudo", DbType.String, 6)]
        public String pseudo
        {
            set { AddAssigned("pseudo"); _pseudo = value; }
            get { return _pseudo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("pseudo_inv", DbType.String, 6)]
        public String pseudo_inv
        {
            set { AddAssigned("pseudo_inv"); _pseudo_inv = value; }
            get { return _pseudo_inv; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CRSSINEIN_inv", DbType.String, 3)]
        public String CRSSINEIN_inv
        {
            set { AddAssigned("CRSSINEIN_inv"); _crssinein_inv = value; }
            get { return _crssinein_inv; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("agency_arc_iata_num", DbType.String, 10)]
        public String agency_arc_iata_num
        {
            set { AddAssigned("agency_arc_iata_num"); _agency_arc_iata_num = value; }
            get { return _agency_arc_iata_num; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("iss_off_code", DbType.String, 50)]
        public String iss_off_code
        {
            set { AddAssigned("iss_off_code"); _iss_off_code = value; }
            get { return _iss_off_code; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("tranflag", DbType.AnsiStringFixedLength, 1)]
        public String tranflag
        {
            set { AddAssigned("tranflag"); _tranflag = value; }
            get { return _tranflag; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOMSTR_IURs : BaseList<tbPEOMSTR_IUR, tbPEOMSTR_IURs> { }
    public class tbPEOMSTR_IURPage : PageResult<tbPEOMSTR_IUR, tbPEOMSTR_IURs> { }
}

