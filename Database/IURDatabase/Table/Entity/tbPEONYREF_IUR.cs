﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PEONYREF
    /// </summary>
    [Serializable]
    public class tbPEONYREF_IUR : BaseTable<tbPEONYREF_IUR>
    {
        public tbPEONYREF_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEONYREF";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            REFID,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            ACTIONCODE,
            /// <summary>
            ///
            /// </summary>
            CLTCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPCODE,
            /// <summary>
            ///
            /// </summary>
            DocNum,
            /// <summary>
            ///
            /// </summary>
            SellCurr,
            /// <summary>
            ///
            /// </summary>
            SellAmt,
            /// <summary>
            ///
            /// </summary>
            SellTaxCurr,
            /// <summary>
            ///
            /// </summary>
            SellTaxAmt,
            /// <summary>
            ///
            /// </summary>
            CostCurr,
            /// <summary>
            ///
            /// </summary>
            CostAmt,
            /// <summary>
            ///
            /// </summary>
            CostTaxCurr,
            /// <summary>
            ///
            /// </summary>
            CostTaxAmt,
            /// <summary>
            ///
            /// </summary>
            Description,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
        }

        #region Model
        private Decimal _refid;
        private String _companycode;
        private String _bkgref;
        private String _staffcode;
        private String _teamcode;
        private String _actioncode;
        private String _cltcode;
        private String _suppcode;
        private String _docnum;
        private String _sellcurr;
        private Decimal _sellamt;
        private String _selltaxcurr;
        private Decimal _selltaxamt;
        private String _costcurr;
        private Decimal _costamt;
        private String _costtaxcurr;
        private Decimal _costtaxamt;
        private String _description;
        private DateTime _createon;
        private String _createby;
        /// <summary>
        /// [Un-Null/decimal(13,0)]
        /// </summary>
        [FieldMapping("REFID", DbType.Decimal, 13, Enums.DataHandle.UnInsert, Enums.DataHandle.UnUpdate)]
        public Decimal REFID
        {
            set { AddAssigned("REFID"); _refid = value; }
            get { return _refid; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.StringFixedLength, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.StringFixedLength, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("ACTIONCODE", DbType.String, 5)]
        public String ACTIONCODE
        {
            set { AddAssigned("ACTIONCODE"); _actioncode = value; }
            get { return _actioncode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("CLTCODE", DbType.String, 8)]
        public String CLTCODE
        {
            set { AddAssigned("CLTCODE"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SUPPCODE", DbType.String, 8)]
        public String SUPPCODE
        {
            set { AddAssigned("SUPPCODE"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(13)]
        /// </summary>
        [FieldMapping("DocNum", DbType.String, 13)]
        public String DocNum
        {
            set { AddAssigned("DocNum"); _docnum = value; }
            get { return _docnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SellCurr", DbType.String, 3)]
        public String SellCurr
        {
            set { AddAssigned("SellCurr"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SellAmt", DbType.Decimal, 14)]
        public Decimal SellAmt
        {
            set { AddAssigned("SellAmt"); _sellamt = value; }
            get { return _sellamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SellTaxCurr", DbType.String, 3)]
        public String SellTaxCurr
        {
            set { AddAssigned("SellTaxCurr"); _selltaxcurr = value; }
            get { return _selltaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SellTaxAmt", DbType.Decimal, 14)]
        public Decimal SellTaxAmt
        {
            set { AddAssigned("SellTaxAmt"); _selltaxamt = value; }
            get { return _selltaxamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CostCurr", DbType.String, 3)]
        public String CostCurr
        {
            set { AddAssigned("CostCurr"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CostAmt", DbType.Decimal, 14)]
        public Decimal CostAmt
        {
            set { AddAssigned("CostAmt"); _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CostTaxCurr", DbType.String, 3)]
        public String CostTaxCurr
        {
            set { AddAssigned("CostTaxCurr"); _costtaxcurr = value; }
            get { return _costtaxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CostTaxAmt", DbType.Decimal, 14)]
        public Decimal CostTaxAmt
        {
            set { AddAssigned("CostTaxAmt"); _costtaxamt = value; }
            get { return _costtaxamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("Description", DbType.String, 100)]
        public String Description
        {
            set { AddAssigned("Description"); _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEONYREF_IURs : BaseList<tbPEONYREF_IUR, tbPEONYREF_IURs> { }
    public class tbPEONYREF_IURPage : PageResult<tbPEONYREF_IUR, tbPEONYREF_IURs> { }
}

