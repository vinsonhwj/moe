﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PEOPAX
    /// </summary>
    [Serializable]
    public class tbPEOPAX_IUR : BaseTable<tbPEOPAX_IUR>
    {
        public tbPEOPAX_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOPAX";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            PAXNUM,
            /// <summary>
            ///
            /// </summary>
            PAXLNAME,
            /// <summary>
            ///
            /// </summary>
            PAXFNAME,
            /// <summary>
            ///
            /// </summary>
            PAXTITLE,
            /// <summary>
            ///
            /// </summary>
            PAXTYPE,
            /// <summary>
            ///
            /// </summary>
            PAXAIR,
            /// <summary>
            ///
            /// </summary>
            PAXHOTEL,
            /// <summary>
            ///
            /// </summary>
            PAXOTHER,
            /// <summary>
            ///
            /// </summary>
            PAXAGE,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _paxnum;
        private String _paxlname;
        private String _paxfname;
        private String _paxtitle;
        private String _paxtype;
        private String _paxair;
        private String _paxhotel;
        private String _paxother;
        private Int16 _paxage;
        private String _rmk1;
        private String _rmk2;
        private String _voidon;
        private DateTime _voidby;
        private String _sourcesystem;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PAXNUM", DbType.String, 5)]
        public String PAXNUM
        {
            set { AddAssigned("PAXNUM"); _paxnum = value; }
            get { return _paxnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PAXLNAME", DbType.String, 20)]
        public String PAXLNAME
        {
            set { AddAssigned("PAXLNAME"); _paxlname = value; }
            get { return _paxlname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("PAXFNAME", DbType.String, 40)]
        public String PAXFNAME
        {
            set { AddAssigned("PAXFNAME"); _paxfname = value; }
            get { return _paxfname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("PAXTITLE", DbType.String, 4)]
        public String PAXTITLE
        {
            set { AddAssigned("PAXTITLE"); _paxtitle = value; }
            get { return _paxtitle; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXTYPE", DbType.String, 3)]
        public String PAXTYPE
        {
            set { AddAssigned("PAXTYPE"); _paxtype = value; }
            get { return _paxtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXAIR", DbType.String, 3)]
        public String PAXAIR
        {
            set { AddAssigned("PAXAIR"); _paxair = value; }
            get { return _paxair; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXHOTEL", DbType.String, 3)]
        public String PAXHOTEL
        {
            set { AddAssigned("PAXHOTEL"); _paxhotel = value; }
            get { return _paxhotel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXOTHER", DbType.String, 3)]
        public String PAXOTHER
        {
            set { AddAssigned("PAXOTHER"); _paxother = value; }
            get { return _paxother; }
        }
        /// <summary>
        /// [Allow Null/smallint(5)]
        /// </summary>
        [FieldMapping("PAXAGE", DbType.Int16, 5)]
        public Int16 PAXAGE
        {
            set { AddAssigned("PAXAGE"); _paxage = value; }
            get { return _paxage; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 20)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 20)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.StringFixedLength, 10)]
        public String VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.DateTime, 8)]
        public DateTime VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOPAX_IURs : BaseList<tbPEOPAX_IUR, tbPEOPAX_IURs> { }
    public class tbPEOPAX_IURPage : PageResult<tbPEOPAX_IUR, tbPEOPAX_IURs> { }
}

