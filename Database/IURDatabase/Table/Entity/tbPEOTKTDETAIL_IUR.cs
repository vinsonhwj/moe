﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.DAL.Database.IURDB.Table.Entity
{
    /// <summary>
    /// Table:PEOTKTDETAIL
    /// </summary>
    [Serializable]
    public class tbPEOTKTDETAIL_IUR : BaseTable<tbPEOTKTDETAIL_IUR>
    {
        public tbPEOTKTDETAIL_IUR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOTKTDETAIL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            Ticket,
            /// <summary>
            ///
            /// </summary>
            AirCode,
            /// <summary>
            ///
            /// </summary>
            TktSeq,
            /// <summary>
            ///
            /// </summary>
            SegNum,
            /// <summary>
            ///
            /// </summary>
            Airline,
            /// <summary>
            ///
            /// </summary>
            Flight,
            /// <summary>
            ///
            /// </summary>
            Class,
            /// <summary>
            ///
            /// </summary>
            Seat,
            /// <summary>
            ///
            /// </summary>
            DepartCity,
            /// <summary>
            ///
            /// </summary>
            ArrivalCity,
            /// <summary>
            ///
            /// </summary>
            DepartDATE,
            /// <summary>
            ///
            /// </summary>
            DepartTime,
            /// <summary>
            ///
            /// </summary>
            ArrDATE,
            /// <summary>
            ///
            /// </summary>
            ArrTime,
            /// <summary>
            ///
            /// </summary>
            ElapsedTime,
            /// <summary>
            ///
            /// </summary>
            StopOverNum,
            /// <summary>
            ///
            /// </summary>
            StopOverCity,
            /// <summary>
            ///
            /// </summary>
            FareAmt,
            /// <summary>
            ///
            /// </summary>
            FareBasic,
            /// <summary>
            ///
            /// </summary>
            Commission,
            /// <summary>
            ///
            /// </summary>
            ValidStart,
            /// <summary>
            ///
            /// </summary>
            ValidEnd,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
            /// <summary>
            ///
            /// </summary>
            CreateBy,
            /// <summary>
            ///
            /// </summary>
            UpdateOn,
            /// <summary>
            ///
            /// </summary>
            UpdateBy,
            /// <summary>
            ///
            /// </summary>
            EQP,
            /// <summary>
            ///
            /// </summary>
            Service,
            /// <summary>
            ///
            /// </summary>
            status,
            /// <summary>
            ///
            /// </summary>
            fare_basis_code,
            /// <summary>
            ///
            /// </summary>
            DepartTerm,
            /// <summary>
            ///
            /// </summary>
            ArrivalTerm,
            /// <summary>
            ///
            /// </summary>
            freq_fight_num,
            /// <summary>
            ///
            /// </summary>
            AirlinePNR,
        }

        #region Model
        private String _companycode;
        private String _ticket;
        private String _aircode;
        private String _tktseq;
        private String _segnum;
        private String _airline;
        private String _flight;
        private String _class;
        private String _seat;
        private String _departcity;
        private String _arrivalcity;
        private DateTime _departdate;
        private String _departtime;
        private DateTime _arrdate;
        private String _arrtime;
        private String _elapsedtime;
        private Int32 _stopovernum;
        private String _stopovercity;
        private Decimal _fareamt;
        private String _farebasic;
        private Decimal _commission;
        private DateTime _validstart;
        private DateTime _validend;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _eqp;
        private String _service;
        private String _status;
        private String _fare_basis_code;
        private String _departterm;
        private String _arrivalterm;
        private String _freq_fight_num;
        private String _airlinepnr;
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.String, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("Ticket", DbType.String, 10)]
        public String Ticket
        {
            set { AddAssigned("Ticket"); _ticket = value; }
            get { return _ticket; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AirCode", DbType.String, 3)]
        public String AirCode
        {
            set { AddAssigned("AirCode"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("TktSeq", DbType.String, 2)]
        public String TktSeq
        {
            set { AddAssigned("TktSeq"); _tktseq = value; }
            get { return _tktseq; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SegNum", DbType.String, 5)]
        public String SegNum
        {
            set { AddAssigned("SegNum"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Un-Null/nchar(10)]
        /// </summary>
        [FieldMapping("Airline", DbType.StringFixedLength, 10)]
        public String Airline
        {
            set { AddAssigned("Airline"); _airline = value; }
            get { return _airline; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("Flight", DbType.String, 6)]
        public String Flight
        {
            set { AddAssigned("Flight"); _flight = value; }
            get { return _flight; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("Class", DbType.String, 2)]
        public String Class
        {
            set { AddAssigned("Class"); _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("Seat", DbType.String, 4)]
        public String Seat
        {
            set { AddAssigned("Seat"); _seat = value; }
            get { return _seat; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("DepartCity", DbType.String, 3)]
        public String DepartCity
        {
            set { AddAssigned("DepartCity"); _departcity = value; }
            get { return _departcity; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("ArrivalCity", DbType.String, 3)]
        public String ArrivalCity
        {
            set { AddAssigned("ArrivalCity"); _arrivalcity = value; }
            get { return _arrivalcity; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("DepartDATE", DbType.DateTime, 8)]
        public DateTime DepartDATE
        {
            set { AddAssigned("DepartDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("DepartTime", DbType.String, 4)]
        public String DepartTime
        {
            set { AddAssigned("DepartTime"); _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("ArrDATE", DbType.DateTime, 8)]
        public DateTime ArrDATE
        {
            set { AddAssigned("ArrDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("ArrTime", DbType.String, 4)]
        public String ArrTime
        {
            set { AddAssigned("ArrTime"); _arrtime = value; }
            get { return _arrtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ElapsedTime", DbType.String, 10)]
        public String ElapsedTime
        {
            set { AddAssigned("ElapsedTime"); _elapsedtime = value; }
            get { return _elapsedtime; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("StopOverNum", DbType.Int32, 10)]
        public Int32 StopOverNum
        {
            set { AddAssigned("StopOverNum"); _stopovernum = value; }
            get { return _stopovernum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(16)]
        /// </summary>
        [FieldMapping("StopOverCity", DbType.String, 16)]
        public String StopOverCity
        {
            set { AddAssigned("StopOverCity"); _stopovercity = value; }
            get { return _stopovercity; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("FareAmt", DbType.Decimal, 14)]
        public Decimal FareAmt
        {
            set { AddAssigned("FareAmt"); _fareamt = value; }
            get { return _fareamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("FareBasic", DbType.String, 15)]
        public String FareBasic
        {
            set { AddAssigned("FareBasic"); _farebasic = value; }
            get { return _farebasic; }
        }
        /// <summary>
        /// [Allow Null/decimal(5,2)]
        /// </summary>
        [FieldMapping("Commission", DbType.Decimal, 3)]
        public Decimal Commission
        {
            set { AddAssigned("Commission"); _commission = value; }
            get { return _commission; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ValidStart", DbType.DateTime, 8)]
        public DateTime ValidStart
        {
            set { AddAssigned("ValidStart"); _validstart = value; }
            get { return _validstart; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ValidEnd", DbType.DateTime, 8)]
        public DateTime ValidEnd
        {
            set { AddAssigned("ValidEnd"); _validend = value; }
            get { return _validend; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CreateBy", DbType.String, 10)]
        public String CreateBy
        {
            set { AddAssigned("CreateBy"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UpdateOn", DbType.DateTime, 8)]
        public DateTime UpdateOn
        {
            set { AddAssigned("UpdateOn"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UpdateBy", DbType.String, 10)]
        public String UpdateBy
        {
            set { AddAssigned("UpdateBy"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("EQP", DbType.String, 3)]
        public String EQP
        {
            set { AddAssigned("EQP"); _eqp = value; }
            get { return _eqp; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("Service", DbType.String, 4)]
        public String Service
        {
            set { AddAssigned("Service"); _service = value; }
            get { return _service; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("status", DbType.String, 2)]
        public String status
        {
            set { AddAssigned("status"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("fare_basis_code", DbType.String, 15)]
        public String fare_basis_code
        {
            set { AddAssigned("fare_basis_code"); _fare_basis_code = value; }
            get { return _fare_basis_code; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DepartTerm", DbType.String, 30)]
        public String DepartTerm
        {
            set { AddAssigned("DepartTerm"); _departterm = value; }
            get { return _departterm; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("ArrivalTerm", DbType.String, 30)]
        public String ArrivalTerm
        {
            set { AddAssigned("ArrivalTerm"); _arrivalterm = value; }
            get { return _arrivalterm; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("freq_fight_num", DbType.String, 15)]
        public String freq_fight_num
        {
            set { AddAssigned("freq_fight_num"); _freq_fight_num = value; }
            get { return _freq_fight_num; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("AirlinePNR", DbType.String, 50)]
        public String AirlinePNR
        {
            set { AddAssigned("AirlinePNR"); _airlinepnr = value; }
            get { return _airlinepnr; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOTKTDETAIL_IURs : BaseList<tbPEOTKTDETAIL_IUR, tbPEOTKTDETAIL_IURs> { }
    public class tbPEOTKTDETAIL_IURPage : PageResult<tbPEOTKTDETAIL_IUR, tbPEOTKTDETAIL_IURs> { }
}

