﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.DALEntity.IUR.Table;

namespace Westminster.MOE.DAL.IUR.Table
{
    /// <summary>
    /// DataAccess [Table:IURLog]
    /// </summary>
    public partial class DAIURLog : BaseDAL<tbIURLog, tbIURLogs>
    {
        public DAIURLog(string connectionString)
            : base(connectionString)
        {
            TableName = tbIURLog.DBTableName;
        }


        public bool Add(tbIURLog entity)
        {
            return this.Add(entity);
        }

        public bool Update(tbIURLog updateEntity)
        {
            return this.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return this.Delete();
        }

        public tbIURLog GetEntity()
        {
            return this.GetEntity();
        }

        public tbIURLogs GetList()
        {
            return this.GetList();
        }

        public tbIURLogs GetList(int top)
        {
            return this.GetList(null, null, null, top);
        }

        public tbIURLogPage GetPage(int pageIndex, int pageSize)
        {
            int RecordCount;
            tbIURLogPage page = new tbIURLogPage();
            DisplayFields pk = new DisplayFields();
            page.PageSize = pageSize;
            page.Result = this.GetPage(null, null, null, pk, pageIndex, page.PageSize, out RecordCount);
            page.RecordCount = RecordCount;
            return page;
        }

    }
}

