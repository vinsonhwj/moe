﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOPAX]
    /// </summary>
    public partial class DAPEOPAX_IUR : DALDependency<tbPEOPAX_IUR, tbPEOPAX_IURs>
    {
        public DAPEOPAX_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOPAX_IUR.DBTableName;
        }


        public  bool Add(tbPEOPAX_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOPAX_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOPAX_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOPAX_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOPAX_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

        public tbPEOPAX_IURs GetListByBKGREF(string COMPANYCODE, string BKGREF)
        {
            FilterParams fp = new FilterParams();
            fp.AddParam(tbPEOPAX_IUR.Fields.COMPANYCODE, COMPANYCODE, Enums.Relation.Equal, Enums.Expression.AND);
            fp.AddParam(tbPEOPAX_IUR.Fields.BKGREF, BKGREF, Enums.Relation.Equal, Enums.Expression.AND);
            return base.GetList(null, fp);
        }

    }
}

