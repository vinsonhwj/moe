﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using hwj.DBUtility;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.IURDB.Table
{
    /// <summary>
    /// DataAccess [Table:PEOOTHERDETAIL]
    /// </summary>
    public partial class DAPEOOTHERDETAIL_IUR : DALDependency<tbPEOOTHERDETAIL_IUR, tbPEOOTHERDETAIL_IURs>
    {
        public DAPEOOTHERDETAIL_IUR(string connectionString)
            : base(connectionString)
        {
            TableName = tbPEOOTHERDETAIL_IUR.DBTableName;
        }


        public bool Add(tbPEOOTHERDETAIL_IUR entity)
        {
            return base.Add(entity);
        }

        public bool Update(tbPEOOTHERDETAIL_IUR updateEntity)
        {
            return base.Update(updateEntity, null);
        }

        public bool Delete()
        {
            return base.Delete();
        }

        public tbPEOOTHERDETAIL_IUR GetEntity()
        {
            return base.GetEntity();
        }

        public tbPEOOTHERDETAIL_IURs GetList()
        {
            return base.GetList();
        }

        public tbPEOOTHERDETAIL_IURs GetList(int top)
        {
            return base.GetList(null, null, null, top);
        }

    }
}

