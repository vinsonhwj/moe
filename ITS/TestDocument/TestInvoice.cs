﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using Westminster.MOE;
using Westminster.MOE.BLL;
using Westminster.MOE.DAL;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace NUnitTest.TestDocument
{
    [TestFixture]
    public class TestInvoice
    {
        public TestInvoice()
        {

        }

        [Test]
        public void CreateInvoice()
        {
            //var mockClass = new Mock<Westminster.MOE.BLL.Document.Invoice>();
            //mockClass.Setup(c => c.TeatMOQ()).Returns(true);
            //mockClass.Setup(c => c.CreateInvoice(It.IsAny<UIInvoice>())).Returns(false);

            Config config = new Config();
            config.DatabaseList.Add(EnumTypes.DALConnectionType.eMos, "Data Source=10.100.133.83;Initial Catalog=wtlemos;Persist Security Info=True;User ID=sa;Password=gzuat");
            InvoiceUIInfo uiInv = new InvoiceUIInfo();
            uiInv.BookingNum = "BKGNUM";
            uiInv.CltCode = "";

            //Westminster.MOE.BLL.Document.Invoice invExTest = new Westminster.MOE.BLL.Document.Invoice(config);
            //invExTest.CreateInvoice(uiInv);

            var mockConn = new Mock<Westminster.MOE.IDAL.Document.IDAInvoice>();

            mockConn.Setup(c => c.ExistCustomer(It.IsAny<String>(), It.IsAny<String>())).Returns(true);
            mockConn.Setup(c => c.ExistBookNum(It.IsAny<String>(), It.IsAny<String>())).Returns(true);

            Westminster.MOE.BLL.Document.Invoice inv = new Westminster.MOE.BLL.Document.Invoice(config, mockConn.Object);
            Assert.AreEqual(true, inv.CreateInvoice(uiInv), "Test Create Invoice");
        }

        [Test]
        public void Transfer()
        {
            try
            {
                Config config = new Config(true);
                Westminster.MOE.BLL.Transfer.LinkPNR link = new Westminster.MOE.BLL.Transfer.LinkPNR(config);
                link.DoWork("WM", "16GBBAGPVX", "BBHBHDWQKJ");
            }
            catch (Exception ex)
            {
                Exception e = ex;
            }
        }

    }
}
