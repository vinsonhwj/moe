﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;

namespace ITS.TestDocument
{
    public class TestVoucher
    {
        [Test]
        public void CheckInput()
        {
            Westminster.MOE.Entity.BLLEntity.UI.Document.Voucher.VCHSelected input = TestDocument.VoucherCase.VoucherInputCase.GetObj();
            Westminster.MOE.Entity.DALEntity.Document.Voucher.VCHPreviewDB db = TestDocument.VoucherCase.VoucherDBForPreviewCase.GetObj();
            Westminster.MOE.Lib.Config config = new Westminster.MOE.Lib.Config();

            var IVch = new Mock<Westminster.MOE.IDAL.Document.IDAVoucher>();
            var ICurr = new Mock<Westminster.MOE.IDAL.Maintain.IDACurrency>();

            IVch.Setup(c => c.GetDBForPreview(It.IsAny<Westminster.MOE.Entity.DALEntity.Document.Voucher.VCHSelectedKey>())).Returns(db);
            IVch.Setup(c => c.IsRestrictedSupplier(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            IVch.Setup(c => c.IsActiveSupplier(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            //IURConn.Setup(c => c.GetPaxList(It.IsAny<String>(), It.IsAny<String>())).Returns(new IURDB.Table.Entity.tbPEOPAXs());

            Westminster.MOE.BLL.Document.Voucher vch = new Westminster.MOE.BLL.Document.Voucher(config, IVch.Object, ICurr.Object);
            Westminster.MOE.Entity.BLLEntity.UI.Document.VCHPreview pv = vch.GetPreview(input);
        }
    }
}
