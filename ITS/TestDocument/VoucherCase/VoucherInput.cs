﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Voucher;

namespace ITS.TestDocument.VoucherCase
{
    public class VoucherInputCase
    {
        public static VCHSelected GetObj()
        {
            VCHSelected _VoucherInput = new VCHSelected();
            _VoucherInput.CompanyCode = @"WM";
            _VoucherInput.UserCode = @"CRW";
            _VoucherInput.BkgRef = @"000001689";
            _VoucherInput.Payment = @"Please Bill Agent";
            _VoucherInput.CurrCode = @"HKD";

            #region Passengers
            _VoucherInput.Passengers = new List<Passenger>();
            #region _Passengers_0
            Passenger _Passengers_0 = new Passenger();
            _Passengers_0.SegNum = @"00001";
            _VoucherInput.Passengers.Add(_Passengers_0);
            #endregion

            #region _Passengers_1
            Passenger _Passengers_1 = new Passenger();
            _Passengers_1.SegNum = @"00002";
            _VoucherInput.Passengers.Add(_Passengers_1);
            #endregion

            #region _Passengers_2
            Passenger _Passengers_2 = new Passenger();
            _Passengers_2.SegNum = @"00003";
            _VoucherInput.Passengers.Add(_Passengers_2);
            #endregion
            #endregion

            #region Segments
            _VoucherInput.Segments = new List<Segment>();

            #region _Segments_0
            Segment _Segments_0 = new Segment();
            _Segments_0.Type = Segment.SegmentType.Hotel;

            #region _Segments_0_Data
            Segment.SegmentKey _Segments_0_Data = new Segment.SegmentKey();
            _Segments_0_Data.SegNum = @"00003";
            _Segments_0.Data = _Segments_0_Data;
            #endregion
            _VoucherInput.Segments.Add(_Segments_0);

            #endregion
            #endregion

            return _VoucherInput;
        }
    }
}
