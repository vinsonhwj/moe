﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;

namespace ITS.TestDocument.VoucherCase
{
    public class VoucherInputCase
    {
        public static VoucherInput GetObj()
        {

            VoucherInput voucherinput_0 = new VoucherInput();
            voucherinput_0.CompanyCode = @"WM";
            voucherinput_0.UserCode = @"CRW";
            voucherinput_0.BkgRef = @"000001689";
            voucherinput_0.Payment = @"Please Bill Agent";
            voucherinput_0.CurrCode = @"HKD";
            #region Passengers
            voucherinput_0.Passengers = new List<Passenger>();
            #region passenger_1

            Passenger passenger_1 = new Passenger();
            passenger_1.SegNum = @"00001";

            voucherinput_0.Passengers.Add(passenger_1);
            #endregion
            #region passenger_2

            Passenger passenger_2 = new Passenger();
            passenger_2.SegNum = @"00002";

            voucherinput_0.Passengers.Add(passenger_2);
            #endregion
            #region passenger_3

            Passenger passenger_3 = new Passenger();
            passenger_3.SegNum = @"00003";

            voucherinput_0.Passengers.Add(passenger_3);
            #endregion
            #endregion
            #region Segments
            voucherinput_0.Segments = new List<Segment>();

            #region segment_4
            Segment segment_4 = new Segment();
            segment_4.Type = Segment.SegmentType.Hotel;

            #region segmentkey_5
            Segment.SegmentKey segmentkey_5 = new Segment.SegmentKey();
            segmentkey_5.SegNum = @"00004";
            segment_4.Data = segmentkey_5;            
            #endregion
            
            voucherinput_0.Segments.Add(segment_4);
            #endregion
            //#region segment_5
            //Segment segment_5 = new Segment();
            //segment_5.Type = Segment.SegmentType.Hotel;

            //#region segmentkey_6
            //Segment.SegmentKey segmentkey_6 = new Segment.SegmentKey();
            //segmentkey_6.SegNum = @"00005";
            //segment_5.Data = segmentkey_5;
            //#endregion

            //voucherinput_0.Segments.Add(segment_5);
            //#endregion
            #endregion

            return voucherinput_0;
        }
    }
}
