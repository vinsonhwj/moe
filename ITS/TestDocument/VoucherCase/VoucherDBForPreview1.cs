﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace ITS.TestDocument.VoucherCase
{
    public class VoucherDBForPreviewCase
    {
        public static VoucherDBForPreview GetObj()
        {

            VoucherDBForPreview voucherdbforpreview_0 = new VoucherDBForPreview();
            #region tbpeomstr_1

            tbPEOMSTR tbpeomstr_1 = new tbPEOMSTR();
            tbpeomstr_1.COMPANYCODE = @"WM";
            tbpeomstr_1.BKGREF = @"000001689";
            tbpeomstr_1.PNR = @"";
            tbpeomstr_1.CRSSINEIN = @"";
            #region COD1
            tbpeomstr_1.COD1 = null;
            #endregion
            #region COD2
            tbpeomstr_1.COD2 = null;
            #endregion
            #region COD3
            tbpeomstr_1.COD3 = null;
            #endregion
            tbpeomstr_1.DEADLINE = System.DateTime.Parse("2011/11/1 0:00:00");
            tbpeomstr_1.CLTODE = @"HA5004  ";
            tbpeomstr_1.CLTNAME = @"A TRAVEL LTD (TKG)";
            tbpeomstr_1.CLTADDR = @"";
            tbpeomstr_1.STAFFCODE = @"CRW       ";
            tbpeomstr_1.TEAMCODE = @"HSD  ";
            tbpeomstr_1.FIRSTPAX = @"CHEN JACK";
            tbpeomstr_1.PHONE = @"";
            tbpeomstr_1.FAX = @"";
            tbpeomstr_1.EMAIL = @"";
            tbpeomstr_1.MASTERPNR = @"000001689";
            tbpeomstr_1.TOURCODE = @"";
            tbpeomstr_1.CONTACTPERSON = @"";
            tbpeomstr_1.DEPARTDATE = System.DateTime.Parse("2012/2/29 15:29:13");
            tbpeomstr_1.TTLSELLCURR = @"HKD";
            tbpeomstr_1.TTLSELLAMT = 1000068464.00M;
            tbpeomstr_1.TTLSELLTAXCURR = @"HKD";
            tbpeomstr_1.TTLSELLTAX = 100003226.00M;
            tbpeomstr_1.TTLCOSTCURR = @"HKD";
            tbpeomstr_1.TTLCOSTAMT = 50445.23M;
            tbpeomstr_1.TTLCOSTTAXCURR = @"HKD";
            tbpeomstr_1.TTLCOSTTAX = 100003242.00M;
            tbpeomstr_1.INVCURR = @"HKD";
            tbpeomstr_1.INVAMT = 3809926.00M;
            tbpeomstr_1.INVTAXCURR = @"HKD";
            tbpeomstr_1.INVTAX = 180988.00M;
            tbpeomstr_1.DOCCURR = @"HKD";
            tbpeomstr_1.DOCAMT = 405952.11M;
            tbpeomstr_1.DOCTAXCURR = @"HKD";
            tbpeomstr_1.DOCTAX = 100031905.00M;
            tbpeomstr_1.TTLSELLGST = 0.00M;
            tbpeomstr_1.TTLCOSTGST = 0.00M;
            tbpeomstr_1.TTLINVGST = 0.00M;
            tbpeomstr_1.TTLDOCGST = 0.00M;
            tbpeomstr_1.INVCOUNT = 289;
            tbpeomstr_1.DOCCOUNT = 134;
            #region RMK1
            tbpeomstr_1.RMK1 = null;
            #endregion
            #region RMK2
            tbpeomstr_1.RMK2 = null;
            #endregion
            #region RMK3
            tbpeomstr_1.RMK3 = null;
            #endregion
            #region RMK4
            tbpeomstr_1.RMK4 = null;
            #endregion
            tbpeomstr_1.SOURCESYSTEM = @"EMOS";
            tbpeomstr_1.SOURCE = @"Agent";
            tbpeomstr_1.CREATEON = System.DateTime.Parse("2011/12/1 10:48:12");
            tbpeomstr_1.CREATEBY = @"CRW";
            tbpeomstr_1.UPDATEON = System.DateTime.Parse("2012/5/16 17:54:26");
            tbpeomstr_1.UPDATEBY = @"CRW";
            #region SOURCEREF
            tbpeomstr_1.SOURCEREF = null;
            #endregion
            #region pseudo
            tbpeomstr_1.pseudo = null;
            #endregion
            tbpeomstr_1.MstrRemark = @"";
            tbpeomstr_1.BookingNo = 0M;
            #region pseudo_inv
            tbpeomstr_1.pseudo_inv = null;
            #endregion
            #region CRSSINEIN_inv
            tbpeomstr_1.CRSSINEIN_inv = null;
            #endregion
            #region agency_arc_iata_num
            tbpeomstr_1.agency_arc_iata_num = null;
            #endregion
            #region iss_off_code
            tbpeomstr_1.iss_off_code = null;
            #endregion
            tbpeomstr_1.salesperson = @"CRW";
            #region ApproveBy
            tbpeomstr_1.ApproveBy = null;
            #endregion
            tbpeomstr_1.ApproveOn = DateTime.MinValue;
            tbpeomstr_1.ISTOUR = @"P";
            tbpeomstr_1.ANALYSISCODE = @"ABDBC";
            tbpeomstr_1.Total_Invoice_GST = 0M;
            tbpeomstr_1.Total_Credit_Note_GST = 0M;
            tbpeomstr_1.Total_Voucher_GST = 0M;
            #region Total_XO_GST
            tbpeomstr_1.Total_XO_GST = null;
            #endregion
            tbpeomstr_1.ProfileID = 1;
            tbpeomstr_1.ReptCurr = @"HKD";
            tbpeomstr_1.ReptAmt = -529634.00M;
            tbpeomstr_1.ReptTaxCurr = @"HKD";
            tbpeomstr_1.ReptTax = 0.00M;
            tbpeomstr_1.IsClosed = @"N";
            tbpeomstr_1.ReptCount = 85;

            voucherdbforpreview_0.Master = tbpeomstr_1;
            #endregion
            #region tbpeomstr_2

            tbPEOMSTR tbpeomstr_2 = new tbPEOMSTR();
            tbpeomstr_2.COMPANYCODE = @"WM";
            tbpeomstr_2.BKGREF = @"000001689";
            #region PNR
            tbpeomstr_2.PNR = null;
            #endregion
            #region CRSSINEIN
            tbpeomstr_2.CRSSINEIN = null;
            #endregion
            #region COD1
            tbpeomstr_2.COD1 = null;
            #endregion
            #region COD2
            tbpeomstr_2.COD2 = null;
            #endregion
            #region COD3
            tbpeomstr_2.COD3 = null;
            #endregion
            tbpeomstr_2.DEADLINE = DateTime.MinValue;
            #region CLTODE
            tbpeomstr_2.CLTODE = null;
            #endregion
            #region CLTNAME
            tbpeomstr_2.CLTNAME = null;
            #endregion
            #region CLTADDR
            tbpeomstr_2.CLTADDR = null;
            #endregion
            tbpeomstr_2.STAFFCODE = @"CRW       ";
            #region TEAMCODE
            tbpeomstr_2.TEAMCODE = null;
            #endregion
            #region FIRSTPAX
            tbpeomstr_2.FIRSTPAX = null;
            #endregion
            #region PHONE
            tbpeomstr_2.PHONE = null;
            #endregion
            #region FAX
            tbpeomstr_2.FAX = null;
            #endregion
            #region EMAIL
            tbpeomstr_2.EMAIL = null;
            #endregion
            tbpeomstr_2.MASTERPNR = @"000001689";
            #region TOURCODE
            tbpeomstr_2.TOURCODE = null;
            #endregion
            #region CONTACTPERSON
            tbpeomstr_2.CONTACTPERSON = null;
            #endregion
            tbpeomstr_2.DEPARTDATE = DateTime.MinValue;
            #region TTLSELLCURR
            tbpeomstr_2.TTLSELLCURR = null;
            #endregion
            tbpeomstr_2.TTLSELLAMT = 0M;
            #region TTLSELLTAXCURR
            tbpeomstr_2.TTLSELLTAXCURR = null;
            #endregion
            tbpeomstr_2.TTLSELLTAX = 0M;
            #region TTLCOSTCURR
            tbpeomstr_2.TTLCOSTCURR = null;
            #endregion
            tbpeomstr_2.TTLCOSTAMT = 50445.23M;
            #region TTLCOSTTAXCURR
            tbpeomstr_2.TTLCOSTTAXCURR = null;
            #endregion
            tbpeomstr_2.TTLCOSTTAX = 100003242.00M;
            #region INVCURR
            tbpeomstr_2.INVCURR = null;
            #endregion
            tbpeomstr_2.INVAMT = 3809926.00M;
            #region INVTAXCURR
            tbpeomstr_2.INVTAXCURR = null;
            #endregion
            tbpeomstr_2.INVTAX = 180988.00M;
            #region DOCCURR
            tbpeomstr_2.DOCCURR = null;
            #endregion
            tbpeomstr_2.DOCAMT = 405952.11M;
            #region DOCTAXCURR
            tbpeomstr_2.DOCTAXCURR = null;
            #endregion
            tbpeomstr_2.DOCTAX = 100031905.00M;
            tbpeomstr_2.TTLSELLGST = 0M;
            tbpeomstr_2.TTLCOSTGST = 0M;
            tbpeomstr_2.TTLINVGST = 0M;
            tbpeomstr_2.TTLDOCGST = 0M;
            tbpeomstr_2.INVCOUNT = 289;
            tbpeomstr_2.DOCCOUNT = 134;
            #region RMK1
            tbpeomstr_2.RMK1 = null;
            #endregion
            #region RMK2
            tbpeomstr_2.RMK2 = null;
            #endregion
            #region RMK3
            tbpeomstr_2.RMK3 = null;
            #endregion
            #region RMK4
            tbpeomstr_2.RMK4 = null;
            #endregion
            #region SOURCESYSTEM
            tbpeomstr_2.SOURCESYSTEM = null;
            #endregion
            #region SOURCE
            tbpeomstr_2.SOURCE = null;
            #endregion
            tbpeomstr_2.CREATEON = DateTime.MinValue;
            #region CREATEBY
            tbpeomstr_2.CREATEBY = null;
            #endregion
            tbpeomstr_2.UPDATEON = DateTime.MinValue;
            #region UPDATEBY
            tbpeomstr_2.UPDATEBY = null;
            #endregion
            #region SOURCEREF
            tbpeomstr_2.SOURCEREF = null;
            #endregion
            #region pseudo
            tbpeomstr_2.pseudo = null;
            #endregion
            #region MstrRemark
            tbpeomstr_2.MstrRemark = null;
            #endregion
            tbpeomstr_2.BookingNo = 0M;
            #region pseudo_inv
            tbpeomstr_2.pseudo_inv = null;
            #endregion
            #region CRSSINEIN_inv
            tbpeomstr_2.CRSSINEIN_inv = null;
            #endregion
            #region agency_arc_iata_num
            tbpeomstr_2.agency_arc_iata_num = null;
            #endregion
            #region iss_off_code
            tbpeomstr_2.iss_off_code = null;
            #endregion
            #region salesperson
            tbpeomstr_2.salesperson = null;
            #endregion
            #region ApproveBy
            tbpeomstr_2.ApproveBy = null;
            #endregion
            tbpeomstr_2.ApproveOn = DateTime.MinValue;
            #region ISTOUR
            tbpeomstr_2.ISTOUR = null;
            #endregion
            #region ANALYSISCODE
            tbpeomstr_2.ANALYSISCODE = null;
            #endregion
            tbpeomstr_2.Total_Invoice_GST = 0M;
            tbpeomstr_2.Total_Credit_Note_GST = 0M;
            tbpeomstr_2.Total_Voucher_GST = 0M;
            #region Total_XO_GST
            tbpeomstr_2.Total_XO_GST = null;
            #endregion
            tbpeomstr_2.ProfileID = 0;
            #region ReptCurr
            tbpeomstr_2.ReptCurr = null;
            #endregion
            tbpeomstr_2.ReptAmt = 0M;
            #region ReptTaxCurr
            tbpeomstr_2.ReptTaxCurr = null;
            #endregion
            tbpeomstr_2.ReptTax = 0M;
            #region IsClosed
            tbpeomstr_2.IsClosed = null;
            #endregion
            tbpeomstr_2.ReptCount = 0;

            voucherdbforpreview_0.PMQMaster = tbpeomstr_2;
            #endregion
            #region PMQCustomer
            voucherdbforpreview_0.PMQCustomer = null;
            #endregion
            #region tbpeostaff_3

            tbPEOSTAFF tbpeostaff_3 = new tbPEOSTAFF();
            tbpeostaff_3.COMPANYCODE = @"WM";
            tbpeostaff_3.STAFFCODE = @"CRW";
            tbpeostaff_3.BRANCHCODE = @"H";
            tbpeostaff_3.TEAMCODE = @"HSD";
            tbpeostaff_3.PHONE = @"()()23139972";
            tbpeostaff_3.JOBTITLE = @"PROJECT MANAGER";
            tbpeostaff_3.SINECODE = @"";
            tbpeostaff_3.UPLIMIT = 30.00M;
            tbpeostaff_3.LOWLIMIT = 10.00M;
            tbpeostaff_3.STAFFNAME = @"CHRIS WONG";
            #region STAFFNUM
            tbpeostaff_3.STAFFNUM = null;
            #endregion
            tbpeostaff_3.EFFECTIVEDATE = System.DateTime.Parse("2003/3/28 0:00:00");
            tbpeostaff_3.SUSPENDDATE = DateTime.MinValue;
            tbpeostaff_3.CREATEON = System.DateTime.Parse("2003/3/28 0:00:00");
            tbpeostaff_3.CREATEBY = @"ADMIN";
            tbpeostaff_3.UPDATEON = System.DateTime.Parse("2003/3/28 0:00:00");
            tbpeostaff_3.UPDATEBY = @"CRW";
            tbpeostaff_3.Sine = @"";
            tbpeostaff_3.CODE = @"";
            tbpeostaff_3.email = @"chris.wong@hkwtl.com";
            tbpeostaff_3.cltcode = @"HA0002    ";
            tbpeostaff_3.FullName = @"Tim";
            tbpeostaff_3.QBox = @"";
            tbpeostaff_3.CommLevel = @"10001";

            voucherdbforpreview_0.PMQStaff = tbpeostaff_3;
            #endregion
            #region Passengers
            voucherdbforpreview_0.Passengers = new tbPEOPAXs();
            #region tbpeopax_4

            tbPEOPAX tbpeopax_4 = new tbPEOPAX();
            tbpeopax_4.BKGREF = @"000001689";
            tbpeopax_4.COMPANYCODE = @"WM";
            tbpeopax_4.SEGNUM = @"00001";
            tbpeopax_4.PAXNUM = @"1";
            tbpeopax_4.SourcePnr = @"000001689";
            tbpeopax_4.PAXLNAME = @"CHEN";
            tbpeopax_4.PAXFNAME = @"JACK";
            tbpeopax_4.PAXTITLE = @"MR";
            tbpeopax_4.PAXTYPE = @"ADT";
            tbpeopax_4.PAXAIR = @"ADT";
            tbpeopax_4.PAXHOTEL = @"ADT";
            tbpeopax_4.PAXOTHER = @"ADT";
            tbpeopax_4.PAXAGE = System.Int16.Parse("30");
            #region RMK1
            tbpeopax_4.RMK1 = null;
            #endregion
            #region RMK2
            tbpeopax_4.RMK2 = null;
            #endregion
            #region VOIDON
            tbpeopax_4.VOIDON = null;
            #endregion
            tbpeopax_4.VOIDBY = DateTime.MinValue;
            tbpeopax_4.SOURCESYSTEM = @"EMOS";
            tbpeopax_4.CREATEON = System.DateTime.Parse("2011/12/1 10:48:13");
            tbpeopax_4.CREATEBY = @"CRW";
            tbpeopax_4.UPDATEON = DateTime.MinValue;
            #region UPDATEBY
            tbpeopax_4.UPDATEBY = null;
            #endregion
            #region Nationality
            tbpeopax_4.Nationality = null;
            #endregion
            #region PassportNo
            tbpeopax_4.PassportNo = null;
            #endregion
            tbpeopax_4.PassportExpireDate = DateTime.MinValue;
            tbpeopax_4.DateofBirth = DateTime.MinValue;
            #region Visa
            tbpeopax_4.Visa = null;
            #endregion
            #region ContactHomeTel
            tbpeopax_4.ContactHomeTel = null;
            #endregion
            #region ContactMobileTel
            tbpeopax_4.ContactMobileTel = null;
            #endregion
            #region ContactOfficeTel
            tbpeopax_4.ContactOfficeTel = null;
            #endregion
            #region Email
            tbpeopax_4.Email = null;
            #endregion
            #region Address1
            tbpeopax_4.Address1 = null;
            #endregion
            #region Address2
            tbpeopax_4.Address2 = null;
            #endregion
            #region Address3
            tbpeopax_4.Address3 = null;
            #endregion
            #region Address4
            tbpeopax_4.Address4 = null;
            #endregion
            #region Source
            tbpeopax_4.Source = null;
            #endregion
            #region SourceRef
            tbpeopax_4.SourceRef = null;
            #endregion
            #region MemberShipNo
            tbpeopax_4.MemberShipNo = null;
            #endregion
            tbpeopax_4.deleted = @"0";
            #region evisa_import
            tbpeopax_4.evisa_import = null;
            #endregion
            tbpeopax_4.evisa_import_datetime = DateTime.MinValue;
            #region SOURCETYPE
            tbpeopax_4.SOURCETYPE = null;
            #endregion
            #region ADCOMTXNO
            tbpeopax_4.ADCOMTXNO = null;
            #endregion
            tbpeopax_4.PAXNAME1 = @"";

            voucherdbforpreview_0.Passengers.Add(tbpeopax_4);
            #endregion
            #region tbpeopax_5

            tbPEOPAX tbpeopax_5 = new tbPEOPAX();
            tbpeopax_5.BKGREF = @"000001689";
            tbpeopax_5.COMPANYCODE = @"WM";
            tbpeopax_5.SEGNUM = @"00002";
            tbpeopax_5.PAXNUM = @"2";
            tbpeopax_5.SourcePnr = @"000001689";
            tbpeopax_5.PAXLNAME = @"LAM";
            tbpeopax_5.PAXFNAME = @"SUSIE";
            tbpeopax_5.PAXTITLE = @"MR";
            tbpeopax_5.PAXTYPE = @"ADT";
            tbpeopax_5.PAXAIR = @"ADT";
            tbpeopax_5.PAXHOTEL = @"ADT";
            tbpeopax_5.PAXOTHER = @"ADT";
            tbpeopax_5.PAXAGE = System.Int16.Parse("30");
            #region RMK1
            tbpeopax_5.RMK1 = null;
            #endregion
            #region RMK2
            tbpeopax_5.RMK2 = null;
            #endregion
            #region VOIDON
            tbpeopax_5.VOIDON = null;
            #endregion
            tbpeopax_5.VOIDBY = DateTime.MinValue;
            tbpeopax_5.SOURCESYSTEM = @"EMOS";
            tbpeopax_5.CREATEON = System.DateTime.Parse("2011/12/1 10:48:13");
            tbpeopax_5.CREATEBY = @"CRW";
            tbpeopax_5.UPDATEON = DateTime.MinValue;
            #region UPDATEBY
            tbpeopax_5.UPDATEBY = null;
            #endregion
            #region Nationality
            tbpeopax_5.Nationality = null;
            #endregion
            #region PassportNo
            tbpeopax_5.PassportNo = null;
            #endregion
            tbpeopax_5.PassportExpireDate = DateTime.MinValue;
            tbpeopax_5.DateofBirth = DateTime.MinValue;
            #region Visa
            tbpeopax_5.Visa = null;
            #endregion
            #region ContactHomeTel
            tbpeopax_5.ContactHomeTel = null;
            #endregion
            #region ContactMobileTel
            tbpeopax_5.ContactMobileTel = null;
            #endregion
            #region ContactOfficeTel
            tbpeopax_5.ContactOfficeTel = null;
            #endregion
            #region Email
            tbpeopax_5.Email = null;
            #endregion
            #region Address1
            tbpeopax_5.Address1 = null;
            #endregion
            #region Address2
            tbpeopax_5.Address2 = null;
            #endregion
            #region Address3
            tbpeopax_5.Address3 = null;
            #endregion
            #region Address4
            tbpeopax_5.Address4 = null;
            #endregion
            #region Source
            tbpeopax_5.Source = null;
            #endregion
            #region SourceRef
            tbpeopax_5.SourceRef = null;
            #endregion
            #region MemberShipNo
            tbpeopax_5.MemberShipNo = null;
            #endregion
            tbpeopax_5.deleted = @"0";
            #region evisa_import
            tbpeopax_5.evisa_import = null;
            #endregion
            tbpeopax_5.evisa_import_datetime = DateTime.MinValue;
            #region SOURCETYPE
            tbpeopax_5.SOURCETYPE = null;
            #endregion
            #region ADCOMTXNO
            tbpeopax_5.ADCOMTXNO = null;
            #endregion
            tbpeopax_5.PAXNAME1 = @"";

            voucherdbforpreview_0.Passengers.Add(tbpeopax_5);
            #endregion
            #region tbpeopax_6

            tbPEOPAX tbpeopax_6 = new tbPEOPAX();
            tbpeopax_6.BKGREF = @"000001689";
            tbpeopax_6.COMPANYCODE = @"WM";
            tbpeopax_6.SEGNUM = @"00003";
            tbpeopax_6.PAXNUM = @"3";
            tbpeopax_6.SourcePnr = @"000001689";
            tbpeopax_6.PAXLNAME = @"CHEN";
            tbpeopax_6.PAXFNAME = @"JAY";
            tbpeopax_6.PAXTITLE = @"INF";
            tbpeopax_6.PAXTYPE = @"INF";
            tbpeopax_6.PAXAIR = @"INF";
            tbpeopax_6.PAXHOTEL = @"INF";
            tbpeopax_6.PAXOTHER = @"INF";
            tbpeopax_6.PAXAGE = System.Int16.Parse("2");
            #region RMK1
            tbpeopax_6.RMK1 = null;
            #endregion
            #region RMK2
            tbpeopax_6.RMK2 = null;
            #endregion
            #region VOIDON
            tbpeopax_6.VOIDON = null;
            #endregion
            tbpeopax_6.VOIDBY = DateTime.MinValue;
            tbpeopax_6.SOURCESYSTEM = @"EMOS";
            tbpeopax_6.CREATEON = System.DateTime.Parse("2011/12/1 10:48:13");
            tbpeopax_6.CREATEBY = @"CRW";
            tbpeopax_6.UPDATEON = DateTime.MinValue;
            #region UPDATEBY
            tbpeopax_6.UPDATEBY = null;
            #endregion
            #region Nationality
            tbpeopax_6.Nationality = null;
            #endregion
            #region PassportNo
            tbpeopax_6.PassportNo = null;
            #endregion
            tbpeopax_6.PassportExpireDate = DateTime.MinValue;
            tbpeopax_6.DateofBirth = DateTime.MinValue;
            #region Visa
            tbpeopax_6.Visa = null;
            #endregion
            #region ContactHomeTel
            tbpeopax_6.ContactHomeTel = null;
            #endregion
            #region ContactMobileTel
            tbpeopax_6.ContactMobileTel = null;
            #endregion
            #region ContactOfficeTel
            tbpeopax_6.ContactOfficeTel = null;
            #endregion
            #region Email
            tbpeopax_6.Email = null;
            #endregion
            #region Address1
            tbpeopax_6.Address1 = null;
            #endregion
            #region Address2
            tbpeopax_6.Address2 = null;
            #endregion
            #region Address3
            tbpeopax_6.Address3 = null;
            #endregion
            #region Address4
            tbpeopax_6.Address4 = null;
            #endregion
            #region Source
            tbpeopax_6.Source = null;
            #endregion
            #region SourceRef
            tbpeopax_6.SourceRef = null;
            #endregion
            #region MemberShipNo
            tbpeopax_6.MemberShipNo = null;
            #endregion
            tbpeopax_6.deleted = @"0";
            #region evisa_import
            tbpeopax_6.evisa_import = null;
            #endregion
            tbpeopax_6.evisa_import_datetime = DateTime.MinValue;
            #region SOURCETYPE
            tbpeopax_6.SOURCETYPE = null;
            #endregion
            #region ADCOMTXNO
            tbpeopax_6.ADCOMTXNO = null;
            #endregion
            tbpeopax_6.PAXNAME1 = @"";

            voucherdbforpreview_0.Passengers.Add(tbpeopax_6);
            #endregion
            #endregion
            #region Hotels
            voucherdbforpreview_0.Hotels = new List<Westminster.MOE.Entity.DALEntity.Document.Hotel>();
            #region hotel_7

            Westminster.MOE.Entity.DALEntity.Document.Hotel hotel_7 = new Westminster.MOE.Entity.DALEntity.Document.Hotel();
            #region tbpeohotel_8

            tbPEOHOTEL tbpeohotel_8 = new tbPEOHOTEL();
            tbpeohotel_8.HOTELCODE = @"07868";
            tbpeohotel_8.HOTELNAME = @"181 Spa Apartments";
            tbpeohotel_8.CITYCODE = @"YIN";
            tbpeohotel_8.COUNTRYCODE = @"CN ";
            tbpeohotel_8.CHAINCODE = @"     ";
            tbpeohotel_8.CURRCODE = @"HKD";
            tbpeohotel_8.LOCATION = @"";
            tbpeohotel_8.CLASS = @"Moderate First Class          ";
            tbpeohotel_8.RANK = @"4    ";
            tbpeohotel_8.ADDR1 = @"181 The Esplanade,";
            tbpeohotel_8.ADDR2 = @"Cairns, QLD";
            tbpeohotel_8.ADDR3 = @"Australia";
            tbpeohotel_8.ADDR4 = @"";
            tbpeohotel_8.HTLDESC = @"                                                                                                                                                                                                                                                               ";
            tbpeohotel_8.CONTACT1 = @"";
            tbpeohotel_8.CONTACT2 = @"";
            tbpeohotel_8.DESIGNATION1 = @"";
            tbpeohotel_8.DESIGNATION2 = @"";
            tbpeohotel_8.STATUS = @"Active    ";
            tbpeohotel_8.LOCATIONDESC = @"                                                                                                                                                                                                                                                               ";
            tbpeohotel_8.TRANSPORTINFO = @"";
            tbpeohotel_8.WEBADDRESS = @"";
            tbpeohotel_8.GeneralPhone = @"( 61 )70-526860     ";
            tbpeohotel_8.RsvnPhone = @"";
            tbpeohotel_8.GeneralFax = @"( 61 )70-316227     ";
            tbpeohotel_8.RSVNFax = @"                    ";
            tbpeohotel_8.GeneralEmail = @"                                        ";
            tbpeohotel_8.RSVNEmail = @"                                        ";
            tbpeohotel_8.CREATEON = System.DateTime.Parse("2000/7/20 18:34:23");
            tbpeohotel_8.CREATEBY = @"XXX       ";
            tbpeohotel_8.UPDATEON = System.DateTime.Parse("2011/12/15 15:05:03");
            tbpeohotel_8.UPDATEBY = @"CRW       ";
            tbpeohotel_8.ROOMS = 0;
            tbpeohotel_8.FLOORS = 0;
            tbpeohotel_8.ABACUS_HOTELCODE = @"         ";
            tbpeohotel_8.ABACUS_VENDORCODE = @"  ";
            tbpeohotel_8.LOCATION_OLD = @"                                        ";
            tbpeohotel_8.HOTELNAME_OLD = @"181 Spa Apartments                                                                                  ";
            #region CRS
            tbpeohotel_8.CRS = null;
            #endregion
            tbpeohotel_8.POSTAL = @"";
            tbpeohotel_8.SuppCode = @"BKKAMA";
            tbpeohotel_8.HOTELNAME_ZHTW = @"繁體酒店名稱";
            tbpeohotel_8.HOTELNAME_ZHCN = @"181 遇事";
            tbpeohotel_8.Class_ZHTW = @"";
            tbpeohotel_8.Class_ZHCN = @"";
            tbpeohotel_8.HTLDESC_ZHTW = @"";
            tbpeohotel_8.HTLDESC_ZHCN = @"";
            tbpeohotel_8.LOCATIONDESC_ZHTW = @"";
            tbpeohotel_8.LOCATIONDESC_ZHCN = @"";
            tbpeohotel_8.ADDR1_ZHTW = @"181難得                                                                           ";
            tbpeohotel_8.ADDR2_ZHTW = @"海寧                                                                              ";
            tbpeohotel_8.ADDR3_ZHTW = @"澳大利亞                                                                            ";
            tbpeohotel_8.ADDR4_ZHTW = @"                                                                                ";
            tbpeohotel_8.ADDR1_ZHCN = @"181 难得                                                                          ";
            tbpeohotel_8.ADDR2_ZHCN = @"海宁                                                                              ";
            tbpeohotel_8.ADDR3_ZHCN = @"澳大利亚                                                                            ";
            tbpeohotel_8.ADDR4_ZHCN = @"                                                                                ";

            hotel_7.HotelInfo = tbpeohotel_8;
            #endregion
            #region bookinghotel_9

            Westminster.MOE.Entity.DALEntity.Document.BookingHotel bookinghotel_9 = new Westminster.MOE.Entity.DALEntity.Document.BookingHotel();
            #region tbpeohtl_10

            tbPEOHTL tbpeohtl_10 = new tbPEOHTL();
            tbpeohtl_10.BKGREF = @"000001689";
            tbpeohtl_10.COMPANYCODE = @"WM";
            tbpeohtl_10.SEGNUM = @"00003";
            #region SourcePnr
            tbpeohtl_10.SourcePnr = null;
            #endregion
            tbpeohtl_10.COUNTRYCODE = @"CN";
            tbpeohtl_10.CITYCODE = @"YIN";
            tbpeohtl_10.ADCOMTXNO = @"";
            tbpeohtl_10.ADCOMSEQNUM = @"";
            tbpeohtl_10.HOTELCODE = @"07868";
            tbpeohtl_10.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtl_10.ARRTIME = @"";
            tbpeohtl_10.DEPARTDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtl_10.DEPARTTIME = @"";
            tbpeohtl_10.ETA = @"Flight/";
            tbpeohtl_10.ETD = @"Flight/";
            tbpeohtl_10.SELLCURR = @"HKD";
            tbpeohtl_10.COSTCURR = @"HKD";
            tbpeohtl_10.TOTALCOST = 0.00M;
            tbpeohtl_10.TOTALSELL = 0.00M;
            tbpeohtl_10.TOTALCTAX = 0.00M;
            tbpeohtl_10.TOTALSTAX = 0.00M;
            tbpeohtl_10.FORMPAY = @"";
            tbpeohtl_10.RMK1 = @"";
            tbpeohtl_10.RMK2 = @"";
            tbpeohtl_10.INVRMK1 = @"";
            tbpeohtl_10.INVRMK2 = @"";
            tbpeohtl_10.VCHRMK1 = @"";
            tbpeohtl_10.VCHRMK2 = @"";
            tbpeohtl_10.WESTELNO = @"";
            tbpeohtl_10.SOURCESYSTEM = @"EMOS";
            tbpeohtl_10.SPCL_REQUEST = @"";
            tbpeohtl_10.HTLREFERENCE = @"";
            tbpeohtl_10.HTLCFmby = @"";
            #region VOIDBY
            tbpeohtl_10.VOIDBY = null;
            #endregion
            tbpeohtl_10.VOIDON = DateTime.MinValue;
            tbpeohtl_10.CREATEBY = @"CRW";
            tbpeohtl_10.CREATEON = System.DateTime.Parse("2011/12/21 15:34:29");
            tbpeohtl_10.UPDATEBY = @"CRW";
            tbpeohtl_10.UPDATEON = System.DateTime.Parse("2012/4/28 12:06:17");
            tbpeohtl_10.SOURCEREF = @"";
            tbpeohtl_10.GSTTAX = @"N";
            tbpeohtl_10.showDetailFee = @"N";
            tbpeohtl_10.SuppCode = @"BKKAMA";
            tbpeohtl_10.hotelVoucherType = @" ";
            tbpeohtl_10.COD1 = @"";
            tbpeohtl_10.COD2 = @"";
            tbpeohtl_10.COD3 = @"";
            tbpeohtl_10.roomTypeCode = @" ";
            tbpeohtl_10.fullRoomRate = 0.00M;
            tbpeohtl_10.dailyRate = 0.00M;
            tbpeohtl_10.lowRoomRate = 0.00M;
            tbpeohtl_10.RateTypeCode = @"";
            #region isRecalable
            tbpeohtl_10.isRecalable = null;
            #endregion
            tbpeohtl_10.ReasonCode = @"99";
            tbpeohtl_10.HOTELNAME = @"";
            tbpeohtl_10.WithBkf = @"B";
            tbpeohtl_10.WithTfr = @"R";

            bookinghotel_9.Header = tbpeohtl_10;
            #endregion
            #region Details
            bookinghotel_9.Details = new tbPeoHtlDetails();
            #region tbpeohtldetail_11

            tbPeoHtlDetail tbpeohtldetail_11 = new tbPeoHtlDetail();
            tbpeohtldetail_11.BKGREF = @"000001689";
            tbpeohtldetail_11.COMPANYCODE = @"WM";
            tbpeohtldetail_11.SEQTYPE = @"SELL";
            tbpeohtldetail_11.SEGNUM = @"00001";
            tbpeohtldetail_11.SEQNUM = @"00001";
            tbpeohtldetail_11.QTY = 3;
            tbpeohtldetail_11.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_11.DEPARTDATE = System.DateTime.Parse("2011/12/5 0:00:00");
            tbpeohtldetail_11.RMNTS = 4M;
            tbpeohtldetail_11.RATENAME = @"R";
            tbpeohtldetail_11.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_11.STATUS = @"KK";
            tbpeohtldetail_11.CURR = @"HKD";
            tbpeohtldetail_11.AMT = 5000.00M;
            tbpeohtldetail_11.TOTALAMT = 5200.00M;
            tbpeohtldetail_11.TAXCURR = @"HKD";
            tbpeohtldetail_11.TAXAMT = 200.00M;
            tbpeohtldetail_11.CREATEBY = @"CRW";
            tbpeohtldetail_11.CREATEON = System.DateTime.Parse("2011/12/1 10:52:09");
            tbpeohtldetail_11.UPDATEBY = @"";
            tbpeohtldetail_11.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_11.itemtype = @"S";
            tbpeohtldetail_11.AMTFEE = 0M;
            tbpeohtldetail_11.gsttype = @"  ";
            tbpeohtldetail_11.UnitPrice = 0.00M;
            tbpeohtldetail_11.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_11);
            #endregion
            #region tbpeohtldetail_12

            tbPeoHtlDetail tbpeohtldetail_12 = new tbPeoHtlDetail();
            tbpeohtldetail_12.BKGREF = @"000001689";
            tbpeohtldetail_12.COMPANYCODE = @"WM";
            tbpeohtldetail_12.SEQTYPE = @"COST";
            tbpeohtldetail_12.SEGNUM = @"00001";
            tbpeohtldetail_12.SEQNUM = @"00002";
            tbpeohtldetail_12.QTY = 3;
            tbpeohtldetail_12.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_12.DEPARTDATE = System.DateTime.Parse("2011/12/5 0:00:00");
            tbpeohtldetail_12.RMNTS = 4M;
            tbpeohtldetail_12.RATENAME = @"R";
            tbpeohtldetail_12.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_12.STATUS = @"KK";
            tbpeohtldetail_12.CURR = @"HKD";
            tbpeohtldetail_12.AMT = 4000.00M;
            tbpeohtldetail_12.TOTALAMT = 4200.00M;
            tbpeohtldetail_12.TAXCURR = @"HKD";
            tbpeohtldetail_12.TAXAMT = 200.00M;
            tbpeohtldetail_12.CREATEBY = @"CRW";
            tbpeohtldetail_12.CREATEON = System.DateTime.Parse("2011/12/1 10:52:09");
            tbpeohtldetail_12.UPDATEBY = @"";
            tbpeohtldetail_12.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_12.itemtype = @"S";
            tbpeohtldetail_12.AMTFEE = 0M;
            tbpeohtldetail_12.gsttype = @"  ";
            tbpeohtldetail_12.UnitPrice = 0.00M;
            tbpeohtldetail_12.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_12);
            #endregion
            #region tbpeohtldetail_13

            tbPeoHtlDetail tbpeohtldetail_13 = new tbPeoHtlDetail();
            tbpeohtldetail_13.BKGREF = @"000001689";
            tbpeohtldetail_13.COMPANYCODE = @"WM";
            tbpeohtldetail_13.SEQTYPE = @"SELL";
            tbpeohtldetail_13.SEGNUM = @"00001";
            tbpeohtldetail_13.SEQNUM = @"00003";
            tbpeohtldetail_13.QTY = 3;
            tbpeohtldetail_13.ARRDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_13.DEPARTDATE = System.DateTime.Parse("2011/12/4 0:00:00");
            tbpeohtldetail_13.RMNTS = 2M;
            tbpeohtldetail_13.RATENAME = @"R";
            tbpeohtldetail_13.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_13.STATUS = @"KK";
            tbpeohtldetail_13.CURR = @"HKD";
            tbpeohtldetail_13.AMT = 5500.00M;
            tbpeohtldetail_13.TOTALAMT = 5600.00M;
            tbpeohtldetail_13.TAXCURR = @"HKD";
            tbpeohtldetail_13.TAXAMT = 100.00M;
            tbpeohtldetail_13.CREATEBY = @"CRW";
            tbpeohtldetail_13.CREATEON = System.DateTime.Parse("2011/12/21 15:22:40");
            tbpeohtldetail_13.UPDATEBY = @"";
            tbpeohtldetail_13.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_13.itemtype = @"S";
            tbpeohtldetail_13.AMTFEE = 0M;
            tbpeohtldetail_13.gsttype = @"  ";
            tbpeohtldetail_13.UnitPrice = 0.00M;
            tbpeohtldetail_13.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_13);
            #endregion
            #region tbpeohtldetail_14

            tbPeoHtlDetail tbpeohtldetail_14 = new tbPeoHtlDetail();
            tbpeohtldetail_14.BKGREF = @"000001689";
            tbpeohtldetail_14.COMPANYCODE = @"WM";
            tbpeohtldetail_14.SEQTYPE = @"COST";
            tbpeohtldetail_14.SEGNUM = @"00001";
            tbpeohtldetail_14.SEQNUM = @"00004";
            tbpeohtldetail_14.QTY = 3;
            tbpeohtldetail_14.ARRDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_14.DEPARTDATE = System.DateTime.Parse("2011/12/4 0:00:00");
            tbpeohtldetail_14.RMNTS = 2M;
            tbpeohtldetail_14.RATENAME = @"R";
            tbpeohtldetail_14.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_14.STATUS = @"KK";
            tbpeohtldetail_14.CURR = @"HKD";
            tbpeohtldetail_14.AMT = 4400.00M;
            tbpeohtldetail_14.TOTALAMT = 4600.00M;
            tbpeohtldetail_14.TAXCURR = @"HKD";
            tbpeohtldetail_14.TAXAMT = 200.00M;
            tbpeohtldetail_14.CREATEBY = @"CRW";
            tbpeohtldetail_14.CREATEON = System.DateTime.Parse("2011/12/21 15:22:40");
            tbpeohtldetail_14.UPDATEBY = @"";
            tbpeohtldetail_14.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_14.itemtype = @"S";
            tbpeohtldetail_14.AMTFEE = 0M;
            tbpeohtldetail_14.gsttype = @"  ";
            tbpeohtldetail_14.UnitPrice = 0.00M;
            tbpeohtldetail_14.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_14);
            #endregion
            #region tbpeohtldetail_15

            tbPeoHtlDetail tbpeohtldetail_15 = new tbPeoHtlDetail();
            tbpeohtldetail_15.BKGREF = @"000001689";
            tbpeohtldetail_15.COMPANYCODE = @"WM";
            tbpeohtldetail_15.SEQTYPE = @"SELL";
            tbpeohtldetail_15.SEGNUM = @"00001";
            tbpeohtldetail_15.SEQNUM = @"00005";
            tbpeohtldetail_15.QTY = 3;
            tbpeohtldetail_15.ARRDATE = System.DateTime.Parse("2011/12/10 0:00:00");
            tbpeohtldetail_15.DEPARTDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_15.RMNTS = 3M;
            tbpeohtldetail_15.RATENAME = @"R";
            tbpeohtldetail_15.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_15.STATUS = @"KK";
            tbpeohtldetail_15.CURR = @"HKD";
            tbpeohtldetail_15.AMT = 5200.00M;
            tbpeohtldetail_15.TOTALAMT = 5300.00M;
            tbpeohtldetail_15.TAXCURR = @"HKD";
            tbpeohtldetail_15.TAXAMT = 100.00M;
            tbpeohtldetail_15.CREATEBY = @"CRW";
            tbpeohtldetail_15.CREATEON = System.DateTime.Parse("2011/12/21 15:43:39");
            tbpeohtldetail_15.UPDATEBY = @"";
            tbpeohtldetail_15.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_15.itemtype = @"S";
            tbpeohtldetail_15.AMTFEE = 0M;
            tbpeohtldetail_15.gsttype = @"  ";
            tbpeohtldetail_15.UnitPrice = 0.00M;
            tbpeohtldetail_15.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_15);
            #endregion
            #region tbpeohtldetail_16

            tbPeoHtlDetail tbpeohtldetail_16 = new tbPeoHtlDetail();
            tbpeohtldetail_16.BKGREF = @"000001689";
            tbpeohtldetail_16.COMPANYCODE = @"WM";
            tbpeohtldetail_16.SEQTYPE = @"COST";
            tbpeohtldetail_16.SEGNUM = @"00001";
            tbpeohtldetail_16.SEQNUM = @"00006";
            tbpeohtldetail_16.QTY = 3;
            tbpeohtldetail_16.ARRDATE = System.DateTime.Parse("2011/12/10 0:00:00");
            tbpeohtldetail_16.DEPARTDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_16.RMNTS = 3M;
            tbpeohtldetail_16.RATENAME = @"R";
            tbpeohtldetail_16.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_16.STATUS = @"KK";
            tbpeohtldetail_16.CURR = @"HKD";
            tbpeohtldetail_16.AMT = 4400.00M;
            tbpeohtldetail_16.TOTALAMT = 4600.00M;
            tbpeohtldetail_16.TAXCURR = @"HKD";
            tbpeohtldetail_16.TAXAMT = 200.00M;
            tbpeohtldetail_16.CREATEBY = @"CRW";
            tbpeohtldetail_16.CREATEON = System.DateTime.Parse("2011/12/21 15:43:39");
            tbpeohtldetail_16.UPDATEBY = @"";
            tbpeohtldetail_16.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_16.itemtype = @"S";
            tbpeohtldetail_16.AMTFEE = 0M;
            tbpeohtldetail_16.gsttype = @"  ";
            tbpeohtldetail_16.UnitPrice = 0.00M;
            tbpeohtldetail_16.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_16);
            #endregion
            #region tbpeohtldetail_17

            tbPeoHtlDetail tbpeohtldetail_17 = new tbPeoHtlDetail();
            tbpeohtldetail_17.BKGREF = @"000001689";
            tbpeohtldetail_17.COMPANYCODE = @"WM";
            tbpeohtldetail_17.SEQTYPE = @"SELL";
            tbpeohtldetail_17.SEGNUM = @"00001";
            tbpeohtldetail_17.SEQNUM = @"00007";
            tbpeohtldetail_17.QTY = 3;
            tbpeohtldetail_17.ARRDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_17.DEPARTDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_17.RMNTS = 4M;
            tbpeohtldetail_17.RATENAME = @"R";
            tbpeohtldetail_17.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_17.STATUS = @"KK";
            tbpeohtldetail_17.CURR = @"HKD";
            tbpeohtldetail_17.AMT = 5000.00M;
            tbpeohtldetail_17.TOTALAMT = 5200.00M;
            tbpeohtldetail_17.TAXCURR = @"HKD";
            tbpeohtldetail_17.TAXAMT = 200.00M;
            tbpeohtldetail_17.CREATEBY = @"CRW";
            tbpeohtldetail_17.CREATEON = System.DateTime.Parse("2011/12/21 16:32:45");
            tbpeohtldetail_17.UPDATEBY = @"";
            tbpeohtldetail_17.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_17.itemtype = @"S";
            tbpeohtldetail_17.AMTFEE = 0M;
            tbpeohtldetail_17.gsttype = @"  ";
            tbpeohtldetail_17.UnitPrice = 0.00M;
            tbpeohtldetail_17.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_17);
            #endregion
            #region tbpeohtldetail_18

            tbPeoHtlDetail tbpeohtldetail_18 = new tbPeoHtlDetail();
            tbpeohtldetail_18.BKGREF = @"000001689";
            tbpeohtldetail_18.COMPANYCODE = @"WM";
            tbpeohtldetail_18.SEQTYPE = @"COST";
            tbpeohtldetail_18.SEGNUM = @"00001";
            tbpeohtldetail_18.SEQNUM = @"00008";
            tbpeohtldetail_18.QTY = 3;
            tbpeohtldetail_18.ARRDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_18.DEPARTDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_18.RMNTS = 4M;
            tbpeohtldetail_18.RATENAME = @"R";
            tbpeohtldetail_18.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_18.STATUS = @"KK";
            tbpeohtldetail_18.CURR = @"HKD";
            tbpeohtldetail_18.AMT = 4000.00M;
            tbpeohtldetail_18.TOTALAMT = 4200.00M;
            tbpeohtldetail_18.TAXCURR = @"HKD";
            tbpeohtldetail_18.TAXAMT = 200.00M;
            tbpeohtldetail_18.CREATEBY = @"CRW";
            tbpeohtldetail_18.CREATEON = System.DateTime.Parse("2011/12/21 16:32:45");
            tbpeohtldetail_18.UPDATEBY = @"";
            tbpeohtldetail_18.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_18.itemtype = @"S";
            tbpeohtldetail_18.AMTFEE = 0M;
            tbpeohtldetail_18.gsttype = @"  ";
            tbpeohtldetail_18.UnitPrice = 0.00M;
            tbpeohtldetail_18.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_18);
            #endregion
            #region tbpeohtldetail_19

            tbPeoHtlDetail tbpeohtldetail_19 = new tbPeoHtlDetail();
            tbpeohtldetail_19.BKGREF = @"000001689";
            tbpeohtldetail_19.COMPANYCODE = @"WM";
            tbpeohtldetail_19.SEQTYPE = @"SELL";
            tbpeohtldetail_19.SEGNUM = @"00001";
            tbpeohtldetail_19.SEQNUM = @"00009";
            tbpeohtldetail_19.QTY = 3;
            tbpeohtldetail_19.ARRDATE = System.DateTime.Parse("2011/12/5 0:00:00");
            tbpeohtldetail_19.DEPARTDATE = System.DateTime.Parse("2011/12/6 0:00:00");
            tbpeohtldetail_19.RMNTS = 1M;
            tbpeohtldetail_19.RATENAME = @"R";
            tbpeohtldetail_19.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_19.STATUS = @"KK";
            tbpeohtldetail_19.CURR = @"HKD";
            tbpeohtldetail_19.AMT = 1000.00M;
            tbpeohtldetail_19.TOTALAMT = 1200.00M;
            tbpeohtldetail_19.TAXCURR = @"HKD";
            tbpeohtldetail_19.TAXAMT = 200.00M;
            tbpeohtldetail_19.CREATEBY = @"CRW";
            tbpeohtldetail_19.CREATEON = System.DateTime.Parse("2011/12/23 12:16:21");
            tbpeohtldetail_19.UPDATEBY = @"";
            tbpeohtldetail_19.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_19.itemtype = @"S";
            tbpeohtldetail_19.AMTFEE = 0M;
            tbpeohtldetail_19.gsttype = @"  ";
            tbpeohtldetail_19.UnitPrice = 0.00M;
            tbpeohtldetail_19.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_19);
            #endregion
            #region tbpeohtldetail_20

            tbPeoHtlDetail tbpeohtldetail_20 = new tbPeoHtlDetail();
            tbpeohtldetail_20.BKGREF = @"000001689";
            tbpeohtldetail_20.COMPANYCODE = @"WM";
            tbpeohtldetail_20.SEQTYPE = @"COST";
            tbpeohtldetail_20.SEGNUM = @"00001";
            tbpeohtldetail_20.SEQNUM = @"00010";
            tbpeohtldetail_20.QTY = 3;
            tbpeohtldetail_20.ARRDATE = System.DateTime.Parse("2011/12/5 0:00:00");
            tbpeohtldetail_20.DEPARTDATE = System.DateTime.Parse("2011/12/6 0:00:00");
            tbpeohtldetail_20.RMNTS = 1M;
            tbpeohtldetail_20.RATENAME = @"R";
            tbpeohtldetail_20.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_20.STATUS = @"KK";
            tbpeohtldetail_20.CURR = @"HKD";
            tbpeohtldetail_20.AMT = 400.00M;
            tbpeohtldetail_20.TOTALAMT = 420.00M;
            tbpeohtldetail_20.TAXCURR = @"HKD";
            tbpeohtldetail_20.TAXAMT = 20.00M;
            tbpeohtldetail_20.CREATEBY = @"CRW";
            tbpeohtldetail_20.CREATEON = System.DateTime.Parse("2011/12/23 12:16:21");
            tbpeohtldetail_20.UPDATEBY = @"";
            tbpeohtldetail_20.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_20.itemtype = @"S";
            tbpeohtldetail_20.AMTFEE = 0M;
            tbpeohtldetail_20.gsttype = @"  ";
            tbpeohtldetail_20.UnitPrice = 0.00M;
            tbpeohtldetail_20.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_20);
            #endregion
            #region tbpeohtldetail_21

            tbPeoHtlDetail tbpeohtldetail_21 = new tbPeoHtlDetail();
            tbpeohtldetail_21.BKGREF = @"000001689";
            tbpeohtldetail_21.COMPANYCODE = @"WM";
            tbpeohtldetail_21.SEQTYPE = @"SELL";
            tbpeohtldetail_21.SEGNUM = @"00001";
            tbpeohtldetail_21.SEQNUM = @"00011";
            tbpeohtldetail_21.QTY = 2;
            tbpeohtldetail_21.ARRDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_21.DEPARTDATE = System.DateTime.Parse("2011/12/4 0:00:00");
            tbpeohtldetail_21.RMNTS = 2M;
            tbpeohtldetail_21.RATENAME = @"";
            tbpeohtldetail_21.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_21.STATUS = @"KK";
            tbpeohtldetail_21.CURR = @"HKD";
            tbpeohtldetail_21.AMT = 4500.00M;
            tbpeohtldetail_21.TOTALAMT = 4600.00M;
            tbpeohtldetail_21.TAXCURR = @"HKD";
            tbpeohtldetail_21.TAXAMT = 100.00M;
            tbpeohtldetail_21.CREATEBY = @"CRW";
            tbpeohtldetail_21.CREATEON = System.DateTime.Parse("2011/12/26 9:53:25");
            tbpeohtldetail_21.UPDATEBY = @"";
            tbpeohtldetail_21.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_21.itemtype = @"S";
            tbpeohtldetail_21.AMTFEE = 0M;
            tbpeohtldetail_21.gsttype = @"  ";
            tbpeohtldetail_21.UnitPrice = 0.00M;
            tbpeohtldetail_21.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_21);
            #endregion
            #region tbpeohtldetail_22

            tbPeoHtlDetail tbpeohtldetail_22 = new tbPeoHtlDetail();
            tbpeohtldetail_22.BKGREF = @"000001689";
            tbpeohtldetail_22.COMPANYCODE = @"WM";
            tbpeohtldetail_22.SEQTYPE = @"COST";
            tbpeohtldetail_22.SEGNUM = @"00001";
            tbpeohtldetail_22.SEQNUM = @"00012";
            tbpeohtldetail_22.QTY = 2;
            tbpeohtldetail_22.ARRDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_22.DEPARTDATE = System.DateTime.Parse("2011/12/4 0:00:00");
            tbpeohtldetail_22.RMNTS = 2M;
            tbpeohtldetail_22.RATENAME = @"";
            tbpeohtldetail_22.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_22.STATUS = @"KK";
            tbpeohtldetail_22.CURR = @"HKD";
            tbpeohtldetail_22.AMT = 3200.00M;
            tbpeohtldetail_22.TOTALAMT = 3300.00M;
            tbpeohtldetail_22.TAXCURR = @"HKD";
            tbpeohtldetail_22.TAXAMT = 100.00M;
            tbpeohtldetail_22.CREATEBY = @"CRW";
            tbpeohtldetail_22.CREATEON = System.DateTime.Parse("2011/12/26 9:53:25");
            tbpeohtldetail_22.UPDATEBY = @"";
            tbpeohtldetail_22.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_22.itemtype = @"S";
            tbpeohtldetail_22.AMTFEE = 0M;
            tbpeohtldetail_22.gsttype = @"  ";
            tbpeohtldetail_22.UnitPrice = 0.00M;
            tbpeohtldetail_22.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_22);
            #endregion
            #region tbpeohtldetail_23

            tbPeoHtlDetail tbpeohtldetail_23 = new tbPeoHtlDetail();
            tbpeohtldetail_23.BKGREF = @"000001689";
            tbpeohtldetail_23.COMPANYCODE = @"WM";
            tbpeohtldetail_23.SEQTYPE = @"SELL";
            tbpeohtldetail_23.SEGNUM = @"00001";
            tbpeohtldetail_23.SEQNUM = @"00013";
            tbpeohtldetail_23.QTY = 3;
            tbpeohtldetail_23.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_23.DEPARTDATE = System.DateTime.Parse("2011/12/3 0:00:00");
            tbpeohtldetail_23.RMNTS = 2M;
            tbpeohtldetail_23.RATENAME = @"";
            tbpeohtldetail_23.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_23.STATUS = @"KK";
            tbpeohtldetail_23.CURR = @"HKD";
            tbpeohtldetail_23.AMT = 4500.00M;
            tbpeohtldetail_23.TOTALAMT = 4520.00M;
            tbpeohtldetail_23.TAXCURR = @"HKD";
            tbpeohtldetail_23.TAXAMT = 20.00M;
            tbpeohtldetail_23.CREATEBY = @"CRW";
            tbpeohtldetail_23.CREATEON = System.DateTime.Parse("2011/12/26 10:57:59");
            tbpeohtldetail_23.UPDATEBY = @"";
            tbpeohtldetail_23.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_23.itemtype = @"S";
            tbpeohtldetail_23.AMTFEE = 0M;
            tbpeohtldetail_23.gsttype = @"  ";
            tbpeohtldetail_23.UnitPrice = 0.00M;
            tbpeohtldetail_23.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_23);
            #endregion
            #region tbpeohtldetail_24

            tbPeoHtlDetail tbpeohtldetail_24 = new tbPeoHtlDetail();
            tbpeohtldetail_24.BKGREF = @"000001689";
            tbpeohtldetail_24.COMPANYCODE = @"WM";
            tbpeohtldetail_24.SEQTYPE = @"COST";
            tbpeohtldetail_24.SEGNUM = @"00001";
            tbpeohtldetail_24.SEQNUM = @"00014";
            tbpeohtldetail_24.QTY = 3;
            tbpeohtldetail_24.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_24.DEPARTDATE = System.DateTime.Parse("2011/12/3 0:00:00");
            tbpeohtldetail_24.RMNTS = 2M;
            tbpeohtldetail_24.RATENAME = @"";
            tbpeohtldetail_24.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_24.STATUS = @"KK";
            tbpeohtldetail_24.CURR = @"HKD";
            tbpeohtldetail_24.AMT = 3300.00M;
            tbpeohtldetail_24.TOTALAMT = 3400.00M;
            tbpeohtldetail_24.TAXCURR = @"HKD";
            tbpeohtldetail_24.TAXAMT = 100.00M;
            tbpeohtldetail_24.CREATEBY = @"CRW";
            tbpeohtldetail_24.CREATEON = System.DateTime.Parse("2011/12/26 10:57:59");
            tbpeohtldetail_24.UPDATEBY = @"";
            tbpeohtldetail_24.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_24.itemtype = @"S";
            tbpeohtldetail_24.AMTFEE = 0M;
            tbpeohtldetail_24.gsttype = @"  ";
            tbpeohtldetail_24.UnitPrice = 0.00M;
            tbpeohtldetail_24.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_24);
            #endregion
            #region tbpeohtldetail_25

            tbPeoHtlDetail tbpeohtldetail_25 = new tbPeoHtlDetail();
            tbpeohtldetail_25.BKGREF = @"000001689";
            tbpeohtldetail_25.COMPANYCODE = @"WM";
            tbpeohtldetail_25.SEQTYPE = @"SELL";
            tbpeohtldetail_25.SEGNUM = @"00001";
            tbpeohtldetail_25.SEQNUM = @"00015";
            tbpeohtldetail_25.QTY = 3;
            tbpeohtldetail_25.ARRDATE = System.DateTime.Parse("2011/12/3 0:00:00");
            tbpeohtldetail_25.DEPARTDATE = System.DateTime.Parse("2011/12/6 0:00:00");
            tbpeohtldetail_25.RMNTS = 3M;
            tbpeohtldetail_25.RATENAME = @"";
            tbpeohtldetail_25.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_25.STATUS = @"KK";
            tbpeohtldetail_25.CURR = @"HKD";
            tbpeohtldetail_25.AMT = 4500.00M;
            tbpeohtldetail_25.TOTALAMT = 4520.00M;
            tbpeohtldetail_25.TAXCURR = @"HKD";
            tbpeohtldetail_25.TAXAMT = 20.00M;
            tbpeohtldetail_25.CREATEBY = @"CRW";
            tbpeohtldetail_25.CREATEON = System.DateTime.Parse("2011/12/26 10:57:59");
            tbpeohtldetail_25.UPDATEBY = @"";
            tbpeohtldetail_25.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_25.itemtype = @"S";
            tbpeohtldetail_25.AMTFEE = 0M;
            tbpeohtldetail_25.gsttype = @"  ";
            tbpeohtldetail_25.UnitPrice = 0.00M;
            tbpeohtldetail_25.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_25);
            #endregion
            #region tbpeohtldetail_26

            tbPeoHtlDetail tbpeohtldetail_26 = new tbPeoHtlDetail();
            tbpeohtldetail_26.BKGREF = @"000001689";
            tbpeohtldetail_26.COMPANYCODE = @"WM";
            tbpeohtldetail_26.SEQTYPE = @"COST";
            tbpeohtldetail_26.SEGNUM = @"00001";
            tbpeohtldetail_26.SEQNUM = @"00016";
            tbpeohtldetail_26.QTY = 3;
            tbpeohtldetail_26.ARRDATE = System.DateTime.Parse("2011/12/3 0:00:00");
            tbpeohtldetail_26.DEPARTDATE = System.DateTime.Parse("2011/12/6 0:00:00");
            tbpeohtldetail_26.RMNTS = 3M;
            tbpeohtldetail_26.RATENAME = @"";
            tbpeohtldetail_26.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_26.STATUS = @"KK";
            tbpeohtldetail_26.CURR = @"HKD";
            tbpeohtldetail_26.AMT = 3300.00M;
            tbpeohtldetail_26.TOTALAMT = 3400.00M;
            tbpeohtldetail_26.TAXCURR = @"HKD";
            tbpeohtldetail_26.TAXAMT = 100.00M;
            tbpeohtldetail_26.CREATEBY = @"CRW";
            tbpeohtldetail_26.CREATEON = System.DateTime.Parse("2011/12/26 10:57:59");
            tbpeohtldetail_26.UPDATEBY = @"";
            tbpeohtldetail_26.UPDATEON = System.DateTime.Parse("2012/5/17 16:17:32");
            tbpeohtldetail_26.itemtype = @"S";
            tbpeohtldetail_26.AMTFEE = 0M;
            tbpeohtldetail_26.gsttype = @"  ";
            tbpeohtldetail_26.UnitPrice = 0.00M;
            tbpeohtldetail_26.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_26);
            #endregion
            #region tbpeohtldetail_27

            tbPeoHtlDetail tbpeohtldetail_27 = new tbPeoHtlDetail();
            tbpeohtldetail_27.BKGREF = @"000001689";
            tbpeohtldetail_27.COMPANYCODE = @"WM";
            tbpeohtldetail_27.SEQTYPE = @"SELL";
            tbpeohtldetail_27.SEGNUM = @"00002";
            tbpeohtldetail_27.SEQNUM = @"00001";
            tbpeohtldetail_27.QTY = 0;
            tbpeohtldetail_27.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_27.DEPARTDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_27.RMNTS = 1M;
            tbpeohtldetail_27.RATENAME = @"";
            tbpeohtldetail_27.ITEMNAME = @"";
            tbpeohtldetail_27.STATUS = @"";
            tbpeohtldetail_27.CURR = @"HKD";
            tbpeohtldetail_27.AMT = 500.00M;
            tbpeohtldetail_27.TOTALAMT = 510.00M;
            tbpeohtldetail_27.TAXCURR = @"HKD";
            tbpeohtldetail_27.TAXAMT = 10.00M;
            tbpeohtldetail_27.CREATEBY = @"CRW";
            tbpeohtldetail_27.CREATEON = System.DateTime.Parse("2011/12/15 15:04:53");
            #region UPDATEBY
            tbpeohtldetail_27.UPDATEBY = null;
            #endregion
            tbpeohtldetail_27.UPDATEON = DateTime.MinValue;
            tbpeohtldetail_27.itemtype = @"S";
            tbpeohtldetail_27.AMTFEE = 0M;
            tbpeohtldetail_27.gsttype = @"  ";
            tbpeohtldetail_27.UnitPrice = 0M;
            tbpeohtldetail_27.UnitTax = 0M;

            bookinghotel_9.Details.Add(tbpeohtldetail_27);
            #endregion
            #region tbpeohtldetail_28

            tbPeoHtlDetail tbpeohtldetail_28 = new tbPeoHtlDetail();
            tbpeohtldetail_28.BKGREF = @"000001689";
            tbpeohtldetail_28.COMPANYCODE = @"WM";
            tbpeohtldetail_28.SEQTYPE = @"COST";
            tbpeohtldetail_28.SEGNUM = @"00002";
            tbpeohtldetail_28.SEQNUM = @"00002";
            tbpeohtldetail_28.QTY = 0;
            tbpeohtldetail_28.ARRDATE = System.DateTime.Parse("2011/12/1 0:00:00");
            tbpeohtldetail_28.DEPARTDATE = System.DateTime.Parse("2011/12/2 0:00:00");
            tbpeohtldetail_28.RMNTS = 1M;
            tbpeohtldetail_28.RATENAME = @"";
            tbpeohtldetail_28.ITEMNAME = @"";
            tbpeohtldetail_28.STATUS = @"";
            tbpeohtldetail_28.CURR = @"HKD";
            tbpeohtldetail_28.AMT = 400.00M;
            tbpeohtldetail_28.TOTALAMT = 410.00M;
            tbpeohtldetail_28.TAXCURR = @"HKD";
            tbpeohtldetail_28.TAXAMT = 10.00M;
            tbpeohtldetail_28.CREATEBY = @"CRW";
            tbpeohtldetail_28.CREATEON = System.DateTime.Parse("2011/12/15 15:04:53");
            #region UPDATEBY
            tbpeohtldetail_28.UPDATEBY = null;
            #endregion
            tbpeohtldetail_28.UPDATEON = DateTime.MinValue;
            tbpeohtldetail_28.itemtype = @"S";
            tbpeohtldetail_28.AMTFEE = 0M;
            tbpeohtldetail_28.gsttype = @"  ";
            tbpeohtldetail_28.UnitPrice = 0M;
            tbpeohtldetail_28.UnitTax = 0M;

            bookinghotel_9.Details.Add(tbpeohtldetail_28);
            #endregion
            #region tbpeohtldetail_29

            tbPeoHtlDetail tbpeohtldetail_29 = new tbPeoHtlDetail();
            tbpeohtldetail_29.BKGREF = @"000001689";
            tbpeohtldetail_29.COMPANYCODE = @"WM";
            tbpeohtldetail_29.SEQTYPE = @"SELL";
            tbpeohtldetail_29.SEGNUM = @"00003";
            tbpeohtldetail_29.SEQNUM = @"00001";
            tbpeohtldetail_29.QTY = 0;
            tbpeohtldetail_29.ARRDATE = System.DateTime.Parse("2011/12/8 0:00:00");
            tbpeohtldetail_29.DEPARTDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_29.RMNTS = 5M;
            tbpeohtldetail_29.RATENAME = @"R";
            tbpeohtldetail_29.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_29.STATUS = @"KK";
            tbpeohtldetail_29.CURR = @"HKD";
            tbpeohtldetail_29.AMT = 0.00M;
            tbpeohtldetail_29.TOTALAMT = 0.00M;
            tbpeohtldetail_29.TAXCURR = @"HKD";
            tbpeohtldetail_29.TAXAMT = 0.00M;
            tbpeohtldetail_29.CREATEBY = @"CRW";
            tbpeohtldetail_29.CREATEON = System.DateTime.Parse("2011/12/21 15:34:29");
            tbpeohtldetail_29.UPDATEBY = @"";
            tbpeohtldetail_29.UPDATEON = System.DateTime.Parse("2012/4/28 12:06:17");
            tbpeohtldetail_29.itemtype = @"S";
            tbpeohtldetail_29.AMTFEE = 0M;
            tbpeohtldetail_29.gsttype = @"  ";
            tbpeohtldetail_29.UnitPrice = 0.00M;
            tbpeohtldetail_29.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_29);
            #endregion
            #region tbpeohtldetail_30

            tbPeoHtlDetail tbpeohtldetail_30 = new tbPeoHtlDetail();
            tbpeohtldetail_30.BKGREF = @"000001689";
            tbpeohtldetail_30.COMPANYCODE = @"WM";
            tbpeohtldetail_30.SEQTYPE = @"COST";
            tbpeohtldetail_30.SEGNUM = @"00003";
            tbpeohtldetail_30.SEQNUM = @"00002";
            tbpeohtldetail_30.QTY = 0;
            tbpeohtldetail_30.ARRDATE = System.DateTime.Parse("2011/12/8 0:00:00");
            tbpeohtldetail_30.DEPARTDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            tbpeohtldetail_30.RMNTS = 5M;
            tbpeohtldetail_30.RATENAME = @"R";
            tbpeohtldetail_30.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_30.STATUS = @"KK";
            tbpeohtldetail_30.CURR = @"HKD";
            tbpeohtldetail_30.AMT = 0.00M;
            tbpeohtldetail_30.TOTALAMT = 0.00M;
            tbpeohtldetail_30.TAXCURR = @"HKD";
            tbpeohtldetail_30.TAXAMT = 0.00M;
            tbpeohtldetail_30.CREATEBY = @"CRW";
            tbpeohtldetail_30.CREATEON = System.DateTime.Parse("2011/12/21 15:34:29");
            tbpeohtldetail_30.UPDATEBY = @"";
            tbpeohtldetail_30.UPDATEON = System.DateTime.Parse("2012/4/28 12:06:17");
            tbpeohtldetail_30.itemtype = @"S";
            tbpeohtldetail_30.AMTFEE = 0M;
            tbpeohtldetail_30.gsttype = @"  ";
            tbpeohtldetail_30.UnitPrice = 0.00M;
            tbpeohtldetail_30.UnitTax = 0.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_30);
            #endregion
            #region tbpeohtldetail_31

            tbPeoHtlDetail tbpeohtldetail_31 = new tbPeoHtlDetail();
            tbpeohtldetail_31.BKGREF = @"000001689";
            tbpeohtldetail_31.COMPANYCODE = @"WM";
            tbpeohtldetail_31.SEQTYPE = @"SELL";
            tbpeohtldetail_31.SEGNUM = @"00004";
            tbpeohtldetail_31.SEQNUM = @"00001";
            tbpeohtldetail_31.QTY = 3;
            tbpeohtldetail_31.ARRDATE = System.DateTime.Parse("2011/12/14 0:00:00");
            tbpeohtldetail_31.DEPARTDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_31.RMNTS = 3M;
            tbpeohtldetail_31.RATENAME = @"R";
            tbpeohtldetail_31.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_31.STATUS = @"KK";
            tbpeohtldetail_31.CURR = @"HKD";
            tbpeohtldetail_31.AMT = 1200.00M;
            tbpeohtldetail_31.TOTALAMT = 1500.00M;
            tbpeohtldetail_31.TAXCURR = @"HKD";
            tbpeohtldetail_31.TAXAMT = 300.00M;
            tbpeohtldetail_31.CREATEBY = @"CRW";
            tbpeohtldetail_31.CREATEON = System.DateTime.Parse("2011/12/21 15:37:43");
            tbpeohtldetail_31.UPDATEBY = @"";
            tbpeohtldetail_31.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_31.itemtype = @"S";
            tbpeohtldetail_31.AMTFEE = 0M;
            tbpeohtldetail_31.gsttype = @"  ";
            tbpeohtldetail_31.UnitPrice = 200.00M;
            tbpeohtldetail_31.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_31);
            #endregion
            #region tbpeohtldetail_32

            tbPeoHtlDetail tbpeohtldetail_32 = new tbPeoHtlDetail();
            tbpeohtldetail_32.BKGREF = @"000001689";
            tbpeohtldetail_32.COMPANYCODE = @"WM";
            tbpeohtldetail_32.SEQTYPE = @"COST";
            tbpeohtldetail_32.SEGNUM = @"00004";
            tbpeohtldetail_32.SEQNUM = @"00002";
            tbpeohtldetail_32.QTY = 3;
            tbpeohtldetail_32.ARRDATE = System.DateTime.Parse("2011/12/14 0:00:00");
            tbpeohtldetail_32.DEPARTDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_32.RMNTS = 3M;
            tbpeohtldetail_32.RATENAME = @"R";
            tbpeohtldetail_32.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_32.STATUS = @"KK";
            tbpeohtldetail_32.CURR = @"HKD";
            tbpeohtldetail_32.AMT = 600.00M;
            tbpeohtldetail_32.TOTALAMT = 900.00M;
            tbpeohtldetail_32.TAXCURR = @"HKD";
            tbpeohtldetail_32.TAXAMT = 300.00M;
            tbpeohtldetail_32.CREATEBY = @"CRW";
            tbpeohtldetail_32.CREATEON = System.DateTime.Parse("2011/12/21 15:37:43");
            tbpeohtldetail_32.UPDATEBY = @"";
            tbpeohtldetail_32.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_32.itemtype = @"S";
            tbpeohtldetail_32.AMTFEE = 0M;
            tbpeohtldetail_32.gsttype = @"  ";
            tbpeohtldetail_32.UnitPrice = 100.00M;
            tbpeohtldetail_32.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_32);
            #endregion
            #region tbpeohtldetail_33

            tbPeoHtlDetail tbpeohtldetail_33 = new tbPeoHtlDetail();
            tbpeohtldetail_33.BKGREF = @"000001689";
            tbpeohtldetail_33.COMPANYCODE = @"WM";
            tbpeohtldetail_33.SEQTYPE = @"SELL";
            tbpeohtldetail_33.SEGNUM = @"00004";
            tbpeohtldetail_33.SEQNUM = @"00003";
            tbpeohtldetail_33.QTY = 3;
            tbpeohtldetail_33.ARRDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_33.DEPARTDATE = System.DateTime.Parse("2011/12/21 0:00:00");
            tbpeohtldetail_33.RMNTS = 4M;
            tbpeohtldetail_33.RATENAME = @"R";
            tbpeohtldetail_33.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_33.STATUS = @"KK";
            tbpeohtldetail_33.CURR = @"HKD";
            tbpeohtldetail_33.AMT = 4500.00M;
            tbpeohtldetail_33.TOTALAMT = 4680.00M;
            tbpeohtldetail_33.TAXCURR = @"HKD";
            tbpeohtldetail_33.TAXAMT = 180.00M;
            tbpeohtldetail_33.CREATEBY = @"CRW";
            tbpeohtldetail_33.CREATEON = System.DateTime.Parse("2012/3/16 9:39:52");
            tbpeohtldetail_33.UPDATEBY = @"";
            tbpeohtldetail_33.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_33.itemtype = @"S";
            tbpeohtldetail_33.AMTFEE = 0M;
            tbpeohtldetail_33.gsttype = @"  ";
            tbpeohtldetail_33.UnitPrice = 500.00M;
            tbpeohtldetail_33.UnitTax = 20.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_33);
            #endregion
            #region tbpeohtldetail_34

            tbPeoHtlDetail tbpeohtldetail_34 = new tbPeoHtlDetail();
            tbpeohtldetail_34.BKGREF = @"000001689";
            tbpeohtldetail_34.COMPANYCODE = @"WM";
            tbpeohtldetail_34.SEQTYPE = @"COST";
            tbpeohtldetail_34.SEGNUM = @"00004";
            tbpeohtldetail_34.SEQNUM = @"00004";
            tbpeohtldetail_34.QTY = 3;
            tbpeohtldetail_34.ARRDATE = System.DateTime.Parse("2011/12/17 0:00:00");
            tbpeohtldetail_34.DEPARTDATE = System.DateTime.Parse("2011/12/21 0:00:00");
            tbpeohtldetail_34.RMNTS = 4M;
            tbpeohtldetail_34.RATENAME = @"R";
            tbpeohtldetail_34.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_34.STATUS = @"KK";
            tbpeohtldetail_34.CURR = @"HKD";
            tbpeohtldetail_34.AMT = 1800.00M;
            tbpeohtldetail_34.TOTALAMT = 2250.00M;
            tbpeohtldetail_34.TAXCURR = @"HKD";
            tbpeohtldetail_34.TAXAMT = 450.00M;
            tbpeohtldetail_34.CREATEBY = @"CRW";
            tbpeohtldetail_34.CREATEON = System.DateTime.Parse("2012/3/16 9:39:52");
            tbpeohtldetail_34.UPDATEBY = @"";
            tbpeohtldetail_34.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_34.itemtype = @"S";
            tbpeohtldetail_34.AMTFEE = 0M;
            tbpeohtldetail_34.gsttype = @"  ";
            tbpeohtldetail_34.UnitPrice = 200.00M;
            tbpeohtldetail_34.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_34);
            #endregion
            #region tbpeohtldetail_35

            tbPeoHtlDetail tbpeohtldetail_35 = new tbPeoHtlDetail();
            tbpeohtldetail_35.BKGREF = @"000001689";
            tbpeohtldetail_35.COMPANYCODE = @"WM";
            tbpeohtldetail_35.SEQTYPE = @"SELL";
            tbpeohtldetail_35.SEGNUM = @"00004";
            tbpeohtldetail_35.SEQNUM = @"00005";
            tbpeohtldetail_35.QTY = 3;
            tbpeohtldetail_35.ARRDATE = System.DateTime.Parse("2011/12/21 0:00:00");
            tbpeohtldetail_35.DEPARTDATE = System.DateTime.Parse("2011/12/26 0:00:00");
            tbpeohtldetail_35.RMNTS = 5M;
            tbpeohtldetail_35.RATENAME = @"R";
            tbpeohtldetail_35.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_35.STATUS = @"KK";
            tbpeohtldetail_35.CURR = @"HKD";
            tbpeohtldetail_35.AMT = 5400.00M;
            tbpeohtldetail_35.TOTALAMT = 6000.00M;
            tbpeohtldetail_35.TAXCURR = @"HKD";
            tbpeohtldetail_35.TAXAMT = 600.00M;
            tbpeohtldetail_35.CREATEBY = @"CRW";
            tbpeohtldetail_35.CREATEON = System.DateTime.Parse("2012/3/16 9:39:52");
            tbpeohtldetail_35.UPDATEBY = @"";
            tbpeohtldetail_35.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_35.itemtype = @"S";
            tbpeohtldetail_35.AMTFEE = 0M;
            tbpeohtldetail_35.gsttype = @"  ";
            tbpeohtldetail_35.UnitPrice = 450.00M;
            tbpeohtldetail_35.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_35);
            #endregion
            #region tbpeohtldetail_36

            tbPeoHtlDetail tbpeohtldetail_36 = new tbPeoHtlDetail();
            tbpeohtldetail_36.BKGREF = @"000001689";
            tbpeohtldetail_36.COMPANYCODE = @"WM";
            tbpeohtldetail_36.SEQTYPE = @"COST";
            tbpeohtldetail_36.SEGNUM = @"00004";
            tbpeohtldetail_36.SEQNUM = @"00006";
            tbpeohtldetail_36.QTY = 3;
            tbpeohtldetail_36.ARRDATE = System.DateTime.Parse("2011/12/21 0:00:00");
            tbpeohtldetail_36.DEPARTDATE = System.DateTime.Parse("2011/12/26 0:00:00");
            tbpeohtldetail_36.RMNTS = 5M;
            tbpeohtldetail_36.RATENAME = @"R";
            tbpeohtldetail_36.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_36.STATUS = @"KK";
            tbpeohtldetail_36.CURR = @"HKD";
            tbpeohtldetail_36.AMT = 1200.00M;
            tbpeohtldetail_36.TOTALAMT = 1800.00M;
            tbpeohtldetail_36.TAXCURR = @"HKD";
            tbpeohtldetail_36.TAXAMT = 600.00M;
            tbpeohtldetail_36.CREATEBY = @"CRW";
            tbpeohtldetail_36.CREATEON = System.DateTime.Parse("2012/3/16 9:39:52");
            tbpeohtldetail_36.UPDATEBY = @"";
            tbpeohtldetail_36.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_36.itemtype = @"S";
            tbpeohtldetail_36.AMTFEE = 0M;
            tbpeohtldetail_36.gsttype = @"  ";
            tbpeohtldetail_36.UnitPrice = 100.00M;
            tbpeohtldetail_36.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_36);
            #endregion
            #region tbpeohtldetail_37

            tbPeoHtlDetail tbpeohtldetail_37 = new tbPeoHtlDetail();
            tbpeohtldetail_37.BKGREF = @"000001689";
            tbpeohtldetail_37.COMPANYCODE = @"WM";
            tbpeohtldetail_37.SEQTYPE = @"SELL";
            tbpeohtldetail_37.SEGNUM = @"00004";
            tbpeohtldetail_37.SEQNUM = @"00007";
            tbpeohtldetail_37.QTY = 3;
            tbpeohtldetail_37.ARRDATE = System.DateTime.Parse("2011/12/22 0:00:00");
            tbpeohtldetail_37.DEPARTDATE = System.DateTime.Parse("2011/12/27 0:00:00");
            tbpeohtldetail_37.RMNTS = 5M;
            tbpeohtldetail_37.RATENAME = @"R";
            tbpeohtldetail_37.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_37.STATUS = @"KK";
            tbpeohtldetail_37.CURR = @"HKD";
            tbpeohtldetail_37.AMT = 4500.00M;
            tbpeohtldetail_37.TOTALAMT = 5250.00M;
            tbpeohtldetail_37.TAXCURR = @"HKD";
            tbpeohtldetail_37.TAXAMT = 750.00M;
            tbpeohtldetail_37.CREATEBY = @"CRW";
            tbpeohtldetail_37.CREATEON = System.DateTime.Parse("2012/3/16 9:42:21");
            tbpeohtldetail_37.UPDATEBY = @"";
            tbpeohtldetail_37.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_37.itemtype = @"S";
            tbpeohtldetail_37.AMTFEE = 0M;
            tbpeohtldetail_37.gsttype = @"  ";
            tbpeohtldetail_37.UnitPrice = 300.00M;
            tbpeohtldetail_37.UnitTax = 50.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_37);
            #endregion
            #region tbpeohtldetail_38

            tbPeoHtlDetail tbpeohtldetail_38 = new tbPeoHtlDetail();
            tbpeohtldetail_38.BKGREF = @"000001689";
            tbpeohtldetail_38.COMPANYCODE = @"WM";
            tbpeohtldetail_38.SEQTYPE = @"COST";
            tbpeohtldetail_38.SEGNUM = @"00004";
            tbpeohtldetail_38.SEQNUM = @"00008";
            tbpeohtldetail_38.QTY = 3;
            tbpeohtldetail_38.ARRDATE = System.DateTime.Parse("2011/12/22 0:00:00");
            tbpeohtldetail_38.DEPARTDATE = System.DateTime.Parse("2011/12/27 0:00:00");
            tbpeohtldetail_38.RMNTS = 5M;
            tbpeohtldetail_38.RATENAME = @"R";
            tbpeohtldetail_38.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_38.STATUS = @"KK";
            tbpeohtldetail_38.CURR = @"HKD";
            tbpeohtldetail_38.AMT = 3900.00M;
            tbpeohtldetail_38.TOTALAMT = 4350.00M;
            tbpeohtldetail_38.TAXCURR = @"HKD";
            tbpeohtldetail_38.TAXAMT = 450.00M;
            tbpeohtldetail_38.CREATEBY = @"CRW";
            tbpeohtldetail_38.CREATEON = System.DateTime.Parse("2012/3/16 9:42:21");
            tbpeohtldetail_38.UPDATEBY = @"";
            tbpeohtldetail_38.UPDATEON = System.DateTime.Parse("2012/3/16 16:33:37");
            tbpeohtldetail_38.itemtype = @"S";
            tbpeohtldetail_38.AMTFEE = 0M;
            tbpeohtldetail_38.gsttype = @"  ";
            tbpeohtldetail_38.UnitPrice = 260.00M;
            tbpeohtldetail_38.UnitTax = 30.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_38);
            #endregion
            #region tbpeohtldetail_39

            tbPeoHtlDetail tbpeohtldetail_39 = new tbPeoHtlDetail();
            tbpeohtldetail_39.BKGREF = @"000001689";
            tbpeohtldetail_39.COMPANYCODE = @"WM";
            tbpeohtldetail_39.SEQTYPE = @"SELL";
            tbpeohtldetail_39.SEGNUM = @"00005";
            tbpeohtldetail_39.SEQNUM = @"00001";
            tbpeohtldetail_39.QTY = 3;
            tbpeohtldetail_39.ARRDATE = System.DateTime.Parse("2012/2/29 15:29:13");
            tbpeohtldetail_39.DEPARTDATE = System.DateTime.Parse("2012/3/3 15:29:13");
            tbpeohtldetail_39.RMNTS = 3M;
            tbpeohtldetail_39.RATENAME = @"";
            tbpeohtldetail_39.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_39.STATUS = @"KK";
            tbpeohtldetail_39.CURR = @"HKD";
            tbpeohtldetail_39.AMT = 4680.00M;
            tbpeohtldetail_39.TOTALAMT = 4950.00M;
            tbpeohtldetail_39.TAXCURR = @"HKD";
            tbpeohtldetail_39.TAXAMT = 270.00M;
            tbpeohtldetail_39.CREATEBY = @"CRW";
            tbpeohtldetail_39.CREATEON = System.DateTime.Parse("2012/3/22 10:26:27");
            tbpeohtldetail_39.UPDATEBY = @"";
            tbpeohtldetail_39.UPDATEON = System.DateTime.Parse("2012/3/28 15:46:13");
            tbpeohtldetail_39.itemtype = @"S";
            tbpeohtldetail_39.AMTFEE = 0M;
            tbpeohtldetail_39.gsttype = @"  ";
            tbpeohtldetail_39.UnitPrice = 520.00M;
            tbpeohtldetail_39.UnitTax = 30.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_39);
            #endregion
            #region tbpeohtldetail_40

            tbPeoHtlDetail tbpeohtldetail_40 = new tbPeoHtlDetail();
            tbpeohtldetail_40.BKGREF = @"000001689";
            tbpeohtldetail_40.COMPANYCODE = @"WM";
            tbpeohtldetail_40.SEQTYPE = @"COST";
            tbpeohtldetail_40.SEGNUM = @"00005";
            tbpeohtldetail_40.SEQNUM = @"00002";
            tbpeohtldetail_40.QTY = 3;
            tbpeohtldetail_40.ARRDATE = System.DateTime.Parse("2012/2/29 15:29:13");
            tbpeohtldetail_40.DEPARTDATE = System.DateTime.Parse("2012/3/3 15:29:13");
            tbpeohtldetail_40.RMNTS = 3M;
            tbpeohtldetail_40.RATENAME = @"";
            tbpeohtldetail_40.ITEMNAME = @"ECONMIC";
            tbpeohtldetail_40.STATUS = @"KK";
            tbpeohtldetail_40.CURR = @"HKD";
            tbpeohtldetail_40.AMT = 4050.00M;
            tbpeohtldetail_40.TOTALAMT = 4250.00M;
            tbpeohtldetail_40.TAXCURR = @"HKD";
            tbpeohtldetail_40.TAXAMT = 200.00M;
            tbpeohtldetail_40.CREATEBY = @"CRW";
            tbpeohtldetail_40.CREATEON = System.DateTime.Parse("2012/3/22 10:26:27");
            tbpeohtldetail_40.UPDATEBY = @"";
            tbpeohtldetail_40.UPDATEON = System.DateTime.Parse("2012/3/28 15:46:13");
            tbpeohtldetail_40.itemtype = @"S";
            tbpeohtldetail_40.AMTFEE = 0M;
            tbpeohtldetail_40.gsttype = @"  ";
            tbpeohtldetail_40.UnitPrice = 450.00M;
            tbpeohtldetail_40.UnitTax = 45.00M;

            bookinghotel_9.Details.Add(tbpeohtldetail_40);
            #endregion
            #endregion

            hotel_7.BookingHotel = bookinghotel_9;
            #endregion

            voucherdbforpreview_0.Hotels.Add(hotel_7);
            #endregion
            #endregion
            #region ServiceDetails
            voucherdbforpreview_0.ServiceDetails = new Westminster.MOE.Entity.eMosDBEntity.SQL.sqlVchHotels();
            #region sqlvchhotel_41

            Westminster.MOE.Entity.eMosDBEntity.SQL.sqlVchHotel sqlvchhotel_41 = new Westminster.MOE.Entity.eMosDBEntity.SQL.sqlVchHotel();
            #region GSTTAXRate
            sqlvchhotel_41.GSTTAXRate = null;
            #endregion
            sqlvchhotel_41.GSTTAX = @"N";
            sqlvchhotel_41.coMPANYCODE = @"WM";
            sqlvchhotel_41.BKGREF = @"000001689";
            sqlvchhotel_41.SEGNUM = @"00003";
            sqlvchhotel_41.SEQNUM = @"00002";
            sqlvchhotel_41.SEQTYPE = @"COST";
            sqlvchhotel_41.RMNTS = 5M;
            sqlvchhotel_41.ARRDATE = System.DateTime.Parse("2011/12/8 0:00:00");
            sqlvchhotel_41.DEPARTDATE = System.DateTime.Parse("2011/12/13 0:00:00");
            sqlvchhotel_41.QTY = 0;
            sqlvchhotel_41.RATENAME = @"R";
            sqlvchhotel_41.ROOMTYPE = @"ECONMIC";
            sqlvchhotel_41.ITEMNAME = @"ECONMIC";
            sqlvchhotel_41.CURR = @"HKD";
            sqlvchhotel_41.AMT = 0.00M;
            sqlvchhotel_41.TOTALAMT = 0.00M;
            sqlvchhotel_41.TAXCURR = @"HKD";
            sqlvchhotel_41.TAXAMT = 0.00M;
            sqlvchhotel_41.itemtype = @"S";
            sqlvchhotel_41.QtyOfHotel = 0;
            sqlvchhotel_41.QTYs = 0;

            voucherdbforpreview_0.ServiceDetails.Add(sqlvchhotel_41);
            #endregion
            #endregion
            #region Others
            voucherdbforpreview_0.Others = new List<Westminster.MOE.Entity.DALEntity.Document.BookingOther>();
            #endregion
            voucherdbforpreview_0.TotalAmount = 0M;

            return voucherdbforpreview_0;
        }
    }
}
