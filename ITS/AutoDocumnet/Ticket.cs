﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace MOS.ITS.AutoDocumnet
{
    public class Ticket
    {
        [Test]
        public void Test()
        {
            string companyCode = "WM";
            string PNR = "BNFOFO";
            string cltCode = "HK0540";
            string adComTxNo = "";
            string tourCode = "";
            string loginUser = "";
            string[] tickets = new string[1];

            eMosBooking.BookingSoapClient emos = new MOS.ITS.eMosBooking.BookingSoapClient();
            eMosBooking.RetrieveIURBooking emosRI = emos.RetrieveBooking_IUR(companyCode, PNR, MOS.ITS.eMosBooking.GDS_Types.Abacus, tickets, cltCode, adComTxNo, tourCode, loginUser);
            hwj.CommonLibrary.Object.FileHelper.CreateFile("C:\\eMosBooking.RetrieveIURBooking.xml", hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(emosRI));

            MOEDocument.eMosAutoDocumentSoapClient moe = new MOS.ITS.MOEDocument.eMosAutoDocumentSoapClient();
            MOEDocument.RetrieveIURBooking moeRI = moe.RetrieveBooking_IUR(companyCode, PNR, MOS.ITS.MOEDocument.GDS_Types.Abacus, null, cltCode, adComTxNo, tourCode, loginUser);
            hwj.CommonLibrary.Object.FileHelper.CreateFile("C:\\MOEDocument.RetrieveIURBooking.xml", hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(moeRI));
        }
    }
}
