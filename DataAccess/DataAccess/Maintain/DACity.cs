﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using hwj.DBUtility.Interface;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Lib;
using hwj.DBUtility;

namespace Westminster.MOE.DAL.Maintain
{
    public class DACity
    {
        public IConnection InnerConnection { get; private set; }
        private DAcity daCity;
        //private DAcity_EN_US daCityEN;
        //private DAcity_ZH_CN daCityCN;
        //private DAcity_ZH_TW daCityTW;

        public DACity(IConnection conn)
        {
            InnerConnection = conn;
            daCity = new DAcity(conn);
            //daCityEN = new DAcity_EN_US(conn);
            //daCityCN = new DAcity_ZH_CN(conn);
            //daCityTW = new DAcity_ZH_TW(conn);
        }

        public tbcitys GetList(List<string> cityCodeList)
        {
            DisplayFields df = new DisplayFields();
            df.Add(tbcity.Fields.COUNTRYCODE);
            df.Add(tbcity.Fields.CITYCODE);
            df.Add(tbcity.Fields.CITYNAME);

            FilterParams fp = new FilterParams();
            fp.AddParam(tbcity.Fields.CITYCODE, cityCodeList, Enums.Relation.IN, Enums.Expression.AND);

            SqlEntity sqlEty = new SqlEntity();

            switch (MOECommon.GetLang())
            {
                case MOEEnums.LanguageType.EN_US:
                    sqlEty = DAL.eMosDB.Table.DAcity_EN_US.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                case MOEEnums.LanguageType.ZH_CN:
                    sqlEty = DAL.eMosDB.Table.DAcity_ZH_CN.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                case MOEEnums.LanguageType.ZH_TW:
                    sqlEty = DAL.eMosDB.Table.DAcity_ZH_TW.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                default:
                    sqlEty = DAL.eMosDB.Table.DAcity.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
            }

            return InnerConnection.GetList<tbcity, tbcitys>(sqlEty);
        }
    }
}
