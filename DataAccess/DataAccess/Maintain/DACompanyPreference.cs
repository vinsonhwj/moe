﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Lib;
using hwj.DBUtility.MSSQL;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.Maintain
{
    public class DACompanyPreference
    {
        public string ConnectionString { get; private set; }
        DACom_Preference daComPref;

        public DACompanyPreference(string connectionString)
        {
            ConnectionString = connectionString;
            daComPref = new DACom_Preference(connectionString);
        }
        public DACompanyPreference(IConnection conn)
        {
            daComPref = new DACom_Preference(conn);
        }

        public bool GetEnableValue(string companyCode, MOEEnums.CompanyPreference comPreferenceType)
        {
            tbCom_Preference comPref = daComPref.GetEntity(companyCode, comPreferenceType.ToString());
            if (comPref != null && !string.IsNullOrEmpty(comPref.ENABLE))
            {
                return comPref.ENABLE.Trim() == "Y";
            }
            else
            {
                return false;
            }
        }
    }

}
