﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.Interface;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Maintain;
using hwj.DBUtility.MSSQL;

namespace Westminster.MOE.DAL.Maintain
{
    public class DAShortFormProd : IDAShortFormProd
    {
        public IConnection InnerConnection { get; private set; }

        DAshortform_prod_AirClassType daShortformProdAirClassType;
        DAshortform_prod_AirlineClass daShortformProdAirlineClass;
        DAshortform_prod_city daShortformProdCity;


        public DAShortFormProd(string connectionString)
            : this(new DbConnection(connectionString))
        {
        }
        public DAShortFormProd(IConnection conn)
        {
            InnerConnection = conn;
            daShortformProdAirClassType = new DAshortform_prod_AirClassType(conn);
            daShortformProdAirlineClass = new DAshortform_prod_AirlineClass(conn);
            daShortformProdCity = new DAshortform_prod_city(conn);
        }

        public string GetAirClassName(string aircode, string classcode)
        {
            tbshortform_prod_AirlineClass cla = daShortformProdAirlineClass.GetEntity(aircode, classcode);
            if (cla == null)
                return string.Empty;

            tbshortform_prod_AirClassType type = daShortformProdAirClassType.GetEntity(cla.ClassType);

            if (type != null)
                return type.ClassName;
            else
                return string.Empty;
        }
        public string GetCityName(string code)
        {
            tbshortform_prod_city city = daShortformProdCity.GetEntity(code);
            if (city != null)
                return city.name_e;
            else
                return code;
        }
    }
}
