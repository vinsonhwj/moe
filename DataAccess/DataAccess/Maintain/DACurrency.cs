﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.Interface;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Maintain;
using hwj.DBUtility.MSSQL;

namespace Westminster.MOE.DAL.Maintain
{
    public class DACurrency : IDACurrency
    {
        public IConnection InnerConnection { get; private set; }
        private DAEXRATE daExrate;

        public DACurrency(string connectionString)
            : this(new DbConnection(connectionString))
        {
        }
        public DACurrency(IConnection conn)
        {
            InnerConnection = conn;
            daExrate = new DAEXRATE(conn);
        }

        public tbEXRATE GetExRateEntity(string companycode, string targetCurr, string sourceCurr)
        {
            return daExrate.GetEntity(companycode, targetCurr, sourceCurr);
        }
    }
}
