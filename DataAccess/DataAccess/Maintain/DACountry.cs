﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.Interface;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using hwj.DBUtility;
using Westminster.MOE.Lib;

namespace Westminster.MOE.DAL.Maintain
{
    public class DACountry
    {
        public IConnection InnerConnection { get; private set; }

        private DAcountry daCountry;
        //private DAcountry_EN_US daCountryEN;
        //private DAcountry_ZH_CN daCountryCN;
        //private DAcountry_ZH_TW daCountryTW;

        public DACountry(IConnection conn)
        {
            InnerConnection = conn;

            daCountry = new DAcountry(conn);
            //daCountryEN = new DAcountry_EN_US(conn);
            //daCountryCN = new DAcountry_ZH_CN(conn);
            //daCountryTW = new DAcountry_ZH_TW(conn);
        }

        public tbcountrys GetList(List<string> countryCodeList)
        {
            DisplayFields df = new DisplayFields();
            df.Add(tbcountry.Fields.COUNTRYCODE);
            df.Add(tbcountry.Fields.COUNTRYNAME);

            FilterParams fp = new FilterParams();
            fp.AddParam(tbcountry.Fields.COUNTRYCODE, countryCodeList, Enums.Relation.IN, Enums.Expression.AND);

            SqlEntity sqlEty = new SqlEntity();

            switch (MOECommon.GetLang())
            {
                case MOEEnums.LanguageType.EN_US:
                    sqlEty = DAL.eMosDB.Table.DAcountry_EN_US.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                case MOEEnums.LanguageType.ZH_CN:
                    sqlEty = DAL.eMosDB.Table.DAcountry_ZH_CN.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                case MOEEnums.LanguageType.ZH_TW:
                    sqlEty = DAL.eMosDB.Table.DAcountry_ZH_TW.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
                default:
                    sqlEty = DAL.eMosDB.Table.DAcountry.GetListSqlEntity(df, fp, null, null, Enums.LockType.NoLock, 30);
                    break;
            }

            return InnerConnection.GetList<tbcountry, tbcountrys>(sqlEty);
        }
    }
}
