﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using Westminster.MOE;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;
using hwj.DBUtility.Interface;
namespace Westminster.MOE.DAL.Booking
{
    public class DACloneBooking : IDAL.Booking.ICloneBooking
    {
        #region Field && Property

        private DAPEOMSTR daMstr = null;
        private DAPEOSTAFF daStaff = null;
        private DAPEOPAX daPax = null;
        private DAPEOAIR daAir = null;
        private DAPEOAIRDETAIL daAirDetail = null;
        private DAPEOHTL daHtl = null;
        private DAPeoHtlDetail daHtlDetail = null;
        private DAPEOOTHER daOther = null;
        private DAPEOOTHERDETAIL daOtherDetail = null;
        private DA_SP_Calculate daCalculate = null;
        private DA_SP_Booking daBooking = null;
        private DAPEONYREF daPeonyRef = null;
        private DALOGIN daLogin = null;
        private IConnection conn;
        #endregion

        public DACloneBooking(IConnection conn)
        {
            daStaff = new DAPEOSTAFF(conn);
            daMstr = new DAPEOMSTR(conn);
            daPax = new DAPEOPAX(conn);
            daAir = new DAPEOAIR(conn);
            daAirDetail = new DAPEOAIRDETAIL(conn);
            daHtl = new DAPEOHTL(conn);
            daHtlDetail = new DAPeoHtlDetail(conn);
            daOther = new DAPEOOTHER(conn);
            daOtherDetail = new DAPEOOTHERDETAIL(conn);
            daCalculate = new DA_SP_Calculate(conn);
            daBooking = new DA_SP_Booking(conn);
            daPeonyRef = new DAPEONYREF(conn);
            daLogin = new DALOGIN(conn);
            this.conn = conn;
        }

        #region IBooking Members

        public string GetNewBkGrefKey(string branchCode, string companyCode)
        {
            return daBooking.GetNum(companyCode, branchCode, DA_SP_Booking.ModuleType.BKGREF, 1);
        }

        public CloneBookingInfo GetCloneBookingInfo(string companyCode, string bkgRef, string staffCode, string login)
        {
            CloneBookingInfo cbi = new CloneBookingInfo();
            cbi.Staff = daStaff.GetEntity(companyCode, staffCode);
            cbi.Master = daMstr.GetEntity(companyCode, bkgRef);
            cbi.PaxList = daPax.GetList(bkgRef, companyCode);
            cbi.AirList = daAir.GetList(companyCode, bkgRef);
            cbi.AirDetailList = daAirDetail.GetList(companyCode, bkgRef);
            cbi.HtlList = daHtl.GetList(companyCode, bkgRef);
            cbi.HtlDetailList = daHtlDetail.GetList(companyCode, bkgRef);
            cbi.OtherList = daOther.GetList(companyCode, bkgRef);
            cbi.OtherDetailList = daOtherDetail.GetList(companyCode, bkgRef);
            cbi.Login = daLogin.GetEntity(companyCode, login);
            return cbi;
        }

        public tbLOGIN GetLogin(string companyCode, string login)
        {
            return daLogin.GetEntity(companyCode, login);
        }

        public void CopyBooking(CloneBookingInfo cbi, string companyCode, string cloneBkgRef, string staffCode)
        {
            SqlList sqlList = new SqlList();
            new DAPEOMSTR(conn).Add(cbi.Master);
            //sqlList.Add(DAPEOMSTR.AddSqlEntity(cbi.Master));
            foreach (tbPEOPAX pax in cbi.PaxList)
            {
                new DAPEOPAX(conn).Add(pax);
                //sqlList.Add(DAPEOPAX.AddSqlEntity(pax));
            }
            foreach (tbPEOAIR air in cbi.AirList)
            {
                new DAPEOAIR(conn).Add(air);
                //sqlList.Add(DAPEOAIR.AddSqlEntity(air));
            }
            foreach (tbPEOAIRDETAIL airDetail in cbi.AirDetailList)
            {
                new DAPEOAIRDETAIL(conn).Add(airDetail);
                //sqlList.Add(DAPEOAIRDETAIL.AddSqlEntity(airDetail));
            }
            foreach (tbPEOHTL htl in cbi.HtlList)
            {
                new DAPEOHTL(conn).Add(htl);
                //sqlList.Add(DAPEOHTL.AddSqlEntity(htl));
            }
            foreach (tbPeoHtlDetail htlDetail in cbi.HtlDetailList)
            {
                new DAPeoHtlDetail(conn).Add(htlDetail);
                //sqlList.Add(DAPeoHtlDetail.AddSqlEntity(htlDetail));
            }
            foreach (tbPEOOTHER other in cbi.OtherList)
            {
                new DAPEOOTHER(conn).Add(other);
                //sqlList.Add(DAPEOOTHER.AddSqlEntity(other));
            }
            foreach (tbPEOOTHERDETAIL otherDetail in cbi.OtherDetailList)
            {
                new DAPEOOTHERDETAIL(conn).Add(otherDetail);
                //sqlList.Add(DAPEOOTHERDETAIL.AddSqlEntity(otherDetail));
            }
            new DAPEONYREF(conn).Add(cbi.NyRef);
            //sqlList.Add(DAPEONYREF.AddSqlEntity(cbi.NyRef));
            daCalculate.CalculatePeomstrSummary(companyCode, cloneBkgRef, staffCode);
            //sqlList.Add(CalculateMasterSummary(companyCode, cloneBkgRef, staffCode));
            //return daMstr.ExecuteSqlTran(sqlList);
        }


        #endregion
    }
}
