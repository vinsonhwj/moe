﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.DAL.GatewayWebSvc
{
    public class Convert
    {
        public static Westminster.MOE.Entity.DALEntity.Transfer.IURObjects ToMOEObject(Westminster.MOE.DAL.GatewayWebSvc.IURObjects wsObj)
        {
            if (wsObj == null)
                return null;

            Westminster.MOE.Entity.DALEntity.Transfer.IURObjects iur = new Westminster.MOE.Entity.DALEntity.Transfer.IURObjects();
            iur.AirDetailList = ToMOEAirDetailList(wsObj.AirDetailList);
            iur.AirList = ToMOEAirList(wsObj.AirList);
            iur.Nyref = ToMOENYREF(wsObj.Nyref);
            iur.PaxAirList = ToMOEAirPaxList(wsObj.PaxAirList);
            iur.PaxList = ToMOEAirPaxList(wsObj.PaxList);
            iur.PEOMSTR = ToMOEMaster(wsObj.PEOMSTR);
            iur.TicketDetailList = ToMOETicketDetailList(wsObj.TicketDetailList);
            iur.TicketList = ToMOETicketList(wsObj.TicketList);
            iur.TranList = ToMOETicketList(wsObj.TranList);
            iur.VoidTicket = ToMOETicket(wsObj.VoidTicket);
            return iur;

        }

        public static tbpeotrans_IUR ToMOEtrans(Westminster.MOE.DAL.GatewayWebSvc.tbpeotrans wsObj)
        {
            if (wsObj == null)
                return null;

            tbpeotrans_IUR tran = new tbpeotrans_IUR();

            tran.bkgref = wsObj.bkgref;
            tran.CarNo = wsObj.CarNo;
            tran.CarType = wsObj.CarType;
            tran.CITYCode = wsObj.CITYCode;
            tran.companycode = tran.companycode;
            tran.ConfirmedBy = wsObj.ConfirmedBy;
            tran.ConfirmedDate = wsObj.ConfirmedDate;
            tran.createby = wsObj.createby;
            tran.createon = wsObj.createon;
            tran.DeptTime = wsObj.DeptTime;
            tran.FlightDetail = wsObj.FlightDetail;
            tran.FromAdd = wsObj.FromAdd;
            tran.FromType = wsObj.FromType;
            tran.PAXNUM = wsObj.PAXNUM;
            tran.segnum = wsObj.segnum;
            tran.tempkey = wsObj.tempkey;
            tran.ToAdd = wsObj.ToAdd;
            tran.ToType = wsObj.ToType;
            tran.updateby = wsObj.updateby;
            tran.updateon = wsObj.updateon;

            return tran;
        }
        public static tbpeotrans_IURs ToMOETicketList(Westminster.MOE.DAL.GatewayWebSvc.tbpeotrans[] wsObj)
        {
            tbpeotrans_IURs trans = new tbpeotrans_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbpeotrans obj in wsObj)
            {
                tbpeotrans_IUR one = ToMOEtrans(obj);
                if (one != null)
                    trans.Add(one);
            }

            return trans;
        }

        public static tbPEOTKT_IUR ToMOETicket(Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKT wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOTKT_IUR tkt = new tbPEOTKT_IUR();

            tkt.add_payment = wsObj.add_payment;
            tkt.AirCode = wsObj.AirCode;
            tkt.Airline = wsObj.Airline;
            tkt.AirReason = wsObj.AirReason;
            tkt.AirSeg = wsObj.AirSeg;
            tkt.BKGREF = wsObj.BKGREF;
            tkt.BranchCode = wsObj.BranchCode;
            tkt.Class = wsObj.Class;
            tkt.CltCode = wsObj.CltCode;
            tkt.COD1 = wsObj.COD1;
            tkt.COD2 = wsObj.COD2;
            tkt.COD3 = wsObj.COD3;
            tkt.COD4 = wsObj.COD4;
            tkt.COD5 = wsObj.COD5;
            tkt.COD6 = wsObj.COD6;
            tkt.COD7 = wsObj.COD7;
            tkt.Commission = wsObj.Commission;
            tkt.commission_per = wsObj.commission_per;
            tkt.CompanyCode = wsObj.CompanyCode;
            tkt.CompressPrintInd = wsObj.CompressPrintInd;
            tkt.ControlNum = wsObj.ControlNum;
            tkt.CorpCurr = wsObj.CorpCurr;
            tkt.CorpFare = wsObj.CorpFare;
            tkt.CreateBy = wsObj.CreateBy;
            tkt.CreateOn = wsObj.CreateOn;
            tkt.DepartOn = wsObj.DepartOn;
            tkt.Destination = wsObj.Destination;
            tkt.Discount = wsObj.Discount;
            tkt.ENDORSEMENTS = wsObj.ENDORSEMENTS;
            tkt.FareBasis = wsObj.FareBasis;
            tkt.FareCalData = wsObj.FareCalData;
            tkt.FareCalType = wsObj.FareCalType;
            tkt.FareType = wsObj.FareType;
            tkt.ForeignFareAmt = wsObj.ForeignFareAmt;
            tkt.ForeignFareCurr = wsObj.ForeignFareCurr;
            tkt.FormPay = wsObj.FormPay;
            tkt.FullCurr = wsObj.FullCurr;
            tkt.FullFare = wsObj.FullFare;
            tkt.INTDOM = wsObj.INTDOM;
            tkt.InvNum = wsObj.InvNum;
            tkt.iss_in_exchange = wsObj.iss_in_exchange;
            tkt.iss_off_code = wsObj.iss_off_code;
            tkt.IssueBy = wsObj.IssueBy;
            tkt.IssueOn = wsObj.IssueOn;
            tkt.Jqty = wsObj.Jqty;
            tkt.LocalFareAmt = wsObj.LocalFareAmt;
            tkt.LocalFareCurr = wsObj.LocalFareCurr;
            tkt.LowCurr = wsObj.LowCurr;
            tkt.LowFare = wsObj.LowFare;
            tkt.NetCurr = wsObj.NetCurr;
            tkt.NetFare = wsObj.NetFare;
            tkt.nst = wsObj.nst;
            tkt.org_iss = wsObj.org_iss;
            tkt.PaidCurr = wsObj.PaidCurr;
            tkt.PaidFare = wsObj.PaidFare;
            tkt.PAXAIR = wsObj.PAXAIR;
            tkt.PaxName = wsObj.PaxName;
            tkt.PNR = wsObj.PNR;
            tkt.pseudo = wsObj.pseudo;
            tkt.QSurcharge = wsObj.QSurcharge;
            tkt.Routing = wsObj.Routing;
            tkt.Security = wsObj.Security;
            tkt.SellCurr = wsObj.SellCurr;
            tkt.SellFare = wsObj.SellFare;
            tkt.sinein = wsObj.sinein;
            tkt.SOURCESYSTEM = wsObj.SOURCESYSTEM;
            tkt.SuppCode = wsObj.SuppCode;
            tkt.TaxAmount1 = wsObj.TaxAmount1;
            tkt.TaxAmount2 = wsObj.TaxAmount2;
            tkt.TaxAmount3 = wsObj.TaxAmount3;
            tkt.TaxID1 = wsObj.TaxID1;
            tkt.TaxID2 = wsObj.TaxID2;
            tkt.TaxID3 = wsObj.TaxID3;
            tkt.TeamCode = wsObj.TeamCode;
            tkt.Ticket = wsObj.Ticket;
            tkt.TktSeq = wsObj.TktSeq;
            tkt.tkttype = wsObj.tkttype;
            tkt.TotalTax = wsObj.TotalTax;
            tkt.tour_code = wsObj.tour_code;
            tkt.tranflag = wsObj.tranflag;
            tkt.TransferFlag = wsObj.TransferFlag;
            tkt.UpdateBy = wsObj.UpdateBy;
            tkt.UpdateOn = wsObj.UpdateOn;
            tkt.VoidBy = wsObj.VoidBy;
            tkt.VoidOn = wsObj.VoidOn;
            tkt.voidreason = wsObj.voidreason;
            tkt.voidTeam = tkt.voidTeam;

            return tkt;
        }
        public static tbPEOTKT_IURs ToMOETicketList(Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKT[] wsObj)
        {
            tbPEOTKT_IURs tkts = new tbPEOTKT_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKT obj in wsObj)
            {
                tbPEOTKT_IUR one = ToMOETicket(obj);
                if (one != null)
                    tkts.Add(one);
            }

            return tkts;
        }

        public static tbPEOTKTDETAIL_IUR ToMOETicketDetail(Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKTDETAIL wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOTKTDETAIL_IUR tkt = new tbPEOTKTDETAIL_IUR();

            tkt.AirCode = wsObj.AirCode;
            tkt.Airline = wsObj.Airline;
            tkt.AirlinePNR = wsObj.AirlinePNR;
            tkt.ArrDATE = wsObj.ArrDATE;
            tkt.ArrivalCity = wsObj.ArrivalCity;
            tkt.ArrivalTerm = wsObj.ArrivalTerm;
            tkt.ArrTime = wsObj.ArrTime;
            tkt.Class = wsObj.Class;
            tkt.Commission = wsObj.Commission;
            tkt.CompanyCode = wsObj.CompanyCode;
            tkt.CreateBy = wsObj.CreateBy;
            tkt.CreateOn = wsObj.CreateOn;
            tkt.DepartCity = wsObj.DepartCity;
            tkt.DepartDATE = wsObj.DepartDATE;
            tkt.DepartTerm = wsObj.DepartTerm;
            tkt.DepartTime = wsObj.DepartTime;
            tkt.ElapsedTime = wsObj.ElapsedTime;
            tkt.EQP = wsObj.EQP;
            tkt.fare_basis_code = wsObj.fare_basis_code;
            tkt.FareAmt = wsObj.FareAmt;
            tkt.FareBasic = wsObj.FareBasic;
            tkt.Flight = wsObj.Flight;
            tkt.freq_fight_num = wsObj.freq_fight_num;
            tkt.Seat = wsObj.Seat;
            tkt.SegNum = wsObj.SegNum;
            tkt.Service = wsObj.Service;
            tkt.status = wsObj.status;
            tkt.StopOverCity = wsObj.StopOverCity;
            tkt.StopOverNum = wsObj.StopOverNum;
            tkt.Ticket = wsObj.Ticket;
            tkt.TktSeq = wsObj.TktSeq;
            tkt.UpdateBy = wsObj.UpdateBy;
            tkt.UpdateOn = wsObj.UpdateOn;
            tkt.ValidEnd = wsObj.ValidEnd;
            tkt.ValidStart = wsObj.ValidStart;

            return tkt;
        }
        public static tbPEOTKTDETAIL_IURs ToMOETicketDetailList(Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKTDETAIL[] wsObj)
        {
            tbPEOTKTDETAIL_IURs tkts = new tbPEOTKTDETAIL_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKTDETAIL obj in wsObj)
            {
                tbPEOTKTDETAIL_IUR one = ToMOETicketDetail(obj);
                if (one != null)
                    tkts.Add(one);
            }

            return tkts;
        }

        public static tbPEOMSTR_IUR ToMOEMaster(Westminster.MOE.DAL.GatewayWebSvc.tbPEOMSTR wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOMSTR_IUR mstr = new tbPEOMSTR_IUR();

            mstr.agency_arc_iata_num = wsObj.agency_arc_iata_num;
            mstr.BKGREF = wsObj.BKGREF;
            mstr.CLTADDR = wsObj.CLTADDR;
            mstr.CLTNAME = wsObj.CLTNAME;
            mstr.CLTODE = wsObj.CLTODE;
            mstr.COD1 = wsObj.COD1;
            mstr.COD2 = wsObj.COD2;
            mstr.COD3 = wsObj.COD3;
            mstr.COMPANYCODE = wsObj.COMPANYCODE;
            mstr.CONTACTPERSON = wsObj.CONTACTPERSON;
            mstr.CREATEBY = wsObj.CREATEBY;
            mstr.CREATEON = wsObj.CREATEON;
            mstr.CRSSINEIN = wsObj.CRSSINEIN;
            mstr.CRSSINEIN_inv = wsObj.CRSSINEIN_inv;
            mstr.DEADLINE = wsObj.DEADLINE;
            mstr.DEPARTDATE = wsObj.DEPARTDATE;
            mstr.DOCAMT = wsObj.DOCAMT;
            mstr.DOCCOUNT = wsObj.DOCCOUNT;
            mstr.DOCCURR = wsObj.DOCCURR;
            mstr.DOCTAX = wsObj.DOCTAX;
            mstr.DOCTAXCURR = wsObj.DOCTAXCURR;
            mstr.EMAIL = wsObj.EMAIL;
            mstr.EMOSBKGREF = wsObj.EMOSBKGREF;
            mstr.FAX = wsObj.FAX;
            mstr.FIRSTPAX = wsObj.FIRSTPAX;
            mstr.INSFLAG = wsObj.INSFLAG;
            mstr.INVAMT = wsObj.INVAMT;
            mstr.INVCOUNT = wsObj.INVCOUNT;
            mstr.INVCURR = wsObj.INVCURR;
            mstr.INVTAX = wsObj.INVTAX;
            mstr.INVTAXCURR = wsObj.INVTAXCURR;
            mstr.iss_off_code = wsObj.iss_off_code;
            mstr.IURFLAG = wsObj.IURFLAG;
            mstr.MASTERPNR = wsObj.MASTERPNR;
            mstr.PHONE = wsObj.PHONE;
            mstr.PNR = wsObj.PNR;
            mstr.pseudo = wsObj.pseudo;
            mstr.pseudo_inv = wsObj.pseudo_inv;
            mstr.RMK1 = wsObj.RMK1;
            mstr.RMK2 = wsObj.RMK2;
            mstr.RMK3 = wsObj.RMK3;
            mstr.RMK4 = wsObj.RMK4;
            mstr.SOURCE = wsObj.SOURCE;
            mstr.SOURCESYSTEM = wsObj.SOURCESYSTEM;
            mstr.STAFFCODE = wsObj.STAFFCODE;
            mstr.TEAMCODE = wsObj.TEAMCODE;
            mstr.TOEMOSBY = wsObj.TOEMOSBY;
            mstr.TOEMOSON = wsObj.TOEMOSON;
            mstr.TOURCODE = wsObj.TOURCODE;
            mstr.tranflag = wsObj.tranflag;
            mstr.TTLCOSTAMT = wsObj.TTLCOSTAMT;
            mstr.TTLCOSTCURR = wsObj.TTLCOSTCURR;
            mstr.TTLCOSTTAX = wsObj.TTLCOSTTAX;
            mstr.TTLCOSTTAXCURR = wsObj.TTLCOSTTAXCURR;
            mstr.TTLSELLAMT = wsObj.TTLSELLAMT;
            mstr.TTLSELLCURR = wsObj.TTLSELLCURR;
            mstr.TTLSELLTAX = wsObj.TTLSELLTAX;
            mstr.TTLSELLTAXCURR = wsObj.TTLSELLTAXCURR;
            mstr.UPDATEBY = wsObj.UPDATEBY;
            mstr.UPDATEON = wsObj.UPDATEON;

            return mstr;
        }

        public static tbPEOPAX_IUR ToMOEPax(Westminster.MOE.DAL.GatewayWebSvc.tbPEOPAX wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOPAX_IUR pax = new tbPEOPAX_IUR();

            pax.BKGREF = wsObj.BKGREF;
            pax.COMPANYCODE = wsObj.COMPANYCODE;
            pax.CREATEBY = wsObj.CREATEBY;
            pax.CREATEON = wsObj.CREATEON;
            pax.PAXAGE = wsObj.PAXAGE;
            pax.PAXAIR = wsObj.PAXAIR;
            pax.PAXFNAME = wsObj.PAXFNAME;
            pax.PAXHOTEL = wsObj.PAXHOTEL;
            pax.PAXLNAME = wsObj.PAXLNAME;
            pax.PAXNUM = wsObj.PAXNUM;
            pax.PAXOTHER = wsObj.PAXOTHER;
            pax.PAXTITLE = wsObj.PAXTITLE;
            pax.PAXTYPE = wsObj.PAXTYPE;
            pax.RMK1 = wsObj.RMK1;
            pax.RMK2 = wsObj.RMK2;
            pax.SEGNUM = wsObj.SEGNUM;
            pax.SOURCESYSTEM = wsObj.SOURCESYSTEM;
            pax.UPDATEBY = wsObj.UPDATEBY;
            pax.UPDATEON = wsObj.UPDATEON;
            pax.VOIDBY = wsObj.VOIDBY;
            pax.VOIDON = wsObj.VOIDON;

            return pax;
        }
        public static tbPEOPAX_IURs ToMOEAirPaxList(Westminster.MOE.DAL.GatewayWebSvc.tbPEOPAX[] wsObj)
        {
            tbPEOPAX_IURs paxs = new tbPEOPAX_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOPAX obj in wsObj)
            {
                tbPEOPAX_IUR one = ToMOEPax(obj);
                if (one != null)
                    paxs.Add(one);
             
            }

            return paxs;
        }

        public static tbPeoAirPax_IUR ToMOEAirPax(Westminster.MOE.DAL.GatewayWebSvc.tbPeoAirPax wsObj)
        {
            if (wsObj == null)
                return null;

            tbPeoAirPax_IUR ap = new tbPeoAirPax_IUR();

            ap.BKGREF = wsObj.BKGREF;
            ap.COMPANYCODE = wsObj.COMPANYCODE;
            ap.Freq_Fight_num = wsObj.Freq_Fight_num;
            ap.PAXNUM = wsObj.PAXNUM;
            ap.SEGNUM = wsObj.SEGNUM;

            return ap;
        }
        public static tbPeoAirPax_IURs ToMOEAirPaxList(Westminster.MOE.DAL.GatewayWebSvc.tbPeoAirPax[] wsObj)
        {
            tbPeoAirPax_IURs airs = new tbPeoAirPax_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPeoAirPax obj in wsObj)
            {
                tbPeoAirPax_IUR one = ToMOEAirPax(obj);
                if (one != null)
                    airs.Add(one);
            }

            return airs;
        }

        public static tbPEONYREF_IUR ToMOENYREF(Westminster.MOE.DAL.GatewayWebSvc.tbPEONYREF wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEONYREF_IUR nyref = new tbPEONYREF_IUR();

            nyref.ACTIONCODE = wsObj.ACTIONCODE;
            nyref.BKGREF = wsObj.BKGREF;
            nyref.CLTCODE = wsObj.CLTCODE;
            nyref.COMPANYCODE = wsObj.COMPANYCODE;
            nyref.CostAmt = wsObj.CostAmt;
            nyref.CostCurr = wsObj.CostCurr;
            nyref.CostTaxAmt = wsObj.CostTaxAmt;
            nyref.CostTaxCurr = wsObj.CostTaxCurr;
            nyref.CREATEBY = wsObj.CREATEBY;
            nyref.CREATEON = wsObj.CREATEON;
            nyref.Description = wsObj.Description;
            nyref.DocNum = wsObj.DocNum;
            nyref.REFID = wsObj.REFID;
            nyref.SellAmt = wsObj.SellAmt;
            nyref.SellCurr = wsObj.SellCurr;
            nyref.SellTaxAmt = wsObj.SellTaxAmt;
            nyref.SellTaxCurr = wsObj.SellTaxCurr;
            nyref.STAFFCODE = wsObj.STAFFCODE;
            nyref.SUPPCODE = wsObj.SUPPCODE;
            nyref.TEAMCODE = wsObj.TEAMCODE;
            return nyref;

        }

        public static tbPEOAIR_IUR ToMOEAir(Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIR wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOAIR_IUR air = new tbPEOAIR_IUR();
            air.AirSeg = wsObj.AirSeg;
            air.ArrDATE = wsObj.ArrDATE;
            air.ArrivalCity = wsObj.ArrivalCity;
            air.ArrivalTerminal = wsObj.ArrivalTerminal;
            air.ArrTime = wsObj.ArrTime;
            air.BkgRef = wsObj.BkgRef;
            air.Class = wsObj.Class;
            air.CompanyCode = wsObj.CompanyCode;
            air.COSTCURR = wsObj.COSTCURR;
            air.CreateBy = wsObj.CreateBy;
            air.CreateOn = wsObj.CreateOn;
            air.DepartCity = wsObj.DepartCity;
            air.DepartDATE = wsObj.DepartDATE;
            air.DepartTerminal = wsObj.DepartTerminal;
            air.DepartTime = wsObj.DepartTime;
            air.ElapsedTime = wsObj.ElapsedTime;
            air.EQP = wsObj.EQP;
            air.Flight = wsObj.Flight;
            air.INVRMK1 = wsObj.INVRMK1;
            air.INVRMK2 = wsObj.INVRMK2;
            air.NumOfPax = wsObj.NumOfPax;
            air.OtherAirlinePNR = wsObj.OtherAirlinePNR;
            air.Pcode = wsObj.Pcode;
            air.PNR = wsObj.PNR;
            air.ReasonCode = wsObj.ReasonCode;
            air.RMK1 = wsObj.RMK1;
            air.RMK2 = wsObj.RMK2;
            air.Seat = wsObj.Seat;
            air.SegNum = wsObj.SegNum;
            air.SELLCURR = wsObj.SELLCURR;
            air.Service = wsObj.Service;
            air.SourceSystem = wsObj.SourceSystem;
            air.SpecialRQ = wsObj.SpecialRQ;
            air.SSR = wsObj.SSR;
            air.Status = wsObj.Status;
            air.StopOverCity = wsObj.StopOverCity;
            air.StopOverNum = wsObj.StopOverNum;
            air.Suppcode = wsObj.Suppcode;
            air.TOTALCOST = wsObj.TOTALCOST;
            air.TOTALCTAX = wsObj.TOTALCTAX;
            air.TOTALSELL = wsObj.TOTALSELL;
            air.TOTALSTAX = wsObj.TOTALSTAX;
            air.UpdateBy = wsObj.UpdateBy;
            air.UpdateOn = wsObj.UpdateOn;
            air.VCHRMK1 = wsObj.VCHRMK1;
            air.VCHRMK2 = wsObj.VCHRMK2;
            air.VoidBy = wsObj.VoidBy;
            air.VoidOn = wsObj.VoidOn;


            return air;
        }
        public static tbPEOAIR_IURs ToMOEAirList(Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIR[] wsObj)
        {
            tbPEOAIR_IURs airs = new tbPEOAIR_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIR obj in wsObj)
            {
                tbPEOAIR_IUR one = ToMOEAir(obj);
                if (one != null)
                    airs.Add(one);
            }

            return airs;
        }

        public static tbPEOAIRDETAIL_IUR ToMOEAirDetail(Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIRDETAIL wsObj)
        {
            if (wsObj == null)
                return null;

            tbPEOAIRDETAIL_IUR air = new tbPEOAIRDETAIL_IUR();
            air.AGETYPE = wsObj.AGETYPE;
            air.AMT = wsObj.AMT;
            air.BkgRef = wsObj.BkgRef;
            air.CompanyCode = wsObj.CompanyCode;
            air.CREATEBY = wsObj.CREATEBY;
            air.CREATEON = wsObj.CREATEON;
            air.CURR = wsObj.CURR;
            air.QTY = wsObj.QTY;
            air.SegNum = wsObj.SegNum;
            air.SEQNUM = wsObj.SEQNUM;
            air.SEQTYPE = wsObj.SEQTYPE;
            air.TAXAMT = wsObj.TAXAMT;
            air.TAXCURR = wsObj.TAXCURR;
            air.UPDATEBY = wsObj.UPDATEBY;
            air.UPDATEON = wsObj.UPDATEON;

            return air;
        }
        public static tbPEOAIRDETAIL_IURs ToMOEAirDetailList(Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIRDETAIL[] wsObj)
        {
            tbPEOAIRDETAIL_IURs airs = new tbPEOAIRDETAIL_IURs();

            foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIRDETAIL obj in wsObj)
            {
                tbPEOAIRDETAIL_IUR one = ToMOEAirDetail(obj);
                if (one != null)
                    airs.Add(one);
            }

            return airs;
        }


    }
}
