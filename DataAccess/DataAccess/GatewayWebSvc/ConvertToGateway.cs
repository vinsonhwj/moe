﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Gateway;

namespace Westminster.MOE.DAL.GatewayWebSvc
{
    public class ConvertToGateway
    {
        public static Westminster.MOE.Entity.DALEntity.Gateway.IURObject ToMOEObject(IURObject iurWS)
        {
            Westminster.MOE.Entity.DALEntity.Gateway.IURObject moeObj = new Westminster.MOE.Entity.DALEntity.Gateway.IURObject();
            if (iurWS != null)
            {

                if (iurWS.Master != null)
                {
                    moeObj.PEOMSTR = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOMSTR();
                    moeObj.PEOMSTR.COMPANYCODE = iurWS.Master.COMPANYCODE;
                    moeObj.PEOMSTR.BKGREF = iurWS.Master.BKGREF;
                    moeObj.PEOMSTR.PNR = iurWS.Master.PNR;
                    moeObj.PEOMSTR.CRSSINEIN = iurWS.Master.CRSSINEIN;
                    moeObj.PEOMSTR.COD1 = iurWS.Master.COD1;
                    moeObj.PEOMSTR.COD2 = iurWS.Master.COD2;
                    moeObj.PEOMSTR.CLTODE = iurWS.Master.CLTODE;
                    moeObj.PEOMSTR.CLTNAME = iurWS.Master.CLTNAME;
                    moeObj.PEOMSTR.CLTADDR = iurWS.Master.CLTADDR;
                    moeObj.PEOMSTR.STAFFCODE = iurWS.Master.STAFFCODE;
                    moeObj.PEOMSTR.TEAMCODE = iurWS.Master.TEAMCODE;
                    moeObj.PEOMSTR.FIRSTPAX = iurWS.Master.FIRSTPAX;
                    moeObj.PEOMSTR.PHONE = iurWS.Master.PHONE;
                    moeObj.PEOMSTR.FAX = iurWS.Master.FAX;
                    moeObj.PEOMSTR.EMAIL = iurWS.Master.EMAIL;
                    moeObj.PEOMSTR.MASTERPNR = iurWS.Master.MASTERPNR;
                    moeObj.PEOMSTR.TOURCODE = iurWS.Master.TOURCODE;
                    moeObj.PEOMSTR.CONTACTPERSON = iurWS.Master.CONTACTPERSON;
                    moeObj.PEOMSTR.DEPARTDATE = iurWS.Master.DEPARTDATE;
                    moeObj.PEOMSTR.TTLSELLCURR = iurWS.Master.TTLSELLCURR;
                    moeObj.PEOMSTR.TTLSELLAMT = iurWS.Master.TTLSELLAMT;
                    moeObj.PEOMSTR.TTLSELLTAXCURR = iurWS.Master.TTLSELLTAXCURR;
                    moeObj.PEOMSTR.TTLSELLTAX = iurWS.Master.TTLSELLTAX;
                    moeObj.PEOMSTR.TTLCOSTCURR = iurWS.Master.TTLCOSTCURR;
                    moeObj.PEOMSTR.TTLCOSTAMT = iurWS.Master.TTLCOSTAMT;
                    moeObj.PEOMSTR.TTLCOSTTAXCURR = iurWS.Master.TTLCOSTTAXCURR;
                    moeObj.PEOMSTR.TTLCOSTTAX = iurWS.Master.TTLCOSTTAX;
                    moeObj.PEOMSTR.INVCURR = iurWS.Master.INVCURR;
                    moeObj.PEOMSTR.INVAMT = iurWS.Master.INVAMT;
                    moeObj.PEOMSTR.INVTAXCURR = iurWS.Master.INVTAXCURR;
                    moeObj.PEOMSTR.INVTAX = iurWS.Master.INVTAX;
                    moeObj.PEOMSTR.DOCCURR = iurWS.Master.DOCCURR;
                    moeObj.PEOMSTR.DOCAMT = iurWS.Master.DOCAMT;
                    moeObj.PEOMSTR.DOCTAXCURR = iurWS.Master.DOCTAXCURR;
                    moeObj.PEOMSTR.DOCTAX = iurWS.Master.DOCTAX;
                    moeObj.PEOMSTR.INVCOUNT = iurWS.Master.INVCOUNT;
                    moeObj.PEOMSTR.DOCCOUNT = iurWS.Master.DOCCOUNT;
                    moeObj.PEOMSTR.RMK1 = iurWS.Master.RMK1;
                    moeObj.PEOMSTR.RMK2 = iurWS.Master.RMK2;
                    moeObj.PEOMSTR.RMK3 = iurWS.Master.RMK3;
                    moeObj.PEOMSTR.RMK4 = iurWS.Master.RMK4;
                    moeObj.PEOMSTR.DEADLINE = iurWS.Master.DEADLINE;
                    moeObj.PEOMSTR.SOURCESYSTEM = iurWS.Master.SOURCESYSTEM;
                    moeObj.PEOMSTR.SOURCE = iurWS.Master.SOURCE;
                    moeObj.PEOMSTR.CREATEON = iurWS.Master.CREATEON;
                    moeObj.PEOMSTR.CREATEBY = iurWS.Master.CREATEBY;
                    moeObj.PEOMSTR.UPDATEON = iurWS.Master.UPDATEON;
                    moeObj.PEOMSTR.UPDATEBY = iurWS.Master.UPDATEBY;
                    moeObj.PEOMSTR.COD3 = iurWS.Master.COD3;
                    moeObj.PEOMSTR.INSFLAG = iurWS.Master.INSFLAG;
                    moeObj.PEOMSTR.TOEMOSON = iurWS.Master.TOEMOSON;
                    moeObj.PEOMSTR.TOEMOSBY = iurWS.Master.TOEMOSBY;
                    moeObj.PEOMSTR.EMOSBKGREF = iurWS.Master.EMOSBKGREF;
                    moeObj.PEOMSTR.IURFLAG = iurWS.Master.IURFLAG;
                    moeObj.PEOMSTR.pseudo = iurWS.Master.pseudo;
                    moeObj.PEOMSTR.pseudo_inv = iurWS.Master.pseudo_inv;
                    moeObj.PEOMSTR.CRSSINEIN_inv = iurWS.Master.CRSSINEIN_inv;
                    moeObj.PEOMSTR.agency_arc_iata_num = iurWS.Master.agency_arc_iata_num;
                    moeObj.PEOMSTR.iss_off_code = iurWS.Master.iss_off_code;
                    moeObj.PEOMSTR.tranflag = iurWS.Master.tranflag;
                }

                moeObj.PaxList = new tbPEOPAXs();
                if (iurWS.Passengers != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOPAX _frtbPEOPAX in iurWS.Passengers)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX _tbPEOPAX = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOPAX();
                        _tbPEOPAX.BKGREF = _frtbPEOPAX.BKGREF;
                        _tbPEOPAX.COMPANYCODE = _frtbPEOPAX.COMPANYCODE;
                        _tbPEOPAX.SEGNUM = _frtbPEOPAX.SEGNUM;
                        _tbPEOPAX.PAXNUM = _frtbPEOPAX.PAXNUM;
                        _tbPEOPAX.PAXLNAME = _frtbPEOPAX.PAXLNAME;
                        _tbPEOPAX.PAXFNAME = _frtbPEOPAX.PAXFNAME;
                        _tbPEOPAX.PAXTITLE = _frtbPEOPAX.PAXTITLE;
                        _tbPEOPAX.PAXTYPE = _frtbPEOPAX.PAXTYPE;
                        _tbPEOPAX.PAXAIR = _frtbPEOPAX.PAXAIR;
                        _tbPEOPAX.PAXHOTEL = _frtbPEOPAX.PAXHOTEL;
                        _tbPEOPAX.PAXOTHER = _frtbPEOPAX.PAXOTHER;
                        _tbPEOPAX.PAXAGE = _frtbPEOPAX.PAXAGE;
                        _tbPEOPAX.RMK1 = _frtbPEOPAX.RMK1;
                        _tbPEOPAX.RMK2 = _frtbPEOPAX.RMK2;
                        _tbPEOPAX.VOIDON = _frtbPEOPAX.VOIDON;
                        _tbPEOPAX.VOIDBY = _frtbPEOPAX.VOIDBY;
                        _tbPEOPAX.SOURCESYSTEM = _frtbPEOPAX.SOURCESYSTEM;
                        _tbPEOPAX.CREATEON = _frtbPEOPAX.CREATEON;
                        _tbPEOPAX.CREATEBY = _frtbPEOPAX.CREATEBY;
                        _tbPEOPAX.UPDATEON = _frtbPEOPAX.UPDATEON;
                        _tbPEOPAX.UPDATEBY = _frtbPEOPAX.UPDATEBY;

                        moeObj.PaxList.Add(_tbPEOPAX);
                    }
                }

                moeObj.AirList = new tbPEOAIRs();
                if (iurWS.Airs != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIR _frtbPEOAIR in iurWS.Airs)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR _tbPEOAIR = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIR();
                        _tbPEOAIR.CompanyCode = _frtbPEOAIR.CompanyCode;
                        _tbPEOAIR.BkgRef = _frtbPEOAIR.BkgRef;
                        _tbPEOAIR.SegNum = _frtbPEOAIR.SegNum;
                        _tbPEOAIR.PNR = _frtbPEOAIR.PNR;
                        _tbPEOAIR.Flight = _frtbPEOAIR.Flight;
                        _tbPEOAIR.Class = _frtbPEOAIR.Class;
                        _tbPEOAIR.DepartCity = _frtbPEOAIR.DepartCity;
                        _tbPEOAIR.ArrivalCity = _frtbPEOAIR.ArrivalCity;
                        _tbPEOAIR.Status = _frtbPEOAIR.Status;
                        _tbPEOAIR.NumOfPax = _frtbPEOAIR.NumOfPax;
                        _tbPEOAIR.DepartDATE = _frtbPEOAIR.DepartDATE;
                        _tbPEOAIR.DepartTime = _frtbPEOAIR.DepartTime;
                        _tbPEOAIR.ArrDATE = _frtbPEOAIR.ArrDATE;
                        _tbPEOAIR.ArrTime = _frtbPEOAIR.ArrTime;
                        _tbPEOAIR.ElapsedTime = _frtbPEOAIR.ElapsedTime;
                        _tbPEOAIR.SSR = _frtbPEOAIR.SSR;
                        _tbPEOAIR.Seat = _frtbPEOAIR.Seat;
                        _tbPEOAIR.ReasonCode = _frtbPEOAIR.ReasonCode;
                        _tbPEOAIR.StopOverNum = _frtbPEOAIR.StopOverNum;
                        _tbPEOAIR.StopOverCity = _frtbPEOAIR.StopOverCity;
                        _tbPEOAIR.Pcode = _frtbPEOAIR.Pcode;
                        _tbPEOAIR.SpecialRQ = _frtbPEOAIR.SpecialRQ;
                        _tbPEOAIR.RMK1 = _frtbPEOAIR.RMK1;
                        _tbPEOAIR.RMK2 = _frtbPEOAIR.RMK2;
                        _tbPEOAIR.INVRMK1 = _frtbPEOAIR.INVRMK1;
                        _tbPEOAIR.INVRMK2 = _frtbPEOAIR.INVRMK2;
                        _tbPEOAIR.VCHRMK1 = _frtbPEOAIR.VCHRMK1;
                        _tbPEOAIR.VCHRMK2 = _frtbPEOAIR.VCHRMK2;
                        _tbPEOAIR.SELLCURR = _frtbPEOAIR.SELLCURR;
                        _tbPEOAIR.TOTALSELL = _frtbPEOAIR.TOTALSELL;
                        _tbPEOAIR.TOTALSTAX = _frtbPEOAIR.TOTALSTAX;
                        _tbPEOAIR.COSTCURR = _frtbPEOAIR.COSTCURR;
                        _tbPEOAIR.TOTALCOST = _frtbPEOAIR.TOTALCOST;
                        _tbPEOAIR.TOTALCTAX = _frtbPEOAIR.TOTALCTAX;
                        _tbPEOAIR.SourceSystem = _frtbPEOAIR.SourceSystem;
                        _tbPEOAIR.CreateOn = _frtbPEOAIR.CreateOn;
                        _tbPEOAIR.CreateBy = _frtbPEOAIR.CreateBy;
                        _tbPEOAIR.UpdateOn = _frtbPEOAIR.UpdateOn;
                        _tbPEOAIR.UpdateBy = _frtbPEOAIR.UpdateBy;
                        _tbPEOAIR.VoidOn = _frtbPEOAIR.VoidOn;
                        _tbPEOAIR.VoidBy = _frtbPEOAIR.VoidBy;
                        _tbPEOAIR.AirSeg = _frtbPEOAIR.AirSeg;
                        _tbPEOAIR.DepartTerminal = _frtbPEOAIR.DepartTerminal;
                        _tbPEOAIR.ArrivalTerminal = _frtbPEOAIR.ArrivalTerminal;
                        _tbPEOAIR.OtherAirlinePNR = _frtbPEOAIR.OtherAirlinePNR;
                        _tbPEOAIR.EQP = _frtbPEOAIR.EQP;
                        _tbPEOAIR.Service = _frtbPEOAIR.Service;
                        _tbPEOAIR.Suppcode = _frtbPEOAIR.Suppcode;

                        moeObj.AirList.Add(_tbPEOAIR);
                    }
                }

                moeObj.AirDetailList = new tbPEOAIRDETAILs();
                if (iurWS.AirDetails != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOAIRDETAIL _frtbPEOAIRDETAIL in iurWS.AirDetails)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL _tbPEOAIRDETAIL = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOAIRDETAIL();
                        _tbPEOAIRDETAIL.CompanyCode = _frtbPEOAIRDETAIL.CompanyCode;
                        _tbPEOAIRDETAIL.BkgRef = _frtbPEOAIRDETAIL.BkgRef;
                        _tbPEOAIRDETAIL.SegNum = _frtbPEOAIRDETAIL.SegNum;
                        _tbPEOAIRDETAIL.SEQNUM = _frtbPEOAIRDETAIL.SEQNUM;
                        _tbPEOAIRDETAIL.SEQTYPE = _frtbPEOAIRDETAIL.SEQTYPE;
                        _tbPEOAIRDETAIL.QTY = _frtbPEOAIRDETAIL.QTY;
                        _tbPEOAIRDETAIL.CURR = _frtbPEOAIRDETAIL.CURR;
                        _tbPEOAIRDETAIL.AMT = _frtbPEOAIRDETAIL.AMT;
                        _tbPEOAIRDETAIL.TAXCURR = _frtbPEOAIRDETAIL.TAXCURR;
                        _tbPEOAIRDETAIL.TAXAMT = _frtbPEOAIRDETAIL.TAXAMT;
                        _tbPEOAIRDETAIL.CREATEON = _frtbPEOAIRDETAIL.CREATEON;
                        _tbPEOAIRDETAIL.CREATEBY = _frtbPEOAIRDETAIL.CREATEBY;
                        _tbPEOAIRDETAIL.UPDATEON = _frtbPEOAIRDETAIL.UPDATEON;
                        _tbPEOAIRDETAIL.UPDATEBY = _frtbPEOAIRDETAIL.UPDATEBY;
                        _tbPEOAIRDETAIL.AGETYPE = _frtbPEOAIRDETAIL.AGETYPE;

                        moeObj.AirDetailList.Add(_tbPEOAIRDETAIL);
                    }
                }

                moeObj.PaxAirList = new tbPeoAirPaxs();
                if (iurWS.PaxAirList != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPeoAirPax _frtbPeoAirPax in iurWS.PaxAirList)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPeoAirPax _tbPeoAirPax = new Westminster.MOE.Entity.DALEntity.Gateway.tbPeoAirPax();
                        _tbPeoAirPax.COMPANYCODE = _frtbPeoAirPax.COMPANYCODE;
                        _tbPeoAirPax.BKGREF = _frtbPeoAirPax.BKGREF;
                        _tbPeoAirPax.SEGNUM = _frtbPeoAirPax.SEGNUM;
                        _tbPeoAirPax.PAXNUM = _frtbPeoAirPax.PAXNUM;
                        _tbPeoAirPax.Freq_Fight_num = _frtbPeoAirPax.Freq_Fight_num;

                        moeObj.PaxAirList.Add(_tbPeoAirPax);
                    }
                }

                moeObj.TicketList = new tbPEOTKTs();
                if (iurWS.Tickets != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKT _frtbPEOTKT in iurWS.Tickets)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT _tbPEOTKT = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT();
                        _tbPEOTKT.CompanyCode = _frtbPEOTKT.CompanyCode;
                        _tbPEOTKT.Ticket = _frtbPEOTKT.Ticket;
                        _tbPEOTKT.TktSeq = _frtbPEOTKT.TktSeq;
                        _tbPEOTKT.BKGREF = _frtbPEOTKT.BKGREF;
                        _tbPEOTKT.PNR = _frtbPEOTKT.PNR;
                        _tbPEOTKT.CltCode = _frtbPEOTKT.CltCode;
                        _tbPEOTKT.SuppCode = _frtbPEOTKT.SuppCode;
                        _tbPEOTKT.InvNum = _frtbPEOTKT.InvNum;
                        _tbPEOTKT.Jqty = _frtbPEOTKT.Jqty;
                        _tbPEOTKT.AirCode = _frtbPEOTKT.AirCode;
                        _tbPEOTKT.Airline = _frtbPEOTKT.Airline;
                        _tbPEOTKT.PaxName = _frtbPEOTKT.PaxName;
                        _tbPEOTKT.DepartOn = _frtbPEOTKT.DepartOn;
                        _tbPEOTKT.COD1 = _frtbPEOTKT.COD1;
                        _tbPEOTKT.COD2 = _frtbPEOTKT.COD2;
                        _tbPEOTKT.COD3 = _frtbPEOTKT.COD3;
                        _tbPEOTKT.COD7 = _frtbPEOTKT.COD7;
                        _tbPEOTKT.Commission = _frtbPEOTKT.Commission;
                        _tbPEOTKT.LocalFareCurr = _frtbPEOTKT.LocalFareCurr;
                        _tbPEOTKT.LocalFareAmt = _frtbPEOTKT.LocalFareAmt;
                        _tbPEOTKT.ForeignFareCurr = _frtbPEOTKT.ForeignFareCurr;
                        _tbPEOTKT.ForeignFareAmt = _frtbPEOTKT.ForeignFareAmt;
                        _tbPEOTKT.Discount = _frtbPEOTKT.Discount;
                        _tbPEOTKT.FormPay = _frtbPEOTKT.FormPay;
                        _tbPEOTKT.Routing = _frtbPEOTKT.Routing;
                        _tbPEOTKT.FullCurr = _frtbPEOTKT.FullCurr;
                        _tbPEOTKT.FullFare = _frtbPEOTKT.FullFare;
                        _tbPEOTKT.PaidCurr = _frtbPEOTKT.PaidCurr;
                        _tbPEOTKT.PaidFare = _frtbPEOTKT.PaidFare;
                        _tbPEOTKT.LowCurr = _frtbPEOTKT.LowCurr;
                        _tbPEOTKT.LowFare = _frtbPEOTKT.LowFare;
                        _tbPEOTKT.NetCurr = _frtbPEOTKT.NetCurr;
                        _tbPEOTKT.NetFare = _frtbPEOTKT.NetFare;
                        _tbPEOTKT.FareBasis = _frtbPEOTKT.FareBasis;
                        _tbPEOTKT.FareType = _frtbPEOTKT.FareType;
                        _tbPEOTKT.TotalTax = _frtbPEOTKT.TotalTax;
                        _tbPEOTKT.Security = _frtbPEOTKT.Security;
                        _tbPEOTKT.QSurcharge = _frtbPEOTKT.QSurcharge;
                        _tbPEOTKT.BranchCode = _frtbPEOTKT.BranchCode;
                        _tbPEOTKT.TeamCode = _frtbPEOTKT.TeamCode;
                        _tbPEOTKT.IssueOn = _frtbPEOTKT.IssueOn;
                        _tbPEOTKT.IssueBy = _frtbPEOTKT.IssueBy;
                        _tbPEOTKT.CreateOn = _frtbPEOTKT.CreateOn;
                        _tbPEOTKT.CreateBy = _frtbPEOTKT.CreateBy;
                        _tbPEOTKT.UpdateOn = _frtbPEOTKT.UpdateOn;
                        _tbPEOTKT.UpdateBy = _frtbPEOTKT.UpdateBy;
                        _tbPEOTKT.VoidOn = _frtbPEOTKT.VoidOn;
                        _tbPEOTKT.VoidBy = _frtbPEOTKT.VoidBy;
                        _tbPEOTKT.voidTeam = _frtbPEOTKT.voidTeam;
                        _tbPEOTKT.voidreason = _frtbPEOTKT.voidreason;
                        _tbPEOTKT.TaxAmount1 = _frtbPEOTKT.TaxAmount1;
                        _tbPEOTKT.TaxID1 = _frtbPEOTKT.TaxID1;
                        _tbPEOTKT.TaxAmount2 = _frtbPEOTKT.TaxAmount2;
                        _tbPEOTKT.TaxID2 = _frtbPEOTKT.TaxID2;
                        _tbPEOTKT.TaxAmount3 = _frtbPEOTKT.TaxAmount3;
                        _tbPEOTKT.TaxID3 = _frtbPEOTKT.TaxID3;
                        _tbPEOTKT.FareCalType = _frtbPEOTKT.FareCalType;
                        _tbPEOTKT.CompressPrintInd = _frtbPEOTKT.CompressPrintInd;
                        _tbPEOTKT.FareCalData = _frtbPEOTKT.FareCalData;
                        _tbPEOTKT.pseudo = _frtbPEOTKT.pseudo;
                        _tbPEOTKT.sinein = _frtbPEOTKT.sinein;
                        _tbPEOTKT.AirSeg = _frtbPEOTKT.AirSeg;
                        _tbPEOTKT.TransferFlag = _frtbPEOTKT.TransferFlag;
                        _tbPEOTKT.nst = _frtbPEOTKT.nst;
                        _tbPEOTKT.SOURCESYSTEM = _frtbPEOTKT.SOURCESYSTEM;
                        _tbPEOTKT.commission_per = _frtbPEOTKT.commission_per;
                        _tbPEOTKT.add_payment = _frtbPEOTKT.add_payment;
                        _tbPEOTKT.iss_off_code = _frtbPEOTKT.iss_off_code;
                        _tbPEOTKT.tour_code = _frtbPEOTKT.tour_code;
                        _tbPEOTKT.org_iss = _frtbPEOTKT.org_iss;
                        _tbPEOTKT.iss_in_exchange = _frtbPEOTKT.iss_in_exchange;
                        _tbPEOTKT.ControlNum = _frtbPEOTKT.ControlNum;
                        _tbPEOTKT.ENDORSEMENTS = _frtbPEOTKT.ENDORSEMENTS;
                        _tbPEOTKT.AirReason = _frtbPEOTKT.AirReason;
                        _tbPEOTKT.Destination = _frtbPEOTKT.Destination;
                        _tbPEOTKT.INTDOM = _frtbPEOTKT.INTDOM;
                        _tbPEOTKT.PAXAIR = _frtbPEOTKT.PAXAIR;
                        _tbPEOTKT.Class = _frtbPEOTKT.Class;
                        _tbPEOTKT.tkttype = _frtbPEOTKT.tkttype;
                        _tbPEOTKT.SellCurr = _frtbPEOTKT.SellCurr;
                        _tbPEOTKT.SellFare = _frtbPEOTKT.SellFare;
                        _tbPEOTKT.CorpCurr = _frtbPEOTKT.CorpCurr;
                        _tbPEOTKT.CorpFare = _frtbPEOTKT.CorpFare;
                        _tbPEOTKT.COD4 = _frtbPEOTKT.COD4;
                        _tbPEOTKT.COD5 = _frtbPEOTKT.COD5;
                        _tbPEOTKT.COD6 = _frtbPEOTKT.COD6;
                        _tbPEOTKT.tranflag = _frtbPEOTKT.tranflag;

                        moeObj.TicketList.Add(_tbPEOTKT);
                    }
                }

                moeObj.TicketDetailList = new tbPEOTKTDETAILs();
                if (iurWS.TicketDetails != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbPEOTKTDETAIL _frtbPEOTKTDETAIL in iurWS.TicketDetails)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAIL _tbPEOTKTDETAIL = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKTDETAIL();
                        _tbPEOTKTDETAIL.CompanyCode = _frtbPEOTKTDETAIL.CompanyCode;
                        _tbPEOTKTDETAIL.Ticket = _frtbPEOTKTDETAIL.Ticket;
                        _tbPEOTKTDETAIL.AirCode = _frtbPEOTKTDETAIL.AirCode;
                        _tbPEOTKTDETAIL.TktSeq = _frtbPEOTKTDETAIL.TktSeq;
                        _tbPEOTKTDETAIL.SegNum = _frtbPEOTKTDETAIL.SegNum;
                        _tbPEOTKTDETAIL.Airline = _frtbPEOTKTDETAIL.Airline;
                        _tbPEOTKTDETAIL.Flight = _frtbPEOTKTDETAIL.Flight;
                        _tbPEOTKTDETAIL.Class = _frtbPEOTKTDETAIL.Class;
                        _tbPEOTKTDETAIL.Seat = _frtbPEOTKTDETAIL.Seat;
                        _tbPEOTKTDETAIL.DepartCity = _frtbPEOTKTDETAIL.DepartCity;
                        _tbPEOTKTDETAIL.ArrivalCity = _frtbPEOTKTDETAIL.ArrivalCity;
                        _tbPEOTKTDETAIL.DepartDATE = _frtbPEOTKTDETAIL.DepartDATE;
                        _tbPEOTKTDETAIL.DepartTime = _frtbPEOTKTDETAIL.DepartTime;
                        _tbPEOTKTDETAIL.ArrDATE = _frtbPEOTKTDETAIL.ArrDATE;
                        _tbPEOTKTDETAIL.ArrTime = _frtbPEOTKTDETAIL.ArrTime;
                        _tbPEOTKTDETAIL.ElapsedTime = _frtbPEOTKTDETAIL.ElapsedTime;
                        _tbPEOTKTDETAIL.StopOverNum = _frtbPEOTKTDETAIL.StopOverNum;
                        _tbPEOTKTDETAIL.StopOverCity = _frtbPEOTKTDETAIL.StopOverCity;
                        _tbPEOTKTDETAIL.FareAmt = _frtbPEOTKTDETAIL.FareAmt;
                        _tbPEOTKTDETAIL.FareBasic = _frtbPEOTKTDETAIL.FareBasic;
                        _tbPEOTKTDETAIL.Commission = _frtbPEOTKTDETAIL.Commission;
                        _tbPEOTKTDETAIL.ValidStart = _frtbPEOTKTDETAIL.ValidStart;
                        _tbPEOTKTDETAIL.ValidEnd = _frtbPEOTKTDETAIL.ValidEnd;
                        _tbPEOTKTDETAIL.CreateOn = _frtbPEOTKTDETAIL.CreateOn;
                        _tbPEOTKTDETAIL.CreateBy = _frtbPEOTKTDETAIL.CreateBy;
                        _tbPEOTKTDETAIL.UpdateOn = _frtbPEOTKTDETAIL.UpdateOn;
                        _tbPEOTKTDETAIL.UpdateBy = _frtbPEOTKTDETAIL.UpdateBy;
                        _tbPEOTKTDETAIL.EQP = _frtbPEOTKTDETAIL.EQP;
                        _tbPEOTKTDETAIL.Service = _frtbPEOTKTDETAIL.Service;
                        _tbPEOTKTDETAIL.status = _frtbPEOTKTDETAIL.status;
                        _tbPEOTKTDETAIL.fare_basis_code = _frtbPEOTKTDETAIL.fare_basis_code;
                        _tbPEOTKTDETAIL.DepartTerm = _frtbPEOTKTDETAIL.DepartTerm;
                        _tbPEOTKTDETAIL.ArrivalTerm = _frtbPEOTKTDETAIL.ArrivalTerm;
                        _tbPEOTKTDETAIL.freq_fight_num = _frtbPEOTKTDETAIL.freq_fight_num;
                        _tbPEOTKTDETAIL.AirlinePNR = _frtbPEOTKTDETAIL.AirlinePNR;

                        moeObj.TicketDetailList.Add(_tbPEOTKTDETAIL);
                    }
                }

                moeObj.TranList = new tbpeotranss();
                if (iurWS.Trans != null)
                {
                    foreach (Westminster.MOE.DAL.GatewayWebSvc.tbpeotrans _frtbpeotrans in iurWS.Trans)
                    {
                        Westminster.MOE.Entity.DALEntity.Gateway.tbpeotrans _tbpeotrans = new Westminster.MOE.Entity.DALEntity.Gateway.tbpeotrans();
                        _tbpeotrans.bkgref = _frtbpeotrans.bkgref;
                        _tbpeotrans.companycode = _frtbpeotrans.companycode;
                        _tbpeotrans.PAXNUM = _frtbpeotrans.PAXNUM;
                        _tbpeotrans.segnum = _frtbpeotrans.segnum;
                        _tbpeotrans.CITYCode = _frtbpeotrans.CITYCode;
                        _tbpeotrans.CarType = _frtbpeotrans.CarType;
                        _tbpeotrans.DeptTime = _frtbpeotrans.DeptTime;
                        _tbpeotrans.FromType = _frtbpeotrans.FromType;
                        _tbpeotrans.FromAdd = _frtbpeotrans.FromAdd;
                        _tbpeotrans.ToType = _frtbpeotrans.ToType;
                        _tbpeotrans.ToAdd = _frtbpeotrans.ToAdd;
                        _tbpeotrans.FlightDetail = _frtbpeotrans.FlightDetail;
                        _tbpeotrans.CarNo = _frtbpeotrans.CarNo;
                        _tbpeotrans.ConfirmedBy = _frtbpeotrans.ConfirmedBy;
                        _tbpeotrans.tempkey = _frtbpeotrans.tempkey;
                        _tbpeotrans.ConfirmedDate = _frtbpeotrans.ConfirmedDate;
                        _tbpeotrans.createon = _frtbpeotrans.createon;
                        _tbpeotrans.createby = _frtbpeotrans.createby;
                        _tbpeotrans.updateon = _frtbpeotrans.updateon;
                        _tbpeotrans.updateby = _frtbpeotrans.updateby;

                        moeObj.TranList.Add(_tbpeotrans);
                    }
                }

                if (iurWS.Reference != null)
                {
                    moeObj.Nyref = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEONYREF();
                    moeObj.Nyref.REFID = iurWS.Reference.REFID;
                    moeObj.Nyref.COMPANYCODE = iurWS.Reference.COMPANYCODE;
                    moeObj.Nyref.BKGREF = iurWS.Reference.BKGREF;
                    moeObj.Nyref.STAFFCODE = iurWS.Reference.STAFFCODE;
                    moeObj.Nyref.TEAMCODE = iurWS.Reference.TEAMCODE;
                    moeObj.Nyref.ACTIONCODE = iurWS.Reference.ACTIONCODE;
                    moeObj.Nyref.CLTCODE = iurWS.Reference.CLTCODE;
                    moeObj.Nyref.SUPPCODE = iurWS.Reference.SUPPCODE;
                    moeObj.Nyref.DocNum = iurWS.Reference.DocNum;
                    moeObj.Nyref.SellCurr = iurWS.Reference.SellCurr;
                    moeObj.Nyref.SellAmt = iurWS.Reference.SellAmt;
                    moeObj.Nyref.SellTaxCurr = iurWS.Reference.SellTaxCurr;
                    moeObj.Nyref.SellTaxAmt = iurWS.Reference.SellTaxAmt;
                    moeObj.Nyref.CostCurr = iurWS.Reference.CostCurr;
                    moeObj.Nyref.CostAmt = iurWS.Reference.CostAmt;
                    moeObj.Nyref.CostTaxCurr = iurWS.Reference.CostTaxCurr;
                    moeObj.Nyref.CostTaxAmt = iurWS.Reference.CostTaxAmt;
                    moeObj.Nyref.Description = iurWS.Reference.Description;
                    moeObj.Nyref.CREATEON = iurWS.Reference.CREATEON;
                    moeObj.Nyref.CREATEBY = iurWS.Reference.CREATEBY;
                }

                if (iurWS.VoidTicket != null)
                {
                    moeObj.VoidTicket = new Westminster.MOE.Entity.DALEntity.Gateway.tbPEOTKT();
                    moeObj.VoidTicket.CompanyCode = iurWS.VoidTicket.CompanyCode;
                    moeObj.VoidTicket.Ticket = iurWS.VoidTicket.Ticket;
                    moeObj.VoidTicket.TktSeq = iurWS.VoidTicket.TktSeq;
                    moeObj.VoidTicket.BKGREF = iurWS.VoidTicket.BKGREF;
                    moeObj.VoidTicket.PNR = iurWS.VoidTicket.PNR;
                    moeObj.VoidTicket.CltCode = iurWS.VoidTicket.CltCode;
                    moeObj.VoidTicket.SuppCode = iurWS.VoidTicket.SuppCode;
                    moeObj.VoidTicket.InvNum = iurWS.VoidTicket.InvNum;
                    moeObj.VoidTicket.Jqty = iurWS.VoidTicket.Jqty;
                    moeObj.VoidTicket.AirCode = iurWS.VoidTicket.AirCode;
                    moeObj.VoidTicket.Airline = iurWS.VoidTicket.Airline;
                    moeObj.VoidTicket.PaxName = iurWS.VoidTicket.PaxName;
                    moeObj.VoidTicket.DepartOn = iurWS.VoidTicket.DepartOn;
                    moeObj.VoidTicket.COD1 = iurWS.VoidTicket.COD1;
                    moeObj.VoidTicket.COD2 = iurWS.VoidTicket.COD2;
                    moeObj.VoidTicket.COD3 = iurWS.VoidTicket.COD3;
                    moeObj.VoidTicket.COD7 = iurWS.VoidTicket.COD7;
                    moeObj.VoidTicket.Commission = iurWS.VoidTicket.Commission;
                    moeObj.VoidTicket.LocalFareCurr = iurWS.VoidTicket.LocalFareCurr;
                    moeObj.VoidTicket.LocalFareAmt = iurWS.VoidTicket.LocalFareAmt;
                    moeObj.VoidTicket.ForeignFareCurr = iurWS.VoidTicket.ForeignFareCurr;
                    moeObj.VoidTicket.ForeignFareAmt = iurWS.VoidTicket.ForeignFareAmt;
                    moeObj.VoidTicket.Discount = iurWS.VoidTicket.Discount;
                    moeObj.VoidTicket.FormPay = iurWS.VoidTicket.FormPay;
                    moeObj.VoidTicket.Routing = iurWS.VoidTicket.Routing;
                    moeObj.VoidTicket.FullCurr = iurWS.VoidTicket.FullCurr;
                    moeObj.VoidTicket.FullFare = iurWS.VoidTicket.FullFare;
                    moeObj.VoidTicket.PaidCurr = iurWS.VoidTicket.PaidCurr;
                    moeObj.VoidTicket.PaidFare = iurWS.VoidTicket.PaidFare;
                    moeObj.VoidTicket.LowCurr = iurWS.VoidTicket.LowCurr;
                    moeObj.VoidTicket.LowFare = iurWS.VoidTicket.LowFare;
                    moeObj.VoidTicket.NetCurr = iurWS.VoidTicket.NetCurr;
                    moeObj.VoidTicket.NetFare = iurWS.VoidTicket.NetFare;
                    moeObj.VoidTicket.FareBasis = iurWS.VoidTicket.FareBasis;
                    moeObj.VoidTicket.FareType = iurWS.VoidTicket.FareType;
                    moeObj.VoidTicket.TotalTax = iurWS.VoidTicket.TotalTax;
                    moeObj.VoidTicket.Security = iurWS.VoidTicket.Security;
                    moeObj.VoidTicket.QSurcharge = iurWS.VoidTicket.QSurcharge;
                    moeObj.VoidTicket.BranchCode = iurWS.VoidTicket.BranchCode;
                    moeObj.VoidTicket.TeamCode = iurWS.VoidTicket.TeamCode;
                    moeObj.VoidTicket.IssueOn = iurWS.VoidTicket.IssueOn;
                    moeObj.VoidTicket.IssueBy = iurWS.VoidTicket.IssueBy;
                    moeObj.VoidTicket.CreateOn = iurWS.VoidTicket.CreateOn;
                    moeObj.VoidTicket.CreateBy = iurWS.VoidTicket.CreateBy;
                    moeObj.VoidTicket.UpdateOn = iurWS.VoidTicket.UpdateOn;
                    moeObj.VoidTicket.UpdateBy = iurWS.VoidTicket.UpdateBy;
                    moeObj.VoidTicket.VoidOn = iurWS.VoidTicket.VoidOn;
                    moeObj.VoidTicket.VoidBy = iurWS.VoidTicket.VoidBy;
                    moeObj.VoidTicket.voidTeam = iurWS.VoidTicket.voidTeam;
                    moeObj.VoidTicket.voidreason = iurWS.VoidTicket.voidreason;
                    moeObj.VoidTicket.TaxAmount1 = iurWS.VoidTicket.TaxAmount1;
                    moeObj.VoidTicket.TaxID1 = iurWS.VoidTicket.TaxID1;
                    moeObj.VoidTicket.TaxAmount2 = iurWS.VoidTicket.TaxAmount2;
                    moeObj.VoidTicket.TaxID2 = iurWS.VoidTicket.TaxID2;
                    moeObj.VoidTicket.TaxAmount3 = iurWS.VoidTicket.TaxAmount3;
                    moeObj.VoidTicket.TaxID3 = iurWS.VoidTicket.TaxID3;
                    moeObj.VoidTicket.FareCalType = iurWS.VoidTicket.FareCalType;
                    moeObj.VoidTicket.CompressPrintInd = iurWS.VoidTicket.CompressPrintInd;
                    moeObj.VoidTicket.FareCalData = iurWS.VoidTicket.FareCalData;
                    moeObj.VoidTicket.pseudo = iurWS.VoidTicket.pseudo;
                    moeObj.VoidTicket.sinein = iurWS.VoidTicket.sinein;
                    moeObj.VoidTicket.AirSeg = iurWS.VoidTicket.AirSeg;
                    moeObj.VoidTicket.TransferFlag = iurWS.VoidTicket.TransferFlag;
                    moeObj.VoidTicket.nst = iurWS.VoidTicket.nst;
                    moeObj.VoidTicket.SOURCESYSTEM = iurWS.VoidTicket.SOURCESYSTEM;
                    moeObj.VoidTicket.commission_per = iurWS.VoidTicket.commission_per;
                    moeObj.VoidTicket.add_payment = iurWS.VoidTicket.add_payment;
                    moeObj.VoidTicket.iss_off_code = iurWS.VoidTicket.iss_off_code;
                    moeObj.VoidTicket.tour_code = iurWS.VoidTicket.tour_code;
                    moeObj.VoidTicket.org_iss = iurWS.VoidTicket.org_iss;
                    moeObj.VoidTicket.iss_in_exchange = iurWS.VoidTicket.iss_in_exchange;
                    moeObj.VoidTicket.ControlNum = iurWS.VoidTicket.ControlNum;
                    moeObj.VoidTicket.ENDORSEMENTS = iurWS.VoidTicket.ENDORSEMENTS;
                    moeObj.VoidTicket.AirReason = iurWS.VoidTicket.AirReason;
                    moeObj.VoidTicket.Destination = iurWS.VoidTicket.Destination;
                    moeObj.VoidTicket.INTDOM = iurWS.VoidTicket.INTDOM;
                    moeObj.VoidTicket.PAXAIR = iurWS.VoidTicket.PAXAIR;
                    moeObj.VoidTicket.Class = iurWS.VoidTicket.Class;
                    moeObj.VoidTicket.tkttype = iurWS.VoidTicket.tkttype;
                    moeObj.VoidTicket.SellCurr = iurWS.VoidTicket.SellCurr;
                    moeObj.VoidTicket.SellFare = iurWS.VoidTicket.SellFare;
                    moeObj.VoidTicket.CorpCurr = iurWS.VoidTicket.CorpCurr;
                    moeObj.VoidTicket.CorpFare = iurWS.VoidTicket.CorpFare;
                    moeObj.VoidTicket.COD4 = iurWS.VoidTicket.COD4;
                    moeObj.VoidTicket.COD5 = iurWS.VoidTicket.COD5;
                    moeObj.VoidTicket.COD6 = iurWS.VoidTicket.COD6;
                    moeObj.VoidTicket.tranflag = iurWS.VoidTicket.tranflag;
                }
            }
            return moeObj;
        }
    }
}
