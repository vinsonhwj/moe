﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using Westminster.MOE.DAL;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Lib;
using Westminster.MOE.Lib.Configuration;
using System.Data.SqlClient;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;
using System.Data;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.Transfer
{
    public class DAAutoDocument : Westminster.MOE.IDAL.Transfer.IAutoDocument
    {
       // public string eMosConnString { get; private set; }
        public WebServiceConfig GatewaySvc { get; private set; }
        private IConnection conn;
        public DAAutoDocument()
        {

        }

        public DAAutoDocument(IConnection conn, WebServiceConfig GatewaySvc)
        {
            this.conn = conn;
            this.GatewaySvc = GatewaySvc;
        }

        public Westminster.MOE.Entity.DALEntity.Gateway.IURObject GetIURObjectsByPNR(string companyCode, string PNR, string bkgref, string SourceSystem, DateTime createOn)
        {
            Westminster.MOE.DAL.GatewayWebSvc.URSoapClient svc = new Westminster.MOE.DAL.GatewayWebSvc.URSoapClient(GatewaySvc.Name, GatewaySvc.Url);
            return Westminster.MOE.DAL.GatewayWebSvc.ConvertToGateway.ToMOEObject(svc.GetIURDataForATS(companyCode, PNR, bkgref, SourceSystem, createOn));
        }
        public tbPEOMISs GetPEOMISList(string companyCode, string TSANumber)
        {
            return new DAPEOMIS(conn).GetListByTSANumber(companyCode, TSANumber);
        }

        public tbPEOMSTRs GetMasterListByObject(tbPEOMSTR master, DateTime createOn)
        {
            return new DAPEOMSTR(conn).GetListByObject(master, createOn);
        }
        public tbPEOMSTR GetPEOMSTR(string bkgref, string companycode)
        {
            return new DAPEOMSTR(conn).GetEntity(companycode, bkgref);
        }

        public tbCustomer GetCustomer(string companyCode, string cltCode)
        {
            return new DACustomer(conn).GetEntity(companyCode, cltCode);
        }

        public bool ExistUnVoidInvoicedTickets(string companyCode, string bkgref, string ticket)
        {
            string CommandText = "select * from (select peoinvtkt.*, peoinv.VOIDON from peoinv inner join peoinvtkt on peoinv.companycode = peoinvtkt.companycode and peoinv.invnum = peoinvtkt.invnum) r where [CompanyCode] = @companyCode and [Ticket] = @Ticket and [bkgref] = @bkgref and [VOIDON] is null and [InvNum] like N'%I%' ";
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@companyCode", companyCode));
            param.Add(new SqlParameter("@bkgref", bkgref));
            param.Add(new SqlParameter("@Ticket", ticket));

            return new DAPEOINVTKT(conn).ExecuteSql(CommandText, param, 300) > 0;

        }
        public bool ExistUnVoidInvoicedTicketRefs(string companyCode, string bkgref, string ticket)
        {
            string CommandText = "select * from (select peoinvtktref.*, peoinv.VOIDON from peoinv inner join peoinvtktref on peoinv.companycode = peoinvtktref.companycode and peoinv.invnum = peoinvtktref.invnum) r where [CompanyCode] = @companyCode and [Ticket] = @Ticket and [bkgref] = @bkgref and [VOIDON] is null and [InvNum] like N'%I%' ";
            List<IDbDataParameter> param = new List<IDbDataParameter>();
            param.Add(new SqlParameter("@companyCode", companyCode));
            param.Add(new SqlParameter("@bkgref", bkgref));
            param.Add(new SqlParameter("@Ticket", ticket));

            return new DAPEOINVTKTref(conn).ExecuteSql(CommandText, param, 300) > 0;

        }
        public tbPEOMIS GetPEOMIS(string bkgref, string companyCode, string SEGNUM)
        {
            return new DAPEOMIS(conn).GetEntity(companyCode, bkgref, SEGNUM);
        }

        public tbPEOMISs GetUnvoidPeoMisList(string companycode, string bkgref)
        {
            return new DAPEOMIS(conn).GetListWithUnvoid(companycode, bkgref);
        }

        public tbPEOPAXs GetAllPaxByBkgref(string companyCode, string bkgref)
        {
            return new DAPEOPAX(conn).GetListByBKGREF(companyCode, bkgref);
        }

        public bool Com_PreferenceIsY(string compangyCode, string field)
        {
            return new DACom_Preference(conn).IsY(compangyCode, field);
        }

        public int GetOtherCount(string compangyCode, string bkgref)
        {
            return new DAPEOOTHER(conn).GetCountByBkgref(compangyCode, bkgref);
        }
        public bool ExistBooking(string companyCode, string bkgRef)
        {
            eMosDB.Table.DAPEOMSTR daPEOMSTR = new eMosDB.Table.DAPEOMSTR(conn);
            return daPEOMSTR.Exists(companyCode, bkgRef);
        }
        public bool InserteMosObjects(eMosBooking eMos, string staffCode)
        {
            SqlList sqlList = new SqlList();
            if (!ExistBooking(eMos.CompanyCode, eMos.Bkgref))
            {
                sqlList.Add(DAPEOMSTR.AddSqlEntity(eMos.PeoMstr));
            }
            if (!string.IsNullOrEmpty(eMos.PeoMis.COD1))
            {
                if (ExistMis(eMos.PeoMis.BKGREF, eMos.PeoMis.COMPANYCODE, eMos.PeoMis.SEGNUM))
                {
                    sqlList.Add(DAPEOMIS.AddSqlEntity(eMos.PeoMis));
                }
            }

            foreach (tbPEOPAX pax in eMos.PeoPaxList)
            {
                sqlList.Add(DAPEOPAX.AddSqlEntity(pax));
            }

            FilterParams deldtfp = new FilterParams();
            deldtfp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
            deldtfp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, eMos.Bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            sqlList.Add(DAPEOAIRDETAIL.DeleteSqlEntity(deldtfp));


            FilterParams delfp = new FilterParams();
            delfp.AddParam(tbPEOAIR.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
            delfp.AddParam(tbPEOAIR.Fields.BkgRef, eMos.Bkgref, Enums.Relation.Equal, Enums.Expression.AND);
            sqlList.Add(DAPEOAIR.DeleteSqlEntity(delfp));

            foreach (eMosAirlineSegment air in eMos.AirlineSegments)
            {
                sqlList.Add(DAPEOAIR.AddSqlEntity(air.PeoAir));
                foreach (tbPEOAIRDETAIL airDt in air.PeoAirDetails)
                {
                    sqlList.Add(DAPEOAIRDETAIL.AddSqlEntity(airDt));
                }
            }
            foreach (eMosTicketSegment tkt in eMos.TicketSegments)
            {
                sqlList.Add(DAPEOTKT.AddSqlEntity(tkt.PeoTkt));
                foreach (tbPEOTKTDETAIL tktDT in tkt.PeoTktDetails)
                {
                    sqlList.Add(DAPEOTKTDETAIL.AddSqlEntity(tktDT));
                }
            }
            foreach (tbPEOTKT vt in (eMos.VoidTicket))
            {
                sqlList.Add(DAPEOTKT.AddSqlEntity(vt));
            }
            foreach (eMosOtherSegment oth in eMos.OtherSegments)
            {
                sqlList.Add(DAPEOOTHER.AddSqlEntity(oth.PeoOther));
                foreach (tbPEOOTHERDETAIL othDt in oth.PeoOtherDetails)
                {
                    sqlList.Add(DAPEOOTHERDETAIL.AddSqlEntity(othDt));
                }
            }
            CallCalPeomstrSummary(eMos.CompanyCode, eMos.Bkgref, staffCode);

            return new DAPEOMSTR(conn).ExecuteSqlTran(sqlList);
        }
        public bool ExistMis(string bkgRef, string companyCode, string SEGNUM)
        {
            return new DAPEOMIS(conn).Exists(bkgRef, companyCode, SEGNUM);
        }

        public tbPEOAIRDETAILs GetFirstAirDetailList(string companycode, string bkgref)
        {
            return new DAPEOAIRDETAIL(conn).GetFirstList(companycode, bkgref);
        }

        public tbPEOTKTs GetTicketList(string companycode, string pnr)
        {
            return new DAPEOTKT(conn).GetList(companycode, pnr);
        }

        public void CallRecal_Air(string companyCode, string bkgRef, string segnum)
        {
            string sql = string.Format("EXEC dbo.sp_Recal_Air '{0}', '{1}', '{2}'"
                                       , bkgRef + "", companyCode + "", segnum + "");
            new DAPEOAIR(conn).ExecuteSql(sql);
        }
        public void CallPEOMSTRsummary_trigger(string companyCode, string bkgRef)
        {
            CallCalPeomstrSummary(companyCode, bkgRef, string.Empty);
        }
        public void CallCalPeomstrSummary(string companyCode, string bkgRef, string userID)
        {
            string sql = string.Format("EXEC dbo.sp_CalPeomstrSummary '{0}', '{1}', '{2}';"
                                        , companyCode + "", bkgRef + "", userID + "");
            new DAPEOMSTR(conn).ExecuteSql(sql);
        }
        public void CallProfitMarginChecking(string companyCode, string bkgRef, string userID, string teamCode)
        {
            string sql = string.Format("EXEC dbo.sp_CalPeomstrSummary '{0}', '{1}', '{2}';"
                                        + " EXEC SP_ProfitMarginChecking '{0}', '{1}', 'TKT', '{2}', '{3}', 'INV'"
                                        , companyCode + "", bkgRef + "", userID + "", teamCode + "");
            new DAPEOMSTR(conn).ExecuteSql(sql);
        }
        public void CallRecalTicketPaidFare(List<string> listInvoice, string companyCode)
        {
            if (listInvoice == null) return;
            foreach (string invoiceNum in listInvoice)
            {
                string sql = string.Format("EXEC dbo.sp_RecalTicketPaidFare '{0}', '{1}', 'INV'"
                                            , companyCode, invoiceNum);
                new DAPEOMSTR(conn).ExecuteSql(sql);
            }
        }
        public bool IsExistedInvoice(string companycode, string bkgref)
        {
            return new DAPEOINV(conn).ExistsInvoice(companycode, bkgref);
        }
        public tbPEOSTAFF GetStaff(string companycode, string staffcode)
        {
            return new DAPEOSTAFF(conn).GetEntity(companycode, staffcode);
        }
        public string GetNewBkGrefKey(string branchCode, string companyCode, DA_SP_Booking.ModuleType moduleCode)
        {
            return new DA_SP_Booking(conn).GetNum(companyCode, branchCode, moduleCode, 1);
        }
        public SqlEntity GetInsertSql(tbPEOINV tbPEOINV)
        {
            return DAPEOINV.AddSqlEntity(tbPEOINV);
        }
        public string InsertTsfrInvoice(Westminster.MOE.Entity.DALEntity.Transfer.TsfrInvoice invoice, InvoiceTypeEnum InvoiceType, Invoice_Types InvType)
        {
            SqlList sqlList = new SqlList();
            string myInvoiceNo = string.Empty;

            if (!string.IsNullOrEmpty(invoice.PeoInv.COMPANYCODE) && !string.IsNullOrEmpty(invoice.PeoInv.STAFFCODE))
            {
                tbPEOSTAFF uoStaff = GetStaff(invoice.PeoInv.COMPANYCODE.Trim(), invoice.PeoInv.STAFFCODE);
                if (uoStaff != null)
                {
                    string branchCode = uoStaff.BRANCHCODE.Trim();
                    myInvoiceNo = GetNewBkGrefKey(branchCode, invoice.PeoInv.COMPANYCODE.Trim(), DA_SP_Booking.ModuleType.VCHNUM);
                }
            }


            if (string.IsNullOrEmpty(myInvoiceNo))
            {
                string errorMsg = "Error occured on get invoice number";
                throw (new Exception(errorMsg));
            }
            invoice.PeoInv.INVNUM = myInvoiceNo;
            sqlList.Add(GetInsertSql(invoice.PeoInv));

            if (invoice.PEOINVSEG != null)
            {
                foreach (tbPEOINVSEG myPeoInvSeg in invoice.PEOINVSEG)
                {
                    myPeoInvSeg.INVNUM = myInvoiceNo;
                    sqlList.Add(DAPEOINVSEG.AddSqlEntity(myPeoInvSeg));
                }
            }
            if (invoice.PEOINVDETAIL != null)
            {
                foreach (tbPEOINVDETAIL myPeoInvDetail in invoice.PEOINVDETAIL)
                {
                    myPeoInvDetail.INVNUM = myInvoiceNo;
                    sqlList.Add(DAPEOINVDETAIL.AddSqlEntity(myPeoInvDetail));
                }
            }
            if (invoice.PEOINPAX != null)
            {
                foreach (tbPEOINPAX myUO_PEOINPAX in invoice.PEOINPAX)
                {
                    myUO_PEOINPAX.INVNUM = myInvoiceNo;
                    sqlList.Add(DAPEOINPAX.AddSqlEntity(myUO_PEOINPAX));
                }
            }
            if (invoice.PEOINVTKTref != null)
            {
                foreach (tbPEOINVTKTref myUO_PEOINVTKTref in invoice.PEOINVTKTref)
                {
                    myUO_PEOINVTKTref.InvNum = myInvoiceNo;
                    sqlList.Add(DAPEOINVTKTref.AddSqlEntity(myUO_PEOINVTKTref));
                }
            }
            if (invoice.PEOINVTKT != null)
            {
                foreach (tbPEOINVTKT myUO_PEOINVTKT in invoice.PEOINVTKT)
                {
                    myUO_PEOINVTKT.InvNum = myInvoiceNo;
                    sqlList.Add(DAPEOINVTKT.AddSqlEntity(myUO_PEOINVTKT));
                }
            }
            sqlList.Add(SetPeonyref(invoice.PeoInv, InvoiceType, InvType));

            try
            {
                new DAPEOINV(conn).ExecuteSqlTran(sqlList);
                return myInvoiceNo;
            }
            catch
            {
                throw;
            }
        }

        public DateTime GetDueDate(string companyCode, string invNum)
        {
            tbPEOINV inv = new DAPEOINV(conn).GetEntity(companyCode, invNum);
            if (inv != null)
            {
                tbpeocltcrlmt crlmt = new DApeocltcrlmt(conn).GetEntity(companyCode, inv.CLTCODE);
                if (crlmt != null)
                    return inv.CREATEON.AddDays((int)crlmt.crterms);
            }
            return DateTime.MinValue;
        }


        private SqlEntity SetPeonyref(tbPEOINV xPeoInv, InvoiceTypeEnum InvoiceType, Invoice_Types InvType)
        {
            DateTime myTransDate = xPeoInv.CREATEON;
            tbPEONYREF myPeonyRef = new tbPEONYREF();
            string myPeonyRefDescription;
            string myPeonyBackDateDescription = "";
            if (myTransDate.AddDays(1) < DateTime.Today)
            {
                myPeonyBackDateDescription = " + ' - Back Date to " + myTransDate.ToShortDateString() + "' ";
            }
            myPeonyRefDescription = "(SELL " + xPeoInv.SELLCURR + xPeoInv.SellExcludeTax.ToString() +
                " + Tax " + xPeoInv.TAXCURR + xPeoInv.TAXAMT.ToString() + " / " + xPeoInv.CLTCODE + ")" + myPeonyBackDateDescription;
            myPeonyRef.COMPANYCODE = xPeoInv.COMPANYCODE;
            myPeonyRef.BKGREF = xPeoInv.BKGREF;
            myPeonyRef.ACTIONCODE = GetActionCodeInvTypeCode(InvoiceType, true, xPeoInv.COMPANYCODE);
            myPeonyRef.CLTCODE = xPeoInv.CLTCODE + "";
            myPeonyRef.DocNum = xPeoInv.INVNUM + "";
            myPeonyRef.STAFFCODE = xPeoInv.STAFFCODE + "";
            myPeonyRef.TEAMCODE = xPeoInv.TEAMCODE + "";
            myPeonyRef.SellAmt = xPeoInv.SellExcludeTax;
            myPeonyRef.SellCurr = xPeoInv.SELLCURR + "";
            myPeonyRef.SellTaxAmt = xPeoInv.TAXAMT;
            myPeonyRef.SellTaxCurr = xPeoInv.TAXCURR + "";
            myPeonyRef.CREATEON = xPeoInv.CREATEON;
            myPeonyRef.CREATEBY = xPeoInv.CREATEBY;
            myPeonyRef.Description = myPeonyRefDescription;
            myPeonyRef.InvTypeCode = Parse2PeonyrefInvTypeCode(xPeoInv.RECINV, InvType);

            return DAPEONYREF.AddSqlEntity(myPeonyRef);
        }

        public string GetActionCodeInvTypeCode(InvoiceTypeEnum invoiceType, bool isActionCode, string companyCode)
        {
            switch (invoiceType)
            {
                case InvoiceTypeEnum.Normal:
                    if (isActionCode)
                    {
                        return "INV";
                    }
                    else
                    {
                        return "INOR";
                    }
                case InvoiceTypeEnum.Credit:
                    if (isActionCode)
                    {
                        return "INV";
                    }
                    else
                    {
                        if (Com_PreferenceIsY(companyCode, "INV_CCARD") && ExistCompanyCardTicket())
                        {
                            return "UCCP";
                        }
                        else
                        {
                            return "ICCR";
                        }
                    }
                case InvoiceTypeEnum.Deposit:
                    if (isActionCode)
                    {
                        return "DEP";
                    }
                    else
                    {
                        return "INOR";
                    }
                case InvoiceTypeEnum.Uatp:
                    if (isActionCode)
                    {
                        return "INV";
                    }
                    else
                    {
                        return "IUAT";
                    }
                default:
                    return "";
            }
        }
        public string Parse2PeonyrefInvTypeCode(string recinv, Invoice_Types invType)
        {
            string myInvType = "INOR";
            switch (invType)
            {
                case Invoice_Types.Normal:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RNOR" : "INOR";
                    break;

                case Invoice_Types.Deposit:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RNOR" : "INOR";
                    break;

                case Invoice_Types.CreditNotes:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RCRN" : "ICRN";
                    break;

                case Invoice_Types.DepositCreditNote:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RCRN" : "ICRN";
                    break;


                case Invoice_Types.UATP:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RUAT" : "IUAT";
                    break;

                case Invoice_Types.CreditCardSales:
                    myInvType = ("RPT".Equals(recinv.ToUpper())) ? "RCCR" : "ICCR";
                    break;

            }

            return myInvType;
        }
        /// <summary>
        /// 哩个方法好！
        /// </summary>
        /// <returns></returns>
        public bool ExistCompanyCardTicket()
        {
            return false;
        }

        #region IAutoDocument Members



        #endregion



    }
}
