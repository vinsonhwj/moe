﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using Westminster.MOE.DAL;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Lib;
using Westminster.MOE.Lib.Configuration;
using System.Data;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.Transfer
{
    public class DALinkPNR : IDAL.Transfer.ILinkPNR
    {
        #region Property
        public IConnection InnerConnection { get; set; }
        public WebServiceConfig GatewaySvc { get; private set; }

        #region DataAccess
        Westminster.MOE.DAL.GatewayWebSvc.URSoapClient svc;

        DAPEOMSTR daPEOMSTR;
        DAPEOPAX daPEOPAX;
        DAPEOAIR daPEOAIR;
        DAPEOAIRDETAIL daPEOAIRDETAIL;
        DAPEOTKTDETAIL daPEOTKTDETAIL;
        DAPEOTKT daPEOTKT;
        DACustomer daCust;
        Maintain.DACompanyPreference daComPref;
        #endregion
        #endregion

        public DALinkPNR(IConnection conn, WebServiceConfig GatewaySvc)
        {
            InnerConnection = conn;
            this.GatewaySvc = GatewaySvc;

            svc = new Westminster.MOE.DAL.GatewayWebSvc.URSoapClient(GatewaySvc.Name, GatewaySvc.Url);
            daPEOMSTR = new DAPEOMSTR(conn);
            daPEOPAX = new DAPEOPAX(conn);
            daPEOAIR = new DAPEOAIR(conn);
            daPEOAIRDETAIL = new DAPEOAIRDETAIL(conn);
            daPEOTKT = new DAPEOTKT(conn);
            daPEOTKTDETAIL = new DAPEOTKTDETAIL(conn);
            daCust = new DACustomer(conn);
            daComPref = new Westminster.MOE.DAL.Maintain.DACompanyPreference(conn);
        }

        #region ILinkPNR Members
        public Westminster.MOE.Entity.DALEntity.Gateway.IURObject GetIURObjects(string companyCode, string bkgRef)
        {
            return Westminster.MOE.DAL.GatewayWebSvc.ConvertToGateway.ToMOEObject(svc.GetIURData(companyCode, bkgRef, string.Empty, DateTime.MinValue));
        }
        public eMosObjects GeteeMosObjects(string companyCode, string bkgRef, string sourcePnr)
        {
            eMosObjects booking = new eMosObjects(true);

            booking.PEOMSTR = daPEOMSTR.GetEntity(companyCode, bkgRef);
            booking.CompanyCode = companyCode;
            booking.BkgRef = bkgRef;
            booking.PaxList = daPEOPAX.GetPaxIgnoreOriginal(companyCode, bkgRef, sourcePnr);
            booking.AirList = daPEOAIR.GetListByBKGREF(companyCode, bkgRef);
            booking.AirDetailList = daPEOAIRDETAIL.GetList(companyCode, bkgRef);

            return booking;
        }
        public tbPEOTKTs GetTicketList(string companycode, string pnr)
        {
            return daPEOTKT.GetList(companycode, pnr);
        }
        /// <summary>
        /// Get First IUR Air Detail 
        /// </summary>
        /// <param name="companycode"></param>
        /// <param name="bkgref"></param>
        /// <returns></returns>
        public tbPEOAIRDETAILs GetFirstAirDetailList(string companycode, string bkgref)
        {
            if (daPEOAIR.IsFirstIUR(companycode, bkgref))
                return daPEOAIRDETAIL.GetFirstList(companycode, bkgref);
            else
                return new tbPEOAIRDETAILs();
        }
        public tbPEOPAXs GetPaxList(string companycode, string bkgref)
        {
            return daPEOPAX.GetList(companycode, bkgref);
        }
        public tbCustomer GetCustomer(string companycode, string cltcode)
        {
            return daCust.GetEntity(companycode, cltcode);
        }

        public bool ExistBooking(string companyCode, string bkgRef)
        {
            return daPEOMSTR.Exists(companyCode, bkgRef);
        }
        public bool UpdateFlag(string companyCode, string bkgref, bool succeed)
        {
            return svc.UpdateIURFlag(companyCode, bkgref, succeed);
        }
        //public bool InsertDB(BookingInfo booking)
        //{
        //    if (booking != null)
        //    {
        //        SqlList sqlList = new SqlList();

        //        sqlList.Add(eMosDB.Table.DAPEOPAX.DeleteByBkgref(booking.CompanyCode, booking.BkgRef, booking.SourcrPnr));
        //        tbPEOAIRs airList = daPEOAIR.GetOriginalList(booking.CompanyCode, booking.BkgRef, booking.SourcrPnr);
        //        List<string> segNumList = new List<string>();
        //        foreach (tbPEOAIR air in airList)
        //        {
        //            segNumList.Add(air.SegNum);
        //        }
        //        sqlList.Add(eMosDB.Table.DAPEOAIRDETAIL.DeleteByBkgref(booking.CompanyCode, booking.BkgRef, segNumList));
        //        sqlList.Add(eMosDB.Table.DAPEOAIR.DeleteByBkgref(booking.CompanyCode, booking.BkgRef));


        //        foreach (tbPEOPAX pax in booking.PaxList)
        //        {
        //            sqlList.Add(eMosDB.Table.DAPEOPAX.AddSqlEntity(pax));
        //        }
        //        foreach (tbPEOAIR air in booking.AirList)
        //        {
        //            sqlList.Add(eMosDB.Table.DAPEOAIR.AddSqlEntity(air));
        //        }
        //        foreach (tbPEOAIRDETAIL airDt in booking.AirDetailList)
        //        {
        //            sqlList.Add(eMosDB.Table.DAPEOAIRDETAIL.AddSqlEntity(airDt));
        //        }
        //        return InnerConnection.ExecuteSqlList(sqlList) > 0;
        //    }
        //    return false;
        //}
        public void InserteMosObjects(eMosObjects eMos)
        {
            if (eMos.isCreate)
            {
                daPEOMSTR.Add(eMos.PEOMSTR);
                foreach (tbPEOPAX pax in eMos.PaxList)
                {
                    daPEOPAX.Add(pax);
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    daPEOAIR.Add(air);
                }
                foreach (tbPEOAIRDETAIL airDt in eMos.AirDetailList)
                {
                    daPEOAIRDETAIL.Add(airDt);
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    InnerConnection.ExecuteSql(CallRecal_Air(air.CompanyCode, air.BkgRef, air.SegNum));
                }
                foreach (tbPEOTKT tkt in eMos.TicketList)
                {
                    daPEOTKT.Add(tkt);
                }
                foreach (tbPEOTKTDETAIL tktDt in eMos.TicketDetailList)
                {
                    daPEOTKTDETAIL.Add(tktDt);
                }
            }
            else
            {
                tbPEOPAXs dbPaxs = daPEOPAX.GetListByBKGREF(eMos.CompanyCode, eMos.BkgRef);
                foreach (tbPEOPAX pax in eMos.PaxList)
                {
                    if (dbPaxs.Exists(c => c.SEGNUM == pax.SEGNUM))
                        daPEOPAX.Update(pax, pax.BKGREF, pax.COMPANYCODE, pax.SEGNUM);
                    else
                        daPEOPAX.Add(pax);
                }


                FilterParams deldtfp = new FilterParams();
                deldtfp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                deldtfp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, eMos.BkgRef, Enums.Relation.Equal, Enums.Expression.AND);
                InnerConnection.ExecuteSql(DAPEOAIRDETAIL.DeleteSqlEntity(deldtfp));


                FilterParams delfp = new FilterParams();
                delfp.AddParam(tbPEOAIR.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                delfp.AddParam(tbPEOAIR.Fields.BkgRef, eMos.BkgRef, Enums.Relation.Equal, Enums.Expression.AND);
                InnerConnection.ExecuteSql(DAPEOAIR.DeleteSqlEntity(delfp));

                foreach (tbPEOAIR air in eMos.AirList)
                {
                    daPEOAIR.Add(air);
                }
                foreach (tbPEOAIRDETAIL airDt in eMos.AirDetailList)
                {
                    daPEOAIRDETAIL.Add(airDt);
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    InnerConnection.ExecuteSql(CallRecal_Air(air.CompanyCode, air.BkgRef, air.SegNum));
                }
                tbPEOTKTs dbTkts = daPEOTKT.GetListByBKGREF(eMos.CompanyCode, eMos.BkgRef);
                foreach (tbPEOTKT tkt in eMos.TicketList)
                {
                    if (dbTkts.Exists(c => c.Ticket == tkt.Ticket && c.TktSeq == tkt.TktSeq && c.Airline == tkt.Airline))
                        daPEOTKT.Update(tkt, tkt.CompanyCode, tkt.Ticket, tkt.TktSeq, tkt.Airline);
                    else
                        daPEOTKT.Add(tkt);
                }

                foreach (tbPEOTKT tkt in eMos.UpdateVoidTicketList)
                {
                    FilterParams fp = new FilterParams();
                    fp.AddParam(tbPEOTKT.Fields.CompanyCode, tkt.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.Airline, tkt.Airline, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.Ticket, tkt.Ticket, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.TktSeq, "01", Enums.Relation.Equal, Enums.Expression.AND);

                    UpdateParam up = new UpdateParam();
                    up.AddParam(tbPEOTKT.Fields.VoidBy, tkt.VoidBy);
                    up.AddParam(tbPEOTKT.Fields.VoidOn, tkt.VoidOn);
                    up.AddParam(tbPEOTKT.Fields.voidreason, tkt.voidreason);

                    InnerConnection.ExecuteSql(DAPEOTKT.UpdateSqlEntity(tkt, fp));
                }
                List<string> tktLst = new List<string>();
                foreach (tbPEOTKT t in eMos.TicketList)
                {
                    tktLst.Add(t.Ticket);
                }
                tbPEOTKTDETAILs dbTktDts = daPEOTKTDETAIL.GetList(eMos.CompanyCode, tktLst);
                foreach (tbPEOTKTDETAIL tktDt in eMos.TicketDetailList)
                {
                    if (dbTktDts.Exists(c => c.Ticket == tktDt.Ticket && c.TktSeq == tktDt.TktSeq && c.AirLine == tktDt.AirLine && c.SegNum == tktDt.SegNum))
                        daPEOTKTDETAIL.Update(tktDt, tktDt.CompanyCode, tktDt.Ticket, tktDt.TktSeq, tktDt.SegNum, tktDt.AirLine);
                    else
                        daPEOTKTDETAIL.Add(tktDt);
                }
            }
            InnerConnection.ExecuteSql(CallPEOMSTRsummary_trigger(eMos.CompanyCode, eMos.BkgRef));
        }
        #endregion

        private SqlList GetSqlList(eMosObjects eMos)
        {
            SqlList sqlList = new SqlList();

            if (eMos.isCreate)
            {
                sqlList.Add(DAPEOMSTR.AddSqlEntity(eMos.PEOMSTR));
                foreach (tbPEOPAX pax in eMos.PaxList)
                {
                    sqlList.Add(DAPEOPAX.AddSqlEntity(pax));
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    sqlList.Add(DAPEOAIR.AddSqlEntity(air));
                }
                foreach (tbPEOAIRDETAIL airDt in eMos.AirDetailList)
                {
                    sqlList.Add(DAPEOAIRDETAIL.AddSqlEntity(airDt));
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    sqlList.Add(CallRecal_Air(air.CompanyCode, air.BkgRef, air.SegNum));
                }
                foreach (tbPEOTKT tkt in eMos.TicketList)
                {
                    sqlList.Add(DAPEOTKT.AddSqlEntity(tkt));
                }
                foreach (tbPEOTKTDETAIL tktDt in eMos.TicketDetailList)
                {
                    sqlList.Add(DAPEOTKTDETAIL.AddSqlEntity(tktDt));
                }
            }
            else
            {
                foreach (tbPEOPAX pax in eMos.PaxList)
                {
                    sqlList.Add(DAPEOPAX.AddSqlEntity(pax));
                }


                FilterParams deldtfp = new FilterParams();
                deldtfp.AddParam(tbPEOAIRDETAIL.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                deldtfp.AddParam(tbPEOAIRDETAIL.Fields.BkgRef, eMos.BkgRef, Enums.Relation.Equal, Enums.Expression.AND);
                sqlList.Add(DAPEOAIRDETAIL.DeleteSqlEntity(deldtfp));


                FilterParams delfp = new FilterParams();
                delfp.AddParam(tbPEOAIR.Fields.CompanyCode, eMos.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                delfp.AddParam(tbPEOAIR.Fields.BkgRef, eMos.BkgRef, Enums.Relation.Equal, Enums.Expression.AND);
                sqlList.Add(DAPEOAIR.DeleteSqlEntity(delfp));

                foreach (tbPEOAIR air in eMos.AirList)
                {
                    sqlList.Add(DAPEOAIR.AddSqlEntity(air));
                }
                foreach (tbPEOAIRDETAIL airDt in eMos.AirDetailList)
                {
                    sqlList.Add(DAPEOAIRDETAIL.AddSqlEntity(airDt));
                }
                foreach (tbPEOAIR air in eMos.AirList)
                {
                    sqlList.Add(CallRecal_Air(air.CompanyCode, air.BkgRef, air.SegNum));
                }

                foreach (tbPEOTKT tkt in eMos.TicketList)
                {
                    sqlList.Add(DAPEOTKT.AddSqlEntity(tkt));
                }
                foreach (tbPEOTKT tkt in eMos.UpdateVoidTicketList)
                {
                    FilterParams fp = new FilterParams();
                    fp.AddParam(tbPEOTKT.Fields.CompanyCode, tkt.CompanyCode, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.Airline, tkt.Airline, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.Ticket, tkt.Ticket, Enums.Relation.Equal, Enums.Expression.AND);
                    fp.AddParam(tbPEOTKT.Fields.TktSeq, "01", Enums.Relation.Equal, Enums.Expression.AND);

                    UpdateParam up = new UpdateParam();
                    up.AddParam(tbPEOTKT.Fields.VoidBy, tkt.VoidBy);
                    up.AddParam(tbPEOTKT.Fields.VoidOn, tkt.VoidOn);
                    up.AddParam(tbPEOTKT.Fields.voidreason, tkt.voidreason);

                    sqlList.Add(DAPEOTKT.UpdateSqlEntity(tkt, fp));
                }
                foreach (tbPEOTKTDETAIL tktDt in eMos.TicketDetailList)
                {
                    sqlList.Add(DAPEOTKTDETAIL.AddSqlEntity(tktDt));
                }
            }
            sqlList.Add(CallPEOMSTRsummary_trigger(eMos.CompanyCode, eMos.BkgRef));

            return sqlList;
        }
        private SqlEntity CallRecal_Air(string companyCode, string bkgRef, string segnum)
        {
            SqlEntity se = new SqlEntity();
            se.CommandText = @"EXEC dbo.sp_Recal_Air @bkgref,@companyCode, @segnum";
            se.Parameters = new List<IDbDataParameter>();
            se.Parameters.Add(new System.Data.SqlClient.SqlParameter("@bkgref", bkgRef));
            se.Parameters.Add(new System.Data.SqlClient.SqlParameter("@companyCode", companyCode));
            se.Parameters.Add(new System.Data.SqlClient.SqlParameter("@segnum", segnum));
            return se;
        }
        private SqlEntity CallPEOMSTRsummary_trigger(string companyCode, string bkgRef)
        {
            SqlEntity se = new SqlEntity();
            se.CommandText = "EXEC dbo.sp_PEOMSTRsummary_trigger @companyCode,@bkgref";
            se.Parameters = new List<IDbDataParameter>();
            se.Parameters.Add(new System.Data.SqlClient.SqlParameter("@bkgref", bkgRef));
            se.Parameters.Add(new System.Data.SqlClient.SqlParameter("@companyCode", companyCode));
            return se;
        }
        private void CallCalPeomstrSummary(string companyCode, string bkgRef, string userID)
        {
            string sql = string.Format("EXEC dbo.sp_CalPeomstrSummary '{0}', '{1}', '{2}';"
                                        , companyCode + "", bkgRef + "", userID + "");
            InnerConnection.ExecuteSql(sql, null, 30);
        }
        public tbPEOTKTs GetAnotherBKGTicket(string companycode, string bkg, List<string> list)
        {

            return daPEOTKT.GetAnotherBKGTicket(companycode, bkg, list);

        }

        #region IDACompanyPreference Members

        public bool GetPreferenceEnable(string companyCode, MOEEnums.CompanyPreference type)
        {
            return daComPref.GetEnableValue(companyCode, type);
        }

        #endregion
    }
}