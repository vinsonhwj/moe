﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using Westminster.MOE;
using Westminster.MOE.DAL.eMosDB.SQL;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Lib;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.DAL.Document
{
    public class DAInvoice : IDAL.Document.IDAInvoice
    {
        #region Property

        DAPEOPAX daPeoPax;
        DAPEOAIR daPeoAir;
        DASqlInvAirDetail daSqlAirDetail;
        DASqlInvHotel daSqlHotel;
        DASqlInvHotelDetail daSqlHotelDetail;
        DASqlInvOther daSqlOther;
        DASqlInvOtherDetail daSqlOtherDetail;
        DACOMPANY daCompany;
        DAPEOMSTR daPeoMstr;
        DAPEOSTAFF daPeoStaff;
        DASqlInvTicket daSqlInvTicket;
        Maintain.DACompanyPreference daComPref;
        DACustomer daCustomer;
        DAInvoiceSQL daInvoiceSQL;
        DAPEOOTHER daPeoOther;
        DAPEOINV daPeoInv;
        DAPEOINPAX daPeoInPax;
        DAPEOINVSEG daPeoInvSeg;
        DAPEOINVDETAIL daPeoInvDetail;
        DAPEOOTHERDETAIL daPeoOtherDetail;
        DAPEONYREF daPeoNyRef;
        DA_SP_Booking daBooking;
        DALOGIN daLogin;
        DAPEOINVTKT daPeoInvTkt;
        DAPEOINVTKTref daPeoInvTktRef;
        DAPEOTKT daPeoTkt;
        DAPEOMISTKT daPeoMisTkt;
        private IConnection conn;
        DApeocltcrlmt daPeocltcrlmt;
        #endregion

        public DAInvoice(IConnection conn)
        {
            this.conn = conn;
            daPeoPax = new DAPEOPAX(conn);
            daPeoAir = new DAPEOAIR(conn);
            daSqlAirDetail = new DASqlInvAirDetail(conn);
            daSqlHotel = new DASqlInvHotel(conn);
            daSqlHotelDetail = new DASqlInvHotelDetail(conn);
            daSqlOther = new DASqlInvOther(conn);
            daSqlOtherDetail = new DASqlInvOtherDetail(conn);
            daCompany = new DACOMPANY(conn);
            daPeoMstr = new DAPEOMSTR(conn);
            daPeoStaff = new DAPEOSTAFF(conn);
            daSqlInvTicket = new DASqlInvTicket(conn);
            daComPref = new Westminster.MOE.DAL.Maintain.DACompanyPreference(conn);
            daCustomer = new DACustomer(conn);
            daInvoiceSQL = new DAInvoiceSQL(conn);
            daBooking = new DA_SP_Booking(conn);
            daPeoOther = new DAPEOOTHER(conn);
            daPeoInv = new DAPEOINV(conn);
            daPeoInPax = new DAPEOINPAX(conn);
            daPeoInvSeg = new DAPEOINVSEG(conn);
            daPeoInvDetail = new DAPEOINVDETAIL(conn);
            daPeoOtherDetail = new DAPEOOTHERDETAIL(conn);
            daPeoNyRef = new DAPEONYREF(conn);
            daLogin = new DALOGIN(conn);
            daPeoInvTkt = new DAPEOINVTKT(conn);
            daPeoInvTktRef = new DAPEOINVTKTref(conn);
            daPeoTkt = new DAPEOTKT(conn);
            daPeoMisTkt = new DAPEOMISTKT(conn);
            daPeocltcrlmt = new DApeocltcrlmt(conn);
        }

        public bool ExistCustomer(string companyCode, string cltCode)
        {
            DACustomer cust = new DACustomer(conn);
            return cust.Exists(companyCode, cltCode);
        }

        public bool ExistBookNum(string companyCode, string bkgNum)
        {
            return true;
        }

        public InvoiceVerifyInfo GetInvoiceVerifyInfo(InvoiceUIForPreview uiInv)
        {
            if (uiInv == null)
                return null;

            InvoiceVerifyInfo info = new InvoiceVerifyInfo();

            info.PeoMstrEntity = new DAPEOMSTR(conn).GetEntity(uiInv.CompanyCode, uiInv.BookingNum);


            if (info.PeoMstrEntity != null && !string.IsNullOrEmpty(info.PeoMstrEntity.TEAMCODE))
            {
                info.PeoMstrTeam = new DATEAM(conn).GetEntity(uiInv.CompanyCode, info.PeoMstrEntity.TEAMCODE);
            }

            info.PeoInvCount = new DAPEOINV(conn).RecordCount(uiInv.CompanyCode, uiInv.BookingNum);

            if (info.PeoMstrEntity != null)
            {
                info.AnalysisCodeCount = new DAAnalysisCode(conn).RecordCount(uiInv.CompanyCode, uiInv.AnaylsisCode, info.PeoMstrEntity.DEPARTDATE);
            }
            List<string> paxSegNumList = new List<string>();
            if (uiInv.Passengers != null)
            {
                foreach (Passenger p in uiInv.Passengers)
                {
                    paxSegNumList.Add(p.SegNum);
                }
            }

            List<string> airSegNumList = new List<string>();
            List<string> hotalSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();

            if (uiInv.Segments != null)
            {
                foreach (Segment s in uiInv.Segments)
                {
                    if (s.Type == Segment.SegmentType.Air)
                    {
                        airSegNumList.Add(s.Data.SegNum);
                    }
                    else if (s.Type == Segment.SegmentType.Hotel)
                    {
                        hotalSegNumList.Add(s.Data.SegNum);
                    }
                    else
                    {
                        otherSegNumList.Add(s.Data.SegNum);
                    }
                }
            }

            if (airSegNumList.Count != 0)
            {
                info.PeoAirDetails = new DAPEOAIRDETAIL(conn).GetListForSell(uiInv.CompanyCode, uiInv.BookingNum, airSegNumList);
                info.isAirSegmentTypeMatch = daInvoiceSQL.CheckAirSegmentTypeMatch(uiInv.CompanyCode, uiInv.BookingNum, airSegNumList);
            }
            else
            {
                info.isAirSegmentTypeMatch = true;
            }

            info.CurrList = daInvoiceSQL.MultiCurr(uiInv.CompanyCode, uiInv.BookingNum, "SELL", airSegNumList, hotalSegNumList, otherSegNumList);

            info.MatchAirAgeType = daInvoiceSQL.MatchAgeType(uiInv.CompanyCode, uiInv.BookingNum, paxSegNumList, airSegNumList, MOEEnums.BkgDetailType.Air, "SELL");
            info.MatchOtherAgeType = daInvoiceSQL.MatchAgeType(uiInv.CompanyCode, uiInv.BookingNum, paxSegNumList, otherSegNumList, MOEEnums.BkgDetailType.Other, "SELL");

            info.Company = daCompany.GetEntity(uiInv.CompanyCode);
            if (info.PeoMstrEntity != null)
            {
                info.PeoMstrsForMasterPNR = daPeoMstr.GetListForMasterPNR(uiInv.CompanyCode, info.PeoMstrEntity.MASTERPNR);
                info.Customer = daCustomer.GetEntity(uiInv.CompanyCode, info.PeoMstrEntity.CLTODE);
                info.PeoStaff = daPeoStaff.GetEntity(uiInv.CompanyCode, info.PeoMstrEntity.STAFFCODE);
            }
            return info;
        }

        public InvoiceDBForPreview GetInvoiceDBForPreview(InvoiceUIForPreview uiInv)
        {
            InvoiceDBForPreview dbpPreview = new InvoiceDBForPreview();
            dbpPreview.UIInfo = uiInv;
            if (uiInv == null || uiInv.CompanyCode == null || uiInv.BookingNum == null)
                return null;

            List<string> paxSegNumList = new List<string>();
            List<string> airSegNumList = new List<string>();
            List<string> hotalSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();

            if (uiInv.Passengers != null)
            {
                tbPEOPAXs paxs = new tbPEOPAXs();
                foreach (Passenger p in uiInv.Passengers)
                {
                    paxSegNumList.Add(p.SegNum);
                }
            }
            if (paxSegNumList.Count > 0)
            {
                dbpPreview.Passengers = daPeoPax.GetList(uiInv.BookingNum, uiInv.CompanyCode, paxSegNumList);
            }

            if (uiInv.Segments != null)
            {
                foreach (Segment s in uiInv.Segments)
                {
                    if (s.Type == Segment.SegmentType.Air)
                    {
                        airSegNumList.Add(s.Data.SegNum);
                    }
                    else if (s.Type == Segment.SegmentType.Hotel)
                    {
                        hotalSegNumList.Add(s.Data.SegNum);
                    }
                    else
                    {
                        otherSegNumList.Add(s.Data.SegNum);
                    }
                }
            }

            if (airSegNumList.Count > 0)
            {
                dbpPreview.Airs = daPeoAir.GetList(uiInv.CompanyCode, uiInv.BookingNum, airSegNumList);
                dbpPreview.AirDetails = daSqlAirDetail.GetSqlAirDetails(uiInv.CompanyCode, uiInv.BookingNum, airSegNumList, paxSegNumList);
            }

            if (hotalSegNumList.Count > 0)
            {
                sqlInvHotels htls = daSqlHotel.GetSqlHotels(uiInv.CompanyCode, uiInv.BookingNum, hotalSegNumList, uiInv.DBTableSuffix);
                if (htls != null && htls.Count > 0)
                {
                    dbpPreview.Hotels = new List<InvPreviewHtl>();
                    foreach (sqlInvHotel s in htls)
                    {
                        InvPreviewHtl h = new InvPreviewHtl();
                        h.Header = s;
                        h.Details = daSqlHotelDetail.GetSqlHotelDetails(uiInv.CompanyCode, uiInv.BookingNum, s.SEGNUM, uiInv.GSTTax);

                        dbpPreview.Hotels.Add(h);
                    }
                }
            }
            tbAirSegments l = new tbAirSegments();
            //l.Find(c => c.Class == "B" && c.curr == "HKD");

            if (otherSegNumList.Count > 0)
            {
                dbpPreview.Others = daSqlOther.GetSqlOthers(uiInv.IsAllowGroupInsurance, uiInv.CompanyCode, uiInv.BookingNum, uiInv.OtherInsSegnum, uiInv.OtherInsCurrency, uiInv.UserCode, otherSegNumList);
                dbpPreview.OtherDetails = daSqlOtherDetail.GetSqlOtherDetails(uiInv.IsAllowGroupInsurance, uiInv.CompanyCode, uiInv.BookingNum, otherSegNumList, paxSegNumList, uiInv.GSTTax);
            }

            dbpPreview.Company = daCompany.GetEntity(uiInv.CompanyCode);
            dbpPreview.PeoMstr = daPeoMstr.GetEntity(uiInv.CompanyCode, uiInv.BookingNum);
            dbpPreview.PeoStaff = daPeoStaff.GetEntity(uiInv.CompanyCode, dbpPreview.PeoMstr.STAFFCODE);

            sqlInvTickets tktList = daSqlInvTicket.GetSqlInvTicket(uiInv.CompanyCode, uiInv.BookingNum);
            if (tktList != null && uiInv.Tickets != null && uiInv.Tickets.Count > 0)
            {
                dbpPreview.Tickets = new sqlInvTickets();
                foreach (Ticket t in uiInv.Tickets)
                {
                    sqlInvTicket tkt = tktList.Find(c => c.ticket == t.TicketNum && c.tktseq == t.TicketSegNum && c.TicketType == t.TicketType);
                    if (tkt != null)
                    {
                        dbpPreview.Tickets.Add(tkt);
                    }
                }
            }

            return dbpPreview;
        }

        public InvoiceVerifyInfoForSave GetInvoiceVerifyInfoForSave(InvoiceUIForSave uiSave)
        {
            if (uiSave == null || uiSave.PreviewInput == null)
            {
                return null;
            }

            InvoiceVerifyInfoForSave vi = new InvoiceVerifyInfoForSave();
            vi.E_EINV = GetPreferenceEnable(uiSave.PreviewInput.CompanyCode, MOEEnums.CompanyPreference.E_EINV);
            vi.PMQ_EXTAX = GetPreferenceEnable(uiSave.PreviewInput.CompanyCode, MOEEnums.CompanyPreference.PMQ_EXTAX);

            vi.Company = daCompany.GetEntity(uiSave.PreviewInput.CompanyCode);
            vi.PeoMstr = daPeoMstr.GetEntity(uiSave.PreviewInput.CompanyCode, uiSave.PreviewInput.BookingNum);
            if (vi.PeoMstr != null)
            {
                vi.PeoMstrsForMasterPNR = daPeoMstr.GetListForMasterPNR(uiSave.PreviewInput.CompanyCode, vi.PeoMstr.MASTERPNR);
                vi.Customer = daCustomer.GetEntity(uiSave.PreviewInput.CompanyCode, vi.PeoMstr.CLTODE);
                vi.PeoStaff = daPeoStaff.GetEntity(uiSave.PreviewInput.CompanyCode, vi.PeoMstr.STAFFCODE);
            }
            return vi;
        }

        public InvoiceDBRefForSave GetInvoiceDBRefForSave(InvoiceUIForSave uiInv)
        {
            InvoiceDBRefForSave dbRef = new InvoiceDBRefForSave();
            // dbRef.UIInput = uiInv;
            if (uiInv == null || uiInv.PreviewInput == null || string.IsNullOrEmpty(uiInv.CompanyCode) || string.IsNullOrEmpty(uiInv.BkgNum))
                return null;

            List<string> paxSegNumList = new List<string>();
            List<string> airSegNumList = new List<string>();
            List<string> hotalSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();

            if (uiInv.PreviewInput.Passengers != null)
            {
                tbPEOPAXs paxs = new tbPEOPAXs();
                foreach (Passenger p in uiInv.PreviewInput.Passengers)
                {
                    paxSegNumList.Add(p.SegNum);
                }
            }
            if (paxSegNumList.Count > 0)
            {
                dbRef.Passengers = daPeoPax.GetList(uiInv.BkgNum, uiInv.CompanyCode, paxSegNumList);
            }

            if (uiInv.PreviewInput.Segments != null)
            {
                foreach (Segment s in uiInv.PreviewInput.Segments)
                {
                    if (s.Type == Segment.SegmentType.Air)
                    {
                        airSegNumList.Add(s.Data.SegNum);
                    }
                    else if (s.Type == Segment.SegmentType.Hotel)
                    {
                        hotalSegNumList.Add(s.Data.SegNum);
                    }
                    else
                    {
                        otherSegNumList.Add(s.Data.SegNum);
                    }
                }
            }

            if (airSegNumList.Count > 0)
            {
                dbRef.Airs = daPeoAir.GetList(uiInv.CompanyCode, uiInv.BkgNum, airSegNumList);
                dbRef.AirDetails = daSqlAirDetail.GetSqlAirDetails(uiInv.CompanyCode, uiInv.BkgNum, airSegNumList, paxSegNumList);
            }

            if (hotalSegNumList.Count > 0)
            {
                sqlInvHotels htls = daSqlHotel.GetSqlHotels(uiInv.CompanyCode, uiInv.BkgNum, hotalSegNumList, uiInv.PreviewInput.DBTableSuffix);
                if (htls != null && htls.Count > 0)
                {
                    dbRef.Hotels = new List<InvPreviewHtl>();
                    foreach (sqlInvHotel s in htls)
                    {
                        InvPreviewHtl h = new InvPreviewHtl();
                        h.Header = s;
                        h.Details = daSqlHotelDetail.GetSqlHotelDetails(uiInv.CompanyCode, uiInv.PreviewInput.BookingNum, s.SEGNUM, uiInv.PreviewInput.GSTTax);

                        dbRef.Hotels.Add(h);
                    }
                }
            }
            tbAirSegments l = new tbAirSegments();
            //l.Find(c => c.Class == "B" && c.curr == "HKD");

            if (otherSegNumList.Count > 0)
            {
                dbRef.Others = daSqlOther.GetSqlOthers(uiInv.PreviewInput.IsAllowGroupInsurance, uiInv.CompanyCode, uiInv.PreviewInput.BookingNum, uiInv.PreviewInput.OtherInsSegnum, uiInv.PreviewInput.OtherInsCurrency, uiInv.PreviewInput.UserCode, otherSegNumList);
                dbRef.OtherDetails = daSqlOtherDetail.GetSqlOtherDetails(uiInv.PreviewInput.IsAllowGroupInsurance, uiInv.CompanyCode, uiInv.PreviewInput.BookingNum, otherSegNumList, paxSegNumList, uiInv.PreviewInput.GSTTax);
            }

            dbRef.Company = daCompany.GetEntity(uiInv.CompanyCode);
            dbRef.PeoMstr = daPeoMstr.GetEntity(uiInv.CompanyCode, uiInv.PreviewInput.BookingNum);
            dbRef.PeoStaff = daPeoStaff.GetEntity(uiInv.CompanyCode, dbRef.PeoMstr.STAFFCODE);

            sqlInvTickets tktList = daSqlInvTicket.GetSqlInvTicket(uiInv.CompanyCode, uiInv.PreviewInput.BookingNum);
            if (tktList != null && uiInv.PreviewInput.Tickets != null && uiInv.PreviewInput.Tickets.Count > 0)
            {
                dbRef.Tickets = new sqlInvTickets();
                foreach (Ticket t in uiInv.PreviewInput.Tickets)
                {
                    sqlInvTicket tkt = tktList.Find(c => c.ticket == t.TicketNum && c.tktseq == t.TicketSegNum && c.TicketType == t.TicketType);
                    if (tkt != null)
                    {
                        dbRef.Tickets.Add(tkt);
                    }
                }
            }
            string branchCode = daPeoStaff.GetBranchCodeByStaffCode(uiInv.CompanyCode, uiInv.PreviewInput.UserCode, uiInv.BkgNum);
            dbRef.InvoiceNo = daBooking.GetNum(branchCode, uiInv.CompanyCode, uiInv.GrandTotalamountvle < 0 ? DA_SP_Booking.ModuleType.CRNNUM : DA_SP_Booking.ModuleType.VCHNUM, 1);

            dbRef.ComRef_INV_CCARD = GetPreferenceEnable(uiInv.CompanyCode, MOEEnums.CompanyPreference.PMQ_EXTAX);

            dbRef.OtherMaxSegNum = daPeoOther.GetMaxSegNum(uiInv.CompanyCode, uiInv.BkgNum);

            return dbRef;
        }

        public bool InvoiceSaveToDB(InvoiceDBForSave dbForSave, out string msg)
        {
            try
            {
                msg = string.Empty;
                SqlList sqllist = new SqlList();
                sqllist.AddRange(DAPEOINV.Insert(dbForSave.PeoInv));

                sqllist.AddRange(DAPEOMSTR.UpdateForInv(dbForSave.UpdatePeoMstr));
                sqllist.AddRange(DAPEOINPAX.InsertList(dbForSave.PeoInPaxList));
                sqllist.AddRange(DAPEOINVSEG.InsertList(dbForSave.PeoInvSegList));
                sqllist.AddRange(DAPEOINVDETAIL.InsertList(dbForSave.PeoInvDetailList));
                sqllist.AddRange(DAPEOOTHER.Insert(dbForSave.PeoOther));
                sqllist.AddRange(DAPEOOTHERDETAIL.InsertList(dbForSave.PeoOtherDetailList));
                sqllist.AddRange(DAPEONYREF.Insert(dbForSave.PeoNyRef));

                daPeoInv.ExecuteSqlTran(sqllist);
                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }
        }

        public InvoiceDBForView GetInvoiceDBForView(string companyCode, string invNum)
        {
            InvoiceDBForView db = new InvoiceDBForView();

            db.PeoInv = daPeoInv.GetEntity(companyCode, invNum);
            db.Company = daCompany.GetEntity(companyCode);
            db.PeoNyRef = daPeoNyRef.GetFristEntity(companyCode, invNum);
            bool hasPeoInvCombinPlus = false;
            //UFS") = 0 And InStr(Trim(invType), "IRCC") = 0 And InStr(Trim(invType), "RRCC") = 0 And InStr(Trim(invType), "INCC"
          //  db.PeoInvCombinPlus = new tbPEOINV();
            if (db.PeoNyRef.InvTypeCode.IndexOf("UFS") > 0 || db.PeoNyRef.InvTypeCode.IndexOf("IRCC") > 0 || db.PeoNyRef.InvTypeCode.IndexOf("RRCC") > 0 || db.PeoNyRef.InvTypeCode.IndexOf("INCC") > 0)
            {
                hasPeoInvCombinPlus = true;
            }
            if (hasPeoInvCombinPlus)
            {
                db.PeoInvCombinPlus = daPeoInv.GetEntityForPeoInvCombinPlus(companyCode, invNum);
            }

            if (db.PeoInv != null)
            {
                db.PeoMstr = daPeoMstr.GetEntity(companyCode, db.PeoInv.BKGREF);

                db.Customer = daCustomer.GetEntity(companyCode, db.PeoInv.CLTCODE);
                db.Login = daLogin.GetEntity(companyCode, db.PeoInv.CREATEBY);
                if (db.Login != null)
                {
                    db.PeoStaffForInv = daPeoStaff.GetEntity(companyCode, db.Login.STAFFCODE);
                }
                db.PeoStaffForMstr = daPeoStaff.GetEntity(companyCode, db.PeoMstr.STAFFCODE);

                db.PeoPaxList = daPeoPax.GetList(db.PeoInv.BKGREF, companyCode);

            }

            #region TKT
            tbPEOINVTKTs PeoInvTKTList = daPeoInvTkt.GetList(companyCode, invNum);
            tbPEOINVTKTrefs PeoInvTKTrefList = daPeoInvTktRef.GetList(companyCode, invNum);

            List<string> tktNuns = new List<string>();
            foreach (tbPEOINVTKT t in PeoInvTKTList)
            {
                tktNuns.Add(t.Ticket);
            }
            foreach (tbPEOINVTKTref t in PeoInvTKTrefList)
            {
                tktNuns.Add(t.Ticket);
            }

            //db.PeoTKTList = daPeoTkt.GetList(companyCode, tktNuns);
            //db.PeoMisTKTList = daPeoMisTkt.GetList(companyCode, tktNuns);
            db.PeoTKTList = new tbPEOTKTs();
            db.PeoMisTKTList = new tbPEOMISTKTs();
            foreach (tbPEOTKT t in daPeoTkt.GetList(companyCode, tktNuns))
            {
                if (PeoInvTKTList.Exists(c => c.Ticket == t.Ticket && c.TktSeq == t.TktSeq) || PeoInvTKTrefList.Exists(c => c.Ticket == t.Ticket && c.TktSeq == t.TktSeq))
                {
                    db.PeoTKTList.Add(t);
                }
            }

            foreach (tbPEOMISTKT t in daPeoMisTkt.GetList(companyCode, tktNuns))
            {
                if (PeoInvTKTList.Exists(c => c.Ticket == t.Ticket && c.TktSeq == t.TktSeq) || PeoInvTKTrefList.Exists(c => c.Ticket == t.Ticket && c.TktSeq == t.TktSeq))
                {
                    db.PeoMisTKTList.Add(t);
                }
            }

            #endregion

            db.PeoInvPaxList = daPeoInPax.GetList(companyCode, invNum);

          //  db.PeoCombineInv = new tbPEOINVs();
            if (hasPeoInvCombinPlus)
            {
                List<string> invNums = new List<string>();
                if (db.PeoInv.INVNUM != null)
                {
                    invNums.Add(db.PeoInv.INVNUM);
                }
                if (db.PeoInvCombinPlus.INVNUM != null)
                {
                    invNums.Add(db.PeoInvCombinPlus.INVNUM);
                }

                db.PeoInvSegList = daPeoInvSeg.GetList(companyCode, invNums);
                db.PeoInvDetailList = daPeoInvDetail.GetList(companyCode, invNums);
                db.PeoCombineInv = new tbPEOINVs();
                db.PeoCombineInv = daPeoInv.GetEntityForPeoInvCombineInvList(companyCode, invNums);
            }
            else
            {
                db.PeoInvSegList = daPeoInvSeg.GetList(companyCode, invNum);
                db.PeoInvDetailList = daPeoInvDetail.GetList(companyCode, invNum);
            }
            db.PeoCreditCardList = daPeoInv.GetList(companyCode, invNum);

            db.Peocltcrlmt = daPeocltcrlmt.GetEntity(companyCode, db.PeoInv.CLTCODE);
            
            if (db.Peocltcrlmt != null && db.Peocltcrlmt.crterms != decimal.Zero)
            {
                db.DueDay = db.PeoInv.CREATEON.AddDays(Convert.ToDouble(db.Peocltcrlmt.crterms));
           
            }
            return db;
        }

        #region IDACompanyPreference Members

        public bool GetPreferenceEnable(string companyCode, MOEEnums.CompanyPreference type)
        {
            return daComPref.GetEnableValue(companyCode, type);
        }

        #endregion
    }
}
