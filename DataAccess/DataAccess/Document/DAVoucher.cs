﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.Interface;
using hwj.DBUtility.MSSQL;
using Westminster.MOE;
using Westminster.MOE.DAL.eMosDB.SQL;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.DALEntity.Document.Voucher;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Document;
using Westminster.MOE.Lib;

namespace Westminster.MOE.DAL.Document
{
    public class DAVoucher : IDAL.Document.IDAVoucher
    {
        public IConnection InnerConnection { get; private set; }

        DAPEOMSTR daMstr;
        DAPEOPAX daPax;
        DAPEOHOTEL daHtl;
        DAPEOHTL daBkgHtl;
        DAPeoHtlDetail daBkgHtlDtl;
        DAPEOOTHER daBkgOth;
        DAPEOOTHERDETAIL daBkgOthDtl;
        DAsqlVchHotel daSqlHotel;
        Maintain.DACity daCity;
        Maintain.DACountry daCountry;
        Maintain.DACompanyPreference daComPref;
        DACustomer daCust;
        DAPEOSTAFF daStaff;
        DARESTRICTSUPPLIER daReSupp;
        DASUPPLIER daSupp;
        DACOMPANY daCom;
        DAPEOMIS daMis;
        DAPEOVCH daVch;
        DADocumentBanner daDocBan;
        DAPEOVCHSEG daSeg;
        DAPEOVCHDETAIL daDetail;
        DAPEOINPAX daInPax;
        DAPEONYREF daNyRef;
        DA_SP_Booking da_SP_Booking;

        public DAVoucher(string connectionString)
            : this(new DbConnection(connectionString))
        {
        }
        public DAVoucher(IConnection conn)
        {
            InnerConnection = conn;
            daMstr = new DAPEOMSTR(conn);
            daPax = new DAPEOPAX(conn);
            daHtl = new DAPEOHOTEL(conn);
            daBkgHtl = new DAPEOHTL(conn);
            daBkgHtlDtl = new DAPeoHtlDetail(conn);
            daBkgOth = new DAPEOOTHER(conn);
            daBkgOthDtl = new DAPEOOTHERDETAIL(conn);
            daSqlHotel = new DAsqlVchHotel(conn);
            daCountry = new Westminster.MOE.DAL.Maintain.DACountry(conn);
            daCity = new Westminster.MOE.DAL.Maintain.DACity(conn);
            daComPref = new Maintain.DACompanyPreference(conn);
            daCust = new DACustomer(conn);
            daStaff = new DAPEOSTAFF(conn);
            daReSupp = new DARESTRICTSUPPLIER(conn);
            daSupp = new DASUPPLIER(conn);
            daCom = new DACOMPANY(conn);
            daMis = new DAPEOMIS(conn);
            daVch = new DAPEOVCH(conn);
            daDocBan = new DADocumentBanner(conn);
            daSeg = new DAPEOVCHSEG(conn);
            daDetail = new DAPEOVCHDETAIL(conn);
            daInPax = new DAPEOINPAX(conn);
            daNyRef = new DAPEONYREF(conn);
            da_SP_Booking = new DA_SP_Booking(conn);
        }

        public VCHPreviewDB GetDBForPreview(VCHSelectedKey input)
        {
            VCHPreviewDB db = new VCHPreviewDB(true);

            if (input == null || input.CompanyCode == null || input.BkgRef == null)
            {
                return null;
            }

            #region Init Data
            List<string> paxSegNumList = new List<string>();
            List<string> hotelSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();
            if (input.Passengers != null)
            {
                foreach (Passenger p in input.Passengers)
                {
                    if (!string.IsNullOrEmpty(p.SegNum))
                    {
                        paxSegNumList.Add(p.SegNum);
                    }
                }
            }
            if (input.Segments != null)
            {
                foreach (Segment s in input.Segments)
                {
                    if (!string.IsNullOrEmpty(s.Data.SegNum))
                    {
                        if (s.Type == Segment.SegmentType.Hotel)
                        {

                            hotelSegNumList.Add(s.Data.SegNum);

                        }
                        else if (s.Type == Segment.SegmentType.Other)
                        {
                            otherSegNumList.Add(s.Data.SegNum);
                        }
                    }
                }
            }
            #endregion

            #region Master
            db.Master = daMstr.GetEntity(input.CompanyCode, input.BkgRef);
            db.PMQMaster = GetMasterForPMQ(db.Master, MOEEnums.DocType.DOC);
            if (db.PMQMaster != null)
            {
                db.PMQCustomer = daCust.GetEntity(db.Master.COMPANYCODE, db.PMQMaster.CLTODE);
                db.PMQStaff = daStaff.GetEntity(db.PMQMaster.COMPANYCODE, db.PMQMaster.STAFFCODE);
            }
            #endregion

            #region Passenger
            if (paxSegNumList.Count > 0)
            {
                db.Passengers = daPax.GetListSortBySEGNUM(input.CompanyCode, input.BkgRef, paxSegNumList);
            }
            #endregion

            #region Hotel
            if (hotelSegNumList.Count > 0)
            {
                db.PeoHtls = daBkgHtl.GetList(input.CompanyCode, input.BkgRef, hotelSegNumList);
                db.PeoHtlDetails = daBkgHtlDtl.GetList(input.CompanyCode, input.BkgRef, hotelSegNumList);

                if (db.PeoHtls != null)
                {
                    List<string> htlCodeList = new List<string>();
                    foreach (tbPEOHTL h in db.PeoHtls)
                    {
                        htlCodeList.Add(h.HOTELCODE);
                    }
                    db.PeoHotels = daHtl.GetList(htlCodeList);

                    List<string> countryCodeList = new List<string>();
                    List<string> cityCodeList = new List<string>();
                    foreach (tbPEOHOTEL h in db.PeoHotels)
                    {
                        if (!string.IsNullOrEmpty(h.COUNTRYCODE))
                        {
                            countryCodeList.Add(h.COUNTRYCODE.Trim());
                        }
                        if (!string.IsNullOrEmpty(h.CITYCODE))
                        {
                            cityCodeList.Add(h.CITYCODE.Trim());
                        }
                    }
                    db.Countrys = daCountry.GetList(countryCodeList);
                    db.Citys = daCity.GetList(cityCodeList);
                }

                foreach (string seg in hotelSegNumList)
                {
                    db.ServiceDetails.AddRange(daSqlHotel.GetList(input.BkgRef, input.CompanyCode, null, seg, "COST"));
                }
            }
            #endregion

            #region Other
            if (otherSegNumList.Count > 0)
            {
                db.PeoOthers = daBkgOth.GetList(input.CompanyCode, input.BkgRef, otherSegNumList);
                db.PeoOtherDetails = daBkgOthDtl.GetList(input.CompanyCode, input.BkgRef, otherSegNumList);
            }
            #endregion

            db.TotalAmount = 0;
            db.MD5 = Lib.MOECommon.GetMD5Key(db);
            db.ServerDateTime = daBkgHtl.GetServerDateTime();

            return db;
        }

        public VCHCreateDB GetCreateDB(VCHSelectedKey input)
        {
            if (input == null || input.CompanyCode == null || input.BkgRef == null)
            {
                return null;
            }

            VCHCreateDB db = new VCHCreateDB();
            db.CompanyCode = input.CompanyCode;
            db.VouchNum = da_SP_Booking.GetNum(input.CompanyCode, input.BranchCode, DA_SP_Booking.ModuleType.VCHNUM, 1);
            db.BkgRef = input.BkgRef;
            db.PreviewDB = GetDBForPreview(input);
            #region Company
            db.Company = daCom.GetEntity(input.CompanyCode);
            #endregion

            #region Staff
            db.Master = daMstr.GetEntity(input.CompanyCode, input.BkgRef);
            db.Staff = daStaff.GetEntity(input.CompanyCode, input.UserCode);
            #endregion

            #region Mis
            db.Mis = daMis.GetEntityWithUnvoid(input.CompanyCode, input.BkgRef);
            #endregion

            db.ServerDateTime = daBkgHtl.GetServerDateTime();
            return db;
        }

        public bool SaveData(VCHSaveDB dbsave)
        {
            if (dbsave != null)
            {
                daVch.Add(dbsave.Header);
                //daVch.Update(dbsave.Vch, dbsave.Vch.COMPANYCODE, dbsave.Vch.VCHNUM);
                daDocBan.Add(dbsave.Banner);
                //daDocBan.Update(dbsave.DocBanner, dbsave.Vch.COMPANYCODE, dbsave.Vch.BKGREF, dbsave.Vch.VCHNUM);
                foreach (tbPEOVCHSEG s in dbsave.Segments)
                {
                    daSeg.Add(s);
                    //daSeg.Update(s, dbsave.Vch.COMPANYCODE, dbsave.Vch.VCHNUM,s.SEGNUM);                
                }
                foreach (tbPEOVCHDETAIL d in dbsave.Details)
                {
                    daDetail.Add(d);
                    //daDetail.Update(d, dbsave.Vch.COMPANYCODE, dbsave.Vch.VCHNUM, d.SEGNUM, d.SEQNUM);    
                }
                foreach (tbPEOINPAX p in dbsave.Passengers)
                {
                    daInPax.Add(p);
                    //daInPax.Update(p, dbsave.Vch.COMPANYCODE, dbsave.Vch.VCHNUM, p.SEQNUM, p.PAXSEQ);
                }
                daNyRef.Add(dbsave.History);
                return true;
            }
            return false;
        }
        public bool IsProfitMarginEnable(string companyCode)
        {
            return daCom.IsProfitMarginEnable(companyCode);
        }
        private tbPEOMSTR GetMasterForPMQ(tbPEOMSTR mstr, MOEEnums.DocType docType)
        {
            tbPEOMSTR dbPMQ = daMstr.GetEntityForPMQ(mstr.COMPANYCODE, mstr.MASTERPNR);

            if (dbPMQ != null)
            {
                dbPMQ.COMPANYCODE = mstr.COMPANYCODE;
                dbPMQ.BKGREF = mstr.BKGREF;
                dbPMQ.MASTERPNR = mstr.MASTERPNR;
                dbPMQ.STAFFCODE = mstr.STAFFCODE;
                if (docType != MOEEnums.DocType.DOC)
                {
                    dbPMQ.CLTODE = mstr.CLTODE;
                }
            }
            return dbPMQ;
        }

        #region IDACompanyPreference Members

        public bool GetPreferenceEnable(string companyCode, MOEEnums.CompanyPreference type)
        {
            return daComPref.GetEnableValue(companyCode, type);
        }

        #endregion

        #region IDASupplier Members

        public bool IsRestrictedSupplier(string companyCode, string suppCode)
        {
            return daReSupp.Exists(companyCode, suppCode);
        }
        public string GetSuppName(string companyCode, string suppCode)
        {
            if (string.IsNullOrEmpty(companyCode) || string.IsNullOrEmpty(suppCode))
            {
                return string.Empty;
            }
            else
            {
                tbSUPPLIER supp = daSupp.GetEntity(companyCode, suppCode);
                if (supp != null)
                {
                    return supp.SUPPNAME;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public bool IsActiveSupplier(string companyCode, string suppCode)
        {
            return daSupp.IsActive(companyCode, suppCode);
        }
        #endregion



    }
}
