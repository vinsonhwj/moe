﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.DAL.eMosDB.Table;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.DAL.eMosDB.SQL;

namespace Westminster.MOE.DAL.Document
{
    public class DAXo : IDAL.Document.IDAXo
    {
        public string ConnectionString { get; private set; }

        public DAXo()
        { }

        public DAXo(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public XoVerifyInfo GetXoVerifyInfo(XoUIInfo uiXo)
        {
            if (uiXo == null)
            {
                return null;
            }

            XoVerifyInfo info = new XoVerifyInfo();
            //info.PeoMstrEntity = new DAPEOMSTR(ConnectionString).GetEntity(uiXo.CompanyCode, uiXo.BookingNum);
            //if (info.PeoMstrEntity != null && !string.IsNullOrEmpty(info.PeoMstrEntity.TEAMCODE))
            //{
            //    info.PeoMstrTeam = new DATEAM(ConnectionString).GetEntity(uiXo.CompanyCode, info.PeoMstrEntity.TEAMCODE);
            //}
            //info.PeoInvCount = new DAPEOINV(ConnectionString).RecordCount(uiXo.CompanyCode, uiXo.BookingNum);
            //if (info.PeoMstrEntity != null)
            //{
            //    info.AnalysisCodeCount = new DAAnalysisCode(ConnectionString).RecordCount(uiXo.CompanyCode, uiXo.AnaylsisCode, info.PeoMstrEntity.DEPARTDATE);

            //}

            //取出paxseg,airseg,hotelseg,otherseg 选中的segnum          
            List<string> airSegNumList = new List<string>();
            List<string> hotalSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();
            List<string> paxSegNumList = new List<string>();
            if (uiXo.Segments != null)
            {
                foreach (Segment s in uiXo.Segments)
                {
                    if (s != null)
                    {
                        if (s.Type == Segment.SegmentType.Air)
                        {
                            airSegNumList.Add(s.Data.SegNum);
                        }
                        else if (s.Type == Segment.SegmentType.Hotel)
                        {
                            hotalSegNumList.Add(s.Data.SegNum);
                        }
                        else
                        {
                            otherSegNumList.Add(s.Data.SegNum);
                        }
                    }
                }

            }
            if (uiXo.Passengers != null)
            {
                foreach (Passenger pax in uiXo.Passengers)
                {
                    if (pax != null)
                    {
                        paxSegNumList.Add(pax.SegNum);
                    }
                }
            }



            // 是否选中Pax
            if (uiXo.Passengers != null)
            {
                foreach (Passenger p in uiXo.Passengers)
                {
                    if (p != null)
                    {
                        info.PaxSeletedCount = new DAPEOPAX(ConnectionString).RecordCount(uiXo.CompanyCode, uiXo.BookingNum, p.SegNum);
                    }
                }
            }

            // 验证PromptNotMatchAgeType
            if (airSegNumList.Count != 0)
            {
                // info.PeoAirDetails = new DAPEOAIRDETAIL(ConnectionString).GetListForSell(uiXo.CompanyCode, uiXo.BookingNum, airSegNumList);
                info.PeoAirDetails = new DAPEOAIRDETAIL(ConnectionString).GetEntity(uiXo.CompanyCode, uiXo.BookingNum, airSegNumList, uiXo.SeqType);
                info.PeoAirList = new DAPEOAIR(ConnectionString).GetEntity(uiXo.CompanyCode, uiXo.BookingNum, airSegNumList);
            }
            if (otherSegNumList.Count != 0)
            {
                info.PeoOthers = new DAPEOOTHER(ConnectionString).GetList(uiXo.CompanyCode, uiXo.BookingNum, otherSegNumList);

                info.PeoOtherDetails = new DAPEOOTHERDETAIL(ConnectionString).GetEntity(uiXo.BookingNum, uiXo.CompanyCode, otherSegNumList, uiXo.SeqType);
            }
            if (paxSegNumList.Count != 0)
            {
                foreach (tbPEOOTHERDETAIL othDtl in info.PeoOtherDetails)
                {
                    info.PeoOthMatchAgeType = new DAPEOPAX(ConnectionString).GetEntityForAgeMatchOth(uiXo.BookingNum, uiXo.CompanyCode, paxSegNumList, othDtl.AGETYPE);
                }
                foreach (tbPEOAIRDETAIL paxDtl in info.PeoAirDetails)
                {
                    info.PeoAirMatchAgeType = new DAPEOPAX(ConnectionString).GetEntityForAgeMatchAir(uiXo.BookingNum, uiXo.CompanyCode, paxSegNumList, paxDtl.AGETYPE);
                }

            }

            //验证SuppCode 
            string airSegNum = string.Join(",", airSegNumList.ToArray());
            string hotelSegNum = string.Join(",", hotalSegNumList.ToArray());
            string otherSegNum = string.Join(",", otherSegNumList.ToArray());

            info.tbAirSegments = new DAAirSegment(ConnectionString).GetList(uiXo.BookingNum, airSegNum);
            info.tbAirSegNotSelected = new DAAirSegment(ConnectionString).GetList(uiXo.BookingNum, "");
            info.tbHotelSegments = new DAHotelSegment(ConnectionString).GetList(uiXo.CompanyCode, uiXo.BookingNum, hotelSegNum);
            info.tbOtherSegments = new DAOtherSegment(ConnectionString).GetList(uiXo.BookingNum, otherSegNum);


            //验证Air Segment Age Type Match
            info.tbAirSegments_AgeTypes = new DAAirSegment_AgeType(ConnectionString).GetList(uiXo.CompanyCode, uiXo.BookingNum, airSegNum);

            // 验证PromptNotSameCurr
            info.tbMultiCurrs = new DAMultiCurr(ConnectionString).GetList(uiXo.BookingNum, airSegNum, hotelSegNum, otherSegNum);

            return info;
        }

        public XoDBForPreview GetXoDBForPreview(XoUIInfo uiXo)
        {
            XoDBForPreview dbXoPreview = new XoDBForPreview();
            dbXoPreview.UIXoInfo = uiXo;
            if (uiXo == null || uiXo.CompanyCode == null || uiXo.BookingNum == null)
                return null;

            List<string> paxSegNumList = new List<string>();
            List<string> airSegNumList = new List<string>();
            List<string> hotalSegNumList = new List<string>();
            List<string> otherSegNumList = new List<string>();

            if (uiXo.Passengers != null)
            {
                foreach (Passenger pax in uiXo.Passengers)
                {
                    if (pax != null)
                    {
                        paxSegNumList.Add(pax.SegNum);
                    }
                }
            }
            if (paxSegNumList.Count > 0)
            {
                dbXoPreview.Passengers = new DAPEOPAX(ConnectionString).GetList(uiXo.BookingNum, uiXo.CompanyCode, paxSegNumList);
            }
            if (uiXo.Segments != null)
            {
                foreach (Segment s in uiXo.Segments)
                {
                    if (s != null)
                    {
                        if (s.Type == Segment.SegmentType.Air)
                        {
                            airSegNumList.Add(s.Data.SegNum);
                        }
                        else if (s.Type == Segment.SegmentType.Hotel)
                        {
                            hotalSegNumList.Add(s.Data.SegNum);
                        }
                        else
                        {
                            otherSegNumList.Add(s.Data.SegNum);
                        }
                    }
                }

            }
            string paxSegNum = string.Join(",", paxSegNumList.ToArray());
            string airSegNum = string.Join(",", airSegNumList.ToArray());
            string hotelSegNum = string.Join(",", hotalSegNumList.ToArray());
            string otherSegNum = string.Join(",", otherSegNumList.ToArray());

            if (airSegNumList.Count > 0)
            {
                dbXoPreview.Airs = new DAPEOAIR(ConnectionString).GetList(uiXo.CompanyCode, uiXo.BookingNum, airSegNumList);
                dbXoPreview.AirDetails = new DAsqlXoAirDetail(ConnectionString).GetList(uiXo.BookingNum, paxSegNum, airSegNum);
            }
            if (hotalSegNumList.Count > 0)
            {
                dbXoPreview.Hotels = new DASqlXoHotel(ConnectionString).GetList(uiXo.CompanyCode, uiXo.BookingNum, hotelSegNum);
                dbXoPreview.HtlDetails = new DAsqlXoHotelDetail(ConnectionString).GetList(uiXo.BookingNum, hotelSegNum);
            }
            if (otherSegNumList.Count > 0)
            {
                dbXoPreview.Others = new DAPEOOTHER(ConnectionString).GetListForOther(uiXo.CompanyCode, uiXo.BookingNum, otherSegNumList);
                dbXoPreview.OthDetails = new DAsqlXoOtherDetail(ConnectionString).GetList(uiXo.BookingNum, paxSegNum, otherSegNum);
            }


            dbXoPreview.Company = new DACOMPANY(ConnectionString).GetEntity(uiXo.CompanyCode);

            dbXoPreview.PeoMstr = new DAPEOMSTR(ConnectionString).GetEntity(uiXo.CompanyCode, uiXo.BookingNum);

            dbXoPreview.Supplier = new DASUPPLIER(ConnectionString).GetEntity(uiXo.CompanyCode, uiXo.SuppCode);

            dbXoPreview.Staff = new DAPEOSTAFF(ConnectionString).GetEntity(uiXo.CompanyCode, dbXoPreview.PeoMstr.STAFFCODE);

            string defaultCurr = string.Empty;
            defaultCurr = GetDefaultCurr(dbXoPreview);
            dbXoPreview.Exrate = new DAEXRATE(ConnectionString).GetEntity(dbXoPreview.Company.COMPANYCODE, dbXoPreview.Company.LOCALCURR, defaultCurr);
            return dbXoPreview;
        }

        private string GetDefaultCurr(XoDBForPreview dbXoPreview)
        {
            string DefaultCurr = string.Empty;
            if (dbXoPreview.AirDetails != null && dbXoPreview.AirDetails.Count > 0)
            {
                foreach (sqlXoAirDetail d in dbXoPreview.AirDetails)
                {
                    DefaultCurr = d.curr;
                }
            }
            if (dbXoPreview.HtlDetails != null && dbXoPreview.HtlDetails.Count > 0)
            {
                foreach (sqlXoHotelDetail d in dbXoPreview.HtlDetails)
                {
                    DefaultCurr = d.COSTCURR;
                }
            }
            if (dbXoPreview.OthDetails != null && dbXoPreview.OthDetails.Count > 0)
            {
                foreach (sqlXoOtherDetail d in dbXoPreview.OthDetails)
                {
                    DefaultCurr = d.curr;
                }
            }
            return DefaultCurr;
        }

    }
}
