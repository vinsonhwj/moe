﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.DALEntity.Transfer;
namespace Westminster.MOE.IDAL.Booking
{
    public interface ICloneBooking
    {
        string GetNewBkGrefKey(string branchCode, string companyCode);
        CloneBookingInfo GetCloneBookingInfo(string companyCode, string bkgRef,string staffCode,string login);
        void CopyBooking(CloneBookingInfo cbi,string companyCode,string cloneBkgRef,string login);
    }
}
