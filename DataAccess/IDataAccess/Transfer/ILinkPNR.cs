﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.Interface;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Maintain;

namespace Westminster.MOE.IDAL.Transfer
{
    public interface ILinkPNR : IDACompanyPreference
    {
        IConnection InnerConnection { get; set; }

        Westminster.MOE.Entity.DALEntity.Gateway.IURObject GetIURObjects(string companyCode, string bkgRef);
        eMosObjects GeteeMosObjects(string companyCode, string bkgRef, string sourcePnr);
        tbPEOTKTs GetTicketList(string companycode, string bkgref);
        tbPEOAIRDETAILs GetFirstAirDetailList(string companycode, string bkgref);
        tbPEOPAXs GetPaxList(string companycode, string bkgref);
        tbCustomer GetCustomer(string companycode, string cltcode);

        bool ExistBooking(string companyCode, string bkgRef);
        bool UpdateFlag(string companyCode, string bkgref, bool succeed);
        void InserteMosObjects(eMosObjects eMos);
        //bool InsertDB(BookingInfo booking);



        tbPEOTKTs GetAnotherBKGTicket(string companycode, string bkg, List<string> list);
    }
}
