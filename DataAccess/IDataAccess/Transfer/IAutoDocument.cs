﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.DAL.eMosDB.StoredProcedure;

namespace Westminster.MOE.IDAL.Transfer
{
    public interface IAutoDocument
    {
        Westminster.MOE.Entity.DALEntity.Gateway.IURObject GetIURObjectsByPNR(string companyCode, string PNR,string bkgref,string SourceSystem,DateTime createOn);

        tbPEOMISs GetPEOMISList(string companyCode, string TSANumber);

        tbPEOMSTRs GetMasterListByObject(tbPEOMSTR master, DateTime dateTime);

        tbCustomer GetCustomer(string companyCode, string cltCode);

        bool ExistUnVoidInvoicedTickets(string companyCode, string bkgref, string ticket);

        bool ExistUnVoidInvoicedTicketRefs(string companyCode, string bkgref, string ticket);

        tbPEOMIS GetPEOMIS(string bkgref, string companyCode, string SEGNUM);

        tbPEOPAXs GetAllPaxByBkgref(string companyCode, string bkgref);

        bool Com_PreferenceIsY(string compangyCode, string field);

        int GetOtherCount(string compangyCode, string bkgref);

        bool ExistBooking(string companyCode, string bkgRef);

        bool InserteMosObjects(eMosBooking eMos,string staffCode);

        bool ExistMis(string bkgRef, string companyCode, string SEGNUM);

        tbPEOAIRDETAILs GetFirstAirDetailList(string companycode, string bkgref);
        tbPEOTKTs GetTicketList(string companycode, string bkgref);

         void CallRecal_Air(string companyCode, string bkgRef, string segnum);
         void CallPEOMSTRsummary_trigger(string companyCode, string bkgRef);
         void CallCalPeomstrSummary(string companyCode, string bkgRef, string userID);
         void CallProfitMarginChecking(string companyCode, string bkgRef, string userID, string teamCode);
         void CallRecalTicketPaidFare(List<string> listInvoice, string companyCode);

         tbPEOMSTR GetPEOMSTR(string bkgref, string companycode);

         tbPEOMISs GetUnvoidPeoMisList(string companycode, string bkgref);

         bool IsExistedInvoice(string companycode, string bkgref);

         tbPEOSTAFF GetStaff(string companycode, string staffcode);

         string GetNewBkGrefKey(string branchCode, string companyCode, DA_SP_Booking.ModuleType moduleCode);

         hwj.DBUtility.SqlEntity GetInsertSql(tbPEOINV tbPEOINV);

         string InsertTsfrInvoice(Westminster.MOE.Entity.DALEntity.Transfer.TsfrInvoice invoice, InvoiceTypeEnum InvoiceType, Invoice_Types InvType);

         DateTime GetDueDate(string companyCode, string invNum);        
    }
}
