﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Lib;

namespace Westminster.MOE.IDAL.Document
{
    public interface IDAInvoice : Maintain.IDACompanyPreference
    {
        bool ExistCustomer(string companyCode, string cltCode);
        bool ExistBookNum(string companyCode, string bkgNum);
        //string GetCompanyPreference(string companyCode, MOEEnums.CompanyPreference comPreferenceType);
        InvoiceVerifyInfo GetInvoiceVerifyInfo(InvoiceUIForPreview uiInv);
        InvoiceDBForPreview GetInvoiceDBForPreview(InvoiceUIForPreview uiInv);

        InvoiceVerifyInfoForSave GetInvoiceVerifyInfoForSave(InvoiceUIForSave uiSave);
        InvoiceDBRefForSave GetInvoiceDBRefForSave(InvoiceUIForSave uiInv);
        bool InvoiceSaveToDB(InvoiceDBForSave dbForSave, out string msg);

        InvoiceDBForView GetInvoiceDBForView(string companyCode, string invNum);
    }
}
