﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hwj.DBUtility.MSSQL;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity.Document.Voucher;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.IDAL.Maintain;
using Westminster.MOE.Lib;
using hwj.DBUtility.Interface;

namespace Westminster.MOE.IDAL.Document
{
    public interface IDAVoucher : IDACompanyPreference, IDASupplier
    {
        IConnection InnerConnection { get; }
        VCHPreviewDB GetDBForPreview(VCHSelectedKey uiVch);
        VCHCreateDB GetCreateDB(VCHSelectedKey uiVch);

        bool SaveData(VCHSaveDB dbsave);
        bool IsProfitMarginEnable(string companyCode);
    }
}
