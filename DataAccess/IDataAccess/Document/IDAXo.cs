﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document;
using Westminster.MOE.Entity.DALEntity;


namespace Westminster.MOE.IDAL.Document
{
   public interface  IDAXo
    {
        //bool ExistCustomer(string companyCode, string cltCode);
        //bool ExistBookNum(string companyCode, string bkgNum);
        //bool GetCompanyPreference(string companyCode, EnumTypes.CompanyPreference comPreferenceType);
        XoVerifyInfo GetXoVerifyInfo(XoUIInfo uiXo);
        XoDBForPreview GetXoDBForPreview(XoUIInfo uiXo);
    }
}
