﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;

namespace Westminster.MOE.IDAL.Maintain
{
    public interface IDACompanyPreference
    {
        bool GetPreferenceEnable(string companyCode, MOEEnums.CompanyPreference type);
    }
}
