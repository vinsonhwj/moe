﻿using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.IDAL.Maintain
{
    public interface IDACurrency
    {
        tbEXRATE GetExRateEntity(string companycode, string targetCurr, string sourceCurr);
    }
}
