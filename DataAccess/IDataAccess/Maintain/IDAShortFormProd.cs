﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.IDAL.Maintain
{
    public interface IDAShortFormProd
    {
        string GetAirClassName(string aircode, string classcode);
        string GetCityName(string code);
    }
}
