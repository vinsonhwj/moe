﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.IDAL.Maintain
{
    public interface IDASupplier
    {
        bool IsRestrictedSupplier(string companyCode, string suppCode);
        string GetSuppName(string companyCode, string suppCode);
        bool IsActiveSupplier(string companyCode, string suppCode);
    }
}
