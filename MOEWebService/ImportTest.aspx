﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportTest.aspx.cs" Inherits="Westminster.MOE.WebService.ImportTest"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtContent" runat="server" Height="300px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbErr" runat="server" Text="---"></asp:Label>
                </td>
                <td width="10%">
                    <asp:Button ID="btnRun" runat="server" Text="Run" OnClick="btnRun_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
