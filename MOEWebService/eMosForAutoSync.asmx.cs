﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Sync.Interface.Template;
using Westminster.MOE.BLL.Transfer;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.WebService
{
    /// <summary>
    /// Summary description for eMosForAutoSync
    /// </summary>
    [WebService(Namespace = "http://wtl.MOE.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eMosForAutoSync : System.Web.Services.WebService
    {

        [WebMethod]
        public string NightJob_SetBooking(string xml)
        {
            SyncResult rs = new SyncResult();
            WSResult wsResult = new WSResult();
            try
            {
                using (LinkPNR link = new LinkPNR(Global.Config))
                {
                    SyncData data = SyncData.FromXml(xml);
                    foreach (Item item in data.ItemList)
                    {
                        link.CreateBookingByIUR(item.XMLContent, out wsResult);
                        item.ErrorMessage = wsResult.Msg;
                    }
                    rs.ItemList = data.ItemList;
                    rs.ErrorMessage = wsResult.Msg;
                    rs.Success = true;
                }
            }
            catch (Exception ex)
            {
                rs.Success = false;
                rs.ErrorMessage = ex.Message;
                MOELog.Error("NightJob_SetBooking", ex, rs.ToXml());
            }
            return rs.ToXml();
        }
    }
}
