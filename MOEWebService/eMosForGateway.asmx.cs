﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Westminster.MOE.BLL.Transfer;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.WebService
{
    /// <summary>
    /// Summary description for eMosForGateway
    /// </summary>
    [WebService(Namespace = "http://wtl.MOE.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eMosForGateway : System.Web.Services.WebService
    {

        [WebMethod]
        public WSResult UpdateBooking(string companyCode, string IURBkgRef)
        {
            WSResult wsResult = new WSResult();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                MOELog.Info(string.Format("[Update eMos Booking] CompanyCode:{0} / BkgRef:{1}", companyCode, IURBkgRef));
                IURBkgRef = IURBkgRef.Trim();

                using (LinkPNR link = new LinkPNR(Global.Config))
                {
                    bool isSucceed = link.UpdateBooking4Gateway(companyCode, IURBkgRef, out wsResult);

                    sw.Stop();

                    MOELog.Info(string.Format("[Update eMos Booking] CompanyCode:{0} / BkgRef:{1} / Succeed:{2} / Elapsed Milliseconds:{3}", companyCode, IURBkgRef, isSucceed.ToString(), sw.ElapsedMilliseconds));
                }

                return wsResult;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("[Update eMos Booking] CompanyCode:{0} / BkgRef:{1} / Elapsed Milliseconds:{2}", companyCode, IURBkgRef, sw.ElapsedMilliseconds);
                sb.AppendLine();
                MOELog.Error(sb.ToString(), ex);
                wsResult.Msg = ex.Message;
                wsResult.Status = MOEEnums.ResultStatus.Error;
                return wsResult;
            }
        }
    }
}
