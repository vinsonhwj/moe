﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Sync.Interface.Template;
using Westminster.MOE.BLL;
using Westminster.MOE.BLL.Transfer;
using Westminster.MOE.Entity;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.BLLEntity.Transfer;
using Westminster.MOE.Entity.BLLEntity.UI.Document;
using Westminster.MOE.Lib;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Voucher;


namespace Westminster.MOE.WebService
{
    /// <summary>
    /// Summary description for eMosBusiness
    /// </summary>
    [WebService(Namespace = "http://wtl.MOE.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eMosBusiness : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetVersion()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                sb.AppendLine(assembly.FullName);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                MOELog.Error(ex);
                return "Error";
            }
        }

        #region Invoice
        [WebMethod]
        public InvoicePreview GetInvoicePreview(Entity.BLLEntity.UI.Document.InvoicePreviewInput uiInv)
        {
            try
            {
                BLL.Document.Invoice inv = new Westminster.MOE.BLL.Document.Invoice(Global.Config);

                return inv.GetInvoicePreview(uiInv);
            }
            catch (Exception ex)
            {
                MOELog.Error(ex);
                return null;
            }
        }

        [WebMethod]
        public InvoiceView GetInvoiceView(string companyCode, string invNum)
        {
            try
            {
                BLL.Document.Invoice inv = new Westminster.MOE.BLL.Document.Invoice(Global.Config);
                return inv.GetInvoiceView(companyCode,  invNum);
            }
            catch (Exception ex)
            {
                MOELog.Error(ex);
                return null;
            }
        }

        //[WebMethod]
        //public InvoiceView GetInvoiceView(string companyCode, string invNum)
        //{
        //    try
        //    {
        //        InvoiceView inv = hwj.CommonLibrary.Object.SerializationHelper.FromXml<InvoiceView>(hwj.CommonLibrary.Object.FileHelper.ReadFile(@"D:\\invview.xml"));
        //        return inv;
        //    }
        //    catch (Exception ex)
        //    {
        //        MOELog.Error(ex);
        //        return null;
        //    }
        //}
        #endregion

        #region Voucher
        [WebMethod]
        public VCHPreview GetVoucherPerview(MOEEnums.LanguageType language, VCHSelected input)
        {
            VCHPreview ui = new VCHPreview();
            try
            {
                MOECommon.SetLang(language);
                BLL.Document.Voucher vch = new Westminster.MOE.BLL.Document.Voucher(Global.Config);
                return vch.GetPreview(input);
            }
            catch (Exception ex)
            {
                ui.Result = new Westminster.MOE.Entity.BLLEntity.WSResult(MOEEnums.ResultStatus.Error, ex.Message);
                MOELog.Error(ex);
            }
            return ui;
        }

        [WebMethod]
        public WSResult CreateVoucher(MOEEnums.LanguageType language, VCHCreate save)
        {
            WSResult rs = new WSResult();
            try
            {
                BLL.Document.Voucher vch = new Westminster.MOE.BLL.Document.Voucher(Global.Config);
                return vch.CreateVoucher(save);
            }
            catch (Exception ex)
            {
                rs = new Westminster.MOE.Entity.BLLEntity.WSResult(MOEEnums.ResultStatus.Error, ex.Message);
                MOELog.Error(ex);
            }
            return rs;
        }
        #endregion

        #region Transfer
        [WebMethod]
        public WSResult CloneBooking(string companyCode, string sourceBkGref, string staffCode, string login)
        {
            WSResult result = new WSResult();
            string cloneBkGref, msg;

            try
            {
                using (BLL.Booking.CloneBooking booking = new BLL.Booking.CloneBooking(Global.Config))
                {
                    if (booking.DoWork(companyCode, sourceBkGref, staffCode, login, out cloneBkGref, out msg))
                    {
                        result.Status = MOEEnums.ResultStatus.Success;
                        result.Ext1 = cloneBkGref;
                    }
                    else
                    {
                        result.Status = MOEEnums.ResultStatus.InvalidData;
                        result.Msg = msg;
                    }
                }
            }
            catch (Exception ex)
            {
                MOELog.Error(string.Format("Clone Booking:{0}|{1}|{2}", companyCode, sourceBkGref, staffCode), ex);
                result.Status = MOEEnums.ResultStatus.Error;
                result.Msg = ex.Message;
            }

            return result;
        }

        [WebMethod]
        public WSResult LinkIUR2eMos(string companyCode, string eMosBkgRef, string IURBkgRef, string staffCode, string teamCode)
        {
            WSResult result = new WSResult();
            try
            {
                eMosBkgRef = eMosBkgRef.Trim().ToUpper();
                IURBkgRef = IURBkgRef.Trim().ToUpper();

                LinkPNR link = new LinkPNR(Global.Config);
                string errmsg = string.Empty;
                link.LinkPNR4emos(companyCode, eMosBkgRef, IURBkgRef, staffCode, teamCode, out result);

                //if (string.IsNullOrEmpty(errmsg))
                //{
                //    result.Status = MOEEnums.ResultStatus.Success;
                //}
                //else
                //{
                //    result.Status = MOEEnums.ResultStatus.InvalidData;
                //    result.Msg = errmsg;
                //}
            }
            catch (Exception ex)
            {
                MOELog.Error(ex);
                result.Status = MOEEnums.ResultStatus.Error;
                result.Msg = ex.Message;
            }
            return result;
        }
        #endregion
    }
}
