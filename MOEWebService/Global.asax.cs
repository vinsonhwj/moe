﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using hwj.CommonLibrary.Object;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Lib;
using Westminster.MOE.Lib.Configuration;
using hwj.CommonLibrary.Object.Email;

namespace Westminster.MOE.WebService
{
    public class Global : System.Web.HttpApplication
    {
        public static Config Config = null;

        protected void Application_Start(object sender, EventArgs e)
        {
            Config = new Config();

            Config.DatabaseList.Add(MOEEnums.DALConnectionType.eMos, hwj.CommonLibrary.Object.ConfigHelper.GetConnectionString("eMos.DBConn"));

            Config.Settings.Use05eMos = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("Use05eMos", "FALSE").ToUpper() == "TRUE";

            Config.Settings.AutoBookingOwner = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("AutoBookingOwner");
            Config.Settings.AutoBookingOwnerTeamCode = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("AutoBookingOwnerTeamCode");
            Config.Settings.ExchageRateOperator = "/";
            Config.Settings.RoundUpTorrent = 0.1M;

            Config.TsfrSettings.AnalysisCode = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("AnalysisCode", string.Empty);
            Config.TsfrSettings.IsForWebjet = hwj.CommonLibrary.Object.ConfigHelper.GetBoolSetting("IsForWebjet");
            Config.TsfrSettings.eInvoice_SendToCustomer = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eInvoice_SendToCustomer", string.Empty) == "0";
            Config.TsfrSettings.eInvoice_SendNow = hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eInvoice_SendNow", string.Empty) == "1";


            WebServiceConfig wsb3 = new WebServiceConfig(hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eMos05BookingSvcName"), hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eMos05BookingSvcUrl"));
            Config.WebServiceList.Add(MOEEnums.BindingName.eMos05BookingSoap, wsb3);
            WebServiceConfig wsb2 = new WebServiceConfig(hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("GatewayWebSvcName"), hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("GatewayWebSvcUrl"));
            Config.WebServiceList.Add(MOEEnums.BindingName.IURSoap, wsb2);
            WebServiceConfig wsb = new WebServiceConfig(hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eInvoiceSvcName"), hwj.CommonLibrary.Object.ConfigHelper.GetAppSetting("eInvoiceSvcUrl"));
            Config.WebServiceList.Add(MOEEnums.BindingName.ServiceSoap, wsb);
            Config.IsDevelopments = ConfigHelper.GetAppSetting("Developments", "FALSE").ToUpper() == "TRUE";
            Config.IsDebugLog = ConfigHelper.GetAppSetting("DebugLog", "FALSE").ToUpper() == "TRUE";

            string emailTo = ConfigHelper.GetAppSetting("LOG_EmailTo");
            string emailCC = ConfigHelper.GetAppSetting("LOG_EmailCC");
            string produceName = "MOE Web Service";
            SmtpInfoList smtpList = new SmtpInfoList(ConfigHelper.GetAppSetting("LOG_SmtpList"));

            MOELog.Initialization(produceName, Server.MapPath("~/LogHelper.config"), emailTo, emailCC, Config, smtpList);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}