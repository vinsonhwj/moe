﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Westminster.MOE.Entity.BLLEntity.Transfer;
using Westminster.MOE.Entity.DALEntity.Transfer;
using Westminster.MOE.Lib;
using System.Text;

namespace Westminster.MOE.WebService
{
    /// <summary>
    /// Summary description for eMosForAutoDocument
    /// </summary>
    [WebService(Namespace = "http://wtl.MOE.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eMosAutoDocument : System.Web.Services.WebService
    {

        [WebMethod]
        public RetrieveIURBooking RetrieveBooking_IUR(string companyCode, string PNR, GDS_Types gds, string[] tickets, string cltCode, string adcomTxno, string tourCode, string loginUser)
        {          

            string paramLog = "Method:RetrieveBooking_IUR";

            RetrieveBooking_IURParam param = new RetrieveBooking_IURParam();
            param.CompanyCode = companyCode;
            param.PNR = PNR;
            param.GDSType = gds;
            param.Tickets = tickets;
            param.CltCode = cltCode;
            param.ADComTxNum = adcomTxno;
            param.TourCode = tourCode;
            param.LoginUser = loginUser;
            param.TSANumber = string.Empty;
            param.InvType = Invoice_Types.Normal;

            paramLog = string.Format("{0}\r\n{1}", paramLog, hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(param));

            try
            {
                MOELog.DebugLog(paramLog);

                if (Global.Config.Settings.Use05eMos)
                {

                    eMosBookingService.BookingSoapClient bookingSvc = new Westminster.MOE.WebService.eMosBookingService.BookingSoapClient(Global.Config.WebServiceList[MOEEnums.BindingName.eMos05BookingSoap].Name, Global.Config.WebServiceList[MOEEnums.BindingName.eMos05BookingSoap].Url);
                    Westminster.MOE.WebService.eMosBookingService.GDS_Types gdsType = (Westminster.MOE.WebService.eMosBookingService.GDS_Types)Enum.Parse(typeof(Westminster.MOE.WebService.eMosBookingService.GDS_Types), gds.ToString());
                    Convert cvt = new Convert();
                    return cvt.ToMOEObj(bookingSvc.RetrieveBooking_IUR2(companyCode, PNR, gdsType, tickets, cltCode, adcomTxno, tourCode, loginUser, string.Empty));
                }


                Westminster.MOE.BLL.Transfer.AutoDocument ad = new Westminster.MOE.BLL.Transfer.AutoDocument(Global.Config);

                RetrieveIURBooking booking = ad.RetrieveBooking_IUR(param);

                if (!string.IsNullOrEmpty(booking.Exception))
                {
                    MOELog.Warn(booking.Exception, null, paramLog);
                }

                return booking;
            }
            catch (Exception ex)
            {
                MOELog.Error(ex.Message, ex, paramLog);
                return null;
            }
        }

        [WebMethod]
        public AutoBookingResponse AutoBooking2(RetrieveIURBooking booking, eMosParameter eMosParam, List<AirlineSegment> airSegment4Invoice, string invrmk, string staffcode, string teamcode, bool isAllowInvoice, bool isAllowEInvoice, string recipient, Invoice_Types invType)
        {

      


            string paramLog = "Method:AutoBooking2 ";

            AutoBookingParam param = new AutoBookingParam();
            param.Booking = booking;
            param.eMosParam = eMosParam;
            param.AirSegment4Invoice = airSegment4Invoice;
            param.InvRemark = invrmk;
            param.StaffCode = staffcode;
            param.TeamCode = teamcode;
            param.IsAllowInvoice = isAllowInvoice;
            param.IsAllowEInvoice = isAllowEInvoice;
            param.Recipient = recipient;
            param.InvType = invType;

            paramLog = string.Format("{0}\r\n{1}", paramLog, hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(param));
            try
            {
                MOELog.DebugLog(paramLog);

                if (Global.Config.Settings.Use05eMos)
                {
                    eMosBookingService.BookingSoapClient bookingSvc = new Westminster.MOE.WebService.eMosBookingService.BookingSoapClient();
                    Westminster.MOE.WebService.eMosBookingService.Invoice_Types iType = (Westminster.MOE.WebService.eMosBookingService.Invoice_Types)Enum.Parse(typeof(Westminster.MOE.WebService.eMosBookingService.Invoice_Types), invType.ToString());
                    Convert cvt = new Convert();
                    List<Westminster.MOE.WebService.eMosBookingService.AirlineSegment> asLst = new List<Westminster.MOE.WebService.eMosBookingService.AirlineSegment>();
                    foreach (AirlineSegment a in airSegment4Invoice)
                    {
                        asLst.Add(cvt.ToeMosObj(a));
                    }

                    return cvt.ToMOEObj(bookingSvc.AutoBooking2(cvt.ToeMosObj(booking), cvt.ToeMosObj(eMosParam), asLst.ToArray(), invrmk, staffcode, teamcode, isAllowInvoice, isAllowEInvoice, recipient, iType));
                }

                Westminster.MOE.BLL.Transfer.AutoDocument ad = new Westminster.MOE.BLL.Transfer.AutoDocument(Global.Config);
                return ad.AutoBooking(param);
            }
            catch (Exception ex)
            {
                MOELog.Error(ex.Message, ex, paramLog);
                return null;
            }
        }

        //[WebMethod]
        //public string TestATS()
        //{
        //    string[] ts = new string[1];
        //    ts[0] = "2742173381";
        //    //ts[1] = "9580319336";
        //    RetrieveIURBooking booking = RetrieveBooking_IUR("WM", "NKYYAI", GDS_Types.Abacus, ts, "CC1500", string.Empty, string.Empty, "WIL");
        //    AutoBooking2(booking, new eMosParameter(), new List<AirlineSegment>(), string.Empty, "WIL", string.Empty, false, false, string.Empty, Invoice_Types.Normal);
        //    return string.Empty;
        //}
    }
}
