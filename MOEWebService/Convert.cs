﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Westminster.MOE.WebService.eMosBookingService;

namespace Westminster.MOE.WebService
{
    public class Convert
    {
        public Westminster.MOE.Entity.BLLEntity.Transfer.RetrieveIURBooking ToMOEObj(Westminster.MOE.WebService.eMosBookingService.RetrieveIURBooking emosObj)
        {
            Westminster.MOE.Entity.BLLEntity.Transfer.RetrieveIURBooking moeObj = new Westminster.MOE.Entity.BLLEntity.Transfer.RetrieveIURBooking();
            if (emosObj != null)
            {

                if (emosObj.Peomstr != null)
                {
                    moeObj.Peomstr = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoMstr();
                    moeObj.Peomstr.COMPANYCODE = emosObj.Peomstr.COMPANYCODE;
                    moeObj.Peomstr.BKGREF = emosObj.Peomstr.BKGREF;
                    moeObj.Peomstr.agency_arc_iata_num = emosObj.Peomstr.agency_arc_iata_num;
                    moeObj.Peomstr.iss_off_code = emosObj.Peomstr.iss_off_code;
                    moeObj.Peomstr.tranflag = emosObj.Peomstr.tranflag;
                    moeObj.Peomstr.PNR = emosObj.Peomstr.PNR;
                    moeObj.Peomstr.CRSSINEIN = emosObj.Peomstr.CRSSINEIN;
                    moeObj.Peomstr.COD1 = emosObj.Peomstr.COD1;
                    moeObj.Peomstr.COD2 = emosObj.Peomstr.COD2;
                    moeObj.Peomstr.CLTODE = emosObj.Peomstr.CLTODE;
                    moeObj.Peomstr.CLTNAME = emosObj.Peomstr.CLTNAME;
                    moeObj.Peomstr.CLTADDR = emosObj.Peomstr.CLTADDR;
                    moeObj.Peomstr.STAFFCODE = emosObj.Peomstr.STAFFCODE;
                    moeObj.Peomstr.TEAMCODE = emosObj.Peomstr.TEAMCODE;
                    moeObj.Peomstr.FIRSTPAX = emosObj.Peomstr.FIRSTPAX;
                    moeObj.Peomstr.PHONE = emosObj.Peomstr.PHONE;
                    moeObj.Peomstr.FAX = emosObj.Peomstr.FAX;
                    moeObj.Peomstr.EMAIL = emosObj.Peomstr.EMAIL;
                    moeObj.Peomstr.MASTERPNR = emosObj.Peomstr.MASTERPNR;
                    moeObj.Peomstr.TOURCODE = emosObj.Peomstr.TOURCODE;
                    moeObj.Peomstr.CONTACTPERSON = emosObj.Peomstr.CONTACTPERSON;
                    moeObj.Peomstr.DEPARTDATE = emosObj.Peomstr.DEPARTDATE;
                    moeObj.Peomstr.TTLSELLCURR = emosObj.Peomstr.TTLSELLCURR;
                    moeObj.Peomstr.TTLSELLAMT = emosObj.Peomstr.TTLSELLAMT;
                    moeObj.Peomstr.TTLSELLTAXCURR = emosObj.Peomstr.TTLSELLTAXCURR;
                    moeObj.Peomstr.TTLSELLTAX = emosObj.Peomstr.TTLSELLTAX;
                    moeObj.Peomstr.TTLCOSTCURR = emosObj.Peomstr.TTLCOSTCURR;
                    moeObj.Peomstr.TTLCOSTAMT = emosObj.Peomstr.TTLCOSTAMT;
                    moeObj.Peomstr.TTLCOSTTAXCURR = emosObj.Peomstr.TTLCOSTTAXCURR;
                    moeObj.Peomstr.TTLCOSTTAX = emosObj.Peomstr.TTLCOSTTAX;
                    moeObj.Peomstr.INVCURR = emosObj.Peomstr.INVCURR;
                    moeObj.Peomstr.INVAMT = emosObj.Peomstr.INVAMT;
                    moeObj.Peomstr.INVTAXCURR = emosObj.Peomstr.INVTAXCURR;
                    moeObj.Peomstr.INVTAX = emosObj.Peomstr.INVTAX;
                    moeObj.Peomstr.DOCCURR = emosObj.Peomstr.DOCCURR;
                    moeObj.Peomstr.DOCAMT = emosObj.Peomstr.DOCAMT;
                    moeObj.Peomstr.DOCTAXCURR = emosObj.Peomstr.DOCTAXCURR;
                    moeObj.Peomstr.DOCTAX = emosObj.Peomstr.DOCTAX;
                    moeObj.Peomstr.INVCOUNT = emosObj.Peomstr.INVCOUNT;
                    moeObj.Peomstr.DOCCOUNT = emosObj.Peomstr.DOCCOUNT;
                    moeObj.Peomstr.RMK1 = emosObj.Peomstr.RMK1;
                    moeObj.Peomstr.RMK2 = emosObj.Peomstr.RMK2;
                    moeObj.Peomstr.RMK3 = emosObj.Peomstr.RMK3;
                    moeObj.Peomstr.RMK4 = emosObj.Peomstr.RMK4;
                    moeObj.Peomstr.DEADLINE = emosObj.Peomstr.DEADLINE;
                    moeObj.Peomstr.SOURCESYSTEM = emosObj.Peomstr.SOURCESYSTEM;
                    moeObj.Peomstr.SOURCE = emosObj.Peomstr.SOURCE;
                    moeObj.Peomstr.CREATEON = emosObj.Peomstr.CREATEON;
                    moeObj.Peomstr.CREATEBY = emosObj.Peomstr.CREATEBY;
                    moeObj.Peomstr.UPDATEON = emosObj.Peomstr.UPDATEON;
                    moeObj.Peomstr.UPDATEBY = emosObj.Peomstr.UPDATEBY;
                    moeObj.Peomstr.COD3 = emosObj.Peomstr.COD3;
                    moeObj.Peomstr.INSFLAG = emosObj.Peomstr.INSFLAG;
                    moeObj.Peomstr.TOEMOSON = emosObj.Peomstr.TOEMOSON;
                    moeObj.Peomstr.TOEMOSBY = emosObj.Peomstr.TOEMOSBY;
                    moeObj.Peomstr.EMOSBKGREF = emosObj.Peomstr.EMOSBKGREF;
                    moeObj.Peomstr.IURFLAG = emosObj.Peomstr.IURFLAG;
                    moeObj.Peomstr.pseudo = emosObj.Peomstr.pseudo;
                    moeObj.Peomstr.pseudo_inv = emosObj.Peomstr.pseudo_inv;
                    moeObj.Peomstr.CRSSINEIN_inv = emosObj.Peomstr.CRSSINEIN_inv;
                }

                moeObj.AirlineSegmentsList = new Westminster.MOE.Entity.BLLEntity.Transfer.AirlineSegments();
                if (emosObj.AirlineSegmentsList != null)
                {
                    foreach (Westminster.MOE.WebService.eMosBookingService.AirlineSegment _frAirlineSegment in emosObj.AirlineSegmentsList)
                    {
                        Westminster.MOE.Entity.BLLEntity.Transfer.AirlineSegment _AirlineSegment = new Westminster.MOE.Entity.BLLEntity.Transfer.AirlineSegment();

                        if (_frAirlineSegment.UO_PEOAIR != null)
                        {
                            _AirlineSegment.UO_PEOAIR = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIR();
                            _AirlineSegment.UO_PEOAIR.CompanyCode = _frAirlineSegment.UO_PEOAIR.CompanyCode;
                            _AirlineSegment.UO_PEOAIR.BkgRef = _frAirlineSegment.UO_PEOAIR.BkgRef;
                            _AirlineSegment.UO_PEOAIR.SegNum = _frAirlineSegment.UO_PEOAIR.SegNum;
                            _AirlineSegment.UO_PEOAIR.PNR = _frAirlineSegment.UO_PEOAIR.PNR;
                            _AirlineSegment.UO_PEOAIR.Flight = _frAirlineSegment.UO_PEOAIR.Flight;
                            _AirlineSegment.UO_PEOAIR.Class = _frAirlineSegment.UO_PEOAIR.Class;
                            _AirlineSegment.UO_PEOAIR.DepartCity = _frAirlineSegment.UO_PEOAIR.DepartCity;
                            _AirlineSegment.UO_PEOAIR.ArrivalCity = _frAirlineSegment.UO_PEOAIR.ArrivalCity;
                            _AirlineSegment.UO_PEOAIR.Status = _frAirlineSegment.UO_PEOAIR.Status;
                            _AirlineSegment.UO_PEOAIR.NumOfPax = _frAirlineSegment.UO_PEOAIR.NumOfPax;
                            _AirlineSegment.UO_PEOAIR.DepartDATE = _frAirlineSegment.UO_PEOAIR.DepartDATE;
                            _AirlineSegment.UO_PEOAIR.DepartTime = _frAirlineSegment.UO_PEOAIR.DepartTime;
                            _AirlineSegment.UO_PEOAIR.ArrDATE = _frAirlineSegment.UO_PEOAIR.ArrDATE;
                            _AirlineSegment.UO_PEOAIR.ArrTime = _frAirlineSegment.UO_PEOAIR.ArrTime;
                            _AirlineSegment.UO_PEOAIR.ElapsedTime = _frAirlineSegment.UO_PEOAIR.ElapsedTime;
                            _AirlineSegment.UO_PEOAIR.SSR = _frAirlineSegment.UO_PEOAIR.SSR;
                            _AirlineSegment.UO_PEOAIR.Seat = _frAirlineSegment.UO_PEOAIR.Seat;
                            _AirlineSegment.UO_PEOAIR.ReasonCode = _frAirlineSegment.UO_PEOAIR.ReasonCode;
                            _AirlineSegment.UO_PEOAIR.StopOverNum = _frAirlineSegment.UO_PEOAIR.StopOverNum;
                            _AirlineSegment.UO_PEOAIR.StopOverCity = _frAirlineSegment.UO_PEOAIR.StopOverCity;
                            _AirlineSegment.UO_PEOAIR.Pcode = _frAirlineSegment.UO_PEOAIR.Pcode;
                            _AirlineSegment.UO_PEOAIR.SpecialRQ = _frAirlineSegment.UO_PEOAIR.SpecialRQ;
                            _AirlineSegment.UO_PEOAIR.RMK1 = _frAirlineSegment.UO_PEOAIR.RMK1;
                            _AirlineSegment.UO_PEOAIR.RMK2 = _frAirlineSegment.UO_PEOAIR.RMK2;
                            _AirlineSegment.UO_PEOAIR.INVRMK1 = _frAirlineSegment.UO_PEOAIR.INVRMK1;
                            _AirlineSegment.UO_PEOAIR.INVRMK2 = _frAirlineSegment.UO_PEOAIR.INVRMK2;
                            _AirlineSegment.UO_PEOAIR.VCHRMK1 = _frAirlineSegment.UO_PEOAIR.VCHRMK1;
                            _AirlineSegment.UO_PEOAIR.VCHRMK2 = _frAirlineSegment.UO_PEOAIR.VCHRMK2;
                            _AirlineSegment.UO_PEOAIR.SELLCURR = _frAirlineSegment.UO_PEOAIR.SELLCURR;
                            _AirlineSegment.UO_PEOAIR.TOTALSELL = _frAirlineSegment.UO_PEOAIR.TOTALSELL;
                            _AirlineSegment.UO_PEOAIR.TOTALSTAX = _frAirlineSegment.UO_PEOAIR.TOTALSTAX;
                            _AirlineSegment.UO_PEOAIR.COSTCURR = _frAirlineSegment.UO_PEOAIR.COSTCURR;
                            _AirlineSegment.UO_PEOAIR.TOTALCOST = _frAirlineSegment.UO_PEOAIR.TOTALCOST;
                            _AirlineSegment.UO_PEOAIR.TOTALCTAX = _frAirlineSegment.UO_PEOAIR.TOTALCTAX;
                            _AirlineSegment.UO_PEOAIR.SourceSystem = _frAirlineSegment.UO_PEOAIR.SourceSystem;
                            _AirlineSegment.UO_PEOAIR.CreateOn = _frAirlineSegment.UO_PEOAIR.CreateOn;
                            _AirlineSegment.UO_PEOAIR.CreateBy = _frAirlineSegment.UO_PEOAIR.CreateBy;
                            _AirlineSegment.UO_PEOAIR.UpdateOn = _frAirlineSegment.UO_PEOAIR.UpdateOn;
                            _AirlineSegment.UO_PEOAIR.UpdateBy = _frAirlineSegment.UO_PEOAIR.UpdateBy;
                            _AirlineSegment.UO_PEOAIR.VoidOn = _frAirlineSegment.UO_PEOAIR.VoidOn;
                            _AirlineSegment.UO_PEOAIR.VoidBy = _frAirlineSegment.UO_PEOAIR.VoidBy;
                            _AirlineSegment.UO_PEOAIR.AirSeg = _frAirlineSegment.UO_PEOAIR.AirSeg;
                            _AirlineSegment.UO_PEOAIR.DepartTerminal = _frAirlineSegment.UO_PEOAIR.DepartTerminal;
                            _AirlineSegment.UO_PEOAIR.ArrivalTerminal = _frAirlineSegment.UO_PEOAIR.ArrivalTerminal;
                            _AirlineSegment.UO_PEOAIR.OtherAirlinePNR = _frAirlineSegment.UO_PEOAIR.OtherAirlinePNR;
                            _AirlineSegment.UO_PEOAIR.EQP = _frAirlineSegment.UO_PEOAIR.EQP;
                            _AirlineSegment.UO_PEOAIR.Service = _frAirlineSegment.UO_PEOAIR.Service;
                            _AirlineSegment.UO_PEOAIR.Suppcode = _frAirlineSegment.UO_PEOAIR.Suppcode;
                        }

                        _AirlineSegment.UOList_PEOAIRDETAIL = new List<Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIRDETAIL>();
                        if (_frAirlineSegment.UOList_PEOAIRDETAIL != null)
                        {
                            foreach (Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL _frUO_PEOAIRDETAIL in _frAirlineSegment.UOList_PEOAIRDETAIL)
                            {
                                Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIRDETAIL _UO_PEOAIRDETAIL = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIRDETAIL();
                                _UO_PEOAIRDETAIL.CompanyCode = _frUO_PEOAIRDETAIL.CompanyCode;
                                _UO_PEOAIRDETAIL.BkgRef = _frUO_PEOAIRDETAIL.BkgRef;
                                _UO_PEOAIRDETAIL.SegNum = _frUO_PEOAIRDETAIL.SegNum;
                                _UO_PEOAIRDETAIL.SEQNUM = _frUO_PEOAIRDETAIL.SEQNUM;
                                _UO_PEOAIRDETAIL.SEQTYPE = _frUO_PEOAIRDETAIL.SEQTYPE;
                                _UO_PEOAIRDETAIL.QTY = _frUO_PEOAIRDETAIL.QTY;
                                _UO_PEOAIRDETAIL.CURR = _frUO_PEOAIRDETAIL.CURR;
                                _UO_PEOAIRDETAIL.AMT = _frUO_PEOAIRDETAIL.AMT;
                                _UO_PEOAIRDETAIL.TAXCURR = _frUO_PEOAIRDETAIL.TAXCURR;
                                _UO_PEOAIRDETAIL.TAXAMT = _frUO_PEOAIRDETAIL.TAXAMT;
                                _UO_PEOAIRDETAIL.CREATEON = _frUO_PEOAIRDETAIL.CREATEON;
                                _UO_PEOAIRDETAIL.CREATEBY = _frUO_PEOAIRDETAIL.CREATEBY;
                                _UO_PEOAIRDETAIL.UPDATEON = _frUO_PEOAIRDETAIL.UPDATEON;
                                _UO_PEOAIRDETAIL.UPDATEBY = _frUO_PEOAIRDETAIL.UPDATEBY;
                                _UO_PEOAIRDETAIL.AGETYPE = _frUO_PEOAIRDETAIL.AGETYPE;


                                _AirlineSegment.UOList_PEOAIRDETAIL.Add(_UO_PEOAIRDETAIL);
                            }
                        }

                        moeObj.AirlineSegmentsList.Add(_AirlineSegment);
                    }
                }

                moeObj.HotelSegmentsList = new Westminster.MOE.Entity.BLLEntity.Transfer.HotelSegments();
                if (emosObj.HotelSegmentsList != null)
                {
                    foreach (Westminster.MOE.WebService.eMosBookingService.HotelSegment _frHotelSegment in emosObj.HotelSegmentsList)
                    {
                        Westminster.MOE.Entity.BLLEntity.Transfer.HotelSegment _HotelSegment = new Westminster.MOE.Entity.BLLEntity.Transfer.HotelSegment();

                        if (_frHotelSegment.PEOHTL != null)
                        {
                            _HotelSegment.PEOHTL = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOHTL();
                            _HotelSegment.PEOHTL.BKGREF = _frHotelSegment.PEOHTL.BKGREF;
                            _HotelSegment.PEOHTL.COMPANYCODE = _frHotelSegment.PEOHTL.COMPANYCODE;
                            _HotelSegment.PEOHTL.SEGNUM = _frHotelSegment.PEOHTL.SEGNUM;
                            _HotelSegment.PEOHTL.COUNTRYCODE = _frHotelSegment.PEOHTL.COUNTRYCODE;
                            _HotelSegment.PEOHTL.CITYCODE = _frHotelSegment.PEOHTL.CITYCODE;
                            _HotelSegment.PEOHTL.ADCOMTXNO = _frHotelSegment.PEOHTL.ADCOMTXNO;
                            _HotelSegment.PEOHTL.ADCOMSEQNUM = _frHotelSegment.PEOHTL.ADCOMSEQNUM;
                            _HotelSegment.PEOHTL.HOTELCODE = _frHotelSegment.PEOHTL.HOTELCODE;
                            _HotelSegment.PEOHTL.ARRDATE = _frHotelSegment.PEOHTL.ARRDATE;
                            _HotelSegment.PEOHTL.ARRTIME = _frHotelSegment.PEOHTL.ARRTIME;
                            _HotelSegment.PEOHTL.DEPARTDATE = _frHotelSegment.PEOHTL.DEPARTDATE;
                            _HotelSegment.PEOHTL.DEPARTTIME = _frHotelSegment.PEOHTL.DEPARTTIME;
                            _HotelSegment.PEOHTL.ETA = _frHotelSegment.PEOHTL.ETA;
                            _HotelSegment.PEOHTL.ETD = _frHotelSegment.PEOHTL.ETD;
                            _HotelSegment.PEOHTL.SELLCURR = _frHotelSegment.PEOHTL.SELLCURR;
                            _HotelSegment.PEOHTL.COSTCURR = _frHotelSegment.PEOHTL.COSTCURR;
                            _HotelSegment.PEOHTL.TOTALCOST = _frHotelSegment.PEOHTL.TOTALCOST;
                            _HotelSegment.PEOHTL.TOTALSELL = _frHotelSegment.PEOHTL.TOTALSELL;
                            _HotelSegment.PEOHTL.TOTALCTAX = _frHotelSegment.PEOHTL.TOTALCTAX;
                            _HotelSegment.PEOHTL.TOTALSTAX = _frHotelSegment.PEOHTL.TOTALSTAX;
                            _HotelSegment.PEOHTL.FORMPAY = _frHotelSegment.PEOHTL.FORMPAY;
                            _HotelSegment.PEOHTL.RMK1 = _frHotelSegment.PEOHTL.RMK1;
                            _HotelSegment.PEOHTL.RMK2 = _frHotelSegment.PEOHTL.RMK2;
                            _HotelSegment.PEOHTL.INVRMK1 = _frHotelSegment.PEOHTL.INVRMK1;
                            _HotelSegment.PEOHTL.INVRMK2 = _frHotelSegment.PEOHTL.INVRMK2;
                            _HotelSegment.PEOHTL.VCHRMK1 = _frHotelSegment.PEOHTL.VCHRMK1;
                            _HotelSegment.PEOHTL.VCHRMK2 = _frHotelSegment.PEOHTL.VCHRMK2;
                            _HotelSegment.PEOHTL.WESTELNO = _frHotelSegment.PEOHTL.WESTELNO;
                            _HotelSegment.PEOHTL.SOURCESYSTEM = _frHotelSegment.PEOHTL.SOURCESYSTEM;
                            _HotelSegment.PEOHTL.SPCL_REQUEST = _frHotelSegment.PEOHTL.SPCL_REQUEST;
                            _HotelSegment.PEOHTL.HTLREFERENCE = _frHotelSegment.PEOHTL.HTLREFERENCE;
                            _HotelSegment.PEOHTL.HTLCFmby = _frHotelSegment.PEOHTL.HTLCFmby;
                            _HotelSegment.PEOHTL.VOIDBY = _frHotelSegment.PEOHTL.VOIDBY;
                            _HotelSegment.PEOHTL.VOIDON = _frHotelSegment.PEOHTL.VOIDON;
                            _HotelSegment.PEOHTL.CREATEBY = _frHotelSegment.PEOHTL.CREATEBY;
                            _HotelSegment.PEOHTL.CREATEON = _frHotelSegment.PEOHTL.CREATEON;
                            _HotelSegment.PEOHTL.UPDATEBY = _frHotelSegment.PEOHTL.UPDATEBY;
                            _HotelSegment.PEOHTL.UPDATEON = _frHotelSegment.PEOHTL.UPDATEON;
                            _HotelSegment.PEOHTL.SOURCEREF = _frHotelSegment.PEOHTL.SOURCEREF;
                            _HotelSegment.PEOHTL.GSTTAX = _frHotelSegment.PEOHTL.GSTTAX;
                            _HotelSegment.PEOHTL.showDetailFee = _frHotelSegment.PEOHTL.showDetailFee;
                            _HotelSegment.PEOHTL.SuppCode = _frHotelSegment.PEOHTL.SuppCode;
                            _HotelSegment.PEOHTL.hotelVoucherType = _frHotelSegment.PEOHTL.hotelVoucherType;
                            _HotelSegment.PEOHTL.COD1 = _frHotelSegment.PEOHTL.COD1;
                            _HotelSegment.PEOHTL.COD2 = _frHotelSegment.PEOHTL.COD2;
                            _HotelSegment.PEOHTL.COD3 = _frHotelSegment.PEOHTL.COD3;
                            _HotelSegment.PEOHTL.roomTypeCode = _frHotelSegment.PEOHTL.roomTypeCode;
                            _HotelSegment.PEOHTL.fullRoomRate = _frHotelSegment.PEOHTL.fullRoomRate;
                            _HotelSegment.PEOHTL.dailyRate = _frHotelSegment.PEOHTL.dailyRate;
                            _HotelSegment.PEOHTL.lowRoomRate = _frHotelSegment.PEOHTL.lowRoomRate;
                            _HotelSegment.PEOHTL.RateTypeCode = _frHotelSegment.PEOHTL.RateTypeCode;
                            _HotelSegment.PEOHTL.isRecalable = _frHotelSegment.PEOHTL.isRecalable;
                            _HotelSegment.PEOHTL.ReasonCode = _frHotelSegment.PEOHTL.ReasonCode;
                            _HotelSegment.PEOHTL.HOTELNAME = _frHotelSegment.PEOHTL.HOTELNAME;

                        }

                        _HotelSegment.PeoHtlDetail = new List<Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoHtlDetail>();
                        if (_frHotelSegment.PeoHtlDetail != null)
                        {
                            foreach (Westminster.MOE.WebService.eMosBookingService.UO_PeoHtlDetail _frUO_PeoHtlDetail in _frHotelSegment.PeoHtlDetail)
                            {
                                Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoHtlDetail _UO_PeoHtlDetail = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoHtlDetail();
                                _UO_PeoHtlDetail.BKGREF = _frUO_PeoHtlDetail.BKGREF;
                                _UO_PeoHtlDetail.COMPANYCODE = _frUO_PeoHtlDetail.COMPANYCODE;
                                _UO_PeoHtlDetail.SEQTYPE = _frUO_PeoHtlDetail.SEQTYPE;
                                _UO_PeoHtlDetail.SEGNUM = _frUO_PeoHtlDetail.SEGNUM;
                                _UO_PeoHtlDetail.SEQNUM = _frUO_PeoHtlDetail.SEQNUM;
                                _UO_PeoHtlDetail.QTY = _frUO_PeoHtlDetail.QTY;
                                _UO_PeoHtlDetail.ARRDATE = _frUO_PeoHtlDetail.ARRDATE;
                                _UO_PeoHtlDetail.DEPARTDATE = _frUO_PeoHtlDetail.DEPARTDATE;
                                _UO_PeoHtlDetail.RMNTS = _frUO_PeoHtlDetail.RMNTS;
                                _UO_PeoHtlDetail.RATENAME = _frUO_PeoHtlDetail.RATENAME;
                                _UO_PeoHtlDetail.ITEMNAME = _frUO_PeoHtlDetail.ITEMNAME;
                                _UO_PeoHtlDetail.STATUS = _frUO_PeoHtlDetail.STATUS;
                                _UO_PeoHtlDetail.CURR = _frUO_PeoHtlDetail.CURR;
                                _UO_PeoHtlDetail.AMT = _frUO_PeoHtlDetail.AMT;
                                _UO_PeoHtlDetail.TOTALAMT = _frUO_PeoHtlDetail.TOTALAMT;
                                _UO_PeoHtlDetail.TAXCURR = _frUO_PeoHtlDetail.TAXCURR;
                                _UO_PeoHtlDetail.TAXAMT = _frUO_PeoHtlDetail.TAXAMT;
                                _UO_PeoHtlDetail.CREATEBY = _frUO_PeoHtlDetail.CREATEBY;
                                _UO_PeoHtlDetail.CREATEON = _frUO_PeoHtlDetail.CREATEON;
                                _UO_PeoHtlDetail.UPDATEBY = _frUO_PeoHtlDetail.UPDATEBY;
                                _UO_PeoHtlDetail.UPDATEON = _frUO_PeoHtlDetail.UPDATEON;
                                _UO_PeoHtlDetail.itemtype = _frUO_PeoHtlDetail.itemtype;
                                _UO_PeoHtlDetail.AMTFEE = _frUO_PeoHtlDetail.AMTFEE;

                                _HotelSegment.PeoHtlDetail.Add(_UO_PeoHtlDetail);
                            }
                        }

                        moeObj.HotelSegmentsList.Add(_HotelSegment);
                    }
                }

                moeObj.OtherSegmentsList = new Westminster.MOE.Entity.BLLEntity.Transfer.OtherSegments();
                if (emosObj.OtherSegmentsList != null)
                {
                    foreach (Westminster.MOE.WebService.eMosBookingService.OtherSegment _frOtherSegment in emosObj.OtherSegmentsList)
                    {
                        Westminster.MOE.Entity.BLLEntity.Transfer.OtherSegment _OtherSegment = new Westminster.MOE.Entity.BLLEntity.Transfer.OtherSegment();

                        if (_frOtherSegment.PEOOTHER != null)
                        {
                            _OtherSegment.PEOOTHER = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOOTHER();
                            _OtherSegment.PEOOTHER.BKGREF = _frOtherSegment.PEOOTHER.BKGREF;
                            _OtherSegment.PEOOTHER.COMPANYCODE = _frOtherSegment.PEOOTHER.COMPANYCODE;
                            _OtherSegment.PEOOTHER.SEGNUM = _frOtherSegment.PEOOTHER.SEGNUM;
                            _OtherSegment.PEOOTHER.SEGTYPE = _frOtherSegment.PEOOTHER.SEGTYPE;
                            _OtherSegment.PEOOTHER.OTHERDATE = _frOtherSegment.PEOOTHER.OTHERDATE;
                            _OtherSegment.PEOOTHER.DESCRIPT1 = _frOtherSegment.PEOOTHER.DESCRIPT1;
                            _OtherSegment.PEOOTHER.DESCRIPT2 = _frOtherSegment.PEOOTHER.DESCRIPT2;
                            _OtherSegment.PEOOTHER.DESCRIPT3 = _frOtherSegment.PEOOTHER.DESCRIPT3;
                            _OtherSegment.PEOOTHER.DESCRIPT4 = _frOtherSegment.PEOOTHER.DESCRIPT4;
                            _OtherSegment.PEOOTHER.CITYCODE = _frOtherSegment.PEOOTHER.CITYCODE;
                            _OtherSegment.PEOOTHER.COUNTRYCODE = _frOtherSegment.PEOOTHER.COUNTRYCODE;
                            _OtherSegment.PEOOTHER.RMK1 = _frOtherSegment.PEOOTHER.RMK1;
                            _OtherSegment.PEOOTHER.RMK2 = _frOtherSegment.PEOOTHER.RMK2;
                            _OtherSegment.PEOOTHER.INVRMK1 = _frOtherSegment.PEOOTHER.INVRMK1;
                            _OtherSegment.PEOOTHER.INVRMK2 = _frOtherSegment.PEOOTHER.INVRMK2;
                            _OtherSegment.PEOOTHER.VCHRMK1 = _frOtherSegment.PEOOTHER.VCHRMK1;
                            _OtherSegment.PEOOTHER.VCHRMK2 = _frOtherSegment.PEOOTHER.VCHRMK2;
                            _OtherSegment.PEOOTHER.SELLCURR = _frOtherSegment.PEOOTHER.SELLCURR;
                            _OtherSegment.PEOOTHER.TOTALSELL = _frOtherSegment.PEOOTHER.TOTALSELL;
                            _OtherSegment.PEOOTHER.TOTALSTAX = _frOtherSegment.PEOOTHER.TOTALSTAX;
                            _OtherSegment.PEOOTHER.COSTCURR = _frOtherSegment.PEOOTHER.COSTCURR;
                            _OtherSegment.PEOOTHER.TOTALCOST = _frOtherSegment.PEOOTHER.TOTALCOST;
                            _OtherSegment.PEOOTHER.TOTALCTAX = _frOtherSegment.PEOOTHER.TOTALCTAX;
                            _OtherSegment.PEOOTHER.PCODE = _frOtherSegment.PEOOTHER.PCODE;
                            _OtherSegment.PEOOTHER.VOIDON = _frOtherSegment.PEOOTHER.VOIDON;
                            _OtherSegment.PEOOTHER.VOIDBY = _frOtherSegment.PEOOTHER.VOIDBY;
                            _OtherSegment.PEOOTHER.SOURCESYSTEM = _frOtherSegment.PEOOTHER.SOURCESYSTEM;
                            _OtherSegment.PEOOTHER.suppliercode = _frOtherSegment.PEOOTHER.suppliercode;
                            _OtherSegment.PEOOTHER.CREATEON = _frOtherSegment.PEOOTHER.CREATEON;
                            _OtherSegment.PEOOTHER.CREATEBY = _frOtherSegment.PEOOTHER.CREATEBY;
                            _OtherSegment.PEOOTHER.UPDATEON = _frOtherSegment.PEOOTHER.UPDATEON;
                            _OtherSegment.PEOOTHER.UPDATEBY = _frOtherSegment.PEOOTHER.UPDATEBY;
                            _OtherSegment.PEOOTHER.GSTTAX = _frOtherSegment.PEOOTHER.GSTTAX;
                            _OtherSegment.PEOOTHER.showDetailFee = _frOtherSegment.PEOOTHER.showDetailFee;
                            _OtherSegment.PEOOTHER.masterDesc = _frOtherSegment.PEOOTHER.masterDesc;
                            _OtherSegment.PEOOTHER.IsRecalable = _frOtherSegment.PEOOTHER.IsRecalable;
                        }

                        _OtherSegment.List_PEOOTHERDETAIL = new List<Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOOTHERDETAIL>();
                        if (_frOtherSegment.List_PEOOTHERDETAIL != null)
                        {
                            foreach (Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHERDETAIL _frUO_PEOOTHERDETAIL in _frOtherSegment.List_PEOOTHERDETAIL)
                            {
                                Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOOTHERDETAIL _UO_PEOOTHERDETAIL = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOOTHERDETAIL();
                                _UO_PEOOTHERDETAIL.BKGREF = _frUO_PEOOTHERDETAIL.BKGREF;
                                _UO_PEOOTHERDETAIL.COMPANYCODE = _frUO_PEOOTHERDETAIL.COMPANYCODE;
                                _UO_PEOOTHERDETAIL.SEGNUM = _frUO_PEOOTHERDETAIL.SEGNUM;
                                _UO_PEOOTHERDETAIL.SEQNUM = _frUO_PEOOTHERDETAIL.SEQNUM;
                                _UO_PEOOTHERDETAIL.SEQTYPE = _frUO_PEOOTHERDETAIL.SEQTYPE;
                                _UO_PEOOTHERDETAIL.AGETYPE = _frUO_PEOOTHERDETAIL.AGETYPE;
                                _UO_PEOOTHERDETAIL.QTY = _frUO_PEOOTHERDETAIL.QTY;
                                _UO_PEOOTHERDETAIL.CURR = _frUO_PEOOTHERDETAIL.CURR;
                                _UO_PEOOTHERDETAIL.AMT = _frUO_PEOOTHERDETAIL.AMT;
                                _UO_PEOOTHERDETAIL.TAXCURR = _frUO_PEOOTHERDETAIL.TAXCURR;
                                _UO_PEOOTHERDETAIL.TAXAMT = _frUO_PEOOTHERDETAIL.TAXAMT;
                                _UO_PEOOTHERDETAIL.VOIDON = _frUO_PEOOTHERDETAIL.VOIDON;
                                _UO_PEOOTHERDETAIL.VOIDBY = _frUO_PEOOTHERDETAIL.VOIDBY;
                                _UO_PEOOTHERDETAIL.SOURCESYSTEM = _frUO_PEOOTHERDETAIL.SOURCESYSTEM;
                                _UO_PEOOTHERDETAIL.CREATEON = _frUO_PEOOTHERDETAIL.CREATEON;
                                _UO_PEOOTHERDETAIL.CREATEBY = _frUO_PEOOTHERDETAIL.CREATEBY;
                                _UO_PEOOTHERDETAIL.UPDATEON = _frUO_PEOOTHERDETAIL.UPDATEON;
                                _UO_PEOOTHERDETAIL.UPDATEBY = _frUO_PEOOTHERDETAIL.UPDATEBY;
                                _UO_PEOOTHERDETAIL.AMTFEE = _frUO_PEOOTHERDETAIL.AMTFEE;

                                _OtherSegment.List_PEOOTHERDETAIL.Add(_UO_PEOOTHERDETAIL);
                            }
                        }

                        moeObj.OtherSegmentsList.Add(_OtherSegment);
                    }
                }

                moeObj.TicketSegmentsList = new Westminster.MOE.Entity.BLLEntity.Transfer.TicketSegments();
                if (emosObj.TicketSegmentsList != null)
                {
                    foreach (Westminster.MOE.WebService.eMosBookingService.TicketSegment _frTicketSegment in emosObj.TicketSegmentsList)
                    {
                        Westminster.MOE.Entity.BLLEntity.Transfer.TicketSegment _TicketSegment = new Westminster.MOE.Entity.BLLEntity.Transfer.TicketSegment();

                        if (_frTicketSegment.UO_PeoTkt != null)
                        {
                            _TicketSegment.UO_PeoTkt = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoTkt();
                            _TicketSegment.UO_PeoTkt.CompanyCode = _frTicketSegment.UO_PeoTkt.CompanyCode;
                            _TicketSegment.UO_PeoTkt.Ticket = _frTicketSegment.UO_PeoTkt.Ticket;
                            _TicketSegment.UO_PeoTkt.FareCalData = _frTicketSegment.UO_PeoTkt.FareCalData;
                            _TicketSegment.UO_PeoTkt.pseudo = _frTicketSegment.UO_PeoTkt.pseudo;
                            _TicketSegment.UO_PeoTkt.sinein = _frTicketSegment.UO_PeoTkt.sinein;
                            _TicketSegment.UO_PeoTkt.AirSeg = _frTicketSegment.UO_PeoTkt.AirSeg;
                            _TicketSegment.UO_PeoTkt.TransferFlag = _frTicketSegment.UO_PeoTkt.TransferFlag;
                            _TicketSegment.UO_PeoTkt.nst = _frTicketSegment.UO_PeoTkt.nst;
                            _TicketSegment.UO_PeoTkt.SOURCESYSTEM = _frTicketSegment.UO_PeoTkt.SOURCESYSTEM;
                            _TicketSegment.UO_PeoTkt.commission_per = _frTicketSegment.UO_PeoTkt.commission_per;
                            _TicketSegment.UO_PeoTkt.add_payment = _frTicketSegment.UO_PeoTkt.add_payment;
                            _TicketSegment.UO_PeoTkt.iss_off_code = _frTicketSegment.UO_PeoTkt.iss_off_code;
                            _TicketSegment.UO_PeoTkt.tour_code = _frTicketSegment.UO_PeoTkt.tour_code;
                            _TicketSegment.UO_PeoTkt.org_iss = _frTicketSegment.UO_PeoTkt.org_iss;
                            _TicketSegment.UO_PeoTkt.iss_in_exchange = _frTicketSegment.UO_PeoTkt.iss_in_exchange;
                            _TicketSegment.UO_PeoTkt.ControlNum = _frTicketSegment.UO_PeoTkt.ControlNum;
                            _TicketSegment.UO_PeoTkt.ENDORSEMENTS = _frTicketSegment.UO_PeoTkt.ENDORSEMENTS;
                            _TicketSegment.UO_PeoTkt.AirReason = _frTicketSegment.UO_PeoTkt.AirReason;
                            _TicketSegment.UO_PeoTkt.Destination = _frTicketSegment.UO_PeoTkt.Destination;
                            _TicketSegment.UO_PeoTkt.INTDOM = _frTicketSegment.UO_PeoTkt.INTDOM;
                            _TicketSegment.UO_PeoTkt.PAXAIR = _frTicketSegment.UO_PeoTkt.PAXAIR;
                            _TicketSegment.UO_PeoTkt.Class = _frTicketSegment.UO_PeoTkt.Class;
                            _TicketSegment.UO_PeoTkt.tkttype = _frTicketSegment.UO_PeoTkt.tkttype;
                            _TicketSegment.UO_PeoTkt.SellCurr = _frTicketSegment.UO_PeoTkt.SellCurr;
                            _TicketSegment.UO_PeoTkt.SellFare = _frTicketSegment.UO_PeoTkt.SellFare;
                            _TicketSegment.UO_PeoTkt.CorpCurr = _frTicketSegment.UO_PeoTkt.CorpCurr;
                            _TicketSegment.UO_PeoTkt.CorpFare = _frTicketSegment.UO_PeoTkt.CorpFare;
                            _TicketSegment.UO_PeoTkt.COD4 = _frTicketSegment.UO_PeoTkt.COD4;
                            _TicketSegment.UO_PeoTkt.COD5 = _frTicketSegment.UO_PeoTkt.COD5;
                            _TicketSegment.UO_PeoTkt.COD6 = _frTicketSegment.UO_PeoTkt.COD6;
                            _TicketSegment.UO_PeoTkt.tranflag = _frTicketSegment.UO_PeoTkt.tranflag;
                            _TicketSegment.UO_PeoTkt.TktSeq = _frTicketSegment.UO_PeoTkt.TktSeq;
                            _TicketSegment.UO_PeoTkt.BKGREF = _frTicketSegment.UO_PeoTkt.BKGREF;
                            _TicketSegment.UO_PeoTkt.PNR = _frTicketSegment.UO_PeoTkt.PNR;
                            _TicketSegment.UO_PeoTkt.CltCode = _frTicketSegment.UO_PeoTkt.CltCode;
                            _TicketSegment.UO_PeoTkt.SuppCode = _frTicketSegment.UO_PeoTkt.SuppCode;
                            _TicketSegment.UO_PeoTkt.InvNum = _frTicketSegment.UO_PeoTkt.InvNum;
                            _TicketSegment.UO_PeoTkt.Jqty = _frTicketSegment.UO_PeoTkt.Jqty;
                            _TicketSegment.UO_PeoTkt.AirCode = _frTicketSegment.UO_PeoTkt.AirCode;
                            _TicketSegment.UO_PeoTkt.Airline = _frTicketSegment.UO_PeoTkt.Airline;
                            _TicketSegment.UO_PeoTkt.PaxName = _frTicketSegment.UO_PeoTkt.PaxName;
                            _TicketSegment.UO_PeoTkt.DepartOn = _frTicketSegment.UO_PeoTkt.DepartOn;
                            _TicketSegment.UO_PeoTkt.COD1 = _frTicketSegment.UO_PeoTkt.COD1;
                            _TicketSegment.UO_PeoTkt.COD2 = _frTicketSegment.UO_PeoTkt.COD2;
                            _TicketSegment.UO_PeoTkt.COD3 = _frTicketSegment.UO_PeoTkt.COD3;
                            _TicketSegment.UO_PeoTkt.Commission = _frTicketSegment.UO_PeoTkt.Commission;
                            _TicketSegment.UO_PeoTkt.LocalFareCurr = _frTicketSegment.UO_PeoTkt.LocalFareCurr;
                            _TicketSegment.UO_PeoTkt.LocalFareAmt = _frTicketSegment.UO_PeoTkt.LocalFareAmt;
                            _TicketSegment.UO_PeoTkt.ForeignFareCurr = _frTicketSegment.UO_PeoTkt.ForeignFareCurr;
                            _TicketSegment.UO_PeoTkt.ForeignFareAmt = _frTicketSegment.UO_PeoTkt.ForeignFareAmt;
                            _TicketSegment.UO_PeoTkt.Discount = _frTicketSegment.UO_PeoTkt.Discount;
                            _TicketSegment.UO_PeoTkt.FormPay = _frTicketSegment.UO_PeoTkt.FormPay;
                            _TicketSegment.UO_PeoTkt.Routing = _frTicketSegment.UO_PeoTkt.Routing;
                            _TicketSegment.UO_PeoTkt.FullCurr = _frTicketSegment.UO_PeoTkt.FullCurr;
                            _TicketSegment.UO_PeoTkt.FullFare = _frTicketSegment.UO_PeoTkt.FullFare;
                            _TicketSegment.UO_PeoTkt.PaidCurr = _frTicketSegment.UO_PeoTkt.PaidCurr;
                            _TicketSegment.UO_PeoTkt.PaidFare = _frTicketSegment.UO_PeoTkt.PaidFare;
                            _TicketSegment.UO_PeoTkt.LowCurr = _frTicketSegment.UO_PeoTkt.LowCurr;
                            _TicketSegment.UO_PeoTkt.LowFare = _frTicketSegment.UO_PeoTkt.LowFare;
                            _TicketSegment.UO_PeoTkt.NetCurr = _frTicketSegment.UO_PeoTkt.NetCurr;
                            _TicketSegment.UO_PeoTkt.NetFare = _frTicketSegment.UO_PeoTkt.NetFare;
                            _TicketSegment.UO_PeoTkt.FareBasis = _frTicketSegment.UO_PeoTkt.FareBasis;
                            _TicketSegment.UO_PeoTkt.FareType = _frTicketSegment.UO_PeoTkt.FareType;
                            _TicketSegment.UO_PeoTkt.TotalTax = _frTicketSegment.UO_PeoTkt.TotalTax;
                            _TicketSegment.UO_PeoTkt.Security = _frTicketSegment.UO_PeoTkt.Security;
                            _TicketSegment.UO_PeoTkt.QSurcharge = _frTicketSegment.UO_PeoTkt.QSurcharge;
                            _TicketSegment.UO_PeoTkt.BranchCode = _frTicketSegment.UO_PeoTkt.BranchCode;
                            _TicketSegment.UO_PeoTkt.TeamCode = _frTicketSegment.UO_PeoTkt.TeamCode;
                            _TicketSegment.UO_PeoTkt.IssueOn = _frTicketSegment.UO_PeoTkt.IssueOn;
                            _TicketSegment.UO_PeoTkt.IssueBy = _frTicketSegment.UO_PeoTkt.IssueBy;
                            _TicketSegment.UO_PeoTkt.CreateOn = _frTicketSegment.UO_PeoTkt.CreateOn;
                            _TicketSegment.UO_PeoTkt.CreateBy = _frTicketSegment.UO_PeoTkt.CreateBy;
                            _TicketSegment.UO_PeoTkt.UpdateOn = _frTicketSegment.UO_PeoTkt.UpdateOn;
                            _TicketSegment.UO_PeoTkt.UpdateBy = _frTicketSegment.UO_PeoTkt.UpdateBy;
                            _TicketSegment.UO_PeoTkt.VoidOn = _frTicketSegment.UO_PeoTkt.VoidOn;
                            _TicketSegment.UO_PeoTkt.VoidBy = _frTicketSegment.UO_PeoTkt.VoidBy;
                            _TicketSegment.UO_PeoTkt.voidTeam = _frTicketSegment.UO_PeoTkt.voidTeam;
                            _TicketSegment.UO_PeoTkt.voidreason = _frTicketSegment.UO_PeoTkt.voidreason;
                            _TicketSegment.UO_PeoTkt.TaxAmount1 = _frTicketSegment.UO_PeoTkt.TaxAmount1;
                            _TicketSegment.UO_PeoTkt.TaxID1 = _frTicketSegment.UO_PeoTkt.TaxID1;
                            _TicketSegment.UO_PeoTkt.TaxAmount2 = _frTicketSegment.UO_PeoTkt.TaxAmount2;
                            _TicketSegment.UO_PeoTkt.TaxID2 = _frTicketSegment.UO_PeoTkt.TaxID2;
                            _TicketSegment.UO_PeoTkt.TaxAmount3 = _frTicketSegment.UO_PeoTkt.TaxAmount3;
                            _TicketSegment.UO_PeoTkt.TaxID3 = _frTicketSegment.UO_PeoTkt.TaxID3;
                            _TicketSegment.UO_PeoTkt.FareCalType = _frTicketSegment.UO_PeoTkt.FareCalType;
                            _TicketSegment.UO_PeoTkt.CompressPrintInd = _frTicketSegment.UO_PeoTkt.CompressPrintInd;

                        }

                        _TicketSegment.UOList_PeoTktDetail = new List<Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoTktDetail>();
                        if (_frTicketSegment.UOList_PeoTktDetail != null)
                        {
                            foreach (Westminster.MOE.WebService.eMosBookingService.UO_PeoTktDetail _frUO_PeoTktDetail in _frTicketSegment.UOList_PeoTktDetail)
                            {
                                Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoTktDetail _UO_PeoTktDetail = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoTktDetail();
                                _UO_PeoTktDetail.CompanyCode = _frUO_PeoTktDetail.CompanyCode;
                                _UO_PeoTktDetail.Ticket = _frUO_PeoTktDetail.Ticket;
                                _UO_PeoTktDetail.AirCode = _frUO_PeoTktDetail.AirCode;
                                _UO_PeoTktDetail.TktSeq = _frUO_PeoTktDetail.TktSeq;
                                _UO_PeoTktDetail.SegNum = _frUO_PeoTktDetail.SegNum;
                                _UO_PeoTktDetail.Airline = _frUO_PeoTktDetail.Airline;
                                _UO_PeoTktDetail.Flight = _frUO_PeoTktDetail.Flight;
                                _UO_PeoTktDetail.Class = _frUO_PeoTktDetail.Class;
                                _UO_PeoTktDetail.Seat = _frUO_PeoTktDetail.Seat;
                                _UO_PeoTktDetail.DepartCity = _frUO_PeoTktDetail.DepartCity;
                                _UO_PeoTktDetail.ArrivalCity = _frUO_PeoTktDetail.ArrivalCity;
                                _UO_PeoTktDetail.DepartDATE = _frUO_PeoTktDetail.DepartDATE;
                                _UO_PeoTktDetail.DepartTime = _frUO_PeoTktDetail.DepartTime;
                                _UO_PeoTktDetail.ArrDATE = _frUO_PeoTktDetail.ArrDATE;
                                _UO_PeoTktDetail.ArrTime = _frUO_PeoTktDetail.ArrTime;
                                _UO_PeoTktDetail.ElapsedTime = _frUO_PeoTktDetail.ElapsedTime;
                                _UO_PeoTktDetail.StopOverNum = _frUO_PeoTktDetail.StopOverNum;
                                _UO_PeoTktDetail.StopOverCity = _frUO_PeoTktDetail.StopOverCity;
                                _UO_PeoTktDetail.FareAmt = _frUO_PeoTktDetail.FareAmt;
                                _UO_PeoTktDetail.FareBasic = _frUO_PeoTktDetail.FareBasic;
                                _UO_PeoTktDetail.Commission = _frUO_PeoTktDetail.Commission;
                                _UO_PeoTktDetail.ValidStart = _frUO_PeoTktDetail.ValidStart;
                                _UO_PeoTktDetail.ValidEnd = _frUO_PeoTktDetail.ValidEnd;
                                _UO_PeoTktDetail.CreateOn = _frUO_PeoTktDetail.CreateOn;
                                _UO_PeoTktDetail.CreateBy = _frUO_PeoTktDetail.CreateBy;
                                _UO_PeoTktDetail.UpdateOn = _frUO_PeoTktDetail.UpdateOn;
                                _UO_PeoTktDetail.UpdateBy = _frUO_PeoTktDetail.UpdateBy;
                                _UO_PeoTktDetail.EQP = _frUO_PeoTktDetail.EQP;
                                _UO_PeoTktDetail.Service = _frUO_PeoTktDetail.Service;
                                _UO_PeoTktDetail.status = _frUO_PeoTktDetail.status;
                                _UO_PeoTktDetail.fare_basis_code = _frUO_PeoTktDetail.fare_basis_code;
                                _UO_PeoTktDetail.DepartTerm = _frUO_PeoTktDetail.DepartTerm;
                                _UO_PeoTktDetail.ArrivalTerm = _frUO_PeoTktDetail.ArrivalTerm;
                                _UO_PeoTktDetail.freq_fight_num = _frUO_PeoTktDetail.freq_fight_num;
                                _UO_PeoTktDetail.AirlinePNR = _frUO_PeoTktDetail.AirlinePNR;

                                _TicketSegment.UOList_PeoTktDetail.Add(_UO_PeoTktDetail);
                            }
                        }

                        moeObj.TicketSegmentsList.Add(_TicketSegment);
                    }
                }

                moeObj.PassengersList = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoPaxs();
                if (emosObj.PassengersList != null)
                {
                    foreach (Westminster.MOE.WebService.eMosBookingService.UO_PeoPax _frUO_PeoPax in emosObj.PassengersList)
                    {
                        Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoPax _UO_PeoPax = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoPax();
                        _UO_PeoPax.BKGREF = _frUO_PeoPax.BKGREF;
                        _UO_PeoPax.COMPANYCODE = _frUO_PeoPax.COMPANYCODE;
                        _UO_PeoPax.SEGNUM = _frUO_PeoPax.SEGNUM;
                        _UO_PeoPax.PAXNUM = _frUO_PeoPax.PAXNUM;
                        _UO_PeoPax.PAXLNAME = _frUO_PeoPax.PAXLNAME;
                        _UO_PeoPax.PAXFNAME = _frUO_PeoPax.PAXFNAME;
                        _UO_PeoPax.PAXTITLE = _frUO_PeoPax.PAXTITLE;
                        _UO_PeoPax.PAXTYPE = _frUO_PeoPax.PAXTYPE;
                        _UO_PeoPax.PAXAIR = _frUO_PeoPax.PAXAIR;
                        _UO_PeoPax.PAXHOTEL = _frUO_PeoPax.PAXHOTEL;
                        _UO_PeoPax.PAXOTHER = _frUO_PeoPax.PAXOTHER;
                        _UO_PeoPax.PAXAGE = _frUO_PeoPax.PAXAGE;
                        _UO_PeoPax.RMK1 = _frUO_PeoPax.RMK1;
                        _UO_PeoPax.RMK2 = _frUO_PeoPax.RMK2;
                        _UO_PeoPax.VOIDON = _frUO_PeoPax.VOIDON;
                        _UO_PeoPax.VOIDBY = _frUO_PeoPax.VOIDBY;
                        _UO_PeoPax.SOURCESYSTEM = _frUO_PeoPax.SOURCESYSTEM;
                        _UO_PeoPax.CREATEON = _frUO_PeoPax.CREATEON;
                        _UO_PeoPax.CREATEBY = _frUO_PeoPax.CREATEBY;
                        _UO_PeoPax.UPDATEON = _frUO_PeoPax.UPDATEON;
                        _UO_PeoPax.UPDATEBY = _frUO_PeoPax.UPDATEBY;

                        moeObj.PassengersList.Add(_UO_PeoPax);
                    }
                }

                if (emosObj.Customer != null)
                {
                    moeObj.Customer = new Westminster.MOE.Entity.BLLEntity.Transfer.UO_CUSTOMER();
                    moeObj.Customer.COMPANYCODE = emosObj.Customer.COMPANYCODE;
                    moeObj.Customer.CLTCODE = emosObj.Customer.CLTCODE;
                    moeObj.Customer.CLTNAME = emosObj.Customer.CLTNAME;
                    moeObj.Customer.BRANCHCODE = emosObj.Customer.BRANCHCODE;
                    moeObj.Customer.TEAMCODE = emosObj.Customer.TEAMCODE;
                    moeObj.Customer.MSTRCLT = emosObj.Customer.MSTRCLT;
                    moeObj.Customer.ADDR1 = emosObj.Customer.ADDR1;
                    moeObj.Customer.ADDR2 = emosObj.Customer.ADDR2;
                    moeObj.Customer.ADDR3 = emosObj.Customer.ADDR3;
                    moeObj.Customer.ADDR4 = emosObj.Customer.ADDR4;
                    moeObj.Customer.ADDR5 = emosObj.Customer.ADDR5;
                    moeObj.Customer.POSTAL = emosObj.Customer.POSTAL;
                    moeObj.Customer.CITY = emosObj.Customer.CITY;
                    moeObj.Customer.COUNTRY = emosObj.Customer.COUNTRY;
                    moeObj.Customer.BILLADDR1 = emosObj.Customer.BILLADDR1;
                    moeObj.Customer.BILLADDR2 = emosObj.Customer.BILLADDR2;
                    moeObj.Customer.BILLADDR3 = emosObj.Customer.BILLADDR3;
                    moeObj.Customer.BILLADDR4 = emosObj.Customer.BILLADDR4;
                    moeObj.Customer.BILLADDR5 = emosObj.Customer.BILLADDR5;
                    moeObj.Customer.BILLPOSTAL = emosObj.Customer.BILLPOSTAL;
                    moeObj.Customer.BILLCITY = emosObj.Customer.BILLCITY;
                    moeObj.Customer.BILLCOUNTRY = emosObj.Customer.BILLCOUNTRY;
                    moeObj.Customer.STATUS = emosObj.Customer.STATUS;
                    moeObj.Customer.CONTACT1 = emosObj.Customer.CONTACT1;
                    moeObj.Customer.CONTACT2 = emosObj.Customer.CONTACT2;
                    moeObj.Customer.JOBTITLE1 = emosObj.Customer.JOBTITLE1;
                    moeObj.Customer.JOBTITLE2 = emosObj.Customer.JOBTITLE2;
                    moeObj.Customer.PHONE1 = emosObj.Customer.PHONE1;
                    moeObj.Customer.FAX1 = emosObj.Customer.FAX1;
                    moeObj.Customer.CRTERMS = emosObj.Customer.CRTERMS;
                    moeObj.Customer.PRFTUP = emosObj.Customer.PRFTUP;
                    moeObj.Customer.PRFTDOWN = emosObj.Customer.PRFTDOWN;
                    moeObj.Customer.CURRENCY = emosObj.Customer.CURRENCY;
                    moeObj.Customer.PFDAY = emosObj.Customer.PFDAY;
                    moeObj.Customer.CREATEON = emosObj.Customer.CREATEON;
                    moeObj.Customer.CREATEBY = emosObj.Customer.CREATEBY;
                    moeObj.Customer.UPDATEON = emosObj.Customer.UPDATEON;
                    moeObj.Customer.UPDATEBY = emosObj.Customer.UPDATEBY;
                    moeObj.Customer.email = emosObj.Customer.email;
                    moeObj.Customer.CustRemarks = emosObj.Customer.CustRemarks;
                    moeObj.Customer.GEMSCODE = emosObj.Customer.GEMSCODE;
                    moeObj.Customer.CLTNAME1 = emosObj.Customer.CLTNAME1;
                    //moeObj.Customer.ADDR1_1 = emosObj.Customer.ADDR1_1;
                    //moeObj.Customer.ADDR1_2 = emosObj.Customer.ADDR1_2;
                    //moeObj.Customer.ADDR1_3 = emosObj.Customer.ADDR1_3;
                    //moeObj.Customer.ADDR1_4 = emosObj.Customer.ADDR1_4;
                    //moeObj.Customer.ADDR1_5 = emosObj.Customer.ADDR1_5;

                }
                moeObj.Message = (Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingMessages)Enum.Parse(typeof(Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingMessages), emosObj.Message.ToString());
                moeObj.Status = emosObj.Status;
                moeObj.Exception = emosObj.Exception;
                moeObj.CompanyCode = emosObj.CompanyCode;
                moeObj.PNR = emosObj.PNR;
                moeObj.Bkgref = emosObj.Bkgref;
                moeObj.SourceSystem = emosObj.SourceSystem;
                moeObj.InvType = (Westminster.MOE.Entity.DALEntity.Transfer.Invoice_Types)Enum.Parse(typeof(Westminster.MOE.Entity.DALEntity.Transfer.Invoice_Types), emosObj.InvType.ToString());
            }
            return moeObj;
        }

        public Westminster.MOE.WebService.eMosBookingService.RetrieveIURBooking ToeMosObj(Westminster.MOE.Entity.BLLEntity.Transfer.RetrieveIURBooking moeObj)
        {
            Westminster.MOE.WebService.eMosBookingService.RetrieveIURBooking emosObj = new Westminster.MOE.WebService.eMosBookingService.RetrieveIURBooking();
            if (moeObj != null)
            {

                if (moeObj.Peomstr != null)
                {
                    emosObj.Peomstr = new Westminster.MOE.WebService.eMosBookingService.UO_PeoMstr();
                    emosObj.Peomstr.COMPANYCODE = moeObj.Peomstr.COMPANYCODE;
                    emosObj.Peomstr.BKGREF = moeObj.Peomstr.BKGREF;
                    emosObj.Peomstr.agency_arc_iata_num = moeObj.Peomstr.agency_arc_iata_num;
                    emosObj.Peomstr.iss_off_code = moeObj.Peomstr.iss_off_code;
                    emosObj.Peomstr.tranflag = moeObj.Peomstr.tranflag;
                    emosObj.Peomstr.PNR = moeObj.Peomstr.PNR;
                    emosObj.Peomstr.CRSSINEIN = moeObj.Peomstr.CRSSINEIN;
                    emosObj.Peomstr.COD1 = moeObj.Peomstr.COD1;
                    emosObj.Peomstr.COD2 = moeObj.Peomstr.COD2;
                    emosObj.Peomstr.CLTODE = moeObj.Peomstr.CLTODE;
                    emosObj.Peomstr.CLTNAME = moeObj.Peomstr.CLTNAME;
                    emosObj.Peomstr.CLTADDR = moeObj.Peomstr.CLTADDR;
                    emosObj.Peomstr.STAFFCODE = moeObj.Peomstr.STAFFCODE;
                    emosObj.Peomstr.TEAMCODE = moeObj.Peomstr.TEAMCODE;
                    emosObj.Peomstr.FIRSTPAX = moeObj.Peomstr.FIRSTPAX;
                    emosObj.Peomstr.PHONE = moeObj.Peomstr.PHONE;
                    emosObj.Peomstr.FAX = moeObj.Peomstr.FAX;
                    emosObj.Peomstr.EMAIL = moeObj.Peomstr.EMAIL;
                    emosObj.Peomstr.MASTERPNR = moeObj.Peomstr.MASTERPNR;
                    emosObj.Peomstr.TOURCODE = moeObj.Peomstr.TOURCODE;
                    emosObj.Peomstr.CONTACTPERSON = moeObj.Peomstr.CONTACTPERSON;
                    emosObj.Peomstr.DEPARTDATE = moeObj.Peomstr.DEPARTDATE;
                    emosObj.Peomstr.TTLSELLCURR = moeObj.Peomstr.TTLSELLCURR;
                    emosObj.Peomstr.TTLSELLAMT = moeObj.Peomstr.TTLSELLAMT;
                    emosObj.Peomstr.TTLSELLTAXCURR = moeObj.Peomstr.TTLSELLTAXCURR;
                    emosObj.Peomstr.TTLSELLTAX = moeObj.Peomstr.TTLSELLTAX;
                    emosObj.Peomstr.TTLCOSTCURR = moeObj.Peomstr.TTLCOSTCURR;
                    emosObj.Peomstr.TTLCOSTAMT = moeObj.Peomstr.TTLCOSTAMT;
                    emosObj.Peomstr.TTLCOSTTAXCURR = moeObj.Peomstr.TTLCOSTTAXCURR;
                    emosObj.Peomstr.TTLCOSTTAX = moeObj.Peomstr.TTLCOSTTAX;
                    emosObj.Peomstr.INVCURR = moeObj.Peomstr.INVCURR;
                    emosObj.Peomstr.INVAMT = moeObj.Peomstr.INVAMT;
                    emosObj.Peomstr.INVTAXCURR = moeObj.Peomstr.INVTAXCURR;
                    emosObj.Peomstr.INVTAX = moeObj.Peomstr.INVTAX;
                    emosObj.Peomstr.DOCCURR = moeObj.Peomstr.DOCCURR;
                    emosObj.Peomstr.DOCAMT = moeObj.Peomstr.DOCAMT;
                    emosObj.Peomstr.DOCTAXCURR = moeObj.Peomstr.DOCTAXCURR;
                    emosObj.Peomstr.DOCTAX = moeObj.Peomstr.DOCTAX;
                    emosObj.Peomstr.INVCOUNT = moeObj.Peomstr.INVCOUNT;
                    emosObj.Peomstr.DOCCOUNT = moeObj.Peomstr.DOCCOUNT;
                    emosObj.Peomstr.RMK1 = moeObj.Peomstr.RMK1;
                    emosObj.Peomstr.RMK2 = moeObj.Peomstr.RMK2;
                    emosObj.Peomstr.RMK3 = moeObj.Peomstr.RMK3;
                    emosObj.Peomstr.RMK4 = moeObj.Peomstr.RMK4;
                    emosObj.Peomstr.DEADLINE = moeObj.Peomstr.DEADLINE;
                    emosObj.Peomstr.SOURCESYSTEM = moeObj.Peomstr.SOURCESYSTEM;
                    emosObj.Peomstr.SOURCE = moeObj.Peomstr.SOURCE;
                    emosObj.Peomstr.CREATEON = moeObj.Peomstr.CREATEON;
                    emosObj.Peomstr.CREATEBY = moeObj.Peomstr.CREATEBY;
                    emosObj.Peomstr.UPDATEON = moeObj.Peomstr.UPDATEON;
                    emosObj.Peomstr.UPDATEBY = moeObj.Peomstr.UPDATEBY;
                    emosObj.Peomstr.COD3 = moeObj.Peomstr.COD3;
                    emosObj.Peomstr.INSFLAG = moeObj.Peomstr.INSFLAG;
                    emosObj.Peomstr.TOEMOSON = moeObj.Peomstr.TOEMOSON;
                    emosObj.Peomstr.TOEMOSBY = moeObj.Peomstr.TOEMOSBY;
                    emosObj.Peomstr.EMOSBKGREF = moeObj.Peomstr.EMOSBKGREF;
                    emosObj.Peomstr.IURFLAG = moeObj.Peomstr.IURFLAG;
                    emosObj.Peomstr.pseudo = moeObj.Peomstr.pseudo;
                    emosObj.Peomstr.pseudo_inv = moeObj.Peomstr.pseudo_inv;
                    emosObj.Peomstr.CRSSINEIN_inv = moeObj.Peomstr.CRSSINEIN_inv;

                }

                List<Westminster.MOE.WebService.eMosBookingService.AirlineSegment> emosObj_AirlineSegmentsList = new List<Westminster.MOE.WebService.eMosBookingService.AirlineSegment>();
                if (moeObj.AirlineSegmentsList != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.AirlineSegment _frAirlineSegment in moeObj.AirlineSegmentsList)
                    {
                        Westminster.MOE.WebService.eMosBookingService.AirlineSegment _AirlineSegment = new Westminster.MOE.WebService.eMosBookingService.AirlineSegment();

                        if (_frAirlineSegment.UO_PEOAIR != null)
                        {
                            _AirlineSegment.UO_PEOAIR = new Westminster.MOE.WebService.eMosBookingService.UO_PEOAIR();
                            _AirlineSegment.UO_PEOAIR.CompanyCode = _frAirlineSegment.UO_PEOAIR.CompanyCode;
                            _AirlineSegment.UO_PEOAIR.BkgRef = _frAirlineSegment.UO_PEOAIR.BkgRef;
                            _AirlineSegment.UO_PEOAIR.SegNum = _frAirlineSegment.UO_PEOAIR.SegNum;
                            _AirlineSegment.UO_PEOAIR.PNR = _frAirlineSegment.UO_PEOAIR.PNR;
                            _AirlineSegment.UO_PEOAIR.Flight = _frAirlineSegment.UO_PEOAIR.Flight;
                            _AirlineSegment.UO_PEOAIR.Class = _frAirlineSegment.UO_PEOAIR.Class;
                            _AirlineSegment.UO_PEOAIR.DepartCity = _frAirlineSegment.UO_PEOAIR.DepartCity;
                            _AirlineSegment.UO_PEOAIR.ArrivalCity = _frAirlineSegment.UO_PEOAIR.ArrivalCity;
                            _AirlineSegment.UO_PEOAIR.Status = _frAirlineSegment.UO_PEOAIR.Status;
                            _AirlineSegment.UO_PEOAIR.NumOfPax = _frAirlineSegment.UO_PEOAIR.NumOfPax;
                            _AirlineSegment.UO_PEOAIR.DepartDATE = _frAirlineSegment.UO_PEOAIR.DepartDATE;
                            _AirlineSegment.UO_PEOAIR.DepartTime = _frAirlineSegment.UO_PEOAIR.DepartTime;
                            _AirlineSegment.UO_PEOAIR.ArrDATE = _frAirlineSegment.UO_PEOAIR.ArrDATE;
                            _AirlineSegment.UO_PEOAIR.ArrTime = _frAirlineSegment.UO_PEOAIR.ArrTime;
                            _AirlineSegment.UO_PEOAIR.ElapsedTime = _frAirlineSegment.UO_PEOAIR.ElapsedTime;
                            _AirlineSegment.UO_PEOAIR.SSR = _frAirlineSegment.UO_PEOAIR.SSR;
                            _AirlineSegment.UO_PEOAIR.Seat = _frAirlineSegment.UO_PEOAIR.Seat;
                            _AirlineSegment.UO_PEOAIR.ReasonCode = _frAirlineSegment.UO_PEOAIR.ReasonCode;
                            _AirlineSegment.UO_PEOAIR.StopOverNum = _frAirlineSegment.UO_PEOAIR.StopOverNum;
                            _AirlineSegment.UO_PEOAIR.StopOverCity = _frAirlineSegment.UO_PEOAIR.StopOverCity;
                            _AirlineSegment.UO_PEOAIR.Pcode = _frAirlineSegment.UO_PEOAIR.Pcode;
                            _AirlineSegment.UO_PEOAIR.SpecialRQ = _frAirlineSegment.UO_PEOAIR.SpecialRQ;
                            _AirlineSegment.UO_PEOAIR.RMK1 = _frAirlineSegment.UO_PEOAIR.RMK1;
                            _AirlineSegment.UO_PEOAIR.RMK2 = _frAirlineSegment.UO_PEOAIR.RMK2;
                            _AirlineSegment.UO_PEOAIR.INVRMK1 = _frAirlineSegment.UO_PEOAIR.INVRMK1;
                            _AirlineSegment.UO_PEOAIR.INVRMK2 = _frAirlineSegment.UO_PEOAIR.INVRMK2;
                            _AirlineSegment.UO_PEOAIR.VCHRMK1 = _frAirlineSegment.UO_PEOAIR.VCHRMK1;
                            _AirlineSegment.UO_PEOAIR.VCHRMK2 = _frAirlineSegment.UO_PEOAIR.VCHRMK2;
                            _AirlineSegment.UO_PEOAIR.SELLCURR = _frAirlineSegment.UO_PEOAIR.SELLCURR;
                            _AirlineSegment.UO_PEOAIR.TOTALSELL = _frAirlineSegment.UO_PEOAIR.TOTALSELL;
                            _AirlineSegment.UO_PEOAIR.TOTALSTAX = _frAirlineSegment.UO_PEOAIR.TOTALSTAX;
                            _AirlineSegment.UO_PEOAIR.COSTCURR = _frAirlineSegment.UO_PEOAIR.COSTCURR;
                            _AirlineSegment.UO_PEOAIR.TOTALCOST = _frAirlineSegment.UO_PEOAIR.TOTALCOST;
                            _AirlineSegment.UO_PEOAIR.TOTALCTAX = _frAirlineSegment.UO_PEOAIR.TOTALCTAX;
                            _AirlineSegment.UO_PEOAIR.SourceSystem = _frAirlineSegment.UO_PEOAIR.SourceSystem;
                            _AirlineSegment.UO_PEOAIR.CreateOn = _frAirlineSegment.UO_PEOAIR.CreateOn;
                            _AirlineSegment.UO_PEOAIR.CreateBy = _frAirlineSegment.UO_PEOAIR.CreateBy;
                            _AirlineSegment.UO_PEOAIR.UpdateOn = _frAirlineSegment.UO_PEOAIR.UpdateOn;
                            _AirlineSegment.UO_PEOAIR.UpdateBy = _frAirlineSegment.UO_PEOAIR.UpdateBy;
                            _AirlineSegment.UO_PEOAIR.VoidOn = _frAirlineSegment.UO_PEOAIR.VoidOn;
                            _AirlineSegment.UO_PEOAIR.VoidBy = _frAirlineSegment.UO_PEOAIR.VoidBy;
                            _AirlineSegment.UO_PEOAIR.AirSeg = _frAirlineSegment.UO_PEOAIR.AirSeg;
                            _AirlineSegment.UO_PEOAIR.DepartTerminal = _frAirlineSegment.UO_PEOAIR.DepartTerminal;
                            _AirlineSegment.UO_PEOAIR.ArrivalTerminal = _frAirlineSegment.UO_PEOAIR.ArrivalTerminal;
                            _AirlineSegment.UO_PEOAIR.OtherAirlinePNR = _frAirlineSegment.UO_PEOAIR.OtherAirlinePNR;
                            _AirlineSegment.UO_PEOAIR.EQP = _frAirlineSegment.UO_PEOAIR.EQP;
                            _AirlineSegment.UO_PEOAIR.Service = _frAirlineSegment.UO_PEOAIR.Service;
                            _AirlineSegment.UO_PEOAIR.Suppcode = _frAirlineSegment.UO_PEOAIR.Suppcode;

                        }

                        List<Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL> _AirlineSegment_UOList_PEOAIRDETAIL = new List<Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL>();
                        if (_frAirlineSegment.UOList_PEOAIRDETAIL != null)
                        {
                            foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIRDETAIL _frUO_PEOAIRDETAIL in _frAirlineSegment.UOList_PEOAIRDETAIL)
                            {
                                Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL _UO_PEOAIRDETAIL = new Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL();
                                _UO_PEOAIRDETAIL.CompanyCode = _frUO_PEOAIRDETAIL.CompanyCode;
                                _UO_PEOAIRDETAIL.BkgRef = _frUO_PEOAIRDETAIL.BkgRef;
                                _UO_PEOAIRDETAIL.SegNum = _frUO_PEOAIRDETAIL.SegNum;
                                _UO_PEOAIRDETAIL.SEQNUM = _frUO_PEOAIRDETAIL.SEQNUM;
                                _UO_PEOAIRDETAIL.SEQTYPE = _frUO_PEOAIRDETAIL.SEQTYPE;
                                _UO_PEOAIRDETAIL.QTY = _frUO_PEOAIRDETAIL.QTY;
                                _UO_PEOAIRDETAIL.CURR = _frUO_PEOAIRDETAIL.CURR;
                                _UO_PEOAIRDETAIL.AMT = _frUO_PEOAIRDETAIL.AMT;
                                _UO_PEOAIRDETAIL.TAXCURR = _frUO_PEOAIRDETAIL.TAXCURR;
                                _UO_PEOAIRDETAIL.TAXAMT = _frUO_PEOAIRDETAIL.TAXAMT;
                                _UO_PEOAIRDETAIL.CREATEON = _frUO_PEOAIRDETAIL.CREATEON;
                                _UO_PEOAIRDETAIL.CREATEBY = _frUO_PEOAIRDETAIL.CREATEBY;
                                _UO_PEOAIRDETAIL.UPDATEON = _frUO_PEOAIRDETAIL.UPDATEON;
                                _UO_PEOAIRDETAIL.UPDATEBY = _frUO_PEOAIRDETAIL.UPDATEBY;
                                _UO_PEOAIRDETAIL.AGETYPE = _frUO_PEOAIRDETAIL.AGETYPE;

                                _AirlineSegment_UOList_PEOAIRDETAIL.Add(_UO_PEOAIRDETAIL);
                                _AirlineSegment.UOList_PEOAIRDETAIL = _AirlineSegment_UOList_PEOAIRDETAIL.ToArray();
                            }
                        }
                        emosObj_AirlineSegmentsList.Add(_AirlineSegment);
                        emosObj.AirlineSegmentsList = emosObj_AirlineSegmentsList.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.HotelSegment> emosObj_HotelSegmentsList = new List<Westminster.MOE.WebService.eMosBookingService.HotelSegment>();
                if (moeObj.HotelSegmentsList != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.HotelSegment _frHotelSegment in moeObj.HotelSegmentsList)
                    {
                        Westminster.MOE.WebService.eMosBookingService.HotelSegment _HotelSegment = new Westminster.MOE.WebService.eMosBookingService.HotelSegment();

                        if (_frHotelSegment.PEOHTL != null)
                        {
                            _HotelSegment.PEOHTL = new Westminster.MOE.WebService.eMosBookingService.UO_PEOHTL();
                            _HotelSegment.PEOHTL.BKGREF = _frHotelSegment.PEOHTL.BKGREF;
                            _HotelSegment.PEOHTL.COMPANYCODE = _frHotelSegment.PEOHTL.COMPANYCODE;
                            _HotelSegment.PEOHTL.SEGNUM = _frHotelSegment.PEOHTL.SEGNUM;
                            _HotelSegment.PEOHTL.COUNTRYCODE = _frHotelSegment.PEOHTL.COUNTRYCODE;
                            _HotelSegment.PEOHTL.CITYCODE = _frHotelSegment.PEOHTL.CITYCODE;
                            _HotelSegment.PEOHTL.ADCOMTXNO = _frHotelSegment.PEOHTL.ADCOMTXNO;
                            _HotelSegment.PEOHTL.ADCOMSEQNUM = _frHotelSegment.PEOHTL.ADCOMSEQNUM;
                            _HotelSegment.PEOHTL.HOTELCODE = _frHotelSegment.PEOHTL.HOTELCODE;
                            _HotelSegment.PEOHTL.ARRDATE = _frHotelSegment.PEOHTL.ARRDATE;
                            _HotelSegment.PEOHTL.ARRTIME = _frHotelSegment.PEOHTL.ARRTIME;
                            _HotelSegment.PEOHTL.DEPARTDATE = _frHotelSegment.PEOHTL.DEPARTDATE;
                            _HotelSegment.PEOHTL.DEPARTTIME = _frHotelSegment.PEOHTL.DEPARTTIME;
                            _HotelSegment.PEOHTL.ETA = _frHotelSegment.PEOHTL.ETA;
                            _HotelSegment.PEOHTL.ETD = _frHotelSegment.PEOHTL.ETD;
                            _HotelSegment.PEOHTL.SELLCURR = _frHotelSegment.PEOHTL.SELLCURR;
                            _HotelSegment.PEOHTL.COSTCURR = _frHotelSegment.PEOHTL.COSTCURR;
                            _HotelSegment.PEOHTL.TOTALCOST = _frHotelSegment.PEOHTL.TOTALCOST;
                            _HotelSegment.PEOHTL.TOTALSELL = _frHotelSegment.PEOHTL.TOTALSELL;
                            _HotelSegment.PEOHTL.TOTALCTAX = _frHotelSegment.PEOHTL.TOTALCTAX;
                            _HotelSegment.PEOHTL.TOTALSTAX = _frHotelSegment.PEOHTL.TOTALSTAX;
                            _HotelSegment.PEOHTL.FORMPAY = _frHotelSegment.PEOHTL.FORMPAY;
                            _HotelSegment.PEOHTL.RMK1 = _frHotelSegment.PEOHTL.RMK1;
                            _HotelSegment.PEOHTL.RMK2 = _frHotelSegment.PEOHTL.RMK2;
                            _HotelSegment.PEOHTL.INVRMK1 = _frHotelSegment.PEOHTL.INVRMK1;
                            _HotelSegment.PEOHTL.INVRMK2 = _frHotelSegment.PEOHTL.INVRMK2;
                            _HotelSegment.PEOHTL.VCHRMK1 = _frHotelSegment.PEOHTL.VCHRMK1;
                            _HotelSegment.PEOHTL.VCHRMK2 = _frHotelSegment.PEOHTL.VCHRMK2;
                            _HotelSegment.PEOHTL.WESTELNO = _frHotelSegment.PEOHTL.WESTELNO;
                            _HotelSegment.PEOHTL.SOURCESYSTEM = _frHotelSegment.PEOHTL.SOURCESYSTEM;
                            _HotelSegment.PEOHTL.SPCL_REQUEST = _frHotelSegment.PEOHTL.SPCL_REQUEST;
                            _HotelSegment.PEOHTL.HTLREFERENCE = _frHotelSegment.PEOHTL.HTLREFERENCE;
                            _HotelSegment.PEOHTL.HTLCFmby = _frHotelSegment.PEOHTL.HTLCFmby;
                            _HotelSegment.PEOHTL.VOIDBY = _frHotelSegment.PEOHTL.VOIDBY;
                            _HotelSegment.PEOHTL.VOIDON = _frHotelSegment.PEOHTL.VOIDON;
                            _HotelSegment.PEOHTL.CREATEBY = _frHotelSegment.PEOHTL.CREATEBY;
                            _HotelSegment.PEOHTL.CREATEON = _frHotelSegment.PEOHTL.CREATEON;
                            _HotelSegment.PEOHTL.UPDATEBY = _frHotelSegment.PEOHTL.UPDATEBY;
                            _HotelSegment.PEOHTL.UPDATEON = _frHotelSegment.PEOHTL.UPDATEON;
                            _HotelSegment.PEOHTL.SOURCEREF = _frHotelSegment.PEOHTL.SOURCEREF;
                            _HotelSegment.PEOHTL.GSTTAX = _frHotelSegment.PEOHTL.GSTTAX;
                            _HotelSegment.PEOHTL.showDetailFee = _frHotelSegment.PEOHTL.showDetailFee;
                            _HotelSegment.PEOHTL.SuppCode = _frHotelSegment.PEOHTL.SuppCode;
                            _HotelSegment.PEOHTL.hotelVoucherType = _frHotelSegment.PEOHTL.hotelVoucherType;
                            _HotelSegment.PEOHTL.COD1 = _frHotelSegment.PEOHTL.COD1;
                            _HotelSegment.PEOHTL.COD2 = _frHotelSegment.PEOHTL.COD2;
                            _HotelSegment.PEOHTL.COD3 = _frHotelSegment.PEOHTL.COD3;
                            _HotelSegment.PEOHTL.roomTypeCode = _frHotelSegment.PEOHTL.roomTypeCode;
                            _HotelSegment.PEOHTL.fullRoomRate = _frHotelSegment.PEOHTL.fullRoomRate;
                            _HotelSegment.PEOHTL.dailyRate = _frHotelSegment.PEOHTL.dailyRate;
                            _HotelSegment.PEOHTL.lowRoomRate = _frHotelSegment.PEOHTL.lowRoomRate;
                            _HotelSegment.PEOHTL.RateTypeCode = _frHotelSegment.PEOHTL.RateTypeCode;
                            _HotelSegment.PEOHTL.isRecalable = _frHotelSegment.PEOHTL.isRecalable;
                            _HotelSegment.PEOHTL.ReasonCode = _frHotelSegment.PEOHTL.ReasonCode;
                            _HotelSegment.PEOHTL.HOTELNAME = _frHotelSegment.PEOHTL.HOTELNAME;
                        }

                        List<Westminster.MOE.WebService.eMosBookingService.UO_PeoHtlDetail> _HotelSegment_PeoHtlDetail = new List<Westminster.MOE.WebService.eMosBookingService.UO_PeoHtlDetail>();
                        if (_frHotelSegment.PeoHtlDetail != null)
                        {
                            foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoHtlDetail _frUO_PeoHtlDetail in _frHotelSegment.PeoHtlDetail)
                            {
                                Westminster.MOE.WebService.eMosBookingService.UO_PeoHtlDetail _UO_PeoHtlDetail = new Westminster.MOE.WebService.eMosBookingService.UO_PeoHtlDetail();
                                _UO_PeoHtlDetail.BKGREF = _frUO_PeoHtlDetail.BKGREF;
                                _UO_PeoHtlDetail.COMPANYCODE = _frUO_PeoHtlDetail.COMPANYCODE;
                                _UO_PeoHtlDetail.SEQTYPE = _frUO_PeoHtlDetail.SEQTYPE;
                                _UO_PeoHtlDetail.SEGNUM = _frUO_PeoHtlDetail.SEGNUM;
                                _UO_PeoHtlDetail.SEQNUM = _frUO_PeoHtlDetail.SEQNUM;
                                _UO_PeoHtlDetail.QTY = _frUO_PeoHtlDetail.QTY;
                                _UO_PeoHtlDetail.ARRDATE = _frUO_PeoHtlDetail.ARRDATE;
                                _UO_PeoHtlDetail.DEPARTDATE = _frUO_PeoHtlDetail.DEPARTDATE;
                                _UO_PeoHtlDetail.RMNTS = _frUO_PeoHtlDetail.RMNTS;
                                _UO_PeoHtlDetail.RATENAME = _frUO_PeoHtlDetail.RATENAME;
                                _UO_PeoHtlDetail.ITEMNAME = _frUO_PeoHtlDetail.ITEMNAME;
                                _UO_PeoHtlDetail.STATUS = _frUO_PeoHtlDetail.STATUS;
                                _UO_PeoHtlDetail.CURR = _frUO_PeoHtlDetail.CURR;
                                _UO_PeoHtlDetail.AMT = _frUO_PeoHtlDetail.AMT;
                                _UO_PeoHtlDetail.TOTALAMT = _frUO_PeoHtlDetail.TOTALAMT;
                                _UO_PeoHtlDetail.TAXCURR = _frUO_PeoHtlDetail.TAXCURR;
                                _UO_PeoHtlDetail.TAXAMT = _frUO_PeoHtlDetail.TAXAMT;
                                _UO_PeoHtlDetail.CREATEBY = _frUO_PeoHtlDetail.CREATEBY;
                                _UO_PeoHtlDetail.CREATEON = _frUO_PeoHtlDetail.CREATEON;
                                _UO_PeoHtlDetail.UPDATEBY = _frUO_PeoHtlDetail.UPDATEBY;
                                _UO_PeoHtlDetail.UPDATEON = _frUO_PeoHtlDetail.UPDATEON;
                                _UO_PeoHtlDetail.itemtype = _frUO_PeoHtlDetail.itemtype;
                                _UO_PeoHtlDetail.AMTFEE = _frUO_PeoHtlDetail.AMTFEE;


                                _HotelSegment_PeoHtlDetail.Add(_UO_PeoHtlDetail);
                                _HotelSegment.PeoHtlDetail = _HotelSegment_PeoHtlDetail.ToArray();
                            }
                        }
                        emosObj_HotelSegmentsList.Add(_HotelSegment);
                        emosObj.HotelSegmentsList = emosObj_HotelSegmentsList.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.OtherSegment> emosObj_OtherSegmentsList = new List<Westminster.MOE.WebService.eMosBookingService.OtherSegment>();
                if (moeObj.OtherSegmentsList != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.OtherSegment _frOtherSegment in moeObj.OtherSegmentsList)
                    {
                        Westminster.MOE.WebService.eMosBookingService.OtherSegment _OtherSegment = new Westminster.MOE.WebService.eMosBookingService.OtherSegment();

                        if (_frOtherSegment.PEOOTHER != null)
                        {
                            _OtherSegment.PEOOTHER = new Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHER();
                            _OtherSegment.PEOOTHER.BKGREF = _frOtherSegment.PEOOTHER.BKGREF;
                            _OtherSegment.PEOOTHER.COMPANYCODE = _frOtherSegment.PEOOTHER.COMPANYCODE;
                            _OtherSegment.PEOOTHER.SEGNUM = _frOtherSegment.PEOOTHER.SEGNUM;
                            _OtherSegment.PEOOTHER.SEGTYPE = _frOtherSegment.PEOOTHER.SEGTYPE;
                            _OtherSegment.PEOOTHER.OTHERDATE = _frOtherSegment.PEOOTHER.OTHERDATE;
                            _OtherSegment.PEOOTHER.DESCRIPT1 = _frOtherSegment.PEOOTHER.DESCRIPT1;
                            _OtherSegment.PEOOTHER.DESCRIPT2 = _frOtherSegment.PEOOTHER.DESCRIPT2;
                            _OtherSegment.PEOOTHER.DESCRIPT3 = _frOtherSegment.PEOOTHER.DESCRIPT3;
                            _OtherSegment.PEOOTHER.DESCRIPT4 = _frOtherSegment.PEOOTHER.DESCRIPT4;
                            _OtherSegment.PEOOTHER.CITYCODE = _frOtherSegment.PEOOTHER.CITYCODE;
                            _OtherSegment.PEOOTHER.COUNTRYCODE = _frOtherSegment.PEOOTHER.COUNTRYCODE;
                            _OtherSegment.PEOOTHER.RMK1 = _frOtherSegment.PEOOTHER.RMK1;
                            _OtherSegment.PEOOTHER.RMK2 = _frOtherSegment.PEOOTHER.RMK2;
                            _OtherSegment.PEOOTHER.INVRMK1 = _frOtherSegment.PEOOTHER.INVRMK1;
                            _OtherSegment.PEOOTHER.INVRMK2 = _frOtherSegment.PEOOTHER.INVRMK2;
                            _OtherSegment.PEOOTHER.VCHRMK1 = _frOtherSegment.PEOOTHER.VCHRMK1;
                            _OtherSegment.PEOOTHER.VCHRMK2 = _frOtherSegment.PEOOTHER.VCHRMK2;
                            _OtherSegment.PEOOTHER.SELLCURR = _frOtherSegment.PEOOTHER.SELLCURR;
                            _OtherSegment.PEOOTHER.TOTALSELL = _frOtherSegment.PEOOTHER.TOTALSELL;
                            _OtherSegment.PEOOTHER.TOTALSTAX = _frOtherSegment.PEOOTHER.TOTALSTAX;
                            _OtherSegment.PEOOTHER.COSTCURR = _frOtherSegment.PEOOTHER.COSTCURR;
                            _OtherSegment.PEOOTHER.TOTALCOST = _frOtherSegment.PEOOTHER.TOTALCOST;
                            _OtherSegment.PEOOTHER.TOTALCTAX = _frOtherSegment.PEOOTHER.TOTALCTAX;
                            _OtherSegment.PEOOTHER.PCODE = _frOtherSegment.PEOOTHER.PCODE;
                            _OtherSegment.PEOOTHER.VOIDON = _frOtherSegment.PEOOTHER.VOIDON;
                            _OtherSegment.PEOOTHER.VOIDBY = _frOtherSegment.PEOOTHER.VOIDBY;
                            _OtherSegment.PEOOTHER.SOURCESYSTEM = _frOtherSegment.PEOOTHER.SOURCESYSTEM;
                            _OtherSegment.PEOOTHER.suppliercode = _frOtherSegment.PEOOTHER.suppliercode;
                            _OtherSegment.PEOOTHER.CREATEON = _frOtherSegment.PEOOTHER.CREATEON;
                            _OtherSegment.PEOOTHER.CREATEBY = _frOtherSegment.PEOOTHER.CREATEBY;
                            _OtherSegment.PEOOTHER.UPDATEON = _frOtherSegment.PEOOTHER.UPDATEON;
                            _OtherSegment.PEOOTHER.UPDATEBY = _frOtherSegment.PEOOTHER.UPDATEBY;
                            _OtherSegment.PEOOTHER.GSTTAX = _frOtherSegment.PEOOTHER.GSTTAX;
                            _OtherSegment.PEOOTHER.showDetailFee = _frOtherSegment.PEOOTHER.showDetailFee;
                            _OtherSegment.PEOOTHER.masterDesc = _frOtherSegment.PEOOTHER.masterDesc;
                            _OtherSegment.PEOOTHER.IsRecalable = _frOtherSegment.PEOOTHER.IsRecalable;

                        }

                        List<Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHERDETAIL> _OtherSegment_List_PEOOTHERDETAIL = new List<Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHERDETAIL>();
                        if (_frOtherSegment.List_PEOOTHERDETAIL != null)
                        {
                            foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOOTHERDETAIL _frUO_PEOOTHERDETAIL in _frOtherSegment.List_PEOOTHERDETAIL)
                            {
                                Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHERDETAIL _UO_PEOOTHERDETAIL = new Westminster.MOE.WebService.eMosBookingService.UO_PEOOTHERDETAIL();
                                _UO_PEOOTHERDETAIL.BKGREF = _frUO_PEOOTHERDETAIL.BKGREF;
                                _UO_PEOOTHERDETAIL.COMPANYCODE = _frUO_PEOOTHERDETAIL.COMPANYCODE;
                                _UO_PEOOTHERDETAIL.SEGNUM = _frUO_PEOOTHERDETAIL.SEGNUM;
                                _UO_PEOOTHERDETAIL.SEQNUM = _frUO_PEOOTHERDETAIL.SEQNUM;
                                _UO_PEOOTHERDETAIL.SEQTYPE = _frUO_PEOOTHERDETAIL.SEQTYPE;
                                _UO_PEOOTHERDETAIL.AGETYPE = _frUO_PEOOTHERDETAIL.AGETYPE;
                                _UO_PEOOTHERDETAIL.QTY = _frUO_PEOOTHERDETAIL.QTY;
                                _UO_PEOOTHERDETAIL.CURR = _frUO_PEOOTHERDETAIL.CURR;
                                _UO_PEOOTHERDETAIL.AMT = _frUO_PEOOTHERDETAIL.AMT;
                                _UO_PEOOTHERDETAIL.TAXCURR = _frUO_PEOOTHERDETAIL.TAXCURR;
                                _UO_PEOOTHERDETAIL.TAXAMT = _frUO_PEOOTHERDETAIL.TAXAMT;
                                _UO_PEOOTHERDETAIL.VOIDON = _frUO_PEOOTHERDETAIL.VOIDON;
                                _UO_PEOOTHERDETAIL.VOIDBY = _frUO_PEOOTHERDETAIL.VOIDBY;
                                _UO_PEOOTHERDETAIL.SOURCESYSTEM = _frUO_PEOOTHERDETAIL.SOURCESYSTEM;
                                _UO_PEOOTHERDETAIL.CREATEON = _frUO_PEOOTHERDETAIL.CREATEON;
                                _UO_PEOOTHERDETAIL.CREATEBY = _frUO_PEOOTHERDETAIL.CREATEBY;
                                _UO_PEOOTHERDETAIL.UPDATEON = _frUO_PEOOTHERDETAIL.UPDATEON;
                                _UO_PEOOTHERDETAIL.UPDATEBY = _frUO_PEOOTHERDETAIL.UPDATEBY;
                                _UO_PEOOTHERDETAIL.AMTFEE = _frUO_PEOOTHERDETAIL.AMTFEE;

                                _OtherSegment_List_PEOOTHERDETAIL.Add(_UO_PEOOTHERDETAIL);
                                _OtherSegment.List_PEOOTHERDETAIL = _OtherSegment_List_PEOOTHERDETAIL.ToArray();
                            }
                        }


                        emosObj_OtherSegmentsList.Add(_OtherSegment);
                        emosObj.OtherSegmentsList = emosObj_OtherSegmentsList.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.TicketSegment> emosObj_TicketSegmentsList = new List<Westminster.MOE.WebService.eMosBookingService.TicketSegment>();
                if (moeObj.TicketSegmentsList != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.TicketSegment _frTicketSegment in moeObj.TicketSegmentsList)
                    {
                        Westminster.MOE.WebService.eMosBookingService.TicketSegment _TicketSegment = new Westminster.MOE.WebService.eMosBookingService.TicketSegment();

                        if (_frTicketSegment.UO_PeoTkt != null)
                        {
                            _TicketSegment.UO_PeoTkt = new Westminster.MOE.WebService.eMosBookingService.UO_PeoTkt();
                            _TicketSegment.UO_PeoTkt.CompanyCode = _frTicketSegment.UO_PeoTkt.CompanyCode;
                            _TicketSegment.UO_PeoTkt.Ticket = _frTicketSegment.UO_PeoTkt.Ticket;
                            _TicketSegment.UO_PeoTkt.FareCalData = _frTicketSegment.UO_PeoTkt.FareCalData;
                            _TicketSegment.UO_PeoTkt.pseudo = _frTicketSegment.UO_PeoTkt.pseudo;
                            _TicketSegment.UO_PeoTkt.sinein = _frTicketSegment.UO_PeoTkt.sinein;
                            _TicketSegment.UO_PeoTkt.AirSeg = _frTicketSegment.UO_PeoTkt.AirSeg;
                            _TicketSegment.UO_PeoTkt.TransferFlag = _frTicketSegment.UO_PeoTkt.TransferFlag;
                            _TicketSegment.UO_PeoTkt.nst = _frTicketSegment.UO_PeoTkt.nst;
                            _TicketSegment.UO_PeoTkt.SOURCESYSTEM = _frTicketSegment.UO_PeoTkt.SOURCESYSTEM;
                            _TicketSegment.UO_PeoTkt.commission_per = _frTicketSegment.UO_PeoTkt.commission_per;
                            _TicketSegment.UO_PeoTkt.add_payment = _frTicketSegment.UO_PeoTkt.add_payment;
                            _TicketSegment.UO_PeoTkt.iss_off_code = _frTicketSegment.UO_PeoTkt.iss_off_code;
                            _TicketSegment.UO_PeoTkt.tour_code = _frTicketSegment.UO_PeoTkt.tour_code;
                            _TicketSegment.UO_PeoTkt.org_iss = _frTicketSegment.UO_PeoTkt.org_iss;
                            _TicketSegment.UO_PeoTkt.iss_in_exchange = _frTicketSegment.UO_PeoTkt.iss_in_exchange;
                            _TicketSegment.UO_PeoTkt.ControlNum = _frTicketSegment.UO_PeoTkt.ControlNum;
                            _TicketSegment.UO_PeoTkt.ENDORSEMENTS = _frTicketSegment.UO_PeoTkt.ENDORSEMENTS;
                            _TicketSegment.UO_PeoTkt.AirReason = _frTicketSegment.UO_PeoTkt.AirReason;
                            _TicketSegment.UO_PeoTkt.Destination = _frTicketSegment.UO_PeoTkt.Destination;
                            _TicketSegment.UO_PeoTkt.INTDOM = _frTicketSegment.UO_PeoTkt.INTDOM;
                            _TicketSegment.UO_PeoTkt.PAXAIR = _frTicketSegment.UO_PeoTkt.PAXAIR;
                            _TicketSegment.UO_PeoTkt.Class = _frTicketSegment.UO_PeoTkt.Class;
                            _TicketSegment.UO_PeoTkt.tkttype = _frTicketSegment.UO_PeoTkt.tkttype;
                            _TicketSegment.UO_PeoTkt.SellCurr = _frTicketSegment.UO_PeoTkt.SellCurr;
                            _TicketSegment.UO_PeoTkt.SellFare = _frTicketSegment.UO_PeoTkt.SellFare;
                            _TicketSegment.UO_PeoTkt.CorpCurr = _frTicketSegment.UO_PeoTkt.CorpCurr;
                            _TicketSegment.UO_PeoTkt.CorpFare = _frTicketSegment.UO_PeoTkt.CorpFare;
                            _TicketSegment.UO_PeoTkt.COD4 = _frTicketSegment.UO_PeoTkt.COD4;
                            _TicketSegment.UO_PeoTkt.COD5 = _frTicketSegment.UO_PeoTkt.COD5;
                            _TicketSegment.UO_PeoTkt.COD6 = _frTicketSegment.UO_PeoTkt.COD6;
                            _TicketSegment.UO_PeoTkt.tranflag = _frTicketSegment.UO_PeoTkt.tranflag;
                            _TicketSegment.UO_PeoTkt.TktSeq = _frTicketSegment.UO_PeoTkt.TktSeq;
                            _TicketSegment.UO_PeoTkt.BKGREF = _frTicketSegment.UO_PeoTkt.BKGREF;
                            _TicketSegment.UO_PeoTkt.PNR = _frTicketSegment.UO_PeoTkt.PNR;
                            _TicketSegment.UO_PeoTkt.CltCode = _frTicketSegment.UO_PeoTkt.CltCode;
                            _TicketSegment.UO_PeoTkt.SuppCode = _frTicketSegment.UO_PeoTkt.SuppCode;
                            _TicketSegment.UO_PeoTkt.InvNum = _frTicketSegment.UO_PeoTkt.InvNum;
                            _TicketSegment.UO_PeoTkt.Jqty = _frTicketSegment.UO_PeoTkt.Jqty;
                            _TicketSegment.UO_PeoTkt.AirCode = _frTicketSegment.UO_PeoTkt.AirCode;
                            _TicketSegment.UO_PeoTkt.Airline = _frTicketSegment.UO_PeoTkt.Airline;
                            _TicketSegment.UO_PeoTkt.PaxName = _frTicketSegment.UO_PeoTkt.PaxName;
                            _TicketSegment.UO_PeoTkt.DepartOn = _frTicketSegment.UO_PeoTkt.DepartOn;
                            _TicketSegment.UO_PeoTkt.COD1 = _frTicketSegment.UO_PeoTkt.COD1;
                            _TicketSegment.UO_PeoTkt.COD2 = _frTicketSegment.UO_PeoTkt.COD2;
                            _TicketSegment.UO_PeoTkt.COD3 = _frTicketSegment.UO_PeoTkt.COD3;
                            _TicketSegment.UO_PeoTkt.Commission = _frTicketSegment.UO_PeoTkt.Commission;
                            _TicketSegment.UO_PeoTkt.LocalFareCurr = _frTicketSegment.UO_PeoTkt.LocalFareCurr;
                            _TicketSegment.UO_PeoTkt.LocalFareAmt = _frTicketSegment.UO_PeoTkt.LocalFareAmt;
                            _TicketSegment.UO_PeoTkt.ForeignFareCurr = _frTicketSegment.UO_PeoTkt.ForeignFareCurr;
                            _TicketSegment.UO_PeoTkt.ForeignFareAmt = _frTicketSegment.UO_PeoTkt.ForeignFareAmt;
                            _TicketSegment.UO_PeoTkt.Discount = _frTicketSegment.UO_PeoTkt.Discount;
                            _TicketSegment.UO_PeoTkt.FormPay = _frTicketSegment.UO_PeoTkt.FormPay;
                            _TicketSegment.UO_PeoTkt.Routing = _frTicketSegment.UO_PeoTkt.Routing;
                            _TicketSegment.UO_PeoTkt.FullCurr = _frTicketSegment.UO_PeoTkt.FullCurr;
                            _TicketSegment.UO_PeoTkt.FullFare = _frTicketSegment.UO_PeoTkt.FullFare;
                            _TicketSegment.UO_PeoTkt.PaidCurr = _frTicketSegment.UO_PeoTkt.PaidCurr;
                            _TicketSegment.UO_PeoTkt.PaidFare = _frTicketSegment.UO_PeoTkt.PaidFare;
                            _TicketSegment.UO_PeoTkt.LowCurr = _frTicketSegment.UO_PeoTkt.LowCurr;
                            _TicketSegment.UO_PeoTkt.LowFare = _frTicketSegment.UO_PeoTkt.LowFare;
                            _TicketSegment.UO_PeoTkt.NetCurr = _frTicketSegment.UO_PeoTkt.NetCurr;
                            _TicketSegment.UO_PeoTkt.NetFare = _frTicketSegment.UO_PeoTkt.NetFare;
                            _TicketSegment.UO_PeoTkt.FareBasis = _frTicketSegment.UO_PeoTkt.FareBasis;
                            _TicketSegment.UO_PeoTkt.FareType = _frTicketSegment.UO_PeoTkt.FareType;
                            _TicketSegment.UO_PeoTkt.TotalTax = _frTicketSegment.UO_PeoTkt.TotalTax;
                            _TicketSegment.UO_PeoTkt.Security = _frTicketSegment.UO_PeoTkt.Security;
                            _TicketSegment.UO_PeoTkt.QSurcharge = _frTicketSegment.UO_PeoTkt.QSurcharge;
                            _TicketSegment.UO_PeoTkt.BranchCode = _frTicketSegment.UO_PeoTkt.BranchCode;
                            _TicketSegment.UO_PeoTkt.TeamCode = _frTicketSegment.UO_PeoTkt.TeamCode;
                            _TicketSegment.UO_PeoTkt.IssueOn = _frTicketSegment.UO_PeoTkt.IssueOn;
                            _TicketSegment.UO_PeoTkt.IssueBy = _frTicketSegment.UO_PeoTkt.IssueBy;
                            _TicketSegment.UO_PeoTkt.CreateOn = _frTicketSegment.UO_PeoTkt.CreateOn;
                            _TicketSegment.UO_PeoTkt.CreateBy = _frTicketSegment.UO_PeoTkt.CreateBy;
                            _TicketSegment.UO_PeoTkt.UpdateOn = _frTicketSegment.UO_PeoTkt.UpdateOn;
                            _TicketSegment.UO_PeoTkt.UpdateBy = _frTicketSegment.UO_PeoTkt.UpdateBy;
                            _TicketSegment.UO_PeoTkt.VoidOn = _frTicketSegment.UO_PeoTkt.VoidOn;
                            _TicketSegment.UO_PeoTkt.VoidBy = _frTicketSegment.UO_PeoTkt.VoidBy;
                            _TicketSegment.UO_PeoTkt.voidTeam = _frTicketSegment.UO_PeoTkt.voidTeam;
                            _TicketSegment.UO_PeoTkt.voidreason = _frTicketSegment.UO_PeoTkt.voidreason;
                            _TicketSegment.UO_PeoTkt.TaxAmount1 = _frTicketSegment.UO_PeoTkt.TaxAmount1;
                            _TicketSegment.UO_PeoTkt.TaxID1 = _frTicketSegment.UO_PeoTkt.TaxID1;
                            _TicketSegment.UO_PeoTkt.TaxAmount2 = _frTicketSegment.UO_PeoTkt.TaxAmount2;
                            _TicketSegment.UO_PeoTkt.TaxID2 = _frTicketSegment.UO_PeoTkt.TaxID2;
                            _TicketSegment.UO_PeoTkt.TaxAmount3 = _frTicketSegment.UO_PeoTkt.TaxAmount3;
                            _TicketSegment.UO_PeoTkt.TaxID3 = _frTicketSegment.UO_PeoTkt.TaxID3;
                            _TicketSegment.UO_PeoTkt.FareCalType = _frTicketSegment.UO_PeoTkt.FareCalType;
                            _TicketSegment.UO_PeoTkt.CompressPrintInd = _frTicketSegment.UO_PeoTkt.CompressPrintInd;

                        }

                        List<Westminster.MOE.WebService.eMosBookingService.UO_PeoTktDetail> _TicketSegment_UOList_PeoTktDetail = new List<Westminster.MOE.WebService.eMosBookingService.UO_PeoTktDetail>();
                        if (_frTicketSegment.UOList_PeoTktDetail != null)
                        {
                            foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoTktDetail _frUO_PeoTktDetail in _frTicketSegment.UOList_PeoTktDetail)
                            {
                                Westminster.MOE.WebService.eMosBookingService.UO_PeoTktDetail _UO_PeoTktDetail = new Westminster.MOE.WebService.eMosBookingService.UO_PeoTktDetail();
                                _UO_PeoTktDetail.CompanyCode = _frUO_PeoTktDetail.CompanyCode;
                                _UO_PeoTktDetail.Ticket = _frUO_PeoTktDetail.Ticket;
                                _UO_PeoTktDetail.AirCode = _frUO_PeoTktDetail.AirCode;
                                _UO_PeoTktDetail.TktSeq = _frUO_PeoTktDetail.TktSeq;
                                _UO_PeoTktDetail.SegNum = _frUO_PeoTktDetail.SegNum;
                                _UO_PeoTktDetail.Airline = _frUO_PeoTktDetail.Airline;
                                _UO_PeoTktDetail.Flight = _frUO_PeoTktDetail.Flight;
                                _UO_PeoTktDetail.Class = _frUO_PeoTktDetail.Class;
                                _UO_PeoTktDetail.Seat = _frUO_PeoTktDetail.Seat;
                                _UO_PeoTktDetail.DepartCity = _frUO_PeoTktDetail.DepartCity;
                                _UO_PeoTktDetail.ArrivalCity = _frUO_PeoTktDetail.ArrivalCity;
                                _UO_PeoTktDetail.DepartDATE = _frUO_PeoTktDetail.DepartDATE;
                                _UO_PeoTktDetail.DepartTime = _frUO_PeoTktDetail.DepartTime;
                                _UO_PeoTktDetail.ArrDATE = _frUO_PeoTktDetail.ArrDATE;
                                _UO_PeoTktDetail.ArrTime = _frUO_PeoTktDetail.ArrTime;
                                _UO_PeoTktDetail.ElapsedTime = _frUO_PeoTktDetail.ElapsedTime;
                                _UO_PeoTktDetail.StopOverNum = _frUO_PeoTktDetail.StopOverNum;
                                _UO_PeoTktDetail.StopOverCity = _frUO_PeoTktDetail.StopOverCity;
                                _UO_PeoTktDetail.FareAmt = _frUO_PeoTktDetail.FareAmt;
                                _UO_PeoTktDetail.FareBasic = _frUO_PeoTktDetail.FareBasic;
                                _UO_PeoTktDetail.Commission = _frUO_PeoTktDetail.Commission;
                                _UO_PeoTktDetail.ValidStart = _frUO_PeoTktDetail.ValidStart;
                                _UO_PeoTktDetail.ValidEnd = _frUO_PeoTktDetail.ValidEnd;
                                _UO_PeoTktDetail.CreateOn = _frUO_PeoTktDetail.CreateOn;
                                _UO_PeoTktDetail.CreateBy = _frUO_PeoTktDetail.CreateBy;
                                _UO_PeoTktDetail.UpdateOn = _frUO_PeoTktDetail.UpdateOn;
                                _UO_PeoTktDetail.UpdateBy = _frUO_PeoTktDetail.UpdateBy;
                                _UO_PeoTktDetail.EQP = _frUO_PeoTktDetail.EQP;
                                _UO_PeoTktDetail.Service = _frUO_PeoTktDetail.Service;
                                _UO_PeoTktDetail.status = _frUO_PeoTktDetail.status;
                                _UO_PeoTktDetail.fare_basis_code = _frUO_PeoTktDetail.fare_basis_code;
                                _UO_PeoTktDetail.DepartTerm = _frUO_PeoTktDetail.DepartTerm;
                                _UO_PeoTktDetail.ArrivalTerm = _frUO_PeoTktDetail.ArrivalTerm;
                                _UO_PeoTktDetail.freq_fight_num = _frUO_PeoTktDetail.freq_fight_num;
                                _UO_PeoTktDetail.AirlinePNR = _frUO_PeoTktDetail.AirlinePNR;

                                _TicketSegment_UOList_PeoTktDetail.Add(_UO_PeoTktDetail);
                                _TicketSegment.UOList_PeoTktDetail = _TicketSegment_UOList_PeoTktDetail.ToArray();
                            }
                        }
                        emosObj_TicketSegmentsList.Add(_TicketSegment);
                        emosObj.TicketSegmentsList = emosObj_TicketSegmentsList.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.UO_PeoPax> emosObj_PassengersList = new List<Westminster.MOE.WebService.eMosBookingService.UO_PeoPax>();
                if (moeObj.PassengersList != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PeoPax _frUO_PeoPax in moeObj.PassengersList)
                    {
                        Westminster.MOE.WebService.eMosBookingService.UO_PeoPax _UO_PeoPax = new Westminster.MOE.WebService.eMosBookingService.UO_PeoPax();
                        _UO_PeoPax.BKGREF = _frUO_PeoPax.BKGREF;
                        _UO_PeoPax.COMPANYCODE = _frUO_PeoPax.COMPANYCODE;
                        _UO_PeoPax.SEGNUM = _frUO_PeoPax.SEGNUM;
                        _UO_PeoPax.PAXNUM = _frUO_PeoPax.PAXNUM;
                        _UO_PeoPax.PAXLNAME = _frUO_PeoPax.PAXLNAME;
                        _UO_PeoPax.PAXFNAME = _frUO_PeoPax.PAXFNAME;
                        _UO_PeoPax.PAXTITLE = _frUO_PeoPax.PAXTITLE;
                        _UO_PeoPax.PAXTYPE = _frUO_PeoPax.PAXTYPE;
                        _UO_PeoPax.PAXAIR = _frUO_PeoPax.PAXAIR;
                        _UO_PeoPax.PAXHOTEL = _frUO_PeoPax.PAXHOTEL;
                        _UO_PeoPax.PAXOTHER = _frUO_PeoPax.PAXOTHER;
                        _UO_PeoPax.PAXAGE = _frUO_PeoPax.PAXAGE;
                        _UO_PeoPax.RMK1 = _frUO_PeoPax.RMK1;
                        _UO_PeoPax.RMK2 = _frUO_PeoPax.RMK2;
                        _UO_PeoPax.VOIDON = _frUO_PeoPax.VOIDON;
                        _UO_PeoPax.VOIDBY = _frUO_PeoPax.VOIDBY;
                        _UO_PeoPax.SOURCESYSTEM = _frUO_PeoPax.SOURCESYSTEM;
                        _UO_PeoPax.CREATEON = _frUO_PeoPax.CREATEON;
                        _UO_PeoPax.CREATEBY = _frUO_PeoPax.CREATEBY;
                        _UO_PeoPax.UPDATEON = _frUO_PeoPax.UPDATEON;
                        _UO_PeoPax.UPDATEBY = _frUO_PeoPax.UPDATEBY;


                        emosObj_PassengersList.Add(_UO_PeoPax);
                        emosObj.PassengersList = emosObj_PassengersList.ToArray();
                    }
                }

                if (moeObj.Customer != null)
                {
                    emosObj.Customer = new Westminster.MOE.WebService.eMosBookingService.UO_CUSTOMER();
                    emosObj.Customer.COMPANYCODE = moeObj.Customer.COMPANYCODE;
                    emosObj.Customer.CLTCODE = moeObj.Customer.CLTCODE;
                    emosObj.Customer.CLTNAME = moeObj.Customer.CLTNAME;
                    emosObj.Customer.BRANCHCODE = moeObj.Customer.BRANCHCODE;
                    emosObj.Customer.TEAMCODE = moeObj.Customer.TEAMCODE;
                    emosObj.Customer.MSTRCLT = moeObj.Customer.MSTRCLT;
                    emosObj.Customer.ADDR1 = moeObj.Customer.ADDR1;
                    emosObj.Customer.ADDR2 = moeObj.Customer.ADDR2;
                    emosObj.Customer.ADDR3 = moeObj.Customer.ADDR3;
                    emosObj.Customer.ADDR4 = moeObj.Customer.ADDR4;
                    emosObj.Customer.ADDR5 = moeObj.Customer.ADDR5;
                    emosObj.Customer.POSTAL = moeObj.Customer.POSTAL;
                    emosObj.Customer.CITY = moeObj.Customer.CITY;
                    emosObj.Customer.COUNTRY = moeObj.Customer.COUNTRY;
                    emosObj.Customer.BILLADDR1 = moeObj.Customer.BILLADDR1;
                    emosObj.Customer.BILLADDR2 = moeObj.Customer.BILLADDR2;
                    emosObj.Customer.BILLADDR3 = moeObj.Customer.BILLADDR3;
                    emosObj.Customer.BILLADDR4 = moeObj.Customer.BILLADDR4;
                    emosObj.Customer.BILLADDR5 = moeObj.Customer.BILLADDR5;
                    emosObj.Customer.BILLPOSTAL = moeObj.Customer.BILLPOSTAL;
                    emosObj.Customer.BILLCITY = moeObj.Customer.BILLCITY;
                    emosObj.Customer.BILLCOUNTRY = moeObj.Customer.BILLCOUNTRY;
                    emosObj.Customer.STATUS = moeObj.Customer.STATUS;
                    emosObj.Customer.CONTACT1 = moeObj.Customer.CONTACT1;
                    emosObj.Customer.CONTACT2 = moeObj.Customer.CONTACT2;
                    emosObj.Customer.JOBTITLE1 = moeObj.Customer.JOBTITLE1;
                    emosObj.Customer.JOBTITLE2 = moeObj.Customer.JOBTITLE2;
                    emosObj.Customer.PHONE1 = moeObj.Customer.PHONE1;
                    emosObj.Customer.FAX1 = moeObj.Customer.FAX1;
                    emosObj.Customer.CRTERMS = moeObj.Customer.CRTERMS;
                    emosObj.Customer.PRFTUP = moeObj.Customer.PRFTUP;
                    emosObj.Customer.PRFTDOWN = moeObj.Customer.PRFTDOWN;
                    emosObj.Customer.CURRENCY = moeObj.Customer.CURRENCY;
                    emosObj.Customer.PFDAY = moeObj.Customer.PFDAY;
                    emosObj.Customer.CREATEON = moeObj.Customer.CREATEON;
                    emosObj.Customer.CREATEBY = moeObj.Customer.CREATEBY;
                    emosObj.Customer.UPDATEON = moeObj.Customer.UPDATEON;
                    emosObj.Customer.UPDATEBY = moeObj.Customer.UPDATEBY;
                    emosObj.Customer.email = moeObj.Customer.email;
                    emosObj.Customer.CustRemarks = moeObj.Customer.CustRemarks;
                    emosObj.Customer.GEMSCODE = moeObj.Customer.GEMSCODE;
                    emosObj.Customer.CLTNAME1 = moeObj.Customer.CLTNAME1;
                    //emosObj.Customer.ADDR1_1 = moeObj.Customer.ADDR1_1;
                    //emosObj.Customer.ADDR1_2 = moeObj.Customer.ADDR1_2;
                    //emosObj.Customer.ADDR1_3 = moeObj.Customer.ADDR1_3;
                    //emosObj.Customer.ADDR1_4 = moeObj.Customer.ADDR1_4;
                    //emosObj.Customer.ADDR1_5 = moeObj.Customer.ADDR1_5;

                }
                emosObj.Message = (Westminster.MOE.WebService.eMosBookingService.AutoBookingMessages)Enum.Parse(typeof(Westminster.MOE.WebService.eMosBookingService.AutoBookingMessages), moeObj.Message.ToString());
                emosObj.Status = moeObj.Status;
                emosObj.Exception = moeObj.Exception;
                emosObj.CompanyCode = moeObj.CompanyCode;
                emosObj.PNR = moeObj.PNR;
                emosObj.Bkgref = moeObj.Bkgref;
                emosObj.SourceSystem = moeObj.SourceSystem;
                emosObj.InvType = (Westminster.MOE.WebService.eMosBookingService.Invoice_Types)Enum.Parse(typeof(Westminster.MOE.WebService.eMosBookingService.Invoice_Types), moeObj.InvType.ToString());
            }
            return emosObj;
        }

        public Westminster.MOE.WebService.eMosBookingService.AirlineSegment ToeMosObj(Westminster.MOE.Entity.BLLEntity.Transfer.AirlineSegment moeObj)
        {
            Westminster.MOE.WebService.eMosBookingService.AirlineSegment emosObj = new Westminster.MOE.WebService.eMosBookingService.AirlineSegment();
            if (moeObj != null)
            {

                if (moeObj.UO_PEOAIR != null)
                {
                    emosObj.UO_PEOAIR = new Westminster.MOE.WebService.eMosBookingService.UO_PEOAIR();
                    emosObj.UO_PEOAIR.CompanyCode = moeObj.UO_PEOAIR.CompanyCode;
                    emosObj.UO_PEOAIR.BkgRef = moeObj.UO_PEOAIR.BkgRef;
                    emosObj.UO_PEOAIR.SegNum = moeObj.UO_PEOAIR.SegNum;
                    emosObj.UO_PEOAIR.PNR = moeObj.UO_PEOAIR.PNR;
                    emosObj.UO_PEOAIR.Flight = moeObj.UO_PEOAIR.Flight;
                    emosObj.UO_PEOAIR.Class = moeObj.UO_PEOAIR.Class;
                    emosObj.UO_PEOAIR.DepartCity = moeObj.UO_PEOAIR.DepartCity;
                    emosObj.UO_PEOAIR.ArrivalCity = moeObj.UO_PEOAIR.ArrivalCity;
                    emosObj.UO_PEOAIR.Status = moeObj.UO_PEOAIR.Status;
                    emosObj.UO_PEOAIR.NumOfPax = moeObj.UO_PEOAIR.NumOfPax;
                    emosObj.UO_PEOAIR.DepartDATE = moeObj.UO_PEOAIR.DepartDATE;
                    emosObj.UO_PEOAIR.DepartTime = moeObj.UO_PEOAIR.DepartTime;
                    emosObj.UO_PEOAIR.ArrDATE = moeObj.UO_PEOAIR.ArrDATE;
                    emosObj.UO_PEOAIR.ArrTime = moeObj.UO_PEOAIR.ArrTime;
                    emosObj.UO_PEOAIR.ElapsedTime = moeObj.UO_PEOAIR.ElapsedTime;
                    emosObj.UO_PEOAIR.SSR = moeObj.UO_PEOAIR.SSR;
                    emosObj.UO_PEOAIR.Seat = moeObj.UO_PEOAIR.Seat;
                    emosObj.UO_PEOAIR.ReasonCode = moeObj.UO_PEOAIR.ReasonCode;
                    emosObj.UO_PEOAIR.StopOverNum = moeObj.UO_PEOAIR.StopOverNum;
                    emosObj.UO_PEOAIR.StopOverCity = moeObj.UO_PEOAIR.StopOverCity;
                    emosObj.UO_PEOAIR.Pcode = moeObj.UO_PEOAIR.Pcode;
                    emosObj.UO_PEOAIR.SpecialRQ = moeObj.UO_PEOAIR.SpecialRQ;
                    emosObj.UO_PEOAIR.RMK1 = moeObj.UO_PEOAIR.RMK1;
                    emosObj.UO_PEOAIR.RMK2 = moeObj.UO_PEOAIR.RMK2;
                    emosObj.UO_PEOAIR.INVRMK1 = moeObj.UO_PEOAIR.INVRMK1;
                    emosObj.UO_PEOAIR.INVRMK2 = moeObj.UO_PEOAIR.INVRMK2;
                    emosObj.UO_PEOAIR.VCHRMK1 = moeObj.UO_PEOAIR.VCHRMK1;
                    emosObj.UO_PEOAIR.VCHRMK2 = moeObj.UO_PEOAIR.VCHRMK2;
                    emosObj.UO_PEOAIR.SELLCURR = moeObj.UO_PEOAIR.SELLCURR;
                    emosObj.UO_PEOAIR.TOTALSELL = moeObj.UO_PEOAIR.TOTALSELL;
                    emosObj.UO_PEOAIR.TOTALSTAX = moeObj.UO_PEOAIR.TOTALSTAX;
                    emosObj.UO_PEOAIR.COSTCURR = moeObj.UO_PEOAIR.COSTCURR;
                    emosObj.UO_PEOAIR.TOTALCOST = moeObj.UO_PEOAIR.TOTALCOST;
                    emosObj.UO_PEOAIR.TOTALCTAX = moeObj.UO_PEOAIR.TOTALCTAX;
                    emosObj.UO_PEOAIR.SourceSystem = moeObj.UO_PEOAIR.SourceSystem;
                    emosObj.UO_PEOAIR.CreateOn = moeObj.UO_PEOAIR.CreateOn;
                    emosObj.UO_PEOAIR.CreateBy = moeObj.UO_PEOAIR.CreateBy;
                    emosObj.UO_PEOAIR.UpdateOn = moeObj.UO_PEOAIR.UpdateOn;
                    emosObj.UO_PEOAIR.UpdateBy = moeObj.UO_PEOAIR.UpdateBy;
                    emosObj.UO_PEOAIR.VoidOn = moeObj.UO_PEOAIR.VoidOn;
                    emosObj.UO_PEOAIR.VoidBy = moeObj.UO_PEOAIR.VoidBy;
                    emosObj.UO_PEOAIR.AirSeg = moeObj.UO_PEOAIR.AirSeg;
                    emosObj.UO_PEOAIR.DepartTerminal = moeObj.UO_PEOAIR.DepartTerminal;
                    emosObj.UO_PEOAIR.ArrivalTerminal = moeObj.UO_PEOAIR.ArrivalTerminal;
                    emosObj.UO_PEOAIR.OtherAirlinePNR = moeObj.UO_PEOAIR.OtherAirlinePNR;
                    emosObj.UO_PEOAIR.EQP = moeObj.UO_PEOAIR.EQP;
                    emosObj.UO_PEOAIR.Service = moeObj.UO_PEOAIR.Service;
                    emosObj.UO_PEOAIR.Suppcode = moeObj.UO_PEOAIR.Suppcode;

                }

                List<Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL> emosObj_UOList_PEOAIRDETAIL = new List<Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL>();
                if (moeObj.UOList_PEOAIRDETAIL != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_PEOAIRDETAIL _frUO_PEOAIRDETAIL in moeObj.UOList_PEOAIRDETAIL)
                    {
                        Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL _UO_PEOAIRDETAIL = new Westminster.MOE.WebService.eMosBookingService.UO_PEOAIRDETAIL();
                        _UO_PEOAIRDETAIL.CompanyCode = _frUO_PEOAIRDETAIL.CompanyCode;
                        _UO_PEOAIRDETAIL.BkgRef = _frUO_PEOAIRDETAIL.BkgRef;
                        _UO_PEOAIRDETAIL.SegNum = _frUO_PEOAIRDETAIL.SegNum;
                        _UO_PEOAIRDETAIL.SEQNUM = _frUO_PEOAIRDETAIL.SEQNUM;
                        _UO_PEOAIRDETAIL.SEQTYPE = _frUO_PEOAIRDETAIL.SEQTYPE;
                        _UO_PEOAIRDETAIL.QTY = _frUO_PEOAIRDETAIL.QTY;
                        _UO_PEOAIRDETAIL.CURR = _frUO_PEOAIRDETAIL.CURR;
                        _UO_PEOAIRDETAIL.AMT = _frUO_PEOAIRDETAIL.AMT;
                        _UO_PEOAIRDETAIL.TAXCURR = _frUO_PEOAIRDETAIL.TAXCURR;
                        _UO_PEOAIRDETAIL.TAXAMT = _frUO_PEOAIRDETAIL.TAXAMT;
                        _UO_PEOAIRDETAIL.CREATEON = _frUO_PEOAIRDETAIL.CREATEON;
                        _UO_PEOAIRDETAIL.CREATEBY = _frUO_PEOAIRDETAIL.CREATEBY;
                        _UO_PEOAIRDETAIL.UPDATEON = _frUO_PEOAIRDETAIL.UPDATEON;
                        _UO_PEOAIRDETAIL.UPDATEBY = _frUO_PEOAIRDETAIL.UPDATEBY;
                        _UO_PEOAIRDETAIL.AGETYPE = _frUO_PEOAIRDETAIL.AGETYPE;


                        emosObj_UOList_PEOAIRDETAIL.Add(_UO_PEOAIRDETAIL);
                        emosObj.UOList_PEOAIRDETAIL = emosObj_UOList_PEOAIRDETAIL.ToArray();
                    }
                }
            }
            return emosObj;
        }

        public Westminster.MOE.WebService.eMosBookingService.eMosParameter ToeMosObj(Westminster.MOE.Entity.BLLEntity.Transfer.eMosParameter moeObj)
        {
            Westminster.MOE.WebService.eMosBookingService.eMosParameter emosObj = new Westminster.MOE.WebService.eMosBookingService.eMosParameter();
            if (moeObj != null)
            {
                emosObj.Companycode = moeObj.Companycode;
                emosObj.Currency = moeObj.Currency;
                emosObj.Remark = moeObj.Remark;
                emosObj.CltCode = moeObj.CltCode;
                emosObj.CltRef = moeObj.CltRef;
                emosObj.Consultant = moeObj.Consultant;
                emosObj.RLOC = moeObj.RLOC;
                emosObj.Adcomtxno = moeObj.Adcomtxno;
                emosObj.TSAno = moeObj.TSAno;
                emosObj.TourCode = moeObj.TourCode;
                emosObj.Email = moeObj.Email;
                emosObj.SourceSystem = moeObj.SourceSystem;
                emosObj.IsSubAgent = moeObj.IsSubAgent;
                emosObj.TeamCode = moeObj.TeamCode;
                emosObj.StaffCode = moeObj.StaffCode;
                emosObj.SuppCode = moeObj.SuppCode;
                emosObj.FollwUpBy = moeObj.FollwUpBy;

                List<Westminster.MOE.WebService.eMosBookingService.UO_Air>  emosObj_AirSegments = new List<Westminster.MOE.WebService.eMosBookingService.UO_Air>();
                if (moeObj.AirSegments != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_Air _frUO_Air in moeObj.AirSegments)
                    {
                        Westminster.MOE.WebService.eMosBookingService.UO_Air _UO_Air = new Westminster.MOE.WebService.eMosBookingService.UO_Air();
                        _UO_Air.Airline = _frUO_Air.Airline;
                        _UO_Air.Segnum = _frUO_Air.Segnum;
                        _UO_Air.Flight = _frUO_Air.Flight;
                        _UO_Air.OAC = _frUO_Air.OAC;
                        _UO_Air.OAC_Terminal = _frUO_Air.OAC_Terminal;
                        _UO_Air.DAC = _frUO_Air.DAC;
                        _UO_Air.DAC_Terminal = _frUO_Air.DAC_Terminal;
                        _UO_Air.Class = _frUO_Air.Class;
                        _UO_Air.DepartDateTime = _frUO_Air.DepartDateTime;
                        _UO_Air.ArrivalDateTime = _frUO_Air.ArrivalDateTime;
                        _UO_Air.IsGdsFare = _frUO_Air.IsGdsFare;
                        _UO_Air.IsDepartSegment = _frUO_Air.IsDepartSegment;

                        emosObj_AirSegments.Add(_UO_Air);
                        emosObj.AirSegments = emosObj_AirSegments.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.UO_Ticket>  emosObj_Tickets = new List<Westminster.MOE.WebService.eMosBookingService.UO_Ticket>();
                if (moeObj.Tickets != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_Ticket _frUO_Ticket in moeObj.Tickets)
                    {
                        Westminster.MOE.WebService.eMosBookingService.UO_Ticket _UO_Ticket = new Westminster.MOE.WebService.eMosBookingService.UO_Ticket();
                        _UO_Ticket.Airline = _frUO_Ticket.Airline;
                        _UO_Ticket.Ticket = _frUO_Ticket.Ticket;
                        _UO_Ticket.SegnumList = _frUO_Ticket.SegnumList;
                        _UO_Ticket.PaxGivenName = _frUO_Ticket.PaxGivenName;
                        _UO_Ticket.PaxSurName = _frUO_Ticket.PaxSurName;
                        _UO_Ticket.PaxNum = _frUO_Ticket.PaxNum;
                        _UO_Ticket.PaxType = _frUO_Ticket.PaxType;
                        _UO_Ticket.PaxTitle = _frUO_Ticket.PaxTitle;
                        _UO_Ticket.PaxAge = _frUO_Ticket.PaxAge;
                        _UO_Ticket.Currency = _frUO_Ticket.Currency;
                        _UO_Ticket.SellAmt = _frUO_Ticket.SellAmt;
                        _UO_Ticket.NetAmt = _frUO_Ticket.NetAmt;
                        _UO_Ticket.TotalTax = _frUO_Ticket.TotalTax;
                        _UO_Ticket.TicketIssueOn = _frUO_Ticket.TicketIssueOn;
                        _UO_Ticket.NetNet = _frUO_Ticket.NetNet;
                        _UO_Ticket.TourCode = _frUO_Ticket.TourCode;
                        _UO_Ticket.Commission = _frUO_Ticket.Commission;
                        _UO_Ticket.Destination = _frUO_Ticket.Destination;
                        _UO_Ticket.UATP_Card = _frUO_Ticket.UATP_Card;
                        _UO_Ticket.UATP_Card_Type = _frUO_Ticket.UATP_Card_Type;
                        _UO_Ticket.UATP_Card_holder = _frUO_Ticket.UATP_Card_holder;
                        _UO_Ticket.UATP_CardExpireDate = _frUO_Ticket.UATP_CardExpireDate;
                        _UO_Ticket.INTDOM = _frUO_Ticket.INTDOM;
                        _UO_Ticket.DepartOn = _frUO_Ticket.DepartOn;
                        _UO_Ticket.FormPay = _frUO_Ticket.FormPay;

                        emosObj_Tickets.Add(_UO_Ticket);
                        emosObj.Tickets = emosObj_Tickets.ToArray();
                    }
                }

                List<Westminster.MOE.WebService.eMosBookingService.UO_Other> emosObj_OtherSegments = new List<Westminster.MOE.WebService.eMosBookingService.UO_Other>();
                if (moeObj.OtherSegments != null)
                {
                    foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_Other _frUO_Other in moeObj.OtherSegments)
                    {
                        Westminster.MOE.WebService.eMosBookingService.UO_Other _UO_Other = new Westminster.MOE.WebService.eMosBookingService.UO_Other();
                        _UO_Other.Segnum = _frUO_Other.Segnum;
                        _UO_Other.Servdesc = _frUO_Other.Servdesc;
                        _UO_Other.Segtype = _frUO_Other.Segtype;
                        _UO_Other.Suppliercode = _frUO_Other.Suppliercode;
                        _UO_Other.OtherDate = _frUO_Other.OtherDate;

                        List<Westminster.MOE.WebService.eMosBookingService.UO_OtherDetail> _UO_Other_List_OtherDetail = new List<Westminster.MOE.WebService.eMosBookingService.UO_OtherDetail>();
                        if (_frUO_Other.List_OtherDetail != null)
                        {
                            foreach (Westminster.MOE.Entity.BLLEntity.Transfer.UO_OtherDetail _frUO_OtherDetail in _frUO_Other.List_OtherDetail)
                            {
                                Westminster.MOE.WebService.eMosBookingService.UO_OtherDetail _UO_OtherDetail = new Westminster.MOE.WebService.eMosBookingService.UO_OtherDetail();
                                _UO_OtherDetail.Segnum = _frUO_OtherDetail.Segnum;
                                _UO_OtherDetail.Seqnum = _frUO_OtherDetail.Seqnum;
                                _UO_OtherDetail.AGETYPE = _frUO_OtherDetail.AGETYPE;
                                _UO_OtherDetail.SEQTYPE = _frUO_OtherDetail.SEQTYPE;
                                _UO_OtherDetail.QTY = _frUO_OtherDetail.QTY;
                                _UO_OtherDetail.CURR = _frUO_OtherDetail.CURR;
                                _UO_OtherDetail.AMT = _frUO_OtherDetail.AMT;
                                _UO_OtherDetail.TAXAMT = _frUO_OtherDetail.TAXAMT;

                                _UO_Other_List_OtherDetail.Add(_UO_OtherDetail);
                                _UO_Other.List_OtherDetail = _UO_Other_List_OtherDetail.ToArray();
                            }
                        }

                        emosObj_OtherSegments.Add(_UO_Other);
                        emosObj.OtherSegments = emosObj_OtherSegments.ToArray();
                    }
                }
                emosObj.InvCardType = (Westminster.MOE.WebService.eMosBookingService.eInvCardType)Enum.Parse(typeof(Westminster.MOE.WebService.eMosBookingService.eInvCardType), moeObj.InvCardType.ToString());
            }


            return emosObj;
        }

        public Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingResponse ToMOEObj(Westminster.MOE.WebService.eMosBookingService.AutoBookingResponse emosObj)
        {
            Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingResponse moeObj = new Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingResponse();
            if (emosObj != null)
            {
                moeObj.Message = (Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingMessages)Enum.Parse(typeof(Westminster.MOE.Entity.BLLEntity.Transfer.AutoBookingMessages), emosObj.Message.ToString());
                moeObj.Status = emosObj.Status;
                moeObj.Invnum = emosObj.Invnum;
                moeObj.Exception = emosObj.Exception;
            }
            return moeObj;
        }
    }
}
