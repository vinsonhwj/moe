﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Westminster.MOE.BLL.Transfer;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.WebService
{
    public partial class ImportTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtContent.Text))
            {
                WSResult wsResult = new WSResult();
                using (LinkPNR link = new LinkPNR(Global.Config))
                {
                    link.CreateBookingByIUR(txtContent.Text, out wsResult);

                    lbErr.Text = wsResult.Msg;
                }
            }
        }
    }
}
