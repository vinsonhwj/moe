﻿CREATE TABLE [dbo].[Creditcardsales] (
    [CompanyCode]  NCHAR (2)  NOT NULL,
    [InvNum]       NCHAR (11) NULL,
    [Cardtype]     NCHAR (4)  NULL,
    [CardNum]      NCHAR (16) NULL,
    [Cardholder]   NCHAR (30) NULL,
    [Expirymonth]  NCHAR (2)  NULL,
    [Expiryyear]   NCHAR (2)  NULL,
    [CustomerCode] NCHAR (8)  NULL,
    [CustomerRef]  NCHAR (15) NULL,
    [TANum]        NCHAR (9)  NULL,
    [ApprovalCode] NCHAR (6)  NULL,
    [Aircode]      NCHAR (3)  NULL,
    [Ticket]       NCHAR (14) NULL,
    [Createby]     NCHAR (3)  NULL,
    [Createon]     DATETIME   NULL,
    [Updateby]     NCHAR (3)  NULL,
    [Updateon]     DATETIME   NULL,
    [VOIDBY]       NCHAR (3)  NULL,
    [VOIDON]       DATETIME   NULL,
    [AUTONUM]      INT        IDENTITY (1, 1) NOT NULL
);

