﻿CREATE TABLE [dbo].[CreditCardCharge] (
    [CompanyCode]   NVARCHAR (2)    NULL,
    [CardType]      NVARCHAR (6)    NULL,
    [ChargePercent] DECIMAL (16, 2) NULL
);

