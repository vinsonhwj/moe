﻿CREATE TABLE [dbo].[fop] (
    [COMPANYID]   CHAR (2)  NOT NULL,
    [PaymentCode] CHAR (3)  NOT NULL,
    [PaymentDesc] CHAR (15) NULL,
    [Createon]    DATETIME  NULL,
    [Createby]    CHAR (10) NULL,
    [Updateon]    DATETIME  NULL,
    [Updateby]    CHAR (10) NULL
);

