﻿CREATE TABLE [dbo].[PEOINVFOOTER] (
    [CompanyCode]     NVARCHAR (2)   NOT NULL,
    [DocNum]          NVARCHAR (20)  NOT NULL,
    [seqnum]          NVARCHAR (5)   NOT NULL,
    [subject]         NVARCHAR (300) NULL,
    [description]     NTEXT          NULL,
    [createon]        DATETIME       NULL,
    [createby]        NVARCHAR (5)   NULL,
    [updateon]        DATETIME       NULL,
    [updateby]        NVARCHAR (5)   NULL,
    [footerImagePath] VARCHAR (50)   NULL
);

