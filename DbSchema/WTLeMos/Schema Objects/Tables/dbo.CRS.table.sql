﻿CREATE TABLE [dbo].[CRS] (
    [CompanyCode]    NVARCHAR (2)  NOT NULL,
    [BRANCHCODE]     NCHAR (1)     NOT NULL,
    [TEAMCODE]       NVARCHAR (5)  NOT NULL,
    [Pseudo]         NVARCHAR (4)  NOT NULL,
    [CRS]            NCHAR (1)     NOT NULL,
    [CRSDescription] NVARCHAR (20) NULL,
    [Createon]       DATETIME      NULL,
    [Createby]       NVARCHAR (10) NULL,
    [Updateon]       DATETIME      NULL,
    [Updateby]       NVARCHAR (10) NULL
);

