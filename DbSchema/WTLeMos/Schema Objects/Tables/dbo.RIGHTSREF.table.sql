﻿CREATE TABLE [dbo].[RIGHTSREF] (
    [RIGHTSCODE] NCHAR (5)     NOT NULL,
    [RIGHTSDESC] NVARCHAR (20) NULL,
    [CREATEON]   DATETIME      NULL,
    [CREATEBY]   NVARCHAR (10) NULL,
    [UPDATEON]   DATETIME      NULL,
    [UPDATEBY]   NVARCHAR (10) NULL
);

