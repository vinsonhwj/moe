﻿CREATE TABLE [dbo].[peoProfile] (
    [CompanyCode]      NVARCHAR (2)   NOT NULL,
    [ProfileID]        INT            NOT NULL,
    [ProfileOwner]     VARCHAR (10)   NULL,
    [PaxLName]         NVARCHAR (20)  NULL,
    [PaxFName]         NVARCHAR (40)  NULL,
    [PaxTitle]         NVARCHAR (4)   NULL,
    [PaxType]          NVARCHAR (3)   NULL,
    [DOB]              DATETIME       NULL,
    [HomeTel]          NVARCHAR (20)  NULL,
    [OfficeTel]        NVARCHAR (20)  NULL,
    [Mobile]           NVARCHAR (20)  NULL,
    [Fax]              NVARCHAR (20)  NULL,
    [Email]            NVARCHAR (100) NULL,
    [Addr1]            NVARCHAR (200) NULL,
    [Addr2]            NVARCHAR (200) NULL,
    [Remarks]          NTEXT          NULL,
    [Nationality]      NVARCHAR (50)  NULL,
    [TravelDocNum1]    NVARCHAR (30)  NULL,
    [TravelDocType1]   NVARCHAR (3)   NULL,
    [TravelDocIssue1]  DATETIME       NULL,
    [TravelDocExpiry1] DATETIME       NULL,
    [TravelDocNum2]    NVARCHAR (30)  NULL,
    [TravelDocType2]   NVARCHAR (3)   NULL,
    [TravelDocIssue2]  DATETIME       NULL,
    [TravelDocExpiry2] DATETIME       NULL,
    [TravelDocNum3]    NVARCHAR (30)  NULL,
    [TravelDocType3]   NVARCHAR (3)   NULL,
    [TravelDocIssue3]  DATETIME       NULL,
    [TravelDocExpiry3] DATETIME       NULL,
    [TravelDocNum4]    NVARCHAR (30)  NULL,
    [TravelDocType4]   NVARCHAR (3)   NULL,
    [TravelDocIssue4]  DATETIME       NULL,
    [TravelDocExpiry4] DATETIME       NULL,
    [TravelDocNum5]    NVARCHAR (30)  NULL,
    [TravelDocType5]   NVARCHAR (3)   NULL,
    [TravelDocIssue5]  DATETIME       NULL,
    [TravelDocExpiry5] DATETIME       NULL,
    [FrequentFlyer1]   NVARCHAR (30)  NULL,
    [ProgramCode1]     NVARCHAR (30)  NULL,
    [FrequentFlyer2]   NVARCHAR (30)  NULL,
    [ProgramCode2]     NVARCHAR (30)  NULL,
    [FrequentFlyer3]   NVARCHAR (30)  NULL,
    [ProgramCode3]     NVARCHAR (30)  NULL,
    [FrequentFlyer4]   NVARCHAR (30)  NULL,
    [ProgramCode4]     NVARCHAR (30)  NULL,
    [FrequentFlyer5]   NVARCHAR (30)  NULL,
    [ProgramCode5]     NVARCHAR (30)  NULL,
    [CreateOn]         DATETIME       NULL,
    [CreateBy]         VARCHAR (10)   NULL,
    [UpdateOn]         DATETIME       NULL,
    [UpdateBy]         VARCHAR (5)    NULL
);

