﻿CREATE TABLE [dbo].[eComStatus] (
    [statusid] NUMERIC (18) NOT NULL,
    [status]   NCHAR (30)   NULL,
    [CREATEON] DATETIME     NULL,
    [CREATEBY] NCHAR (10)   NULL,
    [UPDATEON] DATETIME     NULL,
    [UPDATEBY] NCHAR (10)   NULL
);

