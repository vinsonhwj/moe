﻿CREATE TABLE [dbo].[city] (
    [SOURCECODE]    CHAR (3)  NOT NULL,
    [COUNTRYCODE]   CHAR (3)  NOT NULL,
    [CITYCODE]      CHAR (3)  NOT NULL,
    [CITYNAME]      CHAR (40) NOT NULL,
    [GreenwichTime] INT       NULL,
    [CREATEON]      DATETIME  NULL,
    [CREATEBY]      CHAR (10) NULL,
    [UPDATEON]      DATETIME  NULL,
    [UPDATEBY]      CHAR (10) NULL,
    [CallingCode]   CHAR (6)  NULL
);

