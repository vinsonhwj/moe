﻿CREATE TABLE [dbo].[PEORDFSEG] (
    [COMPANYCODE] NVARCHAR (2)    NOT NULL,
    [RDFNUM]      NVARCHAR (10)   NOT NULL,
    [SEGNUM]      NVARCHAR (5)    NOT NULL,
    [SERVTYPE]    NVARCHAR (5)    NULL,
    [SERVNAME]    NVARCHAR (100)  NULL,
    [SERVDESC]    NVARCHAR (1000) NULL,
    [CREATEON]    DATETIME        NULL,
    [CREATEBY]    NVARCHAR (10)   NULL
);

