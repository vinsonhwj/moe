﻿CREATE TABLE [dbo].[CompanySysEncode] (
    [CompanyCode] NVARCHAR (2)  NOT NULL,
    [Encoding]    NVARCHAR (10) NOT NULL,
    [Createon]    DATETIME      NULL,
    [Createby]    NVARCHAR (5)  NULL,
    [Updateon]    DATETIME      NULL,
    [Updateby]    NVARCHAR (5)  NULL
);

