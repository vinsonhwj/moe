﻿CREATE TABLE [dbo].[EMOSSETUP] (
    [ID]          NVARCHAR (10)  NOT NULL,
    [VALUE]       NVARCHAR (100) NULL,
    [CompanyCode] VARCHAR (2)    DEFAULT ('WM') NOT NULL,
    [BranchCode]  VARCHAR (2)    DEFAULT ('H') NOT NULL
);

