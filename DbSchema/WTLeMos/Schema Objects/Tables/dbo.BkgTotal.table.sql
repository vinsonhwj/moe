﻿CREATE TABLE [dbo].[BkgTotal] (
    [COMPANYCODE] NVARCHAR (4)    NOT NULL,
    [Bkgref]      NVARCHAR (20)   NOT NULL,
    [SEGNUM]      NCHAR (8)       NOT NULL,
    [INVCOUNT]    INT             NOT NULL,
    [DOCCOUNT]    INT             NOT NULL,
    [TTLINVGST]   DECIMAL (16, 2) NULL,
    [TTLDOCGST]   DECIMAL (16, 2) NULL,
    [INVCURR]     NCHAR (6)       NULL,
    [INVAMT]      DECIMAL (16, 2) NULL,
    [INVTAXCURR]  NCHAR (6)       NULL,
    [INVTAX]      DECIMAL (16, 2) NULL,
    [DOCCURR]     NCHAR (6)       NULL,
    [DOCAMT]      DECIMAL (16, 2) NULL,
    [DOCTAX]      DECIMAL (16, 2) NULL,
    [ISLAST]      CHAR (1)        NULL,
    [CREATEON]    DATETIME        NOT NULL,
    [CREATEBY]    NVARCHAR (20)   NOT NULL,
    PRIMARY KEY CLUSTERED ([COMPANYCODE] ASC, [Bkgref] ASC, [SEGNUM] ASC)
);

