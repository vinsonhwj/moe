﻿CREATE TABLE [dbo].[peotransHis] (
    [voucherno]   NVARCHAR (20)   NOT NULL,
    [companycode] NVARCHAR (50)   NOT NULL,
    [suppcode]    NVARCHAR (50)   NULL,
    [suppname]    NVARCHAR (50)   NULL,
    [suppaddr1]   NVARCHAR (50)   NULL,
    [suppaddr2]   NVARCHAR (50)   NULL,
    [suppaddr3]   NVARCHAR (50)   NULL,
    [suppaddr4]   NVARCHAR (50)   NULL,
    [supptel]     NVARCHAR (50)   NULL,
    [suppfax]     NVARCHAR (50)   NULL,
    [bkgref]      NVARCHAR (50)   NULL,
    [consultant]  NVARCHAR (50)   NULL,
    [printdate]   NVARCHAR (50)   NULL,
    [paxname]     NVARCHAR (50)   NULL,
    [pickupdate]  NVARCHAR (50)   NULL,
    [pickuptime]  NVARCHAR (50)   NULL,
    [fromadd]     NVARCHAR (1000) NULL,
    [toadd]       NVARCHAR (1000) NULL,
    [cartype]     NVARCHAR (50)   NULL,
    [remark]      NVARCHAR (1000) NULL,
    [confirmedby] NVARCHAR (50)   NULL,
    [carno]       NVARCHAR (50)   NULL,
    [voidon]      DATETIME        NULL,
    [voidby]      NVARCHAR (50)   NULL,
    [voidreason]  NVARCHAR (500)  NULL,
    [voidteam]    NVARCHAR (50)   NULL,
    [createon]    DATETIME        NULL,
    [createby]    NVARCHAR (50)   NULL,
    [updateon]    DATETIME        NULL,
    [updateby]    NVARCHAR (50)   NULL,
    [staffname]   NVARCHAR (100)  NULL,
    [stafftel]    NVARCHAR (100)  NULL
);

