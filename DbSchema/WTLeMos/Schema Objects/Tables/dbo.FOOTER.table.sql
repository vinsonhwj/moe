﻿CREATE TABLE [dbo].[FOOTER] (
    [CompanyCode]     NVARCHAR (2)   NOT NULL,
    [seqnum]          NVARCHAR (5)   NOT NULL,
    [subject]         NVARCHAR (300) NULL,
    [description]     NTEXT          NULL,
    [isDefaultFor]    NVARCHAR (5)   NULL,
    [createon]        DATETIME       NULL,
    [createby]        NVARCHAR (5)   NULL,
    [updateon]        DATETIME       NULL,
    [updateby]        NVARCHAR (5)   NULL,
    [voidon]          DATETIME       NULL,
    [voidby]          NVARCHAR (5)   NULL,
    [footerImagePath] VARCHAR (50)   NULL
);

