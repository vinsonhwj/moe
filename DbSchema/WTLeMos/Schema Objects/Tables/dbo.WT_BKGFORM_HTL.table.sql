﻿CREATE TABLE [dbo].[WT_BKGFORM_HTL] (
    [BOOKID]       INT            NOT NULL,
    [HTLSEQ]       INT            NOT NULL,
    [HTLSEGNUM]    VARCHAR (5)    NULL,
    [HTLCHOICE]    VARCHAR (5)    NULL,
    [CITYCODE]     VARCHAR (3)    NULL,
    [STATUS]       VARCHAR (2)    NULL,
    [HOTELNAME]    NVARCHAR (100) NULL,
    [CHECKINON]    DATETIME       NULL,
    [CHECKOUTON]   DATETIME       NULL,
    [ROOMNIGHTS]   INT            NULL,
    [ROOMCATEGORY] VARCHAR (50)   NULL,
    [ROOMTYPE]     VARCHAR (50)   NULL,
    [QTY]          INT            NULL,
    [BREAKFAST]    VARCHAR (6)    NULL,
    [CREATEON]     DATETIME       NULL,
    [CREATEBY]     VARCHAR (10)   NULL
);

