﻿CREATE TABLE [dbo].[teamcrs] (
    [companycode] NVARCHAR (2)  NOT NULL,
    [branchcode]  NVARCHAR (1)  NOT NULL,
    [teamcode]    NVARCHAR (5)  NOT NULL,
    [crs]         NVARCHAR (1)  NOT NULL,
    [pseudo]      NVARCHAR (6)  NOT NULL,
    [createon]    DATETIME      NULL,
    [createby]    NVARCHAR (10) NULL,
    [updateon]    DATETIME      NULL,
    [updateby]    NVARCHAR (10) NULL
);

