﻿CREATE TABLE [dbo].[uatpmstr] (
    [companycode] NCHAR (2)       NOT NULL,
    [branch]      NCHAR (1)       NULL,
    [PSEUDO]      NCHAR (4)       NULL,
    [form]        NCHAR (3)       NULL,
    [ticket]      NCHAR (10)      NOT NULL,
    [tktseq]      NCHAR (3)       NOT NULL,
    [tkttime]     NCHAR (4)       NULL,
    [cashtype]    NCHAR (2)       NULL,
    [tkttype]     NCHAR (1)       NULL,
    [bkgref]      NCHAR (10)      NULL,
    [netamt]      DECIMAL (16, 2) NULL,
    [commission]  DECIMAL (16, 2) NULL,
    [paxname]     NCHAR (30)      NULL,
    [upfilename]  NCHAR (40)      NULL,
    [createon]    DATETIME        NULL,
    [createby]    NCHAR (10)      NULL,
    [tktdate]     DATETIME        NULL,
    [SineInCode]  NCHAR (2)       NULL,
    [Conj]        NCHAR (2)       NULL,
    [file_type]   NCHAR (1)       NULL
);

