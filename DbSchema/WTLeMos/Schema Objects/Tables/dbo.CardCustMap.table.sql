﻿CREATE TABLE [dbo].[CardCustMap] (
    [CompanyCode] NCHAR (4)     NOT NULL,
    [CardType]    NCHAR (40)    NOT NULL,
    [BranchCode]  NCHAR (2)     NOT NULL,
    [CltCode]     NCHAR (16)    NOT NULL,
    [CREATEON]    DATETIME      NOT NULL,
    [CREATEBY]    NVARCHAR (20) NOT NULL,
    [UPDATEON]    DATETIME      NULL,
    [UPDATEBY]    NVARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [CardType] ASC, [BranchCode] ASC)
);

