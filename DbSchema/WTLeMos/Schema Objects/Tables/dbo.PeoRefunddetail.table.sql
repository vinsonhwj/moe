﻿CREATE TABLE [dbo].[PeoRefunddetail] (
    [Companycode] CHAR (2)        NOT NULL,
    [Refundno]    CHAR (10)       NOT NULL,
    [seqno]       CHAR (5)        NOT NULL,
    [refundtype]  CHAR (10)       NULL,
    [itemcode]    CHAR (10)       NULL,
    [refunddate]  DATETIME        NULL,
    [refundcurr]  CHAR (3)        NULL,
    [refundamt]   DECIMAL (18, 2) NULL,
    [penaltycurr] CHAR (3)        NULL,
    [penalty]     DECIMAL (18, 2) NULL,
    [content]     NTEXT           NULL,
    [status]      CHAR (10)       NULL,
    [createon]    DATETIME        NULL,
    [createby]    CHAR (10)       NULL,
    [updateon]    DATETIME        NULL,
    [updateby]    CHAR (10)       NULL
);

