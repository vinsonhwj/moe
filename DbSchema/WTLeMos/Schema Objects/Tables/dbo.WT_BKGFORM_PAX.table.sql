﻿CREATE TABLE [dbo].[WT_BKGFORM_PAX] (
    [BOOKID]       INT           NOT NULL,
    [PAXSEQ]       INT           NOT NULL,
    [PAXSEGNUM]    VARCHAR (5)   NULL,
    [PAXNAME]      NVARCHAR (50) NULL,
    [AGE]          INT           NULL,
    [DOB]          DATETIME      NULL,
    [PASSPORTTYPE] VARCHAR (20)  NULL,
    [SHOWPASSPORT] BIT           NULL,
    [VISAREQUIRE]  VARCHAR (20)  NULL,
    [INSURANCE]    VARCHAR (20)  NULL,
    [CREATEON]     DATETIME      NULL,
    [CREATEBY]     VARCHAR (10)  NULL
);

