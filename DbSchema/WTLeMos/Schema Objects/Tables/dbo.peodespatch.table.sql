﻿CREATE TABLE [dbo].[peodespatch] (
    [bkgref]         NVARCHAR (10)  NOT NULL,
    [companycode]    NVARCHAR (2)   NULL,
    [paxnum]         NVARCHAR (5)   NULL,
    [segnum]         NVARCHAR (5)   NOT NULL,
    [tel]            NVARCHAR (20)  NULL,
    [toadd]          NVARCHAR (500) NULL,
    [remark]         NVARCHAR (500) NULL,
    [instructedby]   NVARCHAR (20)  NULL,
    [despatchedby]   NVARCHAR (20)  NULL,
    [tempkey]        NVARCHAR (50)  NULL,
    [despatcheddate] NVARCHAR (50)  NULL,
    [createon]       DATETIME       NULL,
    [createby]       NVARCHAR (10)  NULL,
    [updateon]       DATETIME       NULL,
    [updateby]       NVARCHAR (10)  NULL
);

