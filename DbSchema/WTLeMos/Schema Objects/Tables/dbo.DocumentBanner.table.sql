﻿CREATE TABLE [dbo].[DocumentBanner] (
    [companycode] NVARCHAR (2)  NOT NULL,
    [bkgref]      NVARCHAR (10) NOT NULL,
    [docnum]      NVARCHAR (10) NOT NULL,
    [bannertype]  NVARCHAR (50) NOT NULL,
    [createon]    DATETIME      NOT NULL,
    [createby]    NVARCHAR (5)  NOT NULL,
    [updateon]    DATETIME      NULL,
    [updateby]    NVARCHAR (5)  NULL
);

