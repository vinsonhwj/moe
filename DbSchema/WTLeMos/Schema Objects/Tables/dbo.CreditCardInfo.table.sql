﻿CREATE TABLE [dbo].[CreditCardInfo] (
    [CompanyCode]   NCHAR (2)  NOT NULL,
    [Recordtype]    NCHAR (1)  NOT NULL,
    [Refcode]       NCHAR (8)  NOT NULL,
    [Reftype]       NCHAR (3)  NULL,
    [Cardseq]       NCHAR (2)  NOT NULL,
    [Cardtype]      NCHAR (4)  NOT NULL,
    [Cardnum]       NCHAR (16) NOT NULL,
    [Cardholder]    NCHAR (30) NULL,
    [Expirymonth]   NCHAR (2)  NOT NULL,
    [Expiryyear]    NCHAR (2)  NOT NULL,
    [RefDesc]       NCHAR (30) NULL,
    [effectivedate] DATETIME   NULL,
    [expirydate]    DATETIME   NULL,
    [Createon]      DATETIME   NULL,
    [Createby]      NCHAR (3)  NULL,
    [Updateon]      DATETIME   NULL,
    [Updateby]      NCHAR (3)  NULL
);

