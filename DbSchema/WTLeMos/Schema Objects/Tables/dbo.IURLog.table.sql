﻿CREATE TABLE [dbo].[IURLog] (
    [IURLOGID]    NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [URFilename]  NVARCHAR (200)  NOT NULL,
    [PNR]         NVARCHAR (10)   NULL,
    [Description] NVARCHAR (50)   NULL,
    [Status]      NVARCHAR (50)   NULL,
    [Remarks]     NVARCHAR (1000) NULL,
    [CreateOn]    DATETIME        NULL,
    [CreateBy]    NVARCHAR (20)   NULL
);

