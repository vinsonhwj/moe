﻿CREATE TABLE [dbo].[htlpreference] (
    [COMPANYID]      NCHAR (2)     NOT NULL,
    [MembershipNo]   NCHAR (16)    NOT NULL,
    [SeqNum]         NCHAR (3)     NOT NULL,
    [Hotel]          NCHAR (50)    NULL,
    [RoomPreference] NCHAR (4)     NULL,
    [RoomType]       NCHAR (4)     NULL,
    [Createon]       DATETIME      NULL,
    [Createby]       NCHAR (10)    NULL,
    [Updateon]       DATETIME      NULL,
    [Updateby]       NCHAR (10)    NULL,
    [Remarks]        NVARCHAR (50) NULL
);

