﻿CREATE TABLE [dbo].[shortform_prod_citypairmile] (
    [Org]   VARCHAR (3) NOT NULL,
    [Dst]   VARCHAR (3) NOT NULL,
    [Miles] INT         NOT NULL,
    PRIMARY KEY CLUSTERED ([Org] ASC, [Dst] ASC)
);

