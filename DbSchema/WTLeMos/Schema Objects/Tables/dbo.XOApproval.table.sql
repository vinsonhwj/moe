﻿CREATE TABLE [dbo].[XOApproval] (
    [CompanyCode]   NVARCHAR (2) NULL,
    [ApprovalCode]  VARCHAR (5)  NULL,
    [ApprovalLevel] INT          NULL,
    [CreateOn]      DATETIME     NULL,
    [CreateBy]      VARCHAR (5)  NULL,
    [UpdateOn]      DATETIME     NULL,
    [UpdateBy]      VARCHAR (5)  NULL
);

