﻿CREATE TABLE [dbo].[AirlineCustMap] (
    [CompanyCode]    NCHAR (4)      NOT NULL,
    [AirCode]        NCHAR (4)      NOT NULL,
    [CltCode]        NCHAR (16)     NOT NULL,
    [CltDescription] NVARCHAR (100) NOT NULL,
    [CREATEON]       DATETIME       NOT NULL,
    [CREATEBY]       NVARCHAR (10)  NOT NULL,
    [UPDATEON]       DATETIME       NULL,
    [UPDATEBY]       NVARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [AirCode] ASC)
);

