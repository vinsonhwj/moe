﻿CREATE TABLE [dbo].[PEOMCO] (
    [COMPANYCODE]    NVARCHAR (2)    NOT NULL,
    [MCONUM]         NVARCHAR (14)   NOT NULL,
    [MCOSEQ]         NVARCHAR (5)    NOT NULL,
    [BKGREF]         NVARCHAR (10)   NOT NULL,
    [SUPPCODE]       NVARCHAR (8)    NULL,
    [INVNUM]         NVARCHAR (10)   NULL,
    [COSTCURR]       NVARCHAR (3)    NULL,
    [COSTAMT]        DECIMAL (16, 2) NULL,
    [TAXCURR]        NVARCHAR (3)    NULL,
    [TAXAMT]         DECIMAL (16, 2) NULL,
    [HKDAMT]         DECIMAL (16, 2) NULL,
    [RMK1]           NVARCHAR (50)   NULL,
    [RMK2]           NVARCHAR (20)   NULL,
    [RMK3]           NVARCHAR (20)   NULL,
    [RMK4]           NVARCHAR (20)   NULL,
    [AIRCODE]        NVARCHAR (3)    NULL,
    [ISSUEON]        DATETIME        NULL,
    [ISSUEBY]        NVARCHAR (10)   NULL,
    [BANK]           NVARCHAR (160)  NULL,
    [CHEQUENO]       NVARCHAR (8)    NULL,
    [PAYMENTMETHOD]  NVARCHAR (20)   NULL,
    [AIRLINE]        NVARCHAR (3)    NULL,
    [STAFFCODE]      NVARCHAR (10)   NULL,
    [TEAMCODE]       NVARCHAR (5)    NULL,
    [PRINTTEAM]      NVARCHAR (5)    NULL,
    [CR_card_no]     NCHAR (30)      NULL,
    [CR_card_type]   NCHAR (10)      NULL,
    [CR_card_expiry] DATETIME        NULL,
    [VOID2JBA]       NVARCHAR (10)   NULL,
    [VOIDON]         DATETIME        NULL,
    [VOIDBY]         NVARCHAR (10)   NULL,
    [VOIDTEAM]       NVARCHAR (5)    NULL,
    [VOIDREASON]     NVARCHAR (60)   NULL,
    [CREATEON]       DATETIME        NULL,
    [CREATEBY]       NVARCHAR (10)   NULL,
    [UPDATEON]       DATETIME        NULL,
    [UPDATEBY]       NVARCHAR (10)   NULL,
    [gstpct]         DECIMAL (18)    NULL,
    [gstamt]         DECIMAL (18)    NULL,
    [Track]          NVARCHAR (1)    NULL,
    [INTDOM]         NVARCHAR (2)    NULL,
    [SegType]        NVARCHAR (3)    NULL
);

