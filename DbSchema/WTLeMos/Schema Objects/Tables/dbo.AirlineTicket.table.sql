﻿CREATE TABLE [dbo].[AirlineTicket] (
    [CompanyCode] VARCHAR (2)  NOT NULL,
    [Airline]     VARCHAR (3)  NOT NULL,
    [Value]       VARCHAR (10) NOT NULL,
    [CreateOn]    DATETIME     NOT NULL,
    [CreateBy]    VARCHAR (10) NOT NULL,
    [UpdateOn]    DATETIME     NOT NULL,
    [UpdateBy]    VARCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [Airline] ASC)
);

