﻿CREATE TABLE [dbo].[WT_BKGFORM_FARE] (
    [BOOKID]      INT             NOT NULL,
    [FARESEQ]     INT             NOT NULL,
    [SEGTYPE]     VARCHAR (3)     NOT NULL,
    [DESCRIPTION] NVARCHAR (200)  NOT NULL,
    [ROOMNIGHT]   INT             NOT NULL,
    [QTY]         INT             NOT NULL,
    [TAXAMT]      DECIMAL (16, 2) NOT NULL,
    [SELLAMT]     DECIMAL (16, 2) NOT NULL,
    [TOTALAMT]    DECIMAL (16, 2) NOT NULL,
    [CREATEON]    DATETIME        NOT NULL,
    [CREATEBY]    VARCHAR (10)    NOT NULL
);

