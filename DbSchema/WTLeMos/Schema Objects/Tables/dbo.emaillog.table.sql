﻿CREATE TABLE [dbo].[emaillog] (
    [emailid]     NUMERIC (18)    NOT NULL,
    [companycode] NVARCHAR (2)    NOT NULL,
    [sender]      NVARCHAR (500)  NULL,
    [recipient]   NVARCHAR (500)  NULL,
    [contents]    NVARCHAR (4000) NULL,
    [createon]    DATETIME        NULL,
    [createby]    NVARCHAR (20)   NULL,
    [updateon]    DATETIME        NULL,
    [updateby]    NVARCHAR (20)   NULL
);

