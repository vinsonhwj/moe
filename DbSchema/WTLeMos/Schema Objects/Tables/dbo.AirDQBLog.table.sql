﻿CREATE TABLE [dbo].[AirDQBLog] (
    [IURLOGID]    NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [URFilename]  NVARCHAR (200)  NOT NULL,
    [PNR]         NVARCHAR (10)   NULL,
    [Description] TEXT            NULL,
    [Status]      NVARCHAR (50)   NULL,
    [Remarks]     NVARCHAR (1000) NULL,
    [CreateOn]    DATETIME        NULL,
    [CreateBy]    NVARCHAR (20)   NULL
);

