﻿CREATE TABLE [dbo].[PEOMIS] (
    [BKGREF]         NVARCHAR (10) NOT NULL,
    [COMPANYCODE]    NVARCHAR (2)  NOT NULL,
    [SEGNUM]         NVARCHAR (10) NOT NULL,
    [CLTCODE]        NCHAR (8)     NULL,
    [GEMSCODE]       NVARCHAR (10) NULL,
    [GEMSCODE3]      NVARCHAR (10) NULL,
    [COD1]           NVARCHAR (20) NULL,
    [COD2]           NVARCHAR (20) NULL,
    [COD3]           NVARCHAR (20) NULL,
    [REASONCODE]     NVARCHAR (2)  NULL,
    [VOIDON]         DATETIME      NULL,
    [VOIDBY]         NVARCHAR (10) NULL,
    [CREATEON]       DATETIME      NULL,
    [CREATEBY]       NVARCHAR (10) NULL,
    [UPDATEON]       DATETIME      NULL,
    [UPDATEBY]       NVARCHAR (10) NULL,
    [TRANSACTIONFEE] DECIMAL (18)  NULL,
    [MIS1]           NVARCHAR (50) NULL,
    [MIS2]           NVARCHAR (50) NULL,
    [MIS3]           NVARCHAR (50) NULL
);

