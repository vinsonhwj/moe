﻿CREATE TABLE [dbo].[peotrans] (
    [bkgref]        NVARCHAR (10)  NOT NULL,
    [companycode]   NVARCHAR (2)   NOT NULL,
    [PAXNUM]        NVARCHAR (5)   NOT NULL,
    [segnum]        NVARCHAR (5)   NOT NULL,
    [CITYCode]      NVARCHAR (3)   NULL,
    [CarType]       NVARCHAR (50)  NULL,
    [DeptTime]      NVARCHAR (5)   NULL,
    [FromType]      NVARCHAR (1)   NULL,
    [FromAdd]       NVARCHAR (500) NULL,
    [ToType]        NVARCHAR (1)   NULL,
    [ToAdd]         NVARCHAR (500) NULL,
    [FlightDetail]  NVARCHAR (500) NULL,
    [CarNo]         NVARCHAR (20)  NULL,
    [ConfirmedBy]   NVARCHAR (20)  NULL,
    [tempkey]       NVARCHAR (50)  NULL,
    [ConfirmedDate] DATETIME       NULL,
    [createon]      NVARCHAR (8)   NULL,
    [createby]      NVARCHAR (10)  NULL,
    [updateon]      NVARCHAR (8)   NULL,
    [updateby]      NVARCHAR (10)  NULL
);

