﻿CREATE TABLE [dbo].[ItinRmkMstr] (
    [CompanyCode] NCHAR (2)  NOT NULL,
    [RemarkID]    NCHAR (10) NOT NULL,
    [RemarkType]  NCHAR (10) NULL,
    [Description] NCHAR (80) NULL,
    [CreateOn]    DATETIME   NULL,
    [CreateBy]    NCHAR (10) NULL,
    [UpdateOn]    DATETIME   NULL,
    [UpdateBy]    NCHAR (10) NULL
);

