﻿CREATE TABLE [dbo].[PrinterMapping] (
    [PrinterID]   INT           NOT NULL,
    [CompanyCode] NCHAR (4)     NOT NULL,
    [Login]       NVARCHAR (20) NOT NULL,
    [CreateOn]    DATETIME      NOT NULL,
    [CreateBy]    NVARCHAR (20) NOT NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NVARCHAR (20) NULL
);

