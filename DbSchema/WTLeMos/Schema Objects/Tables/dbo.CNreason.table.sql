﻿CREATE TABLE [dbo].[CNreason] (
    [Companycode] CHAR (2)  NULL,
    [CNaction]    CHAR (10) NULL,
    [CNdescript]  CHAR (60) NULL,
    [createon]    DATETIME  NULL,
    [createby]    CHAR (10) NULL,
    [updateon]    DATETIME  NULL,
    [updateby]    CHAR (10) NULL
);

