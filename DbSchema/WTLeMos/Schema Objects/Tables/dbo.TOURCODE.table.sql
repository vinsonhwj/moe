﻿CREATE TABLE [dbo].[TOURCODE] (
    [COMPANYCODE] NVARCHAR (4)   NOT NULL,
    [TOURCODE]    NVARCHAR (30)  NOT NULL,
    [TOURDESC]    NVARCHAR (200) NOT NULL,
    [STARTDATE]   DATETIME       NOT NULL,
    [ENDDATE]     DATETIME       NOT NULL,
    [CREATEON]    DATETIME       NOT NULL,
    [CREATEBY]    NVARCHAR (20)  NOT NULL,
    [UPDATEON]    DATETIME       NULL,
    [UPDATEBY]    NVARCHAR (20)  NULL,
    [suspended]   INT            DEFAULT (0) NOT NULL,
    [CITYCODE1]   VARCHAR (3)    NULL,
    [CITYCODE2]   VARCHAR (3)    NULL,
    PRIMARY KEY CLUSTERED ([TOURCODE] ASC)
);

