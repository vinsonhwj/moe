﻿CREATE TABLE [dbo].[PeoSetup] (
    [TableName] NCHAR (50) NOT NULL,
    [LastValue] NCHAR (30) NOT NULL,
    [CreateOn]  DATETIME   NULL,
    [CreateBy]  NCHAR (10) NULL,
    [UpdateOn]  DATETIME   NULL,
    [UpdateBy]  NCHAR (10) NULL
);

