﻿CREATE TABLE [dbo].[WT_PRODUCTCODE] (
    [CompanyCode]       VARCHAR (2)  NOT NULL,
    [Bkgref]            VARCHAR (10) NOT NULL,
    [Product_Code]      VARCHAR (20) NULL,
    [Product_TeamCode]  NCHAR (5)    NULL,
    [Product_TypeCode]  VARCHAR (4)  NULL,
    [Product_Airline]   VARCHAR (3)  NULL,
    [Product_CityCode]  VARCHAR (3)  NULL,
    [Product_OtherType] VARCHAR (10) NULL,
    [Product_Suffix]    VARCHAR (5)  NULL
);

