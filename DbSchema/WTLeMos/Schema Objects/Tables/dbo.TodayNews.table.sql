﻿CREATE TABLE [dbo].[TodayNews] (
    [NewsID]       INT            IDENTITY (1, 1) NOT NULL,
    [CompanyCode]  NVARCHAR (2)   NOT NULL,
    [Subject]      NVARCHAR (50)  NULL,
    [Description1] NVARCHAR (100) NULL,
    [Description2] NVARCHAR (100) NULL,
    [Description3] NVARCHAR (100) NULL,
    [EffectiveOn]  DATETIME       NULL,
    [CreateOn]     DATETIME       NULL,
    [CreateBy]     NVARCHAR (10)  NULL,
    [ExpiredOn]    DATETIME       NULL
);

