﻿CREATE TABLE [dbo].[PEOPMQ] (
    [Companycode]    CHAR (2)        NOT NULL,
    [Bkgref]         CHAR (10)       NOT NULL,
    [staffcode]      CHAR (10)       NULL,
    [teamcode]       CHAR (5)        NULL,
    [status]         CHAR (1)        NULL,
    [staff_reason]   CHAR (5)        NULL,
    [mgr_reason]     NVARCHAR (100)  NULL,
    [acct_reason]    NVARCHAR (100)  NULL,
    [staff_deadline] DATETIME        NULL,
    [mgr_deadline]   DATETIME        NULL,
    [acct_deadline]  DATETIME        NULL,
    [profit]         DECIMAL (18, 2) NULL,
    [yield]          DECIMAL (18, 2) NULL,
    [cust_up]        DECIMAL (18, 2) NULL,
    [cust_down]      DECIMAL (18, 2) NULL,
    [staff_up]       DECIMAL (18, 2) NULL,
    [staff_down]     DECIMAL (18, 2) NULL,
    [createon]       DATETIME        NULL,
    [createby]       CHAR (10)       NULL,
    [updateon]       DATETIME        NULL,
    [updateby]       CHAR (10)       NULL,
    [TotalInvCount]  INT             NULL,
    [TotalDocCount]  INT             NULL,
    [TotalInvAmt]    DECIMAL (18)    NULL,
    [TotalDocAmt]    DECIMAL (18)    NULL,
    [CancelBkg]      CHAR (1)        NULL
);

