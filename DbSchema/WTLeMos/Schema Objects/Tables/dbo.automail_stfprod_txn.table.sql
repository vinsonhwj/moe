﻿CREATE TABLE [dbo].[automail_stfprod_txn] (
    [COMPANYCODE] NCHAR (4)       NULL,
    [BRANCHCODE]  NCHAR (2)       NULL,
    [TEAMCODE]    NCHAR (10)      NULL,
    [STAFFCODE]   NCHAR (20)      NULL,
    [TOTALPAX]    INT             NULL,
    [PRICE]       NUMERIC (16, 2) NULL,
    [COST]        NUMERIC (16, 2) NULL,
    [MARGIN]      NUMERIC (16, 2) NULL,
    [YIELD]       NUMERIC (16, 2) NULL,
    [YTODPRICE]   NUMERIC (16, 2) NULL,
    [YTODCOST]    NUMERIC (16, 2) NULL,
    [YTODMARGIN]  NUMERIC (16, 2) NULL,
    [YTODYIELD]   NUMERIC (16, 2) NULL
);

