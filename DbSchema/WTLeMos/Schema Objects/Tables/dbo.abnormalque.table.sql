﻿CREATE TABLE [dbo].[abnormalque] (
    [Companycode] VARCHAR (2)     NOT NULL,
    [MasterPnr]   VARCHAR (10)    NOT NULL,
    [InvCount]    INT             NOT NULL,
    [DocCount]    INT             NOT NULL,
    [InvAmt]      DECIMAL (16, 2) NOT NULL,
    [InvTax]      DECIMAL (16, 2) NOT NULL,
    [DocAmt]      DECIMAL (16, 2) NOT NULL,
    [DocTax]      DECIMAL (16, 2) NOT NULL,
    [Margin]      DECIMAL (16, 2) NOT NULL,
    [Yield]       DECIMAL (16, 2) NOT NULL,
    [CreateOn]    DATETIME        NOT NULL,
    [CreateBy]    VARCHAR (10)    NOT NULL,
    [UpdateOn]    DATETIME        NULL,
    [UpdateBy]    VARCHAR (10)    NULL,
    PRIMARY KEY CLUSTERED ([Companycode] ASC, [MasterPnr] ASC)
);

