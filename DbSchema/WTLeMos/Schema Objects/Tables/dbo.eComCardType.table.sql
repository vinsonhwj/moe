﻿CREATE TABLE [dbo].[eComCardType] (
    [typeID]   NCHAR (10) NOT NULL,
    [typeName] NCHAR (20) NULL,
    [Createon] DATETIME   NULL,
    [Createby] NCHAR (10) NULL,
    [Updateon] DATETIME   NULL,
    [Updateby] NCHAR (10) NULL
);

