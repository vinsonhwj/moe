﻿CREATE TABLE [dbo].[NestleExport] (
    [CompanyCode] VARCHAR (2)   NOT NULL,
    [NesID]       NVARCHAR (10) NOT NULL,
    [StartDate]   DATETIME      NULL,
    [EndDate]     DATETIME      NULL,
    [ExportOn]    DATETIME      NULL,
    [CreateOn]    DATETIME      NULL,
    [CreateBy]    NCHAR (10)    NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NCHAR (10)    NULL,
    [VoidOn]      DATETIME      NULL,
    [VoidBy]      NCHAR (10)    NULL
);

