﻿CREATE TABLE [dbo].[eComAccount] (
    [CompID]          NCHAR (16)   NOT NULL,
    [accountID]       NCHAR (16)   NOT NULL,
    [PaymentCurrency] NCHAR (3)    NULL,
    [payCash]         NCHAR (1)    NULL,
    [payCredit]       NCHAR (1)    NULL,
    [payCreditCard]   NCHAR (1)    NULL,
    [CreditLimit]     NUMERIC (18) NULL,
    [Expiry]          DATETIME     NULL,
    [CreditTerm]      NUMERIC (18) NULL,
    [PMUpperLimit]    NUMERIC (18) NULL,
    [PMLowerLimit]    NUMERIC (18) NULL,
    [Remarks]         TEXT         NULL,
    [Createon]        DATETIME     NULL,
    [Createby]        NCHAR (10)   NULL,
    [Updateon]        DATETIME     NULL,
    [Updateby]        NCHAR (10)   NULL,
    [approved]        NCHAR (1)    NULL
);

