﻿CREATE TABLE [dbo].[Personal] (
    [COMPANYID]      NCHAR (2)  NOT NULL,
    [MembershipNo]   NCHAR (16) NOT NULL,
    [Type]           NCHAR (10) NOT NULL,
    [SeqNum]         NCHAR (3)  NULL,
    [Carrier1]       NCHAR (50) NULL,
    [Carrier2]       NCHAR (50) NULL,
    [Carrier3]       NCHAR (50) NULL,
    [Carrier4]       NCHAR (50) NULL,
    [Carrier5]       NCHAR (50) NULL,
    [MealPreference] NCHAR (4)  NULL,
    [SeatPreference] NCHAR (4)  NULL,
    [Createon]       DATETIME   NULL,
    [Createby]       NCHAR (10) NULL,
    [Updateon]       DATETIME   NULL,
    [Updateby]       NCHAR (10) NULL
);

