﻿CREATE TABLE [dbo].[VisaType] (
    [VisaCode]   NCHAR (10)     NOT NULL,
    [VisaType]   NVARCHAR (100) NULL,
    [CreateDate] DATETIME       NULL,
    [CreateBy]   NCHAR (20)     NULL
);

