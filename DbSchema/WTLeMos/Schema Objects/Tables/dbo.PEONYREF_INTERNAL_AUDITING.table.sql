﻿CREATE TABLE [dbo].[PEONYREF_INTERNAL_AUDITING] (
    [REFID]       NUMERIC (13)    IDENTITY (1, 1) NOT NULL,
    [COMPANYCODE] NVARCHAR (2)    NULL,
    [BKGREF]      NVARCHAR (10)   NULL,
    [STAFFCODE]   NCHAR (10)      NULL,
    [TEAMCODE]    NCHAR (5)       NULL,
    [ACTIONCODE]  NVARCHAR (5)    NULL,
    [CLTCODE]     NVARCHAR (25)   NULL,
    [SUPPCODE]    NVARCHAR (8)    NULL,
    [DocNum]      NVARCHAR (13)   NULL,
    [SellCurr]    NVARCHAR (3)    NULL,
    [SellAmt]     NUMERIC (16, 2) NULL,
    [SellTaxCurr] NVARCHAR (3)    NULL,
    [SellTaxAmt]  NUMERIC (16, 2) NULL,
    [CostCurr]    NVARCHAR (3)    NULL,
    [CostAmt]     NUMERIC (16, 2) NULL,
    [CostTaxCurr] NVARCHAR (3)    NULL,
    [CostTaxAmt]  NUMERIC (16, 2) NULL,
    [Description] NVARCHAR (100)  NULL,
    [CREATEON]    DATETIME        NULL,
    [CREATEBY]    NVARCHAR (10)   NULL,
    [InvTypeCode] NVARCHAR (4)    NULL
);

