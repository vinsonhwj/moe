﻿ALTER TABLE [dbo].[PassportInfo]
    ADD CONSTRAINT [PK_PassportInfo] PRIMARY KEY NONCLUSTERED ([COMPANYID] ASC, [MembershipNo] ASC, [Seqno] ASC);

