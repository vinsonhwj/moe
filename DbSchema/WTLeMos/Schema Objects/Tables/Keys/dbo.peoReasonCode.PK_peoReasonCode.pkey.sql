﻿ALTER TABLE [dbo].[peoReasonCode]
    ADD CONSTRAINT [PK_peoReasonCode] PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [ReasonCode] ASC, [ReasonType] ASC);

