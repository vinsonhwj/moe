﻿ALTER TABLE [dbo].[PrinterMapping]
    ADD CONSTRAINT [PK_PrinterMapping] PRIMARY KEY CLUSTERED ([PrinterID] ASC, [CompanyCode] ASC, [Login] ASC);

