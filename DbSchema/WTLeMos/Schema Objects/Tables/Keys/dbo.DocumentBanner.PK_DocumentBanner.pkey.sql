﻿ALTER TABLE [dbo].[DocumentBanner]
    ADD CONSTRAINT [PK_DocumentBanner] PRIMARY KEY CLUSTERED ([companycode] ASC, [bkgref] ASC, [docnum] ASC);

