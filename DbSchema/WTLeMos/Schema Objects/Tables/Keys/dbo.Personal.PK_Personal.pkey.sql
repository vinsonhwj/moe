﻿ALTER TABLE [dbo].[Personal]
    ADD CONSTRAINT [PK_Personal] PRIMARY KEY NONCLUSTERED ([COMPANYID] ASC, [MembershipNo] ASC, [Type] ASC);

