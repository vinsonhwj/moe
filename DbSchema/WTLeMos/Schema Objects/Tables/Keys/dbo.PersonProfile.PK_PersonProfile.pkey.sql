﻿ALTER TABLE [dbo].[PersonProfile]
    ADD CONSTRAINT [PK_PersonProfile] PRIMARY KEY NONCLUSTERED ([CompanyID] ASC, [MemberShipNo] ASC);

