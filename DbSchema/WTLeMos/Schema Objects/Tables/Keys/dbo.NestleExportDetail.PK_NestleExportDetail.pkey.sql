﻿ALTER TABLE [dbo].[NestleExportDetail]
    ADD CONSTRAINT [PK_NestleExportDetail] PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [NesID] ASC, [SeqNo] ASC);

