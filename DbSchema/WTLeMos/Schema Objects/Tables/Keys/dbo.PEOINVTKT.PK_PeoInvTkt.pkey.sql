﻿ALTER TABLE [dbo].[PEOINVTKT]
    ADD CONSTRAINT [PK_PeoInvTkt] PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [Ticket] ASC, [TktSeq] ASC, [InvNum] ASC);

