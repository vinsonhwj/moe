﻿ALTER TABLE [dbo].[CompanySysEncode]
    ADD CONSTRAINT [PK_CompanySysEncode] PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [Encoding] ASC);

