﻿ALTER TABLE [dbo].[teamcrs]
    ADD CONSTRAINT [PK_teamcrs] PRIMARY KEY CLUSTERED ([companycode] ASC, [branchcode] ASC, [teamcode] ASC, [crs] ASC, [pseudo] ASC);

