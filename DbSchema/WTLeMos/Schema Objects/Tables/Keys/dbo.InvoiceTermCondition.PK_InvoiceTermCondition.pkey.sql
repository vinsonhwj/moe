﻿ALTER TABLE [dbo].[InvoiceTermCondition]
    ADD CONSTRAINT [PK_InvoiceTermCondition] PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [TemplateType] ASC, [Language] ASC);

