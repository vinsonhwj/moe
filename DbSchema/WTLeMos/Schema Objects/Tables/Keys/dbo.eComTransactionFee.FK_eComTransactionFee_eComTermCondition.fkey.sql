﻿ALTER TABLE [dbo].[eComTransactionFee]
    ADD CONSTRAINT [FK_eComTransactionFee_eComTermCondition] FOREIGN KEY ([termid]) REFERENCES [dbo].[eComTermCondition] ([termid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

