﻿ALTER TABLE [dbo].[eComCard]
    ADD CONSTRAINT [PK_eComCard] PRIMARY KEY CLUSTERED ([compID] ASC, [accountID] ASC, [typeID] ASC, [cardID] ASC);

