﻿CREATE TABLE [dbo].[PEOINPAX] (
    [COMPANYCODE] NVARCHAR (2)   NOT NULL,
    [INVNUM]      NVARCHAR (10)  NOT NULL,
    [SEQNUM]      NVARCHAR (5)   NOT NULL,
    [PAXSEQ]      SMALLINT       NOT NULL,
    [PAXNAME]     NVARCHAR (100) NULL,
    [INVTYPE]     NVARCHAR (1)   NULL,
    [curr]        NVARCHAR (3)   NULL,
    [sell]        DECIMAL (18)   NULL
);

