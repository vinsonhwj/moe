﻿CREATE TABLE [dbo].[peoPackage] (
    [CompanyCode]  NVARCHAR (2)   NULL,
    [BkgRef]       NVARCHAR (10)  NULL,
    [Airline]      VARCHAR (2)    NULL,
    [Destination1] VARCHAR (3)    NULL,
    [Destination2] VARCHAR (3)    NULL,
    [DepartDate]   DATETIME       NULL,
    [ReturnDate]   DATETIME       NULL,
    [NoOfDay]      INT            NULL,
    [NoOfNight]    INT            NULL,
    [PackageDesc1] NVARCHAR (100) NULL,
    [PackageDesc2] NVARCHAR (100) NULL,
    [CreateOn]     DATETIME       NULL,
    [CreateBy]     VARCHAR (10)   NULL,
    [UpdateOn]     DATETIME       NULL,
    [UpdateBy]     VARCHAR (5)    NULL,
    [IsEnable]     VARCHAR (1)    NULL
);

