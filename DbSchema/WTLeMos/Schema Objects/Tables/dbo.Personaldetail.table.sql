﻿CREATE TABLE [dbo].[Personaldetail] (
    [COMPANYID]    NCHAR (2)  NOT NULL,
    [MembershipNo] NCHAR (16) NOT NULL,
    [Type]         NCHAR (10) NOT NULL,
    [SeqNum]       NCHAR (3)  NOT NULL,
    [Flyercard]    NCHAR (30) NULL,
    [Flyercarrier] NCHAR (2)  NULL,
    [Flyernum]     NCHAR (30) NULL,
    [Parter]       NCHAR (30) NULL,
    [Expdate]      DATETIME   NULL,
    [Createon]     DATETIME   NULL,
    [Createby]     NCHAR (10) NULL,
    [Updateon]     DATETIME   NULL,
    [Updateby]     NCHAR (10) NULL
);

