﻿CREATE TABLE [dbo].[Customer] (
    [COMPANYCODE] NCHAR (2)      NOT NULL,
    [CLTCODE]     NCHAR (8)      NOT NULL,
    [CLTNAME]     NVARCHAR (100) NULL,
    [BRANCHCODE]  NCHAR (1)      NOT NULL,
    [TEAMCODE]    NCHAR (5)      NOT NULL,
    [MSTRCLT]     NCHAR (8)      NULL,
    [ADDR1]       NVARCHAR (50)  NULL,
    [ADDR2]       NVARCHAR (50)  NULL,
    [ADDR3]       NVARCHAR (50)  NULL,
    [ADDR4]       NVARCHAR (50)  NULL,
    [ADDR5]       NVARCHAR (50)  NULL,
    [POSTAL]      NVARCHAR (10)  NULL,
    [CITY]        NVARCHAR (3)   NULL,
    [COUNTRY]     NVARCHAR (3)   NULL,
    [BILLADDR1]   NVARCHAR (50)  NULL,
    [BILLADDR2]   NVARCHAR (50)  NULL,
    [BILLADDR3]   NVARCHAR (50)  NULL,
    [BILLADDR4]   NVARCHAR (50)  NULL,
    [BILLADDR5]   NVARCHAR (50)  NULL,
    [BILLPOSTAL]  NVARCHAR (10)  NULL,
    [BILLCITY]    NVARCHAR (3)   NULL,
    [BILLCOUNTRY] NVARCHAR (3)   NULL,
    [STATUS]      NCHAR (10)     NULL,
    [CONTACT1]    NVARCHAR (50)  NULL,
    [CONTACT2]    NVARCHAR (50)  NULL,
    [JOBTITLE1]   NVARCHAR (30)  NULL,
    [JOBTITLE2]   NVARCHAR (30)  NULL,
    [PHONE1]      NVARCHAR (20)  NULL,
    [FAX1]        NVARCHAR (16)  NULL,
    [CRTERMS]     NVARCHAR (50)  NULL,
    [PRFTUP]      NUMERIC (5, 1) NULL,
    [PRFTDOWN]    NUMERIC (5, 1) NULL,
    [CURRENCY]    NCHAR (3)      NULL,
    [PFDAY]       NUMERIC (3)    NULL,
    [CREATEON]    DATETIME       NULL,
    [CREATEBY]    NVARCHAR (10)  NULL,
    [UPDATEON]    DATETIME       NULL,
    [UPDATEBY]    NVARCHAR (10)  NULL,
    [email]       NVARCHAR (80)  NULL,
    [CustRemarks] NVARCHAR (300) NULL,
    [GEMSCODE]    NVARCHAR (10)  NULL
);

