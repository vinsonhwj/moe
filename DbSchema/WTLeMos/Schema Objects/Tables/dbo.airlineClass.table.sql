﻿CREATE TABLE [dbo].[airlineClass] (
    [COMPANYCODE] NVARCHAR (2)  NOT NULL,
    [AIRCODE]     NCHAR (2)     NOT NULL,
    [CLASSCODE]   NCHAR (2)     NOT NULL,
    [CLASSNAME]   NVARCHAR (50) NULL,
    [FareType]    CHAR (1)      NULL
);

