﻿CREATE TABLE [dbo].[automail_reportlist] (
    [COMPANYCODE]    NCHAR (2)      NULL,
    [REPORTCODE]     NCHAR (4)      NULL,
    [REPORTNAME]     NVARCHAR (200) NULL,
    [REPORTFILENAME] NCHAR (100)    NULL,
    [STATUS]         INT            NULL
);

