﻿CREATE TABLE [dbo].[CrApproval] (
    [CompanyCode]   NCHAR (2)  NOT NULL,
    [ApprovalCode]  NCHAR (3)  NOT NULL,
    [CodeDesc]      NCHAR (20) NULL,
    [ApprovalPwd]   NCHAR (10) NOT NULL,
    [ApprovalLevel] NCHAR (1)  NOT NULL,
    [CreateOn]      DATETIME   NULL,
    [CreateBy]      NCHAR (10) NULL,
    [EffectiveOn]   DATETIME   NULL,
    [ExpiredOn]     DATETIME   NULL,
    [UpdateOn]      DATETIME   NULL,
    [UpdateBy]      NCHAR (10) NULL
);

