﻿CREATE TABLE [dbo].[peoInvRept] (
    [CompanyCode] VARCHAR (2)     NOT NULL,
    [ReptNum]     VARCHAR (10)    NOT NULL,
    [InvNum]      VARCHAR (10)    NULL,
    [ReptDate]    DATETIME        NULL,
    [Payer]       NVARCHAR (50)   NULL,
    [PaidAmt]     DECIMAL (16, 2) NULL,
    [PayMethod]   VARCHAR (4)     NULL,
    [ChqNum]      NVARCHAR (100)  NULL,
    [Voidon]      DATETIME        NULL,
    [Voidby]      VARCHAR (10)    NULL,
    [ReptType]    VARCHAR (5)     NULL,
    [ReportDate]  DATETIME        NULL,
    [VOID2JBA]    VARCHAR (10)    NULL,
    [BalanceAmt]  DECIMAL (16, 2) NULL,
    [PrintTeam]   NVARCHAR (3)    NULL,
    [IncludeTIC]  VARCHAR (1)     NULL,
    [CreateBy]    VARCHAR (5)     NULL,
    [CreateOn]    DATETIME        NULL,
    [TICAmt]      DECIMAL (16, 2) NULL
);

