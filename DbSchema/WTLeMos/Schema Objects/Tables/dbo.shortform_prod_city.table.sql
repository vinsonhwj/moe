﻿CREATE TABLE [dbo].[shortform_prod_city] (
    [code]              VARCHAR (3)    NOT NULL,
    [name_e]            VARCHAR (255)  NOT NULL,
    [name_c]            NVARCHAR (255) NULL,
    [name_c1]           NVARCHAR (255) NULL,
    [country_code]      VARCHAR (50)   NOT NULL,
    [airport_city_code] VARCHAR (3)    NULL,
    [system]            TINYINT        NOT NULL,
    [DATA_EXTRA]        NVARCHAR (255) NULL,
    [modify_time]       DATETIME       NOT NULL,
    [VERSION_EXTRA]     NVARCHAR (255) NULL
);

