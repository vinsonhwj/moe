﻿CREATE TABLE [dbo].[eComCard] (
    [compID]     NCHAR (16)   NOT NULL,
    [accountID]  NCHAR (16)   NOT NULL,
    [typeID]     NCHAR (10)   NOT NULL,
    [cardID]     NUMERIC (18) NOT NULL,
    [cardNo]     NCHAR (50)   NULL,
    [Expiry]     DATETIME     NULL,
    [cardHolder] NCHAR (30)   NULL,
    [isBTA]      NCHAR (1)    NULL,
    [Createon]   DATETIME     NULL,
    [Createby]   NCHAR (10)   NULL,
    [Updateon]   DATETIME     NULL,
    [Updateby]   NCHAR (10)   NULL
);

