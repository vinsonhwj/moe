﻿CREATE TABLE [dbo].[PasswordHistory] (
    [CompanyCode]    VARCHAR (2)    NOT NULL,
    [Login]          VARCHAR (10)   NOT NULL,
    [ChangePassword] NVARCHAR (100) NOT NULL,
    [ChangeOn]       DATETIME       NOT NULL,
    [ChangeBy]       VARCHAR (10)   NOT NULL
);

