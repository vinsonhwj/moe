﻿CREATE TABLE [dbo].[InvoiceTermCondition] (
    [CompanyCode]     VARCHAR (2)     NOT NULL,
    [TemplateType]    NVARCHAR (25)   NOT NULL,
    [Language]        VARCHAR (5)     NOT NULL,
    [TemplateContent] NVARCHAR (2000) NOT NULL,
    [CreateOn]        DATETIME        NOT NULL,
    [CreateBy]        VARCHAR (10)    NOT NULL,
    [UpdateOn]        DATETIME        NOT NULL,
    [UpdateBy]        VARCHAR (10)    NOT NULL
);

