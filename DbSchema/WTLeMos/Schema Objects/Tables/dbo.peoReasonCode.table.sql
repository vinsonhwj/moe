﻿CREATE TABLE [dbo].[peoReasonCode] (
    [CompanyCode] NVARCHAR (2)   NOT NULL,
    [ReasonCode]  NVARCHAR (2)   NOT NULL,
    [ReasonType]  NVARCHAR (1)   NOT NULL,
    [Description] NVARCHAR (200) NULL,
    [CreateOn]    DATETIME       NOT NULL,
    [CreateBy]    NCHAR (10)     NOT NULL,
    [UpdateOn]    DATETIME       NOT NULL,
    [UpdateBy]    NCHAR (10)     NOT NULL
);

