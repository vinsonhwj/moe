﻿CREATE TABLE [dbo].[Airline] (
    [AIRLINE]             NCHAR (3)     NOT NULL,
    [AIRCODE]             NCHAR (2)     NOT NULL,
    [AIRLINENAME]         NVARCHAR (70) NULL,
    [AIRLINENAME_OTHLANG] NCHAR (70)    NULL,
    [CREATEON]            DATETIME      NULL,
    [CREATEBY]            NCHAR (10)    NULL,
    [UPDATEON]            DATETIME      NULL,
    [UPDATEBY]            NCHAR (10)    NULL
);

