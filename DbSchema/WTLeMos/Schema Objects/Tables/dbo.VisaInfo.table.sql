﻿CREATE TABLE [dbo].[VisaInfo] (
    [CompanyID]     NCHAR (2)     NOT NULL,
    [MembershipNo]  NCHAR (16)    NOT NULL,
    [PassportSeqNo] NCHAR (3)     NOT NULL,
    [SeqNo]         NCHAR (3)     NOT NULL,
    [VisaType]      NCHAR (40)    NULL,
    [CountryCode]   NCHAR (2)     NULL,
    [DocumentName]  NCHAR (100)   NULL,
    [ExpiryDate]    DATETIME      NULL,
    [Remarks]       NVARCHAR (50) NULL,
    [UpdateDate]    DATETIME      NULL,
    [UpdateBy]      NCHAR (20)    NULL,
    [CreateDate]    DATETIME      NULL,
    [CreateBy]      NCHAR (20)    NULL
);

