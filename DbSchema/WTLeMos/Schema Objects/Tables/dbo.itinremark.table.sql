﻿CREATE TABLE [dbo].[itinremark] (
    [CompanyCode] NCHAR (2)     NOT NULL,
    [BkgRef]      NVARCHAR (10) NOT NULL,
    [seqnum]      NUMERIC (18)  NOT NULL,
    [segtype]     NVARCHAR (10) NOT NULL,
    [remark]      NVARCHAR (50) NULL,
    [CreateOn]    DATETIME      NULL,
    [CreateBy]    NCHAR (10)    NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NCHAR (10)    NULL
);

