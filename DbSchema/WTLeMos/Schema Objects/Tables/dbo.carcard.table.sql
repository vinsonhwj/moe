﻿CREATE TABLE [dbo].[carcard] (
    [COMPANYID]    CHAR (2)  NOT NULL,
    [MembershipNo] CHAR (16) NOT NULL,
    [SeqNum]       CHAR (3)  NOT NULL,
    [Ccompany]     CHAR (30) NULL,
    [Vcode]        CHAR (2)  NULL,
    [Cnum]         CHAR (30) NULL,
    [Expdate]      DATETIME  NULL,
    [Createon]     DATETIME  NULL,
    [Createby]     CHAR (10) NULL,
    [Updateon]     DATETIME  NULL,
    [Updateby]     CHAR (10) NULL
);

