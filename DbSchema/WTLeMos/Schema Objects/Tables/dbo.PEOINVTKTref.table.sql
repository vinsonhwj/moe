﻿CREATE TABLE [dbo].[PEOINVTKTref] (
    [CompanyCode] NVARCHAR (2)  NOT NULL,
    [Ticket]      NVARCHAR (10) NOT NULL,
    [TktSeq]      NVARCHAR (10) NOT NULL,
    [BkgRef]      NVARCHAR (10) NOT NULL,
    [InvNum]      NVARCHAR (10) NOT NULL,
    [CreateOn]    DATETIME      NULL,
    [CreateBy]    NVARCHAR (10) NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NVARCHAR (10) NULL,
    [TicketType]  VARCHAR (3)   DEFAULT ('') NULL
);

