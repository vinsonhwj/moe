﻿CREATE TABLE [dbo].[country] (
    [SOURCECODE]  CHAR (3)  NOT NULL,
    [REGIONCODE]  CHAR (10) NOT NULL,
    [COUNTRYCODE] CHAR (3)  NOT NULL,
    [COUNTRYNAME] CHAR (40) NOT NULL,
    [CREATEON]    DATETIME  NULL,
    [CREATEBY]    CHAR (10) NULL,
    [UPDATEON]    DATETIME  NULL,
    [UPDATEBY]    CHAR (10) NULL
);

