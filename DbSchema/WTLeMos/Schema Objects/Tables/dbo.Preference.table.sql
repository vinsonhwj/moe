﻿CREATE TABLE [dbo].[Preference] (
    [COMPANYID]   NCHAR (2)  NOT NULL,
    [Type]        NCHAR (10) NOT NULL,
    [ID]          NCHAR (4)  NOT NULL,
    [Description] NCHAR (50) NULL,
    [Createon]    DATETIME   NULL,
    [Creatby]     NCHAR (10) NULL,
    [Updateon]    DATETIME   NULL,
    [Updatby]     NCHAR (10) NULL
);

