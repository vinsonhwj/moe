﻿CREATE TABLE [dbo].[AgentHotDetail] (
    [CompanyCode]                          VARCHAR (2)     NOT NULL,
    [Ticket]                               VARCHAR (10)    NOT NULL,
    [AirCode]                              VARCHAR (5)     NOT NULL,
    [SeqNo]                                VARCHAR (5)     NOT NULL,
    [TxnCode]                              VARCHAR (4)     NULL,
    [Conj]                                 CHAR (3)        NULL,
    [NetFare]                              DECIMAL (16, 2) NULL,
    [Tax_ID1]                              VARCHAR (8)     NULL,
    [Tax_Amt1]                             DECIMAL (16, 2) NULL,
    [Tax_ID2]                              VARCHAR (8)     NULL,
    [Tax_Amt2]                             DECIMAL (16, 2) NULL,
    [Tax_ID3]                              VARCHAR (8)     NULL,
    [Tax_Amt3]                             DECIMAL (16, 2) NULL,
    [Tax_ID4]                              VARCHAR (8)     NULL,
    [Tax_Amt4]                             DECIMAL (16, 2) NULL,
    [Tax_ID5]                              VARCHAR (8)     NULL,
    [Tax_Amt5]                             DECIMAL (16, 2) NULL,
    [Tax_ID6]                              VARCHAR (8)     NULL,
    [Tax_Amt6]                             DECIMAL (16, 2) NULL,
    [Tax_ID7]                              VARCHAR (8)     NULL,
    [Tax_Amt7]                             DECIMAL (16, 2) NULL,
    [Tax_ID8]                              VARCHAR (8)     NULL,
    [Tax_Amt8]                             DECIMAL (16, 2) NULL,
    [Tax_ID9]                              VARCHAR (8)     NULL,
    [Tax_Amt9]                             DECIMAL (16, 2) NULL,
    [Tax_ID10]                             VARCHAR (8)     NULL,
    [Tax_Amt10]                            DECIMAL (16, 2) NULL,
    [Tax_ID11]                             VARCHAR (8)     NULL,
    [Tax_Amt11]                            DECIMAL (16, 2) NULL,
    [Tax_ID12]                             VARCHAR (8)     NULL,
    [Tax_Amt12]                            DECIMAL (16, 2) NULL,
    [Tax_ID13]                             VARCHAR (8)     NULL,
    [Tax_Amt13]                            DECIMAL (16, 2) NULL,
    [Tax_ID14]                             VARCHAR (8)     NULL,
    [Tax_Amt14]                            DECIMAL (16, 2) NULL,
    [Tax_ID15]                             VARCHAR (8)     NULL,
    [Tax_Amt15]                            DECIMAL (16, 2) NULL,
    [Tax_ID16]                             VARCHAR (8)     NULL,
    [Tax_Amt16]                            DECIMAL (16, 2) NULL,
    [Seg_DepartCity_1]                     VARCHAR (5)     NULL,
    [Seg_DestinationCity_1]                VARCHAR (5)     NULL,
    [Seg_Airline_1]                        VARCHAR (4)     NULL,
    [Seg_Flyer_1]                          VARCHAR (16)    NULL,
    [Seg_Flight_Date_1]                    DATETIME        NULL,
    [Seg_Flight_1]                         VARCHAR (5)     NULL,
    [Seg_Stopover_1]                       VARCHAR (1)     NULL,
    [Seg_Class_1]                          VARCHAR (2)     NULL,
    [Seg_DepartCity_2]                     VARCHAR (5)     NULL,
    [Seg_DestinationCity_2]                VARCHAR (5)     NULL,
    [Seg_Airline_2]                        VARCHAR (4)     NULL,
    [Seg_Flyer_2]                          VARCHAR (16)    NULL,
    [Seg_Flight_Date_2]                    DATETIME        NULL,
    [Seg_Flight_2]                         VARCHAR (5)     NULL,
    [Seg_Stopover_2]                       VARCHAR (1)     NULL,
    [Seg_Class_2]                          VARCHAR (2)     NULL,
    [Seg_DepartCity_3]                     VARCHAR (5)     NULL,
    [Seg_DestinationCity_3]                VARCHAR (5)     NULL,
    [Seg_Airline_3]                        VARCHAR (4)     NULL,
    [Seg_Flyer_3]                          VARCHAR (16)    NULL,
    [Seg_Flight_Date_3]                    DATETIME        NULL,
    [Seg_Flight_3]                         VARCHAR (5)     NULL,
    [Seg_Stopover_3]                       VARCHAR (1)     NULL,
    [Seg_Class_3]                          VARCHAR (2)     NULL,
    [Seg_DepartCity_4]                     VARCHAR (5)     NULL,
    [Seg_DestinationCity_4]                VARCHAR (5)     NULL,
    [Seg_Airline_4]                        VARCHAR (4)     NULL,
    [Seg_Flyer_4]                          VARCHAR (16)    NULL,
    [Seg_Flight_Date_4]                    DATETIME        NULL,
    [Seg_Flight_4]                         VARCHAR (5)     NULL,
    [Seg_Stopover_4]                       VARCHAR (1)     NULL,
    [Seg_Class_4]                          VARCHAR (2)     NULL,
    [FormOfPayment_Acc_No_1]               VARCHAR (19)    NULL,
    [FormOfPayment_Payment_Type_1]         VARCHAR (10)    NULL,
    [FormOfPayment_Curr_1]                 VARCHAR (4)     NULL,
    [FormOfPayment_Amt_1]                  DECIMAL (16, 2) NULL,
    [FormOfPayment_Approval_Code_1]        VARCHAR (6)     NULL,
    [FormOfPayment_Approval_Expiry_Date_1] VARCHAR (4)     NULL,
    [FormOfPayment_Acc_No_2]               VARCHAR (19)    NULL,
    [FormOfPayment_Payment_Type_2]         VARCHAR (10)    NULL,
    [FormOfPayment_Curr_2]                 VARCHAR (4)     NULL,
    [FormOfPayment_Amt_2]                  DECIMAL (16, 2) NULL,
    [FormOfPayment_Approval_Code_2]        VARCHAR (6)     NULL,
    [FormOfPayment_Approval_Expiry_Date_2] VARCHAR (4)     NULL,
    [FormOfPayment_Acc_No_3]               VARCHAR (19)    NULL,
    [FormOfPayment_Payment_Type_3]         VARCHAR (10)    NULL,
    [FormOfPayment_Curr_3]                 VARCHAR (4)     NULL,
    [FormOfPayment_Amt_3]                  DECIMAL (16, 2) NULL,
    [FormOfPayment_Approval_Code_3]        VARCHAR (6)     NULL,
    [FormOfPayment_Approval_Expiry_Date_3] VARCHAR (4)     NULL,
    [CreateOn]                             DATETIME        NOT NULL,
    [CreateBy]                             VARCHAR (10)    NOT NULL,
    [LogNo]                                VARCHAR (26)    NOT NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [Ticket] ASC, [AirCode] ASC, [SeqNo] ASC)
);

