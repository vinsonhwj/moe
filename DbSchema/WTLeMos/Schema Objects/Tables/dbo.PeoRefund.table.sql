﻿CREATE TABLE [dbo].[PeoRefund] (
    [Companycode] CHAR (2)        NOT NULL,
    [Refundno]    CHAR (10)       NOT NULL,
    [bkgref]      CHAR (10)       NULL,
    [Handledby]   CHAR (20)       NULL,
    [refundcurr]  CHAR (3)        NULL,
    [refundamt]   DECIMAL (18, 2) NULL,
    [penaltycurr] CHAR (3)        NULL,
    [penalty]     DECIMAL (18, 2) NULL,
    [createon]    DATETIME        NULL,
    [createby]    CHAR (10)       NULL,
    [updateon]    DATETIME        NULL,
    [updateby]    CHAR (10)       NULL
);

