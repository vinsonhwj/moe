﻿CREATE TABLE [dbo].[PersonalInterest] (
    [COMPANYID]          NCHAR (2)      NOT NULL,
    [MembershipNo]       NCHAR (16)     NOT NULL,
    [DestinationsCode]   NVARCHAR (70)  NULL,
    [MeansId]            NVARCHAR (100) NULL,
    [InterestId]         NVARCHAR (100) NULL,
    [TypeOfTravel]       NCHAR (30)     NULL,
    [TypeOfTravelOthers] NCHAR (30)     NULL,
    [TravelAYear]        INT            NULL,
    [Education]          INT            NULL,
    [MonthlyIncome]      INT            NULL,
    [LearnFrom]          INT            NULL,
    [CreateOn]           DATETIME       NULL,
    [CreateBy]           NCHAR (10)     NULL,
    [UpdateOn]           DATETIME       NULL,
    [UpdateBy]           NCHAR (10)     NULL,
    [DestCountry1]       NCHAR (2)      NULL,
    [DestCity1]          NCHAR (3)      NULL,
    [DestCountry2]       NCHAR (2)      NULL,
    [DestCity2]          NCHAR (3)      NULL,
    [DestCountry3]       NCHAR (2)      NULL,
    [DestCity3]          NCHAR (3)      NULL
);

