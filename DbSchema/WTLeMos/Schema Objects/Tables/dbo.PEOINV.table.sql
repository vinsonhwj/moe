﻿CREATE TABLE [dbo].[PEOINV] (
    [COMPANYCODE]      NVARCHAR (2)    NOT NULL,
    [INVNUM]           NVARCHAR (10)   NOT NULL,
    [BKGREF]           NVARCHAR (10)   NOT NULL,
    [PNR]              NVARCHAR (6)    NULL,
    [CLTCODE]          NVARCHAR (8)    NULL,
    [CLTNAME]          NVARCHAR (40)   NULL,
    [CLTADDR]          NVARCHAR (160)  NULL,
    [PHONE]            NVARCHAR (20)   NULL,
    [FAX]              NVARCHAR (20)   NULL,
    [STAFFCODE]        NVARCHAR (10)   NULL,
    [TEAMCODE]         NVARCHAR (5)    NULL,
    [CRTERMS]          NVARCHAR (50)   NULL,
    [SELLCURR]         NVARCHAR (3)    NULL,
    [SELLAMT]          DECIMAL (16, 2) NULL,
    [TAXCURR]          NVARCHAR (3)    NULL,
    [TAXAMT]           DECIMAL (16, 2) NULL,
    [HKDAMT]           DECIMAL (16, 2) NULL,
    [ANALYSISCODE]     NVARCHAR (20)   NULL,
    [CUSTREF]          NVARCHAR (47)   NULL,
    [CONTACTPERSON]    NVARCHAR (40)   NULL,
    [RMK1]             NVARCHAR (50)   NULL,
    [RMK2]             NVARCHAR (50)   NULL,
    [RMK3]             NVARCHAR (50)   NULL,
    [RMK4]             NVARCHAR (50)   NULL,
    [RMK5]             NVARCHAR (50)   NULL,
    [RMK6]             NVARCHAR (50)   NULL,
    [JBASESSION]       NVARCHAR (8)    NULL,
    [JBADATE]          NVARCHAR (10)   NULL,
    [PAYMENT]          NVARCHAR (3)    NULL,
    [INVTYPE]          NVARCHAR (3)    NULL,
    [PAYDESC]          NVARCHAR (30)   NULL,
    [INVFORMAT]        NVARCHAR (1)    NULL,
    [PrintTeam]        NVARCHAR (5)    NULL,
    [VOIDON]           DATETIME        NULL,
    [VOIDBY]           NVARCHAR (10)   NULL,
    [VoidTeam]         NVARCHAR (5)    NULL,
    [VoidREASON]       NVARCHAR (60)   NULL,
    [VOID2JBA]         NVARCHAR (10)   NULL,
    [ARPurgeRef]       NVARCHAR (10)   NULL,
    [CREATEON]         DATETIME        NULL,
    [CREATEBY]         NVARCHAR (10)   NULL,
    [UPDATEON]         DATETIME        NULL,
    [UPDATEBY]         NVARCHAR (10)   NULL,
    [ATTN]             NVARCHAR (60)   NULL,
    [COD1]             NVARCHAR (20)   NULL,
    [COD2]             NVARCHAR (20)   NULL,
    [COD3]             NVARCHAR (20)   NULL,
    [CR_card_holder]   NVARCHAR (100)  NULL,
    [CR_card_no]       NVARCHAR (30)   NULL,
    [CR_card_type]     NVARCHAR (10)   NULL,
    [CR_card_expiry]   DATETIME        NULL,
    [CR_amt]           DECIMAL (18, 2) NULL,
    [CR_curr]          NVARCHAR (3)    NULL,
    [CR_approval_code] NVARCHAR (10)   NULL,
    [CR_ta]            NVARCHAR (15)   NULL,
    [RECINV]           NCHAR (3)       NOT NULL,
    [PAYER]            NVARCHAR (50)   NULL,
    [CHQ_ISSUEDATE]    DATETIME        NULL,
    [BANKCODE]         NVARCHAR (50)   NULL,
    [PAY_RMK]          NVARCHAR (100)  NULL,
    [ChqNo]            NVARCHAR (10)   NULL,
    [gstpct]           DECIMAL (18, 2) NULL,
    [gstamt]           DECIMAL (18, 2) NULL,
    [SellExcludeTax]   DECIMAL (18, 2) NULL,
    [mstrinvnum]       NVARCHAR (10)   NULL,
    [invdtcurr]        NVARCHAR (3)    NULL,
    [invdtsum]         DECIMAL (18, 2) NULL,
    [CNReason]         CHAR (60)       NULL,
    [InvTC]            NVARCHAR (2000) NULL,
    [DisCreditInfo]    NVARCHAR (1)    NULL,
    [IsHidebreakdown]  NVARCHAR (1)    NULL,
    [IsPackage]        VARCHAR (1)     NULL,
    [PackageDesc1]     NVARCHAR (100)  NULL,
    [PackageDesc2]     NVARCHAR (100)  NULL,
    [IsMICE]           CHAR (1)        NULL,
    [MICEDesc]         NVARCHAR (100)  NULL,
    [IsBkgClass]       CHAR (1)        NULL,
    [IsSendEmailNow]   CHAR (1)        NULL
);

