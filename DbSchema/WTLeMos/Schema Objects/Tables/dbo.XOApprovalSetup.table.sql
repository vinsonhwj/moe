﻿CREATE TABLE [dbo].[XOApprovalSetup] (
    [CompanyCode]   NVARCHAR (2)    NULL,
    [ApprovalLevel] INT             NULL,
    [SelfType]      VARCHAR (1)     NULL,
    [SelfValue]     DECIMAL (16, 2) NULL,
    [AppType]       VARCHAR (1)     NULL,
    [AppValue]      DECIMAL (16, 2) NULL,
    [CreateOn]      DATETIME        NULL,
    [CreateBy]      VARCHAR (5)     NULL,
    [UpdateOn]      DATETIME        NULL,
    [UpdateBy]      VARCHAR (5)     NULL
);

