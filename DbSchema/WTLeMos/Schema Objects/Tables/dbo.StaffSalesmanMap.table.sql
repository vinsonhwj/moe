﻿CREATE TABLE [dbo].[StaffSalesmanMap] (
    [CompanyCode] NCHAR (2)      NOT NULL,
    [Login]       NVARCHAR (20)  NOT NULL,
    [Salesman]    NVARCHAR (200) NOT NULL,
    [CREATEON]    DATETIME       NOT NULL,
    [CREATEBY]    NVARCHAR (20)  NOT NULL,
    [UPDATEON]    DATETIME       NULL,
    [UPDATEBY]    NVARCHAR (20)  NOT NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [Login] ASC, [Salesman] ASC)
);

