﻿CREATE TABLE [dbo].[ViewOther] (
    [Companycode] NVARCHAR (2)  NOT NULL,
    [Login]       NVARCHAR (10) NOT NULL,
    [ViewCompany] NVARCHAR (2)  NULL,
    [ViewBranch]  NVARCHAR (1)  NULL,
    [ViewTeam]    NVARCHAR (5)  NULL,
    [CreateOn]    DATETIME      NULL,
    [CreateBy]    NVARCHAR (10) NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NVARCHAR (10) NULL
);

