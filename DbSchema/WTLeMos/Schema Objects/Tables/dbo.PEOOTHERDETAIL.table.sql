﻿CREATE TABLE [dbo].[PEOOTHERDETAIL] (
    [BKGREF]       NVARCHAR (10)   NOT NULL,
    [COMPANYCODE]  NVARCHAR (2)    NOT NULL,
    [SEGNUM]       NVARCHAR (5)    NOT NULL,
    [SEQNUM]       NVARCHAR (5)    NOT NULL,
    [SEQTYPE]      NVARCHAR (5)    NULL,
    [AGETYPE]      NVARCHAR (6)    NULL,
    [QTY]          INT             NULL,
    [CURR]         NCHAR (3)       NULL,
    [AMT]          DECIMAL (16, 2) NULL,
    [TAXCURR]      NVARCHAR (3)    NULL,
    [TAXAMT]       DECIMAL (16, 2) NULL,
    [VOIDON]       DATETIME        NULL,
    [VOIDBY]       NVARCHAR (10)   NULL,
    [SOURCESYSTEM] NVARCHAR (10)   NULL,
    [CREATEON]     DATETIME        NULL,
    [CREATEBY]     NVARCHAR (10)   NULL,
    [UPDATEON]     DATETIME        NULL,
    [UPDATEBY]     NVARCHAR (10)   NULL,
    [AMTFEE]       DECIMAL (18, 2) NULL,
    [gsttype]      NCHAR (2)       NULL
);

