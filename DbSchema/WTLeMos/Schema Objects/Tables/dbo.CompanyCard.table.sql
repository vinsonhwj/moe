﻿CREATE TABLE [dbo].[CompanyCard] (
    [CompanyCode]     VARCHAR (2)   NOT NULL,
    [CardType]        VARCHAR (2)   NOT NULL,
    [CardCode]        VARCHAR (10)  NOT NULL,
    [CardNo]          VARCHAR (20)  NOT NULL,
    [CardDescription] VARCHAR (100) NOT NULL,
    [deleted]         INT           NOT NULL,
    [CreateOn]        DATETIME      NOT NULL,
    [CreateBy]        VARCHAR (10)  NOT NULL,
    [UpdateOn]        DATETIME      NULL,
    [UpdateBy]        VARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [CardCode] ASC)
);

