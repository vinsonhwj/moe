﻿CREATE TABLE [dbo].[aircard] (
    [COMPANYID]    NCHAR (2)  NOT NULL,
    [MembershipNo] NCHAR (16) NOT NULL,
    [SeqNum]       NCHAR (3)  NOT NULL,
    [FFcard]       NCHAR (30) NULL,
    [Fcarrier]     NCHAR (2)  NULL,
    [FFnum]        NCHAR (30) NULL,
    [Partner]      NCHAR (30) NULL,
    [Expdate]      DATETIME   NULL,
    [Createon]     DATETIME   NULL,
    [Createby]     NCHAR (10) NULL,
    [Updateon]     DATETIME   NULL,
    [Updateby]     NCHAR (10) NULL
);

