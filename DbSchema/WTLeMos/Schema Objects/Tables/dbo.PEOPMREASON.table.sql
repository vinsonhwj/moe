﻿CREATE TABLE [dbo].[PEOPMREASON] (
    [Companycode]   CHAR (2)  NOT NULL,
    [ReasonCode]    CHAR (5)  NOT NULL,
    [ReasonDesc]    CHAR (50) NULL,
    [Mgr_deadline]  INT       NULL,
    [Acct_deadline] INT       NULL,
    [createon]      DATETIME  NULL,
    [createby]      CHAR (10) NULL,
    [updateon]      DATETIME  NULL,
    [updateby]      CHAR (10) NULL
);

