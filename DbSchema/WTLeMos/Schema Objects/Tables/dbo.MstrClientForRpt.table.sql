﻿CREATE TABLE [dbo].[MstrClientForRpt] (
    [CompanyCode]    NCHAR (2)  NOT NULL,
    [MstrClientCode] NCHAR (16) NOT NULL,
    [CltCode]        NCHAR (16) NOT NULL,
    [CreateOn]       DATETIME   NOT NULL,
    [CreateBy]       NCHAR (10) NOT NULL,
    [UpdateOn]       DATETIME   NOT NULL,
    [UpdateBy]       NCHAR (10) NOT NULL
);

