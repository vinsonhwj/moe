﻿CREATE TABLE [dbo].[automail_uninvundocwd] (
    [companycode] NCHAR (2)       NULL,
    [Branch]      NCHAR (1)       NULL,
    [Team]        NCHAR (5)       NULL,
    [Staff]       NCHAR (10)      NULL,
    [BKGREF]      NCHAR (10)      NULL,
    [Docdate]     DATETIME        NULL,
    [Docnum]      DECIMAL (16, 2) NULL,
    [Docamt]      DECIMAL (16, 2) NULL,
    [Cltname]     NCHAR (30)      NULL,
    [CltCode]     NCHAR (8)       NULL,
    [Invdate]     DATETIME        NULL,
    [Invnum]      NCHAR (10)      NULL,
    [Invamt]      DECIMAL (16, 2) NULL,
    [Void2JBA]    NCHAR (8)       NULL,
    [MasterPNR]   NVARCHAR (12)   NULL,
    [DepartDate]  DATETIME        NULL
);

