﻿CREATE TABLE [dbo].[PEOTRAVELTOOLS_EN_US] (
    [ToolsID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Companycode] NCHAR (2)     NOT NULL,
    [ToolsName]   NVARCHAR (50) NOT NULL,
    [ToolsURL]    NVARCHAR (50) NULL,
    [CreateOn]    DATETIME      NOT NULL,
    [CreateBy]    NVARCHAR (10) NOT NULL,
    [UpdateOn]    DATETIME      NOT NULL,
    [UpdateBy]    NVARCHAR (10) NOT NULL,
    [Disable]     NCHAR (1)     NULL
);

