﻿CREATE TABLE [dbo].[Itinerary] (
    [companycode] NVARCHAR (3)  NULL,
    [bkgref]      NVARCHAR (50) NULL,
    [genKey]      NVARCHAR (50) NULL,
    [createon]    DATETIME      NULL,
    [createby]    NVARCHAR (50) NULL,
    [updateon]    DATETIME      NULL,
    [updateup]    NVARCHAR (50) NULL
);

