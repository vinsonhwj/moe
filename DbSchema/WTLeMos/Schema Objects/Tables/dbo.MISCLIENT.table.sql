﻿CREATE TABLE [dbo].[MISCLIENT] (
    [WRSCLIENT]   CHAR (6)     NOT NULL,
    [BTICLIENT]   CHAR (8)     NOT NULL,
    [CreateOn]    DATETIME     NULL,
    [CreateBy]    CHAR (3)     NULL,
    [UpdateOn]    DATETIME     NULL,
    [UpdateBy]    CHAR (3)     NULL,
    [CompanyCode] CHAR (2)     DEFAULT ('WM') NOT NULL,
    [GEMSCODE3]   VARCHAR (20) NULL
);

