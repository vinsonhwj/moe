﻿CREATE TABLE [dbo].[peoMstr_CloseHistory] (
    [CompanyCode] NVARCHAR (2)    NULL,
    [MasterPnr]   NVARCHAR (10)   NULL,
    [TtlReptAmt]  DECIMAL (16, 2) NULL,
    [TtlReptTax]  DECIMAL (16, 2) NULL,
    [TtlDocAmt]   DECIMAL (16, 2) NULL,
    [TtlDocTax]   DECIMAL (16, 2) NULL,
    [ActionType]  VARCHAR (1)     NULL,
    [NetMoveAmt]  DECIMAL (16, 2) NULL,
    [NetMoveTax]  DECIMAL (16, 2) NULL,
    [IsLast]      VARCHAR (1)     NULL,
    [CreateOn]    DATETIME        NULL,
    [CreateBy]    VARCHAR (5)     NULL,
    [ActionOn]    DATETIME        NULL
);

