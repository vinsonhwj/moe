﻿CREATE TABLE [dbo].[tmp_booking_report] (
    [BkgRef]       NVARCHAR (50)   NULL,
    [BookingDate]  DATETIME        NULL,
    [BookingOwner] NVARCHAR (50)   NULL,
    [InvNum]       NVARCHAR (50)   NULL,
    [InvDate]      DATETIME        NULL,
    [InvAmt]       NUMERIC (18, 2) NULL,
    [CardCharge]   NUMERIC (18, 2) NULL,
    [TIC]          NUMERIC (18, 2) NULL,
    [DocNum]       NVARCHAR (50)   NULL,
    [DocDate]      DATETIME        NULL,
    [DocAmt]       NUMERIC (18, 2) NULL,
    [Profit]       NUMERIC (18, 2) NULL,
    [GP]           NUMERIC (18, 2) NULL,
    [SeqNum]       NVARCHAR (50)   NULL
);

