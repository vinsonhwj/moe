﻿CREATE TABLE [dbo].[crscode] (
    [crscode]     NVARCHAR (1)   NOT NULL,
    [description] NVARCHAR (100) NULL,
    [createon]    DATETIME       NULL,
    [createby]    NVARCHAR (10)  NULL,
    [updateon]    DATETIME       NULL,
    [updateby]    NVARCHAR (10)  NULL
);

