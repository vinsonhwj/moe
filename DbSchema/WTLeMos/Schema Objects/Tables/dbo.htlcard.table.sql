﻿CREATE TABLE [dbo].[htlcard] (
    [COMPANYID]    NCHAR (2)  NOT NULL,
    [MembershipNo] NCHAR (16) NOT NULL,
    [SeqNum]       NCHAR (3)  NOT NULL,
    [Cprogram]     NCHAR (30) NULL,
    [Ccode]        NCHAR (2)  NULL,
    [Cnum]         NCHAR (30) NULL,
    [Expdate]      DATETIME   NULL,
    [Createon]     DATETIME   NULL,
    [Createby]     NCHAR (10) NULL,
    [Updateon]     DATETIME   NULL,
    [Updateby]     NCHAR (10) NULL
);

