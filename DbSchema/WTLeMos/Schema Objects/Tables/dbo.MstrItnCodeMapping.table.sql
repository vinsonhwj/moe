﻿CREATE TABLE [dbo].[MstrItnCodeMapping] (
    [COMPANYCODE]    NCHAR (2)      NULL,
    [MstrClientCode] NCHAR (16)     NULL,
    [ItnCode]        NCHAR (3)      NULL,
    [OtherSegType]   NCHAR (6)      NULL,
    [Description]    NVARCHAR (100) NULL,
    [UpdateOn]       DATETIME       NULL,
    [UpdateBy]       NCHAR (10)     NULL
);

