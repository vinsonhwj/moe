﻿CREATE UNIQUE NONCLUSTERED INDEX [CreditCardInfo1]
    ON [dbo].[CreditCardInfo]([CompanyCode] ASC, [Recordtype] ASC, [Refcode] ASC, [Reftype] ASC, [Cardseq] ASC, [Cardtype] ASC, [Cardnum] ASC);

