﻿CREATE UNIQUE NONCLUSTERED INDEX [pbcatcol_idx]
    ON [dbo].[pbcatcol]([pbc_tnam] ASC, [pbc_ownr] ASC, [pbc_cnam] ASC);

