﻿CREATE TABLE [dbo].[VISA_EQUIRY] (
    [CompanyCode]  NVARCHAR (3)  NOT NULL,
    [bkgref]       NVARCHAR (15) NOT NULL,
    [segnum]       NVARCHAR (10) NOT NULL,
    [Visa_country] NVARCHAR (10) NOT NULL,
    [Passport]     NVARCHAR (50) NOT NULL,
    [Visa]         NCHAR (10)    NULL,
    [Visa_type]    NVARCHAR (50) NULL,
    [CreateOn]     DATETIME      NOT NULL,
    [CreateBy]     NVARCHAR (8)  NOT NULL,
    [UpdateOn]     DATETIME      NOT NULL,
    [UpdateBy]     NVARCHAR (8)  NOT NULL
);

