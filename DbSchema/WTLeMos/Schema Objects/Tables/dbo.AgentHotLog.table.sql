﻿CREATE TABLE [dbo].[AgentHotLog] (
    [CompanyCode]     VARCHAR (2)   NOT NULL,
    [LogNo]           VARCHAR (26)  NOT NULL,
    [ProcessFileName] VARCHAR (100) NOT NULL,
    [ProcessDesc]     VARCHAR (200) DEFAULT ('') NULL,
    [Remark]          VARCHAR (500) DEFAULT ('') NULL,
    [Status]          VARCHAR (10)  NOT NULL,
    [Period_Start]    DATETIME      NULL,
    [Period_End]      DATETIME      NULL,
    [CreateOn]        DATETIME      NULL,
    [CreateBy]        VARCHAR (10)  NOT NULL,
    PRIMARY KEY CLUSTERED ([CompanyCode] ASC, [LogNo] ASC)
);

