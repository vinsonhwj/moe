﻿CREATE TABLE [dbo].[PEOINVSEG] (
    [COMPANYCODE] NVARCHAR (2)    NOT NULL,
    [INVNUM]      NVARCHAR (10)   NOT NULL,
    [SEGNUM]      NVARCHAR (5)    NOT NULL,
    [SERVTYPE]    NVARCHAR (5)    NOT NULL,
    [SERVNAME]    NVARCHAR (100)  NULL,
    [SERVDESC]    NVARCHAR (1000) NULL,
    [CREATEON]    DATETIME        NULL,
    [CREATEBY]    NVARCHAR (10)   NULL,
    [GSTAMT]      DECIMAL (18, 2) NULL,
    [SEGTYPE]     NVARCHAR (3)    NULL,
    [Destination] NVARCHAR (3)    NULL,
    [ReasonCode]  NVARCHAR (2)    NULL,
    [WithBkf]     VARCHAR (1)     NULL,
    [WithTfr]     VARCHAR (1)     NULL,
    [GroupID]     INT             NOT NULL
);

