﻿CREATE TABLE [dbo].[eComTermCondition] (
    [compid]   NCHAR (16) NOT NULL,
    [termid]   NCHAR (16) NOT NULL,
    [TFRemark] TEXT       NULL,
    [ACRemark] TEXT       NULL,
    [MFRemark] TEXT       NULL,
    [Createon] DATETIME   NULL,
    [Createby] NCHAR (10) NULL,
    [Updateon] DATETIME   NULL,
    [Updateby] NCHAR (10) NULL
);

