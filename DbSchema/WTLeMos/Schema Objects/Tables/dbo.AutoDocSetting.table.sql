﻿CREATE TABLE [dbo].[AutoDocSetting] (
    [CompanyCode] NVARCHAR (2)    NULL,
    [DocType]     VARCHAR (3)     NULL,
    [AutoSegType] VARCHAR (5)     NULL,
    [RefCode]     VARCHAR (10)    NULL,
    [ServType]    VARCHAR (3)     NULL,
    [SegType]     VARCHAR (3)     NULL,
    [MasterDesc]  NVARCHAR (2000) NULL
);

