﻿CREATE TABLE [dbo].[PEOINVTKT] (
    [CompanyCode] NVARCHAR (2)  NOT NULL,
    [Ticket]      NVARCHAR (10) NOT NULL,
    [TktSeq]      NVARCHAR (10) NOT NULL,
    [BkgRef]      NVARCHAR (10) NULL,
    [InvNum]      NVARCHAR (10) NOT NULL,
    [CreateOn]    DATETIME      NULL,
    [CreateBy]    NVARCHAR (10) NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NVARCHAR (10) NULL
);

