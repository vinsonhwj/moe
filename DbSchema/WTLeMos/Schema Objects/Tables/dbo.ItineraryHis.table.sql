﻿CREATE TABLE [dbo].[ItineraryHis] (
    [companycode] NVARCHAR (3)  NULL,
    [bkgref]      NVARCHAR (10) NULL,
    [segment]     DECIMAL (18)  NULL,
    [viewcode]    TEXT          NULL,
    [createon]    DATETIME      NULL,
    [createby]    NVARCHAR (50) NULL,
    [updateon]    DATETIME      NULL,
    [updateby]    NVARCHAR (50) NULL
);

