﻿CREATE TABLE [dbo].[PEOAIRDETAIL] (
    [CompanyCode]         NVARCHAR (2)    NOT NULL,
    [BkgRef]              NVARCHAR (10)   NOT NULL,
    [SegNum]              NVARCHAR (5)    NOT NULL,
    [SEQNUM]              NVARCHAR (5)    NOT NULL,
    [SEQTYPE]             NVARCHAR (6)    NOT NULL,
    [AGETYPE]             NVARCHAR (3)    NULL,
    [QTY]                 INT             NULL,
    [CURR]                NVARCHAR (3)    NULL,
    [AMT]                 DECIMAL (16, 2) NULL,
    [TAXCURR]             NVARCHAR (3)    NULL,
    [TAXAMT]              DECIMAL (16, 2) NULL,
    [CREATEON]            DATETIME        NULL,
    [CREATEBY]            NVARCHAR (10)   NULL,
    [UPDATEON]            DATETIME        NULL,
    [UPDATEBY]            NVARCHAR (10)   NULL,
    [AMTFEE]              DECIMAL (18, 2) NULL,
    [DISCOUNT_MARKUP_PER] DECIMAL (16, 2) NULL,
    [INVNETTGROSS]        DECIMAL (16, 2) NULL
);

