﻿CREATE TABLE [dbo].[WP_eInvoice] (
    [CompanyCode]      VARCHAR (2)    NOT NULL,
    [Invnum]           VARCHAR (10)   NOT NULL,
    [CustEmail]        VARCHAR (200)  NULL,
    [CustContact]      NVARCHAR (100) NULL,
    [StaffName]        NVARCHAR (100) NOT NULL,
    [StaffContact]     VARCHAR (100)  NOT NULL,
    [NoticeEmail]      VARCHAR (100)  NOT NULL,
    [PaymentCode]      VARCHAR (10)   NULL,
    [GenPaymentCodeOn] DATETIME       NULL,
    [GenPaymentCodeBy] VARCHAR (10)   NULL,
    [Remarks]          NVARCHAR (500) NULL,
    [CreateOn]         DATETIME       NOT NULL,
    [CreateBy]         VARCHAR (10)   NOT NULL,
    [UpdateOn]         DATETIME       NULL,
    [UpdateBy]         VARCHAR (10)   NULL,
    [PayOn]            DATETIME       NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Payment Code from WPS', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'PaymentCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Default as Login Staff Email', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'NoticeEmail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Staff''s Phone No', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'StaffContact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Staff Name', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'StaffName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client Contact Person', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'CustContact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client Email', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'CustEmail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Invoice No.', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'Invnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Y
Company Code', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'CompanyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Store return Message from WPS', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'Remarks';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generate Payment Code User', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'GenPaymentCodeBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generate Payment Code Date', @level0type = N'USER', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WP_eInvoice', @level2type = N'COLUMN', @level2name = N'GenPaymentCodeOn';

