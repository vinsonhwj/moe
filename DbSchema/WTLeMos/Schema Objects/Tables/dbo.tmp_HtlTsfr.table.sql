﻿CREATE TABLE [dbo].[tmp_HtlTsfr] (
    [CompanyCode] NVARCHAR (2)   NULL,
    [BkgRef]      NVARCHAR (10)  NULL,
    [TripNo]      NVARCHAR (15)  NULL,
    [CreateOn]    DATETIME       NULL,
    [Description] NVARCHAR (200) NULL
);

