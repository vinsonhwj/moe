﻿CREATE TABLE [dbo].[CustomerRemark] (
    [CompanyCode] VARCHAR (2)    NOT NULL,
    [CltCode]     VARCHAR (10)   NOT NULL,
    [Description] NVARCHAR (500) NOT NULL,
    [CreateOn]    DATETIME       NOT NULL,
    [CreateBy]    VARCHAR (10)   NOT NULL,
    [UpdateOn]    DATETIME       NULL,
    [UpdateBy]    VARCHAR (10)   NULL
);

