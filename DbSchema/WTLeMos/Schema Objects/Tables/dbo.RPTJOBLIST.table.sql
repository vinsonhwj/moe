﻿CREATE TABLE [dbo].[RPTJOBLIST] (
    [REPORTNAME]  NCHAR (10)  NULL,
    [COMPANYCODE] NCHAR (2)   NULL,
    [PARM1]       NCHAR (50)  NULL,
    [PARM2]       NCHAR (50)  NULL,
    [PARM3]       NCHAR (50)  NULL,
    [PARM4]       NCHAR (50)  NULL,
    [PARM5]       NCHAR (50)  NULL,
    [MAIL_TO]     NCHAR (255) NULL,
    [MAIL_CC]     NCHAR (255) NULL,
    [MAIL_BCC]    NCHAR (255) NULL,
    [ACTION]      NCHAR (1)   NULL,
    [LAST_SENT]   DATETIME    NULL,
    [CREATEON]    DATETIME    NULL,
    [UPDATEON]    DATETIME    NULL,
    [row_number]  INT         IDENTITY (1, 1) NOT NULL
);

