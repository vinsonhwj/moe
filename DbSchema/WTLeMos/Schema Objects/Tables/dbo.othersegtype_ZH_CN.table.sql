﻿CREATE TABLE [dbo].[othersegtype_ZH_CN] (
    [SegType]     NCHAR (3)    NOT NULL,
    [Description] NCHAR (30)   NULL,
    [Updateon]    DATETIME     NULL,
    [Updateby]    NCHAR (3)    NULL,
    [systemonly]  NVARCHAR (1) NULL
);

