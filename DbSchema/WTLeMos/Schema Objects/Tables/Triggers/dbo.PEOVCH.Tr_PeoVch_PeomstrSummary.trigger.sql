﻿/****** Object:  Trigger dbo.Tr_PeoVch_PeomstrSummary    Script Date: 8/11/2005
1:04:54 PM ******/
CREATE  TRIGGER Tr_PeoVch_PeomstrSummary ON dbo.PEOVCH 
-- Modified by EDC on 3Jan2006 ---
FOR UPDATE
--FOR INSERT, UPDATE, DELETE 
-- END

AS
declare @cc varchar(2)
declare @bkg varchar(10)

-- Modified by EDC on 3Jan2006 --
-- Added check voidon and voidby
IF UPDATE(VOIDON) AND UPDATE(VOIDBY)
-- END
BEGIN
	select @cc = inserted.companycode, @bkg = inserted.bkgref from inserted
	exec dbo.sp_PEOMSTRsummary_trigger @cc, @bkg
END
