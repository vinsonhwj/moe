﻿
/****** Object:  Trigger dbo.tr_peomco_update    Script Date: 8/11/2005 1:04:54 PM ******/
/****** Object:  Trigger dbo.tr_peomco_update    Script Date: 2003/6/30 下午 02:35:54 ******/
CREATE  TRIGGER tr_peomco_update ON dbo.PEOMCO 
FOR update
AS
if update(voidon)  
	begin 
/* modify by IVT on 08Ang2003, company code should be inserted 
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
		Select 'peomco',left(MCONUM+MCOSEQ,20),CREATEON,'0',getdate(),'V' from inserted
*/
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
		Select 'peomco',left(MCONUM+MCOSEQ,20),CREATEON,'0',getdate(),'V', companycode from inserted

			INSERT INTO PEONYREF(COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,DocNum,costCurr,costAmt,costTaxCurr,costTaxAmt,Description,CREATEON,CREATEBY)
			select companycode, bkgref, voidby, teamcode, 'VMPD',mconum,costcurr,costamt, taxcurr,taxamt, mconum + '(Less ' +  costcurr + ' ' + CONVERT(VARCHAR,costamt) + ' + ' +  CONVERT(VARCHAR,taxamt) + '/' + suppcode + ')', getdate(), voidby from inserted

/* end modify by IVT on 08Ang2003*/
	end

