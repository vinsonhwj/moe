﻿/****** Object:  Trigger dbo.TR_calSellAmt    Script Date: 8/11/2005 1:04:53 PM ******/
-- Create by IVT on 11Sep2003, add new field sellexcludetax
CREATE TRIGGER [TR_calSellAmt] ON dbo.PEOINV 
FOR INSERT, UPDATE
AS
Begin
if update(sellexcludetax) or update(taxamt)
 update peoinv set peoinv.sellamt = (isnull(inserted.SellExcludeTax, 0) + isnull(inserted.taxamt, 0))
    from peoinv, inserted 
    where peoinv.companycode = inserted.companycode and  
    peoinv.bkgref = inserted.bkgref and peoinv.invnum=inserted.invnum 

	Declare 	@CompanyCode varchar(2)	
	Declare	@InvNum varchar(10)

	Select @CompanyCode=I.CompanyCode, @InvNum=I.InvNum From Inserted I

	Update 	PeoInv
	Set	hkdAmt = (IsNull(v.sellAmt,0) / ISNULL(b.exRate,1))
	From	PeoInv v Left Join exRate b on v.CompanyCode=b.CompanyCode and v.SellCurr=b.SourceCurr and b.TargetCurr = (SELECT localcurr FROM company WHERE companycode =@CompanyCode)
	Where	v.CompanyCode=@CompanyCode And v.InvNum=@InvNum

End

