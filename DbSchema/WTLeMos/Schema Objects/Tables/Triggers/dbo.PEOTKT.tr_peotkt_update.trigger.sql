﻿/****** Object:  Trigger dbo.tr_peotkt_update    Script Date: 8/11/2005 1:04:53 PM ******/
/****** Object:  Trigger dbo.tr_peotkt_update    Script Date: 2003/6/30 ¤U¤È 02:35:55 ******/
/*
ALTER  TRIGGER tr_peotkt_update ON dbo.PEOTKT 
FOR update 
AS
if update(voidon)  
	begin */
/* modify by IVT on 08Ang2003, company code should be inserted 
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
		Select 'PEOTKT',LTRIM(left(Ticket+TKTSEQ,20)),CREATEON,'0',getdate(),'V' from inserted
*/
/*		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
		Select 'PEOTKT',LTRIM(left(Ticket+TKTSEQ,20)),CREATEON,'0',getdate(),'V', companycode from inserted*/
/* end modify by IVT on 08Ang2003*/
/*	end*/
/****** Object:  Trigger dbo.tr_peoTKT_insert    Script Date: 2003/6/30 ¤U¤È 02:35:55 ******/
CREATE   TRIGGER tr_peotkt_update ON dbo.PEOTKT 
FOR  update
AS
begin
/* --- Modify by HBY 2003-09-18*/
DECLARE @as_voidon as datetime
DECLARE @as_createon as datetime
DECLARE @as_createby as varchar(100)
DECLARE @as_Ticket as varchar(100)
DECLARE @as_tktseq as varchar(100)
DECLARE @as_companycode as varchar(100)
DECLARE @as_BKGREF as varchar(100)
DECLARE @as_CltCode as varchar(100)
DECLARE @as_SuppCode as varchar(100)
DECLARE @as_NetCurr as varchar(100)
DECLARE @as_NetFare as decimal(16,2)
DECLARE @as_TotalTax as decimal(16,2)
DECLARE @as_count as integer
declare @mystaffcode varchar(10)
declare @myteamcode varchar(5)

	SELECT @mystaffcode = b.staffcode, @myteamcode = b.teamcode from inserted a, login b where b.login = a.updateBY AND b.companycode = a.companycode

	DECLARE voidon_cursor CURSOR FOR 
--	SELECT Ticket, tktseq, createon, companycode
	SELECT CompanyCode, Ticket, TktSeq, BKGREF, CltCode, SuppCode, NetCurr, NetFare, TotalTax, CreateOn, Createby, voidon
	FROM inserted
	
	OPEN voidon_cursor 
	
	FETCH NEXT FROM voidon_cursor 
	INTO @as_companycode, @as_Ticket, @as_tktseq, @as_BKGREF, @as_CltCode, @as_SuppCode, @as_NetCurr, @as_NetFare, @as_TotalTax, @as_createon, @as_createby, @as_voidon
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
	/* --- Modify by HBY 2003-09-18*/


					/* --- Modify by HBY 2003-09-19*/
		if not exists (select * from peonyref where companycode = @as_companycode and docnum = @as_Ticket + '/' + @as_tktseq and (actioncode = 'VTKT' or actioncode = 'MVTKT') and bkgref = @as_BKGREF)
			begin
				if update(pnr) and @as_voidon is not null
				begin
					insert into peonyref (COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,Description,CREATEON,CREATEBY,InvTypeCode)
					select COMPANYCODE,BKGREF,@mystaffcode,@myteamcode,'VTKT',CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,@as_Ticket + 'void'+Description,getdate(),CREATEBY,InvTypeCode
					 from peonyref where companycode = @as_companycode and actioncode in ('MTKT','TKT') and bkgref = @as_BKGREF and docnum = @as_Ticket + '/' + @as_tktseq 

					if not exists (select * from PeoAcctUp where TABLENAME = 'PEOTKT' and DOCNUM = LTRIM(left(@as_Ticket+@as_tktseq,20)) and ACTION = 'V' )
						insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
						values( 'PEOTKT',LTRIM(left(@as_Ticket+@as_tktseq,20)),@as_createon,'0',getdate(),'V', @as_companycode )
				end
				else 
				begin
					if @as_voidon is not null
					begin
						insert into peonyref (COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,Description,CREATEON,CREATEBY,InvTypeCode)
						select COMPANYCODE,BKGREF,@mystaffcode,@myteamcode,'MVTKT',CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,@as_Ticket + 'void'+Description,getdate(),CREATEBY,InvTypeCode
						 from peonyref where companycode = @as_companycode and actioncode in ('MTKT','TKT') and bkgref = @as_BKGREF and docnum = @as_Ticket + '/' + @as_tktseq 
	
						if not exists (select * from PeoAcctUp where TABLENAME = 'PEOTKT' and DOCNUM = LTRIM(left(@as_Ticket+@as_tktseq,20)) and ACTION = 'V' )
							insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
							values( 'PEOTKT',LTRIM(left(@as_Ticket+@as_tktseq,20)),@as_createon,'0',getdate(),'V', @as_companycode )
					end
				end
			end

	
			FETCH NEXT FROM voidon_cursor 
			INTO @as_companycode, @as_Ticket, @as_tktseq, @as_BKGREF, @as_CltCode, @as_SuppCode, @as_NetCurr, @as_NetFare, @as_TotalTax, @as_createon, @as_createby, @as_voidon
		END
	
	CLOSE voidon_cursor
	DEALLOCATE voidon_cursor
/* --- End by HBY 2003-09-18*/
end

SET QUOTED_IDENTIFIER ON
