﻿/****** Object:  Trigger dbo.Tr_PeoInv_UPDATE    Script Date: 8/11/2005 1:04:53 PM ******/
/****** Object:  Trigger dbo.Tr_PeoInv_UPDATE    Script Date: 2003/6/30 下午 02:35:54 ******/
CREATE TRIGGER Tr_PeoInv_UPDATE ON dbo.PEOINV 
FOR UPDATE
AS
if update(voidon)  
	begin 
/* Modify by IVT on 08Ang2003, update the company code
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
		Select 'PEOINV',INVNUM,CREATEON,'0',getdate(),'V' from inserted
*/ 
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
		Select 'PEOINV',INVNUM,CREATEON,'0',getdate(),'V', companycode from inserted
/* end modify by IVT*/
	end
