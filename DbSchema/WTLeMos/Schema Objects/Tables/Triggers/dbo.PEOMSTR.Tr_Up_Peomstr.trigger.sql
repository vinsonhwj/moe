﻿/****** Object:  Trigger dbo.Tr_Up_Peomstr    Script Date: 8/11/2005 1:04:53 PM ******/
CREATE      TRIGGER [Tr_Up_Peomstr] ON dbo.PEOMSTR 
FOR  UPDATE 
AS
BEGIN
	declare @mystaffcode varchar(10)
	declare @myteamcode varchar(5)
	select @mystaffcode = b.staffcode, @myteamcode = b.teamcode from inserted a, login b where b.login = a.updateBY and a.companycode = b.companycode
   if update(CLTODE)
      insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
	 Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	 select a.COMPANYCODE,a.BKGREF,a.BKGREF,'CLT',a.CLTODE,a.TTLSELLCURR,a.TTLSELLAMT,a.TTLSELLTAXCURR,a.TTLSELLTAX,a.TTLCOSTCURR,a.TTLCOSTAMT,
	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(b.CLTODE,'')+'-->'+isnull(a.CLTODE,''),a.updateon,a.updateBY,@mystaffcode,@myteamcode
--	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(b.CLTODE,'')+'-->'+isnull(a.CLTODE,''),a.updateon,a.updateBY,a.staffcode,a.teamcode
	 from inserted a,deleted b where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF  and a.CLTODE<>b.CLTODE
   if update(MASTERPNR)
	begin
      insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
	 Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	 select a.COMPANYCODE,a.BKGREF,a.BKGREF,'MSTRP',a.CLTODE,a.TTLSELLCURR,a.TTLSELLAMT,a.TTLSELLTAXCURR,a.TTLSELLTAX,a.TTLCOSTCURR,a.TTLCOSTAMT,
	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(b.MASTERPNR,'')+'-->'+isnull(a.MASTERPNR,''),a.updateon,a.updateBY,@mystaffcode,@myteamcode
	 from inserted a,deleted b where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF  and a.MASTERPNR<>b.MASTERPNR

	  insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
		Select 'PEOMSTR',b.MASTERPNR + '|' + a.MASTERPNR,getdate(),'0',getdate(),'I', a.companycode
		from inserted a,deleted b where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF  and a.MASTERPNR<>b.MASTERPNR
	end
-- added by IVT on 29Dec2003, add history for change ownership of a booking
if update(staffcode)
begin
	if not exists (select m.bkgref from deleted m, inserted i where m.bkgref = i.bkgref and m.companycode = i.companycode and m.staffcode = i.staffcode) 
	begin
		insert into peonyref(COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,DocNum,Description,CREATEON,CREATEBY)
		select a.companycode, a.bkgref, @mystaffcode, @myteamcode, 'MCOW', a.bkgref, 'Change Ownership ' + rtrim(b.staffcode) + '/' + rtrim(b.teamcode) + ' --> ' + rtrim(a.staffcode) + '/' + rtrim(a.teamcode) , getdate(), @mystaffcode
		from inserted a , deleted b where a.companycode = b.companycode and a.bkgref = b.bkgref
	end
end

if update(cod1) OR update(cod2) OR update(cod3)
begin

	UPDATE peomis set peomis.COD1 = inserted.cod1, peomis.COD2 = inserted.cod2, peomis.COD3 = inserted.cod3
	FROM inserted
        WHERE peomis.companycode = inserted.companycode and peomis.bkgref = inserted.bkgref
end
-- end added by IVT on 29Dec2003
-- Added by IVT on 28Oct2003, Link bkgref with Master PNR
update peomstr set masterpnr = inserted.bkgref from peomstr , inserted 
where peomstr.bkgref = inserted.bkgref and peomstr.companycode = inserted.companycode and isnull(peomstr.masterpnr, '') = ''
-- End Added by IVT on 28Oct2003
/*
remarks : backup on 28Jul2003 by IVT
   if update(MASTERPNR)
      insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
	 Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	 select a.COMPANYCODE,a.BKGREF,a.BKGREF,'MSTRP',a.CLTODE,a.TTLSELLCURR,a.TTLSELLAMT,a.TTLSELLTAXCURR,a.TTLSELLTAX,a.TTLCOSTCURR,a.TTLCOSTAMT,
	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(b.MASTERPNR,'')+'-->'+isnull(a.MASTERPNR,''),a.updateon,a.updateBY,a.staffcode,a.teamcode
	 from inserted a,deleted b where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF  and a.MASTERPNR<>b.MASTERPNR
remarks : end backup on 28Jul2003 by IVT
*/
/*
   if update(COD3)
      insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
	 Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE)  
	 select a.COMPANYCODE,a.BKGREF,a.BKGREF,'ACODE',a.CLTODE,a.TTLSELLCURR,a.TTLSELLAMT,a.TTLSELLTAXCURR,a.TTLSELLTAX,a.TTLCOSTCURR,a.TTLCOSTAMT,
	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(b.COD3,'')+'-->'+isnull(a.COD3,''),a.updateon,a.updateBY,a.staffcode,a.teamcode
	 from inserted a,deleted b where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF  and a.COD3<>b.COD3
*/

-- added by JNY on 07Jun2006, add history for Approve
if update(ApproveBy)
begin
	
	insert into peonyref(COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,DocNum,Description,CREATEON,CREATEBY)
	select a.companycode, a.bkgref, @mystaffcode, @myteamcode, 'UIUD', a.bkgref, case a.approveby when '' then 'Cancel Un-invoice or Un-doc Approval by ' else 'Un-invoice or Un-doc Approval by ' end + rtrim(a.UpdateBy) , getdate(), @mystaffcode
	from inserted a , deleted b where a.companycode = b.companycode and a.bkgref = b.bkgref
end
END

SET QUOTED_IDENTIFIER ON
