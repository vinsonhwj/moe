﻿CREATE  TRIGGER [TR_IN_PEOMSTR] ON dbo.PEOMSTR 
FOR INSERT 
AS
BEGIN
	declare @CompanyCode varchar(2)
	declare @Docnum varchar(10)
	declare @NeedSyncToeReport varchar(1)
	
	declare @mystaffcode varchar(10)
	declare @myteamcode varchar(5)

	select @CompanyCode = a.CompanyCode, @Docnum =a.bkgref from inserted a
	select @mystaffcode = b.staffcode, @myteamcode = b.teamcode from inserted a, login b where b.login = a.createby and a.companycode = b.companycode
	

	insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	select COMPANYCODE,BKGREF,BKGREF,'MOS',CLTODE,TTLSELLCURR,TTLSELLAMT,TTLSELLTAXCURR,TTLSELLTAX,TTLCOSTCURR,TTLCOSTAMT,TTLCOSTTAXCURR,TTLCOSTTAX,'Booking Created',CREATEON,CREATEBY,STAFFCODE,TEAMCODE
	from inserted  where substring(BKGREF,1,1)='P'

	 insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,CLTCODE,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
	 Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	 select a.COMPANYCODE,a.BKGREF,a.BKGREF,'MSTRP',a.CLTODE,a.TTLSELLCURR,a.TTLSELLAMT,a.TTLSELLTAXCURR,a.TTLSELLTAX,a.TTLCOSTCURR,a.TTLCOSTAMT,
	 a.TTLCOSTTAXCURR,a.TTLCOSTTAX,isnull(a.bkgref,'')+'-->'+isnull(a.MASTERPNR,''),a.createon,a.createby,@mystaffcode,@myteamcode
	 from inserted a

	if @CompanyCode is not null
	begin
		if not exists (select docnum from eReportSyncLog where CompanyCode = @CompanyCode and Docnum = @Docnum and SyncTable = 'PEOMSTR' and SyncStatus = 0)
		begin
			insert into eReportSyncLog(CompanyCode, SyncTable,DOCNUM,SyncStatus,CreateOn)  values (@CompanyCode, 'PEOMSTR',@Docnum,'0',getdate())
		end
	end

	----- Modified at 11 Mar 2010 - insert COD information on peomis
	if not exists (Select bkgref from peomis where companyCode=@CompanyCode and bkgRef=@Docnum)
		Insert into peomis
		(BkgRef, CompanyCode, SegNum, CltCode, GemsCode, COD1, COD2, COD3, ReasonCode, CreateOn, CreateBy, TransactionFee, MIS1, MIS2, MIS3)
		Select BkgRef, CompanyCode, '00001', Cltode, '', IsNull(COD1,''), IsNull(COD2,''), IsNull(COD3,''), '', CreateOn, CreateBy, 0, '', '', '' From Inserted
	----- End Modified at 11 Mar 2010 - insert COD information on peomis

	Update peomstr
	Set Cltode=s.CltCode, CltName=c.CltName
	From peomstr m
		Inner Join inserted i
		on m.CompanyCode=i.CompanyCode and m.BkgRef=i.BkgRef
		Inner Join peoStaff s
		on i.CompanyCode=s.CompanyCode and i.StaffCode=s.StaffCode
		Inner Join Customer c
		on s.CompanyCode=c.CompanyCode and s.CltCode=c.CltCode
	Where IsNull(i.Cltode,'')=''
END
