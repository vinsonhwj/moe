﻿/****** Object:  Trigger dbo.tr_PEOVCH_insert    Script Date: 8/11/2005 1:04:54 PM ******/
/****** Object:  Trigger dbo.tr_PEOVCH_insert    Script Date: 2003/6/30 ?? 02:35:55 ******/
CREATE TRIGGER tr_PEOVCH_insert ON dbo.PEOVCH 
FOR INSERT
AS
begin
	UPDATE PEOMSTR SET DOCCOUNT = ISNULL(DOCcount,0) + 1 FROM PEOMSTR,INSERTED 
	WHERE  PEOMSTR.BKGREF = INSERTED.BKGREF AND PEOMSTR.COMPANYCODE=INSERTED.COMPANYCODE
/* modify by IVT on 08Ang2003, company code should be inserted 
	insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
	Select 'PEOVCH',VCHNUM,CREATEON,'0',getdate(),'I' from inserted
*/
	insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
	Select 'PEOVCH',VCHNUM,CREATEON,'0',getdate(),'I', companycode from inserted
/* end modify by IVT*/
end
