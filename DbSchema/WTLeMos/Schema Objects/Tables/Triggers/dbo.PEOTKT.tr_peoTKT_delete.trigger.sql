﻿/****** Object:  Trigger dbo.tr_peoTKT_delete    Script Date: 8/11/2005 1:04:53 PM ******/
CREATE TRIGGER tr_peoTKT_delete ON dbo.PEOTKT 
FOR DELETE 
AS
begin
/* --- Modify by HBY 2003-09-18*/
DECLARE @as_voidon as datetime
DECLARE @as_createon as datetime
DECLARE @as_createby as varchar(100)
DECLARE @as_Ticket as varchar(100)
DECLARE @as_tktseq as varchar(100)
DECLARE @as_companycode as varchar(100)
DECLARE @as_BKGREF as varchar(100)
DECLARE @as_CltCode as varchar(100)
DECLARE @as_SuppCode as varchar(100)
DECLARE @as_NetCurr as varchar(100)
DECLARE @as_NetFare as decimal(16,2)
DECLARE @as_TotalTax as decimal(16,2)
DECLARE @as_count as integer
/*
	DECLARE voidon_cursor CURSOR FOR 
--	SELECT Ticket, tktseq, createon, companycode
	SELECT CompanyCode, Ticket, TktSeq, BKGREF, CltCode, SuppCode, NetCurr, NetFare, TotalTax, CreateOn, Createby, voidon
	FROM inserted
	
	OPEN voidon_cursor 
	
	FETCH NEXT FROM voidon_cursor 
	INTO @as_companycode, @as_Ticket, @as_tktseq, @as_BKGREF, @as_CltCode, @as_SuppCode, @as_NetCurr, @as_NetFare, @as_TotalTax, @as_createon, @as_createby, @as_voidon
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			select @AS_COUNT = count(*) from peotkt where BKGREF = @as_BKGREF and companycode = @as_companycode
			if @as_count = 0 
				BEGIN
					UPDATE PEOMSTR SET DOCCOUNT = ISNULL(DOCcount,0) + 1 FROM PEOMSTR 
					WHERE  BKGREF = @as_BKGREF AND COMPANYCODE = @as_companycode 
				END
		
		END
	
	CLOSE voidon_cursor
	DEALLOCATE voidon_cursor
*/
/* --- End by HBY 2003-09-18*/
end
