﻿/****** Object:  Trigger dbo.Tr_PeoMCO_Insert    Script Date: 8/11/2005 1:04:54 PM ******/
/****** Object:  Trigger dbo.Tr_PeoMCO_Insert    Script Date: 2003/6/30 下午 02:35:55 ******/
CREATE TRIGGER Tr_PeoMCO_Insert ON dbo.PEOMCO 
FOR INSERT
AS
begin
	INSERT INTO PEONYREF (	COMPANYCODE,	BKGREF,		ACTIONCODE,	CLTCODE,	SUPPCODE,	DocNum,COSTCurr,	COSTAmt,
			COSTTaxCurr,	COSTTAXAMT,	
			Description,
			CREATEON,	CREATEBY,	STAFFCODE,	TEAMCODE)
	SELECT 		A.COMPANYCODE,	A.BKGREF,	'MPD',		'',		SUPPCODE,	A.MCONUM,	A.COSTCURR,	A.COSTAMT,
			A.TAXCURR,	A.TAXAMT,
			
/* Comment by IVT on 06Ang2003, modify the document description
	A.MCONUM +'(COST ' + isnull(A.COSTCURR,'') + convert(varchar(16),isnull(A.COSTAMT,0)) +'+'+isnull(A.TAXCURR,'') +convert(varchar(16),isnull(A.TAXAMT,0))+'/'+ISNULL(A.SUPPCODE,'')+')',
*/
	case when a.gstamt is null or a.gstamt = 0 then
            '(Cost ' + isnull(A.costCURR,'') + convert(varchar(16),isnull(A.costamt,0)) +' + Tax '+isnull(A.taxCURR,'') +convert(varchar(16),isnull(A.taxamt,0)) +' / '+RTRIM(ISNULL(A.SUPPCODE,''))+')'
            when a.gstamt >0 then
            '(Cost ' + isnull(A.costCURR,'') + convert(varchar(16),isnull(A.costamt,0)) +' + Tax '+isnull(A.taxCURR,'') +convert(varchar(16),isnull(A.taxamt,0)) + ' + GST '+isnull(A.costCURR,'') +convert(varchar(16),isnull(A.gstamt,0)) +' / '+RTRIM(ISNULL(A.SUPPCODE,''))+')'
            end, 
/* end Modify by IVT on 06Ang2003 */
			GETDATE(),	A.CREATEBY,	A.STAFFCODE,	A.TEAMCODE
	FROM INSERTED A 
	
	--UPDATE PEOMSTR SET DOCCOUNT = ISNULL(DOCCount,0) + 1 FROM PEOMSTR,INSERTED 
	--WHERE  PEOMSTR.BKGREF = INSERTED.BKGREF AND PEOMSTR.COMPANYCODE=INSERTED.COMPANYCODE 
/* modify by IVT on 08Ang2003, company code should be inserted 
	insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
	Select 'peomco',left(MCONUM+MCOSEQ,20),CREATEON,'0',getdate(),'I' from inserted
*/
	insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
	Select 'peomco',left(MCONUM+MCOSEQ,20),CREATEON,'0',getdate(),'I', companycode from inserted
/* end modify by IVT on 08Ang2003*/
end
