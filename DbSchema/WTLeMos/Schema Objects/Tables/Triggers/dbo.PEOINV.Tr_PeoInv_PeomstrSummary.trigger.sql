﻿/****** Object:  Trigger dbo.Tr_PeoInv_PeomstrSummary    Script Date: 8/11/2005 1:04:53 PM ******/
CREATE  TRIGGER Tr_PeoInv_PeomstrSummary ON dbo.PEOINV
-- Modified by EDC on 3Jan2006 --
FOR UPDATE 
--FOR INSERT, UPDATE, DELETE 
-- END
AS

declare @cc varchar(2)
declare @bkg varchar(10)

-- Modified by EDC on 3Jan2006 
-- Added check voidon and voidby
IF UPDATE(VOIDON) AND UPDATE(VOIDBY)
-- END
BEGIN
	select @cc = inserted.companycode, @bkg = inserted.bkgref from inserted
	exec dbo.sp_PEOMSTRsummary_trigger @cc, @bkg
END
