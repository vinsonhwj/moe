﻿/****** Object:  Trigger dbo.Tr_Up_PeoPax    Script Date: 8/11/2005 1:04:54 PM ******/
/****** Object:  Trigger dbo.Tr_Up_PeoPax    Script Date: 2003/6/30 下午 02:35:55 ******/
CREATE TRIGGER [Tr_Up_PeoPax] ON dbo.PEOPAX 
FOR  UPDATE 
AS
if (update(PAXLNAME) OR update(PAXFNAME))
   BEGIN
      if update(voidby)
	 insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	    select a.COMPANYCODE,a.BKGREF,a.BKGREF,'vPAX','VOID '+a.SEGNUM+' ('+a.PAXOTHER+' '+a.PAXFNAME+'/'+a.PAXLNAME+' '+a.PAXTITLE+')',a.updateon,a.updateBY,c.staffcode,c.teamcode
	    from inserted a,deleted b,peomstr c where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF and a.BKGREF=c.BKGREF and a.PAXLNAME+a.PAXFNAME<>b.PAXLNAME+b.PAXFNAME
      else	
         insert into PEONYREF(COMPANYCODE,BKGREF,DOCNUM,ACTIONCODE,Description,CREATEON,CREATEBY,STAFFCODE,TEAMCODE) 
	    select a.COMPANYCODE,a.BKGREF,a.BKGREF,'PAX',a.SEGNUM+' ('+a.PAXOTHER+' '+a.PAXFNAME+'/'+a.PAXLNAME+' '+a.PAXTITLE+')',a.updateon,a.updateBY,c.staffcode,c.teamcode
	    from inserted a,deleted b,peomstr c where a.COMPANYCODE=b.COMPANYCODE and a.BKGREF=b.BKGREF and a.BKGREF=c.BKGREF and a.PAXLNAME+a.PAXFNAME<>b.PAXLNAME+b.PAXFNAME
   END
