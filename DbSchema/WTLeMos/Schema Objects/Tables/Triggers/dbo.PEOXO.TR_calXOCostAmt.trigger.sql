﻿/****** Object:  Trigger dbo.TR_calXOCostAmt    Script Date: 8/11/2005 1:04:54 PM ******/
-- create by IVT on 11Sep2003, add new field costexcludetax
CREATE TRIGGER [TR_calXOCostAmt] ON dbo.PEOXO 
FOR INSERT, UPDATE
AS
Begin
if update(costexcludetax) or update(taxamt)
 update peoxo set peoxo.costamt = (isnull(inserted.CostExcludeTax, 0) + isnull(inserted.taxamt, 0))
    from peoxo, inserted 
    where peoxo.companycode = inserted.companycode and  
    peoxo.bkgref = inserted.bkgref and peoxo.xonum=inserted.xonum 
End
