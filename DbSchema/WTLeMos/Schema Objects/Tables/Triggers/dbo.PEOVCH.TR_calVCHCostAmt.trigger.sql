﻿/****** Object:  Trigger dbo.TR_calVCHCostAmt    Script Date: 8/11/2005 1:04:54 PM ******/
-- create by IVT on 11Sep2003, add new field costexculdetax
CREATE TRIGGER [TR_calVCHCostAmt] ON dbo.PEOVCH 
FOR INSERT, UPDATE
AS
Begin
if update(costexcludetax) or update(taxamt)
 update peovch set peovch.costamt = (isnull(inserted.CostExcludeTax, 0) + isnull(inserted.taxamt, 0))
    from peovch, inserted 
    where peovch.companycode = inserted.companycode and  
    peovch.bkgref = inserted.bkgref and peovch.vchnum=inserted.vchnum 
End
