﻿/****** Object:  Trigger dbo.tr_peoTKT_INTERNAL_AUDITING    Script Date: 8/11/2005 1:04:53 PM ******/
CREATE TRIGGER [tr_peoTKT_INTERNAL_AUDITING] ON dbo.PEOTKT 
FOR UPDATE
AS
IF UPDATE(formpay) BEGIN
	DECLARE @deletedFormpay nvarchar(50)
	DECLARE @deletedASCII integer
	DECLARE @updatedFormpay nvarchar(50)	
	DECLARE @updatedASCII integer
	SELECT @deletedFormpay = formpay, @deletedASCII = ISNULL( ASCII( SUBSTRING(  formpay, 1, 1 )  ) , 0 ) + ISNULL( ASCII( SUBSTRING(  formpay, 2, 1 )  ) , 0 ) + ISNULL( ASCII( SUBSTRING(  formpay, 3, 1 )  ) , 0 )  FROM deleted
	SELECT @updatedFormpay = formpay, @updatedASCII = ISNULL( ASCII( SUBSTRING(  formpay, 1, 1 )  ) , 0 ) + ISNULL( ASCII( SUBSTRING(  formpay, 2, 1 )  ) , 0 ) + ISNULL( ASCII( SUBSTRING(  formpay, 3, 1 )  ) , 0 )  FROM inserted
		
	-- SELECT @deletedFormpay + '-->' +  @updatedFormpay
	
	IF @deletedASCII <> @updatedASCII BEGIN
		
		INSERT INTO dbo.peonyref_INTERNAL_AUDITING (COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,ACTIONCODE,CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,Description,CREATEON,CREATEBY,InvTypeCode)
		SELECT companycode, bkgref, null, teamcode, 'MTKT', CltCode, SuppCode, LEFT(ticket + '/' + tktseq, 13), null, null, null, null, null, null, null, null, @deletedFormpay + ' --> ' + @updatedFormpay, getdate(), updateby, null
		FROM inserted
	END
END
