﻿/****** Object:  Trigger dbo.tr_peoTKT_insert    Script Date: 8/11/2005 1:04:53 PM ******/
/****** Object:  Trigger dbo.tr_peoTKT_insert    Script Date: 2003/6/30 U 02:35:55 ******/
CREATE    TRIGGER tr_peoTKT_insert ON dbo.PEOTKT 
FOR  INSERT
AS
begin
/* --- Modify by HBY 2003-09-18*/
DECLARE @as_voidon as datetime
declare @sourcesystem as varchar(100)
DECLARE @as_createon as datetime
DECLARE @as_createby as varchar(100)
DECLARE @as_Ticket as varchar(100)
DECLARE @as_tktseq as varchar(100)
DECLARE @as_companycode as varchar(100)
DECLARE @as_BKGREF as varchar(100)
DECLARE @as_CltCode as varchar(100)
DECLARE @as_SuppCode as varchar(100)
DECLARE @as_NetCurr as varchar(100)
DECLARE @as_NetFare as decimal(16,2)
DECLARE @as_TotalTax as decimal(16,2)
DECLARE @as_count as integer
Declare @teamcode as varchar(5)
declare @issueby as varchar(3)
declare @mystaffcode varchar(10)
declare @myteamcode varchar(5)

	SELECT @mystaffcode = b.staffcode, @myteamcode = b.teamcode from inserted a, login b where b.login = a.updateBY AND b.companycode = a.companycode
	DECLARE voidon_cursor CURSOR FOR 
--	SELECT Ticket, tktseq, createon, companycode
	SELECT CompanyCode, sourcesystem, Ticket, TktSeq, BKGREF, CltCode, SuppCode, NetCurr, NetFare, TotalTax, CreateOn, Createby, voidon, Teamcode, issueby
	FROM inserted
	
	OPEN voidon_cursor 
	
	FETCH NEXT FROM voidon_cursor 
	INTO @as_companycode, @sourcesystem,  @as_Ticket, @as_tktseq, @as_BKGREF, @as_CltCode, @as_SuppCode, @as_NetCurr, @as_NetFare, @as_TotalTax, @as_createon, @as_createby, @as_voidon, @teamcode, @issueby
	WHILE @@FETCH_STATUS = 0
		BEGIN
		/* --- End by HBY 2003-09-18*/
		
		/*	INSERT INTO PEONYREF (	COMPANYCODE,	BKGREF,		ACTIONCODE,	CLTCODE,	SUPPCODE,	DocNum,		SellCurr,	SellAmt,
					SellTaxCurr,	SELLTAXAMT,	
					Description,
					CREATEON,	CREATEBY,	STAFFCODE,	TEAMCODE)
			SELECT 		A.COMPANYCODE,	A.BKGREF,	'TKT',		A.CLTCODE,	A.SUPPCODE,	A.TICKET,	A.NETCURR,	A.NETFARE,
					A.NETCURR,	A.TOTALTAX,
					A.TICKET +'(Cost ' + isnull(A.NETCURR,'') + convert(varchar(16),isnull(A.NETFARE,0)) +' and Tax '+isnull(A.NETCURR,'') +convert(varchar(16),isnull(A.TOTALTAX,0))+'('+ISNULL(A.CLTCODE,'')+') )',
					GETDATE(),	A.CREATEBY,	B.STAFFCODE,	B.TEAMCODE
			FROM INSERTED A,PEOMSTR B
			WHERE A.BKGREF=B.BKGREF AND A.COMPANYCODE=B.COMPANYCODE*/
		
			if exists (select companycode from peomstr where companycode = @as_companycode and bkgref = @as_BKGREF)
			begin
				select @teamcode=teamcode, @issueby=staffcode from peomstr where companycode = @as_companycode and bkgref = @as_BKGREF
			end	
			-- if not exists (select * from peonyref where BKGREF = @as_BKGREF and ACTIONCODE = 'TKT' and DocNum = @as_Ticket and Description = '(Cost ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_NetFare,0)) +' + Tax ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_TotalTax,0))+' \ '+ISNULL(@as_CltCode,'')+' )' )
			if not exists (select * from peonyref where BKGREF = @as_BKGREF and (ACTIONCODE = 'TKT' or ACTIONCODE = 'MTKT')  and DocNum = @as_Ticket + '/' + @as_tktseq and Description = '(Cost ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_NetFare,0)) +' + Tax ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_TotalTax,0))+' \ '+ISNULL(@as_SuppCode,'')+' )' )
				begin
				
					print(@sourcesystem)
					if (@sourcesystem = 'EMOS' or @sourcesystem is null)
					
						begin		
							INSERT INTO PEONYREF(COMPANYCODE, BKGREF, ACTIONCODE, CLTCODE, SUPPCODE, DocNum, CostCurr, CostAmt,
							CostTaxCurr, COSTTAXAMT, Description, CREATEON, CREATEBY, STAFFCODE, TEAMCODE)
							-- SELECT		@as_companycode,	@as_BKGREF,	'TKT',	@as_CltCode,	@as_SuppCode,	@as_Ticket,	@as_NetCurr,	@as_NetFare,
							SELECT		@as_companycode,	@as_BKGREF,	'MTKT',	@as_CltCode,	@as_SuppCode,	@as_Ticket + '/' + @as_tktseq,	@as_NetCurr,	@as_NetFare,
									@as_NetCurr,	@as_TotalTax,	'(Cost ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_NetFare,0)) +' + Tax ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_TotalTax,0))+' \ '+ISNULL(@as_SuppCode,'')+' )', 
									GETDATE(),	@as_createby,	 @mystaffcode, @myTeamCode
							/*B.STAFFCODE,	B.TEAMCODE
							FROM		PEOMSTR B  
							WHERE		B.BKGREF = @as_BKGREF AND B.COMPANYCODE = @as_companycode*/
						end
					else
						begin		
							INSERT INTO PEONYREF(COMPANYCODE, BKGREF, ACTIONCODE, CLTCODE, SUPPCODE, DocNum, CostCurr, CostAmt,
							CostTaxCurr, COSTTAXAMT, Description, CREATEON, CREATEBY, STAFFCODE, TEAMCODE)
							-- SELECT		@as_companycode,	@as_BKGREF,	'TKT',	@as_CltCode,	@as_SuppCode,	@as_Ticket,	@as_NetCurr,	@as_NetFare,
							SELECT		@as_companycode,	@as_BKGREF,	'TKT',	@as_CltCode,	@as_SuppCode,	@as_Ticket + '/' + @as_tktseq,	@as_NetCurr,	@as_NetFare,
									@as_NetCurr,	@as_TotalTax,	'(Cost ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_NetFare,0)) +' + Tax ' + isnull(@as_NetCurr,'') + convert(varchar(16), isnull(@as_TotalTax,0))+' \ '+ISNULL(@as_SuppCode,'')+' )', 
									GETDATE(),	@as_createby,	  @mystaffcode, @myTeamCode
							/*B.STAFFCODE,	B.TEAMCODE
							FROM		PEOMSTR B  
							WHERE		B.BKGREF = @as_BKGREF AND B.COMPANYCODE = @as_companycode*/
						end
			
				end
			
			/*
			select @AS_COUNT = count(*) from peotkt where BKGREF = @as_BKGREF and companycode = @as_companycode
			if @as_count = 1 
				begin
					UPDATE PEOMSTR SET DOCCOUNT = ISNULL(DOCcount,0) + 1 FROM PEOMSTR 
					WHERE  BKGREF = @as_BKGREF AND COMPANYCODE = @as_companycode 
				end
			*/
		
		/* modify by IVT on 08Ang2003, also need to insert companycode	
			insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
			Select 'PEOTKT',left(Ticket+TKTSEQ,20),CREATEON,'0',getdate(),'I' from inserted
		*/
			/* --- Modify by HBY 2003-09-19*/
			if not exists (select * from PeoAcctUp where TABLENAME = 'PEOTKT' and DOCNUM = LTRIM(left(@as_Ticket+@as_tktseq,20)) and ACTION = 'V' ) and 
			  not exists (select * from PeoAcctUp where TABLENAME = 'PEOTKT' and DOCNUM = LTRIM(left(@as_Ticket+@as_tktseq,20)) and ACTION = 'I' ) and 
			  /* --- Modify by HBY 2003-11-03*/
			  not exists ( select * from peoacctup where TABLENAME = 'PEOTKT' and docnum = LTRIM(left(@as_Ticket+@as_tktseq,20)) and  companycode = @as_companycode and txndate = @as_createon )
			  --not exists ( select * from peoacctup where TABLENAME = 'PEOTKT' and docnum = LTRIM(left(@as_Ticket+@as_tktseq,20)) and STATUS = '0' and companycode = @as_companycode and txndate = @as_createon )
			  /* ---End by HBY 2003-11-03*/
				begin
					insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
					values('PEOTKT',LTRIM(left(@as_Ticket+@as_tktseq,20)), @as_createon,'0',getdate(),'I', @as_companycode )
				end
			/* --- End by HBY 2003-09-19*/
		/* end mofify by IVT on 08Ang2003*/
	/* --- Modify by HBY 2003-09-18*/
			if @as_voidon is not null
				begin
					/* --- Modify by HBY 2003-09-19*/
					if not exists (select * from PeoAcctUp where TABLENAME = 'PEOTKT' and DOCNUM = LTRIM(left(@as_Ticket+@as_tktseq,20)) and ACTION = 'V' )
						begin
							insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
							values( 'PEOTKT',LTRIM(left(@as_Ticket+@as_tktseq,20)),@as_createon,'0',getdate(),'V', @as_companycode )
						end
					/* --- End by HBY 2003-09-19*/
				end
	
			FETCH NEXT FROM voidon_cursor 
			INTO @as_companycode, @sourcesystem, @as_Ticket, @as_tktseq, @as_BKGREF, @as_CltCode, @as_SuppCode, @as_NetCurr, @as_NetFare, @as_TotalTax, @as_createon, @as_createby, @as_voidon, @teamcode, @issueby
		END
	
	CLOSE voidon_cursor
	DEALLOCATE voidon_cursor
/* --- End by HBY 2003-09-18*/
end
