﻿
/****** Object:  Trigger dbo.tr_peoTKTdetail_insert    Script Date: 8/11/2005 1:04:54 PM ******/
CREATE TRIGGER [dbo].[tr_peoTKTdetail_insert] ON [dbo].[PEOTKTDETAIL]
FOR INSERT
AS
begin
/* --- Modify by HBY 2003-10-08*/
DECLARE @as_Ticket as varchar(100)
DECLARE @as_Tktseq as varchar(2)
DECLARE @as_companycode as varchar(2)
DECLARE @as_ArrivalCity as varchar(3)
DECLARE @as_DepartCity as varchar(3)
DECLARE @as_Rounting as varchar(65)
	set @as_Rounting = ''
	select @as_Ticket = ticket from inserted
	select @as_Tktseq = tktseq from inserted
	select @as_companycode = companycode from inserted
	DECLARE tktdetail_cursor CURSOR FOR 
	SELECT DepartCity, ArrivalCity
	FROM peotktdetail where ticket = @as_Ticket and tktseq = @as_tktseq and companycode = @as_companycode order by SegNum
	
	OPEN tktdetail_cursor 
	
	FETCH NEXT FROM tktdetail_cursor 
	INTO @as_DepartCity, @as_ArrivalCity
	
--	WHILE @@FETCH_STATUS = 0
--		BEGIN
--			if ( len(@as_Rounting) = 0 )
--				BEGIN
--					set @as_Rounting = @as_DepartCity + '/' + @as_ArrivalCity
--				END
--			ELSE
--				BEGIN
--					if ( right(@as_Rounting, 3) <> @as_DepartCity )
--						BEGIN
--							set @as_Rounting = @as_Rounting + '-' + @as_DepartCity + '/' + @as_ArrivalCity
--						END
--					else
--						BEGIN
--							set @as_Rounting = @as_Rounting + '/' + @as_ArrivalCity
--						END
--				END
--			UPDATE PEOTKT
--			   SET Routing = @as_Rounting
--			 WHERE TICKET = @as_Ticket and tktseq = @as_tktseq and companycode = @as_companycode
--	
--			FETCH NEXT FROM tktdetail_cursor 
--			INTO @as_DepartCity, @as_ArrivalCity
--		END
	
	CLOSE tktdetail_cursor
	DEALLOCATE tktdetail_cursor
/* --- End by HBY 2003-10-08*/
end

