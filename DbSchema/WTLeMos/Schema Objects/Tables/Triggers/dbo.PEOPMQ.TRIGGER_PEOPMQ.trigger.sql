﻿
create     TRIGGER [dbo].[TRIGGER_PEOPMQ] ON [dbo].[PEOPMQ] 


FOR INSERT, UPDATE		
AS
BEGIN

	DECLARE	@PMQ_CompanyCode	CHAR(2),
			@PMQ_MasterPNR		CHAR(10),
			@PMQ_StaffCode		CHAR(10),
			@PMQ_TeamCode		CHAR(5),
			@PMQ_Status		CHAR(1),
			@PMQ_StaffReasonCode	CHAR(5),
			@PMQ_StaffReason	NVARCHAR(50),
			@PMQ_MgrReason		NVARCHAR(200),
			@PMQ_AcctReason		NVARCHAR(200),
			@PMQ_StaffDeadline	DATETIME,
			@PMQ_UpdateOn		DATETIME,
			@PMQ_UpdateBy		CHAR(10),
			@PMQ_CancelBkg		CHAR(1)
	
	DECLARE	@PEONYREF_InvTypeCode	CHAR(8),
			@PEONYREF_ActionCode	NVARCHAR(10),
			@PEONYREF_Description	NVARCHAR(200),
			@PEONYREF_DocNum 	NVARCHAR(26),
			@PEONYREF_DATE	NVARCHAR(12),
			@PEONYREF_OTHERDOCNUM	NVARCHAR(26)

	SELECT	@PMQ_CompanyCode = CompanyCode,
		@PMQ_MasterPNR = BkgRef,
		@PMQ_StaffCode = StaffCode,
		@PMQ_TeamCode = TeamCode,
		@PMQ_Status = Status,
		@PMQ_StaffReason = staff_reason,
		@PMQ_StaffDeadline = staff_deadline,
		@PMQ_UpdateOn = UpdateOn,
		@PMQ_UpdateBy = UpdateBy,
		@PMQ_CancelBkg = CancelBkg
	FROM INSERTED

	-- SET Default value --
	SELECT @PEONYREF_InvTypeCode = 'PMQ'
	SELECT @PEONYREF_ActionCode = 'PMQ'
	SELECT @PEONYREF_DocNum = ''
	SELECT @PEONYREF_OTHERDOCNUM = NULL
	-- END

	IF @PMQ_CancelBkg <> 'Y'
	BEGIN 

		IF NOT @PMQ_Status = 9 -- Status 9 is normal status
		BEGIN
	
			IF @PMQ_Status = 0
			BEGIN
	
				DECLARE @Count AS INTEGER
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Over Profit Margin Limit%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Over Profit Margin Limit%'
	
				SELECT @PEONYREF_Description = 'Over Profit Margin Limit' + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
				-- Added by EDC on 27Jan2006 ----
				IF @PMQ_StaffReason IS NOT NULL AND @PMQ_StaffReason <> ''
				BEGIN 
		
					INSERT INTO PeonyRef (COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,
								ACTIONCODE,CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,
								SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
								Description,CREATEON,CREATEBY,InvTypeCode)
					VALUES(@PMQ_CompanyCode, @PMQ_MasterPNR, @PMQ_StaffCode, @PMQ_TeamCode, 
							@PEONYREF_ActionCode, NULL, NULL, @PEONYREF_DocNum, NULL, NULL,
							NULL, NULL, NULL, NULL, NULL, NULL,
							@PEONYREF_Description, GetDate(), @PMQ_UpdateBy, @PEONYREF_InvTypeCode)
	
					SELECT @PMQ_Status = 1
					UPDATE PeoPMQ SET Status = 1 WHERE CompanyCode = @PMQ_CompanyCode AND BkgRef = @PMQ_MasterPNR
					
				END
				-- END added by EDC on 27Jan2006
	
			END
	
			-- Status 1 is staff send requestx
			IF @PMQ_Status = 1
			BEGIN		
				 
				SELECT @PMQ_StaffReasonCode = staff_reason
				FROM PeoPMQ WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND BkgRef = @PMQ_MasterPNR
	
				SELECT @PMQ_StaffReason = ReasonDesc
				FROM PeoPMReason WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND ReasonCode = @PMQ_StaffReasonCode
	
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'RQ by staff:%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'RQ by staff:%'
	
				SELECT @PEONYREF_Description = LTRIM(RTRIM('RQ by staff: ')) + LTRIM(RTRIM(@PMQ_StaffReason)) + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
			END
	
			-- Status 2 is manager approved the request
			IF @PMQ_Status = 2
			BEGIN
	
				SELECT @PMQ_MgrReason = mgr_reason
				FROM PeoPMQ WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND BkgRef = @PMQ_MasterPNR
	
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Mgr approved:%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Mgr approved:%'
	
				SELECT @PEONYREF_Description = LTRIM(RTRIM('Mgr approved: ')) + LTRIM(RTRIM(LEFT(@PMQ_MgrReason, 80))) + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
			END
	
			-- Status 3 is manager rejected the request
			IF @PMQ_Status = 3
			BEGIN
	
				SELECT @PMQ_MgrReason = mgr_reason, @PMQ_StaffDeadline = staff_deadline
				FROM PeoPMQ WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND BkgRef = @PMQ_MasterPNR
	
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Mgr(Reject)%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Mgr(Reject)%'
	
				SELECT @PEONYREF_DATE = CONVERT(VARCHAR, @PMQ_StaffDeadline, 107)
				SELECT @PEONYREF_Description = LTRIM(RTRIM('Mgr')) + LTRIM(RTRIM(LEFT(@PMQ_MgrReason, 80))) + LTRIM(RTRIM(': ')) + @PEONYREF_DATE + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
			END
	
			-- Status 4 is account approved the request
			IF @PMQ_Status = 4
			BEGIN
	
				SELECT @PMQ_AcctReason = acct_reason
				FROM PeoPMQ WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND BkgRef = @PMQ_MasterPNR
	
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Acct approved:%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Acct approved:%'
	
				SELECT @PEONYREF_Description = LTRIM(RTRIM('Acct approved: ')) + LTRIM(RTRIM(LEFT(@PMQ_AcctReason, 80))) + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
			END
	
			-- Status 5 is account rejected the request
			IF @PMQ_Status = 5
			BEGIN
	
				SELECT @PMQ_AcctReason = acct_reason, @PMQ_StaffDeadline = staff_deadline
				FROM PeoPMQ WITH (NOLOCK)
				WHERE CompanyCode = @PMQ_CompanyCode
				AND BkgRef = @PMQ_MasterPNR
	
				SELECT @Count = 1
	
				IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Acct(Reject)%') > 0
					SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Acct(Reject)%'
	
				SELECT @PEONYREF_DATE = CONVERT(VARCHAR, @PMQ_StaffDeadline, 107)
				SELECT @PEONYREF_Description = LTRIM(RTRIM('Acct')) + LTRIM(RTRIM(LEFT(@PMQ_AcctReason, 80))) + LTRIM(RTRIM(': ')) + @PEONYREF_DATE  + ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
	
			END
			-- END
	
		END
		ELSE
		BEGIN
			SELECT @Count = 1
	
			IF (SELECT Count(BkgRef) FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Within Profit Margin Limit, queue removed.%') > 0
				SELECT @Count = Count(BkgRef) + 1 FROM PeonyRef WITH (NOLOCK) WHERE CompanyCode = @PMQ_CompanyCode AND ActionCode = 'PMQ' AND BkgRef = @PMQ_MasterPNR AND Description like 'Within Profit Margin Limit, queue removed.%'
	
			SELECT @PEONYREF_Description = 'Within Profit Margin Limit, queue removed.' --+ ' (' + LTRIM(RTRIM(CONVERT(CHAR, @Count))) + ')'
		END
	
		select @PMQ_Teamcode = teamcode from peostaff (nolock) where companycode = @PMQ_CompanyCOde and staffcode = @PMQ_UpdateBy
		
		INSERT INTO PeonyRef (COMPANYCODE,BKGREF,STAFFCODE,TEAMCODE,
					ACTIONCODE,CLTCODE,SUPPCODE,DocNum,SellCurr,SellAmt,
					SellTaxCurr,SellTaxAmt,CostCurr,CostAmt,CostTaxCurr,CostTaxAmt,
					Description,CREATEON,CREATEBY,InvTypeCode)
		VALUES(@PMQ_CompanyCode, @PMQ_MasterPNR, @PMQ_UpdateBy, @PMQ_TeamCode, 
				@PEONYREF_ActionCode, NULL, NULL, @PEONYREF_DocNum, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL,
				@PEONYREF_Description, GetDate(), @PMQ_UpdateBy, @PEONYREF_InvTypeCode)

	END
	
END







