﻿CREATE    TRIGGER Tr_PeoTkt_PeomstrSummary ON dbo.PEOTKT 
FOR INSERT, UPDATE, DELETE 
AS
declare @cc varchar(2)
declare @bkg varchar(10)

-- Added by EDC on 21Apr2006 --
DECLARE @ForProfitMargin_SourceSystem	CHAR(10),
	@ForProfitMargin_VoidOn		DateTime,
	@ForProfitMargin_VoidBy		CHAR(10),
	@ForProfitMargin_Reason_Auto	CHAR(5),
	@ForProfitMargin_Reason_Manual	CHAR(5),
	@ForProfitMargin_Reason_Void	CHAR(5),
	@a char(3)
DECLARE 	@ForProfitMargin_TeamCode		CHAR(5),
	@ForProfitMargin_UpdateBy		CHAR(5)

SELECT @ForProfitMargin_Reason_Auto = 'TKT'
SELECT @ForProfitMargin_Reason_Manual = 'MTKT'
SELECT @ForProfitMargin_Reason_Void = 'VTKT'
-- End added by EDC --

-- Added SourceSystem, VoidOn, VoidBy by EDC on 21Apr2006 --
-- select @cc = inserted.companycode, @bkg = inserted.bkgref from inserted
select 	@cc = inserted.companycode, 
	@bkg = inserted.bkgref, 
	@ForProfitMargin_SourceSystem = inserted.SourceSystem,
	@ForProfitMargin_VoidOn = inserted.VoidOn,
	@ForProfitMargin_VoidBy = inserted.VoidBy,
	@ForProfitMargin_TeamCode = inserted.TeamCode,
	@ForProfitMargin_UpdateBy = inserted.UpdateBy
from inserted

exec dbo.sp_PEOMSTRsummary_trigger @cc, @bkg
	
-- Added by EDC on 21Apr2006 --
-- Profit Margin checking

IF NOT UPDATE(VOIDON) AND NOT UPDATE (VOIDBY)
BEGIN
	IF @ForProfitMargin_VoidOn IS NOT NULL AND @ForProfitMargin_VoidOn <> ''
		AND @ForProfitMargin_VoidBy IS NOT NULL AND @ForProfitMargin_VoidBy <> ''
	BEGIN
		EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Void, @ForProfitMargin_VoidBy, @ForProfitMargin_TeamCode, 'DOC'
	END
	ELSE
	BEGIN
		IF UPPER(@ForProfitMargin_SourceSystem) <> 'EMOS'
				EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Auto, @ForProfitMargin_UpdateBy, @ForProfitMargin_TeamCode, 'DOC'
			ELSE
				EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Manual, @ForProfitMargin_UpdateBy, @ForProfitMargin_TeamCode, 'DOC'
	END
END
ELSE
BEGIN
	EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Void, @ForProfitMargin_UpdateBy, @ForProfitMargin_TeamCode, 'DOC'
END
-- End added by EDC
