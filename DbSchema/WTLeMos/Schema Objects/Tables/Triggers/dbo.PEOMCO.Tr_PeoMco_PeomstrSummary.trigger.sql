﻿CREATE   TRIGGER Tr_PeoMco_PeomstrSummary ON [dbo].[PEOMCO] 
FOR INSERT, UPDATE, DELETE 
AS
declare @cc varchar(2)
declare @bkg varchar(10)

-- Added by EDC on 21Apr2006 --
DECLARE @ForProfitMargin_VoidOn		DateTime,
	@ForProfitMargin_VoidBy		CHAR(10),
	@ForProfitMargin_Reason_Auto	CHAR(5),
	@ForProfitMargin_Reason_Void	CHAR(5),
	@ForProfitMargin_TeamCode 	CHAR(5),
	@ForProfitMargin_UpdateBy 	CHAR(5)
SELECT @ForProfitMargin_Reason_Auto = 'TKT'
SELECT @ForProfitMargin_Reason_Void = 'VTKT'
-- End added by EDC --

-- Added SourceSystem, VoidOn, VoidBy by EDC on 21Apr2006 --
-- select @cc = inserted.companycode, @bkg = inserted.bkgref from inserted
select	@cc = inserted.companycode, 
	@bkg = inserted.bkgref,
	@ForProfitMargin_VoidOn = inserted.VoidOn,
	@ForProfitMargin_VoidBy = inserted.VoidBy,
	@ForProfitMargin_TeamCode = mstr.TeamCode,
        @ForProfitMargin_UpdateBy = mstr.StaffCode
from inserted, peomstr mstr (nolock)
where inserted.bkgref = mstr.bkgref and inserted.companycode = mstr.companycode

exec dbo.sp_PEOMSTRsummary_trigger @cc, @bkg

-- Added by EDC on 21Apr2006 --
-- Profit Margin checking
IF @ForProfitMargin_VoidOn IS NOT NULL AND @ForProfitMargin_VoidOn <> ''
	AND @ForProfitMargin_VoidBy IS NOT NULL AND @ForProfitMargin_VoidBy <> ''
BEGIN

	EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Void, @ForProfitMargin_VoidBy, @ForProfitMargin_TeamCode,'DOC'

END
ELSE
BEGIN

	EXEC SP_ProfitMarginChecking @cc, @bkg, @ForProfitMargin_Reason_Auto, @ForProfitMargin_UpdateBy, @ForProfitMargin_TeamCode, 'DOC'

END
-- End added by EDC 





