﻿/****** Object:  Trigger dbo.tr_peovch_update    Script Date: 8/11/2005 1:04:54 PM ******/
/****** Object:  Trigger dbo.tr_peovch_update    Script Date: 2003/6/30 ?? 02:35:55 ******/
create  TRIGGER tr_peovch_update ON dbo.PEOVCH 
FOR update
AS
if update(voidon)  
	begin 
/* modify by IVT on 08Ang2003, company code should be inserted 
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION) 
		Select 'PEOVCH',VCHNUM,CREATEON,'0',getdate(),'V' from inserted
*/
		insert into PeoAcctUp(TABLENAME,DOCNUM,TXNDATE,STATUS,CREATEDATE,ACTION, companycode) 
		Select 'PEOVCH',VCHNUM,CREATEON,'0',getdate(),'V', companycode from inserted
/* end modify by IVT*/
	end
