﻿CREATE TABLE [dbo].[PeoItinRmk] (
    [CompanyCode] NCHAR (2)  NOT NULL,
    [BkgRef]      NCHAR (10) NOT NULL,
    [RemarkID]    NCHAR (10) NOT NULL,
    [CreateOn]    DATETIME   NULL,
    [CreateBy]    NCHAR (10) NULL,
    [UpdateOn]    DATETIME   NULL,
    [UpdateBy]    NCHAR (10) NULL
);

