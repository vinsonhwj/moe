﻿CREATE TABLE [dbo].[WTMSTR] (
    [Companycode]          VARCHAR (2)  NOT NULL,
    [Bkgref]               VARCHAR (10) NOT NULL,
    [Holdby]               VARCHAR (10) NOT NULL,
    [HoldOrStaffBranch]    VARCHAR (3)  NOT NULL,
    [ReserveBy]            VARCHAR (3)  NOT NULL,
    [ReserveOrStaffBranch] VARCHAR (3)  NOT NULL,
    [CreateOn]             DATETIME     NOT NULL,
    [CreateBy]             VARCHAR (10) NOT NULL,
    [UpdateOn]             DATETIME     NOT NULL,
    [UpdateBy]             VARCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([Companycode] ASC, [Bkgref] ASC)
    WITH FILLFACTOR = 90
);

