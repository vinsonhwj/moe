﻿CREATE TABLE [dbo].[CommissionMstr] (
    [CompanyCode] NVARCHAR (2)    NULL,
    [StaffLevel]  VARCHAR (5)     NULL,
    [StartRange]  DECIMAL (16, 2) NULL,
    [EndRange]    DECIMAL (16, 2) NULL,
    [CommRate]    DECIMAL (16, 2) NULL
);

