﻿CREATE TABLE [dbo].[city_ZH_CN] (
    [SOURCECODE]    NCHAR (3)  NOT NULL,
    [COUNTRYCODE]   NCHAR (3)  NOT NULL,
    [CITYCODE]      NCHAR (3)  NOT NULL,
    [CITYNAME]      NCHAR (40) NOT NULL,
    [GreenwichTime] INT        NULL,
    [CREATEON]      DATETIME   NULL,
    [CREATEBY]      NCHAR (10) NULL,
    [UPDATEON]      DATETIME   NULL,
    [UPDATEBY]      NCHAR (10) NULL,
    [CallingCode]   NCHAR (6)  NULL
);

