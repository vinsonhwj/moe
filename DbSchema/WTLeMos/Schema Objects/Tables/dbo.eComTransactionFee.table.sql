﻿CREATE TABLE [dbo].[eComTransactionFee] (
    [termid]   NCHAR (16)   NOT NULL,
    [seqnum]   NCHAR (3)    NOT NULL,
    [feefrom]  NUMERIC (18) NULL,
    [feeTo]    NUMERIC (18) NULL,
    [amount]   NUMERIC (18) NULL,
    [pcent]    NUMERIC (18) NULL,
    [minimum]  NUMERIC (18) NULL,
    [maximum]  NUMERIC (18) NULL,
    [mpcent]   NUMERIC (18) NULL,
    [Createon] DATETIME     NULL,
    [Createby] NCHAR (10)   NULL,
    [Updateon] DATETIME     NULL,
    [Updateby] NCHAR (10)   NULL
);

