﻿CREATE TABLE [dbo].[shortform_prod_AirlineClass] (
    [Airline]   NCHAR (2) NOT NULL,
    [Class]     NCHAR (2) NOT NULL,
    [ClassType] INT       NOT NULL
);

