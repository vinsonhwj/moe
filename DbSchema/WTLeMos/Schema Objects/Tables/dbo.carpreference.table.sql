﻿CREATE TABLE [dbo].[carpreference] (
    [COMPANYID]     CHAR (2)     NOT NULL,
    [MembershipNo]  CHAR (16)    NOT NULL,
    [SeqNum]        CHAR (3)     NOT NULL,
    [Carcompany]    CHAR (50)    NULL,
    [CarType]       CHAR (4)     NULL,
    [CarPreference] CHAR (4)     NULL,
    [Createon]      DATETIME     NULL,
    [Createby]      CHAR (10)    NULL,
    [Updateon]      DATETIME     NULL,
    [Updateby]      CHAR (10)    NULL,
    [Remarks]       VARCHAR (50) NULL
);

