﻿CREATE TABLE [dbo].[Airport] (
    [COUNTRYCODE] NCHAR (3)     NOT NULL,
    [CITYCODE]    NCHAR (3)     NOT NULL,
    [AIRPORTCODE] NCHAR (5)     NOT NULL,
    [AIRPORTNAME] NVARCHAR (70) NULL,
    [CREATEDATE]  DATETIME      NULL,
    [CREATEBY]    NCHAR (10)    NULL,
    [UPDATEDATE]  DATETIME      NULL,
    [UPDATEBY]    NCHAR (10)    NULL
);

