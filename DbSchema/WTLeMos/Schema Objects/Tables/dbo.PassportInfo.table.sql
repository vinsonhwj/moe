﻿CREATE TABLE [dbo].[PassportInfo] (
    [COMPANYID]       NCHAR (2)      NOT NULL,
    [MembershipNo]    NCHAR (16)     NOT NULL,
    [Seqno]           NCHAR (3)      NOT NULL,
    [PassportCountry] NCHAR (2)      NULL,
    [PassportNo]      NVARCHAR (20)  NULL,
    [PassportName]    NVARCHAR (100) NULL,
    [PassportExpDate] DATETIME       NULL,
    [Birthday]        DATETIME       NULL,
    [Issueon]         DATETIME       NULL,
    [Countrycode]     NCHAR (2)      NULL,
    [Nationality]     NVARCHAR (20)  NULL,
    [PlaceOfBirth]    NVARCHAR (100) NULL,
    [Createon]        DATETIME       NULL,
    [Createby]        NVARCHAR (10)  NULL,
    [Updateon]        DATETIME       NULL,
    [Updateby]        NVARCHAR (10)  NULL,
    [PassportType]    NVARCHAR (50)  NULL,
    [FirstName]       NCHAR (30)     NULL,
    [LastName]        NCHAR (30)     NULL
);

