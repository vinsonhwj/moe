﻿CREATE TABLE [dbo].[fop_ZH_TW] (
    [COMPANYID]   NCHAR (2)  NOT NULL,
    [PaymentCode] NCHAR (3)  NOT NULL,
    [PaymentDesc] NCHAR (15) NULL,
    [Createon]    DATETIME   NULL,
    [Createby]    NCHAR (10) NULL,
    [Updateon]    DATETIME   NULL,
    [Updateby]    NCHAR (10) NULL
);

