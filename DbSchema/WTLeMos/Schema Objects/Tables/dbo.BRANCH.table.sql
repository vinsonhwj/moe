﻿CREATE TABLE [dbo].[BRANCH] (
    [COMPANYCODE]   NCHAR (2)      NOT NULL,
    [BRANCHCODE]    NCHAR (1)      NOT NULL,
    [BRANCHNAME]    NVARCHAR (40)  NULL,
    [branchaddr1]   NVARCHAR (50)  NULL,
    [branchaddr2]   NVARCHAR (50)  NULL,
    [branchaddr3]   NVARCHAR (50)  NULL,
    [branchaddr4]   NVARCHAR (50)  NULL,
    [postal]        NVARCHAR (10)  NULL,
    [CONTACTPERSON] NVARCHAR (40)  NULL,
    [CONTACTPHONE]  NVARCHAR (40)  NULL,
    [CITY]          NVARCHAR (30)  NULL,
    [COUNTRY]       NVARCHAR (30)  NULL,
    [footer]        NVARCHAR (500) NULL,
    [CREATEON]      DATETIME       NULL,
    [CREATEBY]      NVARCHAR (10)  NULL,
    [UPDATEON]      DATETIME       NULL,
    [UPDATEBY]      NVARCHAR (10)  NULL,
    [email]         NVARCHAR (100) NULL,
    [fax]           NVARCHAR (20)  NULL,
    [IATA]          VARCHAR (10)   NULL
);

