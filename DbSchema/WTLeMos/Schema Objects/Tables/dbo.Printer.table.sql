﻿CREATE TABLE [dbo].[Printer] (
    [PrinterID]   INT           NOT NULL,
    [PrinterName] VARCHAR (100) NOT NULL,
    [CreateOn]    DATETIME      NOT NULL,
    [CreateBy]    NVARCHAR (20) NOT NULL,
    [UpdateOn]    DATETIME      NULL,
    [UpdateBy]    NVARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([PrinterID] ASC)
);

