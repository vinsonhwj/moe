﻿CREATE TABLE [dbo].[othersegtype] (
    [SegType]     CHAR (3)    NOT NULL,
    [Description] CHAR (30)   NULL,
    [Updateon]    DATETIME    NULL,
    [Updateby]    CHAR (3)    NULL,
    [systemonly]  VARCHAR (1) NULL
);

