﻿CREATE TABLE [dbo].[WT_PRODUCTCODE_TYPE] (
    [Product_TypeCode] VARCHAR (4)  NOT NULL,
    [Description]      VARCHAR (50) NULL,
    [CreateOn]         DATETIME     NULL,
    [CreateBy]         VARCHAR (10) NULL,
    [UpdateOn]         DATETIME     NULL,
    [UpdateBy]         VARCHAR (10) NULL
);

