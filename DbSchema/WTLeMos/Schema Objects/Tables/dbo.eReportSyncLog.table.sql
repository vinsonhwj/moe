﻿CREATE TABLE [dbo].[eReportSyncLog] (
    [id]          INT          IDENTITY (1, 1) NOT NULL,
    [CompanyCode] VARCHAR (2)  NOT NULL,
    [Docnum]      VARCHAR (20) NOT NULL,
    [SyncTable]   VARCHAR (20) NOT NULL,
    [SyncStatus]  INT          NOT NULL,
    [SyncDate]    DATETIME     NULL,
    [CreateOn]    DATETIME     NOT NULL
);

