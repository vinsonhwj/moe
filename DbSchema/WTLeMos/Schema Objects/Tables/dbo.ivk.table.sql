﻿CREATE TABLE [dbo].[ivk] (
    [create_mth] INT             NULL,
    [teamcode]   NVARCHAR (5)    NULL,
    [cltcode]    NVARCHAR (8)    NULL,
    [cltname]    NVARCHAR (50)   NULL,
    [sell]       DECIMAL (38, 2) NULL,
    [cost]       DECIMAL (16, 2) NULL
);

