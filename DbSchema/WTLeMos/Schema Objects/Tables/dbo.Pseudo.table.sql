﻿CREATE TABLE [dbo].[Pseudo] (
    [ID]          INT          IDENTITY (1, 1) NOT NULL,
    [CompanyCode] CHAR (2)     NULL,
    [BranchCode]  CHAR (1)     NULL,
    [Pseudo]      CHAR (4)     NULL,
    [DQBType]     VARCHAR (50) NULL,
    [CREATEON]    DATETIME     NULL,
    [CREATEBY]    CHAR (10)    NULL,
    [UPDATEON]    DATETIME     NULL,
    [UPDATEBY]    CHAR (10)    NULL
);

