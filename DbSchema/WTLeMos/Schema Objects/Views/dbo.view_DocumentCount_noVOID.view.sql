﻿



CREATE VIEW dbo.view_DocumentCount_noVOID
AS
SELECT        m.companycode, m.teamcode, m.staffcode, m.createby, m.bkgref, m.masterpnr,
                              (SELECT         count(*)
                                FROM              PeoInv INV (noLock)
                                WHERE          Inv.bkgref = m.bkgref and Inv.voidOn is Null
				and Inv.invnum not in (select distinct isnull(void2jba,'') from Peoinv INV2 (nolock))				 
				 ) 
                          AS [Total Invoice],
                             ( (SELECT         count(*)
                                FROM              Peomco MCO (noLock)
                                WHERE          MCO.bkgref = m.bkgref and MCO.voidOn is Null )  +  
	            (SELECT         count(*)
                                FROM              PeoXO XO (noLock)
                                WHERE          XO.bkgref = m.bkgref and XO.voidOn is Null ) +
	            (SELECT         count(*)
                                FROM              Peotkt TKT (noLock)
                               WHERE          TKT.bkgref = m.bkgref and TKT.voidOn is Null ) +
	            (SELECT         count(*)
                                FROM              PeoVCH VCH (noLock)
                                WHERE          VCH.bkgref = m.bkgref and VCH.voidOn is Null ) ) 
	       AS [Total Document],
                              (SELECT         count(*)
                                FROM              PeoInv INV (noLock)
                                WHERE          SubString(inv.InvNum, 2, 1) = 'I' AND Inv.bkgref = m.bkgref and Inv.voidOn is Null				 
				 ) 
                          AS [Total Inv],
                              (SELECT         count(*)
                                FROM              PeoInv INV (noLock)
                                WHERE          SubString(inv.InvNum, 2, 1) = 'C' AND Inv.bkgref = m.bkgref and Inv.voidOn is Null				 
				and inv.invnum not in (select distinct isnull(void2jba,'') from Peoinv INV2 (nolock))
				 ) 
                          AS [Total Crn],
                              (SELECT         count(*)
                                FROM              PeoVCH VCH (noLock)
                                WHERE          VCH.bkgref = m.bkgref and VCH.voidOn is Null				 
				 ) 
                          AS [Total Vch],
                              (SELECT         count(*)
                                FROM              Peoxo XO(noLock)
                                WHERE          XO.bkgref = m.bkgref and XO.voidOn is Null				 
				 ) 
                          AS [Total Xo],
                              (SELECT         count(*)
                                FROM              Peotkt TKT (noLock)
                                WHERE          TKT.bkgref = m.bkgref and TKT.voidOn is Null				 
				 ) 
                          AS [Total TKT],
                              (SELECT         count(*)
                                FROM              Peomco MCO (noLock)
                                WHERE          MCO.bkgref = m.bkgref and MCO.voidOn is Null				 
				 ) 
                          AS [Total Mco]
FROM               dbo.peomstr m (noLock)
GROUP BY m.companycode, m.teamcode, m.staffcode, m.createby, m.bkgref, m.masterpnr




