﻿



/*
Logic:
Select all booking that satisfy the folling criteria
1. issue AT LEAST ONE Invoice or Credit Note
2. NEVER issue document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE VIEW dbo.view_DocumentReminder
AS
select top 100 percent 
ltrim(rtrim(m.companycode)) as companycode, 
ltrim(rtrim(m.teamcode)) as teamcode, 
ltrim(rtrim(m.staffcode)) as staffcode
, m.createby, m.bkgref, m.masterpnr
, m.createon as BookingDate
, m.departdate as sdeparture
, convert(char(11), m.departdate, 106) as departure
, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
, isnull(m.invcount, 0) as totalinvCount
, isnull(m.doccount, 0) as totaldocCount
from dbo.peomstr m (nolock)
where 
m.masterpnr in ( 
	select a.masterpnr
		from dbo.peomstr a (nolock)
	group by a.masterpnr
	having ( sum(isnull(a.[invcount], 0)) > 0 and sum(isnull(a.[doccount],0)) = 0)
	)
order by m.teamcode, m.staffcode, m.masterpnr




