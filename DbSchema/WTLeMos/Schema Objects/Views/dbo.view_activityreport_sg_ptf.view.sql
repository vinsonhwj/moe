﻿CREATE view dbo.view_activityreport_sg_ptf
as 
select 
peoinv.companycode, peoinv.teamcode, peoinv.staffcode, peoinv.createon, isnull(max(peoinv.mstrinvnum), '') as mstrinvnum
, (select cltode from peomstr where bkgref = (select bkgref from peoinv where invnum = x.invnum)) as cltcode
, isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
from peoinpax 
where invnum = x.invnum and companycode = peoinv.companycode order by paxseq asc), '') as pax

, case when max(x.routing) = '-' then 'MISC' else max(x.routing) end as routing
, case when max(x.flight) = '-' then 'MISC' else max(x.flight) end as flight
, case when max(x.class) = '-' then 'MISC' else max(x.class) end as class
, case when max(x.depart) = '-' then 'MISC' else max(x.depart) end as depart
, case when max(x.arrival) = '-' then 'MISC' else max(x.arrival) end as arrival
, min(x.servtype) as servtype

, sum(x.sellamt) as airfare
, sum(x.taxamt) as taxes
, sum(x.amtfee) as fee
, sum(x.othcharge) as othcharge
, x.invnum 

from (
-- select for non-UATP, UFSP ( not uatp invoice and not combine invoice)
(select
b.servtype  -- , b.servname, b.servdesc
, a.invnum
,
case 
when b.servtype = 'AIR' then a.sellamt * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' then convert(decimal, 0)
end as sellamt
, 
case 
when b.servtype = 'AIR' then a.taxamt * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' then convert(decimal, 0)
end as taxamt
, 
case 
when b.servtype = 'AIR' then a.amtfee * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' then convert(decimal, 0)
end as amtfee
, 
case 
when b.servtype = 'AIR' then convert(decimal, 0)
when b.servtype = 'HTL' then (a.sellamt + a.taxamt)* a.qty * a.rmnts
when b.servtype = 'OTH' then (a.sellamt + a.taxamt)* a.qty
end as othcharge
,
case 
when b.servtype = 'AIR' then convert(char(7), replace(right(rtrim(b.servname), 9), '   ', '/') )
when b.servtype = 'HTL' then '-'
when b.servtype = 'OTH' then '-'
end as routing
,
case
when b.servtype = 'AIR' then substring(b.servname, 1, 2) + ' ' + substring(b.servname, 3, 3)
when b.servtype = 'HTL' then '-'
when b.servtype = 'OTH' then '-'
end as flight
,
case
when b.servtype = 'AIR' then substring(b.servname, 10, 1)
when b.servtype = 'HTL' then '-'
when b.servtype = 'OTH' then '-'
end as class
, 
case
when b.servtype = 'AIR' then substring(b.servname, 16, 5)
when b.servtype = 'HTL' then substring(b.servdesc, 1, 5)
when b.servtype = 'OTH' then '-'
end as depart
, 
case
when b.servtype = 'AIR' then substring(b.servdesc, 8, 5)
when b.servtype = 'HTL' then substring(b.servdesc, 13, 5)
when b.servtype = 'OTH' then '-'
end as arrival
, a.segnum as pk
from peoinvdetail a, peoinvseg b
where 
a.segnum in 
(select segnum from peoinvseg where servtype in ('AIR', 'HTL', 'OTH') and invnum = a.invnum 
and companycode = a.companycode) 
and a.invnum = b.invnum and a.companycode = b.companycode and a.segnum = b.segnum
-- and a.invnum not in (select docnum from peonyref where invtypecode in('IUAT', 'RUAT', 'IUFS', 'RUFS') )
)

-- union
-- select for uatp invoice (ticket only)
-- select all ticket for UATP and Combine Invoice
--(
--select
--'UATP' as servtype
--, b.invnum
--, a.fullfare as sellamt
--, a.totaltax as taxamt
--, convert(decimal, 0) as amtfee
--, convert(decimal, 0) as othcharge
--, a.routing
--, a.airline + convert(char(3), aircode) as flight
--, 'MISC' as class
--, replace(convert(char, a.departon, 106), ' ', '') as depart
--, 'MISC' as arrival
--, a.ticket as pk
--from peotkt a, peoinvtkt b
--where 
--a.bkgref=b.bkgref and a.ticket = b.ticket and a.tktseq = b.tktseq
--)

--union 
-- select for Combine invoice other segments
-- select for non-UATP, UFSP ( not uatp invoice and not combine invoice)
--(select
--b.servtype  -- , b.servname, b.servdesc
--, a.invnum
--,
--case 
--when b.servtype = 'OTH' then convert(decimal, 0)
--end as sellamt
--, 
--case 
--when b.servtype = 'OTH' then convert(decimal, 0)
--end as taxamt
--, 
--case 
--when b.servtype = 'OTH' then convert(decimal, 0)
--end as amtfee
--, 
--case 
--when b.servtype = 'OTH' then (a.sellamt + a.taxamt)* a.qty
--end as othcharge
--,
--case 
--when b.servtype = 'OTH' then 'MISC'
--end as routing
--,
--case
--when b.servtype = 'OTH' then 'MISC'
--end as flight
--,
--case
--when b.servtype = 'OTH' then 'MISC'
--end as class
--, 
--case
--when b.servtype = 'OTH' then 'MISC'
--end as depart
--, 
--case
--when b.servtype = 'OTH' then 'MISC'
--end as arrival
--, a.segnum as pk
--from peoinvdetail a, peoinvseg b
--where 
--a.segnum in 
--(select segnum from peoinvseg where servtype in ('OTH') and invnum = a.invnum 
--and companycode = a.companycode) 
--and a.invnum = b.invnum and a.companycode = b.companycode and a.segnum = b.segnum
--and a.invnum in (select docnum from peonyref where invtypecode in('IUFS', 'RUFS') )
--)

) as x, peoinv
where peoinv.invnum = x.invnum 
and isnull(peoinv.void2jba, '') = ''
group by x.invnum, peoinv.companycode, peoinv.teamcode, peoinv.staffcode, peoinv.createon
-- , x.routing
-- , x.flight
-- , x.class

-- order by x.invnum








