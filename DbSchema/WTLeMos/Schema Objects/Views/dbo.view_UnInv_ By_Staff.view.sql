﻿


CREATE VIEW dbo.[view_UnInv_ By_Staff]
AS
SELECT         TEAMCODE, STAFFCODE, SUM(ISNULL([Total Voucher], 0) + ISNULL([Total XO], 
                          0) + ISNULL([Total TKT], 0) + ISNULL([Total MCO], 0)) AS DocAmt
FROM             dbo.BookSum_By_Staff
WHERE         (BKGREF NOT IN
                              (SELECT         BKGREF
                                FROM              PEOINV))
GROUP BY  STAFFCODE, TEAMCODE
HAVING          (SUM(ISNULL([Total Voucher], 0) + ISNULL([Total XO], 0) + ISNULL([Total TKT], 
                          0) + ISNULL([Total MCO], 0)) <> 0)



