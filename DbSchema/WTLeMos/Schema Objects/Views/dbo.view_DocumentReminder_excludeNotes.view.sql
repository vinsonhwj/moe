﻿



/*
Logic:
Select all booking that satisfy the folling criteria
1. issue AT LEAST ONE Invoice or Credit Note
2. NEVER issue document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE VIEW dbo.view_DocumentReminder_excludeNotes
AS
select top 100 percent a.* from (
SELECT TOP 100 percent
undoc.* 
from view_documentreminder undoc 
where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode)
Union
select top 100 percent 
undoc.* 
from view_documentreminder undoc, peoPFQue pfq
where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref
and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode)
and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document < 0
) as a  
order by a.teamcode, a.staffcode, a.masterpnr




