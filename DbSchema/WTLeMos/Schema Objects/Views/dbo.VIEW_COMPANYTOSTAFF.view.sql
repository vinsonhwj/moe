﻿



CREATE VIEW dbo.VIEW_COMPANYTOSTAFF
AS
SELECT         dbo.BRANCH.COMPANYCODE, dbo.BRANCH.BRANCHCODE, 
                          dbo.BRANCH.BRANCHNAME, ISNULL(dbo.TEAM.TEAMCODE, '') AS TEAMCODE, 
                          ISNULL(dbo.TEAM.TEAMNAME, '') AS TeamName, 
                          ISNULL(dbo.PEOSTAFF.STAFFCODE, '') AS staffcode, 
                          ISNULL(dbo.PEOSTAFF.STAFFNAME, '') AS staffname, ISNULL(dbo.TEAM.crs, '') 
                          AS CRS, ISNULL(dbo.TEAM.pseudo, '') AS pseudo, 
                          dbo.PEOSTAFF.SUSPENDDATE
FROM             dbo.BRANCH LEFT OUTER JOIN
                          dbo.TEAM ON dbo.BRANCH.COMPANYCODE = dbo.TEAM.COMPANYCODE AND 
                          dbo.BRANCH.BRANCHCODE = dbo.TEAM.BRANCHCODE LEFT OUTER JOIN
                          dbo.PEOSTAFF ON 
                          dbo.BRANCH.COMPANYCODE = dbo.PEOSTAFF.COMPANYCODE AND 
                          dbo.BRANCH.BRANCHCODE = dbo.PEOSTAFF.BRANCHCODE AND 
                          dbo.TEAM.TEAMCODE = dbo.PEOSTAFF.TEAMCODE




