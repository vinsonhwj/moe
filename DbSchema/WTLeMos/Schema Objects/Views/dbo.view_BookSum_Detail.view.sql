﻿CREATE VIEW dbo.view_BookSum_Detail
AS
SELECT     M.BKGREF, M.CLTODE, M.TEAMCODE, M.STAFFCODE, M.MASTERPNR, M.Companycode, M.Createby, ISNULL(I.[Total Invoice], 0) AS [Total Invoice], 
                      ISNULL(I.[Total Invoice exclude tax], 0) AS [Total Invoice exclude tax], ISNULL(I.[Total Invoice tax], 0) AS [Total Invoice tax], ISNULL(I.[Total invoice GST], 0) 
                      AS [Total invoice GST], ISNULL(I.[Total Credit Note], 0) AS [Total Credit Note], ISNULL(I.[Total Credit Note exclude tax], 0) AS [Total Credit Note exclude tax], 
                      ISNULL(I.[Total Credit Note tax], 0) AS [Total Credit Note tax], ISNULL(I.[Total Credit Note GST], 0) AS [Total Credit Note GST], ISNULL(V.[Total Voucher], 0) 
                      AS [Total Voucher], ISNULL(V.[Total Voucher exclude tax], 0) AS [Total Voucher exclude tax], ISNULL(V.[Total Voucher tax], 0) AS [Total Voucher tax], 
                      ISNULL(V.[Total Voucher GST], 0) AS [Total Voucher GST], ISNULL(X.[Total XO], 0) AS [Total XO], ISNULL(X.[Total XO exclude tax], 0) AS [Total XO exclude tax], 
                      ISNULL(X.[Total XO tax], 0) AS [Total XO tax], ISNULL(X.[Total XO GST], 0) AS [Total XO GST], ISNULL(T .[Total TKT], 0) AS [Total TKT], ISNULL(T .[Total TKT exclude tax],
                       0) AS [Total TKT exclude tax], ISNULL(T .[Total TKT tax], 0) AS [Total TKT tax], ISNULL(mco.[Total MCO], 0) AS [Total MCO], ISNULL(mco.[Total MCO exclude tax], 0) 
                      AS [Total MCO exclude tax], ISNULL(mco.[Total MCO tax], 0) AS [Total MCO tax], ISNULL(P.[Total Pax], 0) AS [Total Pax], M.DEPARTDATE, CONVERT(CHAR(10), 
                      M.Createon, 111) AS [BookingDate], ISNULL(rept.[Total Rept], 0) AS [Total Rept], ISNULL(rept.[Total Rept Tax], 0) AS [Total Rept Tax], ISNULL(rept.[Total Rept CR], 0) 
                      AS [Total Rept CR], ISNULL(rept.[Total Rept CR Tax], 0) AS [Total Rept CR Tax], ISNULL(I.[Total Credit Note Only], 0) [Total Credit Note Only], 
                      ISNULL(I.[Total Credit Note exclude tax Only], 0) AS [Total Credit Note exclude tax Only], ISNULL(I.[Total Credit Note tax Only], 0) AS [Total Credit Note tax Only]
FROM         dbo.PEOMSTR M(NOLOCK) LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'I' THEN ISNULL(HKDAMT, 0) ELSE 0 END) AS [Total Invoice], 
                                                   SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'I' THEN ISNULL(sellexcludetax, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 
                                                   1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Invoice exclude tax], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) 
                                                   = 'I' THEN ISNULL(taxamt, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Invoice tax], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) 
                                                   = 'I' THEN ISNULL(gstamt, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total invoice GST], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) 
                                                   = 'C' THEN ISNULL(HKDAMT, 0) ELSE 0 END) AS [Total Credit Note], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' THEN ISNULL(sellexcludetax, 0) 
                                                   / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) 
                                                   <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) 
                                                   AS [Total Credit Note exclude tax], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' THEN ISNULL(taxamt, 0) / (CASE WHEN ISNULL(sellamt, 0) 
                                                   = 0 THEN 1 ELSE ISNULL(sellamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) 
                                                   WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Credit Note tax], 
                                                   SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' THEN ISNULL(gstamt, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Credit Note GST], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' AND 
                                                   VOID2JBA IS NULL THEN ISNULL(HKDAMT, 0) ELSE 0 END) AS [Total Credit Note Only], SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' AND 
                                                   VOID2JBA IS NULL THEN ISNULL(sellexcludetax, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Credit Note exclude tax Only], SUM(CASE WHEN SUBSTRING(InvNum, 2, 
                                                   1) = 'C' AND VOID2JBA IS NULL THEN ISNULL(taxamt, 0) / (CASE WHEN ISNULL(sellamt, 0) = 0 THEN 1 ELSE ISNULL(sellamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(sellamt, 1) <> 0 THEN ISNULL(sellamt, 1) WHEN ISNULL(sellamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END) ELSE 0 END) AS [Total Credit Note tax Only]
                            FROM          PeoInv(NoLock)
                            GROUP BY CompanyCode, BkgRef) I ON I.CompanyCode = M.CompanyCode AND I.BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, SUM(ISNULL(hkdamt, 0)) AS [Total Voucher], SUM(ISNULL(costexcludetax, 0) / (CASE WHEN ISNULL(costamt, 0) 
                                                   = 0 THEN 1 ELSE ISNULL(costamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) <> 0 THEN ISNULL(costamt, 1) 
                                                   WHEN ISNULL(costamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total Voucher exclude tax], 
                                                   SUM(ISNULL(taxamt, 0) / (CASE WHEN ISNULL(costamt, 0) = 0 THEN 1 ELSE ISNULL(costamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) <> 0 THEN ISNULL(costamt, 1) WHEN ISNULL(costamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total Voucher tax], SUM(ISNULL(gstamt, 0) / (CASE WHEN ISNULL(costamt, 0) 
                                                   = 0 THEN 1 ELSE ISNULL(costamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) <> 0 THEN ISNULL(costamt, 1) 
                                                   WHEN ISNULL(costamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total Voucher GST]
                            FROM          PeoVch(NOLOCK)
                            WHERE      VoidOn IS NULL
                            GROUP BY CompanyCode, BkgRef) V ON V.CompanyCode = M.CompanyCode AND V.BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, SUM(ISNULL(hkdamt, 0)) AS [Total XO], SUM(ISNULL(costexcludetax, 0) / (CASE WHEN ISNULL(costamt, 0) 
                                                   = 0 THEN 1 ELSE ISNULL(costamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) <> 0 THEN ISNULL(costamt, 1) 
                                                   WHEN ISNULL(costamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total XO exclude tax], SUM(ISNULL(taxamt, 0) 
                                                   / (CASE WHEN ISNULL(costamt, 0) = 0 THEN 1 ELSE ISNULL(costamt, 1) / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) 
                                                   <> 0 THEN ISNULL(costamt, 1) WHEN ISNULL(costamt, 1) = 0 THEN 1 END) WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total XO tax], 
                                                   SUM(ISNULL(gstamt, 0) / (CASE WHEN ISNULL(costamt, 0) = 0 THEN 1 ELSE ISNULL(costamt, 1) 
                                                   / (CASE WHEN hkdamt = 0 THEN (CASE WHEN ISNULL(costamt, 1) <> 0 THEN ISNULL(costamt, 1) WHEN ISNULL(costamt, 1) = 0 THEN 1 END) 
                                                   WHEN hkdamt <> 0 THEN ISNULL(hkdamt, 1) END) END)) AS [Total XO GST]
                            FROM          PeoXO(NOLOCK)
                            WHERE      VoidOn IS NULL
                            GROUP BY CompanyCode, BkgRef) X ON X.CompanyCode = M.CompanyCode AND X.BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, SUM(ISNULL(NetFareHKD, ISNULL(NetFare, 0)) + ISNULL(TotalTaxHKD, ISNULL(TotalTax, 0))) AS [Total TKT], 
                                                   SUM(ISNULL(NetFareHKD, ISNULL(NetFare, 0))) AS [Total TKT exclude tax], SUM(ISNULL(totaltaxHKD, ISNULL(TotalTax, 0))) AS [Total TKT tax]
                            FROM          PeoTKT(NOLOCK)
                            WHERE      VoidOn IS NULL
                            GROUP BY CompanyCode, BkgRef) T ON T .CompanyCode = M.CompanyCode AND T .BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, SUM(ISNULL(costamt, 0) + ISNULL(taxamt, 0)) AS [Total MCO], SUM(ISNULL(costamt, 0)) AS [Total MCO exclude tax], 
                                                   SUM(ISNULL(taxamt, 0)) AS [Total MCO tax]
                            FROM          PeoMco(NOLOCK)
                            WHERE      VoidOn IS NULL
                            GROUP BY CompanyCode, BkgRef) mco ON mco.CompanyCode = M.CompanyCode AND mco.BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     CompanyCode, BkgRef, COUNT(1) AS [Total Pax]
                            FROM          PeoPax(NOLOCK)
                            WHERE      VoidOn IS NULL
                            GROUP BY CompanyCode, BkgRef) P ON P.CompanyCode = M.CompanyCode AND P.BkgRef = M.BkgRef LEFT OUTER JOIN
                          (SELECT     v.CompanyCode, v.BkgRef, SUM(CASE WHEN r.ReptType = 'IN' THEN ISNULL(r.PaidAmt, 0) ELSE 0 END) AS [Total Rept], 0 AS [Total Rept tax], 
                                                   SUM(CASE WHEN r.ReptType = 'CR' THEN ISNULL(r.PaidAmt, 0) ELSE 0 END) AS [Total Rept CR], 0 AS [Total Rept CR tax]
                            FROM          PeoInv v(NoLock) INNER JOIN
                                                   peoinvrept r(nolock) ON v.CompanyCode = r.CompanyCode AND v.InvNum = r.InvNum
                            GROUP BY v.CompanyCode, v.BkgRef) rept ON rept.CompanyCode = M.CompanyCode AND rept.BkgRef = M.BkgRef

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[13] 4[27] 2[51] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'USER', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'view_BookSum_Detail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'USER', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'view_BookSum_Detail';

