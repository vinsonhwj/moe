﻿


CREATE VIEW dbo.view_InvChk
AS
SELECT  (
	SELECT         SUM((TaxAmt + SellAmt + isnull(amtfee, 0)) * qty * rmnts)
                           FROM              peoinvdetail
                           WHERE          invnum = peoinv.invnum) AS detlamt, 
	(
	SELECT         SUM(isnull(amtfee, 0) * qty * rmnts)
                           FROM              peoinvdetail
                           WHERE          invnum = peoinv.invnum) AS detlFee,
	(
	SELECT         SUM(TaxAmt * qty * rmnts)
                           FROM              peoinvdetail
                           WHERE          invnum = peoinv.invnum) AS detlTax, 
	(
	SELECT         SUM(SellAmt * qty * rmnts)
                           FROM              peoinvdetail
                           WHERE          invnum = peoinv.invnum) AS detlSell, 
*
FROM             dbo.PEOINV




