﻿





CREATE VIEW dbo.Vi_AllModuleRights
AS
select f.modulecode,f.moduledesc,f.rightscode,e.companycode,e.login,
	case when e.rightscode is null then 'N' else 'Y' end as RightsFlag,
	case when e.modulecode is null then 'N' else 'Y' end as ModuleFlag,e.accessrights 
from 
(
select d.modulecode,d.moduledesc,a.rightscode  from 
(select b.modulecode,c.rightscode,b.MODULEDESC,c.RIGHTSDESC 
from module b, rightsref c ) d  
left  join modulerights a on a.modulecode=d.modulecode  ) f 
left join access e on f.modulecode=e.modulecode 
and f.rightscode=e.rightscode and e.accessrights=1








