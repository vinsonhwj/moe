﻿


CREATE VIEW dbo.view_BookingActivity
AS
select *
, INVAMT + INVTAX as BA_TotalInv 
, DOCAMT + DOCTAX as BA_TotalDoc
, (INVAMT + INVTAX) - (DOCAMT + DOCTAX) as BA_PMargin
, 
case when (INVAMT + INVTAX) <> 0 then
convert(decimal(16,2), (((INVAMT + INVTAX) - (DOCAMT + DOCTAX)) / (INVAMT + INVTAX) ) * 100 )
when (INVAMT + INVTAX) = 0 then convert(decimal(16, 2), .00)
end as BA_Yield
from peomstr




