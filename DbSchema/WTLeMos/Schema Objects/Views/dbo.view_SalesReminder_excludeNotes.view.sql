﻿



/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE VIEW dbo.view_SalesReminder_excludeNotes
AS
select top 100 percent a.* from (
SELECT TOP 100 percent
uninv.* 
from view_salesreminder uninv
where uninv.bkgref not in (select distinct bkgref from peoPFQue where companycode = uninv.companycode)
Union
select top 100 percent 
uninv.* 
from view_salesreminder uninv, peoPFQue pfq
where uninv.companycode = pfq.companycode and uninv.bkgref = pfq.bkgref
and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = uninv.bkgref and pfq1.companycode = uninv.companycode)
and ( uninv.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - uninv.Document < 0
) as a  
order by a.teamcode, a.staffcode, a.masterpnr




