﻿create VIEW dbo.view_AdnormalBooking_20Nov2003
AS
SELECT TOP 100 PERCENT 
ltrim(rtrim(b.companycode)) as companycode		-- modify by IVT on 29Sep2003, add companycode
, ltrim(rtrim(b.TEAMCODE)) as teamcode
, ltrim(rtrim(b.STAFFCODE)) as staffcode
, ltrim(rtrim(b.createby)) as createby
, b.BKGREF , b.MASTERPNR
, convert(datetime, b.BookingDate, 111) as BookingDate		
, b.DEPARTDATE AS sDeparture 	-- for searching
, CONVERT(CHAR(11), b.DEPARTDATE, 106) AS Departure	-- for display
, CAST(ISNULL(SUM(b.[Total Invoice]), 0) - ISNULL(SUM(b.[Total Credit Note]), 0) AS Decimal(20, 2)) AS Invoice
, CAST(ISNULL(SUM(b.[Total Voucher]), 0) + ISNULL(SUM(b.[Total XO]), 0) 
	+ ISNULL(SUM(b.[Total TKT]), 0)  + ISNULL(SUM(b.[Total MCO]), 0) AS Decimal(20,2)) AS Document
						-- for searching
FROM dbo.BookSum_By_Staff b  (NoLock) 
GROUP BY  b.BKGREF, b.TEAMCODE, b.STAFFCODE, b.DEPARTDATE, 
b.MASTERPNR, b.BookingDate, b.companycode, b.createby		-- modify by IVT on 29Sep2003, add companycode

having(
	(select isnull(sum(b1.[Total Invoice]), 0) - isnull(sum(b1.[Total Credit Note]), 0) - isnull(sum(b1.[Total Voucher]), 0) 
	- isnull(sum(b1.[Total XO]), 0) - isnull(sum(b1.[Total TKT]), 0) - isnull(sum(b1.[Total MCO]), 0)
	from BookSum_By_Staff b1  
	where b1.masterpnr = b.masterpnr
	group by b1.masterpnr) < 0
)
