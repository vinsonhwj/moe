﻿


/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
create VIEW dbo.view_SalesReminder_sg
AS
select distinct 
ltrim(rtrim(m.companycode)) as companycode, 
ltrim(rtrim(m.teamcode)) as teamcode, 
ltrim(rtrim(m.staffcode)) as staffcode
, m.createby, m.bkgref, m.masterpnr
, m.createon as BookingDate
, m.departdate as sdeparture
, convert(char(11), m.departdate, 106) as departure
, Cast(isnull(b.[Total Invoice], .00) - isnull(b.[Total Credit Note], .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(b.[Total TKT], .00) + isnull(b.[Total MCO], .00) + isnull(b.[Total Voucher], .00)
+ isnull(b.[Total XO], .00) as decimal(20,2)) as [Document]
from peomstr m (nolock), BookSum_By_Staff b (nolock)
-- 1.
where 
m.masterpnr in ( 
	select a.masterpnr
		from view_DocumentCount a 
	group by a.masterpnr
	having ( (sum(a.[Total Inv]) - sum(a.[Total Crn])) = 0 and sum(a.[Total Document]) > 0)
	)
and m.bkgref *= b.bkgref








