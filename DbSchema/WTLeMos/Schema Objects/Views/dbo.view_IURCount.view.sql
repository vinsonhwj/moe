﻿


CREATE VIEW dbo.view_IURCount
AS
SELECT         CONVERT(char(10), CreateOn, 111) AS IUR_Date, COUNT(*) AS NoOfIUR, 
                          MAX(DATEPART(ww, CreateOn)) AS Week, MAX(DATEPART(dw, CreateOn)) 
                          AS DOW
FROM             dbo.IURLog
GROUP BY  CONVERT(char(10), CreateOn, 111)



