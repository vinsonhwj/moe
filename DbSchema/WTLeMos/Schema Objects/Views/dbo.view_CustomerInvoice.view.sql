﻿


CREATE    VIEW dbo.view_CustomerInvoice
AS


select peoinv.companycode, peoinv.teamcode, peoinv.staffcode
, customerinvoice.createon
, customerinvoice.cltcode
, upper(left(customerinvoice.contactperson, 15)) as contactperson
, customerinvoice.invnum
, upper(left(customerinvoice.traveller, 25)) as traveller
, upper(left(customerinvoice.routing, 25)) as routing
, upper(left(customerinvoice.other, 15)) as other
, customerinvoice.departure
, customerinvoice.supplier
, customerinvoice.amount,
--  Modified by leander on 16Nov2005 	
--  update for adding the ticketnumber field	
   tktnum,
   class,
   flight
--  End Modified by leander on 16Nov2005 		
from 
(
	-- select AIR, HOTEL, OTHER segment for all inoivce / receipt except UATP and Combine Invoice
	select 

		convert(char(10), a.createon, 111) as createon
		, m.cltode as cltcode
		, m.contactperson
		, i.invnum 
		, m.firstpax as traveller
		-- , (select cltode from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=b.companycode) as cltcode	-- modify by Louie on 3.Oct.2004
		-- , (select contactperson from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=b.companycode) as contactperson		-- modify by Louie on 3.Oct.2004
		-- , (select firstpax from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=b.companycode) as traveller		-- modify by Louie on 3.Oct.2004
		, case 
		when b.servtype = 'AIR' then replace(right(rtrim(b.servname), 9), '   ', '/') 
		when b.servtype = 'HTL' then '-' -- b.servname											-- modify by IVT on 23Oct2003, add other column
		when b.servtype = 'OTH' then '-' -- b.servdesc 											-- modify by IVT on 23Oct2003, add other column
		end as routing
		, case 
		when b.servtype = 'AIR' then 'MISC'													-- added by IVT on 23Oct2003, add othe column
		when b.servtype = 'HTL' then 'Hotel'
--  Modified by leander on 16Nov2005 	
--  update for getting the servdesc as other when servdesc is not null
	--the old  when b.servtype = 'OTH' then isnull((select description from othersegtype_EN_US (READPAST) where segtype = b.segtype), b.servdesc)
          when b.servtype = 'OTH' then isnull( b.servdesc,(select description from othersegtype_EN_US (READPAST) where segtype = b.segtype))
--  End Modified by leander on 16Nov2005 		
		end as other	
--  Modified by leander on 16Nov2005 	
--  update for adding the tktnum
, 
'MISC' as tktnum
,  
 'MISC' as class
,   
'MISC' as flight
,
--  End Modified by leander on 16Nov2005 														-- end added by IVT on 23Oct2003
		 case 
		when b.servtype = 'AIR' then substring(b.servdesc, 8, 9)
		when b.servtype = 'HTL' then left(b.servdesc, 9) 
		-- when b.servtype = 'OTH' then (select replace(convert(char, departdate, 106), ' ', '') from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=a.companycode) 	-- modify by Louie on 3.Oct.2004
		when b.servtype = 'OTH' then replace(convert(char, m.departdate, 106), ' ', '') 
		end as departure
		, case 
		when b.servtype = 'AIR' then 'MISC'
		when b.servtype = 'HTL' then 'MISC'
		when b.servtype = 'OTH' then 'MISC' 
		end as supplier
		, 
		case
		when b.servtype = 'AIR' then ltrim(rtrim(isnull(convert(char, a.qty * a.rmnts * (a.sellamt + a.taxamt)), '-')))  + '/' + isnull(convert(char, a.qty * a.rmnts * (a.amtfee)), '-') 
		when b.servtype = 'HTL'  then isnull(convert(char, a.qty * a.rmnts * (a.sellamt + a.taxamt + a.amtfee)), '-') 
		when b.servtype = 'OTH'  then isnull(convert(char, a.qty * a.rmnts * (a.sellamt + a.taxamt + a.amtfee)), '-') 
		end as amount
		from peoinvdetail a (READPAST), peoinvseg b (READPAST)
		LEFT JOIN dbo.peoinv i (READPAST) ON b.COMPANYCODE=i.COMPANYCODE AND b.invnum=i.invnum 
		LEFT JOIN dbo.peomstr m (READPAST) ON m.COMPANYCODE=i.COMPANYCODE AND m.bkgref=i.bkgref
		LEFT JOIN dbo.peonyref r (READPAST) ON r.COMPANYCODE=b.COMPANYCODE AND r.docnum=b.invnum AND r.Createon = (SELECT MIN(Createon) FROM dbo.peonyref (READPAST) WHERE COMPANYCODE=r.COMPANYCODE and docnum=r.Docnum) 
		WHERE a.companycode = b.companycode and a.invnum = b.invnum and a.segnum = b.segnum
		AND ISNULL(r.invtypecode, '') NOT IN ('IUAT', 'RUAT', 'IUFS', 'RUFS')
		and b.servtype in ( 'AIR' , 'HTL' , 'OTH')
		-- and a.invnum not in (select docnum from peonyref where invtypecode in('IUAT', 'RUAT', 'IUFS', 'RUFS') and COMPANYCODE=a.COMPANYCODE )

union

-- select all ticket for UATP and Combine Invoice
select 
convert(char(10), b.createon, 111) as createon
-- , (select cltode from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = b.invnum and peoinv.companycode=b.companycode) and companycode=b.companycode) as cltcode			-- modify by Louie on 3.Oct.2004
-- , (select contactperson from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = b.invnum and peoinv.companycode=b.companycode) and companycode=b.companycode) as contactperson		-- modify by Louie on 3.Oct.2004
, m.cltode as cltcode
, m.contactperson
, b.invnum, a.paxname, a.routing
, 'MISC' as other																-- added by IVT on 23Oct2003, add other column
-- , convert(char, a.departon, 111) as departure
-- , a.ticket as supplier
, 
--  Modified by leander on 16Nov2005 	
--  update for adding the tktnum
a.ticket as tktnum
,
a.class as class
,
a.airline+' '+a.aircode as flight
,
--  End Modified by leander on 16Nov2005 
replace(convert(char, a.departon, 106), ' ', '') as departure											-- modify by IVT on 15Oct2003
, right('             ' + a.airline + ' ' + a.ticket, 13) as supplier											-- modify by IVT on 15Oct2003
, isnull(convert(char,(a.fullfare + a.totaltax)), '-') as amount 
from peotkt a (READPAST), peoinvtkt b (READPAST)
LEFT JOIN dbo.peomstr m (READPAST) ON m.COMPANYCODE=b.COMPANYCODE AND m.bkgref=b.bkgref
where 
-- a.invnum = b.invnum and 
a.bkgref=b.bkgref and a.ticket = b.ticket and a.tktseq = b.tktseq

union 

-- select other segment for Combine Invoice (IUFS, RUFS)
select 
convert(char(10), a.createon, 111) as createon
, m.cltode as cltcode
, m.contactperson
, i.invnum 
, m.firstpax as traveller
/*, (select cltode from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=a.companycode) as cltcode	-- modify by Louie on 3.Oct.2004
, (select contactperson from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=a.companycode) as contactperson		-- modify by Louie on 3.Oct.2004
, a.invnum 
, (select firstpax from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=a.companycode) as traveller		-- modify by Louie on 3.Oct.2004
*/
,
 
 case 
when b.servtype = 'OTH' then '-' -- b.servdesc 											-- modify by IVT on 23Oct2003, add other column
end as routing
, case 
--  Modified by leander on 16Nov2005 	
--  update for getting the servdesc as other when servdesc is not null
	--the old  when b.servtype = 'OTH' then isnull((select description from othersegtype_EN_US (READPAST) where segtype = b.segtype), b.servdesc)
          when b.servtype = 'OTH' then isnull( b.servdesc,(select description from othersegtype_EN_US (READPAST) where segtype = b.segtype))
--  End Modified by leander on 16Nov2005 	
end as other															-- end added by IVT on 23Oct2003
, 
--  Modified by leander on 16Nov2005 	
--  update for adding the tktnum
	  
'MISC' as tktnum
,  
 'MISC' as class
, 
'MISC' as flight
,
--  End Modified by leander on 16Nov2005
case 
-- when b.servtype = 'OTH' then (select replace(convert(char, departdate, 106), ' ', '') from peomstr where bkgref = (select bkgref from peoinv where peoinv.invnum = a.invnum and peoinv.companycode=a.companycode) and companycode=a.companycode)	-- modify by Louie on 3.Oct.2004
when b.servtype = 'OTH' then replace(convert(char, m.departdate, 106), ' ', '') 
end as departure
, case 
when b.servtype = 'OTH' then 'MISC' 
end as supplier
, isnull(convert(char, a.qty * a.rmnts * (a.sellamt + a.taxamt + a.amtfee)),'-') as amount
from peoinvdetail a (READPAST) , peoinvseg b (READPAST)
LEFT JOIN dbo.peoinv i (READPAST) ON b.COMPANYCODE=i.COMPANYCODE AND b.invnum=i.invnum 
LEFT JOIN dbo.peomstr m (READPAST) ON m.COMPANYCODE=i.COMPANYCODE AND m.bkgref=i.bkgref
LEFT JOIN dbo.peonyref r (READPAST) ON r.COMPANYCODE=b.COMPANYCODE AND r.docnum=b.invnum AND r.Createon = (SELECT MIN(Createon) FROM dbo.peonyref (READPAST) WHERE COMPANYCODE=r.COMPANYCODE and docnum=r.Docnum) 
where a.companycode = b.companycode and a.invnum = b.invnum and a.segnum = b.segnum 
-- and a.companycode = 'sg' 
and ( b.servtype in ( 'OTH') or (b.servtype in ('AIR') and b.segtype in ('SVC') )	)							-- modify by IVT on 24Oct2003, save combine invoice slave as 'SVC' type
-- and a.invnum in (select docnum from peonyref where invtypecode in('IUFS', 'RUFS') )
AND ISNULL(r.invtypecode, '') in ( 'IUFS', 'RUFS' )
) 
as CustomerInvoice, peoinv (READPAST)

where peoinv.invnum = CustomerInvoice.invnum 
and isnull(peoinv.void2jba, '') = '' 													-- added by IVT on 15Oct2003







