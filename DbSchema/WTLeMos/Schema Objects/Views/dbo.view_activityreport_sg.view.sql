﻿
CREATE  view view_activityreport_sg
as 
select 
peoinv.companycode, peoinv.teamcode, peoinv.staffcode, peoinv.createon, convert(varchar(10),peoinv.createon,103) as issuedate, 
isnull(max(peoinv.mstrinvnum), '') as mstrinvnum
, peoinv.cltcode
, 
case
when isnull(max(peoinv.mstrinvnum), '') = ''  then
isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
from peoinpax (nolock)
where invnum = x.invnum and companycode = peoinv.companycode order by paxseq asc), '')
when isnull(max(peoinv.mstrinvnum), '') <> '' then
isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
from peoinpax (nolock)
where invnum = isnull(max(peoinv.mstrinvnum), '') and companycode = peoinv.companycode order by paxseq asc), '')
end as pax
, (select pnr from peomstr mstr (nolock) where mstr.companycode = peoinv.companycode and mstr.bkgref = peoinv.bkgref) as pnr
, isnull(x.tkt1,'') as ticket
, substring(min(x.routing), 6, 100) as routing
, substring(min(x.flight), 6, 100) as flight
, substring(min(x.class), 6, 100) as class
, substring(min(x.depart), 6, 100) as depart
, substring(min(x.arrival), 6, 100) as arrival
, min(x.servtype) as servtype
, case 
when sum(x.sellamt) <> 0 then cast(sum(x.sellamt) as char(25))
when sum(x.sellamt) = 0 then '-'
end as airfare
, case  
when sum(x.taxamt) <> 0 then cast(sum(x.taxamt) as char(25))
when sum(x.taxamt) = 0 then '-'
end as taxes
, case
when sum(x.amtfee) <> 0 then cast(sum(x.amtfee) as char(25))
when sum(x.amtfee) = 0 then '-'
end as fee
, case when sum(x.othcharge) <> 0 then cast(sum(x.othcharge) as char(25))
when sum(x.othcharge) = 0 then '-' 
end as othcharge
, x.invnum 
from (
-- select for non-UATP, UFSP ( not uatp invoice and not combine invoice)
(select
a.invnum
, 
case when (SELECT count(*) FROM peoinvtktref (nolock) WHERE companycode = a.companycode and invnum = a.invnum)>0 then (SELECT top 1 Ticket FROM peoinvtktref (nolock) WHERE companycode = a.companycode and invnum = a.invnum)
when (SELECT count(*) FROM peoinvtkt (nolock) WHERE companycode = a.companycode and invnum = a.invnum)>0 then (SELECT top 1 Ticket FROM peoinvtkt (nolock) WHERE companycode = a.companycode and invnum = a.invnum)
else
	''
end
--(SELECT TOP 1 Ticket FROM 
--     (SELECT Ticket FROM peoinvtktref WHERE companycode = a.companycode and invnum = a.invnum
--     UNION SELECT Ticket FROM peoinvtkt WHERE companycode = a.companycode and invnum = a.invnum
--     ) as t
--   )
 as tkt1
,case 
when b.segtype <> 'SVC' and b.segtype <> 'DOC' then b.servtype  
when b.segtype = 'DOC' or b.segtype = 'SVC' then 'OTH'
end as servtype
-- , b.servname, b.servdesc
,
case 
when b.servtype = 'AIR' and b.segtype <> 'SVC' and b.segtype <> 'DOC' then a.sellamt * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' then convert(decimal, 0)
end as sellamt
, 
case 
when b.servtype = 'AIR' then a.taxamt * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' then convert(decimal, 0)
end as taxamt
, 
case 
when b.servtype = 'AIR' and b.segtype <> 'SVC' and b.segtype <> 'DOC' then a.amtfee * a.qty
when b.servtype = 'AIR' and b.segtype = 'SVC' or b.segtype = 'DOC' then a.sellamt * a.qty
when b.servtype = 'HTL' then convert(decimal, 0)
when b.servtype = 'OTH' and b.segtype <> 'HLE' then convert(decimal, 0)
when b.servtype = 'OTH' and b.segtype = 'HLE' then (a.sellamt + a.taxamt)* a.qty
end as amtfee
, 
case 
when b.servtype = 'AIR' then convert(decimal, 0)
when b.servtype = 'HTL' then (a.sellamt + a.taxamt)* a.qty * a.rmnts
when b.servtype = 'OTH' and b.segtype <> 'HLE' then (a.sellamt + a.taxamt)* a.qty
when b.servtype = 'OTH' and b.segtype = 'HLE' then convert(decimal, 0)
end as othcharge
,
case 
-- when b.servtype = 'AIR' then b.segnum + convert(char(7), replace(right(rtrim(b.servname), 9), '   ', '/') )
when b.servtype = 'AIR' then (select top 1 segnum + convert(char(7), replace(right(rtrim(servname), 9), '   ', '/') ) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum asc)
when b.servtype = 'HTL' then b.segnum + 'MISC'
when b.servtype = 'OTH' then b.segnum + 'MISC'
end as routing
,
case
-- when b.servtype = 'AIR' then b.segnum + substring(b.servname, 1, 2) + ' ' + substring(b.servname, 3, 3)
when b.servtype = 'AIR' then (select top 1 segnum + substring(servname, 1, 2) + ' ' + substring(servname, 3, 3) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum asc)
when b.servtype = 'HTL' then b.segnum + 'MISC'
when b.servtype = 'OTH' then b.segnum + 'MISC'
end as flight
,
case
-- when b.servtype = 'AIR' then b.segnum + substring(b.servname, 10, 1)
when b.servtype = 'AIR' then (select top 1 segnum + substring(servname, 10, 1) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum asc)
when b.servtype = 'HTL' then b.segnum + 'MISC'
when b.servtype = 'OTH' then b.segnum + 'MISC'
end as class
, 
case
-- when b.servtype = 'AIR' then b.segnum + substring(b.servname, 16, 5)
when b.servtype = 'AIR' then (select top 1 segnum + substring(servname, 16, 5) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum asc)
-- when b.servtype = 'HTL' then b.segnum + substring(b.servdesc, 1, 5)
when b.servtype = 'HTL' then (select top 1 segnum + substring(servdesc, 1, 5) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum asc)
when b.servtype = 'OTH' then b.segnum + 'MISC'
end as depart
, 
case
-- when b.servtype = 'AIR' then b.segnum + substring(b.servdesc, 8, 5)
when b.servtype = 'AIR' then (select top 1 segnum + substring(servdesc, 8, 5) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum desc)
-- when b.servtype = 'HTL' then b.segnum + substring(b.servdesc, 13, 5)
when b.servtype = 'HTL' then (select top 1 segnum + substring(servdesc, 13, 5) from peoinvseg (nolock) where invnum = b.invnum and companycode = b.companycode and servtype = b.servtype order by segnum desc)
when b.servtype = 'OTH' then b.segnum + 'MISC'
end as arrival
, a.segnum as pk
from peoinvdetail a (nolock), peoinvseg b (nolock)
where 
a.segnum in 
(select segnum from peoinvseg (nolock) where servtype in ('AIR', 'HTL', 'OTH') and invnum = a.invnum 
and companycode = a.companycode) 
and a.invnum = b.invnum and a.companycode = b.companycode and a.segnum = b.segnum
-- and a.invnum in( 'HI00001670', 'HI00001671')
-- and a.invnum not in (select docnum from peonyref where invtypecode in('IUAT', 'RUAT', 'IUFS', 'RUFS') )
)
) as x, peoinv (nolock)
where peoinv.invnum = x.invnum 
and substring(x.invnum, 2, 1) = 'I'
and isnull(peoinv.void2jba, '') = ''
group by x.invnum, x.tkt1, peoinv.invnum, peoinv.companycode, peoinv.bkgref, peoinv.teamcode, peoinv.staffcode, peoinv.createon, peoinv.cltcode







