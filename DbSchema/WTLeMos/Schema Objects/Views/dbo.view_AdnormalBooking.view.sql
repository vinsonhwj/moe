﻿



/*
Logic:
Select all booking that satisfy the folling criteria
1. Total selling < Total Costing, Total selling means (Total invoice amount - Total Credit note amount)
, Total Costing means (Total XO amount + Total VCH amount + Total TKT amount + Total MCO amount)
2. link with Master PNR
END Logic
*/
CREATE VIEW dbo.view_AdnormalBooking
AS
SELECT TOP 100 percent
ltrim(rtrim(b.companycode)) as companycode		-- modify by IVT on 29Sep2003, add companycode
, ltrim(rtrim(b.TEAMCODE)) as teamcode
, ltrim(rtrim(b.STAFFCODE)) as staffcode
, ltrim(rtrim(b.createby)) as createby
, b.BKGREF , b.MASTERPNR
, convert(datetime, b.createon, 111) as BookingDate		
, b.DEPARTDATE AS sDeparture 	-- for searching
, CONVERT(CHAR(11), b.DEPARTDATE, 106) AS Departure	-- for display
, CAST(ISNULL(SUM(b.[invamt]), 0) + ISNULL(SUM(b.[invtax]), 0) AS Decimal(20, 2)) AS Invoice
, CAST(ISNULL(SUM(b.[docamt]), 0)  + ISNULL(SUM(b.[doctax]), 0) AS Decimal(20,2)) AS Document
FROM dbo.peomstr b  (NoLock) 
GROUP BY  b.BKGREF, b.TEAMCODE, b.STAFFCODE, b.DEPARTDATE, 
b.MASTERPNR, b.createon, b.companycode, b.createby		-- modify by IVT on 29Sep2003, add companycode
having(
	(select 
 	CAST(ISNULL(SUM(b1.[invamt]), 0) + ISNULL(SUM(b1.[invtax]), 0) AS Decimal(20, 2)) 
	- CAST(ISNULL(SUM(b1.[docamt]), 0)  + ISNULL(SUM(b1.[doctax]), 0) AS Decimal(20,2))
	from dbo.peomstr b1
	where b1.masterpnr = b.masterpnr
	group by b1.masterpnr
	) < 0
and	-- not in Uninvoice reminder queue
	(
	sum(isnull(b.INVCOUNT, 0))
	) > 0
and -- not in Undocument reminder queue
	(
	sum(isnull(b.DOCCOUNT, 0))
	) > 0
)
order by b.teamcode, b.staffcode, b.masterpnr




