﻿----
CREATE view [view_CustomerViewOther]
as
SELECT DISTINCT top 100 percent
                      cust.COMPANYCODE, cust.CLTCODE, cust.CLTNAME, vo.Login
FROM         (SELECT     t.COMPANYCODE, t.TEAMCODE, o.Login
                       FROM          dbo.ViewOther AS o WITH (nolock) INNER JOIN
                                              dbo.TEAM AS t WITH (nolock) ON o.ViewCompany = t.COMPANYCODE
                       WHERE      (o.ViewBranch IS NULL) OR
                                              (t.BRANCHCODE = o.ViewBranch) AND (o.ViewTeam IS NULL) OR
                                              (t.BRANCHCODE = o.ViewBranch) AND (o.ViewTeam IS NOT NULL) AND (t.TEAMCODE = o.ViewTeam)) AS vo INNER JOIN
                      dbo.CUSTOMER AS cust ON cust.TEAMCODE = vo.TEAMCODE AND cust.COMPANYCODE = vo.COMPANYCODE
order by cust.CLTCODE
