﻿



CREATE VIEW dbo.BookSum
AS
SELECT         BKGREF, CLTODE,
                              (SELECT         SUM(sellexcludetax)
                                FROM              PeoInv I
                                WHERE          M.BkgRef = I.Bkgref AND SubString(InvNum, 2, 1) = 'I') 
                          AS [Total Invoice],
                              (SELECT         SUM(sellexcludetax)
                                FROM              PeoInv I
                                WHERE          M.BkgRef = I.Bkgref AND SubString(InvNum, 2, 1) = 'C') 
                          AS [Total Credit Note],
                              (SELECT         SUM(CostAmt)
                                FROM              PeoVch V
                                WHERE          M.BkgRef = V.Bkgref AND VoidOn IS NULL) AS [Total Voucher],
                              (SELECT         SUM(CostAmt)
                                FROM              PeoXO X
                                WHERE          M.BkgRef = X.Bkgref AND VoidOn IS NULL) AS [Total XO],
                              (SELECT         SUM(NetFare)
                                FROM              PeoTKT T
                                WHERE          M.BkgRef = T.Bkgref AND VoidOn IS NULL) AS [Total TKT]
FROM             dbo.PEOMSTR M




