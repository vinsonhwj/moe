﻿


/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE VIEW dbo.view_SalesReminder_excludeNotes_REVERISED
AS
select top 100 percent a.* from (
SELECT TOP 100 percent
uninv.* 
from view_salesreminder uninv (NOLOCK), peoPFQue pfq (NOLOCK)
where uninv.masterpnr = pfq.masterpnr AND pfq.AbnormalAmount IS NOT NULL
AND pfq.actiondate = (
		select MAX(pfq1.Actiondate) from peoPFQue pfq1 (NOLOCK) where pfq1.companycode = uninv.companycode
		and pfq1.masterpnr = pfq.masterpnr
		)
/*
	uninv.masterpnr not in 
	(
	select distinct pfq.masterpnr from peoPFQue pfq (NOLOCK) where pfq.companycode = uninv.companycode and 
	pfq.AbnormalAmount is not null and pfq.Actiondate = 
		(
		select MAX(pfq1.Actiondate) from peoPFQue pfq1 (NOLOCK) where pfq1.companycode = uninv.companycode
		and pfq1.masterpnr = pfq.masterpnr
		)
	)
*/
Union

select top 100 percent 
uninv.* 
from view_salesreminder uninv (NOLOCK), peoPFQue pfq (NOLOCK)
where uninv.companycode = pfq.companycode 
and uninv.masterpnr = pfq.masterpnr
and pfq.ActionDate = (select max(pfq1.Actiondate) from peoPFQue pfq1 (NOLOCK) where pfq1.companycode = uninv.companycode and pfq1.masterpnr = pfq.masterpnr)
and pfq.AbnormalAmount is not null
-- profit < AbnormalAmount
and pfq.AbnormalAmount > ( select sum(invamt+invtax-docamt-doctax) from peomstr mstr (NOLOCK) where mstr.companycode = uninv.companycode and mstr.masterpnr = pfq.masterpnr group by mstr.masterpnr )
) as a  
order by a.teamcode, a.staffcode, a.masterpnr











