﻿
CREATE VIEW [dbo].[view_DocumentCount]
AS
SELECT m.companycode, m.teamcode, m.staffcode, m.createby, m.bkgref, m.masterpnr,
	ISNULL(i.inv, 0) + ISNULL(i.crn, 0) AS [Total Invoice],
	ISNULL(V.vch, 0) + ISNULL(X.xo, 0) + ISNULL(T.tkt, 0) + ISNULL(mc.mco, 0) AS [Total Document],
	ISNULL(i.inv, 0) AS [Total Inv],
	ISNULL(i.crn, 0) AS [Total Crn],
	ISNULL(V.vch, 0) AS [Total Vch],
	ISNULL(X.xo, 0) AS [Total Xo],
	ISNULL(T.tkt, 0) AS [Total TKT],
	ISNULL(mc.mco, 0) AS [Total Mco],
	ISNULL(R.rept,0) AS [Total Rept]
FROM dbo.PeoMstr m (NOLOCK)
	LEFT OUTER JOIN (
		SELECT CompanyCode, BkgRef,
			SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'I' THEN 1 ELSE 0 END) AS inv,
			SUM(CASE WHEN SUBSTRING(InvNum, 2, 1) = 'C' THEN 1 ELSE 0 END) AS crn
		FROM PeoInv
		GROUP BY CompanyCode, BkgRef
	) I ON I.CompanyCode = m.CompanyCode AND I.BkgRef = m.BkgRef
	LEFT OUTER JOIN (
		SELECT CompanyCode, BkgRef,
			COUNT(1) AS vch
		FROM PeoVch
		GROUP BY CompanyCode, BkgRef
	) V ON V.CompanyCode = m.CompanyCode AND V.BkgRef = m.BkgRef
	LEFT OUTER JOIN (
		SELECT CompanyCode, BkgRef,
			COUNT(1) AS tkt
		FROM PeoTKT
		GROUP BY CompanyCode, BkgRef
	) T ON T.CompanyCode = m.CompanyCode AND T.BkgRef = m.BkgRef
	LEFT OUTER JOIN (
		SELECT CompanyCode, BkgRef,
			COUNT(1) AS xo
		FROM PeoXO
		GROUP BY CompanyCode, BkgRef
	) X ON X.CompanyCode = m.CompanyCode AND X.BkgRef = m.BkgRef
	LEFT OUTER JOIN (
		SELECT CompanyCode, BkgRef,
			COUNT(1) AS mco
		FROM PeoMco
		GROUP BY CompanyCode, BkgRef
	) mc ON mc.CompanyCode = m.CompanyCode AND mc.BkgRef = m.BkgRef
	LEFT OUTER JOIN (
		SELECT r.CompanyCode, v.BkgRef,
			COUNT(1) as rept
		FROM PeoInv v
			Inner Join PeoInvRept r
			on v.CompanyCode=r.CompanyCode and v.InvNum=r.InvNum
		GROUP BY r.CompanyCode, v.BkgRef
	) R ON R.CompanyCode = m.CompanyCode AND R.BkgRef = m.BkgRef
