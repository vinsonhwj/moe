﻿



/*
Logic:
Select all booking that satisfy the folling criteria
1. Total selling < Total Costing, Total selling means (Total invoice amount - Total Credit note amount)
, Total Costing means (Total XO amount + Total VCH amount + Total TKT amount + Total MCO amount)
2. link with Master PNR
END Logic
*/
CREATE VIEW dbo.view_AdnormalBooking_excludeNotes
AS
select top 100 percent a.* from (
SELECT TOP 100 percent
ab.* 
from view_adnormalbooking ab 
where ab.bkgref not in (select distinct bkgref from peoPFQue where companycode = ab.companycode)
Union
select top 100 percent 
ab.* 
from view_adnormalbooking ab, peoPFQue pfq
where ab.companycode = pfq.companycode and ab.bkgref = pfq.bkgref
and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = ab.bkgref and pfq1.companycode = ab.companycode)
and ( ab.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - ab.Document < 0
) as a  
order by a.teamcode, a.staffcode, a.masterpnr




