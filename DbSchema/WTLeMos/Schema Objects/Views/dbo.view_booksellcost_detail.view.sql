﻿



CREATE view dbo.view_booksellcost_detail
AS
SELECT BKGREF, CLTODE, TEAMCODE, STAFFCODE,MASTERPNR, companycode, createby,
			      (
				select
				(
				select isnull(sum(isnull(peoairdetail.qty, 0)* (isnull( peoairdetail.amt, 0) + isnull(peoairdetail.amtfee, 0)) / isnull(exrate.exrate,1)), 0)
				From peoairdetail left outer join  exrate on
				-- peoairdetail.COMPANYCODE=m.companycode AND peoairdetail.bkgref=M.bkgref and
				peoairdetail.companycode = exrate.companycode and 	peoairdetail.curr = exrate.sourcecurr 
				where
				exrate.TARGETCURR=(select localcurr from company where companycode=peoairdetail.companycode)
				 and peoairdetail.seqtype = 'SELL'
				AND peoairdetail.bkgref=M.bkgref
and peoairdetail.COMPANYCODE=m.companycode
				)
				+ 
			       	(select 	      isnull(sum(isnull(ph.totalsell, 0)), 0)
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref)
				+
				(select 	      isnull(sum(isnull(po.totalsell, 0)), 0)
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref)
				) as [Total selling],
			      (
				select 
				(select 	      isnull(sum(isnull(pa.totalstax, 0)), 0)
				from 		  peoair pa (NoLock)
				where 	       M.bkgref = pa.bkgref)
				+
				(select 	      isnull(sum(isnull(ph.totalstax, 0)), 0)
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref)
				+
				(select 	      isnull(sum(isnull(po.totalstax, 0)), 0)
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref)
				) as [Total selling tax],
			       (
				select 				
				(select  	( ( isnull(sum(isnull(ph.totalsell, 0) + isnull(ph.totalstax, 0)), 0) )
						/ (1+(1-(select (100 - isnull(gsttax, 0)) / 100 from company where companycode = M.companycode))))
						* (1 - (select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref and ph.gsttax='Y')
				+
				(select 	      ( ( isnull(sum(isnull(po.totalsell, 0) + isnull(po.totalstax, 0)), 0) )
						/ (1+(1-(select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))))
						* (1 - (select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref and po.gsttax='Y')
				) as [Total selling GST],
			      (
				select
				(select 	      isnull(sum(isnull(pa.totalcost, 0)), 0)
				from 		  peoair pa (NoLock)
				where 	       M.bkgref = pa.bkgref)
				+
				(select 	      isnull(sum(isnull(ph.totalcost, 0)), 0)
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref)
				+
				(select 	      isnull(sum(isnull(po.totalcost, 0)), 0)
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref)
				) as [Total costing],
			      (
				select
				(select 	      isnull(sum(isnull(pa.totalctax, 0)), 0)
				from 		  peoair pa (NoLock)
				where 	       M.bkgref = pa.bkgref)
				+
				(select 	      isnull(sum(isnull(ph.totalctax, 0)), 0)
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref)
				+
				(select 	      isnull(sum(isnull(po.totalctax, 0)), 0)
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref)
				) as [Total costing tax],
			       (
				select 				
				(select  	( ( isnull(sum(isnull(ph.totalcost, 0) + isnull(ph.totalctax, 0)), 0) )
						/ (1+(1-(select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))))
						* (1 - (select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))
				from 		  peohtl ph (NoLock)
				where 	       M.bkgref = ph.bkgref and ph.gsttax='Y')
				+
				(select 	      ( ( isnull(sum(isnull(po.totalcost, 0) + isnull(po.totalctax, 0)), 0) )
						/ (1+(1-(select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))))
						* (1 - (select (100 - isnull(gsttax, 0)) / 100  from company where companycode = M.companycode))
				from 		  peoother po (NoLock)
				where 	       M.bkgref = po.bkgref and po.gsttax='Y')
				) as [Total costing GST],
   DEPARTDATE, CONVERT(CHAR(10), Createon, 111) [BookingDate]
FROM             dbo.PEOMSTR M (NOLOCK) 
GROUP BY  BKGREF, CLTODE, TEAMCODE, STAFFCODE, DEPARTDATE, MASTERPNR, CONVERT(CHAR(10), Createon, 111), companycode, createby	-- modify by IVT on 29Sep2003, add companycode




