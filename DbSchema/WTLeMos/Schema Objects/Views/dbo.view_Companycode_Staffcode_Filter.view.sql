﻿



CREATE View dbo.view_Companycode_Staffcode_Filter
as
select distinct (companycode + '' + staffcode) as staffcode 
from peostaff 
where companycode in ( select distinct viewcompany 
			from viewother 
			where       isnull(viewcompany, '') <> ''  
				and isnull(viewbranch, '') <> ''  
				and isnull(viewteam, '') <> '' ) 
	and branchcode in ( select distinct viewbranch 
				from viewother 
				where  isnull(viewcompany, '') <> ''  
					and isnull(viewbranch, '') <> ''  
					and isnull(viewteam, '') <> '' ) 
	and teamcode in ( select distinct viewteam 
				from viewother 
				where  isnull(viewcompany, '') <> ''  
					and isnull(viewbranch, '') <> ''  
					and isnull(viewteam, '') <> '' ) 
UNION 
select distinct (companycode + '' +  staffcode) as staffcode 
from peostaff  
where companycode in ( select distinct G.viewcompany 
				from viewother G  
				inner join Login L on L.LoginGroup=G.Login  
					and L.companycode=G.companycode  
					and isnull(G.viewcompany, '') <> ''  
					and isnull(G.viewbranch, '') <> ''  
					and isnull(G.viewteam, '') <> '' ) 
	and branchcode in ( select distinct G.viewbranch 
				from viewother G  
				inner join Login L on L.LoginGroup=G.Login  
					and L.companycode=G.companycode  
					and isnull(G.viewcompany, '') <> ''  
					and isnull(G.viewbranch, '') <> ''  
					and isnull(G.viewteam, '') <> '' )  
	and teamcode in ( select distinct G.viewteam 
				from viewother G  
				inner join Login L on L.LoginGroup=G.Login  
					and L.companycode=G.companycode  
					and isnull(G.viewcompany, '') <> ''  
					and isnull(G.viewbranch, '') <> ''  
					and isnull(G.viewteam, '') <> '' )  
union  
select distinct (companycode + '' + staffcode) as staffcode 
from peostaff
where companycode in ( select distinct viewcompany 
				from viewother 
				where  isnull(viewcompany, '') <> '' 
					and isnull(viewbranch, '') <> '' 
					and isnull(viewteam, '') = '' ) 
	and branchcode in ( select distinct viewbranch 
					from viewother 
					where  isnull(viewcompany, '') <> '' 
					and isnull(viewbranch, '') <> '' 
					and isnull(viewteam, '') = '' ) 
UNION
select distinct (companycode + '' +staffcode) as staffcode 
from peostaff 
where companycode in (  select distinct G.viewcompany
				from viewother G
				inner join Login L on L.LoginGroup=G.Login  
					and L.companycode=G.companycode  
					and isnull(G.viewcompany, '') <> ''  
					and isnull(G.viewbranch, '') <> ''  
					and isnull(G.viewteam, '') = '' )
	and branchcode in ( select distinct G.viewbranch 
					from viewother G  
					inner join Login L on L.LoginGroup=G.Login  
						and L.companycode=G.companycode  
						and isnull(G.viewcompany, '') <> ''  
						and isnull(G.viewbranch, '') <> ''  
						and isnull(G.viewteam, '') = '' )  




