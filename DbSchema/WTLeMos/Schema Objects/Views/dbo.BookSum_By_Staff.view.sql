﻿



create VIEW dbo.BookSum_By_Staff
AS
-- SELECT         BKGREF, CLTODE, TEAMCODE, STAFFCODE,MASTERPNR, 
SELECT         BKGREF, CLTODE, TEAMCODE, STAFFCODE,MASTERPNR, companycode, createby, 	-- modify by IVT on 29Sep2003, add companycode
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)),0)
                                FROM              PeoInv I (NoLock)
                                WHERE          M.BkgRef = I.Bkgref AND SubString(InvNum, 2, 1) = 'I') 
                          AS [Total Invoice],
                              (SELECT         isnull(SUM(isnull(SellAmt,0)), 0)
                                FROM              PeoInv I (NoLock)
                                WHERE          M.BkgRef = I.Bkgref AND SubString(InvNum, 2, 1) = 'C') 
                          AS [Total Credit Note],
                              (SELECT         isnull(SUM(isnull(costamt,0)), 0)
                                FROM              PeoVch V (NoLock)
                                WHERE          M.BkgRef = V.Bkgref AND v.VoidOn IS NULL) AS [Total Voucher],
                              (SELECT         isnull(SUM(isnull(costamt,0)), 0)
                                FROM              PeoXO X (NoLock)
                                WHERE          M.BkgRef = X.Bkgref AND x.VoidOn IS NULL) AS [Total XO],
                              (SELECT         isnull(SUM(isnull(NetFare,0) + isnull(TotalTax,0)), 0)
                                FROM              PeoTKT T (NoLock)
                                WHERE          M.BkgRef = T.Bkgref AND t.VoidOn IS NULL) AS [Total TKT],
			-- added by IVT on 14Nov2003, add calculation for MCO
				(SELECT         isnull(SUM(isnull(mco.costamt,0) + isnull(mco.taxamt,0)), 0)
                                FROM              peomco mco (NoLock)
                                WHERE          M.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL) AS [Total MCO],
			-- end added by IVT on 14Nov2003
                              (SELECT         COUNT(*)
                                FROM              PeoPax P (NoLock)
                                WHERE          M.BkgRef = P.Bkgref AND p.VoidOn IS NULL) AS [Total Pax], 
                          DEPARTDATE, CONVERT(CHAR(10), Createon, 111) [BookingDate]
FROM             dbo.PEOMSTR M (NOLOCK) 
-- GROUP BY  BKGREF, CLTODE, TEAMCODE, STAFFCODE, DEPARTDATE, MASTERPNR, CONVERT(CHAR(10), Createon, 111)
GROUP BY  BKGREF, CLTODE, TEAMCODE, STAFFCODE, DEPARTDATE, MASTERPNR, CONVERT(CHAR(10), Createon, 111), companycode, createby	-- modify by IVT on 29Sep2003, add companycode




