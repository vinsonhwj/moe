﻿
CREATE      Procedure dbo.sp_RptSalesReminderNew
@COMPANY_CODE VARCHAR(2),
@TEAM_CODE    NVARCHAR(10),
@STAFF_CODE   NCHAR(20),
@START_DATE   VARCHAR(10),
@END_DATE     VARCHAR(10),
@SEARCH_MODE  CHAR(10),
@EXCLUDE_NOTE CHAR(1)
AS



Declare @SQL nvarchar(2500)

SELECT @SQL = 'select  ' +
'ltrim(rtrim(m.companycode)) as companycode,   ' +
'ltrim(rtrim(m.teamcode)) as teamcode,   ' +
'ltrim(rtrim(m.staffcode)) as staffcode  ' +
', m.createby, m.bkgref, m.masterpnr  ' +
', m.createon as BookingDate  ' +
', m.departdate as sdeparture  ' +
', m.cltode as cltcode ' +
', convert(char(11), m.departdate, 106) as departure  ' +
', Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]  ' +
', Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]  ' +
', isnull(m.invcount, 0) as totalinvCount  ' +
', isnull(m.doccount, 0) as totaldocCount  ' +
'into #tmpSalesReminder  ' +
'from dbo.peomstr m (nolock), MstrPnrTotal ttl (nolock)  ' + 
'where ttl.companycode = m.companycode and ttl.masterpnr = m.masterpnr ' + 
'and ttl.isLast = ''Y'' and ttl.invcount = 0 and ttl.doccount > 0 and ttl.approveon is null ' + 
'and m.companycode = ''' + @COMPANY_CODE + ''''
IF @STAFF_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND m.staffcode = ''' +  @STAFF_CODE + ''''
	END
IF @TEAM_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND m.teamcode = ''' +  @TEAM_CODE + ''''
	END 

IF @START_DATE <> ''
	BEGIN
		SELECT @SQL = @SQL + ' AND m.' + @SEARCH_MODE + '  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ' 23:59:59.999'''
	
	END
	
	
SELECT @SQL = @SQL + 'order by m.teamcode, m.staffcode, m.masterpnr;'

IF @EXCLUDE_NOTE = 'N' 
	begin
 	SELECT @SQL = @SQL + 
	'select a.*, case when isnull(c.cltcode,'''') = '''' then '''' else rtrim(c.cltcode) + ''-'' + c.cltname end as cltname, ' +
        'case when invoice = 0 or document = 0 then ''0'' else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'from #tmpSalesReminder a ' +
        'left outer join customer c (nolock) on c.companycode = a.companycode and c.cltcode = a.cltcode ' +
 	'where  ' +
	'not ( Invoice = 0 and Document = 0);' +
	'drop table #tmpSalesReminder;'
	end
ELSE
   --exclude note
   begin
	SELECT @SQL = @SQL + 	
	'select  a.*, case when isnull(c.cltcode,'''') = '''' then '''' else rtrim(c.cltcode) + ''-'' + c.cltname end as cltname into #tmpSalesReminderENR from ' +
        '( ' +
	'SELECT  ' +
	'uninv.*  ' +
	'from #tmpSalesReminder uninv ' +
	'where uninv.bkgref not in (select distinct bkgref from peoPFQue where companycode = uninv.companycode) ' +
	'Union ' +
	'select   ' +
	'uninv.*  ' +
	'from #tmpSalesReminder uninv, peoPFQue pfq ' +
	'where uninv.companycode = pfq.companycode and uninv.bkgref = pfq.bkgref ' +
	'and pfq.seqnum = (select right ( ''000'' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = uninv.bkgref and pfq1.companycode = uninv.companycode) ' +
	'and ( uninv.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - uninv.Document < 0 ' +
	') as a ' +
        'left outer join customer c (nolock) on c.companycode = a.companycode and c.cltcode = a.cltcode ' +
	'order by a.teamcode, a.staffcode, a.masterpnr; ' +
	'select *, ' +
	'case when invoice = 0 or document = 0 then ''0'' else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	' from #tmpSalesReminderENR where not ( Invoice = 0 and Document = 0);' +
	'DROP TABLE #tmpSalesReminder; ' +
	'DROP TABLE #tmpSalesReminderENR '
   end
PRINT @SQL
exec (@SQL)





















--exec sp_RptSalesReminderNew 'SG',' ALL','ALL','2007-01-01','2007-12-21','CREATEON','N'

