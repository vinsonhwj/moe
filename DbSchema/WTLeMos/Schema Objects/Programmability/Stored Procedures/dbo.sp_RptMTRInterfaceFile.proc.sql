﻿




CREATE    procedure dbo.sp_RptMTRInterfaceFile
@COMPANY_CODE nchar(2),
@MSTR_CLIENT_CODE nchar(16),
@START_DATE nvarchar(25),
@END_DATE nvarchar(25)
AS

CREATE TABLE #tmpResult
(
	Travel_no nvarchar(8)
,	Trip_Seq  int
,	Itn_Code   nchar(3)
,	Daily_Rate numeric(5,2)
,	Curr_Code  nchar(3) 
,	CurrAmt    numeric(16,2)
,	ExRate     numeric(5,2)
,	HKDAmt     numeric(16,2)
,	Agency_Code nchar(4)
,	Issue_By    nchar(4)
,  	Inv_No	    nchar(20)
,	Inv_date    datetime
,	Htl_Name    nvarchar(40)
,	Htl_Daily_Rate numeric(5,2)
,	Auto_Num     integer
)

DECLARE @lstr_TravelNo nvarchar(8), @lnum_TripSeq int, @lstr_Itn_Code nchar(3), @lnum_Daily_Rate numeric(5,2)
, @lstr_CurrCode nchar(3), @lnum_CurrAmt numeric(16,2), @lnum_ExRate numeric(5,2), @lnum_HKDAmt numeric(16,2)
, @lstr_AgencyCode nchar(4), @lstr_IssueBy nchar(4), @lstr_InvNo nchar(20), @ldt_InvDate datetime
, @lstr_HtlName nvarchar(40), @lnum_HtlDailyRate numeric(5,2), @lstr_OthDescr nvarchar(200)
, @lnum_AutoNum integer

select @lnum_AutoNum = 0 

--AIR
DECLARE lcur_Air cursor for
SELECT  rtrim(Mstr.COD1) as [Travel No], 0 as [Trip Seq], Itn.ItnCode as [Itinerary Code],
'HKD' as [Currency], detail.hkdamt as [Total Amount], 
'WTL' as [Agency Code],'CX' as [Issue By],
seg.invnum as [Invoice No], inv.createOn as [Invoice Date]
FROM peomstr Mstr (nolock), peoinv inv (nolock), peoinvseg seg(nolock), peoinvdetail detail (nolock)
, MstrClientForRpt MR (nolock), MstrItnCodeMapping Itn (nolock)
where Mstr.CompanyCode = inv.CompanyCode and Mstr.bkgref = Inv.Bkgref
and inv.companycode = seg.companycode and inv.invnum = seg.invnum 
and seg.companycode = detail.companycode and seg.invnum = detail.invnum and seg.segnum = detail.segnum
and inv.companycode = MR.companycode and inv.cltcode = MR.cltcode
and seg.companycode = Itn.Companycode and  seg.ServType = Itn.OtherSegType  AND Itn.MstrClientCode = MR.MstrClientCode 
and inv.companycode = @COMPANY_CODE 
and MR.MstrClientCode = @MSTR_CLIENT_CODE
and inv.Createon between @START_DATE and @END_DATE
and seg.ServType = 'AIR'

open lcur_Air
fetch lcur_Air into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate
while @@fetch_status = 0
begin

	SELECT @lnum_AutoNum = @lnum_AutoNum + 1 
	INSERT INTO #tmpResult
        (Travel_no,Trip_Seq,Itn_Code,Curr_Code,HKDAmt,Agency_Code,Issue_By,Inv_No,Inv_date, Auto_Num)
	VALUES
	(@lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate,@lnum_AutoNum)

	fetch lcur_Air into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate
end

close lcur_Air
deallocate lcur_Air

--Hotel
DECLARE lcur_HTL cursor for
SELECT  rtrim(Mstr.COD1) as [Travel No], 0 as [Trip Seq], Itn.ItnCode as [Itinerary Code],
'HKD' as [Currency], detail.hkdamt as [Total Amount], 
'WTL' as [Agency Code],'WTL' as [Issue By],
seg.invnum as [Invoice No], inv.createon as [Invoice Date],
seg.SERVNAME as [Hotel Name]
FROM peomstr Mstr (nolock), peoinv inv (nolock), peoinvseg seg(nolock), peoinvdetail detail (nolock)
, MstrClientForRpt MR (nolock), MstrItnCodeMapping Itn (nolock)
where Mstr.CompanyCode = inv.CompanyCode and Mstr.bkgref = Inv.Bkgref
and inv.companycode = seg.companycode and inv.invnum = seg.invnum 
and seg.companycode = detail.companycode and seg.invnum = detail.invnum and seg.segnum = detail.segnum
and inv.companycode = MR.companycode and inv.cltcode = MR.cltcode
and seg.companycode = Itn.Companycode and  seg.ServType = Itn.OtherSegType  AND Itn.MstrClientCode = MR.MstrClientCode 
and inv.companycode = @COMPANY_CODE 
and MR.MstrClientCode = @MSTR_CLIENT_CODE
and inv.Createon between @START_DATE and @END_DATE
and seg.ServType = 'HTL'

open lcur_HTL
fetch lcur_HTL into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate, @lstr_HtlName
while @@fetch_status = 0
begin
	SELECT @lnum_AutoNum = @lnum_AutoNum + 1 

	INSERT INTO #tmpResult
        (Travel_no,Trip_Seq,Itn_Code,Curr_Code,HKDAmt,Agency_Code,Issue_By,Inv_No,Inv_date,Htl_Name,Auto_Num)
	VALUES
	(@lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate,@lstr_HtlName,@lnum_AutoNum)

	fetch lcur_HTL into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate,@lstr_HtlName
end

close lcur_HTL
deallocate lcur_HTL

--Other Segement----
DECLARE lcur_OTH cursor for
SELECT  rtrim(isnull(Mstr.COD1,NULL)) as [Travel No], 0 as [Trip Seq], Itn.ItnCode as [Itinerary Code],
'HKD' as [Currency], detail.hkdamt as [Total Amount], 
'WTL' as [Agency Code],'WTL' as [Issue By],
seg.invnum as [Invoice No], inv.createon as [Invoice Date],
seg.SERVDESC as [SERVNAME]
FROM peomstr Mstr (nolock)
INNER JOIN peoinv inv (nolock) ON
Mstr.CompanyCode = inv.CompanyCode and Mstr.bkgref = Inv.Bkgref
INNER JOIN peoinvseg seg(nolock) ON
inv.companycode = seg.companycode and inv.invnum = seg.invnum 
INNER JOIN peoinvdetail detail (nolock) ON
seg.companycode = detail.companycode and seg.invnum = detail.invnum and seg.segnum = detail.segnum
INNER JOIN MstrClientForRpt MR (nolock) ON
inv.companycode = MR.companycode and inv.cltcode = MR.cltcode
LEFT OUTER JOIN MstrItnCodeMapping Itn (nolock) ON 
seg.companycode = Itn.Companycode and  seg.ServType = Itn.OtherSegType  AND Itn.MstrClientCode = MR.MstrClientCode 
WHERE inv.companycode = @COMPANY_CODE 
and MR.MstrClientCode = @MSTR_CLIENT_CODE
and inv.Createon between @START_DATE and @END_DATE
and seg.ServType not in ('HTL','AIR')


Declare @TargetCode varchar(10)
Declare @lnum_OTHCount int
SELECT @lnum_OTHCount = 0
open lcur_OTH
fetch lcur_OTH into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate, @lstr_OthDescr
while @@fetch_status = 0
begin
	
	SELECT @lnum_OTHCount = 0
	SELECT @lnum_AutoNum = @lnum_AutoNum + 1 
	While (rtrim(isnull(@lstr_OthDescr,'')) <> '' and CharIndex('^', @lstr_OthDescr)>0)
	Begin
		SELECT @lnum_OTHCount = @lnum_OTHCount + 1
	
		If left(@lstr_OthDescr,1) = '^'
		Begin
			Select @lstr_OthDescr = (Select Right(@lstr_OthDescr, Len(@lstr_OthDescr)-1))
		End
		Else
		Begin
			Select @TargetCode = (Select substring(@lstr_OthDescr, 0, CharIndex('^', @lstr_OthDescr)))
			
			If @TargetCode = NULL
				Select @TargetCode=@lstr_OthDescr
			IF @lnum_OTHCount = 1
		          BEGIN     
			     SELECT @lstr_CurrCode = @TargetCode
		          END
		        ELSE
		          IF @lnum_OTHCount = 2
		          BEGIN     
			     SELECT @lnum_CurrAmt = CAST(@TargetCode AS NUMERIC(16,2))
		          END
		        ELSE
		          IF @lnum_OTHCount = 3
		          BEGIN     
			     SELECT @lnum_ExRate = CAST(@TargetCode AS NUMERIC(5,2))
		          END
	
			If (Select CharIndex('^', @lstr_OthDescr)) > 0
				Select @lstr_OthDescr = (Select Right(@lstr_OthDescr, Len(@lstr_OthDescr) - Len(@TargetCode) - 1))
			Else
				
				Select @lstr_OthDescr = (Select Right(@lstr_OthDescr, Len(@lstr_OthDescr) - Len(@TargetCode)))
		End
	End
	
	INSERT INTO #tmpResult
        (Travel_no,Trip_Seq,Itn_Code,Curr_Code,Daily_Rate, CurrAmt, ExRate, HKDAmt,Agency_Code,Issue_By,Inv_No,Inv_date,Auto_Num)
	VALUES
	(@lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_Daily_Rate, @lnum_CurrAmt, @lnum_ExRate,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate,@lnum_AutoNum)

	fetch lcur_OTH into @lstr_TravelNo, @lnum_TripSeq,@lstr_Itn_Code,@lstr_CurrCode,@lnum_HKDAmt, @lstr_AgencyCode,@lstr_IssueBy,@lstr_InvNo,@ldt_InvDate, @lstr_OthDescr
end

close lcur_OTH
deallocate lcur_OTH


--Set Trip Seq
DECLARE @lnum_TmpTripSeq int, @lstr_TmpInvNo nchar(20)
DECLARE lcur_TripSeq cursor for
SELECT Inv_No, Auto_num FROM #tmpResult ORDER BY Inv_No,Inv_Date

SELECT @lnum_TmpTripSeq = 1
SELECT @lstr_TmpInvNo = NULL

open lcur_TripSeq
fetch lcur_TripSeq into @lstr_InvNo,@lnum_AutoNum
while @@fetch_status = 0
begin
	print @lstr_TmpInvNo + ' - ' + @lstr_InvNo
	IF @lstr_InvNo <> isnull(@lstr_TmpInvNo,'')
		SELECT @lnum_TmpTripSeq = 1
	ELSE
	begin
		SELECT @lnum_TmpTripSeq = @lnum_TmpTripSeq + 1
	end

	UPDATE #tmpResult SET Trip_Seq = @lnum_TmpTripSeq
        WHERE Auto_Num = @lnum_AutoNum
	
	SELECT @lstr_TmpInvNo = @lstr_InvNo
	fetch lcur_TripSeq into @lstr_InvNo,@lnum_AutoNum
end
close lcur_TripSeq
deallocate lcur_TripSeq



--Get Result
SELECT 
isnull(Travel_no,'') as [TravelNo], Trip_Seq as [TripSeq], isnull(Itn_Code,'') as [ItinCode]
, Curr_Code as [CurrCod], isnull(CurrAmt,0) as [CurrAmt]
, isnull(ExRate,0) as [ExRate], isnull(HKDAmt,0) as [TotAmt], Agency_Code as[AgentCod]
, Issue_By as [IssuBy], Inv_No as [InvNo], CONVERT(VARCHAR(10),Inv_Date,103) as [InvDate], isnull(Htl_Name,'') as HotelName
, isnull(Htl_Daily_Rate,0) as [HotelDailyRate]
FROM #tmpResult
ORDER BY Inv_No,Inv_Date


DROP TABLE #tmpResult





