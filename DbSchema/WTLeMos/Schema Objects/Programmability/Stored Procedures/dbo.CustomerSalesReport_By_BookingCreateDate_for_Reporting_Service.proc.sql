﻿



CREATE PROCEDURE dbo.CustomerSalesReport_By_BookingCreateDate_for_Reporting_Service
@StartDate CHAR(10),
@EndDate CHAR(10),
@TeamCode VarChar(10) = '%',
@CompanyCode VarChar(2) = 'SG', 
@Report_TypeCode VarChar(5) = 'SPRBC',
@StaffCode VarChar(5) = '%',
@VO CHAR(1) = 'T',
@userid char(5)
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
select @TeamCode =  '%' + @TeamCode + '%'
select @StaffCode = '%' + @StaffCode + '%'
			
SELECT         I.Bkgref, 
		I.CompanyCode,						
		I.StaffCode,
		I.TeamCode,
                              (SELECT         SUM(InvAmt + InvTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @StartDate AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode) 
                          AS [Total Invoice],
                              (SELECT         SUM(DocAmt + DocTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @StartDate AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode) 
                          AS [Total DocAmt],
                              ISNULL((SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND I.CompanyCode = p.CompanyCode
				AND P.CreateOn BETWEEN @StartDate AND @enddayofyear), 0) 
	        AS [Total Pax]
INTO #BookingSum
FROM              dbo.peomstr I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
AND I.companycode = @CompanyCode
GROUP BY I.Bkgref, I.CompanyCode, I.TeamCode, I.StaffCode
SELECT         I.Bkgref, 
		I.CompanyCode,						
		I.StaffCode,
		I.TeamCode,
                              ISNULL((SELECT         SUM(InvAmt + InvTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @firstdayofyear AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode), 0) 
                          AS [Total Invoice],
                              ISNULL((SELECT         SUM(DocAmt + DocTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @firstdayofyear AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode), 0) 
                          AS [Total DocAmt],
                              ISNULL((SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND I.CompanyCode = p.CompanyCode
				AND P.CreateOn BETWEEN @firstdayofyear AND @enddayofyear), 0) 
	        AS [Total Pax]
INTO #BookingSum_year_to_date
FROM              dbo.peomstr I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
AND I.companycode = @CompanyCode
GROUP BY I.Bkgref, I.CompanyCode, I.TeamCode, I.StaffCode
SELECT 
m.CompanyCode,
m.TeamCode,
m.StaffCode,
CONVERT(Decimal(20,2), SUM(b.[Total Pax])) AS [Total Pax],
CONVERT(Decimal(20,2), SUM(b.[Total Invoice])) AS [Price],
CONVERT(Decimal(20,2), SUM(b.[Total DocAmt])) AS [Cost],
(CONVERT(Decimal(20,2), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) AS [Margin],
CONVERT(Decimal(20,2), CASE 
	WHEN (CONVERT(Decimal(20,0),(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt])))) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), 0) 
	WHEN (CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), -100) 
	ELSE
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) /
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) * 100) END) AS [Yield(%)]
INTO #result_normal
FROM 
#BookingSum b,
peomstr m (nolock)
WHERE
m.CompanyCode = b.CompanyCode
AND m.bkgRef = b.bkgRef
AND m.TeamCode Like @TeamCode
AND m.StaffCode Like @StaffCode
GROUP BY m.CompanyCode, m.TeamCode, m.StaffCode
SELECT 
m.CompanyCode,
m.TeamCode,
m.StaffCode,
CONVERT(Decimal(20,2), SUM(b.[Total Pax])) AS [Total Pax],
CONVERT(Decimal(20,2), SUM(b.[Total Invoice])) AS [Price],
CONVERT(Decimal(20,2), SUM(b.[Total DocAmt])) AS [Cost],
(CONVERT(Decimal(20,2), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) AS [Margin],
CONVERT(Decimal(20,2), CASE 
	WHEN (CONVERT(Decimal(20,0),(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt])))) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), 0) 
	WHEN (CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), -100) 
	ELSE
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) /
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) * 100) END) AS [Yield(%)]
INTO #result_year_to_date
FROM 
#BookingSum_year_to_date b,
peomstr m (nolock)
WHERE
m.CompanyCode = b.CompanyCode
AND m.bkgRef = b.bkgRef
AND m.TeamCode Like @TeamCode
AND m.StaffCode Like @StaffCode
GROUP BY m.CompanyCode, m.TeamCode, m.StaffCode
IF @Report_TypeCode = 'SPRBC' 
BEGIN
	if @VO = 'T'
		BEGIN
SELECT ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
, ltrim(rtrim(a.staffcode)) as staffcode
, sum(a.[Total Pax]) as [Total Pax]
, sum(a.[Price]) as Price
, sum(a.[Cost])  as Cost 
, sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100) else 0 end as [Yield(%)]
,sum(b.Price)  as YTDPrice
, sum(b.Cost) as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100) else 0 end as [YTDYield(%)]
FROM 
#result_normal a, #result_year_to_date b
WHERE a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and b.companycode+b.staffcode  in (select distinct (companycode + '' + staffcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
GROUP BY a.companycode, a.teamcode, a.staffcode
ORDER BY a.staffcode
	END
	ELSE
	BEGIN
	SELECT ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
, ltrim(rtrim(a.staffcode)) as staffcode
, sum(a.[Total Pax]) as [Total Pax]
, sum(a.[Price]) as Price
, sum(a.[Cost])  as Cost 
, sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100) else 0 end as [Yield(%)]
,sum(b.Price)  as YTDPrice
, sum(b.Cost) as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100) else 0 end as [YTDYield(%)]
FROM 
#result_normal a, #result_year_to_date b
WHERE a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and b.companycode+b.staffcode  in (select staffcode from view_Companycode_Staffcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@userid)
GROUP BY a.companycode, a.teamcode, a.staffcode
ORDER BY a.staffcode
	END
END
DROP TABLE #BookingSum
DROP TABLE #BookingSum_year_to_date
DROP TABLE #result_normal
DROP TABLE #result_year_to_date
END




