﻿








create               PROCEDURE dbo.sp_AutomailRptStaffProdTxnD
@COMPANY_CODE VARCHAR(2),
@START_DATE VARCHAR(25),
@END_DATE VARCHAR(25)
AS


truncate table automail_stfprod_txn

DECLARE @lstr_YearStartDate VARCHAR(25)
SELECT @lstr_YearStartDate = LEFT(@START_DATE,4) + '-01-01'
 select 
  
 companycode, branchcode, teamcode,staffcode, 
   
 isnull((select count(*) from peopax (nolock)  
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE), 0) as peopax  ,  
  
 isnull((select sum(hkdamt) from peoinv (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE and   
 substring(invnum, 2, 1) = 'I'), 0) as peoinv  ,  
  
 isnull((select sum(hkdamt) from peoinv (nolock)  
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE and   
 substring(invnum, 2, 1) = 'C'), 0) as peocn  ,  
  
 isnull((select sum(netfare + totaltax) from peotkt (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE 
 ), 0) as peotkt  ,  
  
 isnull((select sum(netfare + totaltax) from peotkt (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @START_DATE and @END_DATE   
 ), 0) as peotktVoid  ,  
  
 isnull((select sum(hkdamt) from peoxo (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE    
 ), 0) as peoxo  ,  
  
 isnull((select sum(hkdamt) from peoxo (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @START_DATE and @END_DATE    
 ), 0) as peoxoVoid  ,  
  
 isnull((select sum(hkdamt) from peovch (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE  
 ), 0) as peovch  ,  
  
 isnull((select sum(hkdamt) from peovch (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @START_DATE and @END_DATE  
 ), 0) as peovchVoid  ,  
  
 isnull((select sum(costamt+taxamt) from peomco (nolock) 
 where bkgref in (select bkgref from peomstr (nolock) where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @START_DATE and @END_DATE    
 ), 0) as peomco  ,  
  
 isnull((select sum(costamt+taxamt) from peomco (nolock) 
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @START_DATE and @END_DATE    
 ), 0) as peomcoVoid ,  

 isnull((select sum(hkdamt) from peoinv (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE and   
 substring(invnum, 2, 1) = 'I'), 0) as peoinv2  ,  
  
 isnull((select sum(hkdamt) from peoinv (nolock)  
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE and   
 substring(invnum, 2, 1) = 'C'), 0) as peocn2  ,  
  
 isnull((select sum(netfare + totaltax) from peotkt (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE 
 ), 0) as peotkt2  ,  
  
 isnull((select sum(netfare + totaltax) from peotkt (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @lstr_YearStartDate and @END_DATE   
 ), 0) as peotktVoid2  ,  
  
 isnull((select sum(hkdamt) from peoxo (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE    
 ), 0) as peoxo2  ,  
  
 isnull((select sum(hkdamt) from peoxo (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @lstr_YearStartDate and @END_DATE    
 ), 0) as peoxoVoid2  ,  
  
 isnull((select sum(hkdamt) from peovch (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE  
 ), 0) as peovch2  ,  
  
 isnull((select sum(hkdamt) from peovch (nolock)   
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @lstr_YearStartDate and @END_DATE  
 ), 0) as peovchVoid2  ,  
  
 isnull((select sum(costamt+taxamt) from peomco (nolock) 
 where bkgref in (select bkgref from peomstr (nolock) where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 createon between @lstr_YearStartDate and @END_DATE    
 ), 0) as peomco2  ,  
  
 isnull((select sum(costamt+taxamt) from peomco (nolock) 
 where bkgref in (select bkgref from peomstr (nolock)  where teamcode = staff.teamcode and  staffcode = staff.staffcode and companycode = @COMPANY_CODE) and 
 companycode = @COMPANY_CODE and   
 voidon between @lstr_YearStartDate and @END_DATE    
 ), 0) as peomcoVoid2 
into #tmpRawData
 from peostaff staff
where companycode = @COMPANY_CODE

insert into automail_stfprod_txn
(companycode, branchcode, teamcode, staffcode, totalpax,price,cost,margin,ytodprice, ytodcost, ytodmargin)
select companycode, branchcode, teamcode, staffcode, peopax, 
(peoinv - peocn) as price, ((peotkt - peotktvoid) + (peoxo-peoxovoid) + (peovch - peovchvoid) + (peomco-peomcovoid)) as cost
,((peoinv - peocn) - ((peotkt - peotktvoid) + (peoxo-peoxovoid) + (peovch - peovchvoid) + (peomco-peomcovoid))) as Margin
,(peoinv2 - peocn2) as ytoDPrice, ((peotkt2 - peotktvoid2) + (peoxo2-peoxovoid2) + (peovch2 - peovchvoid2) + (peomco2-peomcovoid2)) as ytoDCost
,((peoinv2 - peocn2) - ((peotkt2 - peotktvoid2) + (peoxo2-peoxovoid2) + (peovch2 - peovchvoid2) + (peomco2-peomcovoid2))) as ytoDMargin
from #tmpRawData

UPDATE automail_stfprod_txn set yield = case when price = 0 then 0 
else ((price-cost)*100/price) end,ytodYield = case when ytodprice = 0 then 0 
else ((ytodprice-ytodcost)*100/ytodprice) end

drop table #tmpRawData






