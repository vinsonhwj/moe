﻿



CREATE procedure [dbo].[sp_XOOtherList]
@Companycode varchar(2)
, @bkgref varchar(10)
, @AutoXo varchar(1)
, @SegmentList varchar(500)

as



CREATE TABLE #tmpOther
(
	COMPANYCODE varchar(2) not null default ''
,	BKGREF varchar(10) not null default ''
,	SEQ int not null default ''
,	SEGNUM varchar(5) not null default ''
,	Suppcode varchar(8) not null default ''
,	OTHERDATE datetime not null 
,	SEGTYPE varchar(5) not null default ''
,	DESCRIPT1 nvarchar(120) not null default ''
,	DESCRIPT2 nvarchar(120) not null default ''
,	DESCRIPT3 nvarchar(120) not null default ''
,	DESCRIPT4 nvarchar(120) not null default ''
,	MASTERDESC nvarchar(4000) not null default ''
,	INVRMK1 nvarchar(120) not null default ''
,	INVRMK2 nvarchar(120) not null default ''
,	VCHRMK1 nvarchar(100) not null default ''
,	VCHRMK2 nvarchar(100) not null default ''
,	COUNTRYCODE varchar(10) not null default ''
,	CITYCODE varchar(10) not null default ''
,	CURR varchar(3) not null default ''
,	OTHAMT decimal(16,2) not null default 0
,	AGETYPE varchar(10) not null default ''
,	TAXCURR varchar(10) not null default ''
, 	TAXAMT decimal(16,2) not null default 0
, 	GSTTAX nchar(2) not null default ''
, 	QTY int not null default 0
)

-- Split Segnum into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpSegment (Segnum varchar(5) )
While (@SegmentList <> '')
Begin
	If left(@SegmentList,1) = '|'
	Begin
		Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@SegmentList, 0, CharIndex('|', @SegmentList)))
	
		If @TargetCode = ''
			Select @TargetCode=@SegmentList

		INSERT INTO #TmpSegment VALUES (@TargetCode)

		If (Select CharIndex('|', @SegmentList)) > 0
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode) - 1))
		Else
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

IF NOT EXISTS (SELECT Segnum FROM #TmpSegment)
begin
	INSERT INTO #tmpOther
	(COMPANYCODE, BKGREF, SEQ, SEGNUM,Suppcode, SEGTYPE 
	 , OTHERDATE, DESCRIPT1, DESCRIPT2, DESCRIPT3, DESCRIPT4
	 , MASTERDESC, INVRMK1, INVRMK2, VCHRMK1,VCHRMK2,CURR, OTHAMT
         , COUNTRYCODE, CITYCODE
	)
	SELECT oth.Companycode, oth.Bkgref, convert(int, oth.SEGNUM) as seq, oth.SEGNUM,oth.Suppliercode,oth.SEGTYPE
	,oth.OTHERDATE,DESCRIPT1,DESCRIPT2,DESCRIPT3,DESCRIPT4
	,isNull(masterDesc,'') as masterDesc,isnull(INVRMK1,'-') as INVRMK11 ,isnull(INVRMK2,'-') as INVRMK22,isnull(VCHRMK1,'-') as VCHRMK11 ,isnull(VCHRMK2,'-') as VCHRMK22 ,isnull(detail.curr,'') as othcurr, amt as othamt 
	,isnull(countrycode,'HK') as countrycode, isnull(citycode,'HKG') as citycode 
	 FROM peoother oth (NOLOCK), peootherdetail detail(NOLOCK) 
	 WHERE oth.companycode = detail.companycode AND oth.bkgref=detail.bkgref AND oth.segnum=detail.segnum 
	 AND detail.seqnum= (SELECT min(seqnum) FROM peootherdetail detail2 WHERE detail.companycode = detail2.companycode AND detail.segnum=detail2.segnum AND detail.bkgref=detail2.bkgref AND detail.seqtype=detail2.seqtype AND detail2.seqtype='COST' AND detail2

.qty * (detail2.amt+detail2.taxamt+detail2.amtfee) >= 0) 
	 AND oth.bkgref=@Bkgref AND oth.companycode = @Companycode
	 AND oth.segtype<>'GST' ORDER BY oth.OTHERDATE, oth.SEGNUM ASC

end
else
begin


	INSERT INTO #tmpOther
	(COMPANYCODE, BKGREF, SEQ, SEGNUM, Suppcode,SEGTYPE 
	 , OTHERDATE, DESCRIPT1, DESCRIPT2, DESCRIPT3, DESCRIPT4
	 , MASTERDESC, INVRMK1, INVRMK2, VCHRMK1,VCHRMK2,COUNTRYCODE, CITYCODE
	 , AGETYPE, TAXCURR, TAXAMT, CURR, OTHAMT
	 , GSTTAX, QTY
	)
	SELECT oth.Companycode, oth.Bkgref, convert(int, oth.SEGNUM) as seq, oth.segnum,oth.Suppliercode, oth.segtype
	, oth.OTHERDATE,DESCRIPT1,DESCRIPT2,DESCRIPT3,DESCRIPT4
	, isNull(oth.masterDesc,''),isnull(INVRMK1,'-'),isnull(INVRMK2,'-'),isnull(VCHRMK1,'-'),isnull(VCHRMK2,'-'),isnull(countrycode,'HK'), isnull(citycode,'HKG')
	, detail.agetype,detail.taxcurr, isnull(detail.taxamt,0),detail.curr, isnull(amt,0)
	, isnull(oth.gsttax,''),detail.qty
	FROM  peoother oth (nolock), peootherdetail detail (nolock), #TmpSegment seg
	WHERE detail.Companycode = oth.Companycode AND detail.bkgref = oth.bkgref AND detail.seqtype='COST'  AND detail.segnum = oth.segnum
	AND oth.segnum = seg.segnum
	AND oth.Companycode = @Companycode AND oth.bkgref=@Bkgref
	order by detail.segnum,detail.agetype,detail.curr,detail.taxcurr



end

if @AutoXo = 'Y'
begin
	SELECT COMPANYCODE, BKGREF, SEQ, SEGNUM, Suppcode,SEGTYPE 
		 , OTHERDATE, DESCRIPT1, DESCRIPT2, DESCRIPT3, DESCRIPT4 
		 , MASTERDESC, INVRMK1, INVRMK2, VCHRMK1,VCHRMK2,COUNTRYCODE, CITYCODE
		 , AGETYPE, TAXCURR, TAXAMT, CURR, OTHAMT
		 , GSTTAX, QTY
	FROM #tmpOther--  WHERE masterDesc like '%TIC Levy Charge for Receipt%' or masterDesc like '%Ticket Adjustment for Ticket%' or masterDesc like '%Credit Card Charge for Receipt%' or masterDesc like '%EPS Charge for Receipt%'
	ORDER BY SEQ
end
else
begin
	SELECT COMPANYCODE, BKGREF, SEQ, SEGNUM, Suppcode,SEGTYPE 
		 , OTHERDATE, DESCRIPT1, DESCRIPT2, DESCRIPT3, DESCRIPT4 
		 , MASTERDESC, INVRMK1, INVRMK2, VCHRMK1,VCHRMK2,COUNTRYCODE, CITYCODE
		 , AGETYPE, TAXCURR, TAXAMT, CURR, OTHAMT
		 , GSTTAX, QTY
	FROM #tmpOther--  WHERE masterDesc not like '%TIC Levy Charge for Receipt%'  and masterDesc not like '%Ticket Adjustment for Ticket%' or masterDesc not like '%Credit Card Charge for Receipt%' or masterDesc not like '%EPS Charge for Receipt%'
	ORDER BY SEQ

end
DROP TABLE #TmpSegment
DROP TABLE #tmpOther

--exec sp_InvOtherList 'WM','WMH0001430','00001'









