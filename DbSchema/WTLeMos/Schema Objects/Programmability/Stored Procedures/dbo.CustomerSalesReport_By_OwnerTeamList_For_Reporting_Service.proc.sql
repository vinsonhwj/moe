﻿



CREATE PROCEDURE dbo.CustomerSalesReport_By_OwnerTeamList_For_Reporting_Service
@StartDate CHAR(10) = '2004-06-17',
@EndDate CHAR(10) = '2004-06-17',
@TeamCode VarChar(10) = '',
@CompanyCode VarChar(2) = 'SC', 
@Report_TypeCode VarChar(4) = 'STPR', 	-- added by IVT on 12Nov2003, add Staff productivity report
@StaffCode VarChar(5) = '',
@userid varchar(5),
@VO varchar(2)	,	-- added by IVT on 12Nov2003, add Staff productivity report
@filtercode varchar(5)
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
--select @TeamCode =  '%' + @TeamCode + '%'
--select @StaffCode = '%' + @StaffCode + '%'
			
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear 
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C'  -- AND  vInv.cltCode = i.CltCode 
				AND VINV.CreateOn BETWEEN @StartDate AND @enddayofyear  ) 
                          AS [Total Credit Note],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @StartDate AND @enddayofyear)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @enddayofyear)  AS [Total XO],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @StartDate AND @enddayofyear) AS [Total TKT],
                              (SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND P.CreateOn BETWEEN @StartDate AND @enddayofyear ) AS [Total Pax]
INTO #BookingSum
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
and I.companycode = @CompanyCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
-- debug
-- select * from #BookingSum
-- debug
/* added by IVT on 07Nov2003, add year to date field */
SELECT         I.Bkgref, 
		-- i. TeamCode, 									-- commented by IVT
		i.companycode,									-- added by IVT
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear 
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C'  -- AND  vInv.cltCode = i.CltCode 
				AND VINV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear  ) 
    AS [Total Credit Note],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  AS [Total XO],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @firstdayofyear AND @enddayofyear) AS [Total TKT],
                              (SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND P.CreateOn BETWEEN @firstdayofyear AND @enddayofyear ) AS [Total Pax]
INTO #BookingSum_year_to_date
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
-- and I.bkgref in (select distinct bkgref from #BookingSum)	-- added by IVT on 12Nov2003
and I.companycode = @CompanyCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
-- debug
-- select * from #BookingSum_year_to_date
-- debug
select 
m.companycode,
m.teamcode,                                     
m.staffcode, 					-- added by IVT on 12Nov2003
CONVERT(Decimal(20,0), SUM(b.[Total Pax])) 
as  [Total Pax],
CAST(
ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) 
 AS Decimal(20,2) ) [Price],
CAST(
ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0)
 AS Decimal(20,2) ) [Cost],
CAST(
	(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0))
	 AS Decimal(20,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
	(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) )  = 0 THEN 
	CAST(
		0
	 AS Decimal(20,2) )
WHEN 	ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(20,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0))
	) /
		(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  * 100
	AS Decimal(20,2) )
END
into #result_year_to_date
From 
 #BookingSum_year_to_date b
, peomstr m (nolock)												-- added by IVT
where
-- b.TeamCode like @TeamCode										-- commented by IVT
b.companycode = @CompanyCode									-- added by IVT
and b.companycode = m.companycode 									-- added by IVT
and b.bkgref = m.bkgref											-- added by IVT
and 
(
m.teamcode in (select distinct teamcode from #tempteam )	-- added by IVT
or 
	(
	m.staffcode in ( select distinct staffcode from #tempstaff )	 
	and 
	m.teamcode  = (select teamcode from peostaff where staffcode = m.staffcode and companycode = m.companycode  )
	)
)
group by  m.companycode, m.teamcode
, m.staffcode				-- added by IVT on 12Nov2003
/* end added by IVT on 07Nov2003*/
-- debug
-- select * from #result_year_to_date
-- debug
select 
m.companycode,
m. teamcode,
m.staffcode, 		-- added by IVT on 12Nov2003
CONVERT(Decimal(20,0), SUM(b.[Total Pax])) 
as  [Total Pax],
CAST(
ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) 
 AS Decimal(20,2) ) [Price],
CAST(
ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0)
 AS Decimal(20,2) ) [Cost],
CAST(
	(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0))
	 AS Decimal(20,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
	(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) )  = 0 THEN 
	CAST(
		0
	 AS Decimal(20,2) )
WHEN 	ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(20,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0))
	) /
		(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  * 100
	AS Decimal(20,2) )
END
INTO #result_normal
From 
 #BookingSum b
, peomstr m (nolock)												-- added by IVT
where
-- b.TeamCode like @TeamCode										-- commented by IVT
b.companycode = @CompanyCode									-- added by IVT
and b.companycode = m.companycode 									-- added by IVT
and b.bkgref = m.bkgref											-- added by IVT
and 
(
m.teamcode in (select distinct teamcode from #tempteam )
or 
	(
	m.staffcode in ( select distinct staffcode from #tempstaff )	 
	and 
	m.teamcode  = (select teamcode from peostaff where staffcode = m.staffcode and companycode = m.companycode)
	)
)
group by  m.companycode, m.teamcode
, m.staffcode					-- added by IVT on 12Nov2003
-- debug
-- select * from #result_normal
-- debug
if @Report_TypeCode = 'TPR' -- team productivity report
begin 
if @VO  = 'T'
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]   
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.teamcode in (select distinct teamcode from #tempteam )
and b.companycode+b.teamcode  in (select distinct (companycode + '' + teamcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode, a.teamcode
order by a.teamcode
end
else
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]   
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.teamcode in (select distinct teamcode from #tempteam )
and b.companycode+b.teamcode  in (select staffcode from view_Companycode_Teamcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@filtercode) 
group by a.companycode, a.teamcode
order by a.teamcode
end
end
if @Report_TypeCode = 'TPRB' -- team productivity report - order by branch
begin 
if @VO='T'
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code 
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]       
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
and b.companycode+c.branchcode  in (select distinct (companycode + '' + branchcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode, c.branchcode
order by c.branchcode
end
else
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code 
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]       
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
and b.companycode+c.branchcode  in (select staffcode from view_Companycode_Branchcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@filtercode) 
group by a.companycode, c.branchcode
order by c.branchcode
end
end
if @Report_TypeCode = 'TPRC' -- team productivity report - order by branch
begin 
if @VO='T'
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]        
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
and b.companycode in (select distinct (companycode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode
order by a.companycode
end
else
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)]        
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
and b.companycode  in (@companycode) 
group by a.companycode
order by a.companycode
end
end
if @Report_TypeCode = 'SPR' -- Staff productivity report
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 ) else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)else 0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode in ( select distinct staffcode from #tempstaff  )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode)
group by a.companycode, a.teamcode, a.staffcode
order by a.staffcode
end
if @Report_TypeCode = 'SPRT' -- Staff productivity report - order by Team
begin
	if @VO = 'T'
	Begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
(sum(a.[Margin])/sum(a.[Price])) * 100  else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else 0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode in ( select distinct staffcode from #tempstaff)
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
and b.companycode+b.teamcode  in (select distinct (companycode + '' + teamcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode, a.teamcode
order by a.teamcode
end
else
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
(sum(a.[Margin])/sum(a.[Price])) * 100 else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else 0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode in ( select distinct staffcode from #tempstaff)
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
and b.companycode+b.teamcode  in (select staffcode from view_Companycode_Teamcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@filtercode) 
group by a.companycode, a.teamcode
order by a.teamcode
end
end
if @Report_TypeCode = 'SPRB' -- Staff productivity report - order by Branch
begin
	if @VO='T'
		begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
 (sum(a.[Margin])/sum(a.[Price])) * 100  else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else 0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff)
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode)
and b.companycode+c.branchcode  in (select distinct (companycode + '' + branchcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode, c.branchcode
order by c.branchcode
end
else
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
(sum(a.[Margin])/sum(a.[Price])) * 100  else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else  0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff)
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode)
and b.companycode+c.branchcode  in (select staffcode from view_Companycode_Branchcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@filtercode)
group by a.companycode, c.branchcode
order by c.branchcode
end
end
if @Report_TypeCode = 'SPRC' -- Staff productivity report - order by company
begin
if @VO='T'
Begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
(sum(a.[Margin])/sum(a.[Price])) * 100  else 0 end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else 0 end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff   )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
and b.companycode+b.staffcode in (select distinct (companycode + '' + staffcode) as staffcode from peostaff where isnull(staffcode, '') <> ''  and companycode in (select distinct viewcompany from viewother where companycode =@CompanyCode and login = @userid and isnull(viewcompany, '') <> '' UNION select distinct G.viewcompany from viewother G inner join Login L on L.LoginGroup=G.Login and L.CompanyCode=G.CompanyCode and L.LoginGroup=G.Login and L.companycode=@CompanyCode and L.Login=@userid and isNull(G.viewCompany, '') <> '' ))
group by a.companycode
order by a.companycode
end
else
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
,sum(a.[Total Pax]) as [Total Pax]
,sum(a.[Price])  as Price
,sum(a.[Cost])as Cost 
,sum(a.[Margin]) as Margin
,case when sum(a.[Price]) <> 0 then
(sum(a.[Margin])/sum(a.[Price])) * 100  else '-' end as [Yield(%)]
,sum(b.Price) as YTDPrice
,sum(b.Cost)  as YTDCost
,sum(b.Margin)  as YTDMargin
,case when sum(b.[Price]) <> 0 then
(sum(b.[Margin])/sum(b.[Price])) * 100 else '-' end as [YTDYield(%)] 
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff   )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
and b.companycode+b.staffcode  in (select staffcode from view_Companycode_Staffcode_Filter where substring(staffcode,1,2) = @companycode and substring(staffcode, 3,len(staffcode)-2)=@filtercode)
group by a.companycode
order by a.companycode
end
end
/* debug
select * from #BookingSum
select * from #BookingSum_year_to_date
select * from #result_normal
select * from #result_year_to_date
*/
DROP TABLE #BookingSum
DROP TABLE #BookingSum_year_to_date
DROP TABLE #result_normal
DROP TABLE #result_year_to_date
END




