﻿CREATE procedure dbo.sp_InvPassengerList
@Companycode varchar(2)
, @bkgref varchar(10)
, @SegmentList varchar(500)

as


CREATE TABLE #tmpPax
(
	COMPANYCODE varchar(2) not null default ''
,	BKGREF varchar(10) not null default ''
,	SEGNUM varchar(5) not null default ''
,	PAXLNAME nvarchar(50) not null default ''
,	PAXFNAME nvarchar(100) not null default ''
,	PAXTITLE nvarchar(8) not null default ''
,	PAXTYPE nvarchar(6) not null default ''
,	PAXAIR nvarchar(6) not null default ''
,	PAXHOTEL nvarchar(6) not null default ''
,	PAXOTHER nvarchar(6) not null default ''
,	PAXAGE int not null default 0
,	PAXSRC nvarchar(20) not null default ''
)

-- Split Segnum into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpSegment (Segnum varchar(5) )
While (@SegmentList <> '')
Begin
	If left(@SegmentList,1) = '|'
	Begin
		Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@SegmentList, 0, CharIndex('|', @SegmentList)))
	
		If @TargetCode = ''
			Select @TargetCode=@SegmentList

		INSERT INTO #TmpSegment VALUES (@TargetCode)

		If (Select CharIndex('|', @SegmentList)) > 0
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode) - 1))
		Else
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

IF NOT EXISTS (SELECT Segnum FROM #TmpSegment)
begin
	INSERT INTO #tmpPax
	(Companycode, bkgref, PAXLNAME, PAXFNAME, SEGNUM
         , PAXTITLE, PAXTYPE, PAXAIR, PAXAGE, PAXOTHER
	 , PAXSRC)
	SELECT COMPANYCODE, BKGREF, PAXLNAME, PAXFNAME, SEGNUM
         , PAXTITLE, PAXTYPE, PAXAIR, ISNULL(PAXAGE,0), PAXOTHER
	 , ISNULL(SOURCESYSTEM,'')
         FROM peopax (NOLOCK) 
         WHERE companycode=@Companycode AND bkgref= @bkgref AND deleted='0'

end
else
begin
	INSERT INTO #tmpPax
	(Companycode, bkgref, PAXLNAME, PAXFNAME, SEGNUM
         , PAXTITLE, PAXTYPE, PAXAIR, PAXAGE, PAXOTHER
	 , PAXSRC)
	SELECT COMPANYCODE, BKGREF, PAXLNAME, PAXFNAME, pax.SEGNUM
         , PAXTITLE, PAXTYPE, PAXAIR, ISNULL(PAXAGE,0), PAXOTHER
	 , ISNULL(SOURCESYSTEM,'')
         FROM peopax pax(NOLOCK), #TmpSegment seg 
         WHERE pax.segnum = seg.segnum
	 AND pax.companycode=@Companycode AND pax.bkgref= @bkgref AND deleted='0'


end

SELECT Companycode, bkgref, PAXLNAME, PAXFNAME, SEGNUM
         , PAXTITLE, PAXTYPE, PAXAIR, PAXAGE, PAXOTHER
	 , PAXSRC
FROM #tmpPax 
ORDER BY SEGNUM ASC

DROP TABLE #TmpSegment
DROP TABLE #tmpPax

--EXEC sp_InvPassengerList 'WM','WMH0001448','00001|00002'

