﻿



create    procedure dbo.sp_CalTotalBkg
	@COMPANYCODE varchar(2) = '',
	@BKGREF varchar(10) = '',
	@LOGIN varchar(10)

as


declare @ttlinv decimal(20, 2)
declare @ttlinvtax decimal(20, 2)
declare @ttldoc decimal(20, 2)
declare @ttldoctax decimal(20, 2)
declare @ttlinvcount integer
declare @ttldoccount integer
declare @ttlinvgst decimal(20, 2)
declare @ttldocgst decimal(20, 2)
declare @localcurr varchar(3)
declare @lstr_segnum varchar(4)

SELECT @localcurr = localcurr FROM company WHERE companycode = @COMPANYCODE


SELECT @ttlinv = isnull(sum(invamt),0)
, @ttlinvtax = isnull(sum(invtax),0)
, @ttldoc = isnull(sum(docamt),0)
, @ttldoctax = isnull(sum(doctax),0)
, @ttlinvcount = isnull(sum(invcount),0)
, @ttldoccount = isnull(sum(doccount),0)
, @ttlinvgst = isnull(sum(ttlinvgst),0)
, @ttldocgst = isnull(sum(ttldocgst),0)
FROM peomstr (nolock) 
WHERE companycode = @COMPANYCODE and bkgref = @BKGREF

SELECT @lstr_segnum = right(rtrim('0000' +convert(varchar,isnull(max(segnum)+1,1))),4)
FROM BkgTotal
WHERE COMPANYCODE = @COMPANYCODE AND bkgref = @BKGREF


if not exists(SELECT bkgref FROM BkgTotal WHERE companycode = @companycode and bkgref = @BKGREF AND ISLAST = 'Y' and (invamt = @ttlinv and docamt = @ttldoc and invtax = @ttlinvtax and doctax = @ttldoctax))
begin
	if exists (SELECT bkgref FROM BkgTotal WHERE companycode = @companycode and bkgref = @BKGREF)
	begin
		UPDATE BkgTotal set ISLAST = null WHERE companycode = @companycode and bkgref = @BKGREF
	end
	
	
	INSERT INTO BkgTotal VALUES
	(@companycode, @BKGREF, @lstr_segnum,@ttlinvcount,@ttldoccount,@ttlinvgst,@ttldocgst,@localcurr, @ttlinv, @localcurr, @ttlinvtax, @localcurr, @ttldoc, @ttldoctax, 'Y',getdate(),@LOGIN)
end





