﻿



create    procedure dbo.sp_TmpInitMasterData
@lstr_Companycode nchar(4),
@lstr_bkgref nvarchar(20)
as

	--Insert For New Bkg
	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, createby)
	select companycode, createon, masterpnr, bkgref, 'N' as type, createby 		
	from peomstr (nolock)
	where companycode = @lstr_Companycode and bkgref = @lstr_Bkgref

	INSERT INTO #tmpMasterPnrDoc 
	(companycode, txndate, masterpnr, bkgref, type, invamt, invtax, invgst, createby, invcount)
	select companycode, createon, masterpnr, bkgref, 'I' as type
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceAmountexcluetax else TotalInvoiceAmountexcluetax * -1 end) as invamt
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceTax else TotalInvoiceTax * -1 end) as invtax
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceGst else TotalInvoiceGst * -1 end) as invgst
	, Createby, 1 as invcount
	from
	(select i.companycode, i.createon, a.masterpnr, a.bkgref, i.invnum
	, isnull( (isnull(sellexcludetax,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0)  as TotalInvoiceAmountexcluetax
	, isnull( (isnull(taxamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceTax
	, isnull( (isnull(gstamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceGst
	, i.Createby
	from peoinv i(nolock), peomstr a (nolock)
	where i.companycode = a.companycode and i.bkgref = a.bkgref
	and a.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	) mstr



	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare, t.totaltax, 0, t.createby, 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	--and t.voidon is null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare * -1, t.totaltax * -1, 0, t.voidby , 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon is not null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	--and t.voidon is null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon is not null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'X' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	--and t.voidon is null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref,  'X' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon is not null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'T' as type
	, t.costamt, t.taxamt, 0, t.createby, 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	--and t.voidon is null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref, 'T' as type
	, t.costamt * -1, t.taxamt * -1, 0, t.voidby , 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon is not null


	--Insert For Approved
	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, createby)
	select companycode, approveon, masterpnr, bkgref, 'A' as type, createby 		
	from peomstr (nolock)
	where approveon is not null
	and companycode = @lstr_Companycode and bkgref = @lstr_Bkgref






