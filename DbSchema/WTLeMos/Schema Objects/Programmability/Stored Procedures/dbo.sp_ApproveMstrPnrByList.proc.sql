﻿





CREATE       procedure dbo.sp_ApproveMstrPnrByList
@COMPANYCODE nchar(4),
@MASTERPNRLIST varchar(2000),
@LOGIN varchar(10)


as 
 

Declare @TargetCode varchar(10)

While (@MASTERPNRLIST <> '')
Begin
	If left(@MASTERPNRLIST,1) = '|'
	Begin
		Select @MASTERPNRLIST = (Select Right(@MASTERPNRLIST, Len(@MASTERPNRLIST)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@MASTERPNRLIST, 0, CharIndex('|', @MASTERPNRLIST)))
	
		If @TargetCode = ''
			Select @TargetCode=@MASTERPNRLIST

		EXEC sp_ApproveMstrPnr @COMPANYCODE, @TargetCode, @LOGIN, 'A'

		If (Select CharIndex('|', @MASTERPNRLIST)) > 0
			Select @MASTERPNRLIST = (Select Right(@MASTERPNRLIST, Len(@MASTERPNRLIST) - Len(@TargetCode) - 1))
		Else
			Select @MASTERPNRLIST = (Select Right(@MASTERPNRLIST, Len(@MASTERPNRLIST) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

