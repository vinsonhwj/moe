﻿
CREATE                       PROCEDURE dbo.sp_RptBranchProductivityReport
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25)

AS


DECLARE @lstr_Staffcode char(10), @lstr_BranchCode char(3), @lstr_CltCode char(6), @lnum_Cost decimal(16,2)
, @ldt_DepartDate datetime, @lstr_bkgref nvarchar(20), @lnum_PaxTotal integer, @lstr_InvType char(3)
DECLARE @FinancalYear datetime, @ldt_StartDate datetime

-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------


print 'Start Report at ' + convert(varchar,getdate(),109)
------------------Invoice --------------------------------------------------
SELECT 
a.Companycode, a.bkgref, t.Branchcode, b.invnum, b.createon, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, isnull(b.invType,'') as invType
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), team t (nolock), #tempTeam Team
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND t.companycode = a.companycode and t.teamcode = a.teamcode
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND a.Teamcode = Team.Teamcode
GROUP BY a.companycode, a.bkgref, t.Branchcode, b.invnum, b.createon, b.invType
ORDER BY 1


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Branchcode varchar(10) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)


print 'Start Tkt at ' + convert(varchar,getdate(),109)
----------TKT-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, tkt.ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost  
, 'TKT', case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode  = Team.Teamcode
AND tkt.issueon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode




print 'Start Tkt void at ' + convert(varchar,getdate(),109)
----------TKT Void-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, tkt.ticket, tkt.voidon, ((tkt.netfare+tkt.totaltax) * -1) cost  
, 'TKT', case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode  = Team.TeamCode
AND tkt.voidon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode



print 'Start Xo at ' + convert(varchar,getdate(),109)
----------xo-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, xo.xonum, xo.createon, xo.hkdamt cost  
, 'XO', case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team 
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode = Team.TeamCode
AND xo.createon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode




print 'Start Xo void at ' + convert(varchar,getdate(),109)
----------xo Void-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, xo.xonum, xo.voidon, xo.hkdamt * -1 cost  
, 'XO', case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode  = Team.TeamCode
AND xo.voidon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode




print 'Start Voucher at ' + convert(varchar,getdate(),109)
----------vch-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, vch.vchnum, vch.createon, vch.hkdamt cost  
, 'VCH', case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), team t (nolock),#tempteam Team
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode = Team.TeamCode
AND vch.createon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode




print 'Start Voucher Void at ' + convert(varchar,getdate(),109)
----------vch Void-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, vch.vchnum, vch.voidon, vch.hkdamt * -1 cost  
, 'VCH', case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode = Team.TeamCode
AND vch.voidon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode




print 'Start Mco at ' + convert(varchar,getdate(),109)
----------mco-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, mco.mconum, mco.issueon, (mco.costamt + mco.taxamt) cost  
, 'MCO', case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode = Team.TeamCode
AND mco.issueon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode



print 'Start Mco void at ' + convert(varchar,getdate(),109)
----------mco Void-------------------------------------------------------
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, t.branchcode, mco.mconum, mco.voidon, (mco.costamt + mco.taxamt) * -1 cost  
, 'MCO', case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), team t (nolock), #tempteam Team 
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND t.companycode = mstr.companycode and t.teamcode = mstr.teamcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.teamcode  = Team.TeamCode
AND mco.voidon between @FinancalYear and @END_DATE
--GROUP BY t.branchcode



print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	CompanyCode varchar(10) null default ''
,	Branchcode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
, 	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)

INSERT INTO #TmpSellAmt (CompanyCode, Branchcode)
SELECT distinct COMPANYCODE, t.branchcode 
FROM Team t (nolock), #tempTeam Team
WHERE COMPANYCODE = @COMPANY_CODE AND t.TEAMCODE = Team.TeamCode




--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode


--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDPrice = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode


--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode


--Selling (MTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode 


--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode



--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, count(1) as counts
From #TmpDoc 
WHERE Issueon between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode




print 'Start Pax at ' + convert(varchar,getdate(),109)
--Passenger

UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.Branchcode, count(1) paxcount
FROM peopax pax(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
AND inv.createon between @START_DATE AND @END_DATE
AND pax.companycode = inv.companycode
GROUP BY inv.companycode, inv.Branchcode) i 
WHERE i.companycode = t.companycode and i.Branchcode = t.Branchcode


print 'END  YTD at ' + convert(varchar,getdate(),109)



SELECT Companycode
, branchcode as code, PaxTotal as [Total Pax]
, TktCount as [TktCount]
, CASE when Price <> 0 then CONVERT(VARCHAR,Price) else '-' END as Price
, CASE when Dep <> 0 then CONVERT(VARCHAR,Dep) else '-' END as Dep
, CASE WHEN cost <> 0 THEN CONVERT(VARCHAR,cost) else '-' END AS Cost
, CASE WHEN price-cost <> 0 THEN CONVERT(VARCHAR,price-cost) ELSE '-' END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then CONVERT(VARCHAR,YTDPrice) else '-' END as YTDPrice
, CASE when YTDDep <> 0 then CONVERT(VARCHAR,YTDDep) else '-' END as YTDDep
, CASE WHEN YTDcost <> 0 THEN CONVERT(VARCHAR,YTDcost) else '-' END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN CONVERT(VARCHAR,YTDprice-YTDcost) ELSE '-' END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]
FROM #TmpSellAmt
ORDER BY branchcode


-- Drop Table
drop table #TmpSellAmt;
drop table #tempteam;





