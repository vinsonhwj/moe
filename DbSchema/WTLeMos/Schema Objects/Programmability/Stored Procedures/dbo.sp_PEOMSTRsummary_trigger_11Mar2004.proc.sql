﻿CREATE PROCEDURE [dbo].[sp_PEOMSTRsummary_trigger_11Mar2004] 
	@companycode varchar(2) = '',
	@bkg varchar(10) = ''
AS
if @companycode <> '' and @bkg <> '' 
begin
	declare @ttlinv decimal(20, 2)
	declare @ttlinvtax decimal(20, 2)
	declare @ttldoc decimal(20, 2)
	declare @ttldoctax decimal(20, 2)
	declare @ttlsell decimal(20, 2)
	declare @ttlselltax decimal(20, 2)
	declare @ttlcost decimal(20, 2)
	declare @ttlcosttax decimal(20, 2)
	declare @localcurr varchar(3)
	declare @ttlinvcount integer
	declare @ttldoccount integer
	
		select  @ttlinv = (invdocamt.[Total Invoice exclude tax] - invdocamt.[Total Credit Note exclude tax])
			, @ttlinvtax = (invdocamt.[Total Invoice tax] - invdocamt.[Total Credit Note tax]) 
			, @ttldoc = (invdocamt.[Total Voucher exclude tax] + invdocamt.[Total XO exclude tax] + invdocamt.[Total TKT exclude tax] + invdocamt.[Total MCO exclude tax]) 
			, @ttldoctax = (invdocamt.[Total Voucher tax] + invdocamt.[Total XO tax] + invdocamt.[Total TKT tax] + invdocamt.[Total MCO tax]) 
			, @ttlsell = segamt.[Total selling]
			, @ttlselltax = segamt.[Total selling tax]
			, @ttlcost = segamt.[Total costing]
			, @ttlcosttax = segamt.[Total costing tax]
			, @ttlinvcount = doccount.[Total Invoice]
			, @ttldoccount = doccount.[Total Document]
		from dbo.view_booksum_detail invdocamt, dbo.view_booksellcost_detail segamt
			, dbo.view_DocumentCount doccount
		where invdocamt.bkgref = @bkg and invdocamt.companycode = @companycode
		and segamt.companycode = invdocamt.companycode
		and segamt.bkgref = invdocamt.bkgref
		and doccount.companycode = invdocamt.companycode
		and doccount.bkgref = invdocamt.bkgref
		
		select @localcurr = localcurr from company where companycode = @companycode
	
		if exists (select companycode from peomstr where bkgref = @bkg and companycode = @companycode) begin
			update peomstr 
			set 
			ttlsellcurr = @localcurr 
			, ttlselltaxcurr = @localcurr 
			, ttlcostcurr = @localcurr
			, ttlcosttaxcurr = @localcurr
			, ttlsellamt = @ttlsell
			, ttlselltax = @ttlselltax
			, ttlcostamt = @ttlcost
			, ttlcosttax = @ttlcosttax
			, invamt = @ttlinv
			, invtax = @ttlinvtax
			, docamt = @ttldoc
			, doctax = @ttldoctax
			, invcount = @ttlinvcount
			, doccount = @ttldoccount
			where companycode = @companycode and bkgref = @bkg
		end
end
