﻿








create                   PROCEDURE dbo.sp_RptCustomerSalesReportByTxnDate
@COMPANY_CODE	CHAR(2),
@START_DATE  	CHAR(10),
@END_DATE    	CHAR(25),
@CLT_CODE   	VARCHAR(50),
@CLT_MODE       CHAR(1),
@STAFF_CODE	VARCHAR(10)

AS

-- Get Client List
------------------------------------------------------------------------------------------------
CREATE TABLE #TmpClient
(
	CltCode varchar(50) default ''
)

EXEC sp_RptCustomerSalesReportByTxnDate_GetCltList @COMPANY_CODE, @CLT_CODE, @CLT_MODE, @STAFF_CODE
--END of get Client List------------------------------------------------------------------------------------------------



CREATE TABLE #TmpSellAmt
(
	CltCode varchar(50) default ''
,	Cltname nvarchar(200) default ''
,	Sell decimal(16,2) default 0.0
,	cost decimal(16,2) default 0.0
,  	YTDSell decimal(16,2) default 0.0
,	YTDCost decimal(16,2) default 0.0
)

IF EXISTS (SELECT * FROM #TmpClient)
BEGIN
	print '## Start Report Time:' + convert(varchar,getdate(),109)
	------------------Selling Part --------------------------------------------------
	INSERT INTO #TmpSellAmt
	SELECT 
	b.cltcode,isnull(c.cltname,'No Client Name') as cltname,
	sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell, cast('0' as decimal(16,2)) as cost,
	cast('0' as decimal(16,2)) as YTDSell, cast('0' as decimal(16,2)) as YTDCost
	FROM  #TmpClient tc, peoinv b (nolock)
	left outer join customer c (nolock) on b.companycode=c.companycode  AND b.cltcode=c.cltcode 
	WHERE b.companycode = @COMPANY_CODE
	AND b.createon BETWEEN @START_DATE AND @END_DATE
	AND b.cltcode = tc.cltcode
	GROUP BY b.cltcode,c.cltname
	
	print '## Start Tkt Time:' + convert(varchar,getdate(),109)
	
	
	---Cost--------------------------------------------------------------
	CREATE TABLE #TmpCostAmt
	(
		bkgref varchar(10) default ''
	,	cltcode varchar(50) default ''
	,	cost decimal(16,2) default 0
	,	invamt decimal(16,2) default 0
	)
	
	----------TKT-------------------------------------------------------
	INSERT INTO #TmpCostAmt
	select distinct tkttmp.bkgref,
	       isnull(i.cltcode,'') cltcode, tkttmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = tkttmp.companycode 
	 and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select tkt.companycode, 
	  tkt.bkgref, sum(tkt.netfare+tkt.totaltax) cost  
	 from peotkt tkt(nolock)
	 WHERE tkt.createon between @START_DATE and @END_DATE
	 AND tkt.companycode = @COMPANY_CODE
	 group by tkt.companycode, tkt.bkgref
	) tkttmp 
	left outer join  peoinv i (nolock)
	on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	
	print '## Start Loop Tkt Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	-- Void Tkt-----------------------------------------------------------------
	print '## Start Void Tkt Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct tkttmp.bkgref,
	       isnull(i.cltcode,'') cltcode, tkttmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = tkttmp.companycode and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select tkt.companycode, 
	  tkt.bkgref, sum(((tkt.netfare+tkt.totaltax)*-1)) cost  
	 from peotkt tkt(nolock)
	 WHERE tkt.Voidon between @START_DATE and @END_DATE
	 AND tkt.companycode = @COMPANY_CODE
	 group by tkt.companycode, tkt.bkgref
	) tkttmp
	left outer join  peoinv i (nolock)  
	on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	
	print '## Start Loop Void Tkt Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	
	-- Mco-----------------------------------------------------------------
	print '## Start mco Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct mcotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, mcotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = mcotmp.companycode 
	 and inv.bkgref = mcotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select mco.companycode, 
	  mco.bkgref, sum(mco.costamt + mco.taxamt) cost  
	 from peomco mco(nolock)
	 WHERE mco.createon between @START_DATE and @END_DATE
	 AND mco.companycode = @COMPANY_CODE
	 group by mco.companycode, mco.bkgref
	) mcotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = mcotmp.companycode and i.bkgref = mcotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop mco Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	-- Void Mco-----------------------------------------------------------------
	print '## Start Void mco Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct mcotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, mcotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = mcotmp.companycode 
	 and inv.bkgref = mcotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select mco.companycode, 
	  mco.bkgref, sum(((mco.costamt + mco.taxamt)*-1)) cost  
	 from peomco mco(nolock)
	 WHERE mco.voidon between @START_DATE and @END_DATE
	 AND mco.companycode = @COMPANY_CODE
	 group by mco.companycode, mco.bkgref
	) mcotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = mcotmp.companycode and i.bkgref = mcotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop Void mco Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	
	
	-- XO-----------------------------------------------------------------
	print '## Start xo Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct xotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, xotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = xotmp.companycode 
	 and inv.bkgref = xotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select xo.companycode, 
	  xo.bkgref, sum(xo.hkdamt) cost  
	 from peoxo xo(nolock)
	 WHERE xo.createon between @START_DATE and @END_DATE
	 AND xo.companycode = @COMPANY_CODE
	 group by xo.companycode, xo.bkgref
	) xotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = xotmp.companycode and i.bkgref = xotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop xo Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	-- Void XO-----------------------------------------------------------------
	print '## Start Void xo Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct xotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, xotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = xotmp.companycode 
	 and inv.bkgref = xotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select xo.companycode, 
	  xo.bkgref, sum(((xo.hkdamt)*-1)) cost  
	 from peoxo xo(nolock)
	 WHERE xo.voidon between @START_DATE and @END_DATE
	 AND xo.companycode = @COMPANY_CODE
	 group by xo.companycode, xo.bkgref
	) xotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = xotmp.companycode and i.bkgref = xotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop Void xo Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	
	
	-- Voucher-----------------------------------------------------------------
	print '## Start vch Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct vchtmp.bkgref,
	       isnull(i.cltcode,'') cltcode, vchtmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = vchtmp.companycode 
	 and inv.bkgref = vchtmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select vch.companycode, 
	  vch.bkgref, sum(vch.hkdamt) cost  
	 from peovch vch(nolock)
	 WHERE vch.createon between @START_DATE and @END_DATE
	 AND vch.companycode = @COMPANY_CODE
	 group by vch.companycode, vch.bkgref
	) vchtmp 
	left outer join  peoinv i (nolock)
	on i.companycode = vchtmp.companycode and i.bkgref = vchtmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop vch Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	-- Void Voucher-----------------------------------------------------------------
	print '## Start Void vch Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct vchtmp.bkgref,
	       isnull(i.cltcode,'') cltcode, vchtmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = vchtmp.companycode 
	 and inv.bkgref = vchtmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select vch.companycode, 
	  vch.bkgref, sum(((vch.hkdamt)*-1)) cost  
	 from peovch vch(nolock)
	 WHERE vch.voidon between @START_DATE and @END_DATE
	 AND vch.companycode = @COMPANY_CODE
	 group by vch.companycode, vch.bkgref
	) vchtmp 
	left outer join  peoinv i (nolock)
	on i.companycode = vchtmp.companycode and i.bkgref = vchtmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop Void vch Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'N'
	
	
	--=============================================================================================
	-- YEAR TO DATE
	DECLARE @ldt_StartDate datetime, @FinancalYear datetime
	select @ldt_StartDate = convert(datetime,@START_DATE)
	select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
	select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
	---------------------------------------------------------------------------------------------
	
	print '## Start YTD Selling Time: ' + convert(varchar,getdate(),109)
	
	SELECT 
	b.cltcode,
	sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
	INTO #TmpYTDSell
	FROM  #TmpSellAmt tc, peoinv b (nolock)
	WHERE b.companycode = @COMPANY_CODE
	AND b.createon BETWEEN @FinancalYear AND @END_DATE
	AND b.cltcode = tc.cltcode
	GROUP BY b.cltcode
	
	UPDATE #TmpSellAmt
	SET YTDSell = b.sell
	FROM #TmpSellAmt tc, #TmpYTDSell b (nolock)
	WHERE b.cltcode = tc.cltcode
	
	drop table #TmpYTDSell
	
	
	
	print '## Start YTD tkt Time:' + convert(varchar,getdate(),109)
	----------YTD TKT-------------------------------------------------------
	INSERT INTO #TmpCostAmt
	select distinct tkttmp.bkgref,
	       isnull(i.cltcode,'') cltcode, tkttmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = tkttmp.companycode 
	 and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select tkt.companycode, 
	  tkt.bkgref, sum(tkt.netfare+tkt.totaltax) cost  
	 from peotkt tkt(nolock)
	 WHERE tkt.createon between @FinancalYear and @END_DATE
	 AND tkt.companycode = @COMPANY_CODE
	 group by tkt.companycode, tkt.bkgref
	) tkttmp 
	left outer join  peoinv i (nolock)
	on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	
	print '## Start Loop YTD Tkt Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	-- Void Tkt-----------------------------------------------------------------
	print '## Start YTD Void Tkt Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct tkttmp.bkgref,
	       isnull(i.cltcode,'') cltcode, tkttmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = tkttmp.companycode and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select tkt.companycode, 
	  tkt.bkgref, sum(((tkt.netfare+tkt.totaltax)*-1)) cost  
	 from peotkt tkt(nolock)
	 WHERE tkt.Voidon between @FinancalYear and @END_DATE
	 AND tkt.companycode = @COMPANY_CODE
	 group by tkt.companycode, tkt.bkgref
	) tkttmp
	left outer join  peoinv i (nolock)  
	on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	
	print '## Start Loop YTD Void Tkt Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	
	-- Mco-----------------------------------------------------------------
	print '## Start YTD mco Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct mcotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, mcotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = mcotmp.companycode 
	 and inv.bkgref = mcotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select mco.companycode, 
	  mco.bkgref, sum(mco.costamt + mco.taxamt) cost  
	 from peomco mco(nolock)
	 WHERE mco.createon between @FinancalYear and @END_DATE
	 AND mco.companycode = @COMPANY_CODE
	 group by mco.companycode, mco.bkgref
	) mcotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = mcotmp.companycode and i.bkgref = mcotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD mco Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	-- Void Mco-----------------------------------------------------------------
	print '## Start YTD Void mco Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct mcotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, mcotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = mcotmp.companycode 
	 and inv.bkgref = mcotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select mco.companycode, 
	  mco.bkgref, sum(((mco.costamt + mco.taxamt)*-1)) cost  
	 from peomco mco(nolock)
	 WHERE mco.voidon between @FinancalYear and @END_DATE
	 AND mco.companycode = @COMPANY_CODE
	 group by mco.companycode, mco.bkgref
	) mcotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = mcotmp.companycode and i.bkgref = mcotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD Void mco Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	
	
	-- XO-----------------------------------------------------------------
	print '## Start YTD xo Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct xotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, xotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = xotmp.companycode 
	 and inv.bkgref = xotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select xo.companycode, 
	  xo.bkgref, sum(xo.hkdamt) cost  
	 from peoxo xo(nolock)
	 WHERE xo.createon between @FinancalYear and @END_DATE
	 AND xo.companycode = @COMPANY_CODE
	 group by xo.companycode, xo.bkgref
	) xotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = xotmp.companycode and i.bkgref = xotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD xo Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	-- Void XO-----------------------------------------------------------------
	print '## Start YTD Void xo Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct xotmp.bkgref,
	       isnull(i.cltcode,'') cltcode, xotmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = xotmp.companycode 
	 and inv.bkgref = xotmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select xo.companycode, 
	  xo.bkgref, sum(((xo.hkdamt)*-1)) cost  
	 from peoxo xo(nolock)
	 WHERE xo.voidon between @FinancalYear and @END_DATE
	 AND xo.companycode = @COMPANY_CODE
	 group by xo.companycode, xo.bkgref
	) xotmp 
	left outer join  peoinv i (nolock)
	on i.companycode = xotmp.companycode and i.bkgref = xotmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD Void xo Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	
	
	-- Voucher-----------------------------------------------------------------
	print '## Start YTD vch Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct vchtmp.bkgref,
	       isnull(i.cltcode,'') cltcode, vchtmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = vchtmp.companycode 
	 and inv.bkgref = vchtmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select vch.companycode, 
	  vch.bkgref, sum(vch.hkdamt) cost  
	 from peovch vch(nolock)
	 WHERE vch.createon between @FinancalYear and @END_DATE
	 AND vch.companycode = @COMPANY_CODE
	 group by vch.companycode, vch.bkgref
	) vchtmp 
	left outer join  peoinv i (nolock)
	on i.companycode = vchtmp.companycode and i.bkgref = vchtmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD vch Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'
	
	-- Void Voucher-----------------------------------------------------------------
	print '## Start YTD Void vch Time:' + convert(varchar,getdate(),109)
	INSERT INTO #TmpCostAmt
	select distinct vchtmp.bkgref,
	       isnull(i.cltcode,'') cltcode, vchtmp.cost,
	(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
	 from peoinv inv (nolock)
	 where inv.companycode = vchtmp.companycode 
	 and inv.bkgref = vchtmp.bkgref and inv.cltcode = i.cltcode
	) invamt
	from #TmpClient tc,
	(select vch.companycode, 
	  vch.bkgref, sum(((vch.hkdamt)*-1)) cost  
	 from peovch vch(nolock)
	 WHERE vch.voidon between @FinancalYear and @END_DATE
	 AND vch.companycode = @COMPANY_CODE
	 group by vch.companycode, vch.bkgref
	) vchtmp 
	left outer join  peoinv i (nolock)
	on i.companycode = vchtmp.companycode and i.bkgref = vchtmp.bkgref 
	WHERE i.cltcode = tc.cltcode and i.companycode = @COMPANY_CODE
	
	print '## Start Loop YTD Void vch Time:' + convert(varchar,getdate(),109)
	exec sp_RptCustomerSalesReportByTxnDate_CalCostProportion @COMPANY_CODE,'Y'

	DROP TABLE #TmpCostAmt;

END	
	

SELECT Cltcode as [Cltcode], cltname as [Cltname]
, CONVERT(Decimal(20,2), sell) as [Price], CONVERT(Decimal(20,2), cost) as [Cost], CONVERT(Decimal(20,2), sell - cost) as [Margin]
, CONVERT(Decimal(20,2), case isnull(sell,0) when 0 then 0 else ((sell-cost)*100/sell) end) as [Yield(%)]
, CONVERT(Decimal(20,2), YTDSell) as [YTDPrice], CONVERT(Decimal(20,2), YTDCost) as [YTDCost], CONVERT(Decimal(20,2), YTDSell - YTDCost) as [YTDMargin]
, CONVERT(Decimal(20,2), case isnull(YTDSell,0) when 0 then 0 else ((YTDSell-YTDCost)*100/YTDSell) end) as [YTDYield(%)]
FROM #TmpSellAmt

-- Drop Table
DROP TABLE #TmpSellAmt;
DROP TABLE #TmpClient;
















