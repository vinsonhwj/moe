﻿
CREATE PROCEDURE [dbo].[sp_Booking_Reopen_Checking]
    @Companycode VARCHAR(2) ,
    @MasterPNR NVARCHAR(10) ,
    @StaffCode NCHAR(10)
AS 
BEGIN

    DECLARE @sResult VARCHAR(1) --'Y' can reopen ,'N' can't reopen 
	
    DECLARE @ModuleCode NVARCHAR(10)
    DECLARE @RightsCode NCHAR(15)
    DECLARE @tmpCloseDate DATETIME
    DECLARE @Today DATETIME
	
    SET @Today = GETDATE()
    SET @ModuleCode = 'BKGMNT'
    SET @RightsCode = 'REOPN'

    SET @sResult = 'N'
	
    IF EXISTS(Select Login From Access Where CompanyCode=@Companycode and ModuleCode=@ModuleCode and RightsCode=@RightsCode and Login=@StaffCode)
    BEGIN
	IF EXISTS(Select BkgRef From PeoMstr Where CompanyCode=@CompanyCode and BkgRef=@MasterPnr and IsNull(IsClosed,'N')='Y')
	BEGIN
	  SELECT @sResult = 'Y'
 	END
    END

    SELECT  @sResult AS Result

/*
    IF @sResult = 'Y' 
        BEGIN            
            SELECT  @tmpCloseDate = MIN(CreateOn)
            FROM    peoMstr_CloseHistory
            WHERE   COMPANYCODE = @Companycode
                    AND MasterPnr = @MasterPNR
                    AND ActionType = 'C'
                    
            --SELECT  @tmpCloseDate
            
            IF YEAR(@Today) = YEAR(@tmpCloseDate)
                AND MONTH(@Today) = MONTH(@tmpCloseDate) 
                BEGIN
                    SET @sResult = 'Y'
                    --SELECT  'Date'
                END
            ELSE 
                BEGIN
                    SELECT  @sResult = (CASE WHEN COUNT(1) > 0 THEN 'Y'
                                             ELSE 'N'
                                        END)
                    FROM    dbo.ACCESS
                    WHERE   COMPANYCODE = @Companycode
                            AND MODULECODE = @ModuleCode
                            AND RIGHTSCODE = @RightsCode
                            AND LOGIN = @StaffCode
                            AND ACCESSRIGHTS = 1
                    --SELECT  'Access'
                END
        END       
*/
END








