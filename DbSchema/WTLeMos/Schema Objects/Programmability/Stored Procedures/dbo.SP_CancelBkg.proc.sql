﻿




CREATE PROCEDURE [dbo].[SP_CancelBkg]
	@CompanyCode CHAR(2) = 'SG'
AS

BEGIN 

	DECLARE 	@BkgRef CHAR(10)

	-- Collect all bkg which the update date equal to current day and depart date over 13 days
	DECLARE CUR_PeoPMQ2 CURSOR FOR

	select b.bkgref from peomstr a (nolock), peopmq b (nolock) 
				 where a.masterpnr=b.bkgref
				and a.companycode =  b.companycode
				and a.companycode=@companycode
				AND b.TotalDocAmt = 0
				AND b.TotalInvAmt = 0
				AND b.Status NOT IN ('4', '9')
				AND b.CancelBkg <> 'Y'
				AND (b.Yield IS NULL OR Yield = 0)
				and DateDiff(d, a.DepartDate, GetDate())>=14

	OPEN CUR_PeoPMQ2
	FETCH CUR_PeoPMQ2 INTO @BkgRef
	WHILE(@@FETCH_STATUS = 0)
	BEGIN
		-- Check valid document and invoice
		IF NOT EXISTS(SELECT InvNum FROM PeoInv (NOLOCK) WHERE CompanyCode = @CompanyCode AND (VOID2JBA IS NULL OR VOID2JBA = '') AND (VOIDON IS NULL OR VOIDON = '') AND (VOIDBY IS NULL OR VOIDBY = '') 
					AND BkgRef in (select bkgref from peomstr (nolock) where masterpnr=@BkgRef ))
		BEGIN
			IF NOT EXISTS(SELECT XONum FROM PeoXO (NOLOCK) WHERE CompanyCode = @CompanyCode AND (VOIDON IS NULL OR VOIDON = '') AND (VOIDBY IS NULL OR VOIDBY = '')
						AND BkgRef in (select bkgref from peomstr (nolock) where masterpnr=@BkgRef ))
			BEGIN 
				IF NOT EXISTS(SELECT VchNum FROM PeoVch (NOLOCK) WHERE CompanyCode = @CompanyCode AND (VOIDON IS NULL OR VOIDON = '') AND (VOIDBY IS NULL OR VOIDBY = '')
						AND BkgRef in (select bkgref from peomstr (nolock) where masterpnr=@BkgRef ))
				BEGIN 
					IF NOT EXISTS(SELECT Ticket FROM PeoTkt (NOLOCK) WHERE CompanyCode = @CompanyCode AND (VOIDON IS NULL OR VOIDON = '') AND (VOIDBY IS NULL OR VOIDBY = '')
							AND BkgRef in (select bkgref from peomstr (nolock) where masterpnr=@BkgRef ))
					BEGIN 
						IF NOT EXISTS(SELECT MCONum FROM PeoMCO (NOLOCK) WHERE CompanyCode = @CompanyCode AND (VOIDON IS NULL OR VOIDON = '') AND (VOIDBY IS NULL OR VOIDBY = '')
								AND BkgRef in (select bkgref from peomstr (nolock) where masterpnr=@BkgRef ))
						BEGIN
							-- All invoice(s) and document(s) are voided
							UPDATE PeoPMQ SET CancelBkg = 'Y', UpdateOn = GetDate(), UpdateBy = 'ADMIN' WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef

						END		
					END
				
				END

			END

		END

		FETCH CUR_PeoPMQ2 INTO @BkgRef

	END

	CLOSE CUR_PeoPMQ2
	DEALLOCATE CUR_PeoPMQ2

END




