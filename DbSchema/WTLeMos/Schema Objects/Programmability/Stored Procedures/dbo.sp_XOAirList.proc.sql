﻿CREATE procedure dbo.sp_XOAirList
@Companycode varchar(2)
, @bkgref varchar(10)
, @SegmentList varchar(500)

as




CREATE TABLE #tmpAir
(
	COMPANYCODE varchar(2) not null default ''
,	BKGREF varchar(10) not null default ''
,	SEQ int not null default ''
,	SEGNUM varchar(5) not null default ''
,	DEPARTCITY varchar(10) not null default ''
,	ARRIVALCITY varchar(10) not null default ''
,	DEPARTDATE datetime not null
,	DEPARTTIME varchar(5) not null default ''
,	ARRDATE datetime not null
,	ARRTIME varchar(5) not null default ''
,	FLIGHT varchar(10) not null default ''
,	CLASS varchar(2) not null default ''
,	REASONCODE nvarchar(4) not null default ''
,	INVRMK11 nvarchar(120) not null default '-'
,	INVRMK22 varchar(120) not null default '-'
,	VCHRMK1 nvarchar(120) not null default '-'
,	VCHRMK2 varchar(120) not null default '-'
,	AIRCURR	varchar(10) not null default ''
,	AIRAMT decimal(16,2) not null default 0
,	SHOWDETAILFEE varchar(1) not null default ''
,	AGETYPE varchar(3) not null default ''
,	AMTFEE decimal(16,2) not null default 0
,	TAXCURR varchar(3) not null default ''
,	TAXFEE decimal(16,2) not null default 0
,	QTY int not null default 0
,	PNR varchar(10) not null default ''
)

-- Split Segnum into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpSegment (Segnum varchar(5) )
While (@SegmentList <> '')
Begin
	If left(@SegmentList,1) = '|'
	Begin
		Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@SegmentList, 0, CharIndex('|', @SegmentList)))
	
		If @TargetCode = ''
			Select @TargetCode=@SegmentList

		INSERT INTO #TmpSegment VALUES (@TargetCode)

		If (Select CharIndex('|', @SegmentList)) > 0
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode) - 1))
		Else
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

IF NOT EXISTS (SELECT Segnum FROM #TmpSegment)
begin
INSERT INTO #tmpAir
	(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
         , ARRIVALCITY, DEPARTDATE, DEPARTTIME, ARRDATE,ARRTIME,FLIGHT, CLASS
	 , REASONCODE, INVRMK11, INVRMK22, VCHRMK1, VCHRMK2, AIRCURR, AIRAMT,PNR)
	SELECT a.Companycode, a.bkgref, convert(int, a.SEGNUM) as seq, a.SEGNUM, DepartCity
	, ArrivalCity, DepartDATE, DepartTime,ArrDate, ArrTime,  Flight, Class, IsNull(a.ReasonCode, '') As ReasonCode
	, isnull(INVRMK1,'-') as INVRMK11, isnull(INVRMK2,'-') as INVRMK22, isnull(VCHRMK1,'-') as VCHRMK1, isnull(VCHRMK2,'-') as VCHRMK2,isnull(dt.curr,'') as aircurr, dt.amt as airamt ,a.PNR as pnr
	FROM peoair a (nolock), peoairdetail dt (nolock) 
	WHERE  a.companycode = dt.companycode 
	AND a.bkgref=dt.bkgref AND a.segnum=dt.segnum 
	AND dt.seqnum = (SELECT min(seqnum) FROM peoairdetail dt2 (nolock) WHERE dt.segnum=dt2.segnum AND dt.bkgref=dt2.bkgref AND dt.companycode = dt2.companycode AND dt.seqtype=dt2.seqtype AND dt2.seqtype='COST' AND dt2.qty * (dt2.amt+dt2.taxamt+dt2.amtfee) >=


 0 ) 
	AND a.companycode = @Companycode
	AND a.bkgref=@bkgref 
	ORDER BY a.DepartDATE asc, a.SEGNUM asc

end
else
begin
	INSERT INTO #tmpAir
	(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, ARRDATE,ARRTIME, FLIGHT
	 , CLASS, REASONCODE, INVRMK11, INVRMK22,  VCHRMK1, VCHRMK2, AGETYPE 
	 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE, QTY,PNR)
	SELECT air.Companycode, air.bkgref, convert(int, air.SEGNUM) as seq, air.segnum, air.DEPARTCITY
	, isnull(air.Showdetailfee,'N') as Showdetailfee, air.ARRIVALCITY, air.DEPARTDATE, air.DEPARTTIME, air.ARRDATE, air.ARRTIME, air.FLIGHT
	, air.CLASS, IsNull(air.ReasonCode, '') As ReasonCode, isnull(air.INVRMK1,'-') as INVRMK11, isnull(air.INVRMK2,'-') as INVRMK22, isnull(air.VCHRMK1,'-') as VCHRMK1, isnull(air.VCHRMK2,'-') as VCHRMK2
	, detail.agetype, detail.curr, isnull(detail.amtfee,0) as amtfee, isnull(detail.amt,0) as amt, detail.taxcurr, isnull(detail.taxamt,0) as taxamt, isnull(detail.qty,0) as qty,air.PNR as pnr
	FROM peoair air (nolock), peoairdetail detail (nolock), #TmpSegment t 
	WHERE air.Companycode = detail.Companycode 
	AND air.segnum = detail.segnum
	AND air.bkgref = detail.bkgref
	AND air.segnum = t.segnum
	AND air.companycode = @Companycode
	AND air.bkgref=@bkgref 
	AND detail.seqtype = 'COST'
	ORDER BY air.Companycode, air.Bkgref, air.Segnum, detail.agetype



end

SELECT COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, ARRDATE,ARRTIME, FLIGHT
	 , CLASS, REASONCODE, INVRMK11, INVRMK22, VCHRMK1, VCHRMK2, AGETYPE 
	 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE, QTY,PNR
FROM #tmpAir 
ORDER BY SEGNUM ASC

DROP TABLE #TmpSegment
DROP TABLE #tmpAir


