﻿
CREATE PROCEDURE [dbo].[sp_Booking_CloseReopenHistory]
    @CompanyCode VARCHAR(2) ,
    @MasterPNR NVARCHAR(10) ,
    @StaffCode NCHAR(10) ,
    @ActionType VARCHAR(1), --C=Close/O=Reopen,
    @CreateOn datetime
AS 
BEGIN
    DECLARE @sNetAmt DECIMAL(16, 2)
    DECLARE @sNetTax DECIMAL(16, 2)
    DECLARE @sCount INT
    DECLARE @sTeamCode VARCHAR(10)
	
    BEGIN TRAN
    
    UPDATE  dbo.PEOMSTR
    SET     IsClosed = (CASE WHEN @ActionType = 'C' THEN 'Y'
                             ELSE 'N'
                        END)
    WHERE   COMPANYCODE = @CompanyCode
            AND MASTERPNR = @MasterPNR
	
    UPDATE  dbo.MstrPnrTotal
    SET     IsClosed = (CASE WHEN @ActionType = 'C' THEN 'Y'
                             ELSE 'N'
                        END)
    WHERE   COMPANYCODE = @CompanyCode
            AND MASTERPNR = @MasterPNR AND IsLast='Y'

    SELECT  @sNetAmt = ISNULL(TtlReptAmt, 0) - ISNULL(TtlDocAmt, 0),
            @sNetTax = ISNULL(TtlReptTax, 0) - ISNULL(TtlDocTax, 0)
    FROM    dbo.peoMstr_CloseHistory
    WHERE   COMPANYCODE = @CompanyCode
            AND MASTERPNR = @MasterPNR
            AND IsLast = 'Y'
            
    SET @sNetAmt = ISNULL(@sNetAmt, 0.00)
    SET @sNetTax = ISNULL(@sNetTax, 0.00)

    SELECT  @sCount = COUNT(*)
    FROM    dbo.peoMstr_CloseHistory
    WHERE   COMPANYCODE = @CompanyCode
            AND MASTERPNR = @MasterPNR
            AND IsLast = 'Y'

    UPDATE  dbo.peoMstr_CloseHistory
    SET     IsLast = 'N'
    WHERE   COMPANYCODE = @CompanyCode
            AND MASTERPNR = @MasterPNR
            AND IsLast = 'Y'

    INSERT  dbo.peoMstr_CloseHistory (CompanyCode, MasterPnr, TtlReptAmt,
                                      TtlReptTax, TtlDocAmt, TtlDocTax,
                                      ActionType, NetMoveAmt, NetMoveTax,
                                      IsLast, CreateOn, CreateBy,ActionOn )
            SELECT  COMPANYCODE, MASTERPNR, SUM(ISNULL(ReptAmt, 0)),
                    SUM(ISNULL(ReptTax, 0)),
                    SUM(ISNULL(DOCAMT, 0)) AS TtlDocAmt,
                    SUM(ISNULL(DOCTAX, 0)) AS TtlDocTax, @ActionType,
                    (CASE WHEN @ActionType = 'C'
                               AND @sCount <> 0
                          THEN SUM(ISNULL(ReptAmt, 0)) - SUM(ISNULL(DOCAMT, 0))
                               - @sNetAmt
                          ELSE 0.00
                     END),
                    (CASE WHEN @ActionType = 'C'
                               AND @sCount <> 0
                          THEN SUM(ISNULL(ReptTax, 0)) - SUM(ISNULL(DOCTAX, 0))
                               - @sNetTax
                          ELSE 0.00
                     END), 'Y', @CreateOn, @StaffCode,GETDATE()
            FROM    dbo.PEOMSTR
            WHERE   COMPANYCODE = @CompanyCode
                    AND MASTERPNR = @MasterPNR
            GROUP BY COMPANYCODE, MASTERPNR
	
    SELECT  @sTeamCode = TEAMCODE
    FROM    dbo.PEOSTAFF
    WHERE   COMPANYCODE = @CompanyCode
            AND STAFFCODE = @StaffCode
	
    INSERT  dbo.PEONYREF (COMPANYCODE, BKGREF, STAFFCODE, TEAMCODE, DocNum,
                          ACTIONCODE, [Description], CREATEON, CREATEBY)
            SELECT  m.COMPANYCODE, m.BKGREF, @StaffCode, @sTeamCode, m.BKGREF,
                    CASE WHEN @ActionType = 'C' THEN 'BKGCL'
                         ELSE 'BKGRE'
                    END,
                    CASE WHEN @ActionType = 'C'
                         THEN 'Master PNR:' + @MasterPNR + ' Closed. Profit:' + convert(varchar,(ch.NetMoveAmt + ch.NetMoveTax))
                         ELSE 'Master PNR:' + @MasterPNR + ' Reopened.'
                    END, GETDATE(), @StaffCode
            FROM    dbo.PEOMSTR m
			INNER JOIN peoMstr_CloseHistory ch
			on m.CompanyCode=ch.CompanyCode and m.MasterPnr=ch.MasterPnr
            WHERE   m.COMPANYCODE = @CompanyCode
                    AND m.MASTERPNR = @MasterPNR
		    AND ch.IsLast='Y'
	

    IF @@Error <> 0 
        BEGIN
            RAISERROR ('Booking Close/Reopen History Error',16,1) 
            ROLLBACK TRAN
        END
    ELSE 
        BEGIN
            COMMIT TRAN
        END
END



