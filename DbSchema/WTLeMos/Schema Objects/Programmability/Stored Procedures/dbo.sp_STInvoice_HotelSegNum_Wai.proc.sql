﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_STInvoice_HotelSegNum_Wai]
    @GSTTAXRate VARCHAR(10) ,
    @Companycode NVARCHAR(2) ,
    @BkgRef NVARCHAR(10) ,
    @SegNum NVARCHAR(5) ,
    @SeqType NVARCHAR(5)
AS 
    BEGIN
	--建保存结果的临时表
        CREATE TABLE #resultHtlDetail
            (
              [BKGREF] [nvarchar](10) NOT NULL ,
              [COMPANYCODE] [nvarchar](2) NOT NULL ,
              [SEQTYPE] [nvarchar](5) NOT NULL ,
              [SEGNUM] [nvarchar](5) NOT NULL ,
              [SEQNUM] [nvarchar](5) NOT NULL ,
              [QTY] [int] NULL ,
              [ARRDATE] [datetime] NULL ,
              [DEPARTDATE] [datetime] NULL ,
              [RMNTS] [decimal](4, 0) NULL ,
              [RATENAME] [nvarchar](40) NULL ,
              [ITEMNAME] [nvarchar](100) NULL ,
              [CURR] [nvarchar](3) NULL ,
              [AMT] [decimal](16, 2) NULL ,
              [TOTALAMT] [decimal](16, 2) NULL ,
              [TAXCURR] [nvarchar](3) NULL ,
              [TAXAMT] [decimal](16, 2) NULL ,
              [itemtype] [nchar](1) NULL ,
              [AMTFEE] [decimal](18, 0) NULL,
	      )
	
	--建保存临时数据的临时表
        SELECT  *
        INTO    #tmpHtlDetail
        FROM    #resultHtlDetail

        DECLARE @sBKGREF NVARCHAR(10) ,
            @sCOMPANYCODE NVARCHAR(2) ,
            @sSEQTYPE NVARCHAR(5) ,
            @sSEGNUM NVARCHAR(5) ,
            @sSEQNUM NVARCHAR(5) ,
            @sQTY INT ,
            @sARRDATE DATETIME ,
            @sDEPARTDATE DATETIME ,
            @sRMNTS DECIMAL(4, 0) ,
            @sRATENAME NVARCHAR(40) ,
            @sITEMNAME NVARCHAR(100) ,
            @sCURR NVARCHAR(3) ,
            @sAMT DECIMAL(16, 2) ,
            @sTOTALAMT DECIMAL(16, 2) ,
            @sTAXCURR NVARCHAR(3) ,
            @sTAXAMT DECIMAL(16, 2) ,
            @sItemType NCHAR(1) ,
            @sAMTFEE DECIMAL(18, 0)
 			
        DECLARE @bCount INT ,
            @aCount INT
 			
        INSERT  #tmpHtlDetail
                ( BKGREF ,
                  COMPANYCODE ,
                  SEQTYPE ,
                  SEGNUM ,
                  SEQNUM ,
                  QTY ,
                  ARRDATE ,
                  DEPARTDATE ,
                  RMNTS ,
                  RATENAME ,
                  ITEMNAME ,
                  CURR ,
                  AMT ,
                  TOTALAMT ,
                  TAXCURR ,
                  TAXAMT ,
                  itemtype ,
                  AMTFEE
                )
                SELECT  BKGREF ,
                        COMPANYCODE ,
                        SEQTYPE ,
                        SEGNUM ,
                        SEQNUM ,
                        QTY ,
                        ARRDATE ,
                        DEPARTDATE ,
                        RMNTS ,
                        RATENAME ,
                        ITEMNAME ,
                        CURR ,
                        AMT ,
                        TOTALAMT ,
                        TAXCURR ,
                        TAXAMT ,
                        itemtype ,
                        AMTFEE
                FROM    dbo.PeoHtlDetail
                WHERE   COMPANYCODE = @Companycode
                        AND BKGREF = @BkgRef
                        AND SEGNUM IN (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                        AND SEQTYPE = @SeqType
 	
        SET @aCount = 0
        SELECT  @bCount = COUNT(*)
        FROM    #tmpHtlDetail
 		
        WHILE @aCount <> @bCount 
            BEGIN
                IF @aCount <> 0 
                    BEGIN
                        DELETE  #tmpHtlDetail 
                        INSERT  #tmpHtlDetail
                                ( BKGREF ,
                                  COMPANYCODE ,
                                  SEQTYPE ,
                                  SEGNUM ,
                                  SEQNUM ,
                                  QTY ,
                                  ARRDATE ,
                                  DEPARTDATE ,
                                  RMNTS ,
                                  RATENAME ,
                                  ITEMNAME ,
                                  CURR ,
                                  AMT ,
                                  TOTALAMT ,
                                  TAXCURR ,
                                  TAXAMT ,
                                  itemtype ,
                                  AMTFEE
                                )
                                SELECT  BKGREF ,
                                        COMPANYCODE ,
                                        SEQTYPE ,
                                        SEGNUM ,
                                        SEQNUM ,
                                        QTY ,
                                        ARRDATE ,
                                        DEPARTDATE ,
                                        RMNTS ,
                                        RATENAME ,
                                        ITEMNAME ,
                                        CURR ,
                                        AMT ,
                                        TOTALAMT ,
                                        TAXCURR ,
                                        TAXAMT ,
                                        itemtype ,
                                        AMTFEE
                                FROM    #resultHtlDetail
                    END 
                SELECT  @bCount = COUNT(*)
                FROM    #resultHtlDetail
	
                DELETE  #resultHtlDetail 
	
                DECLARE MyCursor CURSOR
                FOR
                    SELECT  BKGREF ,
                            COMPANYCODE ,
                            SEQTYPE ,
                            SEGNUM ,
                            MIN(SEQNUM) SEQNUM ,
                            SUM(QTY) QTY ,
                            ARRDATE ,
                            DEPARTDATE ,
                            RMNTS ,
                            MAX(RATENAME) RATENAME ,
                            ITEMNAME ,
                            CURR ,
                            SUM(AMT) AMT ,
                            SUM(TOTALAMT) TOTALAMT ,
                            TAXCURR ,
                            SUM(TAXAMT) TAXAMT ,
                            itemtype ,
                            SUM(AMTFEE) AMTFEE
                    FROM    #tmpHtlDetail
                    WHERE   COMPANYCODE = @Companycode
                            AND BKGREF = @BkgRef
                            AND SEGNUM IN (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                            AND SEQTYPE = @SeqType
                    GROUP BY BKGREF ,
                            COMPANYCODE ,
                            SEQTYPE ,
                            SEGNUM ,
                            ARRDATE ,
                            DEPARTDATE ,
                            RMNTS ,
                            ITEMNAME ,
                            itemtype ,
                            CURR ,
                            TAXCURR
                    ORDER BY ARRDATE ,
                            DEPARTDATE ,
                            SEQNUM 

	--打开一个游标	
                OPEN MyCursor

	--循环一个游标
	
                FETCH NEXT FROM MyCursor INTO @sBKGREF, @sCOMPANYCODE,
                    @sSEQTYPE, @sSEGNUM, @sSEQNUM, @sQTY, @sARRDATE,
                    @sDEPARTDATE, @sRMNTS, @sRATENAME, @sITEMNAME, @sCURR,
                    @sAMT, @sTOTALAMT, @sTAXCURR, @sTAXAMT, @sItemType,
                    @sAMTFEE
                WHILE @@FETCH_STATUS = 0 
                    BEGIN
                        DECLARE @rowcount INT
                        SELECT  @rowcount = COUNT(1)
                        FROM    #resultHtlDetail
                        WHERE   BKGREF = @sBKGREF
                                AND COMPANYCODE = @sCOMPANYCODE
                                AND QTY = @sQTY
                                AND DEPARTDATE = @sARRDATE
                                AND ITEMNAME = @sITEMNAME
                                AND itemtype = @sItemType
			
                        IF @rowcount > 0 
                            BEGIN
                                UPDATE  #resultHtlDetail
                                SET     DEPARTDATE = @sDEPARTDATE ,
                                        RMNTS = RMNTS + @sRMNTS ,
                                        Amt = Amt + @sAmt ,
                                        TOTALAMT = TOTALAMT + @sTOTALAMT ,
                                        TAXAMT = TAXAMT + @sTAXAMT ,
                                        AMTFEE = AMTFEE + @sAMTFEE
                                WHERE   BKGREF = @sBKGREF
                                        AND COMPANYCODE = @sCOMPANYCODE
                                        AND QTY = @sQTY
                                        AND DEPARTDATE = @sARRDATE
                                        AND ITEMNAME = @sITEMNAME
                                        AND itemtype = @sItemType
				
                            END
                        ELSE 
                            BEGIN
                                INSERT  #resultHtlDetail
                                        ( BKGREF ,
                                          COMPANYCODE ,
                                          SEQTYPE ,
                                          SEGNUM ,
                                          SEQNUM ,
                                          QTY ,
                                          ARRDATE ,
                                          DEPARTDATE ,
                                          RMNTS ,
                                          RATENAME ,
                                          ITEMNAME ,
                                          CURR ,
                                          AMT ,
                                          TOTALAMT ,
                                          TAXCURR ,
                                          TAXAMT ,
                                          itemtype ,
                                          AMTFEE
                                        )
                                VALUES  ( @sBKGREF ,
                                          @sCOMPANYCODE ,
                                          @sSEQTYPE ,
                                          @sSEGNUM ,
                                          @sSEQNUM ,
                                          @sQTY ,
                                          @sARRDATE ,
                                          @sDEPARTDATE ,
                                          @sRMNTS ,
                                          @sRATENAME ,
                                          @sITEMNAME ,
                                          @sCURR ,
                                          @sAMT ,
                                          @sTOTALAMT ,
                                          @sTAXCURR ,
                                          @sTAXAMT ,
                                          @sItemType ,
                                          @sAMTFEE
                                        )
			
                            END
			
                        FETCH NEXT FROM MyCursor INTO @sBKGREF, @sCOMPANYCODE,
                            @sSEQTYPE, @sSEGNUM, @sSEQNUM, @sQTY, @sARRDATE,
                            @sDEPARTDATE, @sRMNTS, @sRATENAME, @sITEMNAME,
                            @sCURR, @sAMT, @sTOTALAMT, @sTAXCURR, @sTAXAMT,
                            @sItemType, @sAMTFEE
			
			
                    END	

	--关闭游标
                CLOSE MyCursor
	--释放资源
                DEALLOCATE MyCursor
	
                SELECT  @aCount = COUNT(*)
                FROM    #resultHtlDetail
               -- SELECT  @aCount
            END

        SELECT  @GSTTAXRate AS GSTTAXRate ,
                b.GSTTAX ,
                a.coMPANYCODE ,
                a.BKGREF ,
                a.SEGNUM ,
                a.SEQNUM ,
                a.SEQTYPE ,
                ISNULL(RMNTS, 0) AS RMNTS ,
                ISNULL(a.ARRDATE, '') AS ARRDATE ,
                ISNULL(a.DEPARTDATE, '') AS DEPARTDATE ,
                ISNULL(QTY, 1) AS QTY ,
                RATENAME ,
                ITEMNAME AS ROOMTYPE ,
                ITEMNAME ,
                ISNULL(CURR, '') AS CURR ,
                ISNULL(AMT, 0) AS AMT ,
                ISNULL(TOTALAMT, 0) AS TOTALAMT ,
                ISNULL(TAXCURR, '') AS TAXCURR ,
                ISNULL(a.TAXAMT, 0) AS TAXAMT ,
                ISNULL(a.itemtype, '') AS itemtype ,
                CASE WHEN a.QTY > 2 THEN 2
                     WHEN a.QTY <= 2 THEN QTY
                END AS QtyOfHotel ,
                ISNULL(a.QTY, 0) AS QTYs
        FROM    #resultHtlDetail AS a ,
                peohtl AS b
        WHERE   b.companycode = @Companycode
                AND a.bkgref = @BkgRef
                AND a.SEQTYPE = @SeqType
                AND a.SEGNUM IN(SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                AND a.bkgref = b.bkgref
                AND a.companycode = b.companycode
                AND a.segnum = b.segnum
        ORDER BY  a.seqnum

--SELECT @GSTTAXRate  as GSTTAXRate, b.GSTTAX, a.coMPANYCODE,a.BKGREF,a.SEGNUM,a.SEQNUM,a.SEQTYPE,
--		isnull(RMNTS,0) as RMNTS, a.ARRDATE, a.DEPARTDATE,isnull(QTY,1) as QTY,RATENAME,
--		ITEMNAME as ROOMTYPE,isnull(CURR,'') as SELLCURR, isnull(AMT,0) as SELLAMT,isnull(TAXCURR,'') as TAXCURR ,
--		isnull(a.TAXAMT,0) as TAXAMT, isnull(a.itemtype,'') as itemtype 
--	from 
--		#resultHtlDetail AS a, peohtl as b 
--	where b.companycode=@Companycode and a.bkgref=@BkgRef and a.SEQTYPE=@SeqType and a.SEGNUM =@SegNum 
--		and a.bkgref=b.bkgref and a.companycode=b.companycode and a.segnum=b.segnum order by a.seqnum


    END
