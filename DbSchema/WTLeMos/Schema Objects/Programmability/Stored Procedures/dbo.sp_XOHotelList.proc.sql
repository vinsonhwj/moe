﻿CREATE procedure dbo.sp_XOHotelList
@Companycode varchar(2)
, @bkgref varchar(10)
, @SegmentList varchar(500)

as



CREATE TABLE #tmpHotel
(
	COMPANYCODE varchar(2) not null default ''
,	BKGREF varchar(10) not null default ''
,	SEQ int not null default ''
,	SEGNUM varchar(5) not null default ''
,	COUNTRYCODE varchar(10) not null default ''
,	CITYCODE varchar(10) not null default ''
,	ARRDATE datetime not null
,	ARRTIME varchar(5) not null default ''
,	ETA varchar(20) not null default ''
,	DEPARTDATE datetime not null
,	DEPARTTIME nvarchar(4) not null default ''
,	ETD varchar(20) not null default ''
,	RMK1 nvarchar(100) not null default ''
,	RMK2 nvarchar(100) not null default ''
,	VCHRMK1 nvarchar(100) not null default ''
,	VCHRMK2 nvarchar(100) not null default ''
,	SPCL_REQUEST nvarchar(510) not null default ''
,	HTLREFERENCE nvarchar(40) not null default ''
,	HTLCFMBY nvarchar(60) not null default ''
,	HOTELCODE varchar(10) not null default ''
,	HOTELNAME nvarchar(800) not null default ''
,	COUNTRYNAME nvarchar(80) not null default ''
,	CITYNAME nvarchar(80) not null default ''
,	GSTTAX nvarchar(2) not null default ''
,	SEQNUM nvarchar(10) not null default ''
,	SEQTYPE nvarchar(10) not null default ''
,	RMNTS decimal(4,0) not null default 0
,	QTY int not null default 0
,	RATENAME nvarchar(80) not null default ''
,	ITEMNAME nvarchar(200) not null default ''
,	SELLCURR varchar(10) not null default ''
,	SELLAMT decimal(16,2) not null default 0
,	TAXCURR varchar(10) not null default ''
,	TAXAMT decimal(16,2) not null default 0
,	ITEMTYPE nchar(2) not null default ''
,	REASONCODE nvarchar(4) not null default ''
)

-- Split Segnum into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpSegment (Segnum varchar(5) )
While (@SegmentList <> '')
Begin
	If left(@SegmentList,1) = '|'
	Begin
		Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@SegmentList, 0, CharIndex('|', @SegmentList)))
	
		If @TargetCode = ''
			Select @TargetCode=@SegmentList

		INSERT INTO #TmpSegment VALUES (@TargetCode)

		If (Select CharIndex('|', @SegmentList)) > 0
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode) - 1))
		Else
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

IF NOT EXISTS (SELECT Segnum FROM #TmpSegment)
begin
	INSERT INTO #tmpHotel
	(COMPANYCODE, BKGREF, SEQ, SEGNUM, COUNTRYCODE
         , CITYCODE, ARRDATE, ARRTIME, ETA, DEPARTDATE
	 , DEPARTTIME, ETD, ReasonCode, RMK1, RMK2,VCHRMK1,VCHRMK2
	 , SPCL_REQUEST, HTLREFERENCE, HTLCFMBY, HOTELCODE, HOTELNAME
	 , COUNTRYNAME, CITYNAME,SELLCURR
	)
	SELECT  htl.Companycode, htl.Bkgref, convert(int,htl.segnum) as seq, htl.SEGNUM,htl.COUNTRYCODE
	,htl.CITYCODE,htl.ARRDATE,htl.ARRTIME,htl.ETA,htl.DEPARTDATE
	,htl.DEPARTTIME,htl.ETD, IsNull(htl.ReasonCode, '') As ReasonCode, htl.rmk1,htl.rmk2,htl.VCHRMK1,htl.VCHRMK2
	,htl.SPCL_REQUEST,htl.HTLREFERENCE, htl.HTLCFMBY, htl.HOTELCODE,isnull(hotel.hotelname,isnull(htl.hotelname,'')) as HOTELNAME
	,country.countryname as COUNTRYNAME,city.cityname as CITYNAME,htl.SELLCURR 
	FROM peohtl htl (nolock)
	LEFT OUTER JOIN peohotel hotel (nolock) ON htl.hotelcode=hotel.hotelcode  
	JOIN country_en_us country on htl.countrycode=country.countrycode 
	JOIN city_en_us city on htl.citycode=city.citycode  
	JOIN peohtldetail dt (nolock) ON dt.companycode = htl.companycode and dt.bkgref = htl.bkgref AND dt.segnum = htl.segnum
	WHERE htl.companycode=@Companycode and htl.bkgref=@Bkgref 
	AND dt.seqnum = (SELECT min(seqnum) FROM peohtldetail dt2 (nolock) 
			 WHERE dt.companycode = dt2.companycode AND dt.segnum=dt2.segnum 
			 AND dt.bkgref=dt2.bkgref AND dt.seqtype=dt2.seqtype 
			 AND dt2.seqtype='COST' AND (dt2.amt+dt2.taxamt+dt2.amtfee) >= 0) 
	ORDER BY htl.departdate, htl.SEGNUM ASC

end
else
begin

	INSERT INTO #tmpHotel
	(COMPANYCODE, BKGREF, SEQ, SEGNUM, COUNTRYCODE
         , CITYCODE, ARRDATE, ARRTIME, ETA, DEPARTDATE
	 , DEPARTTIME, ETD, REASONCODE, RMK1, RMK2,VCHRMK1,VCHRMK2
	 , SPCL_REQUEST, HTLREFERENCE, HTLCFMBY, HOTELCODE, HOTELNAME
         , COUNTRYNAME, CITYNAME, GSTTAX, SEQNUM, SEQTYPE
         , RMNTS, QTY, RATENAME, ITEMNAME, SELLCURR
         , SELLAMT, TAXCURR, TAXAMT, ITEMTYPE
	)
	SELECT  htl.Companycode, htl.Bkgref, convert(int,htl.segnum) as seq, htl.SEGNUM,htl.COUNTRYCODE
	, htl.CITYCODE,htl.ARRDATE,htl.ARRTIME,htl.ETA,htl.DEPARTDATE
	, htl.DEPARTTIME,htl.ETD, IsNull(htl.ReasonCode, '') As ReasonCode, htl.rmk1,htl.rmk2,htl.VCHRMK1,htl.VCHRMK2
	, htl.SPCL_REQUEST, htl.HTLREFERENCE, htl.HTLCFMBY, htl.HOTELCODE,isnull(hotel.hotelname,isnull(htl.hotelname,'')) as HOTELNAME
        , country.countryname as COUNTRYNAME, city.cityname as CITYNAME, htl.gsttax, detail.seqnum, detail.seqtype
	, isnull(detail.RMNTS,0) as RMNTS, isnull(detail.QTY,1) as QTY, detail.RATENAME,detail.ITEMNAME as ROOMTYPE, isnull(detail.CURR,'') as SELLCURR
	, isnull(detail.AMT,0) as SELLAMT, isnull(detail.TAXCURR,'') as TAXCURR ,isnull(detail.TAXAMT,0) as TAXAMT, isnull(detail.itemtype,'') as itemtype 
	FROM peohtl htl (nolock)
	LEFT OUTER JOIN peohotel hotel (nolock) ON htl.hotelcode=hotel.hotelcode  
	JOIN peohtldetail detail (nolock) ON detail.companycode = htl.companycode and detail.bkgref = htl.bkgref AND detail.segnum = htl.segnum
	JOIN country_en_us country on htl.countrycode=country.countrycode 
	JOIN city_en_us city on htl.citycode=city.citycode  
	JOIN #TmpSegment seg on htl.segnum = seg.segnum
	WHERE htl.companycode=@Companycode and htl.bkgref=@Bkgref 
	AND detail.seqtype = 'COST'
	ORDER BY htl.departdate, htl.SEGNUM ASC

end


SELECT COMPANYCODE, BKGREF, SEQ, SEGNUM, COUNTRYCODE
         , CITYCODE, ARRDATE, ARRTIME, ETA, DEPARTDATE
	 , DEPARTTIME, ETD, REASONCODE, RMK1, RMK2, VCHRMK1,VCHRMK2,SPCL_REQUEST 
	 , HTLREFERENCE, HTLCFMBY, HOTELCODE, HOTELNAME, COUNTRYNAME
	 , CITYNAME, GSTTAX, SEQNUM, SEQTYPE
	 , RMNTS, QTY, RATENAME, ITEMNAME, SELLCURR
	 , SELLAMT, TAXCURR, TAXAMT, ITEMTYPE
FROM #tmpHotel 

DROP TABLE #TmpSegment
DROP TABLE #tmpHotel

--exec sp_InvHotelList 'WM','WMH0001345','00001'


