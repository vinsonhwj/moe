﻿
CREATE   PROCEDURE dbo.CustomerSalesReport_By_OwnerTeamList_Old
@StartDate CHAR(10) = '2004-06-17',
@EndDate CHAR(10) = '2004-06-17',
@TeamCode VarChar(10) = '',
@CompanyCode VarChar(2) = 'SC', 
@Report_TypeCode VarChar(4) = 'STPR', 	-- added by IVT on 12Nov2003, add Staff productivity report
@StaffCode VarChar(5) = ''		-- added by IVT on 12Nov2003, add Staff productivity report
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
--select @TeamCode =  '%' + @TeamCode + '%'
--select @StaffCode = '%' + @StaffCode + '%'
			
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' and isnull(inv.invType,'') <> 'DEP'
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear --AND INV.VOIDON IS NULL
				--AND INV.VOIDON IS NULL AND ISNULL( RTRIM(INV.VOID2JBA), '') = ''				
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C' and isnull(vinv.invType,'') <> 'DEP' -- AND  vInv.cltCode = i.CltCode 
				AND VINV.createon BETWEEN @StartDate AND @enddayofyear  
--				AND VINV.VOIDON IS not NULL --AND ISNULL( RTRIM(VINV.VOID2JBA), '') = ''				
				) 
                          AS [Total Credit Note],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' and isnull(inv.invType,'') = 'DEP'
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear --AND INV.VOIDON IS NULL
				--AND INV.VOIDON IS NULL AND ISNULL( RTRIM(INV.VOID2JBA), '') = ''				
				 ) 
                          AS [Total Deposit],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C' and isnull(vinv.invType,'') = 'DEP' -- AND  vInv.cltCode = i.CltCode 
				AND VINV.createon BETWEEN @StartDate AND @enddayofyear  
--				AND VINV.VOIDON IS not NULL --AND ISNULL( RTRIM(VINV.VOID2JBA), '') = ''				
				) 
                          AS [Total Deposit CN],

                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref --AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @StartDate AND @enddayofyear)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref --AND VoidOn IS NULL
				AND V.voidon BETWEEN @StartDate AND @enddayofyear and V.voidon is not null)  AS [Total Voucher Void],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref --AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @enddayofyear)  AS [Total XO],
                              (SELECT         SUM(HKDAmt)
                                FROM            PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref --AND VoidOn IS NULL
				AND X.VoidOn BETWEEN @StartDate AND @enddayofyear and X.voidon is not null)  AS [Total XO Void],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref --AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @StartDate AND @enddayofyear) AS [Total TKT],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref --AND VoidOn IS NULL
				AND T.VoidOn BETWEEN @StartDate AND @enddayofyear and T.voidon is not null) AS [Total TKT Void],
                              (SELECT         SUM(costamt+taxamt)
                                FROM              Peomco M (nolock)
                                WHERE          I.BkgRef = M.Bkgref --AND VoidOn IS NULL
				AND M.CreateOn BETWEEN @StartDate AND @enddayofyear) AS [Total MCO],	--added by LUC
                              (SELECT         SUM(costamt+taxamt)
                                FROM              Peomco M (nolock)
                                WHERE          I.BkgRef = M.Bkgref AND VoidOn IS not NULL
				AND M.voidon BETWEEN @StartDate AND @enddayofyear and M.voidon is not null) AS [Total MCO Void],	--added by JNY
                              (SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND P.CreateOn BETWEEN @StartDate AND @enddayofyear ) AS [Total Pax]
INTO #BookingSum
FROM              dbo.peomstr I (nolock)
WHERE	 
-- I.CreateOn BETWEEN @StartDate AND @enddayofyear and 
I.companycode = @CompanyCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
-- debug

-- debug
/* added by IVT on 07Nov2003, add year to date field */
SELECT         I.Bkgref, 
		-- i. TeamCode, 									-- commented by IVT
		i.companycode,		

							-- added by IVT
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I'  and isnull(inv.invType,'') <> 'DEP'
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear --AND INV.VOIDON IS NULL
				--AND INV.VOIDON IS NULL AND ISNULL( RTRIM(INV.VOID2JBA), '') = ''				
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C' and isnull(vinv.invType,'') <> 'DEP'  -- AND  vInv.cltCode = i.CltCode 
				AND VINV.createon BETWEEN @firstdayofyear AND @enddayofyear  
--				AND VINV.VOIDON IS not NULL --AND ISNULL( RTRIM(VINV.VOID2JBA), '') = ''				
				) 
                          AS [Total Credit Note],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' and isnull(inv.invType,'') = 'DEP'
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear --AND INV.VOIDON IS NULL
				--AND INV.VOIDON IS NULL AND ISNULL( RTRIM(INV.VOID2JBA), '') = ''				
				 ) 
                          AS [Total Deposit],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C' and isnull(vinv.invType,'') = 'DEP' -- AND  vInv.cltCode = i.CltCode 
				AND VINV.createon BETWEEN @firstdayofyear AND @enddayofyear  
--				AND VINV.VOIDON IS not NULL --AND ISNULL( RTRIM(VINV.VOID2JBA), '') = ''				
				) 
                          AS [Total Deposit CN],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref --AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref --AND VoidOn IS NULL
				AND V.voidon BETWEEN @firstdayofyear AND @enddayofyear and V.voidon is not null)  AS [Total Voucher Void],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref --AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @enddayofyear)  AS [Total XO],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref --AND VoidOn IS NULL
				AND X.VoidOn BETWEEN @firstdayofyear AND @enddayofyear and X.voidon is not null)  AS [Total XO Void],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref --AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @firstdayofyear AND @enddayofyear) AS [Total TKT],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref --AND VoidOn IS NULL
				AND T.VoidOn BETWEEN @firstdayofyear AND @enddayofyear and T.voidon is not null) AS [Total TKT Void],
                              (SELECT         SUM(costamt+taxamt)
                                FROM              Peomco M (nolock)
                                WHERE          I.BkgRef = M.Bkgref --AND VoidOn IS NULL
				AND M.CreateOn BETWEEN @firstdayofyear AND @enddayofyear) AS [Total MCO],	--added by LUC
                              (SELECT         SUM(costamt+taxamt)
                                FROM              Peomco M (nolock)
                                WHERE          I.BkgRef = M.Bkgref AND VoidOn IS not NULL
				AND M.voidon BETWEEN @firstdayofyear AND @enddayofyear and M.voidon is not null) AS [Total MCO Void],	--added by JNY
                              (SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND P.CreateOn BETWEEN @firstdayofyear AND @enddayofyear ) AS [Total Pax]
INTO #BookingSum_year_to_date
FROM              dbo.peomstr I (nolock)
WHERE	 
I.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
-- and I.bkgref in (select distinct bkgref from #BookingSum)	-- added by IVT on 12Nov2003
and I.companycode = @CompanyCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
-- debug
-- select * from #BookingSum_year_to_date
-- debug
select 
m.companycode,
m.teamcode,                                     
m.staffcode, 					-- added by IVT on 12Nov2003
CONVERT(Decimal(20,0), SUM(b.[Total Pax])) 
as  [Total Pax],
CAST(
ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) 
 AS Decimal(20,2) ) [Price],
CAST(
ISNULL(SUM(b.[Total Deposit]), 0)  -  ISNULL(SUM(b.[Total Deposit CN]), 0) 
 AS Decimal(20,2) ) [Dep],
CAST(
((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0))-(ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(
b.[Total MCO Void]), 0)))	--modified by LUC
 AS Decimal(20,2) ) [Cost],
CAST(
	(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0))- (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)))	--modified by LUC
	 AS Decimal(20,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
	((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0)) -  - (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)) )  = 0 THEN 	--modified by LUC
	CAST(
		0
	 AS Decimal(20,2) )
WHEN 	ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(20,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0))- - (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)))	-- modified by LUC
	) /
		(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  * 100
	AS Decimal(20,2) )
END
into #result_year_to_date
From 
 #BookingSum_year_to_date b
, peomstr m (nolock)												-- added by IVT
where
-- b.TeamCode like @TeamCode										-- commented by IVT
b.companycode = @CompanyCode									-- added by IVT
and b.companycode = m.companycode 									-- added by IVT
and b.bkgref = m.bkgref											-- added by IVT
and 
(
m.teamcode in (select distinct teamcode from #tempteam )	-- added by IVT
or 
	(
	m.staffcode in ( select distinct staffcode from #tempstaff )	 
	and 
	m.teamcode  = (select teamcode from peostaff where staffcode = m.staffcode and companycode = m.companycode  )
	)
)
group by  m.companycode, m.teamcode
, m.staffcode				-- added by IVT on 12Nov2003
/* end added by IVT on 07Nov2003*/
-- debug
-- select * from #result_year_to_date
-- debug
select 
m.companycode,
m. teamcode,
m.staffcode, 		-- added by IVT on 12Nov2003
CONVERT(Decimal(20,0), SUM(b.[Total Pax])) 
as  [Total Pax],
CAST(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) 
 AS Decimal(20,2) ) [Price],
CAST(ISNULL(SUM(b.[Total Deposit]), 0)  -  ISNULL(SUM(b.[Total Deposit CN]), 0) 
 AS Decimal(20,2) ) [Dep],
(CAST(ISNULL(SUM(b.[Total Voucher]),0)+ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0)	-- modified by LUC
 AS Decimal(20,2) ) -
CAST(ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)	-- modified by LUC
 AS Decimal(20,2) ))
[Cost],
CAST((ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0))	-- modified by LUC
		-
		 (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0))   --Modified by JNY Void Case added
		)

	 AS Decimal(20,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0)) -
	((ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0) - (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)) ))  = 0 THEN -- modified by LUC
	CAST(
		0
	 AS Decimal(20,2) )
WHEN 	ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(20,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  -
		(ISNULL(SUM(b.[Total Voucher]),0) + ISNULL(SUM(b.[Total XO]),0) + ISNULL(SUM( b.[Total TKT]), 0) + ISNULL(SUM(b.[Total MCO]), 0) - (ISNULL(SUM(b.[Total Voucher Void]),0)+ISNULL(SUM(b.[Total XO Void]),0) + ISNULL(SUM( b.[Total TKT Void]), 0) + ISNULL(SUM(b.[Total MCO Void]), 0)))	-- modified by LUC
	) /
		(ISNULL(SUM(b.[Total Invoice]), 0)  -  ISNULL(SUM(b.[Total Credit Note]), 0))  * 100
	AS Decimal(20,2) )
END
INTO #result_normal
From 
 #BookingSum b
, peomstr m (nolock)												-- added by IVT
where
-- b.TeamCode like @TeamCode										-- commented by IVT
b.companycode = @CompanyCode									-- added by IVT
and b.companycode = m.companycode 									-- added by IVT
and b.bkgref = m.bkgref											-- added by IVT
and 
(
m.teamcode in (select distinct teamcode from #tempteam )
or 
	(
	m.staffcode in ( select distinct staffcode from #tempstaff )	 
	and 
	m.teamcode  = (select teamcode from peostaff where staffcode = m.staffcode and companycode = m.companycode)
	)
)
group by  m.companycode, m.teamcode
, m.staffcode					-- added by IVT on 12Nov2003
-- debug
-- select * from #result_normal
-- debug
if @Report_TypeCode = 'TPR' -- team productivity report
begin 
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char,  convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 )) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]                 
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.teamcode in (select distinct teamcode from #tempteam )
group by a.companycode, a.teamcode
order by a.teamcode
end
if @Report_TypeCode = 'TPRB' -- team productivity report - order by branch
begin 
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char,  convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 )) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]                 
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
group by a.companycode, c.branchcode
order by c.branchcode
end
if @Report_TypeCode = 'TPRC' -- team productivity report - order by branch
begin 
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char,  convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100 )) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]                 
from 
#result_normal a, #result_year_to_date b, peostaff c
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode		-- new add
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.teamcode in (select distinct teamcode from #tempteam)
group by a.companycode
order by a.companycode
end
if @Report_TypeCode = 'SPR' -- Staff productivity report
begin
select ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
, ltrim(rtrim(a.staffcode)) as staffcode
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100)) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode in ( select distinct staffcode from #tempstaff  )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode)
group by a.companycode, a.teamcode, a.staffcode
order by a.staffcode
end
if @Report_TypeCode = 'SPRT' -- Staff productivity report - order by Team
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100)) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]
from 
#result_normal a, #result_year_to_date b
where a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode in ( select distinct staffcode from #tempstaff)
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
group by a.companycode, a.teamcode
order by a.teamcode
end
if @Report_TypeCode = 'SPRB' -- Staff productivity report - order by Branch
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(c.branchcode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100)) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode)
group by a.companycode, c.branchcode
order by c.branchcode
end
if @Report_TypeCode = 'SPRC' -- Staff productivity report - order by company
begin
select 
ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.companycode)) as code
--, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
--, ltrim(rtrim(a.staffcode)) as staffcode
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as Dep
, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100)) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as YTDDep
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]
from 
#result_normal a, #result_year_to_date b, peostaff c
where 
a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
and a.staffcode = c.staffcode
and a.companycode = c.companycode
and a.staffcode in ( select distinct staffcode from #tempstaff   )
and a.teamcode = (select teamcode from peostaff where staffcode = a.staffcode and companycode = a.companycode )
group by a.companycode
order by a.companycode
end
/* debug
select * from #BookingSum
select * from #BookingSum_year_to_date
select * from #result_normal
select * from #result_year_to_date
*/
DROP TABLE #BookingSum
DROP TABLE #BookingSum_year_to_date
DROP TABLE #result_normal
DROP TABLE #result_year_to_date
END







