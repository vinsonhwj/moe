﻿

CREATE  procedure dbo.sp_MntStaffSalesmanList
@CompanyCode nchar(2),
@Login nvarchar(20)
as

DECLARE @lstr_SalesmanList varchar(4000)
DECLARE @lstr_Salesman varchar(10)
DECLARE @lstr_TmpLogin varchar(20)
DECLARE @lstr_Login varchar(20)
DECLARE @lnum_Count integer

CREATE TABLE #tmpSalesList
(
	Login nvarchar(20) null default ''
,	SalesmanList varchar(4000) null default ''
)

CREATE TABLE #tmpSales
(
	Login nvarchar(20) null default ''
,	Salesman nchar(200) null default ''
			
)

IF RTRIM(LTRIM(@Login)) <> ''
BEGIN
	INSERT INTO #tmpSales (Login, Salesman) 
	SELECT  DISTINCT Login, Salesman
	FROM StaffSalesmanMap
	WHERE CompanyCode=@CompanyCode AND Login = @Login
	GROUP BY Login, Salesman
END
ELSE
BEGIN
	INSERT INTO #tmpSales (Login, Salesman) 
	SELECT  DISTINCT Login, Salesman
	FROM StaffSalesmanMap
	WHERE CompanyCode=@CompanyCode
	GROUP BY Login, Salesman
END


SELECT @lnum_Count = 0
SELECT @lstr_SalesmanList = ''

DECLARE lcur_Salesman cursor for
SELECT Login, Salesman FROM #tmpSales ORDER BY 1, 2

open lcur_Salesman
fetch lcur_Salesman into @lstr_Login, @lstr_Salesman
while @@fetch_status = 0
begin
	SELECT @lnum_Count = @lnum_Count + 1
	IF LTRIM(RTRIM(@lstr_Login)) <> LTRIM(RTRIM(@lstr_TmpLogin)) AND @lnum_Count <> 1
	BEGIN
		INSERT INTO #tmpSalesList VALUES (@lstr_TmpLogin, SUBSTRING(@lstr_SalesmanList,3,LEN(@lstr_SalesmanList)))		
		SELECT @lstr_SalesmanList = ''
	END

	SELECT @lstr_SalesmanList = @lstr_SalesmanList + ', ' + LTRIM(RTRIM(@lstr_Salesman))
	SELECT @lstr_TmpLogin = @lstr_Login
	fetch lcur_Salesman into @lstr_Login, @lstr_Salesman
end

IF @lnum_Count > 0
BEGIN
	INSERT INTO #tmpSalesList VALUES (@lstr_TmpLogin, SUBSTRING(@lstr_SalesmanList,3,LEN(@lstr_SalesmanList)))
END

close lcur_Salesman
deallocate lcur_Salesman

SELECT  Login, SalesmanList FROM #tmpSalesList ORDER BY Login

DROP TABLE #tmpSales
DROP TABLE #tmpSalesList






