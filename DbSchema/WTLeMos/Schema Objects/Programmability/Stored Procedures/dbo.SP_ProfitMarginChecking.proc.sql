﻿

CREATE          PROCEDURE [dbo].[SP_ProfitMarginChecking]
	@CompanyCode 	VARCHAR(2) = '',
	@BkgRef 	VARCHAR(10) = '',
	@ReasonCode	CHAR(5),
	@StaffCode      NVARCHAR(10),
        @TeamCode       NVARCHAR(10),
	@DocType	CHAR(3)
AS

IF @CompanyCode <> '' AND @BkgRef <> ''
BEGIN

	DECLARE	@PEOMSTR_STAFFCODE		NCHAR(20),
			@PEOMSTR_TEAMCODE	NVARCHAR(10),
			@PEOMSTR_CLTCODE	NVARCHAR(50),
			@PEOMSTR_YIELD		DECIMAL(16,2),
			@PEOMSTR_MASTERPNR	NVARCHAR(20),
			@PEOMSTR_TOTALINVCOUNT	INTEGER,
			@PEOMSTR_TOTALDOCCOUNT	INTEGER,
			@PEOMSTR_TOTALINVAMT	DECIMAL(18,2),
			@PEOMSTR_TOTALDOCAMT	DECIMAL(18,2),
			@PEOSTAFF_UPLIMIT	DECIMAL(16,2),
			@PEOSTAFF_LOWLIMIT	DECIMAL(16,2),
			@INSERT_RECORD		INTEGER,
			@CUSTOMER_UPLIMIT	DECIMAL(16,2),
			@CUSTOMER_LOWLIMIT	DECIMAL(16,2),
			@PEOPMQ_PROFIT		DECIMAL,
			@TmpStatus 		CHAR(1),
			@PEOPMQ_STATUS		CHAR(1)

	-- 0 no need to add profit margin reminder, 1 need to add profit margin reminder
	SELECT @INSERT_RECORD = 0
	-- END

	-- Default value
	SELECT @PEOSTAFF_UPLIMIT = null
	SELECT @PEOSTAFF_LOWLIMIT = null
	SELECT @CUSTOMER_UPLIMIT = null
	SELECT @CUSTOMER_LOWLIMIT = null
	-- END


	IF EXISTS(SELECT ENABLE FROM Com_Preference (NOLOCK) WHERE CompanyCode = @CompanyCode AND PreferenceID = 'SYS_PEOPMQ' AND ENABLE = 'Y')
	BEGIN

		IF EXISTS(SELECT StaffCode FROM PeoMstr WITH (NOLOCK) WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef)
		BEGIN 
	
			SELECT 	
			@PEOMSTR_MASTERPNR = LTRIM(RTRIM(MasterPNR))
			FROM PeoMstr WITH (NOLOCK)
			WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef

			IF @DocType = 'DOC'
			BEGIN
				SELECT 	@PEOMSTR_STAFFCODE = LTRIM(RTRIM(StaffCode)),
					@PEOMSTR_TEAMCODE = LTRIM(RTRIM(TeamCode)),
					@PEOMSTR_CLTCODE = LTRIM(RTRIM(CLTODE))
				FROM PeoMstr WITH (NOLOCK)	
				WHERE CompanyCode = @CompanyCode AND BkgRef = @PEOMSTR_MASTERPNR
			END
			ELSE
			BEGIN
				SELECT 	@PEOMSTR_STAFFCODE = LTRIM(RTRIM(StaffCode)),
					@PEOMSTR_TEAMCODE = LTRIM(RTRIM(TeamCode)),
					@PEOMSTR_CLTCODE = LTRIM(RTRIM(CLTODE))
				FROM PeoMstr WITH (NOLOCK)	
				WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef
			END


	
			SELECT 	@PEOMSTR_TOTALINVCOUNT = SUM(IsNull(InvCount, 0)),
				@PEOMSTR_TOTALDOCCOUNT = SUM(IsNull(DocCount, 0)),
				@PEOMSTR_TOTALINVAMT = SUM(IsNull(InvAmt, 0)) + SUM(IsNull(InvTax, 0)),
				@PEOMSTR_TOTALDOCAMT = SUM(IsNull(DocAmt, 0)) + SUM(IsNull(DocTax, 0))
			FROM PeoMstr WITH (NOLOCK)	
			WHERE CompanyCode = @CompanyCode AND MasterPNR = @PEOMSTR_MASTERPNR

	
			SELECT @PEOPMQ_PROFIT = @PEOMSTR_TOTALINVAMT - @PEOMSTR_TOTALDOCAMT
			PRINT @PEOMSTR_TOTALINVAMT
			PRINT @PEOMSTR_TOTALDOCAMT
			PRINT @PEOPMQ_PROFIT

			IF @PEOMSTR_TOTALINVCOUNT <> 0 AND @PEOMSTR_TOTALDOCCOUNT <> 0 
			BEGIN 
				IF @PEOMSTR_TOTALINVAMT <> 0 OR @PEOMSTR_TOTALDOCAMT <> 0
				BEGIN 


					-- GET Customer UP & LOW Limit
					IF EXISTS(SELECT CltCode FROM Customer WITH (NOLOCK) WHERE CompanyCode = @CompanyCode AND CltCode = @PEOMSTR_CLTCODE)
						SELECT 	@CUSTOMER_UPLIMIT = IsNull(PRFTUP,0), 
							@CUSTOMER_LOWLIMIT = IsNull(PRFTDOWN,0) 
						FROM Customer WITH (NOLOCK)
						WHERE CompanyCode = @CompanyCode AND CltCode = @PEOMSTR_CLTCODE 
			
					-- GET Staff UP & LOW Limit
					IF EXISTS(SELECT StaffCode FROM PeoStaff WITH (NOLOCK) WHERE CompanyCode = @CompanyCode AND StaffCode = @PEOMSTR_STAFFCODE)
						SELECT 	@PEOSTAFF_UPLIMIT = IsNull(UPLIMIT,0), 
							@PEOSTAFF_LOWLIMIT = IsNull(LOWLIMIT,0)
						FROM PeoStaff WITH (NOLOCK)
						WHERE CompanyCode = @CompanyCode AND StaffCode = @PEOMSTR_STAFFCODE
	
					IF @PEOMSTR_TOTALINVAMT IS NULL OR @PEOMSTR_TOTALINVAMT = 0
					BEGIN
	
						SELECT @PEOMSTR_YIELD = 0
						SELECT @INSERT_RECORD = 1
		
					END
					ELSE
					BEGIN
	
						-- Yield Calculation --
						SELECT @PEOMSTR_YIELD =  (((@PEOMSTR_TOTALINVAMT - @PEOMSTR_TOTALDOCAMT) / @PEOMSTR_TOTALINVAMT) * 100)
						-- END
						print @PEOMSTR_YIELD
						
	
						IF @CUSTOMER_UPLIMIT IS NOT NULL AND @CUSTOMER_LOWLIMIT IS NOT NULL
						BEGIN
	
							-- If customer profit limi up and down set to 0, just check staff's limit
							IF isnull(@CUSTOMER_UPLIMIT,0) = 0 AND isnull(@CUSTOMER_LOWLIMIT,0) = 0
							BEGIN 
	
								SELECT 	@PEOSTAFF_UPLIMIT = UPLIMIT, 
									@PEOSTAFF_LOWLIMIT = LOWLIMIT 
								FROM PeoStaff WITH (NOLOCK)
								WHERE CompanyCode = @CompanyCode AND StaffCode = @PEOMSTR_STAFFCODE
	
								IF isnull(@PEOSTAFF_UPLIMIT,0)<>0 AND isnull(@PEOSTAFF_LOWLIMIT,0) <> 0
								BEGIN
	
									IF NOT ((@PEOMSTR_YIELD <= @PEOSTAFF_UPLIMIT) AND (@PEOMSTR_YIELD >= @PEOSTAFF_LOWLIMIT))
										SELECT @INSERT_RECORD = 1										
			
								END
	
		
							END
							ELSE
							BEGIN 


								IF NOT ((@PEOMSTR_YIELD <= isnull(@CUSTOMER_UPLIMIT,0)) AND (@PEOMSTR_YIELD >= isnull(@CUSTOMER_LOWLIMIT,0)))
								BEGIN 

									SELECT @INSERT_RECORD = 1
									/*
									IF @PEOSTAFF_UPLIMIT IS NOT NULL AND @PEOSTAFF_LOWLIMIT IS NOT NULL
									BEGIN 
			
										SELECT 	@PEOSTAFF_UPLIMIT = UPLIMIT, 
											@PEOSTAFF_LOWLIMIT = LOWLIMIT 
										FROM PeoStaff WITH (NOLOCK)
										WHERE CompanyCode = @CompanyCode AND StaffCode = @PEOMSTR_STAFFCODE
		
										IF @PEOSTAFF_UPLIMIT IS NOT NULL AND @PEOSTAFF_LOWLIMIT IS NOT NULL
										BEGIN
	
											IF NOT ((@PEOMSTR_YIELD <= @PEOSTAFF_UPLIMIT) AND (@PEOMSTR_YIELD >= @PEOSTAFF_LOWLIMIT))
																						
					
										END
										
									END
*/
									
								END

	

							END
		
						END	
						ELSE
						BEGIN
							SELECT 	@PEOSTAFF_UPLIMIT = UPLIMIT, 
								@PEOSTAFF_LOWLIMIT = LOWLIMIT 
							FROM PeoStaff WITH (NOLOCK)
							WHERE CompanyCode = @CompanyCode AND StaffCode = @PEOMSTR_STAFFCODE

							IF @PEOSTAFF_UPLIMIT IS NOT NULL AND @PEOSTAFF_LOWLIMIT IS NOT NULL
							BEGIN

								IF NOT ((@PEOMSTR_YIELD <= @PEOSTAFF_UPLIMIT) AND (@PEOMSTR_YIELD >= @PEOSTAFF_LOWLIMIT))
									SELECT @INSERT_RECORD = 1										
		
							END

						END
			
					END
				END	
			END

			IF @INSERT_RECORD = 1
			BEGIN
				---------- FOR DEFAULT REASON CODE
				Select @ReasonCode = (Case When IsNull(@ReasonCode,'') = '' Then 'AMD' Else @ReasonCode End)
	
				IF EXISTS(SELECT BkgRef FROM PeoPMQ WITH (NOLOCK) WHERE CompanyCode = @CompanyCode AND BkgRef = @PEOMSTR_MASTERPNR)
				BEGIN
					SELECT @PEOPMQ_STATUS = 1
	
					UPDATE PeoPMQ SET Status = @PEOPMQ_STATUS, profit = @PEOPMQ_PROFIT, yield = CONVERT(VARCHAR,@PEOMSTR_YIELD), cust_up = @CUSTOMER_UPLIMIT, 
						cust_down = @CUSTOMER_LOWLIMIT, staff_up = @PEOSTAFF_UPLIMIT, staff_down = @PEOSTAFF_LOWLIMIT, 
						UpdateOn = GetDate(), UpdateBy = @StaffCode, staff_reason = @ReasonCode, mgr_reason = NULL, acct_reason = NULL, Staff_deadline = NULL,
						TotalInvCount = @PEOMSTR_TOTALINVCOUNT, TotalDocCount = @PEOMSTR_TOTALDOCCOUNT, TotalInvAmt = @PEOMSTR_TOTALINVAMT, 
						TotalDocAmt = @PEOMSTR_TOTALDOCAMT, CancelBkg = 'N', staffcode=@PEOMSTR_STAFFCODE, teamcode=@PEOMSTR_TEAMCODE
					WHERE CompanyCode = @CompanyCode AND  BkgRef = @PEOMSTR_MASTERPNR
	
				END
				ELSE
				BEGIN
					SELECT @PEOPMQ_STATUS = 1

					INSERT INTO PeoPMQ (Companycode, Bkgref, staffcode, teamcode, status, staff_reason, mgr_reason, 
								acct_reason, staff_deadline, mgr_deadline, acct_deadline, profit, yield, 
								cust_up, cust_down, staff_up, staff_down, 
								createon, createby, updateon, updateby, 
								TotalInvCount, TotalDocCount, TotalInvAmt, TotalDocAmt, CancelBkg)
					VALUES (UPPER(@CompanyCode), @PEOMSTR_MASTERPNR, @PEOMSTR_STAFFCODE, @PEOMSTR_TEAMCODE, @PEOPMQ_STATUS, @ReasonCode, NULL,
							NULL, NULL, NULL, NULL, @PEOPMQ_PROFIT, CONVERT(VARCHAR,@PEOMSTR_YIELD), 
							@CUSTOMER_UPLIMIT, @CUSTOMER_LOWLIMIT, @PEOSTAFF_UPLIMIT, @PEOSTAFF_LOWLIMIT, 
							GetDate(), @StaffCode, GetDate(), @StaffCode,
							@PEOMSTR_TOTALINVCOUNT, @PEOMSTR_TOTALDOCCOUNT, @PEOMSTR_TOTALINVAMT, @PEOMSTR_TOTALDOCAMT, 'N')
	
				END
				
			END
			ELSE
			BEGIN

				SELECT @TmpStatus = Status  FROM PeoPMQ WHERE CompanyCode = @CompanyCode AND BkgRef = @PEOMSTR_MASTERPNR
				IF @TmpStatus <> '9' AND @TmpStatus <> '4'
				BEGIN

					UPDATE PeoPMQ SET status = 9, profit = @PEOPMQ_PROFIT, yield = CONVERT(VARCHAR,@PEOMSTR_YIELD), cust_up = @CUSTOMER_UPLIMIT, 
								cust_down = @CUSTOMER_LOWLIMIT, staff_up = @PEOSTAFF_UPLIMIT, 
								staff_down = @PEOSTAFF_LOWLIMIT, UpdateOn = GetDate(), UpdateBy = @StaffCode,
								TotalInvCount = @PEOMSTR_TOTALINVCOUNT, TotalDocCount = @PEOMSTR_TOTALDOCCOUNT, TotalInvAmt = @PEOMSTR_TOTALINVAMT, TotalDocAmt = @PEOMSTR_TOTALDOCAMT,
								staffcode=@PEOMSTR_STAFFCODE, teamcode=@PEOMSTR_TEAMCODE
					WHERE CompanyCode = @CompanyCode AND  BkgRef = @PEOMSTR_MASTERPNR
				END
	
			END
				
		END

	END

END





