﻿
create PROCEDURE dbo.sp_RptSupplierPerformanceDetail
@COMPANYCODE varchar(2),
@SUPPCODE varchar(8),
@START_DATE varchar(20),
@END_DATE varchar(20),
@EXCLUDE_HOTEL char(1),
@EXCLUDE_VOID char(1)
AS


CREATE TABLE #tmpSuppAmt
(
	SuppCode nchar(8) null default ''
,	SuppName nvarchar(70) null default ''
,	Bkgref varchar(10) null default ''
,	TeamCode varchar(10) null default ''
,	StaffCode varchar(10) null default ''
,	IssueDate datetime null
,	DepartDate datetime null
,	VoidOn datetime null
,	VoidBy varchar(10) null default ''
,	DocType varchar(50) null default ''
,	Docnum varchar(10) null default ''
,	Qty int null default 0
,	CostAmt decimal(16,2) null default 0
,	TaxAmt decimal(16,2) null default 0
,	NetAmt decimal(16,2) null default 0
)



--VOUCHER
IF @EXCLUDE_VOID = 'Y'
begin
	IF @EXCLUDE_HOTEL = 'Y'
	begin
		INSERT INTO #tmpSuppAmt
		SELECT 
		c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, 'VCH - ' + a.vchtype, 
		a.vchnum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
		FROM peovch a (nolock), peovchdetail b (nolock), peomstr m (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND m.companycode = a.companycode and m.bkgref = a.bkgref
		AND a.companycode = @COMPANYCODE
		AND a.createon between @START_DATE and @END_DATE
		AND a.voidon is null AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
		AND a.suppcode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, a.vchtype, a.vchnum
		having sum(b.hkdamt * b.qty) > 0 
	end
	else
	begin
		INSERT INTO #tmpSuppAmt
		SELECT 
		c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, 'VCH - ' + a.vchtype, 
		a.vchnum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
		FROM peovch a (nolock), peovchdetail b (nolock), peomstr m (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND m.companycode = a.companycode and m.bkgref = a.bkgref
		AND a.companycode = @COMPANYCODE
		AND a.createon between @START_DATE and @END_DATE
		AND a.voidon is null
		AND a.suppcode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, a.vchtype, a.vchnum
		having sum(b.hkdamt * b.qty) > 0 
	end
end
else
begin
	IF @EXCLUDE_HOTEL = 'Y'
	begin
		INSERT INTO #tmpSuppAmt
		SELECT 
		c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, 'VCH - ' + a.vchtype, 
		a.vchnum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
		FROM peovch a (nolock), peovchdetail b (nolock), peomstr m (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND m.companycode = a.companycode and m.bkgref = a.bkgref
		AND a.companycode = @COMPANYCODE
		AND a.createon between @START_DATE and @END_DATE
		AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
		AND a.suppcode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, a.vchtype, a.vchnum
		having sum(b.hkdamt * b.qty) > 0 
	end
	else
	begin
		INSERT INTO #tmpSuppAmt
		SELECT 
		c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, 'VCH - ' + a.vchtype, 
		a.vchnum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
		FROM peovch a (nolock), peovchdetail b (nolock), peomstr m (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND m.companycode = a.companycode and m.bkgref = a.bkgref
		AND a.companycode = @COMPANYCODE
		AND a.createon between @START_DATE and @END_DATE
		AND a.suppcode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
		a.createon, m.departdate, a.voidon, a.voidby, a.vchtype, a.vchnum
		having sum(b.hkdamt * b.qty) > 0 

	end
end


--VOUCHER
IF @EXCLUDE_VOID = 'Y'
begin
	INSERT INTO #tmpSuppAmt
	SELECT 
	c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
	a.createon, m.departdate, a.voidon, a.voidby, 'XO', 
	a.xonum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
	FROM peoxo a (nolock), peoxodetail b (nolock), peomstr m (nolock), supplier c (nolock)
	WHERE a.companycode = b.companycode and a.xonum = b.xonum
	AND c.companycode = a.companycode and c.suppcode = a.suppcode
	AND m.companycode = a.companycode and m.bkgref = a.bkgref
	AND a.companycode = @COMPANYCODE
	AND a.createon between @START_DATE and @END_DATE
	AND a.voidon is null
	AND a.suppcode = @SUPPCODE
	GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
	a.createon, m.departdate, a.voidon, a.voidby, a.xonum
	having sum(b.hkdamt * b.qty) > 0 
end
else
begin
	INSERT INTO #tmpSuppAmt
	SELECT 
	c.suppcode, c.suppname, a.bkgref, m.teamcode, m.staffcode,
	a.createon, m.departdate, a.voidon, a.voidby, 'XO', 
	a.xonum, sum(b.qty), sum(b.costamt), sum(b.taxamt), sum(b.hkdamt * b.qty) 
	FROM peoxo a (nolock), peoxodetail b (nolock), peomstr m (nolock), supplier c (nolock)
	WHERE a.companycode = b.companycode and a.xonum = b.xonum
	AND c.companycode = a.companycode and c.suppcode = a.suppcode
	AND m.companycode = a.companycode and m.bkgref = a.bkgref
	AND a.companycode = @COMPANYCODE
	AND a.createon between @START_DATE and @END_DATE
	AND a.suppcode = @SUPPCODE
	GROUP BY  c.suppcode, c.suppname,a.bkgref, m.teamcode, m.staffcode,
	a.createon, m.departdate, a.voidon, a.voidby, a.xonum
	having sum(b.hkdamt * b.qty) > 0 

end



SELECT suppcode, suppname,  bkgref, teamcode, staffcode
, convert(varchar,issuedate,103) as issuedate, convert(varchar,voidon,103) as voidon
, voidby, doctype, docnum, qty, costamt, taxamt, netamt as hkdamt 
FROM #tmpSuppAmt 
ORDER BY suppcode, issuedate

DROP TABLE #tmpSuppAmt



