﻿


--exec SP_SalesHeaderFile '2010-12-01','2010-12-31','SG','hn0026|hn0027'

CREATE  procedure dbo.SP_SalesHeaderFile
    @createonfrom datetime,
    @createonto datetime,
    @companycode varchar(2),
    @clt_code varchar(1000)
as


select * into #tmpclt from dbo.SplitCltCode(@clt_code)

set nocount on
-----**************Tmp1
select IsNull(b.GemsCode,'          ') as GemsCode,
 c.cltcode as cltcode, 
b.ticket as ticket, 
'AS' as partner, 
e.countrycode as countrycode,
 b.airline as airline,  
case when charindex('/', b.paxname) > 0 then upper(b.paxname) else
 upper((select top 1 rtrim(paxlname) + '/' + rtrim(paxfname) + ' ' + rtrim(paxtitle) from peopax where companycode = b.companycode and bkgref = b.bkgref  
and ((rtrim(paxlname) + ' ' + rtrim(paxfname) + rtrim(paxtitle)) = rtrim(b.paxname) or (rtrim(paxlname) + ' ' + rtrim(paxfname)) = rtrim(b.paxname)))) end  as traveller, 
isnull(b.COD1, '-') as [COD 1], 
isnull(b.COD2, '-') as [COD 2], 
isnull(year(b.IssueOn),'    ') as [Invoice Year],
isnull(month(b.IssueOn), '  ') as [Invoice Month], 
'1' as [No. of Travellers], 
b.fullfareWOTax as [FullFare], 
b.fullcurr as [Full Fare Currency Code], 
b.PaidFareWOTax as [Fare Paid], 
b.paidcurr as [Fare Paid Currency Code], 
b.lowFareWOTax as [Low Fare], 
IsNull(b.lowCurr,'HKD') as [Low Fare Currency Code], 
case when rtrim(b.AirReason) = '99' then 'LF' else rtrim(b.AirReason) end as [Reason Code], 
b.destination as [Destination Airport Code], [Credit Card Company Code] = '  ', 

--*******Credit card******-
case when left(b.Formpay,2) = 'CC' then
	case when isnumeric(substring(b.FormPay, 3, 1)) >  0 and (charindex('/', b.FormPay) > 0) then  left(b.FormPay, charindex('/', b.FormPay) - 1) 
	 when isnumeric(substring(b.FormPay, 5, 1)) >  0 and (charindex('/', b.FormPay) > 0) then  right(left(b.FormPay, charindex('/', b.FormPay) - 1), 
	len(left(b.FormPay, charindex('/', b.FormPay) - 1)) -2)  else b.FormPay end 
when left(b.Formpay,1) = '*' then
	case when isnumeric(substring(b.FormPay, 4, 1)) >  0 and (charindex(' ', b.FormPay) > 0) then  substring(b.FormPay, 2,charindex(' ', b.FormPay) - 1) 
	when (isnumeric(substring(b.FormPay, 4, 1)) >  0) and (charindex('#', b.FormPay) > 0) then  substring(b.FormPay, 2,charindex('#', b.FormPay) - 2) 
	else b.FormPay end 
end
as [Credit Card No], 
--*******Credit card******-

 isnull(b.COD3, '-') as [COD 3], 
isnull(b.pseudo, '-') as [Pseudo City Code], 
 '-' as [IATA No.], [CRS id] = case m.sourcesystem when 'AIR' then '1A' when 'GLO' then '1G'  when 'IUR' then '1W'  else '1P' end,  
convert(decimal, isnull(b.taxamount1, 0)) + convert(decimal, isnull(b.taxamount2, 0))  + convert(decimal, isnull(b.taxamount3, 0))  as [Tax],
  e.localcurr as [Tax Currency Code], b.commission as [Commission], e.localcurr as [Comission Currency Code], 
 b.PNR as [Record Locator], isnull(b.tour_code, '-') as [Tour Code],  
isnull((select top 1 invnum from peoinv WHERE bkgref = b.bkgref and companycode = b.companycode and isnull(void2jba,'') = '' order by CreateOn desc), '         ') as [Invoice No.],
isnull((select top 1 convert(varchar,createon, 106) from peoinv WHERE bkgref = b.bkgref and companycode = b.companycode and isnull(void2jba,'') = '' order by CreateOn desc),
convert(varchar, convert(datetime, '1900/01/01'), 106)) as [Invoice Date], 
s.staffname as [Booking Agent], 
convert(varchar, m.createon, 106) as [Booking Date],
 case when (isnull(b.FareType,'') = 'N' or isnull(b.FareType,'') = '') then 'N' else 'S' end as [Fare Type], 
case when b.tktType is null or b.tktType = '' then 'P' else b.tktType end as [Ticket Type],  
[Booking Type] =  case upper(substring(b.bkgref, 1, 1)) when 'P' then '-'  else 'G' end, '-' as [Sold/Ticketed Ind.], 
convert(varchar, b.issueon, 106) as [Ticket Issue Date], isnull(b.transactionfee, 0) as [Transaction Fee],
e.localcurr as [Transaction Fee Currency Code],  isnull(b.farecaldata, '-') as [Fare Calculation], isnull(mi.bticlient,'        ') as btiClient  
into #SHTmp1
from peomistkt b  
inner join peomstr m on b.companycode = m.companycode and b.bkgref = m.bkgref 
 inner join peostaff s on s.companycode = m.companycode and s.staffcode = m.staffcode 
 left outer join customer c on b.companycode = c.companycode and b.cltcode = c.cltcode  
inner join company e on b.companycode = e.companycode  
left outer join misclient mi on mi.wrsclient = b.cltcode and mi.companycode=b.companycode 
inner join #tmpclt t on t.cltcode = b.cltcode
 where b.companycode = @companycode 
and b.Issueon BETWEEN @createonfrom and @createonto 
and  ((b.voidon is null or ltrim(rtrim(b.voidon)) = '') 
and (b.voidby is null or ltrim(rtrim(b.voidby)) = '') 
and (b.voidteam is null or ltrim(rtrim(b.voidteam)) = '') 
and (b.voidreason is null or ltrim(rtrim(b.voidreason)) = '')) 
--and b.CltCode In ('hn0026') 
group by b.GemsCode, c.cltcode, b.ticket, e.countrycode, b.airline, b.paxname, b.fullfareWOTax, b.
fullcurr, b.PaidFareWOTax, b.paidcurr, b.lowfare, b.lowcurr,  b.AirReason, b.destination, b.cod1, b.cod2, b.cod3, b.destination, 
b.pseudo, b.taxamount1, b.taxamount2, b.taxamount3, e.localcurr,  b.commission, b.PNR, b.tour_code, s.staffname, m.createon, b.issueon, 
b.transactionfee, b.farecaldata,  b.FormPay, m.sourcesystem, b.companycode, b.bkgref, b.FareType, b.TktType, b.AirReason, b.lowFareWOTax, mi.bticlient 

-----**************Tmp2
select IsNull(b.GemsCode,'          ') as GemsCode, c.cltcode as [cltcode], b.ticket as [ticket], 'AS' as [partner], e.countrycode as [countrycode], b.airline as [airline],  case when charindex('/', b.paxname) > 0 then upper(b.paxname) else upper((select top 1 rtrim(paxlname) + '/' + rtrim(paxfname) + ' ' + rtrim(paxtitle) from peopax where companycode = b.companycode and bkgref = b.bkgref  and ((rtrim(paxlname) + ' ' + rtrim(paxfname) + rtrim(paxtitle)) = rtrim(b.paxname) or (rtrim(paxlname) + ' ' + rtrim(paxfname)) = rtrim(b.paxname)))) end  as [traveller], isnull(b.COD1, '-') as [COD 1], 
isnull(b.COD2, '-') as [COD 2],  isnull(year(b.IssueOn),'    ') as [Invoice Year], isnull(month(b.IssueOn), '  ') as [Invoice Month], '1' as [No. of Travellers], b.fullfareWOTax as [FullFare], b.fullcurr as [Full Fare Currency Code], b.PaidFareWOTax as [Fare Paid], b.paidcurr as [Fare Paid Currency Code], b.lowFareWOTax as [Low Fare], IsNull(b.lowCurr,'HKD') as [Low Fare Currency Code], case when rtrim(b.AirReason) = '99' then 'LF' else rtrim(b.AirReason) end as [Reason Code], b.destination as [Destination Airport Code],
 [Credit Card Company Code] = '  ', 

/*
 case when (substring(b.FormPay, 3, 1) >  '0' and substring(b.FormPay, 3, 1) < '9') and (charindex('/', b.FormPay) > 0) then  left(b.FormPay, charindex('/', b.FormPay) - 1)  
when (substring(b.FormPay, 5, 1) >  '0' and substring(b.FormPay, 5, 1) < '9') and (charindex('/', b.FormPay) > 0) then  right(left(b.FormPay, charindex('/', b.FormPay) - 1), len(left(b.FormPay, charindex('/', b.FormPay) - 1)) -2)  
else b.FormPay end as 'Credit Card No', 
*/

case when left(b.Formpay,2) = 'CC' then
	case when isnumeric(substring(b.FormPay, 3, 1)) >  0 and (charindex('/', b.FormPay) > 0) then  left(b.FormPay, charindex('/', b.FormPay) - 1) 
	 when isnumeric(substring(b.FormPay, 5, 1)) >  0 and (charindex('/', b.FormPay) > 0) then  right(left(b.FormPay, charindex('/', b.FormPay) - 1), 
	len(left(b.FormPay, charindex('/', b.FormPay) - 1)) -2)  else b.FormPay end 
when left(b.Formpay,1) = '*' then
	case when isnumeric(substring(b.FormPay, 4, 1)) >  0 and (charindex(' ', b.FormPay) > 0) then  substring(b.FormPay, 2,charindex(' ', b.FormPay) - 1) 
	when (isnumeric(substring(b.FormPay, 4, 1)) >  0) and (charindex('#', b.FormPay) > 0) then  substring(b.FormPay, 2,charindex('#', b.FormPay) - 2) 
	else b.FormPay end 
end
as [Credit Card No],

 isnull(b.COD3, '-') as [COD 3],
isnull(b.pseudo, '-') as [Pseudo City Code],  '-' as [IATA No.],
 [CRS id] = case m.sourcesystem when 'AIR' then '1A' when 'GLO' then '1G'  when 'IUR' then '1W'  else '1P' end,  convert(decimal, isnull(b.taxamount1, 0)) + convert(decimal, isnull(b.taxamount2, 0))  + convert(decimal, isnull(b.taxamount3, 0))  as [Tax],
 e.localcurr as [Tax Currency Code], 
b.commission as [Commission],
 e.localcurr as [Comission Currency Code],  b.PNR as [Record Locator],
 isnull(b.tour_code, '-') as [Tour Code], 
 case when vk.InvNum is null then
 (select top 1 InvNum from peoinv WHERE bkgref = b.bkgref and companycode = b.companycode and IsNull(void2jba,'') = '' order by CreateOn desc) 
 else isnull(vk.InvNum, '         ') end as [Invoice No.], 
case when vk.createon is null then (select top 1 isnull(convert(varchar,createon, 106), convert(varchar, convert(datetime, '1900-01-01'), 106)) from peoinv WHERE bkgref = b.bkgref and companycode = b.companycode and IsNull(void2jba,'') = '' order by CreateOn desc)  
else isnull(convert(varchar,vk.createon, 106), 
convert(varchar, convert(datetime, '1900-01-01'), 106)) end as [Invoice Date], 
s.staffname as [Booking Agent], convert(varchar, m.createon, 106) as [Booking Date], 
case when (isnull(b.FareType,'') = 'N' or isnull(b.FareType,'') = '') then 'N' else 'S' end as [Fare Type],
 case when b.tktType is null or b.tktType = '' then 'P' else b.tktType end as [Ticket Type],  [Booking Type] =  case upper(substring(b.bkgref, 1, 1)) when 'P' then '-'  else 'G' end, 
'-' as [Sold/Ticketed Ind.], convert(varchar, b.issueon, 106) as [Ticket Issue Date], isnull(b.transactionfee, 0) as [Transaction Fee], e.localcurr as [Transaction Fee Currency Code],  isnull(b.farecaldata, '-') as [Fare Calculation], isnull(mi.bticlient,'        ') as btiClient 
 into #SHTmp2  
from peotkt b  
inner join peomstr m on b.companycode = m.companycode and b.bkgref = m.bkgref  
inner join peostaff s on s.companycode = m.companycode and s.staffcode = m.staffcode  
left outer join customer c on m.companycode = c.companycode and m.cltode = c.cltcode  
inner join company e on b.companycode = e.companycode  
left outer join misclient mi on mi.wrsclient = b.cltcode and mi.companycode=b.companycode  
Left outer join peoinvtkt vk on vk.companycode=b.companycode and vk.ticket=b.ticket and vk.tktseq=b.tktseq 
inner join #tmpclt t on t.cltcode = m.cltode
 where b.companycode = @companycode 
and b.Issueon BETWEEN @createonfrom and @createonto 
and  ((b.voidon is null or ltrim(rtrim(b.voidon)) = '') 
and (b.voidby is null or ltrim(rtrim(b.voidby)) = '') 
and (b.voidteam is null or ltrim(rtrim(b.voidteam)) = '') 
and (b.voidreason is null or ltrim(rtrim(b.voidreason)) = ''))  
--and m.Cltode In ('hn0026') 
group by b.GemsCode, c.cltcode, m.cltode, b.ticket, e.countrycode, b.airline, b.paxname, b.fullfareWOTax, b.fullcurr, b.PaidFareWOTax, b.paidcurr, b.lowfare, b.lowcurr,  b.AirReason, b.destination, b.cod1, b.cod2, b.cod3, b.destination, b.pseudo, b.taxamount1, b.taxamount2, b.taxamount3, e.localcurr,  b.commission, b.PNR, b.tour_code, s.staffname, m.createon, b.issueon, b.transactionfee, b.farecaldata,  b.
FormPay, m.sourcesystem, b.companycode, b.bkgref, b.FareType, b.TktType, b.AirReason, b.lowFareWOTax, mi.bticlient, vk.createon, vk.invnum 



----**************result
 Select [GemsCode], [cltcode], [ticket], [partner], [countrycode], [airline], [traveller], [COD 1], [COD 2], [Invoice Year],  
[Invoice Month], [No. of Travellers], [FullFare], [Full Fare Currency Code], [Fare Paid], [Fare Paid Currency Code],  
[Low Fare], [Low Fare Currency Code], [Reason Code], [Destination Airport Code],  
Case When isnumeric(substring([Credit Card No], 3, 1)) > 0  then 
left([Credit Card No], 2) else '  ' end as [Credit Card Company Code],  Case When isnumeric(substring([Credit Card No], 3, 1)) > 0
 then substring([Credit Card No],3,len([Credit Card No])-1) else '  ' end as [Credit Card No],  [COD 3], [Pseudo City Code], [IATA No.], 
[CRS id], [Tax], [Tax Currency Code],   [Record Locator], [Tour Code], [Invoice No.], [Invoice Date],  [Booking Agent], [Booking Date], [Fare Type], [Ticket Type],
 [Booking Type], [Sold/Ticketed Ind.],  [Ticket Issue Date], [Transaction Fee], [Transaction Fee Currency Code], [btiClient]  
From #SHTmp1  

UNION 
 Select [GemsCode], [cltcode], [ticket], [partner], [countrycode], [airline], [traveller], [COD 1], [COD 2], [Invoice Year],  [Invoice Month], [No. of Travellers], [FullFare], [Full Fare Currency Code], [Fare Paid], [Fare Paid Currency Code],  [Low Fare], [Low Fare Currency Code], [Reason Code], [Destination Airport Code], 
 Case When isnumeric(substring([Credit Card No], 3, 1)) > 0 then left([Credit Card No], 2) else '  ' end as [Credit Card Company Code], 
 Case When isnumeric(substring([Credit Card No], 3, 1)) > 0 then substring([Credit Card No],3,len([Credit Card No])-1) else '  ' end as [Credit Card No], 
 [COD 3], [Pseudo City Code], [IATA No.], [CRS id], [Tax], [Tax Currency Code],   [Record Locator], [Tour Code], [Invoice No.], [Invoice Date],  [Booking Agent], [Booking Date], [Fare Type],
 [Ticket Type], [Booking Type], [Sold/Ticketed Ind.],  [Ticket Issue Date], [Transaction Fee], [Transaction Fee Currency Code], [btiClient] 
From #SHTmp2  Where ticket not in (select ticket from #SHTmp1)  order by cltcode, ticket  


--select [credit card no] from #SHTmp1
--select [credit card no] from #SHTmp2


drop table #SHTmp1  
drop table #SHTmp2
drop table #tmpclt



