﻿





CREATE      procedure dbo.sp_ApproveMstrPnr
@COMPANYCODE nchar(4),
@MASTERPNR varchar(10),
@LOGIN varchar(10),
@APPROVEFLAG nchar(1)

as 
 
DECLARE @ttlinv decimal(20, 2)
DECLARE @ttlinvtax decimal(20, 2)
DECLARE @ttldoc decimal(20, 2)
DECLARE @ttldoctax decimal(20, 2)
DECLARE @ttlinvcount integer
DECLARE @ttldoccount integer
DECLARE @ttlinvgst decimal(20, 2)
DECLARE @ttldocgst decimal(20, 2)
DECLARE @ttlprofit decimal(20, 2)
DECLARE @localcurr varchar(3)
DECLARE @lstr_segnum varchar(4);
DECLARE @lstr_Approveby varchar(10);
DECLARE @ldt_ApproveOn datetime;
SELECT @lstr_segnum = '0001'


SELECT @localcurr = localcurr FROM company WHERE companycode = @COMPANYCODE
SELECT @lstr_segnum = right(rtrim('0000' +convert(varchar,isnull(segnum+1,1))),4)
, @ttlinv = isnull(invamt,0)
, @ttlinvtax = isnull(invtax,0)
, @ttldoc = isnull(docamt,0)
, @ttldoctax = isnull(doctax,0)
, @ttlinvcount = isnull(invcount,0)
, @ttldoccount = isnull(doccount,0)
, @ttlinvgst = isnull(ttlinvgst,0)
, @ttldocgst = isnull(ttldocgst,0) 
, @ttlprofit = isnull(profit,0)
FROM MstrPnrTotal
WHERE COMPANYCODE = @COMPANYCODE AND masterpnr = @MASTERPNR AND islast = 'Y'


UPDATE MstrPnrTotal SET ISLAST = NULL WHERE COMPANYCODE = @COMPANYCODE AND MASTERPNR = @MASTERPNR

IF @APPROVEFLAG = 'A'
BEGIN
	SELECT @lstr_Approveby = @LOGIN;
	SELECT @ldt_ApproveOn = getdate();

END

INSERT INTO MstrPnrTotal VALUES
(@companycode, @MASTERPNR, @lstr_segnum,@ttlinvcount,@ttldoccount,@ttlinvgst,@ttldocgst,@localcurr, @ttlinv, @localcurr, @ttlinvtax, @localcurr, @ttldoc, @ttldoctax, 'Y',getdate(),@LOGIN,@ldt_ApproveOn, @lstr_Approveby,@ttlprofit)







