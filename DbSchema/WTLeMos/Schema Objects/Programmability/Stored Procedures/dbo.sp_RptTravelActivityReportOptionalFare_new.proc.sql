﻿







--sp_RptTravelActivityReportOptionalFare_new 'SG','2010-12-01','2010-12-31 23:59:59','HT1065',''
CREATE        PROCEDURE [dbo].[sp_RptTravelActivityReportOptionalFare_new]

@COMPANYCODE varchar(2),
@START_DATE varchar(10),
@END_DATE varchar(25),
@CLT_CODE varchar(10),
@PAXNAME nvarchar(100)

AS

BEGIN


select invnum,
identity(int,1,1) as tempID, peotkt.ticket,cltcode,DepartCity,ArrivalCity  ,peotktdetail.flight,peotktdetail.class,peotkt.issueon,DepartDATE,ArrDATE,
--case when commission_per > 0 then netfare else fullfare end paidfarewotax,
netfare paidfarewotax,
--netfare,
peotkt.companycode,
rtrim(COD5) as COD5,AirReason ,convert(decimal(16,2),0) as optionalfare1,convert(decimal(16,2),0) as optionalfare2,convert(decimal(16,2),0) as potentialloss1, convert(decimal(16,2),0) as potentialloss2, convert(varchar(20),'') as TmpString
,IsNull((select top 1 paxlname +'/' + paxFname + paxtitle from peopax where bkgref = peotkt.bkgref and paxname like (paxlname +'%' + paxFname + '%')),'') as paxname,segnum, '1' as top1
into #temp
from peotkt
inner join peotktdetail on peotkt.companycode = peotktdetail.companycode
and peotkt.ticket = peotktdetail.ticket
and peotkt.tktseq = peotktdetail.tktseq
where peotkt.companycode=@COMPANYCODE and voidon is null  and paxname like '%' + @PAXNAME + '%' and cltcode = @CLT_CODE
and issueon between @START_DATE and @END_DATE
order by peotkt.ticket

---- Modified to retrieve Optional Fare --------------
Update #Temp
Set TmpString=(Case When charIndex(',', COD5) > 1 Then Left(COD5, charIndex(',',COD5) - 1) Else '' End)

Update #Temp
Set TmpString=COD5
Where TmpString=''

Update #Temp
Set TmpString=(Case When charIndex('(',rtrim(TmpString)) > 1 Then Left(rtrim(TmpString), charIndex('(',rtrim(TmpString)) - 1) Else '' End)
Where TmpString <> ''

Update #Temp
Set OptionalFare1 = (Case When IsNumeric(rtrim(TmpString)) = 1 Then convert(decimal(16,2),rtrim(TmpString)) Else 0 End)
Where TmpString <> ''


Update #Temp
Set TmpString=''

Update #Temp
Set TmpString=(Case When charIndex(',', COD5) > 1 Then Right(COD5, Len(COD5) - charIndex(',',COD5) ) Else '' End)

Update #Temp
Set TmpString=(Case When charIndex('(',rtrim(TmpString)) > 1 Then Left(rtrim(TmpString), charIndex('(',rtrim(TmpString)) - 1) Else '' End)
Where TmpString <> ''

Update #Temp
Set OptionalFare2 = (Case When IsNumeric(rtrim(TmpString)) = 1 Then convert(decimal(16,2),rtrim(TmpString)) Else 0 End)
Where TmpString <> ''
-------------------------------------------------------------

select  ticket,cltcode,DepartCity,ArrivalCity  ,flight,class,issueon,DepartDATE,ArrDATE ,invnum,companycode
,paidfareWOTax,COD5,AirReason ,optionalfare1,optionalfare2,potentialloss1,potentialloss2,paxname,segnum,top1
into #tempb from #temp


delete a  from
#temp a
inner join #temp b
on a.tempid > b.tempid and a.ticket = b.ticket

update #temp
set cltcode='',DepartCity='',ArrivalCity=''  ,flight='',class='',issueon='',DepartDATE='',ArrDATE =''
,paidfareWOTax=0,COD5='',AirReason='' ,optionalfare1=0,optionalfare2=0,potentialloss1=0,potentialloss2=0,paxname='',segnum='99999'

insert into #tempb
(ticket,cltcode,DepartCity,ArrivalCity  ,flight,class,issueon,DepartDATE,ArrDATE , invnum,companycode
,paidfareWOTax,COD5,AirReason ,optionalfare1,optionalfare2,potentialloss1,potentialloss2,paxname,segnum,top1)
 select 
ticket,cltcode,DepartCity,ArrivalCity  ,flight,class,issueon,DepartDATE,ArrDATE , invnum, companycode
,paidfareWOTax,COD5,AirReason ,optionalfare1,optionalfare2,potentialloss1,potentialloss2,paxname,segnum,top1
from #temp


update a set a.top1 = '0'
from
#tempb a
inner join #tempb b
on a.segnum > b.segnum and a.ticket = b.ticket

delete from #tempb
where segnum='99999'


/*	Jay 20101118	*/
/*	Non-UATP invoice	*/

update #tempb set invnum = itr.invnum
from #tempb t 
inner join peoinvtktref itr on itr.ticket = t.ticket
inner join peoinv i on itr.invnum = i.invnum
where i.invnum not like 'HC%' and i.void2jba is null


Update #tempb
Set potentialloss1= (paidfareWOTax - optionalfare1)
Where optionalfare1<>0

Update #tempb
Set potentialloss2 = (paidfareWOTax - optionalfare2)
Where optionalfare2<>0


--select * from #tempb order by issueon

/* below scripts for generate without via systems if got any problems */


select paxname, departcity+'/'+arrivalcity as Routing,flight,Class,issueon as bookdate,DepartDATE,arrdate,t.invnum,
case when top1 =1 then paidfareWOTax else 0 end as issuedfare,
case when top1 =1 then cod5 else '' end as optionalfare1,
case when top1 = 1 then potentialloss1 else 0 end as potentialloss1,
case when top1 = 1 then isnull(r.[description],'') else '' end as reasoncode from #tempb t
left join PeoReasonCode r on r.reasontype='A' and r.reasoncode = t.AirReason
 order by issueon


drop table #temp
drop table #tempb

end








