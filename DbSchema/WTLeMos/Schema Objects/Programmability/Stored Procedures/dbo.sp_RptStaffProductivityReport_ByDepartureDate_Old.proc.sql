﻿
CREATE  PROCEDURE dbo.sp_RptStaffProductivityReport_ByDepartureDate_Old
@START_DATE CHAR(10),
@END_DATE CHAR(25),
@TEAM_CODE VarChar(10),
@COMPANY_CODE VarChar(2),
@STAFF_CODE VarChar(10)
AS

DECLARE @FinancalYear datetime
DECLARE @ldt_StartDate datetime
DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_TeamCode varchar(10)
DECLARE @lstr_StaffCode varchar(10) 
DECLARE @lnum_TTLInvamt decimal(16,2) 
DECLARE @lnum_TTLDocamt decimal(16,2)
DECLARE @lnum_TTLDepamt decimal(16,2) 
DECLARE @lnum_TTLPaxCount int
DECLARE @lnum_YTDInvamt decimal(16,2) 
DECLARE @lnum_YTDDocamt decimal(16,2)
DECLARE @lnum_YTDDepamt decimal(16,2) 
DECLARE @lnum_YTDPaxCount int

print 'Start Report at ' + convert(varchar,getdate(),109)

CREATE TABLE #tmpReportResult
(
	CompanyCode varchar(2) not null default ''
,	StaffCode varchar(10) not null default ''
,	TeamCode varchar(10) not null default ''
,	TTL_Invoice decimal(16,2) not null default 0.0
,	TTL_Deposit decimal(16,2) not null default 0.0
,	TTL_DocAmt decimal(16,2) not null default 0.0
,	TTL_Paxcount decimal(16,2) not null default 0.0
,	YTD_Invoice decimal(16,2) not null default 0.0
,	YTD_Deposit decimal(16,2) not null default 0.0
,	YTD_DocAmt decimal(16,2) not null default 0.0
,	YTD_Paxcount decimal(16,2) not null default 0.0
	
)

CREATE TABLE #tempStaff
(
	CompanyCode varchar(2) not null default ''
,	Teamcode varchar(10) not null default ''
,	Staffcode varchar(10) not null default ''
)

if ltrim(rtrim(@STAFF_CODE)) <> ''
begin
	INSERT INTO #tempStaff 
	(Companycode, TeamCode, StaffCode)
	SELECT Companycode, Teamcode, Staffcode FROM peostaff (nolock) WHERE companycode = @COMPANY_CODE AND Staffcode = @STAFF_CODE
end
else
begin
	INSERT INTO #tempStaff 
	(Companycode, TeamCode, StaffCode)
	SELECT Companycode, TeamCode, staffcode FROM peostaff (nolock) WHERE companycode = @COMPANY_CODE AND teamcode = @TEAM_CODE
end

INSERT INTO #tmpReportResult (Companycode, TeamCode, Staffcode) SELECT CompanyCode, Teamcode, Staffcode from #tempStaff


-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end

print 'Start Total Invoice at ' + convert(varchar,getdate(),109)

-- Total Invoice amount, Doc Amount 
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	ISNULL(SUM(InvAmt + InvTax), 0) AS TTL_Invoice,
	ISNULL(SUM(docamt + doctax), 0) as TTL_Docamt
INTO #tmpTTLInvDoc
FROM              dbo.peomstr mstr (nolock), #tempStaff staff
WHERE	 mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @START_DATE AND @END_DATE
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode


DECLARE lcur_TTLInvdoc CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, TTL_Invoice, TTL_Docamt
FROM #tmpTTLInvDoc

OPEN lcur_TTLInvdoc
FETCH lcur_TTLInvdoc INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLInvamt, @lnum_TTLDocamt
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET TTL_Invoice = @lnum_TTLInvamt
		, TTL_Docamt = @lnum_TTLDocamt
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, TTL_Invoice, TTL_Docamt)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLInvamt, @lnum_TTLDocamt)

	end
	FETCH lcur_TTLInvdoc INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLInvamt, @lnum_TTLDocamt
END
CLOSE lcur_TTLInvdoc
DEALLOCATE lcur_TTLInvdoc
DROP TABLE #tmpTTLInvDoc

print 'Start Deposit Invoice at ' + convert(varchar,getdate(),109)

-- Total Deposit Amount
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	ISNULL(SUM(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end), 0) as TTL_DepositAmt
INTO #tmpTTLDepsoit
FROM   dbo.peomstr mstr (nolock), dbo.peoinv inv (nolock), #tempStaff staff
WHERE inv.Companycode = mstr.Companycode AND inv.bkgref = mstr.bkgref
AND mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @START_DATE AND @END_DATE
AND inv.invType = 'DEP'
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode

DECLARE lcur_TTLDeposit CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, TTL_DepositAmt
FROM #tmpTTLDepsoit

OPEN lcur_TTLDeposit
FETCH lcur_TTLDeposit INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLDepamt
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET TTL_Deposit = @lnum_TTLDepamt
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, TTL_Deposit)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLDepamt)

	end
	FETCH lcur_TTLDeposit INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLDepamt
END
CLOSE lcur_TTLDeposit
DEALLOCATE lcur_TTLDeposit
DROP TABLE #tmpTTLDepsoit

print 'Start Pax at ' + convert(varchar,getdate(),109)

-- Total Pax Count
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	count(1) as PaxCount
INTO #tmpTTLPaxCont
FROM   dbo.peomstr mstr (nolock), dbo.peopax pax (nolock), #tempStaff staff
WHERE pax.Companycode = mstr.Companycode AND pax.bkgref = mstr.bkgref
AND mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @START_DATE AND @END_DATE
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode

DECLARE lcur_TTLPaxcount CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, PaxCount
FROM #tmpTTLPaxCont

OPEN lcur_TTLPaxcount
FETCH lcur_TTLPaxcount INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLPaxCount
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET TTL_Paxcount = @lnum_TTLPaxCount
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, TTL_Paxcount)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLPaxCount)

	end
	FETCH lcur_TTLPaxcount INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_TTLPaxCount
END
CLOSE lcur_TTLPaxcount
DEALLOCATE lcur_TTLPaxcount
DROP TABLE #tmpTTLPaxCont


-- Total Invoice amount, Doc Amount 
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	ISNULL(SUM(InvAmt + InvTax), 0) AS YTD_Invoice,
	ISNULL(SUM(docamt + doctax), 0) as YTD_Docamt
INTO #tmpYTDInvDoc
FROM              dbo.peomstr mstr (nolock), #tempStaff staff
WHERE	 mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @FinancalYear AND @END_DATE
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode


DECLARE lcur_YTDInvdoc CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, YTD_Invoice, YTD_Docamt
FROM #tmpYTDInvDoc

OPEN lcur_YTDInvdoc
FETCH lcur_YTDInvdoc INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDInvamt, @lnum_YTDDocamt
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET YTD_Invoice = @lnum_YTDInvamt
		, YTD_Docamt = @lnum_YTDDocamt
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, YTD_Invoice, YTD_Docamt)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDInvamt, @lnum_YTDDocamt)

	end
	FETCH lcur_YTDInvdoc INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDInvamt, @lnum_YTDDocamt
END
CLOSE lcur_YTDInvdoc
DEALLOCATE lcur_YTDInvdoc
DROP TABLE #tmpYTDInvDoc

print 'Start YTD Deposit Invoice at ' + convert(varchar,getdate(),109)

-- Total Deposit Amount
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	ISNULL(SUM(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end), 0) as YTD_DepositAmt
INTO #tmpYTDDepsoit
FROM   dbo.peomstr mstr (nolock), dbo.peoinv inv (nolock), #tempStaff staff
WHERE inv.Companycode = mstr.Companycode AND inv.bkgref = mstr.bkgref
AND mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @FinancalYear AND @END_DATE
AND inv.invType = 'DEP'
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode

DECLARE lcur_YTDDeposit CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, YTD_DepositAmt
FROM #tmpYTDDepsoit

OPEN lcur_YTDDeposit
FETCH lcur_YTDDeposit INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDDepamt
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET YTD_Deposit = @lnum_YTDDepamt
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, YTD_Deposit)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDDepamt)

	end
	FETCH lcur_YTDDeposit INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDDepamt
END
CLOSE lcur_YTDDeposit
DEALLOCATE lcur_YTDDeposit
DROP TABLE #tmpYTDDepsoit

print 'Start YTD Pax at ' + convert(varchar,getdate(),109)

-- Total Pax Count
SELECT  mstr.CompanyCode,						
	mstr.StaffCode,
	mstr.TeamCode,
	count(1) as PaxCount
INTO #tmpYTDPaxCont
FROM   dbo.peomstr mstr (nolock), dbo.peopax pax (nolock), #tempStaff staff
WHERE pax.Companycode = mstr.Companycode AND pax.bkgref = mstr.bkgref
AND mstr.staffcode = staff.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.DepartDate BETWEEN @FinancalYear AND @END_DATE
GROUP BY mstr.CompanyCode, mstr.TeamCode, mstr.StaffCode

DECLARE lcur_YTDPaxcount CURSOR FOR
SELECT Companycode, Teamcode, Staffcode, PaxCount
FROM #tmpYTDPaxCont

OPEN lcur_YTDPaxcount
FETCH lcur_YTDPaxcount INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDPaxCount
WHILE @@FETCH_STATUS = 0
BEGIN

	IF EXISTS (SELECT staffcode FROM #tmpReportResult WHERE Companycode = @lstr_Companycode AND teamcode = @lstr_TeamCode AND staffcode = @lstr_Staffcode)
	begin
		UPDATE #tmpReportResult 
		SET YTD_Paxcount = @lnum_YTDPaxCount
		WHERE Companycode = @lstr_Companycode
		AND TeamCode = @lstr_TeamCode AND StaffCode = @lstr_StaffCode
	end
	else
	begin
		INSERT INTO #tmpReportResult
		(CompanyCode, Teamcode, Staffcode, YTD_Paxcount)
		VALUES
		(@lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDPaxCount)

	end
	FETCH lcur_YTDPaxcount INTO @lstr_Companycode, @lstr_TeamCode, @lstr_StaffCode, @lnum_YTDPaxCount
END
CLOSE lcur_YTDPaxcount
DEALLOCATE lcur_YTDPaxcount
DROP TABLE #tmpYTDPaxCont

SELECT Companycode, TeamCode + '/' + Staffcode as code, staffcode
, CASE WHEN TTL_Paxcount <> 0 THEN  TTL_Paxcount else 0 END AS [Total Pax]
, CASE WHEN TTL_Invoice <> 0 THEN  TTL_Invoice else 0 END AS [Price]
, CASE WHEN TTL_Deposit <> 0 THEN  TTL_Deposit else 0 END AS [Dep]
, CASE WHEN TTL_DocAmt <> 0 THEN  TTL_DocAmt else 0 END AS [Cost]
, CASE WHEN TTL_Invoice-TTL_DocAmt <> 0 THEN TTL_Invoice-TTL_DocAmt else 0 END as Margin
, CASE WHEN TTL_Invoice <> 0 THEN convert(decimal(16,2),(TTL_Invoice-TTL_DocAmt)*100/TTL_Invoice) else 0 END [Yield(%)]
, CASE WHEN YTD_Paxcount <> 0 THEN  YTD_Paxcount else 0 END AS [YTD Total Pax]
, CASE WHEN YTD_Invoice <> 0 THEN  YTD_Invoice else 0 END AS [YTDPrice]
, CASE WHEN YTD_Deposit <> 0 THEN  YTD_Deposit else 0 END AS [YTDDep]
, CASE WHEN YTD_DocAmt <> 0 THEN  YTD_DocAmt else 0 END AS [YTDCost]
, CASE WHEN YTD_Invoice-YTD_DocAmt <> 0 THEN YTD_Invoice-YTD_DocAmt else 0 END as YTDMargin
, CASE WHEN YTD_Invoice <> 0 THEN convert(decimal(16,2),(YTD_Invoice-YTD_DocAmt)*100/YTD_Invoice) else 0 END [YTDYield(%)]

FROM #tmpReportResult




DROP TABLE #tempStaff
DROP TABLE #tmpReportResult






