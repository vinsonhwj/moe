﻿







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GeteReportSyncList]
	-- Add the parameters for the stored procedure here
@companycode nchar(2)
AS
BEGIN

	begin tran

		select * into #temp_eReportSyncLog from eReportSyncLog where companycode = @companycode and SyncStatus = '0'-- and tablename like 'eRpt_%'-- and docnum = 'HC00036183'
		order by id

		select a.* into #temp_update from
		#temp_eReportSyncLog a inner join #temp_eReportSyncLog b
		on a.SyncTable = b.SyncTable
		and a.DOCNUM = b.DOCNUM
		and a.SyncStatus = b.SyncStatus
		and a.[id] < b.[id]
--		and a.SyncStatus = '0'

		delete a from
		#temp_eReportSyncLog a inner join #temp_eReportSyncLog b
		on a.SyncTable = b.SyncTable
		and a.DOCNUM = b.DOCNUM
		and a.SyncStatus = b.SyncStatus
		and a.[id] < b.[id]
--		and a.SyncStatus = '0'

		update eReportSyncLog set SyncStatus = 1
		from eReportSyncLog
		inner join #temp_update 
		on eReportSyncLog.[id] = #temp_update.[id]

	COMMIT TRAN

	select * from  #temp_eReportSyncLog
	--select * from  #temp_update
END








