﻿


Create Procedure sp_CutOff as

Declare @CutOffDate Char(20)
Select @CutOffDate = '30 jun 2006 23:59:59'

--Print 'Drop all temp tables'
--/*
drop table tmp_cutoffdt, tmp_abnormal
drop table tmp_cutoff_bkg , tmp_cutoff_mstr , tmp_cutoff, tmp_cutoff_ttl
--*/
-- UnDoc Bkgref
--/*
select TeamCode, StaffCode, MasterPNR, BkgRef, BkgDate=CreateOn, CltCode=Cltode, Type='UNDOC'
into tmp_cutoff
from peomstr (nolock) where createon <= @cutoffdate and doccount =0 and invcount<> 0 
and invamt+invtax <> 0
--*/
-- Uninv bkgref
--/*
insert into tmp_cutoff
select TeamCode, StaffCode, MasterPNR, BkgRef, BkgDate=CreateOn, CltCode=Cltode, Type='UNINV'
from peomstr (nolock) where createon <= @cutoffdate and invcount =0 and doccount<> 0 
and docamt <> 0
--*/
-- Last doc(ticket) date > cut off date
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LDOC'
from peotkt (nolock) , peoinv(nolock) , peomstr(nolock) where IssueOn > @cutoffdate 
and peotkt.voidby is null and peotkt.bkgref = peoinv.bkgref 
and peoinv.CREATEON <= @cutoffdate and peoinv.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last doc(Voucher) date > cut off date
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LVDOC'
from peovch (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peovch.createOn > @cutoffdate or peovch.voidon > @cutoffdate)
and peovch.bkgref = peoinv.bkgref 
and peoinv.CREATEON <= @cutoffdate and peoinv.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last doc(MCO) date > cut off date
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LMDOC'
from peomco (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peomco.ISSUEON > @cutoffdate or peomco.issueon > @cutoffdate)
and peomco.bkgref = peoinv.bkgref 
and peoinv.CREATEON <= @cutoffdate and peoinv.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last doc(XO) date > cut off date
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LXDOC'
from peoxo (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peoxo.CREATEON > @cutoffdate or peoxo.voidon > @cutoffdate)
and peoxo.bkgref = peoinv.bkgref 
and peoinv.CREATEON <= @cutoffdate and peoinv.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last inv date > cut off date => TKT
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LTINV'
from peotkt (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peoinv.CREATEON > @cutoffdate or peoinv.voidon > @cutoffdate)
and peotkt.bkgref = peoinv.bkgref 
and peotkt.issueon <= @cutoffdate and peotkt.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last Last inv date > cut off date => XO
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LXINV'
from peoxo (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peoinv.CREATEON > @cutoffdate or peoinv.voidon > @cutoffdate)
and peoxo.bkgref = peoinv.bkgref 
and peoxo.createon <= @cutoffdate and peoxo.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last Last inv date > cut off date => VCH
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LVINV'
from peovch (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peoinv.CREATEON > @cutoffdate or peoinv.voidon > @cutoffdate)
and peovch.bkgref = peoinv.bkgref 
and peovch.createon <= @cutoffdate and peovch.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
-- Last Last inv date > cut off date => MCO
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='LMINV'
from peomco (nolock) , peoinv(nolock) , peomstr(nolock) where 
(peoinv.CREATEON > @cutoffdate or peoinv.voidon > @cutoffdate)
and peomco.bkgref = peoinv.bkgref 
and peomco.createon <= @cutoffdate and peomco.voidby is null
and peomstr.bkgref = peoinv.bkgref
--*/
--Profit Margin exceed - YWT & HWT
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='EXPMQ'
from peopmq (nolock), peomstr (nolock) where peopmq.teamcode in ('ywt','hwt') and (yield < -5 or yield > 30) 
and (left(acct_reason,3)<>'aok' or acct_reason is null) and peopmq.createon <= @cutoffdate 
and peomstr.bkgref = peopmq.bkgref
--*/
--Profit Margin exceed - YCT
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='EXPMQ'
from peopmq (nolock), peomstr (nolock) where peopmq.teamcode = 'yct' and (yield < -2 or yield > 30) 
and (left(acct_reason,3)<>'aok'  or acct_reason is null) and peopmq.createon <= @cutoffdate 
and peomstr.bkgref = peopmq.bkgref
--*/
--Profit Margin exceed - Other teams
--/*
insert into tmp_cutoff
select peomstr.TeamCode, peomstr.StaffCode, MasterPNR, peomstr.BkgRef, BkgDate=peomstr.CreateOn,CltCode=Cltode, Type='EXPMQ'
from peopmq (nolock), peomstr (nolock) where peopmq.teamcode not in ('yct','ywt','hwt') 
and (yield < 2 or yield > 30) and (left(acct_reason,3)<>'aok' or acct_reason is null) and peopmq.createon <= @cutoffdate 
and peomstr.bkgref = peopmq.bkgref
--*/
-- Group into distinct master pnr
--/*
select distinct masterpnr
into tmp_cutoff_mstr
from tmp_cutoff
--*/
-- Create Cutoff Bkg
--/*
select peomstr.TeamCode, peomstr.StaffCode, peomstr.MasterPNR, peomstr.BkgRef, 
BkgDate=peomstr.CreateOn,CltCode=Cltode
into tmp_cutoff_bkg
from tmp_cutoff_mstr, peomstr(nolock) where peomstr.masterpnr = tmp_cutoff_mstr.masterpnr
--*/
-- CutOff bkg details - Inv
--/*
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=CREATEON, InvNum=InvNum, InvAmt=HKDAmt,
DocDate=createon, SuppCode=tmp_cutoff_bkg.cltcode, Docnum=invnum,
DocCurr=Sellcurr,DocAmt=HKDAmt, DocHKD=HKDAmt, Doc_CutOff_Amt=HKDAmt, Inv_CutOff_Amt=HKDAmt, 
Profit=HKDAmt, Yield=HKDAmt
into tmp_cutoffdt
from peoinv(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peoinv.bkgref 
and substring(invnum,2,1)='i' and hkdamt <> 0 
and (voidon is null or voidon > @cutoffdate) 
--*/
-- Reset Docurr
--/*
update tmp_cutoffdt set DocDate=null, SuppCode='', Docnum='', 
DocCurr='',DocAmt=0, DocHKD=0, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0, profit=0, yield=0
--*/
-- Mark Deposit Invoice
--/*
Update tmp_cutoffdt set SuppCode='D' 
from tmp_cutoffdt, peoinv(nolock)
where peoinv.invnum = tmp_cutoffdt.invnum and invtype = 'dep'
--*/
-- Inv Voided
--/*
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=CREATEON, InvNum, InvAmt=HKDAmt*-1,
DocDate=null, SuppCode=null, Docnum='', 
DocCurr=null,DocAmt=0, DocHKD=0, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peoinv(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peoinv.bkgref 
and substring(invnum,2,1)='c' and hkdamt <> 0 
and (CREATEON > @cutoffdate or void2jba is null)
--*/
-- CutOff bkg details - Tkt
--/*
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=issueon, SuppCode, Docnum=ticket, 
DocCurr=netcurr,DocAmt=netfare+totaltax, DocHKD=netfare+totaltax, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peotkt(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peotkt.bkgref and netfare <> 0 
and (voidon is null or voidon > @cutoffdate)
-- Voided Tkt
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=voidon, SuppCode, Docnum=ticket, DocCurr=netcurr,DocAmt=(netfare+totaltax)*-1, DocHKD=(netfare+totaltax)*-1, 
Doc_CutOff_Amt=0, Inv_CutOff_Amt=0, Profit=0, Yield=0
from peotkt(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peotkt.bkgref and netfare <> 0
and voidon > @cutoffdate
--*/
-- CutOff bkg details - MCO
--/*
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=createon, SuppCode, Docnum=mconum, 
DocCurr=costcurr,DocAmt=costamt+taxamt, DocHKD=hkdamt+taxamt, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peomco(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peomco.bkgref and hkdamt <> 0
and (voidon is null or voidon > @cutoffdate)
-- Voided MCO
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=voidon, SuppCode, Docnum=mconum, 
DocCurr=costcurr,DocAmt=(costamt+taxamt)*-1, DocHKD=(hkdamt+taxamt)*-1, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peomco(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peomco.bkgref and hkdamt <> 0
and voidon > @cutoffdate
--*/
-- CutOff bkg details - VCH
--/*
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=createon, SuppCode, Docnum=vchnum, 
DocCurr=costcurr,DocAmt=costamt, DocHKD=hkdamt, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0, 
Profit=0, Yield=0
from peovch(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peovch.bkgref and hkdamt <> 0
and (voidon is null or voidon > @cutoffdate)
-- Voided VCH
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=voidon, SuppCode, Docnum=vchnum, 
DocCurr=costcurr,DocAmt=costamt*-1, DocHKD=hkdamt*-1, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peovch(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peovch.bkgref and hkdamt <> 0
and voidon > @cutoffdate
--*/
-- CutOff bkg details - XO
--/*
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=createon, SuppCode, Docnum=xonum, 
DocCurr=costcurr,DocAmt=costamt, DocHKD=hkdamt, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peoxo(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peoxo.bkgref and hkdamt <> 0
and (voidon is null or voidon > @cutoffdate)
-- Voided XO
insert into tmp_cutoffdt
Select tmp_cutoff_bkg.TeamCode, tmp_cutoff_bkg.StaffCode, tmp_cutoff_bkg.MasterPNR, tmp_cutoff_bkg.BkgRef, 
tmp_cutoff_bkg.BkgDate, tmp_cutoff_bkg.CltCode,InvDate=null, '', InvAmt=0,
DocDate=voidon, SuppCode, Docnum=xonum, 
DocCurr=costcurr,DocAmt=costamt*-1, DocHKD=hkdamt*-1, Doc_CutOff_Amt=0, Inv_CutOff_Amt=0,
Profit=0, Yield=0
from peoxo(nolock) , tmp_cutoff_bkg where tmp_cutoff_bkg.bkgref = peoxo.bkgref and hkdamt <> 0
and voidon > @cutoffdate
--*/
-- Update CutOff DocAmt
--/*
update tmp_cutoffdt set Doc_CutOff_Amt = dochkd
where docdate > @cutoffdate
--*/
-- Update CutOff InvAmt
--/*
update tmp_cutoffdt set Inv_CutOff_Amt = InvAmt
where invdate > @cutoffdate
--*/
-- Calculate Booking total
--/*
select TeamCode, masterpnr, FInvDate=min(invdate), FDocDate=min(docdate), Sell=sum(invamt), Cost=sum(dochkd), 
Cutoff_Doc=sum(Doc_CutOff_Amt), Cutoff_Inv=sum(Inv_CutOff_Amt), Profit=sum(invamt)-sum(dochkd), 
Yield=sum(Inv_CutOff_Amt) into tmp_cutoff_ttl
from tmp_cutoffdt
group by teamcode, masterpnr
--*/
-- Calculate Booking Yield
--/*
update tmp_cutoff_ttl set yield = (profit/sell)*100 where sell <> 0
--*/
-- Insert Booking total
--/*
insert into tmp_cutoffdt
Select TeamCode, '', MasterPNR, 'z$:', 
null, '',null, '', InvAmt=Sell,
DocDate=null, SuppCode='', Docnum='', 
DocCurr='',DocAmt=0, DocHKD=cost, Doc_CutOff_Amt=cutoff_doc, Inv_CutOff_Amt=cutoff_inv,
Profit=tmp_cutoff_ttl.profit, yield=tmp_cutoff_ttl.yield
from tmp_cutoff_ttl 
--*/
-- Delete within profit margin range records
/*
delete tmp_cutoffdt
where masterpnr in (select masterpnr from tmp_cutoff_ttl where yield between 2 and 30 and yield <> 0)
delete tmp_cutoffdt
where masterpnr in (select masterpnr from tmp_cutoff_ttl where yield between -5 and 30 
and teamcode in ('ywt','hwt') and yield <> 0)
delete tmp_cutoffdt
where masterpnr in (select masterpnr from tmp_cutoff_ttl where yield between -2 and 30 
and teamcode = 'yct' and yield <> 0)
delete tmp_cutoff_ttl where yield between 2 and 30 and yield <> 0
delete tmp_cutoff_ttl where yield between -5 and 30 and teamcode in ('ywt','hwt') and yield <> 0
delete tmp_cutoff_ttl where yield between -2 and 30 and teamcode = 'yct' and yield <> 0
--*/
--Print 'Create Tmp_Abnormal'
--/*
select tmp_cutoff_ttl.TeamCode, StaffCode, tmp_cutoff_ttl.Masterpnr, CltCode=Cltode,
CltName, FInvDate, FDocDate, Bkg_Sell=Sell, Bkg_Cost=Cost, Cutoff_Doc,Cutoff_Inv, Profit,Yield 
into tmp_abnormal
from tmp_cutoff_ttl, peomstr(nolock)
where tmp_cutoff_ttl.masterpnr = peomstr.bkgref 
--*/
--Delete Docdate & Invdate after cutoff date
--/*
delete tmp_cutoffdt where masterpnr in
(select masterpnr from tmp_abnormal where 
(fdocdate > @cutoffdate and finvdate > @cutoffdate) or 
(fdocdate > @cutoffdate and finvdate is null) or
(fdocdate is null and finvdate > @cutoffdate))
delete tmp_abnormal where 
(fdocdate > @cutoffdate and finvdate > @cutoffdate) or 
(fdocdate > @cutoffdate and finvdate is null) or
(fdocdate is null and finvdate > @cutoffdate)
--*/





