﻿















/*
Logic:
Select all booking that satisfy the folling criteria
1. issue AT LEAST ONE Invoice or Credit Note
2. NEVER issue document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE              PROCEDURE dbo.sp_RptAdnormalBookingReminder_Test
@COMPANY_CODE VARCHAR(2),
@TEAM_CODE    NVARCHAR(10),
@STAFF_CODE   NCHAR(20),
@START_DATE   VARCHAR(10),
@END_DATE     VARCHAR(10),
@SEARCH_MODE  CHAR(10),
@EXCLUDE_NOTE CHAR(1)
AS

Declare @SQL nvarchar(4000)
SELECT @SQL = 'select  ' +
'ltrim(rtrim(b.companycode)) as companycode ' +
', ltrim(rtrim(b.TEAMCODE)) as teamcode ' +
', ltrim(rtrim(b.STAFFCODE)) as staffcode ' +
', ltrim(rtrim(b.createby)) as createby ' +
', b.BKGREF , b.MASTERPNR ' +
', convert(datetime, b.createon, 111) as BookingDate ' +
', b.DEPARTDATE AS sDeparture 	 ' +
', CONVERT(CHAR(11), b.DEPARTDATE, 106) AS departure  ' +	
', CAST(ISNULL(SUM(b.[invamt]), 0) + ISNULL(SUM(b.[invtax]), 0) AS Decimal(20, 2)) AS Invoice ' +
', CAST(ISNULL(SUM(b.[docamt]), 0)  + ISNULL(SUM(b.[doctax]), 0) AS Decimal(20,2)) AS Document ' +
'into #tmpAdnormalBooking ' +
'FROM dbo.peomstr b  (NoLock) ' + 
'where b.companycode = ''' + @COMPANY_CODE + ''''
IF @STAFF_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND b.staffcode = ''' +  @STAFF_CODE + ''''
	END
IF @TEAM_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND b.teamcode = ''' +  @TEAM_CODE + ''''
	END 

IF @START_DATE <> ''
	BEGIN
		SELECT @SQL = @SQL + ' AND b.' + @SEARCH_MODE + '  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ' 23:59:59.999'''
	
	END
	
	
SELECT @SQL = @SQL + ' and (b.ApproveBy is null or b.ApproveBy = '''')   ' +
'GROUP BY  b.BKGREF, b.TEAMCODE, b.STAFFCODE, b.DEPARTDATE,  ' +
'b.MASTERPNR, b.createon, b.companycode, b.createby		 ' +
'having( ' +
'	(select  ' +
' 	CAST(ISNULL(SUM(b1.[invamt]), 0) + ISNULL(SUM(b1.[invtax]), 0) AS Decimal(20, 2))  ' +
'	- CAST(ISNULL(SUM(b1.[docamt]), 0)  + ISNULL(SUM(b1.[doctax]), 0) AS Decimal(20,2)) ' +
'	from dbo.peomstr b1 ' +
'	where b1.masterpnr = b.masterpnr ' +
'	and b1.companycode = b.companycode ' +
'	group by b1.masterpnr ' +
'	) < 0 ' +
'and	 ' +
'	( ' +
'	sum(isnull(b.INVCOUNT, 0)) ' +
'	) > 0 ' +
'and  ' +
'	( ' +
'	sum(isnull(b.DOCCOUNT, 0)) ' +
'	) > 0 ' +
') ' +
'order by b.teamcode, b.staffcode, b.masterpnr '




IF @EXCLUDE_NOTE = 'N' 
	begin
 	SELECT @SQL = @SQL + 
	'select *, ' +
	'case when invoice = 0 or document = 0 then ''0'' else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'from #tmpAdnormalBooking where  ' +
        'not ( Invoice = 0 and Document = 0)  ' +
	'DROP TABLE #tmpAdnormalBooking;'
	end
ELSE
   --exclude note
	begin
	SELECT @SQL = @SQL + 	
	'SELECT companycode,masterpnr,SUM(Invoice) AS invoice, SUM(document) AS document ' +
	'INTO #tmpAmt ' +
	'FROM #tmpAdnormalBooking (nolock) ' + 
	'GROUP BY companycode,masterpnr; ' +

	'SELECT companycode,masterpnr,MAX(seqnum) AS seqnum ' +
	'INTO #seq ' +
	'FROM peopfque (nolock) ' +
	'WHERE companycode = ''' + @COMPANY_CODE + '''' +
	'AND masterpnr in (select bkgref from #tmpAdnormalBooking) and masterpnr=bkgref ' +
	'GROUP BY companycode,masterpnr; ' +

	'select  a.*,case when invoice = 0 or document = 0 then ''0'' ' +
	'else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'into #tmpAdnormalBookingR from ( ' +
	'SELECT  ' +
	'undoc.*  ' +
	'from #tmpAdnormalBooking undoc  ' +
	'where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode) ' +
	' Union ' +
	'select   undoc.*  ' +
	'from #tmpAdnormalBooking undoc, #tmpAmt pfq ,#seq seq ' +
	'where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.masterpnr and undoc.companycode = seq.companycode and seq.masterpnr = pfq.masterpnr and ' +
	'pfq.invoice - ' +
	'(select isnull(AbnormalAmount,0.00) from peopfque where companycode=''' + @COMPANY_CODE + ''' and bkgref=pfq.masterpnr and seqnum=seq.seqnum) - pfq.Document < 0 ) as a   ' +
	'order by a.teamcode, a.staffcode, a.masterpnr ' +


/*
	'select  a.*,case when invoice = 0 or document = 0 then ''0'' ' +
	'else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'into #tmpAdnormalBookingR from ( ' +
	'SELECT  ' +
	'undoc.*  ' +
	'from #tmpAdnormalBooking undoc  ' +
	'where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode) ' +
	'Union ' +
	'select   ' +
	'undoc.*  ' +
	'from #tmpAdnormalBooking undoc, peoPFQue pfq ' +
	'where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref ' +
	'and pfq.seqnum = (select right ( ''000'' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode) ' +
	'and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document < 0 ' +
	') as a   ' +
	'order by a.teamcode, a.staffcode, a.masterpnr; ' +*/

	'select * from #tmpAdnormalBookingR where  ' +
        'not ( Invoice = 0 and Document = 0)  ' +
	'and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = ''' + @COMPANY_CODE + ''')); ' +
	'DROP TABLE #tmpAdnormalBooking;' +
	'DROP TABLE #tmpAdnormalBookingR;'
	end

PRINT @SQL
EXEC (@SQL)












