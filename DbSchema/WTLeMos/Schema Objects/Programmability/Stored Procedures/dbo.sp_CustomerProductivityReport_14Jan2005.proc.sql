﻿create PROCEDURE sp_CustomerProductivityReport_14Jan2005
@StartDate CHAR(10),
@EndDate CHAR(10),
@cltCode VarChar(10) = '%',
@companycode VARCHAR(10) = 'SG'
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
select @cltCode = '%' + @cltCode + '%'
/* 
--debug
declare @StartDate CHAR(10)
declare @EndDate CHAR(10)
declare @cltCode VarChar(10)
declare @companycode VARCHAR(10)
declare @firstdayofyear CHAR(10) 
declare @enddayofyear CHAR(22)
select @StartDate = '2003-10-01'
select @EndDate = '2003-10-31'
select @cltCode = '%'
select @companycode = 'SG'
select @firstdayofyear = '2003-09-01'
select @enddayofyear = '2003-10-31 23:59:59.99'
-- debug
*/
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
		i.cltcode,
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock) 
                                WHERE          I.BkgRef = INV.Bkgref and i.cltcode = inv.cltcode AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref and i.cltcode = vinv.cltcode AND SubString(vinv.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @StartDate AND @enddayofyear
				)  as [TOTAL Price],
							
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @StartDate AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(NetFare,0)+isnull(TotalTax,0)),0)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @StartDate AND @enddayofyear) 
				+
				(SELECT         isnull(SUM(isnull(mco.costamt,0) + isnull(mco.taxamt,0)),0)
                                FROM              peomco mco (NoLock)
                                WHERE          I.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL
				AND mco.createon between @StartDate AND @enddayofyear) AS [Total Cost]
				,
				(SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref AND SubString(VINV.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @StartDate AND @enddayofyear
				)  as [TOTAL BKGREFPrice]
into #Booksum_by_cltcode
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
and I.companycode = @CompanyCode
and I.cltcode like @cltCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
, I.cltcode
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
		i.cltcode,
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock) 
             WHERE          I.BkgRef = INV.Bkgref and i.cltcode = inv.cltcode AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref and i.cltcode = vinv.cltcode AND SubString(vinv.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				)  as [TOTAL Price],
							
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(NetFare,0)+isnull(TotalTax,0)),0)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @firstdayofyear AND @enddayofyear) 
				+
				(SELECT         isnull(SUM(isnull(mco.costamt,0) + isnull(mco.taxamt,0)),0)
                                FROM              peomco mco (NoLock)
                                WHERE          I.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL
				AND mco.createon between @firstdayofyear AND @enddayofyear) AS [Total Cost]
				,
				(SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref AND SubString(VINV.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				)  as [TOTAL BKGREFPrice]
into #Booksum_by_cltcodeYTD
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
and I.companycode = @CompanyCode
and I.cltCode in (select distinct cltcode from #Booksum_by_cltcode (nolock))
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
, I.cltcode
select b.companycode, b.cltcode
, sum(b.[TOTAL Price]) as [Price]
, sum(case when b.[TOTAL BKGREFPrice] <> 0 then b.[TOTAL Price] * b.[TOTAL Cost]/b.[TOTAL BKGREFPrice] else 0 end) as [Cost]
, sum(b.[TOTAL Price] - (case when b.[TOTAL BKGREFPrice] <> 0 then b.[TOTAL Price] * b.[TOTAL Cost]/b.[TOTAL BKGREFPrice] else 0 end)) as [Margin] 
into #result
from #Booksum_by_cltcode b (nolock)
group by b.companycode, b.cltcode
select y.companycode, y.cltcode
, sum(y.[TOTAL Price]) as [Price]
, sum(case when y.[TOTAL BKGREFPrice] <> 0 then y.[TOTAL Price] * y.[TOTAL Cost]/y.[TOTAL BKGREFPrice] else 0 end) as [Cost]
, sum(y.[TOTAL Price] - (case when y.[TOTAL BKGREFPrice] <> 0 then y.[TOTAL Price] * y.[TOTAL Cost]/y.[TOTAL BKGREFPrice] else 0 end)) as [Margin] 
into #resultYTD
from #Booksum_by_cltcodeYTD y (nolock)
group by y.companycode, y.cltcode
select b.companycode, b.cltcode
, (select ltrim(rtrim(isnull(cltname, cltcode))) from customer where cltcode = b.cltcode) as cltname
, case when b.Price = 0 then '-' else 
cast(b.Price as varchar) end as Price
, case when b.Cost = 0 then '-' else
cast(cast(b.Cost as decimal(20,2)) as varchar) end as Cost
, case when b.Margin = 0 then '-' else 
cast(b.Margin as varchar) end as Margin
, 
case when b.Price <> 0 then cast(cast(b.Margin * 100 / b.Price as decimal(20,2)) as varchar) else '-' end as [Yield(%)]
, case when y.Price = 0 then '-' else 
cast(y.Price as varchar) end as YTDPrice
, case when y.Cost = 0 then '-' else
cast(cast(y.Cost as decimal(20,2)) as varchar) end as YTDCost
, case when y.Margin = 0 then '-' else 
cast(y.Margin as varchar) end as YTDMargin
, 
case when y.Price <> 0 then cast(cast(y.Margin * 100 / y.Price as decimal(20,2)) as varchar) else '-' end as [YTDYield(%)]
/*
, case when y.Price = 0 then '-' else 
cast(y.Price as varchar) end as YTDPrice
, case when y.Cost = 0 then '-' else 
cast(cast(y.Cost as decimal(20,2)) as varchar) end as YTDCost
, case when y.Margin = 0 then '-' else 
cast(y.Margin as decimal(20,2)) end as YTDMargin
, case when y.Price <> 0 then cast(cast(y.Margin * 100 / y.Price as decimal(20,2)) as varchar) else '-' end as [YTDYield(%)]
*/
from #result b (nolock), #resultYTD y (nolock)
where b.companycode = y.companycode and b.cltcode = y.cltcode
order by cast(b.Price as decimal(20,2)) desc
-- debug
-- select * from #Booksum_by_cltcode
-- select * from #Booksum_by_cltcodeYTD
-- select * from #result
-- select * from #resultYTD
-- debug
drop table #Booksum_by_cltcode
drop table #Booksum_by_cltcodeYTD
drop table #result
drop table #resultYTD
END
