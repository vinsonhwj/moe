﻿










/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE         PROCEDURE dbo.sp_SalesReminder
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS

IF @EXCLUDE_NOTE = 'N' 
	begin
	select  
	ltrim(rtrim(m.companycode)) as companycode, 
	ltrim(rtrim(m.teamcode)) as teamcode, 
	ltrim(rtrim(m.staffcode)) as staffcode
	, m.createby, m.bkgref, m.masterpnr
	, m.createon as BookingDate
	, m.departdate as sdeparture
	, convert(char(11), m.departdate, 106) as depart
	, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
	, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
	, isnull(m.invcount, 0) as totalinvCount
	, isnull(m.doccount, 0) as totaldocCount
        into #tmpSalesReminder
	from dbo.peomstr m (nolock) 
	where m.companycode = @COMPANY_CODE AND m.staffcode = @STAFF_CODE 
        and (m.ApproveBy is null or m.ApproveBy = '') and
	m.masterpnr in ( 
		select a.masterpnr
			from dbo.peomstr a (nolock)
	        where a.companycode = m.companycode --and a.staffcode = m.staffcode AND (m.ApproveBy is null or m.ApproveBy = '')
		group by a.masterpnr
		having ( sum(isnull(a.[invcount], 0)) = 0 and sum(isnull(a.[doccount],0)) > 0)
		)
	order by m.teamcode, m.staffcode, m.masterpnr

        select * from #tmpSalesReminder where not ( Invoice = 0 and Document = 0)
        drop table #tmpSalesReminder
	end
ELSE
   --exclude note
   begin
   select 
	ltrim(rtrim(m.companycode)) as companycode, 
	ltrim(rtrim(m.teamcode)) as teamcode, 
	ltrim(rtrim(m.staffcode)) as staffcode
	, m.createby, m.bkgref, m.masterpnr
	, m.createon as BookingDate
	, m.departdate as sdeparture
	, convert(char(11), m.departdate, 106) as depart
	, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
	, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
	, isnull(m.invcount, 0) as totalinvCount
	, isnull(m.doccount, 0) as totaldocCount
	into #tmpSalesReminderEN
	from dbo.peomstr m (nolock) 
	where m.companycode = @COMPANY_CODE AND m.staffcode = @STAFF_CODE 
        and (m.ApproveBy is null or m.ApproveBy = '') and
	m.masterpnr in ( 
		select a.masterpnr
			from dbo.peomstr a (nolock)
	        where a.companycode = m.companycode and a.staffcode = m.staffcode and (m.ApproveBy is null or m.ApproveBy = '')
		group by a.masterpnr
		having ( sum(isnull(a.[invcount], 0)) = 0 and sum(isnull(a.[doccount],0)) > 0)
		)
	order by m.teamcode, m.staffcode, m.masterpnr
	
	
	select  a.* into #tmpSalesReminderENR from 
        (
	SELECT 
	uninv.* 
	from #tmpSalesReminderEN uninv
	where uninv.bkgref not in (select distinct bkgref from peoPFQue where companycode = uninv.companycode)
	Union
	select  
	uninv.* 
	from #tmpSalesReminderEN uninv, peoPFQue pfq
	where uninv.companycode = pfq.companycode and uninv.bkgref = pfq.bkgref
	and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = uninv.bkgref and pfq1.companycode = uninv.companycode)
	and ( uninv.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - uninv.Document < 0
	) as a  
	order by a.teamcode, a.staffcode, a.masterpnr
	
	select * from #tmpSalesReminderENR where not ( Invoice = 0 and Document = 0)

DROP TABLE #tmpSalesReminderEN
DROP TABLE #tmpSalesReminderENR
end










