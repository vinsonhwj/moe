﻿



create PROCEDURE dbo.SalesReport
@StartDate CHAR(10),
@EndDate CHAR(10),
@cltCode VarChar(10) = '%',
@companycode VARCHAR(10) = 'SG'
AS
BEGIN
select @cltCode = '%' + @cltCode + '%'
SELECT         I.Bkgref, i. CLTCODE, I.invnum,
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv INV
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' AND  Inv.cltCode= i.cltcode
				AND INV.CreateOn BETWEEN @StartDate AND @EndDate 
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv VINV
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C'  AND  vInv.cltCode = i.cltcode
				AND VINV.CreateOn BETWEEN @StartDate AND @EndDate  ) 
                          AS [Total Credit Note],
		( Select count(*) 
	               From peoinv inv 
		 Where inv.bkgref=I.bkgRef AND INV.CreateOn BETWEEN @StartDate AND @EndDate 
		AND inv.voidon is null AND Inv.cltCode=i.cltCode And SubString(inv.InvNum, 2, 1) = 'I'
                           ) As costNo,		
		( Select count(*) 
	               From peoinv inv 
		 Where inv.bkgref=I.bkgRef AND INV.CreateOn BETWEEN @StartDate AND @EndDate 
		AND inv.voidon is null AND SubString(inv.InvNum, 2, 1) = 'I'
                           ) As TotalcostNo,		
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V
                                WHERE          I.BkgRef = V.Bkgref  AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @StartDate AND @EndDate)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @EndDate)  AS [Total XO],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @StartDate AND @EndDate) AS [Total TKT],
				(SELECT         SUM(mco.costamt + mco.taxamt)
                                FROM              peomco mco (NoLock)
                                WHERE          I.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL) AS [Total MCO],
                              (SELECT         COUNT(*)
                                FROM              PeoPax P
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND P.CreateOn BETWEEN @StartDate AND @EndDate ) AS [Total Pax]
INTO #BookingSum
FROM              dbo.peoinv I
WHERE	 I.cltCode in (select distinct cltcode from peoinv where cltcode like @cltCode)
AND I.CreateOn BETWEEN @StartDate AND @EndDate
and companyCode=@companycode
GROUP BY I.BKGREF, I.CLTCODE, I.invnum
Select 
ltrim(rtrim(b.companycode)) as companycode
,a.cltcode
,a.bkgref
,ltrim(rtrim(b.teamcode)) as teamcode
,ltrim(rtrim(b.staffCode)) as staffcode
,convert(char(11), b.departdate,113) as departDate, 
CONVERT(CHAR(3), SUM([Total Pax])) [Total Pax],
CAST(
ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) 
 AS Decimal(20,2) ) [Price],
CAST(
(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0)
+ ISNULL(SUM( [Total MCO]), 0))
*
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
Else
	Convert(Decimal,(Case When Max(costNo)=0 Then 1 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
End
 AS Decimal(20,2) 
) [Cost],
CAST(
	(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0 + ISNULL(SUM( [Total MCO]), 0))
   	  *
	  CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
 	  Else
		Convert(Decimal,(Case When Max(costNo)=0 Then 1 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	 End
                )
	 AS Decimal(20,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(
                 ( ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) ) + ISNULL(SUM( [Total MCO]), 0))
	    *
	   CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
	   Else
  		Convert(Decimal,(Case When Max(costNo)=0 Then 1 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	  End
                 = 0 THEN 
	CAST(
		0
	 AS Decimal(20,2) )
WHEN 	ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(20,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)) 
	             *
		   CASE WHEN Max(TotalcostNo)=0 Then
			Convert(Decimal,1)
		   Else
  			Convert(Decimal,(Case When Max(costNo)=0 Then 1 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
		  End
	) /
		(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  * 100
	AS Decimal(20,2) )
END
,
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
Else
	Convert(Decimal,(Case When Max(costNo)=0 Then 1 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
End As proportion
From 
 #BookingSum a , peomstr b
where a.bkgref = b.bkgref
group by a.cltcode,a.bkgref, b.companycode, b.teamcode, b.departdate, b.staffCode 
Order by a.bkgref,a.cltcode,b.teamcode, b.staffCode
DROP TABLE #BookingSum
END




