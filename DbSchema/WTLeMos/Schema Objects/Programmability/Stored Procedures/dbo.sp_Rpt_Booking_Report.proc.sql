﻿CREATE PROCEDURE dbo.sp_Rpt_Booking_Report
(
	@CompanyCode CHAR(2),
	@InvType CHAR(4), --INV for Invoie, REPT for Receipt
	@BookingStatus CHAR(1), --O for Open, C for Close
	@StartDate DATETIME,
	@EndDate DATETIME,
	@StaffList VARCHAR(4000),
	@RunBy VARCHAR(10),
	@IsAbnormal varchar(1)	-- Y/N
)
AS
BEGIN
--	SELECT @EndDate=DateAdd(ss,-1,DateAdd(d,1,@EndDate))

	DECLARE @staff TABLE (staffcode VARCHAR(10));
	
	INSERT INTO @staff (staffcode)
	(
		SELECT * FROM dbo.f_SplitString(@StaffList, '|')
	);

	CREATE TABLE #tmpBkg
	(
		CompanyCode NCHAR(2) NOT NULL,
		MasterPnr NVARCHAR(10) NOT NULL,
		StaffCode VARCHAR(3) NOT NULL,
		InvAmt DECIMAL(12,2) NOT NULL,
		InvTax DECIMAL(12,2) NOT NULL,
		DocAmt DECIMAL(12,2) NOT NULL,
		DocTax DECIMAL(12,2) NOT NULL,
		ReptAmt DECIMAL(12,2) NOT NULL,
		ReptTax DECIMAL(12,2) NOT NULL
		PRIMARY KEY (CompanyCode, MasterPnr)
	)

	CREATE TABLE #tmpBkgResult
	(
		CompanyCode NCHAR(2) NOT NULL,
		MasterPnr NVARCHAR(10) NOT NULL,
		BookDate datetime,
		Owner varchar(10),
		OwnerName nvarchar(70),
		SeqNum INT NOT NULL,
		CardCharge DECIMAL(12,2),
		TIC DECIMAL(12,2),
		Profit DECIMAL(12,2),
		GP DECIMAL(12,2)
		PRIMARY KEY (CompanyCode, MasterPnr, SeqNum)
	)

	CREATE TABLE #tmpBkgInv
	(
		CompanyCode NCHAR(2) NOT NULL,
		MasterPnr NVARCHAR(10) NOT NULL,
		SeqNum INT NOT NULL,
		InvNum CHAR(10) NULL,
		InvDate datetime NULL,
		InvHkdAmt DECIMAL(12,2) NULL,
		ReptHkdAmt DECIMAL(12,2) NULL
	)

	CREATE TABLE #tmpBkgDoc
	(
		CompanyCode NCHAR(2) NOT NULL,
		MasterPnr NVARCHAR(10) NOT NULL,
		SeqNum INT NOT NULL,
		DocNum CHAR(10) NULL,
		DocDate datetime NULL,
		DocHkdAmt DECIMAL(12,2) NULL
	)

	CREATE TABLE #TmpOthCharge
	(
		CompanyCode NCHAR(2) NOT NULL,
		MasterPnr NVARCHAR(10) NOT NULL,
		XoNum CHAR(10) NULL,
		HkdAmt DECIMAL(12,2) NULL,
		SegType VARCHAR(3)
	)

	------ FILTER MASTER BOOKING
	IF EXISTS(SELECT LOGIN FROM ACCESS WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_VIEW' and RightsCode='VIEW')
	BEGIN
		INSERT INTO #tmpBkg (CompanyCode, MasterPnr, StaffCode, InvAmt, InvTax, DocAmt, DocTax, ReptAmt, ReptTax)
		(
			SELECT mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
			FROM MstrPnrTotal mstr (nolock)
				INNER JOIN peomstr m (nolock)
				ON mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
				INNER JOIN @staff s ON s.staffcode = m.staffcode
			WHERE m.CompanyCode = @CompanyCode
				AND m.CreateOn BETWEEN @StartDate AND @EndDate AND mstr.IsLast='Y'
				AND IsNull(mstr.IsClosed,'N')=(Case When @BookingStatus='C' Then 'Y' Else 'N' End)
			Group By mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
		)
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT LOGIN FROM ACCESS WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_TEAM' and RightsCode='VIEW')
		BEGIN
			INSERT INTO #tmpBkg (CompanyCode, MasterPnr, StaffCode, InvAmt, InvTax, DocAmt, DocTax, ReptAmt, ReptTax)
			(
				SELECT mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
				FROM MstrPnrTotal mstr (nolock)
					INNER JOIN peomstr m (nolock)
					ON mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
					INNER JOIN @staff s ON s.staffcode = m.staffcode
				WHERE m.CompanyCode = @CompanyCode
					AND m.CreateOn BETWEEN @StartDate AND @EndDate AND mstr.IsLast='Y'
					AND IsNull(mstr.IsClosed,'N')=(Case When @BookingStatus='C' Then 'Y' Else 'N' End)
					AND m.TeamCode in (Select TeamCode From Login Where CompanyCode=@CompanyCode and Login=@RunBy)
				Group By mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
			)
		END		
		ELSE
		BEGIN
			INSERT INTO #tmpBkg (CompanyCode, MasterPnr, StaffCode, InvAmt, InvTax, DocAmt, DocTax, ReptAmt, ReptTax)
			(
				SELECT mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
				FROM MstrPnrTotal mstr (nolock)
					INNER JOIN peomstr m (nolock)
					ON mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
					INNER JOIN @staff s ON s.staffcode = m.staffcode
				WHERE m.CompanyCode = @CompanyCode
					AND m.CreateOn BETWEEN @StartDate AND @EndDate AND mstr.IsLast='Y'
					AND IsNull(mstr.IsClosed,'N')=(Case When @BookingStatus='C' Then 'Y' Else 'N' End)
					AND m.StaffCode=@RunBy
				Group By mstr.CompanyCode, mstr.Masterpnr, m.StaffCode, mstr.InvAmt, mstr.Invtax, mstr.DocAmt, mstr.DocTax, IsNull(mstr.ReptAmt,0), IsNull(mstr.ReptTax,0)
			)
		END
	END

	------ XO CHARGE (TIC OR CREDIT CARD)
	INSERT INTO #TmpOthCharge (CompanyCode, MasterPnr, XoNum, HkdAmt, SegType)
	Select mstr.CompanyCode, mstr.MasterPnr, x.XoNum, x.HkdAmt, s.SegType
	From #tmpBkg mstr
		Inner Join peomstr m (nolock)
		on mstr.MasterPnr=m.MasterPnr and mstr.CompanyCode=m.CompanyCode
		Inner Join peoXo x (nolock)
		on m.CompanyCode=x.CompanyCode and m.BkgRef=x.BkgRef
		Inner Join peoXoSeg s (nolock)
		on x.CompanyCode=s.CompanyCode and x.XoNum=s.XoNum 
	Where x.VoidOn is Null and s.ServType='OTH' and s.SegType in ('TIC','CVS','CAE','EPS')
	Group By mstr.CompanyCode, mstr.MasterPnr, x.XoNum, x.HkdAmt, s.SegType

	----- XO - COST DOCUMENT
	INSERT INTO #tmpBkgDoc (CompanyCode, MasterPnr, SeqNum, DocNum, DocDate, DocHkdAmt)
	Select mstr.CompanyCode, mstr.MasterPnr, 0, x.XoNum, x.CreateOn, x.HkdAmt
	From #tmpBkg mstr
		Inner Join peomstr m (nolock)
		on mstr.MasterPnr=m.MasterPnr and mstr.CompanyCode=m.CompanyCode
		Inner Join peoXo x (nolock)
		on m.CompanyCode=x.CompanyCode and m.BkgRef=x.BkgRef
		Left Outer Join #TmpOthCharge o
		on x.CompanyCode=o.CompanyCode and x.XoNum=o.XoNum
	Where x.VoidOn is Null and o.XoNum is Null
	Group By mstr.CompanyCode, mstr.MasterPnr, x.XoNum, x.CreateOn, x.HkdAmt

	----- VOUCHER - COST DOCUMENT
	INSERT INTO #tmpBkgDoc (CompanyCode, MasterPnr, SeqNum, DocNum, DocDate, DocHkdAmt)
	Select mstr.CompanyCode, mstr.MasterPnr, 0, v.VchNum, v.CreateOn, v.HkdAmt
	From #tmpBkg mstr
		Inner Join peomstr m (nolock)
		on mstr.MasterPnr=m.MasterPnr and mstr.CompanyCode=m.CompanyCode
		Inner Join peoVch v (nolock)
		on m.CompanyCode=v.CompanyCode and m.BkgRef=v.BkgRef
	Where v.VoidOn is Null
	Group By mstr.CompanyCode, mstr.MasterPnr, v.VchNum, v.CreateOn, v.HkdAmt

	IF (@InvType = 'INV')
	BEGIN
		----- INVOICE - SELL DOCUMENT
		INSERT INTO #tmpBkgInv (CompanyCode, MasterPnr, SeqNum, InvNum, InvDate, InvHkdAmt, ReptHkdAmt)
		Select mstr.CompanyCode, mstr.MasterPnr, 0, v.InvNum, v.CreateOn, v.HkdAmt, IsNull(r.PaidAmt,0)
		From #tmpBkg mstr
			Inner Join peomstr m (nolock)
			on mstr.MasterPnr=m.MasterPnr and mstr.CompanyCode=m.CompanyCode
			Inner Join peoInv v (nolock)
			on m.CompanyCode=v.CompanyCode and m.BkgRef=v.BkgRef
			Left Outer Join (Select CompanyCode, InvNum, Sum(PaidAmt) as PaidAmt From peoInvRept (nolock) Where IsNull(void2jba,'')='' Group By CompanyCode, InvNum) r
			on v.CompanyCode=r.CompanyCode and v.InvNum=r.InvNum
		Where IsNull(v.Void2jba ,'')=''
	END
	ELSE
	BEGIN
		----- RECEIPT - SELL DOCUMENT
		INSERT INTO #tmpBkgInv (CompanyCode, MasterPnr, SeqNum, InvNum, InvDate, InvHkdAmt, ReptHkdAmt)
		Select mstr.CompanyCode, mstr.MasterPnr, 0, r.ReptNum, r.ReptDate, r.PaidAmt, r.PaidAmt
		From #tmpBkg mstr
			Inner Join peomstr m (nolock)
			on mstr.MasterPnr=m.MasterPnr and mstr.CompanyCode=m.CompanyCode
			Inner Join peoInv v (nolock)
			on m.CompanyCode=v.CompanyCode and m.BkgRef=v.BkgRef
			Inner Join peoInvRept r (nolock)
			on v.CompanyCode=r.CompanyCode and v.InvNum=r.InvNum
		Where IsNull(r.Void2jba ,'')=''
	END


	DECLARE @Count int, @NUM Char(10), @MasterPnr NVARCHAR(10), @TmpMasterPnr NVARCHAR(10)
	Select @Count=2, @MasterPnr='', @TmpMasterPnr=''

	--- SET INV/RECEIPT SEGNUM
	DECLARE crsr CURSOR FOR
	SELECT MasterPnr, InvNum FROM #tmpBkgInv ORDER BY MasterPnr, InvDate
	
	OPEN crsr;	
	FETCH NEXT FROM crsr INTO @MasterPnr, @NUM;
	
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @MasterPnr<>@TmpMasterPnr
			Select @Count=2, @TmpMasterPnr=@MasterPnr
		
		Update #TmpBkgInv
		Set SeqNum=@Count
		Where MasterPnr=@MasterPnr and InvNum=@NUM
		
		Select @Count=@Count + 1

		FETCH NEXT FROM crsr INTO @MasterPnr, @NUM;		
	END
	CLOSE crsr;
	DEALLOCATE crsr;

	--- SET XO/VOUCHER SEGNUM
	Select @Count=2, @MasterPnr='', @TmpMasterPnr=''
	
	DECLARE crsr CURSOR FOR
	SELECT MasterPnr, DocNum FROM #tmpBkgDoc ORDER BY MasterPnr, DocDate
	
	OPEN crsr;	
	FETCH NEXT FROM crsr INTO @MasterPnr, @NUM;
	
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @MasterPnr<>@TmpMasterPnr
			Select @Count=2, @TmpMasterPnr=@MasterPnr
		
		Update #TmpBkgDoc
		Set SeqNum=@Count
		Where MasterPnr=@MasterPnr and DocNum=@NUM
		
		Select @Count=@Count + 1

		FETCH NEXT FROM crsr INTO @MasterPnr, @NUM;		
	END
	CLOSE crsr;
	DEALLOCATE crsr;


	-- UPDATE RESULT (Master)
	INSERT INTO #TmpBkgResult (CompanyCode, MasterPnr, SeqNum)
	Select m.CompanyCode, m.MasterPnr, v.SeqNum
	From #TmpBkg m
		Inner Join #TmpBkgInv v
		on m.CompanyCode=v.CompanyCode and m.MasterPnr=v.MasterPnr
	UNION
	Select m.CompanyCode, m.MasterPnr, x.SeqNum
	From #TmpBkg m
		Inner Join #TmpBkgDoc x
		on m.CompanyCode=x.CompanyCode and m.MasterPnr=x.MasterPnr

	INSERT INTO #TmpBkgResult (CompanyCode, MasterPnr, SeqNum)
	Select Distinct CompanyCode, MasterPnr, 1 From #TmpBkg

	Update #TmpBkgResult
	Set BookDate=(Select min(b.CreateOn) From peomstr b Where b.CompanyCode=mstr.CompanyCode and b.MasterPnr=mstr.MasterPnr),
		Owner=mstr.StaffCode,
		OwnerName=IsNull((Select min(StaffName) From peoStaff s Where s.CompanyCode=mstr.CompanyCode and s.StaffCode=mstr.StaffCode),''),
		CardCharge=IsNull((Select Sum(HkdAmt) From #TmpOthCharge c Where c.CompanyCode=mstr.CompanyCode and c.MasterPnr=mstr.MasterPnr and c.SegType in ('CVS','CAE','EPS')),0),
		TIC=IsNull((Select Sum(HkdAmt) From #TmpOthCharge c Where c.CompanyCode=mstr.CompanyCode and c.MasterPnr=mstr.MasterPnr and c.SegType='TIC'),0)
	From #TmpBkgResult t
		Inner Join #TmpBkg mstr
		on t.CompanyCode=mstr.CompanyCode and t.MasterPnr=mstr.MasterPnr 
	Where t.SeqNum=1

	-- UPDATE RESULT (GP)
	Update #TmpBkgResult
	Set Profit=(Case When @InvType='INV' Then (mstr.InvAmt + mstr.InvTax) - (mstr.DocAmt + mstr.DocTax) 
			Else (mstr.ReptAmt + mstr.ReptTax) - (mstr.DocAmt + mstr.DocTax) End),
		GP=(Case When @InvType='INV' and (mstr.InvAmt + mstr.InvTax)=0 Then 0
			When @InvType<>'INV' and (mstr.ReptAmt + mstr.ReptTax)=0 Then 0
			When @InvType='INV' and (mstr.InvAmt + mstr.InvTax)<>0 Then 
				(( (mstr.InvAmt + mstr.InvTax) - (mstr.DocAmt + mstr.DocTax) ) / (mstr.InvAmt + mstr.InvTax) ) * 100
			When @InvType<>'INV' and (mstr.ReptAmt + mstr.ReptTax)<>0 Then 
				(( (mstr.ReptAmt + mstr.ReptTax) - (mstr.DocAmt + mstr.DocTax) ) / (mstr.ReptAmt + mstr.ReptTax) ) * 100
			End)
	From #TmpBkgResult t
		Inner Join #TmpBkg mstr
		on t.CompanyCode=mstr.CompanyCode and t.MasterPnr=mstr.MasterPnr 
	Where SeqNum=1

	---- SELECT RESULT
	Select r.MasterPnr as BkgRef, r.BookDate as BookingDate, IsNull(r.OwnerName,'') as BookingOwner, 
		IsNull(i.InvNum,'') as InvNum, i.InvDate as InvDate, IsNull(i.InvHkdAmt,0) as InvAmt,
		IsNull(d.DocNum,'') as DocNum, d.DocDate as DocDate, IsNull(d.DocHkdAmt,0) as DocAmt,
		IsNull(r.CardCharge,0) as CardCharge, IsNull(r.TIC,0) as TIC, IsNull(r.Profit,0) as Profit, IsNull(r.GP,0) as GP, 
		r.SeqNum, IsNull(i.ReptHkdAmt,0) as ReptAmt, convert(varchar(1),'*') as IsReopen
	From #TmpBkgResult r
		Left Outer join #TmpBkgInv i
		On r.CompanyCode=i.CompanyCode and r.MasterPnr=i.MasterPnr and r.SeqNum=i.SeqNum
		Left Outer Join #TmpBkgDoc d
		On r.CompanyCode=d.CompanyCode and r.MasterPnr=d.MasterPnr and r.SeqNum=d.SeqNum
	Order by r.MasterPnr, r.SeqNum


	DROP TABLE #TmpBkgResult
	DROP TABLE #TmpBkg
	DROP TABLE #TmpBkgDoc
	DROP TABLE #TmpBkgInv
	DROP TABLE #TmpOthCharge
END