﻿CREATE      procedure dbo.sp_InvAirList
@Companycode varchar(2)
, @bkgref varchar(10)
, @SegmentList varchar(500)
, @invtype varchar(30)
as




CREATE TABLE #tmpAir
(
	COMPANYCODE varchar(2) not null default ''
,	BKGREF varchar(10) not null default ''
,	PNR varchar(6) not null default ''
,	SEQ int not null default ''
,	SEGNUM varchar(5) not null default ''
,	DEPARTCITY varchar(10) not null default ''
,	ARRIVALCITY varchar(10) not null default ''
,	DEPARTDATE datetime null
,	DEPARTTIME varchar(5) not null default ''
,	ARRDATE datetime null
,	ARRTIME varchar(5) not null default ''
,	FLIGHT varchar(10) not null default ''
,	CLASS varchar(2) not null default ''
,	REASONCODE nvarchar(4) not null default ''
,	INVRMK11 nvarchar(120) not null default '-'
,	INVRMK22 varchar(120) not null default '-'
,	AIRCURR	varchar(10) not null default ''
,	AIRAMT decimal(16,2) not null default 0
,	SHOWDETAILFEE varchar(1) not null default ''
,	AGETYPE varchar(3) not null default ''
,	AMTFEE decimal(16,2) not null default 0
,	TAXCURR varchar(3) not null default ''
,	TAXFEE decimal(16,2) not null default 0
,	QTY int not null default 0
,	txtamt decimal(16,2) not null default 0
)

-- Split Segnum into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpSegment (Segnum varchar(5) )
While (@SegmentList <> '')
Begin
	If left(@SegmentList,1) = '|'
	Begin
		Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@SegmentList, 0, CharIndex('|', @SegmentList)))
	
		If @TargetCode = ''
			Select @TargetCode=@SegmentList

		INSERT INTO #TmpSegment VALUES (@TargetCode)

		If (Select CharIndex('|', @SegmentList)) > 0
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode) - 1))
		Else
			Select @SegmentList = (Select Right(@SegmentList, Len(@SegmentList) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table

IF NOT EXISTS (SELECT Segnum FROM #TmpSegment)
begin
	If (@invtype = 'NORMAL_INVOICE' or @invtype = 'CREDITCARD_INVOICE' or @invtype = 'DEPOSIT_INVOICE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT, CLASS
		 , REASONCODE, INVRMK11, INVRMK22, AIRCURR, AIRAMT
		 , ARRDATE, ARRTIME, PNR)
		SELECT a.Companycode, a.bkgref, convert(int, a.SEGNUM) as seq, a.SEGNUM, DepartCity
		, ArrivalCity, DepartDATE, DepartTime, Flight, Class
		, IsNull(a.ReasonCode, '') As ReasonCode, isnull(INVRMK1,'-') as INVRMK11, isnull(INVRMK2,'-') as INVRMK22,isnull(dt.curr,'') as aircurr, dt.amt as airamt 
		, a.arrdate, a.arrtime, a.pnr
		FROM peoair a (nolock), peoairdetail dt (nolock) 
		WHERE  a.companycode = dt.companycode 
		AND a.bkgref=dt.bkgref AND a.segnum=dt.segnum 
		AND dt.seqnum = (SELECT min(seqnum) FROM peoairdetail dt2 (nolock) WHERE dt.segnum=dt2.segnum AND dt.bkgref=dt2.bkgref AND dt.companycode = dt2.companycode AND dt.seqtype=dt2.seqtype AND dt2.seqtype='SELL' AND dt2.qty * (dt2.amt+dt2.taxamt+dt2.amtfee) >
= 0 ) 
		AND a.companycode = @Companycode
		AND a.bkgref=@bkgref 
		ORDER BY a.DepartDATE asc, a.SEGNUM asc
	End
	Else If (@invtype = 'CREDITNOTE' or @invtype = 'DEPOSIT_CREDITNOTE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT, CLASS
		 , REASONCODE, INVRMK11, INVRMK22, AIRCURR, AIRAMT
		 , ARRDATE, ARRTIME, PNR)
		SELECT a.Companycode, a.bkgref, convert(int, a.SEGNUM) as seq, a.SEGNUM, DepartCity
		, ArrivalCity, DepartDATE, DepartTime, Flight, Class
		, IsNull(a.ReasonCode, '') As ReasonCode, isnull(INVRMK1,'-') as INVRMK11, isnull(INVRMK2,'-') as INVRMK22,isnull(dt.curr,'') as aircurr, abs(dt.amt) as airamt 
		, a.arrdate, a.arrtime, a.pnr
		FROM peoair a (nolock), peoairdetail dt (nolock) 
		WHERE  a.companycode = dt.companycode 
		AND a.bkgref=dt.bkgref AND a.segnum=dt.segnum 
		AND dt.seqnum = (SELECT min(seqnum) FROM peoairdetail dt2 (nolock) WHERE dt.segnum=dt2.segnum AND dt.bkgref=dt2.bkgref AND dt.companycode = dt2.companycode AND dt.seqtype=dt2.seqtype AND dt2.seqtype='SELL' AND dt2.qty * (dt2.amt+dt2.taxamt+dt2.amtfee) 
< 0 ) 
		AND a.companycode = @Companycode
		AND a.bkgref=@bkgref 
		AND dateDiff("D",a.departdate, getdate())<=7
		ORDER BY a.DepartDATE asc, a.SEGNUM asc
	End
	Else If (@invtype = 'UATP_INVOICE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT, CLASS
		 , REASONCODE, INVRMK11, INVRMK22, AIRCURR, AIRAMT
		 , ARRDATE, ARRTIME, PNR, txtamt)
		SELECT a.Companycode, a.bkgref, convert(int, a.SEGNUM) as seq, a.SEGNUM, DepartCity
		, ArrivalCity, DepartDATE, DepartTime, Flight, Class
		, IsNull(a.ReasonCode, '') As ReasonCode, isnull(INVRMK1,'-') as INVRMK11, isnull(INVRMK2,'-') as INVRMK22,isnull(dt.curr,'') as aircurr, dt.amt as airamt 
		, a.arrdate, a.arrtime, a.pnr, dt.taxamt as txtamt
		FROM peoair a (nolock), peoairdetail dt (nolock) 
		WHERE  a.companycode = dt.companycode 
		AND a.bkgref=dt.bkgref AND a.segnum=dt.segnum 
		AND dt.seqnum = (SELECT min(seqnum) FROM peoairdetail dt2 (nolock) WHERE dt.segnum=dt2.segnum AND dt.bkgref=dt2.bkgref AND dt.companycode = dt2.companycode AND dt.seqtype=dt2.seqtype AND dt2.seqtype='SELL' AND dt2.qty * (dt2.amt+dt2.taxamt+dt2.amtfee) >
= 0 ) 
		AND a.companycode = @Companycode
		AND a.bkgref=@bkgref 
		ORDER BY a.DepartDATE asc, a.SEGNUM asc
	End
end
else
begin
	If (@invtype = 'NORMAL_INVOICE' or @invtype = 'CREDITCARD_INVOICE' or @invtype = 'DEPOSIT_INVOICE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT
		 , CLASS, REASONCODE, INVRMK11, INVRMK22, AGETYPE 
		 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE, QTY
		 , ARRDATE, ARRTIME, PNR)
		SELECT air.Companycode, air.bkgref, convert(int, air.SEGNUM) as seq, air.segnum, air.DEPARTCITY
		, isnull(air.Showdetailfee,'N') as Showdetailfee, air.ARRIVALCITY, air.DEPARTDATE, air.DEPARTTIME, air.FLIGHT
		, air.CLASS, IsNull(air.ReasonCode, '') As ReasonCode, isnull(air.INVRMK1,'-') as INVRMK11, isnull(air.INVRMK2,'-') as INVRMK22, detail.agetype
		, detail.curr, isnull(detail.amtfee,0) as amtfee, isnull(detail.amt,0) as amt, detail.taxcurr, isnull(detail.taxamt,0) as taxamt, isnull(detail.qty,0) as qty
		, air.arrdate, air.arrtime, air.PNR
		FROM peoair air (nolock), peoairdetail detail (nolock), #TmpSegment t 
		WHERE air.Companycode = detail.Companycode 
		AND air.segnum = detail.segnum
		AND air.bkgref = detail.bkgref
		AND air.segnum = t.segnum
		AND air.companycode = @Companycode
		AND air.bkgref=@bkgref 
		AND detail.seqtype = 'SELL'
		ORDER BY air.Companycode, air.Bkgref, air.Segnum, detail.agetype
	End
	Else If (@invtype = 'CREDITNOTE' or @invtype = 'DEPOSIT_CREDITNOTE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT
		 , CLASS, REASONCODE, INVRMK11, INVRMK22, AGETYPE 
		 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE, QTY
		 , ARRDATE, ARRTIME, PNR)
		SELECT air.Companycode, air.bkgref, convert(int, air.SEGNUM) as seq, air.segnum, air.DEPARTCITY
		, isnull(air.Showdetailfee,'N') as Showdetailfee, air.ARRIVALCITY, air.DEPARTDATE, air.DEPARTTIME, air.FLIGHT
		, air.CLASS, IsNull(air.ReasonCode, '') As ReasonCode, isnull(air.INVRMK1,'-') as INVRMK11, isnull(air.INVRMK2,'-') as INVRMK22, detail.agetype
		, detail.curr, abs(isnull(detail.amtfee,0)) as amtfee, abs(isnull(detail.amt,0)) as amt, detail.taxcurr, abs(isnull(detail.taxamt,0)) as taxamt, isnull(detail.qty,0) as qty
		, air.arrdate, air.arrtime, air.PNR
		FROM peoair air (nolock), peoairdetail detail (nolock), #TmpSegment t 
		WHERE air.Companycode = detail.Companycode 
		AND air.segnum = detail.segnum
		AND air.bkgref = detail.bkgref
		AND air.segnum = t.segnum
		AND air.companycode = @Companycode
		AND air.bkgref=@bkgref 
		AND detail.seqtype = 'SELL'
		ORDER BY air.Companycode, air.Bkgref, air.Segnum, detail.agetype
	End
	Else If (@invtype = 'UATP_INVOICE')
	Begin
		INSERT INTO #tmpAir
		(COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
	         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT
		 , CLASS, REASONCODE, INVRMK11, INVRMK22, AGETYPE 
		 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE, QTY
		 , ARRDATE, ARRTIME, PNR, txtamt)
		SELECT air.Companycode, air.bkgref, convert(int, air.SEGNUM) as seq, air.segnum, air.DEPARTCITY
		, isnull(air.Showdetailfee,'N') as Showdetailfee, air.ARRIVALCITY, air.DEPARTDATE, air.DEPARTTIME, air.FLIGHT
		, air.CLASS, IsNull(air.ReasonCode, '') As ReasonCode, isnull(air.INVRMK1,'-') as INVRMK11, isnull(air.INVRMK2,'-') as INVRMK22, detail.agetype
		, detail.curr, isnull(detail.amtfee,0) as amtfee, isnull(detail.amt,0) as amt, detail.taxcurr, isnull(detail.taxamt,0) as taxamt, isnull(detail.qty,0) as qty
		, air.arrdate, air.arrtime, air.PNR, detail.taxamt as txtamt
		FROM peoair air (nolock), peoairdetail detail (nolock), #TmpSegment t 
		WHERE air.Companycode = detail.Companycode 
		AND air.segnum = detail.segnum
		AND air.bkgref = detail.bkgref
		AND air.segnum = t.segnum
		AND air.companycode = @Companycode
		AND air.bkgref=@bkgref 
		AND detail.seqtype = 'SELL'
		ORDER BY air.Companycode, air.Bkgref, air.Segnum, detail.agetype
	End

end

SELECT COMPANYCODE, BKGREF, SEQ, SEGNUM, DEPARTCITY
         , SHOWDETAILFEE, ARRIVALCITY, DEPARTDATE, DEPARTTIME, FLIGHT
	 , CLASS, REASONCODE, INVRMK11, INVRMK22, AGETYPE 
	 , AIRCURR, AMTFEE, AIRAMT, TAXCURR, TAXFEE
	 , QTY, ARRDATE, ARRTIME, PNR, txtamt
FROM #tmpAir 
ORDER BY AGETYPE ASC

DROP TABLE #TmpSegment
DROP TABLE #tmpAir





















