﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[sp_GeteReportSyncList_for_Peoacctup]
	-- Add the parameters for the stored procedure here
@companycode nchar(2)
AS
BEGIN

	begin tran

		select * into #temp_peoacctup from peoacctup where companycode = @companycode and STATUS = '0' and tablename like 'eRpt_%'-- and docnum = 'HC00036183'
		order by createdate desc

		select a.* into #temp_update from
		#temp_peoacctup a inner join #temp_peoacctup b
		on a.TABLENAME = b.TABLENAME
		and a.DOCNUM = b.DOCNUM
		and a.STATUS = b.STATUS
		and a.ACCTUPID < b.ACCTUPID
--		and a.STATUS = '0'

		delete a from
		#temp_peoacctup a inner join #temp_peoacctup b
		on a.TABLENAME = b.TABLENAME
		and a.DOCNUM = b.DOCNUM
		and a.STATUS = b.STATUS
		and a.ACCTUPID < b.ACCTUPID
--		and a.STATUS = '0'

		update peoacctup set status = 1
		from peoacctup
		inner join #temp_update 
		on peoacctup.ACCTUPID = #temp_update.ACCTUPID

	COMMIT TRAN

	select * from  #temp_peoacctup
	--select * from  #temp_update
END







