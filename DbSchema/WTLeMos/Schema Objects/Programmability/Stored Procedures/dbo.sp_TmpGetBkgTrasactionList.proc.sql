﻿






CREATE       procedure dbo.sp_TmpGetBkgTrasactionList
@lstr_Companycode nchar(4),
@lstr_bkgref nvarchar(20),
@ldt_TxnDate datetime,
@lstr_Createby varchar(10),
@operator char(1)
as
if @operator = 'G'
begin


	INSERT INTO #tmpMasterPnrDoc 
	(companycode, txndate, masterpnr, bkgref, type, invamt, invtax, invgst, createby, invcount)
	select companycode, createon, masterpnr, bkgref, 'I' as type
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceAmountexcluetax else TotalInvoiceAmountexcluetax * -1 end) as invamt
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceTax else TotalInvoiceTax * -1 end) as invtax
	, convert(decimal(16,2),case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceGst else TotalInvoiceGst * -1 end) as invgst
	, Createby, 1 as invcount
	from
	(select i.companycode, i.createon, a.masterpnr, a.bkgref, i.invnum
	, isnull( (isnull(sellexcludetax,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0)  as TotalInvoiceAmountexcluetax
	, isnull( (isnull(taxamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceTax
	, isnull( (isnull(gstamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceGst
	, i.Createby
	from peoinv i(nolock), peomstr a (nolock)
	where i.companycode = a.companycode and i.bkgref = a.bkgref
	and a.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and i.createon >= @ldt_TxnDate
	) mstr



	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare, t.totaltax, 0, t.createby, 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon >= @ldt_TxnDate
	--and t.voidon is null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare * -1, t.totaltax * -1, 0, t.voidby , 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon >= @ldt_Txndate
	and t.voidon is not null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon >= @ldt_Txndate
	--and t.voidon is null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon >= @ldt_Txndate
	and t.voidon is not null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon >= @ldt_Txndate
	--and t.voidon is null


	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref,  'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon >= @ldt_Txndate
	and t.voidon is not null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'T' as type
	, t.costamt, t.taxamt, 0, t.createby, 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon >= @ldt_Txndate
	--and t.voidon is null

	INSERT INTO #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref, 'T' as type
	, t.costamt * -1, t.taxamt * -1, 0, t.voidby , 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon >= @ldt_Txndate
	and t.voidon is not null

end
else
begin

	declare @lnum_ttlinvcount integer;
	declare @lnum_ttlinvamt decimal(16,2);
	declare @lnum_ttlinvtax decimal(16,2);
	declare @lnum_ttlinvgst decimal(16,2);
	declare @lnum_ttldoccount integer;
	declare @localcurr nchar(6);
	declare @lnum_ttldocamt decimal(16,2);
	declare @lnum_ttldoctax decimal(16,2);
	declare @lnum_ttldocgst decimal(16,2);
	
	declare @lnum_invamt decimal(16,2);
	declare @lnum_invtax decimal(16,2);
	declare @lnum_invgst decimal(16,2);
	declare @lnum_docamt decimal(16,2);
	declare @lnum_doctax decimal(16,2);
	declare @lnum_docgst decimal(16,2);
	declare @lnum_doccount decimal(16,2);
	declare @lnum_invcount decimal(16,2);


	declare @ldt_ttxndate datetime;
	declare @lstr_tCreateby varchar(10);
	declare @lstr_type char(1);
	declare @lnum_segnum integer;


	create table #tmpMasterBkgDoc
	(
		companycode nchar(4) null default ''
	,	TxnDate datetime null
	,	masterpnr nchar(20) null default ''
	,	bkgref nchar(20) null default ''
	,	type char(1) null default ''
	,	invcount integer null default 0
	,	doccount integer null default 0
	,	InvAmt decimal(16,2) null default 0
	,	InvTax decimal(16,2) null default 0
	,	InvGst decimal(16,2) null default 0
	,	DocAmt decimal(16,2) null default 0
	,	DocTax decimal(16,2) null default 0
	,	DocGst decimal(16,2) null default 0
	,	CreateBy varchar(10) null default ''
	
	)


	select @lnum_ttlinvcount  = 0;
	select @lnum_ttlinvamt  = 0.0;
	select @lnum_ttlinvtax  = 0.0;
	select @lnum_ttlinvgst  = 0.0;
	select @lnum_ttldoccount  = 0;
	select @localcurr = localcurr from company where companycode = @lstr_companycode
	select @lnum_ttldocamt  = 0.0;
	select @lnum_ttldoctax  = 0.0;
	select @lnum_ttldocgst  = 0.0;
	select @lnum_segnum = 0;
	--Insert For New Bkg
	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, createby)
	select companycode, createon, masterpnr, bkgref, 'N' as type, createby 		
	from peomstr (nolock)
	where companycode = @lstr_Companycode and bkgref = @lstr_Bkgref

	INSERT INTO #tmpMasterBkgDoc 
	(companycode, txndate, masterpnr, bkgref, type, invamt, invtax, invgst, createby, invcount)
	select companycode, createon, masterpnr, bkgref, 'I' as type
	, case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceAmountexcluetax else TotalInvoiceAmountexcluetax * -1 end as invamt
	, case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceTax else TotalInvoiceTax * -1 end as invtax
	, case when SubString(InvNum, 2, 1) = 'I' then TotalInvoiceGst else TotalInvoiceGst * -1 end as invgst
	, Createby, 1 as invcount
	from
	(select i.companycode, i.createon, a.masterpnr, a.bkgref, i.invnum
	, isnull( (isnull(sellexcludetax,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0)  as TotalInvoiceAmountexcluetax
	, isnull( (isnull(taxamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceTax
	, isnull( (isnull(gstamt,0)/(case when isnull(sellamt, 0) = 0 then 1 else isnull(sellamt , 1)/(case when hkdamt = 0 then (case when isnull(sellamt, 1) <> 0 then isnull(sellamt, 1) when isnull(sellamt, 1) = 0 then 1 end ) when hkdamt <> 0 then isnull(hkdamt, 1) end ) end )),0) as TotalInvoiceGst
	, i.Createby
	from peoinv i(nolock), peomstr a (nolock)
	where i.companycode = a.companycode and i.bkgref = a.bkgref
	and a.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and i.createon <= @ldt_TxnDate
	) mstr



	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare, t.totaltax, 0, t.createby, 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon <= @ldt_TxnDate
	--and t.voidon is null

	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref, 'T' as type
	, t.netfare * -1, t.totaltax * -1, 0, t.voidby , 1
	FROM peotkt t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon <= @ldt_Txndate
	and t.voidon is not null

	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon <= @ldt_Txndate
	--and t.voidon is null


	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peovch t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon <= @ldt_Txndate
	and t.voidon is not null


	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0)
	, t.createby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon <= @ldt_Txndate
	--and t.voidon is null


	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref,  'V' as type
	, isnull( ( isnull(t.costexcludetax,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.taxamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, isnull( ( isnull(t.gstamt,0)/(case when isnull(t.costamt, 0) = 0 then 1 else isnull(t.costamt , 1)/(case when t.hkdamt = 0 then (case when isnull(t.costamt, 1) <> 0 then isnull(t.costamt, 1) when isnull(t.costamt, 1) = 0 then 1 end ) when t.hkdamt <> 0 then isnull(t.hkdamt, 1) end ) end )),0) * -1
	, t.voidby, 1
	FROM peoxo t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon <= @ldt_Txndate
	and t.voidon is not null

	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.createon, a.masterpnr, a.bkgref, 'M' as type
	, t.costamt, t.taxamt, 0 
	, t.createby, 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.createon <= @ldt_Txndate
	--and t.voidon is null

	INSERT INTO #tmpMasterBkgDoc
	(companycode, txndate, masterpnr, bkgref, type, docamt, doctax, docgst, createby, doccount)
	select t.companycode, t.voidon, a.masterpnr, a.bkgref,  'M' as type
	, t.costamt * -1, t.taxamt * -1, 0 
	, t.voidby, 1
	FROM peomco t (nolock), peomstr a (nolock)
	where a.companycode = a.companycode and t.bkgref = a.bkgref
	and t.companycode = @lstr_companycode and a.bkgref = @lstr_Bkgref
	and t.voidon <= @ldt_Txndate
	and t.voidon is not null


	DECLARE lcur_bkg cursor for
	SELECT txndate, type, invamt, invtax, invgst, docamt, doctax, docgst, invcount, doccount, Createby 
	FROM #tmpMasterBkgDoc (nolock) order by txndate asc
	OPEN lcur_bkg
	FETCH lcur_bkg INTO @ldt_ttxndate,@lstr_type, @lnum_invamt, @lnum_invtax, @lnum_invgst,@lnum_docamt, @lnum_doctax, @lnum_docgst, @lnum_invcount, @lnum_doccount   , @lstr_tCreateby
	while @@fetch_status = 0
	begin


		select @lnum_segnum = @lnum_segnum + 1;

		if @lstr_type = 'I'
		begin
			select @lnum_ttlinvcount  = @lnum_ttlinvcount + @lnum_invcount;
			select @lnum_ttlinvamt  = @lnum_ttlinvamt + @lnum_invamt;
			select @lnum_ttlinvtax  = @lnum_ttlinvtax + @lnum_invtax;
			select @lnum_ttlinvgst  = @lnum_ttlinvgst + @lnum_invgst;
		end
		else 
		begin
			if @lstr_type <> 'N'
			begin
				select @lnum_ttldoccount  = @lnum_ttldoccount + @lnum_doccount;
				select @lnum_ttldocamt  = @lnum_ttldocamt + @lnum_docamt;
				select @lnum_ttldoctax  = @lnum_ttldoctax + @lnum_doctax;
				select @lnum_ttldocgst  = @lnum_ttldocgst + @lnum_docgst;
			end
		end
		
		print '-- Bkgref:' + @lstr_bkgref
		if not exists (select masterpnr from MstrPnrTotal where companycode = @lstr_companycode and masterpnr = @lstr_bkgref and segnum = right('0000' + convert(varchar,@lnum_segnum),4))
		begin
			INSERT INTO MstrPnrTotal VALUES
			(@lstr_companycode, @lstr_bkgref, right('0000' + convert(varchar,@lnum_segnum),4)
	                 ,@lnum_ttlinvcount,@lnum_ttldoccount,@lnum_ttlinvgst,@lnum_ttldocgst
	                 ,@localcurr, @lnum_ttlinvamt, @localcurr, @lnum_ttlinvtax, @localcurr
	                 ,@lnum_ttldocamt, @lnum_ttldoctax, null,@ldt_ttxndate,@lstr_tCreateby,null, null,(@lnum_ttlinvamt+@lnum_invtax)-(@lnum_ttldocamt+@lnum_ttldoctax))
		end	


		FETCH lcur_bkg INTO @ldt_ttxndate,@lstr_type, @lnum_invamt, @lnum_invtax, @lnum_invgst,@lnum_docamt, @lnum_doctax, @lnum_docgst, @lnum_invcount, @lnum_doccount   , @lstr_tCreateby

	end
	close lcur_bkg
	deallocate lcur_bkg

	select @lnum_segnum = @lnum_segnum + 1;

	INSERT INTO MstrPnrTotal VALUES
	(@lstr_companycode, @lstr_bkgref, right('0000' + convert(varchar,@lnum_segnum),4)
         ,0,0,0,0,@localcurr,0 , @localcurr, 0, @localcurr,0, 0, 'Y',@ldt_ttxndate,@lstr_tCreateby,null, null,0)
	

	insert into #tmpMasterPnrDoc
	(companycode, txndate, masterpnr, bkgref, type, invamt, invtax, invgst, docamt, doctax, docgst, createby, doccount, invcount)
	select companycode, '' + @ldt_TxnDate + '', masterpnr, bkgref, 'P',
	sum(invamt), sum(invtax), sum(invgst), sum(docamt), sum(doctax), sum(docgst), '' + @lstr_Createby + '', sum(doccount),sum(invcount)
	from #tmpMasterBkgDoc
	where companycode = @lstr_companycode and bkgref = @lstr_bkgref
	group by companycode, masterpnr, bkgref

	drop table #tmpMasterBkgDoc

end







