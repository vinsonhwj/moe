﻿



CREATE PROCEDURE dbo.Staff_Productivity_Sub_Report_for_Reporting_Service 
@createonfrom char(30),
@createonto char(30),
@user varchar(10),
@companycode varchar(2)
 AS
 select      
         bkgref,     
         isnull(invtype, '') as invtype,      
         invnum,      
         '' as doctype,    
         '' as docnum ,    
         isnull(hkdamt, 0) as price,    
         0 as cost,    
         isnull(voidby, '') as voidby,      
         isnull(voidon, '') as voidon,      
         isnull(void2jba, '') as void2jba ,     
         createon,   
         1 as orderkey,    
         '' as void   
         into #BTDsub
         from peoinv inv  (nolock) where     
         bkgref in (select bkgref from peomstr  [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         createon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'XO' as doctype,    
         xonum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         2 as orderkey,    
         '' as void   
         from peoxo (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         createon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'XO' as doctype,    
         xonum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         2 as orderkey,    
         'void' as void   
         from peoxo where      
         bkgref in (select bkgref from peomstr [index4]  where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         voidon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,    
         '' as invtype,    
         '' as invnum,    
         'vch' as doctype,    
         vchnum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         3 as orderkey,    
         '' as void   
         from peovch (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         createon between @createonfrom and @createonto    
           
           
         union    
           
         select      
         bkgref,    
         '' as invtype,    
         '' as invnum,    
         'vch' as doctype,    
         vchnum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         3 as orderkey,    
         'void' as void   
         from peovch (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         voidon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'tkt' as doctype,    
         ticket,     
         0 as price,    
         isnull(netfare + totaltax, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         4 as orderkey,    
         '' as void   
         from peotkt (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         createon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'tkt' as doctype,    
         ticket,     
         0 as price,    
         isnull(netfare + totaltax, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         4 as orderkey,    
         'void' as void   
         from peotkt (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         voidon between @createonfrom and @createonto    
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'mco' as doctype,    
         mconum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         5 as orderkey,    
         '' as void   
         from peomco (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         createon between @createonfrom and @createonto    
           
           
         union    
           
         select      
         bkgref,     
         '' as invtype,    
         '' as invnum,    
         'mco' as doctype,    
         mconum,     
         0 as price,    
         isnull(hkdamt, 0) as cost,    
         isnull(voidby, '') as voidby,     
         isnull(voidon, '') as voidon,    
         '' as void2jba,    
         createon,   
         5 as orderkey,    
         'void' as void   
         from peomco (nolock) where      
         bkgref in (select bkgref from peomstr [index4]  (nolock) where staffcode = @user and companycode = @companycode) and    
         companycode = @companycode and      
         voidon between @createonfrom and @createonto    
           
         order by bkgref, orderkey, docnum
select bkgref,     
         case when invnum <>'' then  case when invtype in('CCR','UAT','CHQ') Then invtype else 'CSH' end else '' end as invtype,
         invnum,    
         doctype,    
         docnum,     
         case when invnum <> '' then case when substring(invnum,1,2) = 'HC' then -1*price else price end  else price end as price,    
         case when void = 'void' then -1*cost else cost end  as cost,    
         voidby,     
         voidon,    
         void2jba,    
         createon,   
         orderkey,    
         void    from #BTDsub
 order by bkgref, orderkey, docnum




