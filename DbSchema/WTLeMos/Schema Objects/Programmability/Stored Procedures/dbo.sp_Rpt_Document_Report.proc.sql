﻿CREATE PROCEDURE dbo.sp_Rpt_Document_Report
(
	@CompanyCode CHAR(2),
	@StartDate DATETIME,
	@EndDate DATETIME,
	@DocType CHAR(5), --INV/CDN/XO/VCH/REPT
	@DocStatus CHAR(1), -- I-Issue, V-Void, A-All
	@RefCode VARCHAR(10) = '',
	@StaffList VARCHAR(4000),
	@RunBy varchar(10)
)
AS
BEGIN
	SELECT @EndDate=DateAdd(ss,-1,DateAdd(d,1,@EndDate))

	IF (@RefCode IS NULL)
		SELECT @RefCode = '';
	
	IF (@RefCode IS NULL)
		SELECT @RefCode = 'ZZZZZZZZZZ';

	Create Table #TmpBkg
	(	
		CompanyCode varchar(2),
		BkgRef varchar(10)
	)

	IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_VIEW' and RightsCode='VIEW')
	BEGIN
		Insert into #TmpBkg
		Select CompanyCode, BkgRef From peoMstr (nolock) Where CompanyCode=@CompanyCode
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_TEAM' and RightsCode='VIEW')
		BEGIN
			Insert into #TmpBkg
			Select CompanyCode, BkgRef From peoMstr (nolock) Where CompanyCode=@CompanyCode and TeamCode in (Select TeamCode From Login Where CompanyCode=@CompanyCode and Login=@RunBy)
		END
		ELSE
		BEGIN
			Insert into #TmpBkg
			Select CompanyCode, BkgRef From peoMstr (nolock) Where CompanyCode=@CompanyCode and StaffCode=@RunBy
		END
	END




	IF (@DocType = 'INV')
	BEGIN
		SELECT i.InvNum as DocNum, i.CreateOn as DocDate, i.hkdamt, i.SellCurr AS ForeignCurr, i.SellAmt AS ForeignCurrAmt, i.BkgRef, i.CltName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'ISSUE' AS [Status]
		FROM peoinv i (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = i.CompanyCode AND cs.StaffCode = i.CreateBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'INV'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = i.CompanyCode AND p.DocNum = i.InvNum
		WHERE i.CompanyCode = @CompanyCode
		AND i.CreateOn BETWEEN @StartDate AND @EndDate
		AND i.CltCode BETWEEN @RefCode AND @RefCode
		AND i.InvNum LIKE '_I%'
		ORDER BY i.CreateOn, i.InvNum;
	END
	ELSE IF (@DocType = 'CDN')
	BEGIN
		SELECT i.InvNum as DocNum, i.CreateOn as DocDate, i.hkdamt * -1 AS hkdamt, i.SellCurr AS ForeignCurr, (i.SellAmt + i.TaxAmt) * -1 AS ForeignCurrAmt, i.BkgRef, i.CltName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'VOID' AS [Status]
		FROM peoinv i (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = i.CompanyCode AND cs.StaffCode = i.CreateBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'VINV'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = i.CompanyCode AND p.DocNum = i.InvNum
		WHERE i.CompanyCode = @CompanyCode
		AND i.CreateOn BETWEEN @StartDate AND @EndDate
		AND i.CltCode BETWEEN @RefCode AND @RefCode
		AND i.InvNum LIKE '_C%'
		ORDER BY i.CreateOn, i.InvNum;
	END
	ELSE IF (@DocType = 'XO')
	BEGIN
		SELECT x.XoNum as DocNum, x.CreateOn as DocDate, x.hkdamt, x.CostCurr AS ForeignCurr, x.CostAmt + x.TaxAmt AS ForeignCurrAmt, x.BkgRef, x.SuppName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'ISSUE' AS [Status]
		FROM peoxo x (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = x.CompanyCode AND m.BkgRef = x.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = x.CompanyCode AND cs.StaffCode = x.CreateBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'XO'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = x.CompanyCode AND p.DocNum = x.XoNum
		WHERE x.CompanyCode = @CompanyCode
		AND x.CreateOn BETWEEN @StartDate AND @EndDate
		AND x.SuppCode BETWEEN @RefCode AND @RefCode
		
		UNION ALL
		
		SELECT x.XoNum as DocNum, x.VoidOn as DocDate, x.hkdamt * -1 AS hkdamt, x.CostCurr AS ForeignCurr, (x.CostAmt + x.TaxAmt) * -1 AS ForeignCurrAmt, x.BkgRef, x.SuppName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'VOID' AS [Status]
		FROM peoxo x (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = x.CompanyCode AND m.BkgRef = x.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = x.CompanyCode AND cs.StaffCode = x.VoidBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'VXO'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = x.CompanyCode AND p.DocNum = x.XoNum
		WHERE x.CompanyCode = @CompanyCode
		AND x.VoidON IS NOT NULL
		AND x.VoidOn BETWEEN @StartDate AND @EndDate
		AND x.SuppCode BETWEEN @RefCode AND @RefCode
		
		ORDER BY x.CreateOn, x.XoNum;
	END
	ELSE IF (@DocType = 'VCH')
	BEGIN
		SELECT v.VchNum as DocNum, v.CreateOn as DocDate, v.hkdamt, v.CostCurr AS ForeignCurr, v.CostAmt + v.TaxAmt AS ForeignCurrAmt, v.BkgRef, v.SuppName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'ISSUE' AS [Status]
		FROM peovch v (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = v.CompanyCode AND m.BkgRef = v.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = v.CompanyCode AND cs.StaffCode = v.CreateBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'VCH'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = v.CompanyCode AND p.DocNum = v.VchNum
		WHERE v.CompanyCode = @CompanyCode
		AND v.CreateOn BETWEEN @StartDate AND @EndDate
		AND v.SuppCode BETWEEN @RefCode AND @RefCode
		
		UNION ALL
		
		SELECT v.VchNum as DocNum, v.VoidOn as DocDate, v.hkdamt * -1 AS hkdamt, v.CostCurr AS ForeignCurr, (v.CostAmt + v.TaxAmt) * -1 AS ForeignCurrAmt, v.BkgRef, v.SuppName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print], 'VOID' AS [Status]
		FROM peovch v (nolock)
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = v.CompanyCode AND m.BkgRef = v.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = v.CompanyCode AND cs.StaffCode = v.VoidBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'VVCH'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = v.CompanyCode AND p.DocNum = v.VchNum
		WHERE v.CompanyCode = @CompanyCode
		AND v.VoidON IS NOT NULL
		AND v.VoidOn BETWEEN @StartDate AND @EndDate
		AND v.SuppCode BETWEEN @RefCode AND @RefCode
		
		ORDER BY v.CreateOn, v.VchNum;
	END
	ELSE IF (@DocType = 'REPT')
	BEGIN
		SELECT r.ReptNum as DocNum, r.ReptDate as DocDate, CASE WHEN r.ReptType = 'IN' THEN r.PaidAmt ELSE r.PaidAmt * -1 END AS hkdamt, 'HKD' AS ForeignCurr, 
			CASE WHEN r.ReptType = 'IN' THEN r.PaidAmt ELSE r.PaidAmt * -1 END AS ForeignCurrAmt, i.BkgRef, i.CltName AS RefName,
			os.STAFFNAME AS BookingOwner, cs.STAFFNAME AS CreateBy, ISNULL(p.[print], 0) AS [Print],  CASE WHEN r.ReptType = 'IN' THEN 'ISSUE' ELSE 'VOID' END AS [Status]
		FROM peoinvrept r (nolock)
			INNER JOIN peoinv i (nolock) ON i.CompanyCode = r.CompanyCode AND i.InvNum = r.InvNum
			INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
			INNER JOIN #TmpBkg mstr (nolock) ON m.CompanyCode=mstr.CompanyCode and m.BkgRef=mstr.BkgRef
			LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
			LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = r.CompanyCode AND cs.StaffCode = r.CreateBy
			LEFT OUTER JOIN (
				SELECT CompanyCode, DocNum, COUNT(1) AS [print]
				FROM peonyref (nolock)
				WHERE ActionCode = 'REPT'
				GROUP BY CompanyCode, DocNum
			) p ON p.CompanyCode = r.CompanyCode AND p.DocNum = r.ReptNum
		WHERE r.CompanyCode = @CompanyCode
		AND r.ReptDate BETWEEN @StartDate AND @EndDate
		AND i.CltCode BETWEEN @RefCode AND @RefCode
		ORDER BY r.ReptDate, r.ReptNum;
	END

	Drop Table #TmpBkg
			
END