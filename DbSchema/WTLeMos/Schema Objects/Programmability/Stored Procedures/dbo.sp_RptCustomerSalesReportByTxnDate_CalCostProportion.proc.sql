﻿




create     procedure dbo.sp_RptCustomerSalesReportByTxnDate_CalCostProportion
@COMPANY_CODE CHAR(2),
@CAL_TYPE CHAR(1)
as

DECLARE @lstr_TeamCode char(3), @lstr_CltCode char(6), @lstr_CltName nvarchar(100), @lnum_Cost decimal(16,2), @lnum_SellTotal decimal(16,2), @lnum_Sell decimal(16,2),@lnum_Month int, @lstr_bkgref nvarchar(20), @lnum_invamt decimal(16,2)
-- VARIABLE FOR CALCUATE PERCENTAGE
DECLARE @lnum_BkgInvTotal decimal(16,2), @lnum_BkgDocTotal  decimal(16,2), @lnum_CltCount integer


DECLARE lcur_Cost cursor for
select bkgref, cltcode,cost,invamt from #TmpCostAmt (nolock)
open lcur_Cost
fetch lcur_Cost into @lstr_bkgref, @lstr_CltCode,@lnum_Cost,@lnum_invamt
while @@fetch_status = 0
begin

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpCostAmt where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_CostLost cursor for
			SELECT cltcode from #TmpCostAmt (nolock) where bkgref = @lstr_BkgRef
			open lcur_CostLost
			fetch lcur_CostLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				IF @CAL_TYPE = 'Y'
					update #TmpSellAmt set #TmpSellAmt.YTDCost = #TmpSellAmt.YTDCost+@lnum_Cost 
			                where cltcode=@lstr_CltCode
				ELSE
					update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
			                where cltcode=@lstr_CltCode

				fetch lcur_CostLost into @lstr_CltCode
			end
			close lcur_CostLost
			deallocate lcur_CostLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where cltcode=@lstr_CltCode)
			begin
				IF @CAL_TYPE = 'Y'
					update #TmpSellAmt set #TmpSellAmt.YTDCost = #TmpSellAmt.YTDCost+@lnum_Cost 
			                where cltcode=@lstr_CltCode
				ELSE
					update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
			                where cltcode=@lstr_CltCode
			end
			else
			begin
				print '^^' + @lstr_bkgref
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				IF @CAL_TYPE = 'Y'
					insert into #TmpSellAmt values (@lstr_CltCode,@lstr_CltName,0.00,0.00,0.00,@lnum_Cost)
				ELSE
					insert into #TmpSellAmt values (@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost,0.00,0.00)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpCostAmt where bkgref = @lstr_bkgref
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin
			IF @CAL_TYPE = 'Y'
				update #TmpSellAmt set #TmpSellAmt.YTDCost = #TmpSellAmt.YTDCost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			        where cltcode=@lstr_CltCode
			ELSE
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			        where cltcode=@lstr_CltCode
		end
		else
		begin
			IF @CAL_TYPE = 'Y'
				update #TmpSellAmt set #TmpSellAmt.YTDCost = #TmpSellAmt.YTDCost+@lnum_Cost
			        where cltcode=@lstr_CltCode
			ELSE
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
			        where cltcode=@lstr_CltCode
		end

	end

	fetch lcur_Cost into @lstr_bkgref, @lstr_CltCode,@lnum_Cost,@lnum_invamt
end

close lcur_Cost
deallocate lcur_Cost
DELETE FROM #TmpCostAmt





