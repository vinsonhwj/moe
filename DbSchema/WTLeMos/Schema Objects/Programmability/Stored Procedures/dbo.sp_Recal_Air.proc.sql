﻿




CREATE     PROCEDURE sp_Recal_Air
@LS_BKGREF 	VARCHAR(10),
@LS_COMPANYCODE	VARCHAR(2),
@LS_SEGNUM		VARCHAR(5)
AS 
declare	@LDC_AMTCOST		DECIMAL(16,2),
	@LDC_AMTSELL		DECIMAL(16,2),
	@LDC_TAXCOST		DECIMAL(16,2),
	@LDC_TAXSELL		DECIMAL(16,2),
	@LS_AMTCURR		CHAR(3)
	
SELECT @LDC_AMTCOST=0,@LDC_AMTSELL=0,@LDC_TAXCOST=0,@LDC_TAXSELL=0
	BEGIN
		SELECT @LS_AMTCURR=SELLCurr FROM PEOAIR WHERE COMPANYCODE=@LS_COMPANYCODE AND BKGREF=@LS_BKGREF AND  SEGNUM=@LS_SEGNUM
		select @LDC_AMTCOST=sum(isnull(a.qty, 0)*isnull( a.amt, 0) / isnull(b.exrate,1)), @LDC_TAXCOST=sum(isnull(a.qty, 0)*isnull( a.taxamt, 0) / isnull(b.exrate,1))
		From peoairdetail a, exrate b 
		WHERE A.BKGREF=@LS_BKGREF AND A.COMPANYCODE=@LS_COMPANYCODE AND A.SEGNUM=@LS_SEGNUM and
			a.companycode *= b.companycode and 	a.curr *= b.sourcecurr and
			B.TARGETCURR=@LS_AMTCURR and seqtype = 'COST'
		
		select @LDC_AMTSELL=sum(isnull(a.qty, 0)*isnull( a.amt, 0) / isnull(b.exrate,1)), @LDC_TAXSELL=sum(isnull(a.qty, 0)*isnull( a.taxamt, 0) / isnull(b.exrate,1))
		From peoairdetail a, exrate b 
		WHERE A.BKGREF=@LS_BKGREF AND A.COMPANYCODE=@LS_COMPANYCODE AND A.SEGNUM=@LS_SEGNUM and
			a.companycode *= b.companycode and 	a.curr *= b.sourcecurr and
			B.TARGETCURR=@LS_AMTCURR and seqtype = 'SELL'
		--UPDATE
		UPDATE PEOAIR SET TotalSell=ISNULL(@LDC_AMTSELL,0),TotalCost=ISNULL(@LDC_AMTCOST,0),TOTALSTAX=ISNULL(@LDC_TAXSELL ,0)
		,TotalCTax=ISNULL(@LDC_TAXCOST,0)
		WHERE COMPANYCODE=@LS_COMPANYCODE AND BKGREF=@LS_BKGREF AND  SEGNUM=@LS_SEGNUM

	END








