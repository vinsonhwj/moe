﻿




CREATE PROCEDURE [dbo].[SP_Search_Traveller] 
 (
	@CompanyCode NVARCHAR(2),
	@LastName NVARCHAR(20),
	@FirstName NVARCHAR(40),
	@Email NVARCHAR(100),
	@Telephone NVARCHAR(20)
 )
AS
BEGIN
    SELECT  c.PaxLName AS LastName ,
            c.PaxFName AS FirstName ,
            c.ProfileOwner AS ProfileOwner ,
            TeamCode ,
            C.Mobile AS Telephone ,
            c.ProfileID AS ProfileID
    FROM    dbo.peoProfile AS c
            LEFT JOIN dbo.PEOSTAFF AS t 
	ON c.ProfileOwner = t.STAFFCODE AND c.CompanyCode = t.COMPANYCODE
	WHERE c.CompanyCode=@CompanyCode 
	AND c.PaxLName LIKE (CASE WHEN ISNULL(@LastName,'')='' THEN C.PaxLName ELSE @LastName END)+'%'
	AND c.PaxFName LIKE '%'+(CASE WHEN ISNULL(@FirstName,'')='' THEN PaxFName ELSE @FirstName END)+'%'
	AND c.Email=(CASE WHEN ISNULL(@Email,'')='' THEN c.Email ELSE @Email END)
	AND c.Mobile=(CASE WHEN ISNULL(@Telephone,'')='' THEN C.Mobile ELSE @Telephone END)
END




