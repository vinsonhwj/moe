﻿
CREATE procedure dbo.sp_LstCreditLimit
@companycode varchar(2)
, @login varchar(10)
, @bkgref varchar(10)
, @level varchar(10)

as

create table #Tmp_ViewOther 
(
	COMPANYCODE VARCHAR(2)
,	TEAMCODE VARCHAR(10)
)

declare @lstr_pValue nvarchar(50)
declare @lstr_pEnable varchar(1)

insert into #Tmp_ViewOther
select t.companycode, t.teamcode 
from viewother o (nolock), team t (nolock) 
where t.companycode = o.viewcompany
and o.COMPANYCODE = @companycode AND o.login = @login AND o.viewbranch is null

insert into #Tmp_ViewOther
select t.companycode, t.teamcode 
from viewother o (nolock), team t (nolock) 
where t.companycode = o.viewcompany and t.branchcode = o.viewbranch
and o.COMPANYCODE = @companycode AND o.login = @login AND o.viewteam is null

insert into #Tmp_ViewOther
select t.companycode, t.teamcode 
from viewother o (nolock), team t (nolock) 
where t.companycode = o.viewcompany and t.branchcode = o.viewbranch and t.teamcode = o.viewteam
and o.COMPANYCODE = @companycode AND o.login = @login AND o.viewteam is not null


select @lstr_pValue = convert(int,case when enable = 'Y'  then preferencevalue * -1 else -5 end) from com_preference (nolock) where companycode = @companycode and preferenceid = 'CLC_WAIT'


select * from peonyref ref(nolock), #Tmp_ViewOther vo 
where ref.teamcode = vo.teamcode
and ref.companycode = @companycode and ref.actioncode = 'CRD'
and getdate() > createon and createon > dateadd(mi,-5,getdate()) 
and invtypecode <= @level and (selltaxcurr <> 'F' or selltaxcurr is null )  and invtypecode <= @level 
and bkgref = @bkgref 
order by createon desc


drop table #Tmp_ViewOther














