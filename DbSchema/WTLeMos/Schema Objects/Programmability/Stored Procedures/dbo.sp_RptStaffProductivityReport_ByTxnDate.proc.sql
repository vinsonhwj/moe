﻿
CREATE	                       PROCEDURE dbo.sp_RptStaffProductivityReport_ByTxnDate
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@STAFF_CODE   VARCHAR(2000)

AS

DECLARE @lstr_Staffcode varchar(5)
DECLARE @lnum_Cost decimal(16,2)
DECLARE @ldt_DepartDate datetime
DECLARE @lstr_bkgref nvarchar(20)
DECLARE @lnum_PaxTotal integer
DECLARE @FinancalYear datetime
DECLARE @ldt_StartDate datetime
DECLARE @lstr_invtype varchar(10)

-- Split Staffcode into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpStaff (StaffCode nvarchar(10) )
While (@STAFF_CODE <> '')
Begin
	If left(@STAFF_CODE,1) = '|'
	Begin
		Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE)-1))
	End
	Else
	Begin
		If @TargetCode = '' or CharIndex('|', @STAFF_CODE) = 0
			Select @TargetCode=@STAFF_CODE
		else
			Select @TargetCode = (Select substring(@STAFF_CODE, 1, CharIndex('|', @STAFF_CODE)-1))

		INSERT INTO #TmpStaff VALUES (@TargetCode)

		If (Select CharIndex('|', @STAFF_CODE)) > 0
			Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE) - Len(@TargetCode) - 1))
		Else
			Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE) - Len(@TargetCode)))
		
	End
End



-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------

print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT 
a.companycode, a.teamcode, a.bkgref, a.staffcode, b.invnum, b.createon, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), #tmpStaff staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND a.staffcode = staff.staffcode
GROUP BY a.companycode,a.teamcode,  a.bkgref, a.StaffCode, b.invtype, b.invnum, b.createon
ORDER BY 1, 2


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Teamcode varchar(10) not null
,	Staffcode varchar(10) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)
--Ticket
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), #tmpstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.issueon between @FinancalYear AND @END_DATE


insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.voidon, (tkt.netfare+tkt.totaltax) * -1 cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), #tmpstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.voidon between @FinancalYear AND @END_DATE


print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.createon, xo.hkdamt cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.voidon, xo.hkdamt * -1 cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.voidon between @FinancalYear AND @END_DATE

print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.createon, vch.hkdamt cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND vch.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.voidon, vch.hkdamt * -1 cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND vch.voidon between @FinancalYear AND @END_DATE

print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.issueon, mco.costamt + mco.taxamt cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.issueon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.voidon, (mco.costamt + mco.taxamt) * -1 cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), #tmpStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.voidon between @FinancalYear AND @END_DATE

print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	TeamCode nvarchar(50) null default ''
,	StaffCode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)

insert into #TmpSellAmt
--(Companycode, teamcode, staffcode)
(Companycode, teamcode, staffcode)
select distinct s.companycode, s.teamcode, s. staffcode from peostaff s(nolock), #tmpStaff staff
where s.companycode = @COMPANY_CODE and s.staffcode = staff.staffcode

--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode



--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDPrice = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as cost
--(Select CompanyCode, TeamCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as costs
--(Select CompanyCode, TeamCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, count(1) as counts
--(Select CompanyCode, TeamCode, count(1) as counts
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, inv.staffcode, count(1) paxcount
--(SELECT inv.companycode, inv.teamcode, count(1) paxcount
FROM peopax pax(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
AND inv.createon between @START_DATE AND @END_DATE
GROUP BY inv.companycode, inv.Teamcode, inv.StaffCode ) i 
--GROUP BY inv.companycode, inv.Teamcode) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--WHERE i.companycode = t.companycode and i.teamcode = t.teamcode





SELECT staff.teamcode + '/' + rpt.staffcode as code, PaxTotal as [PaxTotal]
, CASE when TktCount <> 0 then TktCount else 0 END as TktCount
, CASE when Price <> 0 then Price else 0 END as Price
, CASE when Dep <> 0 then Dep else 0 END as Dep
, CASE WHEN cost <> 0 THEN cost else 0 END AS Cost
, CASE WHEN price-cost <> 0 THEN price-cost ELSE 0 END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then YTDPrice else 0 END as YTDPrice
, CASE when YTDDep <> 0 then YTDDep else 0 END as YTDDep
, CASE WHEN YTDcost <> 0 THEN YTDcost else 0 END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN YTDprice-YTDcost ELSE 0 END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]
FROM #TmpSellAmt Rpt, Peostaff staff (nolock)
WHERE staff.companycode = rpt.companycode and staff.staffcode = rpt.staffcode
ORDER BY 1, 2

/*
SELECT staff.teamcode + '/' + rpt.staffcode as staff, PaxTotal as [PaxTotal]
, Price, Dep, Cost, YTDPrice, YTDDep,YTDCost
FROM #TmpSellAmt Rpt, Peostaff staff (nolock)
WHERE staff.companycode = rpt.companycode and staff.staffcode = rpt.staffcode
ORDER BY 1, 2
*/


-- Drop Table
drop table #TmpSellAmt;
drop table #tmpStaff;
































