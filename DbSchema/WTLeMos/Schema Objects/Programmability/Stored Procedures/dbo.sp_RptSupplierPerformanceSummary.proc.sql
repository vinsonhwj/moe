﻿


create   PROCEDURE dbo.sp_RptSupplierPerformanceSummary
@COMPANYCODE varchar(2),
@SUPPCODE varchar(8),
@START_DATE varchar(20),
@END_DATE varchar(20),
@EXCLUDE_HOTEL char(1),
@EXCLUDE_VOID char(1)
AS


CREATE TABLE #tmpSuppAmt
(
	SuppCode nchar(8) null default ''
,	SuppName nvarchar(70) null default ''
,	NetAmt decimal(16,2) null default 0
)


--VOUCHER
IF @EXCLUDE_VOID = 'Y'
begin
	IF @EXCLUDE_HOTEL = 'Y'
	begin
		IF LTRIM(RTRIM(@SUPPCODE)) <> ''
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.voidon is null AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
			AND a.suppcode = @SUPPCODE
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 
		end
		else
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.voidon is null AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 


		end
	end
	else
	begin
		IF LTRIM(RTRIM(@SUPPCODE)) <> ''
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.voidon is null AND a.SuppCode = @SUPPCODE
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
		else
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.voidon is null
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
	end
end
else
begin
	IF @EXCLUDE_HOTEL = 'Y'
	begin
		IF LTRIM(RTRIM(@SUPPCODE)) <> ''
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
			AND a.SuppCode = @SUPPCODE
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
		else
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.vchtype <> 'HOTEL' and a.vchtype <> 'HTL'
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
	end
	else
	begin
		IF LTRIM(RTRIM(@SUPPCODE)) <> ''
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			AND a.SuppCode = @SUPPCODE
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
		else
		begin
			INSERT INTO #tmpSuppAmt
			SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
			FROM peovch a (nolock), peovchdetail b (nolock), supplier c (nolock)
			WHERE a.companycode = b.companycode and a.vchnum = b.vchnum
			AND c.companycode = a.companycode and c.suppcode = a.suppcode
			AND a.companycode = 'WM' 
			AND a.createon between @START_DATE and @END_DATE
			GROUP BY  c.suppcode, c.suppname
			having sum(b.hkdamt * b.qty) > 0 

		end
	end
end



--XO
CREATE TABLE #tmpSuppXoAmt
(
	SuppCode nchar(8) null default ''
,	SuppName nvarchar(70) null default ''
,	NetAmt decimal(16,2) null default 0
)

IF @EXCLUDE_VOID = 'Y'
begin
	IF LTRIM(RTRIM(@SUPPCODE)) <> ''
	begin
		INSERT INTO #tmpSuppXoAmt
		SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
		FROM peoxo a (nolock), peoxodetail b (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.xonum = b.xonum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND a.companycode = 'WM' 
		AND a.createon between @START_DATE and @END_DATE
		AND voidon is null
		AND a.SuppCode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname
		having sum(b.hkdamt * b.qty) > 0 

	end
	else
	begin
		INSERT INTO #tmpSuppXoAmt
		SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
		FROM peoxo a (nolock), peoxodetail b (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.xonum = b.xonum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND a.companycode = 'WM' 
		AND a.createon between @START_DATE and @END_DATE
		AND voidon is null
		GROUP BY  c.suppcode, c.suppname
		having sum(b.hkdamt * b.qty) > 0 


	end	

end
else
begin
	IF LTRIM(RTRIM(@SUPPCODE)) <> ''
	begin
		INSERT INTO #tmpSuppXoAmt
		SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
		FROM peoxo a (nolock), peoxodetail b (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.xonum = b.xonum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND a.companycode = 'WM' 
		AND a.createon between @START_DATE and @END_DATE
		AND a.SuppCode = @SUPPCODE
		GROUP BY  c.suppcode, c.suppname
		having sum(b.hkdamt * b.qty) > 0 

	end
	else
	begin
		INSERT INTO #tmpSuppXoAmt
		SELECT c.suppcode, c.suppname, sum(b.hkdamt * b.qty) 
		FROM peoxo a (nolock), peoxodetail b (nolock), supplier c (nolock)
		WHERE a.companycode = b.companycode and a.xonum = b.xonum
		AND c.companycode = a.companycode and c.suppcode = a.suppcode
		AND a.companycode = 'WM' 
		AND a.createon between @START_DATE and @END_DATE
		GROUP BY  c.suppcode, c.suppname
		having sum(b.hkdamt * b.qty) > 0 

	end	
end

DECLARE @lstr_SuppCode nchar(8)
DECLARE @lstr_SuppName nvarchar(70)
DECLARE @lnum_NetAmt decimal(16,2)


DECLARE lcur_Xo CURSOR FOR
SELECT  suppcode, suppname, NetAmt FROM #tmpSuppXoAmt ORDER BY suppcode
OPEN lcur_Xo
FETCH lcur_Xo into @lstr_SuppCode, @lstr_SuppName, @lnum_NetAmt
WHILE @@FETCH_STATUS = 0
begin
	IF exists(select suppcode from #tmpSuppAmt WHERE suppcode = @lstr_SuppCode)
	begin
		UPDATE #tmpSuppAmt SET NetAmt = #tmpSuppAmt.NetAmt + @lnum_NetAmt
		WHERE SuppCode = @lstr_SuppCode;
	end
	else
	begin
		INSERT INTO #tmpSuppAmt
		VALUES
		(@lstr_SuppCode, @lstr_SuppName, @lnum_NetAmt);
	end

	FETCH lcur_Xo into @lstr_SuppCode, @lstr_SuppName, @lnum_NetAmt
end
close lcur_Xo
deallocate lcur_Xo

DROP TABLE #tmpSuppXoAmt

SELECT suppcode, suppname, netamt as hkdamt 
FROM #tmpSuppAmt 
ORDER BY suppcode

DROP TABLE #tmpSuppAmt






