﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Search_Customer]
	-- Add the parameters for the stored procedure here
    @companycode CHAR(2) ,
    @cltcode VARCHAR(10) ,
    @cltname NVARCHAR(100)
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

    -- Insert statements for procedure here
        SELECT  branchcode ,
                teamcode ,
                city ,
                cltcode ,
                cltname ,
                PHONE1 ,
                REPLACE(ISNULL(Addr1, '') + '<br>' + ISNULL(Addr2, '')
                        + '<br>' + ISNULL(Addr3, '') + '<br>' + ISNULL(Addr4,
                                                              '') + '<br>'
                        + ISNULL(Addr5, ''), '<br><br>', '') AS addr1
        FROM    customer
        WHERE   companycode = @companycode
                AND ( CLTCODE = ( CASE WHEN @cltCode <> '' THEN @cltcode
                                       ELSE CLTCODE
                                  END )
                      AND CLTNAME LIKE '%' + @cltname + '%'
                    )
        ORDER BY city ,
                cltname 
    END





