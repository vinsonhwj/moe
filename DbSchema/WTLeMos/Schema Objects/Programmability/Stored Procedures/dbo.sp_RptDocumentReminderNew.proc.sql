﻿
CREATE                 PROCEDURE dbo.sp_RptDocumentReminderNew
@COMPANY_CODE VARCHAR(2),
@TEAM_CODE    NVARCHAR(10),
@STAFF_CODE   NCHAR(20),
@START_DATE   VARCHAR(10),
@END_DATE     VARCHAR(10),
@SEARCH_MODE  CHAR(10),
@EXCLUDE_NOTE CHAR(1)
AS


Declare @SQL nvarchar(2500)

SELECT @SQL = 'select  ' +
'ltrim(rtrim(m.companycode)) as companycode,   ' +
'ltrim(rtrim(m.teamcode)) as teamcode,   ' +
'ltrim(rtrim(m.staffcode)) as staffcode  ' +
', m.createby, m.bkgref, m.masterpnr  ' +
', m.createon as BookingDate  ' +
', m.departdate as sdeparture  ' +
', m.cltode as cltcode ' +
', convert(char(11), m.departdate, 106) as departure  ' +
', Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]  ' +
', Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]  ' +
', isnull(m.invcount, 0) as totalinvCount  ' +
', isnull(m.doccount, 0) as totaldocCount  ' +
'into #tmpDocumentReminder  ' +
'from dbo.peomstr m (nolock), Mstrpnrtotal ttl (nolock) ' +
'where ttl.companycode = m.companycode and ttl.masterpnr = m.masterpnr ' + 
'AND ttl.isLast = ''Y'' ' +
'AND ttl.invcount > 0 and ttl.doccount = 0 ' +
'AND m.companycode = ''' + @COMPANY_CODE + ''''
IF @STAFF_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND m.staffcode = ''' +  @STAFF_CODE + ''''
	END
IF @TEAM_CODE <> 'ALL'
	BEGIN
		SELECT @SQL = @SQL + ' AND m.teamcode = ''' +  @TEAM_CODE + ''''
	END 

IF @START_DATE <> ''
	BEGIN
		SELECT @SQL = @SQL + ' AND m.' + @SEARCH_MODE + '  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ' 23:59:59.999'''
	
	END
	
	
SELECT @SQL = @SQL + ' and (ttl.ApproveBy is null or ttl.ApproveBy = '''') ' +
'order by m.teamcode, m.staffcode, m.masterpnr  '


IF @EXCLUDE_NOTE = 'N' 
	begin
 	SELECT @SQL = @SQL + 
	'select a.*, rtrim(c.cltcode) + ''-'' + c.cltname as cltname, ' +
	'case when invoice = 0 or document = 0 then ''0'' else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'from #tmpDocumentReminder a, customer c (nolock) where  ' +
	'c.companycode = a.companycode and c.cltcode = a.cltcode and ' +
        'not ( Invoice = 0 and Document = 0)  ' +
	'and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = ''' + @COMPANY_CODE + ''')); ' +
	'DROP TABLE #tmpDocumentReminder;'
	end
ELSE
   --exclude note
	begin
	SELECT @SQL = @SQL + 	
	'select  a.*, ' +
	'case when invoice = 0 or document = 0 then ''0'' else convert(char, convert(decimal(20,2) , ( (invoice - document) / invoice ) * 100 ) ) end as losspercent ' +
	'into #tmpDocumentReminderENR from ( ' +
	'SELECT  ' +
	'undoc.*  ' +
	'from #tmpDocumentReminder undoc  ' +
	'where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode) ' +
	'Union ' +
	'select   ' +
	'undoc.*  ' +
	'from #tmpDocumentReminder undoc, peoPFQue pfq ' +
	'where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref ' +
	'and pfq.seqnum = (select right ( ''000'' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode) ' +
	'and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document > 0 ' +
	') as a   ' +
	'order by a.teamcode, a.staffcode, a.masterpnr; ' +

	'select a.*, rtrim(c.cltcode) + ''-'' + c.cltname as cltname ' +
	'from #tmpDocumentReminderENR a, customer c (nolock) ' + 
	'where  c.companycode = a.companycode and c.cltcode = a.cltcode and ' +
        'not ( Invoice = 0 and Document = 0)  ' +
	'and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = ''' + @COMPANY_CODE + ''')); ' +
	'DROP TABLE #tmpDocumentReminder;' +
	'DROP TABLE #tmpDocumentReminderENR;'
	end

PRINT @SQL
exec (@SQL)




























