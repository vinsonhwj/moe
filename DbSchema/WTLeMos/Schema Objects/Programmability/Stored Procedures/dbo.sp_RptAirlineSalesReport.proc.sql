﻿




CREATE                  PROCEDURE dbo.sp_RptAirlineSalesReport
@COMPANY_CODE VARCHAR(2),
@BRANCH_CODE  CHAR(1),
@TEAM_CODE    NVARCHAR(10),
@START_DATE   VARCHAR(10),
@END_DATE     VARCHAR(10),
@SORT         CHAR(20),
@SEARCH_MODE  CHAR(10)

AS

DECLARE @SQL NVARCHAR(4000)
DECLARE @PSEUDOSQL NVARCHAR(4000)
DECLARE @PSEUDOSQL1 NVARCHAR(4000)
DECLARE @PSEUDOSQL2 NVARCHAR(4000)
DECLARE @PSEUDOSQL3 NVARCHAR(4000)
DECLARE @PSEUDOSQL4 NVARCHAR(4000)
DECLARE @PSEUDOSQL5 NVARCHAR(4000)
DECLARE @PSEUDOSQL6 NVARCHAR(4000)
DECLARE @PSEUDOSQL7 NVARCHAR(4000)
DECLARE @PSEUDOSQL8 NVARCHAR(4000)
DECLARE @PSEUDOSQL9 NVARCHAR(4000)
DECLARE @PSEUDOSQL10 NVARCHAR(4000)
DECLARE @WHERESQL NVARCHAR(4000)
DECLARE @SUMSQL NVARCHAR(1000)
DECLARE @pseudo   CHAR(8)
DECLARE @iCount   INT

SELECT @SQL = 'SELECT distinct air.aircode as ''AirCode'', ' +
              'air.airline as ''AirLine'',isnull(staff.branchCode,'''') as ''Branch'',' +
              'isnull(staff.teamcode,'''') as ''Team'' ' 

IF @SEARCH_MODE = 'D'
BEGIN
	DECLARE pseudo_cursor CURSOR FOR
	SELECT DISTINCT pseudo FROM companycrs WHERE companycode = @COMPANY_CODE ORDER BY 1
	
	
	OPEN pseudo_cursor
	FETCH NEXT FROM pseudo_cursor INTO @pseudo
	
	-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
	SELECT @PSEUDOSQL = ''
	SELECT @PSEUDOSQL1 = ''
	SELECT @PSEUDOSQL2 = ''
	SELECT @PSEUDOSQL3 = ''
	SELECT @PSEUDOSQL4 = ''
	SELECT @PSEUDOSQL5 = ''
	SELECT @PSEUDOSQL6 = ''
	SELECT @PSEUDOSQL7 = ''
	SELECT @PSEUDOSQL8 = ''
	SELECT @PSEUDOSQL9 = ''
	SELECT @PSEUDOSQL10 = ''
	
	SELECT @iCount = 1
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
	        SELECT @PSEUDOSQL = ',(	Select isnull(sum(case when (isnull(b.conj,'''') = '''' or b.conj=0) then 1 else b.conj end),0) ' +
	                            'FROM uatpMstr b (nolock), staffCrs c (nolock) ' +
		                    'WHERE b.CompanyCode = c.CompanyCode and b.Pseudo = c.Pseudo ' + 
		                    'AND b.SineInCode = c.Code ' +
	 		            'AND b.CompanyCode=''' + @COMPANY_CODE + ''' '+
				    'AND b.tkttype <> ''V'' ' + 
	                            'AND b.Form = uatp.Form and c.TeamCode = staff.TeamCode ' +
		                    'AND b.Pseudo=''' + rtrim(@pseudo) + ''' ' +
	                            'and b.tktDate between ''' + @START_DATE + ''' and ''' + @END_DATE + ''' '+
	                            ')as ''' + rtrim(@pseudo) + ' Tkt''' +
				    ',(	Select ISNULL(SUM(ISNULL(netAmt,0)),0) ' +
	                            'FROM uatpMstr b (nolock), staffCrs c (nolock) ' +
		                    'WHERE b.CompanyCode = c.CompanyCode and b.Pseudo = c.Pseudo ' + 
		                    'AND b.SineInCode = c.Code ' +
	 		            'AND b.CompanyCode=''' + @COMPANY_CODE + ''' '+
				    'AND b.tkttype <> ''V'' ' + 
	                            'AND b.Form = uatp.Form and c.TeamCode = staff.TeamCode ' +
		                    'AND b.Pseudo=''' + rtrim(@pseudo) + ''' ' +
	                            'and b.tktDate between ''' + @START_DATE + ''' and ''' + @END_DATE + ''' '+
	                            ')as ''' + rtrim(@pseudo) + ' Nett'''
		IF @iCount <= 5
	          BEGIN     
		     SELECT @PSEUDOSQL1 = @PSEUDOSQL1 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 5 AND @iCount <= 10
	          BEGIN     
		     SELECT @PSEUDOSQL2 = @PSEUDOSQL2 + @PSEUDOSQL
	          END
		ELSE
	          IF @iCount > 10 AND @iCount <= 15
	          BEGIN     
		     SELECT @PSEUDOSQL3 = @PSEUDOSQL3 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 15 AND @iCount <= 20
	          BEGIN     
		     SELECT @PSEUDOSQL4 = @PSEUDOSQL4 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 20 AND @iCount <= 25
	          BEGIN     
		     SELECT @PSEUDOSQL5 = @PSEUDOSQL5 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 30 AND @iCount <= 36
	          BEGIN     
		     SELECT @PSEUDOSQL6 = @PSEUDOSQL6 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 36 AND @iCount <= 42
	          BEGIN     
		     SELECT @PSEUDOSQL7 = @PSEUDOSQL7 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 42 AND @iCount <= 48
	          BEGIN     
		     SELECT @PSEUDOSQL8 = @PSEUDOSQL8 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 48 AND @iCount <= 54
	          BEGIN     
		     SELECT @PSEUDOSQL9 = @PSEUDOSQL9 + @PSEUDOSQL
	          END
	        ELSE
	          IF @iCount > 54 AND @iCount <= 60
	          BEGIN     
		     SELECT @PSEUDOSQL10 = @PSEUDOSQL10 + @PSEUDOSQL
	          END
		
		SELECT @iCount = @iCount + 1
	   FETCH NEXT FROM pseudo_cursor INTO @pseudo
	END
CLOSE pseudo_cursor
DEALLOCATE pseudo_cursor

	
SELECT @WHERESQL = ' into #tmp ' +
              'FROM uatpmstr uatp, airline air ,staffcrs staff  ' +
              'WHERE ' +  
	      ' air.airline = uatp.form and  uatp.companycode = ''' + @COMPANY_CODE + ''' and  uatp.tkttype <> ''V'' and   ' +
	      ' uatp.tktseq = (select max(uatp2.tktseq) from uatpmstr uatp2 where uatp.ticket = uatp2.ticket) and   ' +
	      ' uatp.sineincode = staff.code and staff.CompanyCode=uatp.CompanyCode and uatp.pseudo=staff.pseudo '

IF @START_DATE <> ''
	BEGIN
		SELECT @WHERESQL = @WHERESQL + ' AND uatp.tktdate  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ''''
	
	END
	

-- Get total Amt
SELECT @SUMSQL = 'SELECT air.aircode, isnull(staff.teamcode,'''') as teamcode, ' +
                 'SUM(case when (isnull(uatp.conj, '''') = '''' or uatp.conj=0) then 1 else uatp.conj end) AS TotalCount,  ' +
                 'SUM(uatp.netamt) as netAmt into #tmpTotal ' + 
                 'FROM uatpmstr uatp, airline air ,staffcrs staff  ' +
  	         'WHERE air.airline = uatp.form and  uatp.companycode = ''' + @COMPANY_CODE + ''' and  uatp.tkttype <> ''V'' and   ' +
                 'uatp.tktdate  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ''' and ' +
		 'uatp.tktseq = (select max(uatp2.tktseq) from uatpmstr uatp2 where uatp.ticket = uatp2.ticket) and   ' +
                 'uatp.companycode = staff.companycode and uatp.sineincode = staff.code and uatp.pseudo = staff.pseudo ' +
                 'GROUP BY air.aircode,air.airline,staff.teamcode; ' +
                 -- FINAL RESULT
                 'SELECT distinct a.*, t.TotalCount as ''Total Count'', ''0'' as CountP, t.netAmt as ''Total Amount'',  ''0'' as AmtP ' +
                 'FROM #tmp a, #tmpTotal t ' +
                 'WHERE a.Aircode = t.AirCode '  +
                 'and a.team = t.teamcode ' +
	         'and (t.TotalCount <> 0 OR t.netAmt <> 0) '
IF @BRANCH_CODE <> ''
BEGIN
  SELECT @SUMSQL = @SUMSQL + ' AND a.Branch = ''' + @BRANCH_CODE + ''' '
END

IF @TEAM_CODE <> ''
BEGIN
  SELECT @SUMSQL = @SUMSQL + ' AND a.Team = ''' + @TEAM_CODE + ''' '
END

IF @SORT = 'SortBy_amt'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY t.netAmt desc; '
END
ELSE
IF @SORT = 'SortBy_name'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY a.AirCode desc; '
END
ELSE
IF @SORT = 'SortBy_tkt'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY t.TotalCount desc; '
END
ELSE
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY 1,2,3,4; '
	
SELECT @SUMSQL = @SUMSQL + 'DROP TABLE #tmp; ' +
                           'DROP TABLE #tmpTotal;'

PRINT @SUMSQL
exec (@SQL  +  @PSEUDOSQL1  + @PSEUDOSQL2 + @PSEUDOSQL3 + @PSEUDOSQL4 + @PSEUDOSQL5 + @PSEUDOSQL6 + @PSEUDOSQL7 + @PSEUDOSQL8 + @PSEUDOSQL9 + @PSEUDOSQL10 + @WHERESQL + @SUMSQL)
END
ELSE
-- SUMMARY
BEGIN
SELECT @SUMSQL = 'SELECT air.aircode as AirCode, air.airline as ''AirLine'', ' +
                 'SUM(case when (isnull(uatp.conj, '''') = '''' or uatp.conj=0) then 1 else uatp.conj end) AS TotalCount,  ' +
                 'SUM(uatp.netamt) as netAmt into #tmpTotal ' + 
                  'FROM uatpmstr uatp, airline air  ' +
  	         'WHERE air.airline = uatp.form and  uatp.companycode = ''' + @COMPANY_CODE + ''' and  uatp.tkttype <> ''V'' and   ' +
		 'uatp.tktseq = (select max(uatp2.tktseq) from uatpmstr uatp2 where uatp.ticket = uatp2.ticket) and   ' +
                 'uatp.tktdate  between ''' +  @START_DATE + ''' AND  ''' + @END_DATE + ''' ' +
                 'GROUP BY air.aircode,air.airline; ' +
                 -- FINAL RESULT
                 'SELECT distinct t.AirCode as ''Air Code'', t.AirLine as ''Air Line'', ' +
                 ' t.TotalCount as ''Total Count'', ''0'' as CountP, t.netAmt as ''Total Amount'',  ''0'' as AmtP ' +
                 'FROM #tmpTotal t ' +
	         'WHERE (t.TotalCount <> 0 OR t.netAmt <> 0) ' 

IF @SORT = 'SortBy_amt'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY t.netAmt desc;'
END
ELSE
IF @SORT = 'SortBy_name'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY AirCode desc;'
END
ELSE
IF @SORT = 'SortBy_tkt'
BEGIN
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY t.TotalCount desc;'
END
ELSE
  SELECT @SUMSQL = @SUMSQL + 'ORDER BY 1,2;'

SELECT @SUMSQL = @SUMSQL + ' DROP TABLE #tmpTotal;'
PRINT @SUMSQL
exec(@SUMSQL)
END











