﻿create PROCEDURE [dbo].[sp_RetrieveTKT]
@CompanyCode varchar(20),
@Ticket varchar(20),
@TktSeq varchar(20)
AS
BEGIN

select top 1 *
from PEOTKT
where PEOTKT.CompanyCode = @CompanyCode
and PEOTKT.Ticket = @Ticket
and PEOTKT.TktSeq = @TktSeq

select * from PEOTKTDETAIL
where PEOTKTDETAIL.CompanyCode = @CompanyCode
and PEOTKTDETAIL.Ticket = @Ticket
and PEOTKTDETAIL.TktSeq = @TktSeq

END
