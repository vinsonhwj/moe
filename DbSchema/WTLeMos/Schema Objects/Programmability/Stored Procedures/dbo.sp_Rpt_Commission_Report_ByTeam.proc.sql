﻿CREATE PROCEDURE dbo.sp_Rpt_Commission_Report_ByTeam
(
	@CompanyCode CHAR(2),
	@StaffList varchar(4000),
	@HeadCount int,
	@CommMonth varchar(7),	-- Format 2011/12
	@RunBy varchar(10)
)
AS
BEGIN
	Select convert(varchar(3),'') as TeamCode, convert(varchar(3),'') as StaffCode,
		convert(varchar(1),'N') as BookingType, 
		convert(decimal(16,2),0) as Sales, convert(decimal(16,2),0) as Cost, convert(decimal(16,2),0) as CardCharge, convert(decimal(16,2),0) as Profit, 
		convert(decimal(16,2),0) as CommGP, convert(decimal(16,2),0) as BookCount, convert(decimal(16,2),0) as BookAvg, 
		convert(decimal(16,2),0) as CommPaid
END