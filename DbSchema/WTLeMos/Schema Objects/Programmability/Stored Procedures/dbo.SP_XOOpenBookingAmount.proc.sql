﻿

CREATE PROCEDURE [dbo].[SP_XOOpenBookingAmount]
    @CompanyCode CHAR(2) ,
    @OwnerStaff VARCHAR(10) ,
    @OpenXOAmt DECIMAL(16, 2) OUTPUT
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  @OpenXOAmt = ISNULL(SUM(ISNULL(HKDAMT, 0))+SUM(ISNULL(TAXAMT,0)),0)
    FROM    dbo.PEOXO AS XO WITH (NOLOCK)
    INNER JOIN (
                SELECT  M.COMPANYCODE, M.BKGREF
                FROM    dbo.PEOMSTR AS M WITH (NOLOCK)
                INNER JOIN dbo.PEOSTAFF AS S WITH (NOLOCK)
                ON      M.COMPANYCODE = S.COMPANYCODE
                        AND m.STAFFCODE = s.STAFFCODE
                        AND M.CLTODE = S.cltcode
                WHERE   M.COMPANYCODE = @CompanyCode
                        AND ISNULL(M.IsClosed,'N') = 'N'
                        AND M.STAFFCODE = @OwnerStaff
               ) AS BKG
    ON      XO.COMPANYCODE = BKG.COMPANYCODE
            AND XO.BKGREF = BKG.BKGREF
    WHERE   xo.VOIDON IS NULL
END

