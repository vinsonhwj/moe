﻿






create       procedure dbo.sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion
@COMPANY_CODE CHAR(2)
as

DECLARE @lstr_CltCode char(6) 
DECLARE @lstr_bkgref nvarchar(20)
DECLARE @lnum_invamt decimal(16,2)
DECLARE @lnum_Cost decimal(16,2) 
DECLARE @lnum_BkgInvTotal decimal(16,2)

DECLARE @lnum_TCost decimal(16,2)


DECLARE lcur_Cost cursor for
select bkgref, cltcode,cost,invamt,invTotal from #TmpCostAmt (nolock)
open lcur_Cost
fetch lcur_Cost into @lstr_bkgref, @lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_BkgInvTotal
while @@fetch_status = 0
begin

	--CltCode has found
	begin

		if @lnum_BkgInvTotal > 0 
		begin

			SELECT @lnum_TCost = Costamt FROM #TmpSellAmt WHERE BKGREF = @lstr_bkgref
			PRINT '^^Cost: ' + convert(varchar,@lnum_cost) + ' - Costamt:' + convert(varchar,@lnum_TCost) + ' - Inv amt:' + convert(varchar,@lnum_invamt) + ' - InvTotal: ' + convert(varchar,@lnum_BkgInvTotal)
			update #TmpSellAmt set #TmpSellAmt.Costamt = #TmpSellAmt.Costamt+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_Cost)
		        where BKGREF=@lstr_bkgref
		end
		else
		begin
			update #TmpSellAmt set #TmpSellAmt.Costamt = #TmpSellAmt.Costamt+@lnum_Cost
		        where BKGREF=@lstr_bkgref
		end

	end

	fetch lcur_Cost into @lstr_bkgref, @lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_BkgInvTotal
end

close lcur_Cost
deallocate lcur_Cost
DELETE FROM #TmpCostAmt







