﻿
CREATE       procedure [dbo].[sp_CreateEmptyMstrPnr]
@COMPANYCODE nchar(4),
@MASTERPNR varchar(10),
@LOGIN varchar(10)

as
DECLARE @lstr_segnum varchar(4);
DECLARE @localcurr varchar(3);
declare @lstr_Approveby varchar(10);
declare @ldt_ApproveOn datetime;
SELECT @lstr_segnum = '0001'


SELECT @localcurr = localcurr FROM company WHERE companycode = @COMPANYCODE
SELECT @lstr_segnum = right(rtrim('0000' +convert(varchar,isnull(segnum+1,1))),4) 
, @ldt_Approveon = ApproveOn, @lstr_Approveby = Approveby
FROM MstrPnrTotal (nolock)
WHERE COMPANYCODE = @COMPANYCODE AND masterpnr = @MASTERPNR AND islast = 'Y'

UPDATE MstrPnrTotal SET ISLAST = NULL WHERE COMPANYCODE = @COMPANYCODE AND MASTERPNR = @MASTERPNR

INSERT INTO MstrPnrTotal 
(COMPANYCODE,MASTERPNR,SEGNUM,INVCOUNT,DOCCOUNT,TTLINVGST,TTLDOCGST,INVCURR,INVAMT,INVTAXCURR,INVTAX,DOCCURR,DOCAMT,DOCTAX,ISLAST,CREATEON,CREATEBY,APPROVEON,APPROVEBY,PROFIT,ReptCurr,ReptAmt,ReptTaxCurr,ReptTax,IsClosed,ReptCount)
Select @COMPANYCODE, @MASTERPNR, @lstr_segnum,0,0,0,0,@localcurr, 0, @localcurr, 0, @localcurr, 0, 0, 'Y',getdate(),@LOGIN,@ldt_Approveon, @lstr_Approveby,0,@localcurr,0,@localcurr,0,'N',0

delete from abnormalque where companycode = @COMPANYCODE and masterpnr = @MASTERPNR

select top 10 * from MstrPnrTotal
