﻿



-- this tirgger is used to update the whole database for update the peomstr selling, costing, invoice amount, document amount, inv count, doc count, gst amount
CREATE PROCEDURE [dbo].[sp_updatePeomstrSummary] 
	@cc varchar(2) = 'SG'
AS
DECLARE @bkg as varchar(10)
	DECLARE mstr_cursor CURSOR FOR 
	select distinct bkgref from dbo.peomstr where companycode = @cc 
	
	OPEN mstr_cursor
	
	FETCH NEXT FROM mstr_cursor 
	INTO @bkg
	WHILE @@FETCH_STATUS = 0
		BEGIN	
			-- select @bkg, @cc, @emosdb, @sourcesystem
			exec dbo.sp_peomstrsummary_trigger @cc,  @bkg
			FETCH NEXT FROM mstr_cursor 
			INTO @bkg
		END
	
	CLOSE mstr_cursor
	DEALLOCATE mstr_cursor




