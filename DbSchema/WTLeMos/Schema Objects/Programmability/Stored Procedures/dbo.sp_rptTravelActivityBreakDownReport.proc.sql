﻿


CREATE procedure [dbo].[sp_rptTravelActivityBreakDownReport]
 @COMPANYCODE varchar(2)
, @START_DATE varchar(10)
, @END_DATE varchar(25)
, @CLT_CODE varchar(10)
, @PAXNAME nvarchar(100)

as
/*
declare @COMPANYCODE varchar(2)
declare @START_DATE varchar(10)
declare @END_DATE varchar(25)
declare @CLT_CODE varchar(10)
declare @PAXNAME nvarchar(100)


select @COMPANYCODE = 'SG'
select @START_DATE = '2008-01-01'
select @END_DATE = '2008-07-21 23:59:59.000'
select @CLT_CODE = 'HP0068 '
select @PAXNAME = ''
*/
DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_Invnum varchar(10)
DECLARE @lstr_ServType varchar(3)
DECLARE @lstr_Servname varchar(200)
DECLARE @lstr_Servdesc varchar(2000)
DECLARE @lnum_Sellamt decimal(16,2)
DECLARE @lnum_Taxamt decimal(16,2)
DECLARE @lnum_Amtfee decimal(16,2)
DECLARE @lstr_PaxType varchar(3)
DECLARE @lnum_Qty int

Create table #TmpInvoice
(
	CompanyCode	varchar(2) not null
,	Teamcode	varchar(10) not null
,	Staffcode	varchar(10) not null
,	Invnum		varchar(10) not null
,	InvDate		datetime not null
,	Bkgref		varchar(10) not null
,	Mstrinvnum 	varchar(10) not null
,	Cltcode		varchar(10) not null
,	pnr		varchar(6) not null
)


create table #TmpInvoicePax
(
	Companycode 	varchar(2) not null
,	Invnum		varchar(10) not null
,	Paxname		varchar(200) not null
)


if ltrim(rtrim(@PAXNAME)) <> '' 
begin


	if ltrim(rtrim(@CLT_CODE)) = '' 
	begin
		insert into #TmpInvoicePax
		SELECT DISTINCT inv.companycode, inv.invnum, replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		FROM Peoinv inv (nolock), peoinpax pax (nolock) 
		WHERE pax.Companycode = inv.Companycode AND pax.invnum = inv.invnum
		AND replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ') like '%' + @PAXNAME + '%'
		AND inv.Companycode = @COMPANYCODE
		AND inv.Createon between @START_DATE AND @END_DATE
		AND substring(inv.invnum, 2, 1) = 'I'
		AND isnull(inv.void2jba, '') = ''
	end
	else
	begin
		insert into #TmpInvoicePax
		SELECT DISTINCT inv.companycode, inv.invnum, replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		FROM Peoinv inv (nolock), peoinpax pax (nolock) 
		WHERE pax.Companycode = inv.Companycode AND pax.invnum = inv.invnum
		AND replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ') like '%' + @PAXNAME + '%'
		AND inv.Companycode = @COMPANYCODE
		AND inv.Createon between @START_DATE AND @END_DATE
		AND inv.cltcode = @CLT_CODE
		AND substring(inv.invnum, 2, 1) = 'I'
		AND isnull(inv.void2jba, '') = ''
	end


	insert into #TmpInvoice
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.invnum, inv.Createon, inv.bkgref, ISNULL(inv.Mstrinvnum,'')
	       , inv.Cltcode, Mstr.pnr
	FROM Peoinv inv (nolock), Peomstr mstr (nolock), #TmpInvoicePax pax (nolock)
	WHERE inv.Companycode = pax.Companycode AND inv.invnum = pax.invnum
	AND mstr.Companycode = inv.Companycode AND inv.bkgref = mstr.bkgref

end
else
begin
	insert into #TmpInvoice
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.invnum, inv.Createon, inv.bkgref, ISNULL(inv.Mstrinvnum,'')
	       , inv.Cltcode, Mstr.pnr
	FROM Peoinv inv (nolock), Peomstr mstr (nolock)
	WHERE mstr.Companycode = inv.Companycode AND inv.bkgref = mstr.bkgref
	AND inv.Companycode = @COMPANYCODE
	AND inv.Createon between @START_DATE AND @END_DATE
	AND inv.cltcode = @CLT_CODE
	AND substring(inv.invnum, 2, 1) = 'I'
	AND isnull(inv.void2jba, '') = ''
	
	insert into #TmpInvoicePax
	select t.companycode, t.invnum
	, replace(substring(pax.paxname, 1, charindex('/', pax.paxname, charindex('/', pax.paxname) + 1) - 1), '/', ' ')	
	from #TmpInvoice t, peoinpax pax (nolock)
	where pax.companycode = t.companycode and pax.invnum = t.invnum


end

create table #tmpInvoiceTicket
(
	Companycode 	varchar(2) not null
,	Invnum		varchar(10) not null
,	Ticket		varchar(10) not null
,	Tktseq		varchar(3) not null
)

insert into #tmpInvoiceTicket
select t.companycode, m.invnum, t.ticket, t.tktseq
from #TmpInvoice m, peoinvtktref r, peotkt t (nolock)
where r.companycode = m.companycode and r.invnum = m.invnum
and t.companycode = r.companycode and t.ticket = r.ticket and t.voidon is null


insert into #tmpInvoiceTicket
select t.companycode, m.invnum, t.ticket, t.tktseq
from #TmpInvoice m, peoinvtkt r, peotkt t (nolock)
where r.companycode = m.companycode and r.invnum = m.invnum
and t.companycode = r.companycode and t.ticket = r.ticket and t.voidon is null


create table #tmpInvoiceTicketSegments
(
	Companycode	varchar(2) not null
,	Invnum		varchar(10) not null
,	Ticket		varchar(10) not null
,	Tktseq		varchar(3) not null
,	Paxname		varchar(200) not null
,	Flight		varchar(10) not null
,	DepartCity	varchar(3) not null
,	ArrivalCity	varchar(3) not null
,	DepartDate	datetime not null
,	ArrivalDate	datetime not null
,	Class		varchar(1) not null
,	PaxType		varchar(3) not null
,	ServiceFee		decimal(16,2) default 0
)


insert into #tmpInvoiceTicketSegments
select m.companycode, m.invnum, t.ticket, t.tktseq, t.paxname
, d.Flight, d.departcity, d.arrivalcity, d.departdate, d.arrdate, d.class, isnull(t.paxair, '   '),isnull(t.ServiceFee, 0)
from #tmpInvoiceTicket m, peotkt t (nolock), peotktdetail d (nolock)
where t.companycode = m.companycode and t.ticket = m.ticket and t.tktseq = m.tktseq
and d.companycode = t.companycode and d.ticket = t.ticket and d.tktseq = t.tktseq


create table #tmpInvoiceSegments
(
	Companycode	varchar(2) not null
,	Invnum		varchar(10) not null
,	SegType		nvarchar(10) null
,	segnum		varchar(5) not null
,	servtype	varchar(5) not null
,	servname	nvarchar(200) not null
,	servdesc	nvarchar(2000) not null
,	sellamt		decimal(16,2) not null
,	taxamt		decimal(16,2) not null
,	amtfee		decimal(16,2) not null
,	qty		int not null
,	rmnts		int not null
,	PaxType		varchar(3) not null
)


insert into #tmpInvoiceSegments
SELECT rpt.Companycode, rpt.Invnum, seg.Segtype, seg.Segnum, seg.servtype, seg.servname, Servdesc,
isnull(sum(detail.sellamt),0) as sellamt, isnull(sum(detail.taxamt),0) as taxamt, isnull(sum(detail.amtfee),0) as amtfee, 
isnull(sum(detail.qty),0) as qty, isnull(sum(detail.rmnts),0) as rmnts, isnull(detail.detltype,'ADT')
FROM #tmpInvoice rpt
inner join Peoinvseg seg (nolock) on  seg.Companycode = rpt.Companycode AND seg.Invnum = rpt.Invnum
left outer join Peoinvdetail detail (nolock) on detail.Companycode = seg.Companycode AND detail.Invnum = seg.Invnum AND detail.segnum = seg.segnum
--WHERE seg.segtype in ('AIR','HTL','OTH')
GROUP BY rpt.Companycode, rpt.Invnum, seg.SegType, seg.servtype, seg.Segnum, seg.servname, Servdesc, detail.detltype
order by rpt.Companycode, rpt.Invnum, seg.Segtype, seg.Segnum, detail.detltype


create table #tmpInvoiceAirSegment
(
	Companycode	varchar(2) not null
,	Invnum		varchar(10) not null
,	Flight		varchar(10) not null
,	DepartCity	varchar(10) not null
,	ArrivalCity	varchar(10) not null
,	Class 		varchar(1) not null
,	DepartDate	varchar(20) not null
,	ArrivalDate	varchar(20) not null
,	sellamt		decimal(16,2) not null
,	amtfee		decimal(16,2) not null
,	Taxamt 		decimal(16,2) not null
,	PaxType		varchar(3) not null
)

DECLARE lcur_InvoiceAirSegment CURSOR FOR
select Companycode, Invnum, servtype, servname, servdesc, sellamt, taxamt, amtfee, qty, PaxType
from #tmpInvoiceSegments
where segtype = 'AIR'
order by companycode, invnum, servname, segnum





OPEN lcur_InvoiceAirSegment
FETCH lcur_InvoiceAirSegment INTO @lstr_Companycode, @lstr_Invnum, @lstr_ServType, @lstr_Servname, @lstr_Servdesc, @lnum_Sellamt, @lnum_Taxamt, @lnum_amtfee, @lnum_qty, @lstr_PaxType
WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO #tmpInvoiceAirSegment
	values
	(@lstr_Companycode
	, @lstr_Invnum
	, substring(@lstr_Servname, 1, 8)
	, substring(@lstr_Servname, 38, 3)
	, substring(@lstr_Servname, 44, 3)
	, substring(@lstr_Servname, 10, 1)
	, substring(@lstr_Servname, 16, 10)
	, substring(@lstr_Servdesc, 8, 10)
	, @lnum_Sellamt-- * @lnum_qty
	, @lnum_Amtfee-- * @lnum_Qty
	, @lnum_taxamt-- * @lnum_Qty	
	, @lstr_PaxType
	)


	FETCH lcur_InvoiceAirSegment INTO @lstr_Companycode, @lstr_Invnum, @lstr_ServType, @lstr_Servname, @lstr_Servdesc, @lnum_Sellamt, @lnum_Taxamt, @lnum_amtfee, @lnum_qty,@lstr_PaxType

END
CLOSE lcur_InvoiceAirSegment
DEALLOCATE lcur_InvoiceAirSegment


CREATE TABLE #tmpRptResult
(
	  Companycode  	varchar(2) not null default ''
	, Teamcode 	varchar(10) not null default ''
	, Staffcode	varchar(10) not null default ''
	, Createon 	datetime null
	, Mstrinvnum    varchar(10) not null default ''
	, Cltcode       varchar(10) not null default ''
	, Pax           nvarchar(100) not null default ''
	, pnr           varchar(6) not null default ''
	, ticket        varchar(10) not null default ''
	, routing       varchar(100) not null default ''
	, flight        varchar(10) not null default ''
	, class        	varchar(10) not null default ''
	, depart        varchar(20) not null default ''
	, arrival       varchar(20) not null default ''
	, servtype      varchar(50) not null default ''
	, airfare       decimal(16,2) not null default 0
	, taxamt        decimal(16,2) not null default 0
	, amtfee        decimal(16,2) not null default 0
	, othcharge     decimal(16,2) not null default 0
	, invnum        varchar(10) not null default ''
)


--Air (W Ticket)
insert into #tmpRptResult
select t.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode
, t.paxname, i.pnr, t.ticket, t.departcity + '/' + t.arrivalcity, t.flight, t.class, t.departdate, t.arrivaldate, 'AIR'
, a.sellamt,  a.taxamt, t.ServiceFee,0, t.invnum
from #tmpInvoice i, #tmpInvoiceAirSegment a, #tmpInvoiceTicketSegments t
where i.companycode = a.companycode and i.invnum = a.invnum 
and a.companycode = t.companycode and a.invnum = t.invnum
and a.flight = t.flight and a.departcity = t.departcity and a.class = t.class and a.PaxType = t.paxtype

--Air (W/O Ticket)


--Hotel
insert into #tmpRptResult
select i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode
, '', i.pnr, '', left(a.servname,100), 'MISC', '', substring(a.servdesc, 1, 10), substring(a.servdesc, 13, 10), 'HTL'
, 0, 0, 0
, sum(sellamt + taxamt), i.invnum
from #tmpInvoice i, #tmpInvoiceSegments a
where i.companycode = a.companycode and i.invnum = a.invnum and a.servtype = 'HTL'
group by i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode, i.pnr, a.servname, a.servdesc, i.invnum


--AIR (Others)
insert into #tmpRptResult
select i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode
, '', i.pnr, '', left(a.servname,100), 'SVR', '', substring(a.servdesc, 1, 10), substring(a.servdesc, 13, 10), 'HTL'
, 0, 0, 0
, sum((sellamt + taxamt) * qty), i.invnum
from #tmpInvoice i, #tmpInvoiceSegments a
where i.companycode = a.companycode and i.invnum = a.invnum and a.servtype = 'AIR' and a.segtype <> 'AIR'
group by i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode, i.pnr, a.servname, a.servdesc, i.invnum

--Other Segments
insert into #tmpRptResult
select i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode
, '', i.pnr, '', left(a.servdesc,100), 'MISC', '', 'MISC', 'MISC', 'OTH'
, 0, 0, 0, sum((sellamt + taxamt) * qty), i.invnum
from #tmpInvoice i, #tmpInvoiceSegments a
where i.companycode = a.companycode and i.invnum = a.invnum and a.servtype = 'OTH'
group by i.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode, i.pnr, a.servdesc, i.invnum

declare @lstr_Paxname varchar(200)
declare @lstr_Teamcode varchar(10)
declare @lstr_Staffcode varchar(10)
declare @ldt_Createon datetime
declare @lstr_Mstrinvnum varchar(10)
declare @lstr_Cltcode varchar(10)
declare @lstr_pnr varchar(10)


declare lcur_InvoicePax cursor for
select p.companycode, i.teamcode, i.staffcode, i.invdate, i.mstrinvnum, i.cltcode, p.Paxname, i.pnr, p.invnum
from #TmpInvoicePax p, #Tmpinvoice i
where p.companycode = i.companycode and p.invnum = i.invnum
order by p.companycode, p.Invnum, p.paxname

open lcur_InvoicePax
fetch lcur_InvoicePax into @lstr_Companycode, @lstr_Teamcode, @lstr_Staffcode, @ldt_Createon, @lstr_Mstrinvnum, @lstr_Cltcode, @lstr_paxname, @lstr_pnr, @lstr_Invnum
while @@fetch_status = 0
begin

	select @lstr_Paxname = replace(@lstr_Paxname,' ','/')
	if not exists (select invnum from #tmpRptResult where companycode = @lstr_Companycode and invnum = @lstr_Invnum and pax like '' + @lstr_Paxname + '%')
	begin
		if exists(select invnum from #tmpRptResult where companycode = @lstr_Companycode and invnum = @lstr_Invnum and pax = '')
		begin
			update #tmpRptResult
			set pax = @lstr_Paxname
			where companycode = @lstr_Companycode
			and invnum = @lstr_Invnum
			and pax = ''
		end
		else
		begin
			insert into #tmpRptResult
			(Companycode,TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, invnum)
			values
			(@lstr_Companycode, @lstr_Teamcode, @lstr_Staffcode, @ldt_Createon, @lstr_Mstrinvnum, @lstr_Cltcode, @lstr_paxname, @lstr_pnr, @lstr_Invnum)
		end
	end

	

	fetch lcur_InvoicePax into @lstr_Companycode, @lstr_Teamcode, @lstr_Staffcode, @ldt_Createon, @lstr_Mstrinvnum, @lstr_Cltcode, @lstr_paxname, @lstr_pnr, @lstr_Invnum
end
close lcur_InvoicePax
deallocate lcur_InvoicePax





SELECT Companycode, Teamcode, Staffcode, Createon, SERVTYPE, convert(varchar(12),createon,107) as issuedate
, mstrinvnum, cltcode, pax, pnr, Ticket, Routing, Flight, Class
, convert(varchar(12),Depart,107) as Depart, convert(varchar(12),Arrival,107) as Arrival
, CASE WHEN airfare = 0 THEN '-' ELSE CAST(airfare AS VARCHAR(25)) end as airfare
, CASE WHEN taxamt = 0 THEN '-' ELSE CAST(taxamt AS VARCHAR(25)) end as taxes
, CASE WHEN amtfee = 0 THEN '-' ELSE CAST(amtfee AS VARCHAR(25)) end as fee
, CASE WHEN othcharge = 0 THEN '-' ELSE CAST(othcharge AS VARCHAR(25)) end as othcharge
, invnum
FROM #tmpRptResult 
order by companycode, cltcode, invnum, createon, servtype, ticket, convert(datetime, case when isdate(depart) = 1 then depart else null end) asc


drop table #TmpInvoice
drop table #tmpInvoiceTicket
drop table #TmpInvoicePax
drop table #tmpInvoiceTicketSegments
drop table #tmpInvoiceSegments
drop table #tmpInvoiceAirSegment
drop table #tmpRptResult








