﻿












/*
Logic:
Select all booking that satisfy the folling criteria
1. issue AT LEAST ONE Invoice or Credit Note
2. NEVER issue document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE           PROCEDURE dbo.sp_DocumentReminder
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS

IF @EXCLUDE_NOTE = 'N' 
	begin
		select 
		ltrim(rtrim(m.companycode)) as companycode, 
		ltrim(rtrim(m.teamcode)) as teamcode, 
		ltrim(rtrim(m.staffcode)) as staffcode
		, m.createby, m.bkgref, m.masterpnr
		, m.createon as BookingDate
		, m.departdate as sdeparture
		, convert(char(11), m.departdate, 106) as depart
		, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
		, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
		, isnull(m.invcount, 0) as totalinvCount
		, isnull(m.doccount, 0) as totaldocCount
		into #tmpDocumentReminder
		from dbo.peomstr m (nolock)
		where m.companycode = @COMPANY_CODE AND m.staffcode = @STAFF_CODE 
                AND (m.ApproveBy is null or m.ApproveBy = '') AND
		m.masterpnr in ( 
			select a.masterpnr
				from dbo.peomstr a (nolock)
		        where a.companycode = m.companycode --and a.staffcode = m.staffcode AND (m.ApproveBy is null or m.ApproveBy = '')
			group by a.masterpnr
			having ( sum(isnull(a.[invcount], 0)) > 0 and sum(isnull(a.[doccount],0)) = 0)
			)
		order by m.teamcode, m.staffcode, m.masterpnr

		select * from #tmpDocumentReminder where 
	        not ( Invoice = 0 and Document = 0) 
        	and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = @COMPANY_CODE))

		DROP TABLE #tmpDocumentReminder
	end
ELSE
   --exclude note
   begin
		select 
		ltrim(rtrim(m.companycode)) as companycode, 
		ltrim(rtrim(m.teamcode)) as teamcode, 
		ltrim(rtrim(m.staffcode)) as staffcode
		, m.createby, m.bkgref, m.masterpnr
		, m.createon as BookingDate
		, m.departdate as sdeparture
		, convert(char(11), m.departdate, 106) as depart
		, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
		, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
		, isnull(m.invcount, 0) as totalinvCount
		, isnull(m.doccount, 0) as totaldocCount
		into #tmpDocumentReminderEN
		from dbo.peomstr m (nolock)
		where m.companycode = @COMPANY_CODE AND m.staffcode = @STAFF_CODE  
                AND (m.ApproveBy is null or m.ApproveBy = '') AND 
		m.masterpnr in ( 
			select a.masterpnr
				from dbo.peomstr a (nolock)
		        where a.companycode = m.companycode --and a.staffcode = m.staffcode AND (m.ApproveBy is null or m.ApproveBy = '')
			group by a.masterpnr
			having ( sum(isnull(a.[invcount], 0)) > 0 and sum(isnull(a.[doccount],0)) = 0)
			)
		order by m.teamcode, m.staffcode, m.masterpnr
	
	
		select  a.* into #tmpDocumentReminderENR from (
		SELECT 
		undoc.* 
		from #tmpDocumentReminderEN undoc 
		where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode)
		Union
		select  
		undoc.* 
		from #tmpDocumentReminderEN undoc, peoPFQue pfq
		where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref
		and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode)
		and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document < 0
		) as a  
		order by a.teamcode, a.staffcode, a.masterpnr

		select * from #tmpDocumentReminderENR where 
	        not ( Invoice = 0 and Document = 0) 
        	and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = @COMPANY_CODE))

DROP TABLE #tmpDocumentReminderEN
DROP TABLE #tmpDocumentReminderENR
end












