﻿
/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE             PROCEDURE dbo.sp_SalesReminderNew
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS
print convert(varchar,getdate(),109)

select 
m.companycode as companycode, 
m.teamcode as teamcode, 
m.staffcode as staffcode
, m.createby, m.bkgref, m.masterpnr
, m.createon 
, m.departdate, m.invamt, m.invtax, m.docamt, m.doctax, m.invcount, m.doccount
into #tmpDocument
from dbo.peomstr m (nolock), mstrpnrtotal ttl (nolock)
where m.CompanyCode = ttl.Companycode and m.masterpnr = ttl.masterpnr  and ttl.isLast = 'Y'
AND isnull(ttl.invcount,0) = 0 and isnull(ttl.doccount,0) > 0
AND m.companycode = @COMPANY_CODE AND m.staffcode = CONVERT(CHAR(20), @STAFF_CODE) and ttl.ApproveBy is null
order by m.teamcode, m.staffcode, m.masterpnr



SELECT
ltrim(rtrim(companycode)) as companycode, 
ltrim(rtrim(teamcode)) as teamcode, 
ltrim(rtrim(staffcode)) as staffcode
, createby, bkgref, masterpnr
, createon as BookingDate
, departdate as sdeparture
, convert(char(11), departdate, 106) as depart
, Cast(isnull(invamt, .00) + isnull(invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(docamt, .00) + isnull(doctax, .00) as decimal(20,2)) as [Document]
, isnull(invcount, 0) as totalinvCount
, isnull(doccount, 0) as totaldocCount
into #tmpSalesReminder
FROM #tmpDocument
order by teamcode, staffcode, masterpnr


IF @EXCLUDE_NOTE = 'N' 
	begin
        select * from #tmpSalesReminder where not ( Invoice = 0 and Document = 0)
        drop table #tmpSalesReminder
	DROP TABLE #tmpDocument
	end
ELSE
   --exclude note
   begin
	
	select  a.* into #tmpSalesReminderENR from 
        (
	SELECT 
	uninv.* 
	from #tmpSalesReminder uninv
	where uninv.bkgref not in (select distinct bkgref from peoPFQue where companycode = uninv.companycode)
	Union
	select  
	uninv.* 
	from #tmpSalesReminder uninv, peoPFQue pfq
	where uninv.companycode = pfq.companycode and uninv.bkgref = pfq.bkgref
	and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = uninv.bkgref and pfq1.companycode = uninv.companycode)
	and ( uninv.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - uninv.Document < 0
	) as a  
	order by a.teamcode, a.staffcode, a.masterpnr
	
	select * from #tmpSalesReminderENR where not ( Invoice = 0 and Document = 0)

DROP TABLE #tmpSalesReminder
DROP TABLE #tmpSalesReminderENR
DROP TABLE #tmpDocument
end
print convert(varchar,getdate(),109)

