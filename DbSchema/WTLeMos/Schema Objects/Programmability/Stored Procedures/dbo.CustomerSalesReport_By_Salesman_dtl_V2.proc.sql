﻿
---Booking Activity (By Customer)



CREATE PROCEDURE dbo.CustomerSalesReport_By_Salesman_dtl_V2
	@StartDate CHAR(10),
	@EndDate VARCHAR(25),
	@SalesMan VARCHAR(10),
	@cltCode VarChar(10) = '%',
	@CompanyCode varchar(2),
	@RunBy varchar(10)
AS
BEGIN

--exec CustomerSalesReport_By_Salesman_dtl_V2 '2010/09/01','2010/09/30 23:59:59.000','','HS0291','SG','CDS'

	select @SalesMan = replace(@SalesMan,'%','')
	select @cltCode = replace(@cltCode,'','%')
--	SELECT @EndDate = @EndDate + ' 23:59:59.000'
	
	CREATE TABLE #TmpReport
	(
		CompanyCode varchar(2) not null
		, Bkgref    varchar(10) not null
		, StaffCode varchar(10) not null
		, TeamCode varchar(10) not null
		, CltCode varchar(10) not null
		, PaxCount int not null default 0
		, DepartDate datetime null
		, Sell decimal(16,2) not null default 0
		, Cost decimal(16,2) not null default 0
		, Profit decimal(16,2) not null default 0
	)


	CREATE TABLE #TmpDocuments
	(
		CompanyCode varchar(2) not null
		, Bkgref varchar(10) not null
		, StaffCode varchar(10) not null
		, CltCode varchar(10) not null
		, Docnum varchar(10) not null
		, Costamt decimal(16,2) not null default 0
		, TeamCode varchar(10) not null
		, DepartDate datetime null
	)

	print 'Start Retrieve Report at ' + convert(varchar,getdate(),109)	


	Create Table #TmpStaff (Staffcode varchar(10) not null);
	Create Table #TmpTeam (TeamCode varchar(10) not null);
	if @SalesMan = ''
	begin

		insert into #TmpTeam
		select s.TeamCode
		from viewother o (nolock), team s (nolock) 
		where s.companycode = o.viewcompany
		and o.COMPANYCODE = @companycode AND o.login = @RunBy AND o.viewbranch is null

		
		insert into #TmpTeam
		select s.TeamCode
		from viewother o (nolock), team s (nolock) 
		where s.companycode = o.viewcompany AND s.BranchCode = o.ViewBranch
		and o.COMPANYCODE = @companycode AND o.login = @RunBy AND o.viewteam is null


		insert into #TmpTeam
		select s.TeamCode
		from viewother o (nolock), team s (nolock) 
		where s.companycode = o.viewcompany AND s.BranchCode = o.ViewBranch and s.teamcode = o.viewteam
		and o.COMPANYCODE = @companycode AND o.login = @RunBy AND o.viewteam is not null

		insert into #TmpStaff select Staffcode from peostaff s(nolock),#TmpTeam t  where s.CompanyCode = @CompanyCode and s.Teamcode = t.TeamCode
	end
	else
	begin
		insert into #TmpStaff values (@Salesman);
		insert into #TmpTeam select teamcode from peostaff (nolock) where CompanyCode = @CompanyCode and Staffcode = @Salesman
	end
	
	---- Modify at 09Apr2010 for migrate with Customer Sales Report by Salesperson
--	Create Table #TmpClient (CltCode varchar(10) not null);
--	if @cltCode = ''
--	begin
--		INSERT INTO #TmpClient SELECT Cltcode FROM customer c (nolock), #TmpTeam t WHERE CompanyCode = @CompanyCode and c.TeamCode = t.TeamCode
--	end
--	else
--	begin
--		INSERT INTO #TmpClient VALUES (@cltCode);
--	end
	---- End Modify at 09Apr2010 for migrate with Customer Sales Report by Salesperson
	
---------- Modified on 20Jan2010 for show <empty> 
--	INSERT INTO #TmpReport
--	(CompanyCode, Bkgref, Staffcode, Cltcode, Sell, TeamCode, DepartDate)
--	SELECT i.CompanyCode, i.Bkgref, m.Staffcode, m.Cltode
--	, sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end) sell
--	, m.TeamCode, m.DepartDate
--	FROM peoinv i (nolock), peomstr m (nolock), #TmpClient c , #TmpStaff s
--	WHERE m.CompanyCode = i.CompanyCode and m.Bkgref = i.Bkgref
--	AND m.Cltode = c.CltCode AND m.salesperson = s.StaffCode
--	AND m.CompanyCode = @CompanyCode and i.createon between @StartDate and @EndDate
--	GROUP BY i.CompanyCode, i.Bkgref, m.Staffcode, m.Cltode, m.TeamCode, m.DepartDate

	INSERT INTO #TmpReport
	(CompanyCode, Bkgref, Staffcode, Cltcode, Sell, TeamCode, DepartDate)
	SELECT i.CompanyCode, i.Bkgref, m.Staffcode, m.Cltode
	, sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end) sell
	, m.TeamCode, m.DepartDate
	FROM peoinv i (nolock), peomstr m (nolock) --, #TmpClient c 
	WHERE m.CompanyCode = i.CompanyCode and m.Bkgref = i.Bkgref
--	AND m.Cltode = c.CltCode 
	AND i.CltCode In (Select Cltode from peomstr (nolock) Where companyCode=@CompanyCode and (Salesperson in (Select StaffCode From #TmpStaff) or IsNull(Salesperson,'')='') and cltode like @cltcode)	  
	AND m.CompanyCode = @CompanyCode and convert(char(10),i.createon,111) between @StartDate and @EndDate
	GROUP BY i.CompanyCode, i.Bkgref, m.Staffcode, m.Cltode, m.TeamCode, m.DepartDate



	print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)		
	--Ticket
---------- Modified on 20Jan2010 for show <empty> 
--	insert into #TmpDocuments
--	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.ticket, t.netfare + t.totaltax
--	, m.TeamCode, m.DepartDate
--	FROM peomstr m (nolock), peotkt t (nolock), #TmpClient c , #TmpStaff s
--	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode AND m.salesperson = s.StaffCode
--	AND m.CompanyCode = @CompanyCode AND t.IssueOn between @StartDate and @EndDate
--	AND t.voidon is null

	insert into #TmpDocuments
	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.ticket, t.netfare + t.totaltax
	, m.TeamCode, m.DepartDate
	FROM peomstr m (nolock), peotkt t (nolock) --, #TmpClient c 
	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode 
	AND m.Cltode In (Select Cltode from peomstr (nolock) Where companyCode=@CompanyCode and (Salesperson in (Select StaffCode From #TmpStaff) or IsNull(Salesperson,'')='') and cltode like @cltcode)
	AND m.CompanyCode = @CompanyCode AND  convert(char(10),t.CreateOn,111) between @StartDate and @EndDate
	AND t.voidon is null

	print 'Start Retrieve Mco at ' + convert(varchar,getdate(),109)		
	--Mco
---------- Modified on 20Jan2010 for show <empty> 
--	insert into #TmpDocuments
--	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.mconum, t.costamt + t.taxamt
--	, m.TeamCode, m.DepartDate
--	FROM peomstr m (nolock), peomco t (nolock), #TmpClient c , #TmpStaff s
--	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode AND m.salesperson = s.StaffCode
--	AND m.CompanyCode = @CompanyCode AND t.CreateOn between @StartDate and @EndDate
--	AND t.voidon is null

	insert into #TmpDocuments
	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.mconum, t.costamt + t.taxamt
	, m.TeamCode, m.DepartDate
	FROM peomstr m (nolock), peomco t (nolock)--, #TmpClient c 
	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode 
	AND m.Cltode In (Select Cltode from peomstr (nolock) Where companyCode=@CompanyCode and (Salesperson in (Select StaffCode From #TmpStaff) or IsNull(Salesperson,'')='') and cltode like @cltcode)
	AND m.CompanyCode = @CompanyCode AND  convert(char(10),t.CreateOn,111) between @StartDate and @EndDate
	AND t.voidon is null


	print 'Start Retrieve Xo at ' + convert(varchar,getdate(),109)		
	--Xo
---------- Modified on 20Jan2010 for show <empty> 
--	insert into #TmpDocuments
--	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.xonum, t.hkdamt
--	, m.TeamCode, m.DepartDate
--	FROM peomstr m (nolock), peoxo t (nolock), #TmpClient c , #TmpStaff s
--	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode AND m.salesperson = s.StaffCode
--	AND m.CompanyCode = @CompanyCode AND t.CreateOn between @StartDate and @EndDate
--	AND t.voidon is null

	insert into #TmpDocuments
	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.xonum, t.hkdamt
	, m.TeamCode, m.DepartDate
	FROM peomstr m (nolock), peoxo t (nolock)--, #TmpClient c
	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode 
	AND m.Cltode In (Select Cltode from peomstr (nolock) Where companyCode=@CompanyCode and (Salesperson in (Select StaffCode From #TmpStaff) or IsNull(Salesperson,'')='') and cltode like @cltcode)
	AND m.CompanyCode = @CompanyCode AND  convert(char(10),t.CreateOn,111) between @StartDate and @EndDate
	AND t.voidon is null

	print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)		
	--Voucher
---------- Modified on 20Jan2010 for show <empty> 
--	insert into #TmpDocuments
--	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.vchnum, t.hkdamt
--	, m.TeamCode, m.DepartDate
--	FROM peomstr m (nolock), peovch t (nolock), #TmpClient c , #TmpStaff s
--	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode AND m.salesperson = s.StaffCode
--	AND m.CompanyCode = @CompanyCode AND t.CreateOn between @StartDate and @EndDate
--	AND t.voidon is null

	insert into #TmpDocuments
	SELECT m.CompanyCode, m.Bkgref, m.staffcode, m.cltode, t.vchnum, t.hkdamt
	, m.TeamCode, m.DepartDate
	FROM peomstr m (nolock), peovch t (nolock)--, #TmpClient c
	WHERE t.CompanyCode = m.CompanyCode and t.bkgref = m.bkgref
--	AND m.Cltode = c.CltCode 
	AND m.Cltode In (Select Cltode from peomstr (nolock) Where companyCode=@CompanyCode and (Salesperson in (Select StaffCode From #TmpStaff) or IsNull(Salesperson,'')='') and cltode like @cltcode)
	AND m.CompanyCode = @CompanyCode AND  convert(char(10),t.CreateOn,111) between @StartDate and @EndDate
	AND t.voidon is null


	
	--Calcuate the Cost
	UPDATE #TmpReport set cost = d.costamt
	FROM #TmpReport t, 
	(
		SELECT CompanyCode, Bkgref, sum(costamt) as costamt
		FROM #TmpDocuments
		GROUP by CompanyCode, Bkgref
	) d
	WHERE t.CompanyCode = d.CompanyCode and t.Bkgref = d.Bkgref

	INSERT INTO #TmpReport
	(CompanyCode, Bkgref, StaffCode, Cltcode, Cost, TeamCode, DepartDate)
	SELECT d.CompanyCode, d.Bkgref, d.StaffCode, d.CltCode, sum(d.CostAmt), d.TeamCode, d.DepartDate
	FROM #TmpDocuments d 
	LEFT OUTER JOIN #TmpReport t on t.CompanyCode = d.CompanyCode and t.Bkgref = d.Bkgref
	WHERE t.Bkgref is null
	group by d.CompanyCode, d.Bkgref, d.StaffCode, d.CltCode, d.TeamCode, d.DepartDate

	UPDATE #TmpReport SET Profit = Sell - Cost

	--Calculate the Pax Count
	print 'Start Retrieve Pax at ' + convert(varchar,getdate(),109)		
	SELECT t.CompanyCode, t.Bkgref, count(1) as PaxCount
	into #TmpPaxCount
	FROM #TmpReport t, peopax p (nolock)	
	WHERE p.CompanyCode = t.CompanyCode and p.bkgref =t.bkgref
	AND p.voidon is null
--jay
--	AND  convert(char(10),p.CreateOn,111) between @StartDate and @EndDate 	
	GROUP BY t.CompanyCode, t.Bkgref
	
	UPDATE #TmpReport set paxcount = p.PaxCount
	FROM #TmpReport t, #TmpPaxCount p
 	WHERE t.CompanyCode = p.CompanyCode and t.Bkgref = p.Bkgref

	SELECT t.Cltcode, t.Bkgref, t.TeamCode, t.StaffCode, convert(char(11), t.departdate,113) as departDate, 
	t.Paxcount as [Total Pax], Sell as Price, Cost as Cost, Profit as [Margin]
	, CAST(case when sell = 0 then -100 else (profit/sell) * 100 end AS decimal(12,2)) as [Yield(%)]
	FROM #TmpReport t

	print 'End of Report : ' + convert(varchar,getdate(),109)		


	DROP TABLE #TmpPaxCount
	DROP TABLE #TmpDocuments
	DROP TABLE #TmpReport
	DROP TABLE #TmpStaff
	DROP TABLE #TmpTeam
--	DROP TABLE #TmpClient

END

