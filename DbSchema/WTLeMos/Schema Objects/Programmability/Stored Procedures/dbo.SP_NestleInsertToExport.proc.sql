﻿


CREATE  PROCEDURE dbo.SP_NestleInsertToExport
@CompanyCode varchar(2),
@StartDate datetime,
@EndDate datetime,
@User nchar(10),
@customer nvarchar(8)
AS

declare @NesID varchar(10)
declare @SeqNo decimal
declare @FileCode nvarchar(1)
declare @Sender nvarchar(8)
declare @FileVersion nvarchar(8)
declare @CreationDate nvarchar(6)
declare @CreationTime nvarchar(6)
declare @RecordNum int
declare @CltCode nvarchar(8)
declare @IsoCountry nvarchar(3)
declare @CltLocal nvarchar(5)
declare @TravellerName nvarchar(30)
declare @StaffID nvarchar(25)
declare @DepartDate nvarchar(6)
declare @RecordType nvarchar(1)
declare @GdsCode nvarchar(3)
declare @InvNum nvarchar(10)
declare @InvDate nvarchar(6)
declare @PayMethod nvarchar(2)
declare @BranchCode nvarchar(8)
declare @InitInvNum nvarchar(9)
declare @TktNum nvarchar(15)
declare @TktConj nvarchar(2)
declare @RecordSubType nvarchar(1)
declare @Passenger nvarchar(30)
declare @Airline nvarchar(3)
declare @Class nvarchar(2)
declare @PaidFare nvarchar(11)
declare @PrecedingSign nvarchar(1)
declare @IsoCurrency nvarchar(4)
declare @BkgDate nvarchar(6)
declare @TktDate nvarchar(6)
declare @ContractNo nvarchar(15)
declare @IataAgency nvarchar(8)
declare @OrgTktNum nvarchar(15)
declare @SegDate nvarchar(5)
declare @ArrivalDateInd nvarchar(1)
declare @DepartCity nvarchar(5)
declare @ArrivalCity nvarchar(5)
declare @Air nvarchar(3)
declare @Flight nvarchar(5)
declare @Cls nvarchar(2)
declare @DepartTime nvarchar(4)
declare @ArrivalTime nvarchar(4)
declare @FareBasic nvarchar(8)
declare @SegDate1 nvarchar(5)
declare @ArrivalDateInd1 nvarchar(1)
declare @DepartCity1 nvarchar(5)
declare @ArrivalCity1 nvarchar(5)
declare @Air1 nvarchar(3)
declare @Flight1 nvarchar(5)
declare @Cls1 nvarchar(2)
declare @DepartTime1 nvarchar(4)
declare @ArrivalTime1 nvarchar(4)
declare @FareBasic1 nvarchar(8)
declare @SegDate2 nvarchar(5)
declare @ArrivalDateInd2 nvarchar(1)
declare @DepartCity2 nvarchar(5)
declare @ArrivalCity2 nvarchar(5)
declare @Air2 nvarchar(3)
declare @Flight2 nvarchar(5)
declare @Cls2 nvarchar(2)
declare @DepartTime2 nvarchar(4)
declare @ArrivalTime2 nvarchar(4)
declare @FareBasic2 nvarchar(8)
declare @SegDate3 nvarchar(5)
declare @ArrivalDateInd3 nvarchar(1)
declare @DepartCity3 nvarchar(5)
declare @ArrivalCity3 nvarchar(5)
declare @Air3 nvarchar(3)
declare @Flight3 nvarchar(5)
declare @Cls3 nvarchar(2)
declare @DepartTime3 nvarchar(4)
declare @ArrivalTime3 nvarchar(4)
declare @FareBasic3 nvarchar(8)
declare @SegDate4 nvarchar(5)
declare @ArrivalDateInd4 nvarchar(1)
declare @DepartCity4 nvarchar(5)
declare @ArrivalCity4 nvarchar(5)
declare @Air4 nvarchar(3)
declare @Flight4 nvarchar(5)
declare @Cls4 nvarchar(2)
declare @DepartTime4 nvarchar(4)
declare @ArrivalTime4 nvarchar(4)
declare @FareBasic4 nvarchar(8)
declare @StopOver nvarchar(4)
declare @iCount int
declare @isFirstRecord int

--------- Common Setting ------------------
SELECT @NesID = 'NES' + Right('0000000' + Convert(varchar, (Convert(int, SubString(IsNull((SELECT Max(NesID) FROM NestleExport), 'NES0000000'), 4, 7)) + 1)), 7)
SELECT @SeqNo = 1
SELECT @FileCode = 'I'
SELECT @Sender = 'EX077050'
SELECT @FileVersion = 'WO28-E01'
SELECT @CreationDate = (Right(Convert(varchar, Year(GetDate())),2) + Right('00' + Convert(varchar, Month(GetDate())), 2) + Right('00' + Convert(varchar, Day(GetDate())), 2))
SELECT @CreationTime = (Right(Convert(varchar, DATEPART(hh,GetDate())),2) + Right('00' + Convert(varchar, DATEPART(mi,GetDate())), 2) + Right('00' + Convert(varchar, DATEPART(ss,GetDate())), 2))
SELECT @RecordNum = 1

SELECT @GdsCode = 'SAB'

-------------------------------------------------------------------------------
---------- Create Nestle Export Header --------------------------------
INSERT INTO NestleExport
(CompanyCode, NesID, StartDate, EndDate, CreateOn, CreateBy)
SELECT @CompanyCode, @NesID, @StartDate, DateAdd(d,-1, @EndDate), GetDate(), @User


	if(@customer<>'%')
	begin
		---------- Get Common Record Header --------------------------------
		DECLARE loop_cursor CURSOR FOR
	
		SELECT I.InvNum, (Right(Convert(varchar, Year(I.CreateOn)),2) + Right('00' + Convert(varchar, Month(I.CreateOn)), 2) + Right('00' + Convert(varchar, Day(I.CreateOn)), 2)) AS InvDate,
		(Case I.InvType When 'CHQ' Then 'CK' When 'UAT' Then 'TP' Else 'CA' End) AS PayMethod, 
		(Right(Convert(varchar, Year(B.CreateOn)),2) + Right('00' + Convert(varchar, Month(B.CreateOn)), 2) + Right('00' + Convert(varchar, Day(B.CreateOn)), 2)) AS BookingDate,
		(Right(Convert(varchar, Year(B.DepartDate)),2) + Right('00' + Convert(varchar, Month(B.DepartDate)), 2) + Right('00' + Convert(varchar, Day(B.DepartDate)), 2)) AS DepartDate,
	                 I.cltcode as CltCode 
		        FROM NestleCustomer N 
			Inner join PeoInv I On N.CompanyCode=I.CompanyCode and N.Cltcode=I.Cltcode
			Left Outer Join PeoMstr B On I.CompanyCode = B.CompanyCode AND I.BkgRef = B.BkgRef 
		WHERE 	I.CompanyCode = @CompanyCode AND
		I.VoidOn Is Null AND I.Void2JBA Is Null AND
		I.CreateOn >= @StartDate And I.CreateOn < @EndDate 
		and N.cltcode=@customer
		ORDER BY I.CreateOn, I.InvNum
	end 
	else
	begin
		DECLARE loop_cursor CURSOR FOR
		SELECT I.InvNum, (Right(Convert(varchar, Year(I.CreateOn)),2) + Right('00' + Convert(varchar, Month(I.CreateOn)), 2) + Right('00' + Convert(varchar, Day(I.CreateOn)), 2)) AS InvDate,
		(Case I.InvType When 'CHQ' Then 'CK' When 'UAT' Then 'TP' Else 'CA' End) AS PayMethod, 
		(Right(Convert(varchar, Year(B.CreateOn)),2) + Right('00' + Convert(varchar, Month(B.CreateOn)), 2) + Right('00' + Convert(varchar, Day(B.CreateOn)), 2)) AS BookingDate,
		(Right(Convert(varchar, Year(B.DepartDate)),2) + Right('00' + Convert(varchar, Month(B.DepartDate)), 2) + Right('00' + Convert(varchar, Day(B.DepartDate)), 2)) AS DepartDate ,
	                I.cltcode as CltCode 
       	             	FROM NestleCustomer N 
			Inner join PeoInv I On N.CompanyCode=I.CompanyCode and N.Cltcode=I.Cltcode
			Left Outer Join PeoMstr B On I.CompanyCode = B.CompanyCode AND I.BkgRef = B.BkgRef 
		WHERE 	I.CompanyCode = @CompanyCode AND
		I.VoidOn Is Null AND I.Void2JBA Is Null AND
		I.CreateOn >= @StartDate And I.CreateOn < @EndDate 
	end
OPEN loop_cursor
FETCH NEXT FROM loop_cursor INTO @InvNum, @InvDate, @PayMethod, @BkgDate, @DepartDate, @CltCode 

WHILE @@FETCH_STATUS = 0
BEGIN	
	---------- Insert Common Record Header --------------------------------
	SELECT @RecordType = '1'

	INSERT INTO NestleExportDetail
	(CompanyCode, NesID, SeqNo, FileCode, Sender, FileVersion, CreationDate, CreationTime, 
	RecordNum, CltCode, IsoCountry, CltLocal, TravellerName, StaffID, DepartDate,
	RecordType, GdsCode, InvNum, InvDate, PayMethod, BranchCode, InitInvNum, CreateOn, CreateBy)
	SELECT @CompanyCode, @NesID, @SeqNo, @FileCode, @Sender, @FileVersion, @CreationDate, @CreationTime, 
	Right('0000000' + Convert(varchar, @RecordNum), 7), @CltCode, @IsoCountry, @CltLocal,@TravellerName, @StaffID, @DepartDate,
	@RecordType, @GdsCode, (Left(@InvNum,2) + Substring(@InvNum, 4,7)), @InvDate, @PayMethod, @BranchCode, @InitInvNum,
	GetDate(), @User

	SELECT @SeqNo = @SeqNo + 1
	SELECT @RecordNum = @RecordNum + 1
	SELECT @isFirstRecord = 1

	-------------------------------------------------------------------------------
        if(@customer<>'%')
        begin
		---------- Get Ticket Header [Air Ticker]--------------------------------
		DECLARE loop_tkt_cursor CURSOR FOR	
		SELECT T.Ticket, Right('00' + Convert(varchar, Case When T.JQty < 1 Then 0 Else (T.JQty - 1) End),2), T.PaxName, T.Airline, 
		Left(Convert(varchar, Case When T.PaidFare = 0 Then T.FullFare Else T.PaidFare End),11), Case When T.PaidFare = 0 Then T.FullCurr Else T.PaidCurr End,
		(Right(Convert(varchar, Year(T.IssueOn)),2) + Right('00' + Convert(varchar, Month(T.IssueOn)), 2) + Right('00' + Convert(varchar, Day(T.IssueOn)), 2)) AS IssueOn,
		N.CltCode
                From PeoTkt T Inner join NestleCustomer N On T.CompanyCode=N.CompanyCode and T.CltCode=N.CltCode
                WHERE T.CompanyCode = @CompanyCode AND   
		VoidOn Is Null AND
		Ticket IN (SELECT Ticket FROM PeoInvTkt WHERE InvNum = @InvNum AND CompanyCode = @CompanyCode
			UNION
			SELECT Ticket FROM PeoInvTktRef WHERE InvNum = @InvNum AND CompanyCode = @CompanyCode) 
	and N.cltcode=@customer
		ORDER BY T.Ticket, T.TktSeq
        end
        else
        begin

                DECLARE loop_tkt_cursor CURSOR FOR	
		SELECT T.Ticket, Right('00' + Convert(varchar, Case When T.JQty < 1 Then 0 Else (T.JQty - 1) End),2), T.PaxName, T.Airline, 
		Left(Convert(varchar, Case When T.PaidFare = 0 Then T.FullFare Else T.PaidFare End),11), Case When T.PaidFare = 0 Then T.FullCurr Else T.PaidCurr End,
		(Right(Convert(varchar, Year(T.IssueOn)),2) + Right('00' + Convert(varchar, Month(T.IssueOn)), 2) + Right('00' + Convert(varchar, Day(T.IssueOn)), 2)) AS IssueOn,
		N.CltCode
                From PeoTkt T Inner join NestleCustomer N On T.CompanyCode=N.CompanyCode and T.CltCode=N.CltCode
                WHERE T.CompanyCode = @CompanyCode AND
		T.VoidOn Is Null AND
		T.Ticket IN (SELECT Ticket FROM PeoInvTkt WHERE InvNum = @InvNum AND CompanyCode = @CompanyCode
			UNION
			SELECT Ticket FROM PeoInvTktRef WHERE InvNum = @InvNum AND CompanyCode = @CompanyCode) 
       		ORDER BY T.Ticket, T.TktSeq
        end

	OPEN loop_tkt_cursor
	FETCH NEXT FROM loop_tkt_cursor INTO @TktNum, @TktConj, @Passenger, @Airline, @PaidFare, @IsoCurrency, @TktDate, @CltCode

	WHILE @@FETCH_STATUS = 0
	BEGIN	
		---------- Insert Ticket Header [Air Ticker]--------------------------------
		IF @isFirstRecord <> 1
		BEGIN
			SELECT @RecordType = '1'

			INSERT INTO NestleExportDetail
			(CompanyCode, NesID, SeqNo, FileCode, Sender, FileVersion, CreationDate, CreationTime, 
			RecordNum, CltCode, IsoCountry, CltLocal, TravellerName, StaffID, DepartDate,
			RecordType, GdsCode, InvNum, InvDate, PayMethod, BranchCode, InitInvNum, CreateOn, CreateBy)
			SELECT @CompanyCode, @NesID, @SeqNo, @FileCode, @Sender, @FileVersion, @CreationDate, @CreationTime, 
			Right('0000000' + Convert(varchar, @RecordNum), 7), @CltCode, @IsoCountry, @CltLocal,@TravellerName, @StaffID, @DepartDate,
			@RecordType, @GdsCode, (Left(@InvNum,2) + Substring(@InvNum, 4,7)), @InvDate, @PayMethod, @BranchCode, @InitInvNum,
			GetDate(), @User
		
			SELECT @SeqNo = @SeqNo + 1
			SELECT @RecordNum = @RecordNum + 1
		END

		SELECT @RecordType = '2'
		SELECT @RecordSubType = '1'

		INSERT INTO NestleExportDetail
		(CompanyCode, NesID, SeqNo, FileCode, Sender, FileVersion, CreationDate, CreationTime, 
		RecordNum, CltCode, IsoCountry, CltLocal, TravellerName, StaffID, DepartDate,
		RecordType, TktNum, TktConj, RecordSubType, Passenger, Airline, Class, PaidFare, PrecedingSign,
		IsoCurrency, BkgDate, TktDate, ContractNo, IataAgency, OrgTktNum, CreateOn, CreateBy)
		SELECT @CompanyCode, @NesID, @SeqNo, @FileCode, @Sender, @FileVersion, @CreationDate, @CreationTime, 
		Right('0000000' + Convert(varchar, @RecordNum), 7), @CltCode, @IsoCountry, @CltLocal,@TravellerName, @StaffID, @DepartDate,
		@RecordType, @TktNum, @TktConj, @RecordSubType, @Passenger, @Airline, @Class, @PaidFare, @PrecedingSign,
		@IsoCurrency, @BkgDate, @TktDate, @ContractNo, @IataAgency, @OrgTktNum, GetDate(), @User
		
		SELECT @SeqNo = @SeqNo + 1
		SELECT @RecordNum = @RecordNum + 1

		-------------------------------------------------------------------------------
		---------- Get Ticket Detail [Segment]--------------------------------
		DECLARE loop_del_cursor CURSOR FOR	
			SELECT (Left(Convert(varchar, CreateOn, 113),2) + Upper(Substring(Convert(varchar, CreateOn, 113),4,3))), DepartCity, ArrivalCity, 
			Airline, Flight, Class, DepartTime, ArrTime, FareBasic
			FROM PeoTktDetail
			WHERE CompanyCode = @CompanyCode AND
			Ticket = @TktNum
			ORDER BY Ticket, TktSeq

		OPEN loop_del_cursor
		FETCH NEXT FROM loop_del_cursor INTO @SegDate, @DepartCity, @ArrivalCity, 
		@Air, @Flight, @Cls, @DepartTime, @ArrivalTime, @FareBasic

		----------Reset Values --------------------------------------------------------------------------	
		SELECT @iCount = 1
		SELECT @SegDate1=Null,@ArrivalDateInd1=Null,@DepartCity1=Null,@ArrivalCity1=Null,@Air1=Null,@Flight1=Null,@Cls1=Null,@DepartTime1=Null,@ArrivalTime1=Null,@FareBasic1=Null
		SELECT @SegDate2=Null,@ArrivalDateInd2=Null,@DepartCity2=Null,@ArrivalCity2=Null,@Air2=Null,@Flight2=Null,@Cls2=Null,@DepartTime2=Null,@ArrivalTime2=Null,@FareBasic2=Null
		SELECT @SegDate3=Null,@ArrivalDateInd3=Null,@DepartCity3=Null,@ArrivalCity3=Null,@Air3=Null,@Flight3=Null,@Cls3=Null,@DepartTime3=Null,@ArrivalTime3=Null,@FareBasic3=Null
		SELECT @SegDate4=Null,@ArrivalDateInd4=Null,@DepartCity4=Null,@ArrivalCity4=Null,@Air4=Null,@Flight4=Null,@Cls4=Null,@DepartTime4=Null,@ArrivalTime4=Null,@FareBasic4=Null
		----------------------------------------------------------------------------------------------------
		WHILE @@FETCH_STATUS = 0
		BEGIN	
			IF @iCount = 1
			BEGIN
				SELECT @SegDate1=@SegDate
				SELECT @ArrivalDateInd1=@ArrivalDateInd
				SELECT @DepartCity1=@DepartCity,@ArrivalCity1=@ArrivalCity
				SELECT @Air1=@Air,@Flight1=@Flight,@Cls1=@Cls
				SELECT @DepartTime1=@DepartTime,@ArrivalTime1=@ArrivalTime
				SELECT @FareBasic1=@FareBasic
			END
			IF @iCount = 2 
			BEGIN
				SELECT @SegDate2=@SegDate
				SELECT @ArrivalDateInd2=@ArrivalDateInd
				SELECT @DepartCity2=@DepartCity,@ArrivalCity2=@ArrivalCity
				SELECT @Air2=@Air,@Flight2=@Flight,@Cls2=@Cls
				SELECT @DepartTime2=@DepartTime,@ArrivalTime2=@ArrivalTime
				SELECT @FareBasic2=@FareBasic
			END
			IF @iCount = 3
			BEGIN
				SELECT @SegDate3=@SegDate
				SELECT @ArrivalDateInd3=@ArrivalDateInd
				SELECT @DepartCity3=@DepartCity,@ArrivalCity3=@ArrivalCity
				SELECT @Air3=@Air,@Flight3=@Flight,@Cls3=@Cls
				SELECT @DepartTime3=@DepartTime,@ArrivalTime3=@ArrivalTime
				SELECT @FareBasic3=@FareBasic
			END
			IF @iCount = 4
			BEGIN
				SELECT @SegDate4=@SegDate
				SELECT @ArrivalDateInd4=@ArrivalDateInd
				SELECT @DepartCity4=@DepartCity,@ArrivalCity4=@ArrivalCity
				SELECT @Air4=@Air,@Flight4=@Flight,@Cls4=@Cls
				SELECT @DepartTime4=@DepartTime,@ArrivalTime4=@ArrivalTime
				SELECT @FareBasic4=@FareBasic
			END

			SELECT @iCount = @iCount + 1
			FETCH NEXT FROM loop_del_cursor INTO @SegDate, @DepartCity, @ArrivalCity, 
			@Air, @Flight, @Cls, @DepartTime, @ArrivalTime, @FareBasic
		END
		DEALLOCATE loop_del_cursor

		---------- Insert Ticket Detail [Segment]--------------------------------
		IF @iCount > 1 
		BEGIN
			SELECT @RecordType = '2'
			SELECT @RecordSubType = '2'

			INSERT INTO NestleExportDetail
			(CompanyCode, NesID, SeqNo, FileCode, Sender, FileVersion, CreationDate, CreationTime, 
			RecordNum, CltCode, IsoCountry, CltLocal, TravellerName, StaffID, DepartDate,
			RecordType, TktNum, TktConj, RecordSubType, 
			SegDate1, ArrivalDateInd1, DepartCity1, ArrivalCity1, Airline1, Flight1, Class1, DepartTime1, ArrivalTime1, FareBasic1,
			SegDate2, ArrivalDateInd2, DepartCity2, ArrivalCity2, Airline2, Flight2, Class2, DepartTime2, ArrivalTime2, FareBasic2,
			SegDate3, ArrivalDateInd3, DepartCity3, ArrivalCity3, Airline3, Flight3, Class3, DepartTime3, ArrivalTime3, FareBasic3,
			SegDate4, ArrivalDateInd4, DepartCity4, ArrivalCity4, Airline4, Flight4, Class4, DepartTime4, ArrivalTime4, FareBasic4,
			StopOver, CreateOn, CreateBy)
			SELECT @CompanyCode, @NesID, @SeqNo, @FileCode, @Sender, @FileVersion, @CreationDate, @CreationTime, 
			Right('0000000' + Convert(varchar, @RecordNum), 7), @CltCode, @IsoCountry, @CltLocal,@TravellerName, @StaffID, @DepartDate,
			@RecordType, @TktNum, @TktConj, @RecordSubType, 
			@SegDate1, @ArrivalDateInd1, @DepartCity1, @ArrivalCity1, @Air1, @Flight1, @Cls1, @DepartTime1, @ArrivalTime1, @FareBasic1,
			@SegDate2, @ArrivalDateInd2, @DepartCity2, @ArrivalCity2, @Air2, @Flight2, @Cls2, @DepartTime2, @ArrivalTime2, @FareBasic2,
			@SegDate3, @ArrivalDateInd3, @DepartCity3, @ArrivalCity3, @Air3, @Flight3, @Cls3, @DepartTime3, @ArrivalTime3, @FareBasic3,
			@SegDate4, @ArrivalDateInd4, @DepartCity4, @ArrivalCity4, @Air4, @Flight4, @Cls4, @DepartTime4, @ArrivalTime4, @FareBasic4,
			@StopOver, GetDate(), @User
		
			SELECT @SeqNo = @SeqNo + 1
			SELECT @RecordNum = @RecordNum + 1
		END

		SELECT @isFirstRecord = 0

		FETCH NEXT FROM loop_tkt_cursor INTO @TktNum, @TktConj, @Passenger, @Airline, @PaidFare, @IsoCurrency, @TktDate,@CltCode
	END
	DEALLOCATE loop_tkt_cursor



	FETCH NEXT FROM loop_cursor INTO @InvNum, @InvDate, @PayMethod, @BkgDate, @DepartDate,@CltCode
END
DEALLOCATE loop_cursor

---------- RETURN Nes ID
SELECT @NesID




