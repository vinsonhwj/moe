﻿





CREATE     PROCEDURE dbo.sp_CltProductivityReport_ByTranDate
@StartDate CHAR(10),
@EndDate CHAR(10),
@CltCode VarChar(50) = '%',
@CompanyCode VarChar(2) = 'SH'
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003'
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003'
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
select @CltCode =   ltrim(rtrim(@CltCode)) + '%'
print @cltcode
--return
SELECT m.CompanyCode, m.Cltcode as Cltcode, m.CltName as CltName,
isnull(
	(select sum(hkdamt) from peoinv t (nolock)
	WHERE t.companycode=m.companycode AND t.cltcode=m.cltcode
	AND t.createon between @StartDate and @enddayofyear
	AND substring(t.invnum, 2, 1) = 'I'
), 0) as peoinv,
isnull(
	(select sum(hkdamt) from peoinv t (nolock)
	WHERE t.companycode=m.companycode AND t.cltcode=m.cltcode
	AND t.createon between @StartDate and @enddayofyear
	AND substring(t.invnum, 2, 1) = 'C'
), 0) as peocn,
isnull(
	(select sum(netfare + totaltax) from peotkt t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peotkt,
isnull(
	(select sum(netfare + totaltax) from peotkt t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peotktVoid,
isnull(
	(select sum(hkdamt) from peoxo t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peoxo,
isnull(
	(select sum(hkdamt) from peoxo t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peoxoVoid,
isnull(
	(select sum(hkdamt) from peovch t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peovch,
isnull(
	(select sum(hkdamt) from peovch t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peovchVoid,
isnull(
	(select sum(hkdamt) from peomco t (nolock)  LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peomco,
isnull(
	(select sum(hkdamt) from peomco t (nolock)  LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @StartDate and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peomcoVoid,
isnull(
	(select sum(hkdamt) from peoinv t (nolock)
	WHERE t.companycode=m.companycode
	AND t.createon between @firstdayofyear and @enddayofyear
	AND t.cltcode=m.cltcode AND substring(t.invnum, 2, 1) = 'I'
), 0) as peoinv2,
isnull(
	(select sum(hkdamt) from peoinv t (nolock)
	WHERE t.companycode=m.companycode AND t.cltcode=m.cltcode
	AND t.createon between @firstdayofyear and @enddayofyear
	AND substring(t.invnum, 2, 1) = 'C'
), 0) as peocn2,
isnull(
	(select sum(netfare + totaltax) from peotkt t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peotkt2,
isnull(
	(select sum(netfare + totaltax) from peotkt t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peotktVoid2,
isnull(
	(select sum(hkdamt) from peoxo t (nolock)    	LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peoxo2,
isnull(
	(select sum(hkdamt) from peoxo t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peoxoVoid2,
isnull(
	(select sum(hkdamt) from peovch t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peovch2,
isnull(
	(select sum(hkdamt) from peovch t (nolock)    LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peovchVoid2,
isnull(
	(select sum(hkdamt) from peomco t (nolock)  LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.createon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peomco2,
isnull(
	(select sum(hkdamt) from peomco t (nolock)  LEFT JOIN
	peomstr p ON p.companycode=t.companycode AND p.bkgref=t.bkgref AND t.voidon between @firstdayofyear and @enddayofyear
	WHERE p.companycode=m.companycode AND p.cltode=m.cltcode
), 0) as peomcoVoid2
INTO #tmp_result_nor
from (select CompanyCode, Cltcode as Cltcode, CltName as CltName from customer mmm (nolock) where mmm.companycode = @CompanyCode
and mmm.cltcode like @CltCode) AS m
group by m.companycode, m.cltcode, m.cltName
-- SELECT * FROM  #tmp_result_nor
SELECT 	b.CompanyCode,
	b.CltCode,
	b.CltName,
	CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) as [Price],
	CONVERT(Decimal(20,2), ((b.peotkt + b.peomco + b.peoxo + peovch) - (b.peotktvoid + peomcovoid + peoxovoid + peovchvoid))) as [Cost],
	(CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) -
	CONVERT(Decimal(20,2), ((b.peotkt + b.peomco + b.peoxo + peovch) - (b.peotktvoid + peomcovoid + peoxovoid + peovchvoid)))) as [Margin],
	CONVERT(Decimal(20,2), CASE
		WHEN (CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) -
			CONVERT(Decimal(20,2), ((b.peotkt + b.peomco + b.peoxo + peovch) - (b.peotktvoid + peomcovoid + peoxovoid + peovchvoid)))
			= CONVERT(Decimal(20,2), 0)) THEN CONVERT(Decimal(20, 2), 0)
		WHEN (CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) = CONVERT(Decimal(20,2), 0))
			THEN CONVERT(Decimal(20, 2), -100)
		ELSE ((CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) - CONVERT(Decimal(20,2), ((b.peotkt + b.peomco + b.peoxo + peovch) - (b.peotktvoid + peomcovoid + peoxovoid + peovchvoid))))
			/ CONVERT(Decimal(20,2), (b.peoinv - b.peocn)) * 100) END) as [Yield(%)],
	CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) as [YTDPrice],
	CONVERT(Decimal(20,2), ((b.peotkt2 + b.peomco2 + b.peoxo2 + peovch2) - (b.peotktvoid2 + peomcovoid2 + peoxovoid2 + peovchvoid2))) as [YTDCost],
	(CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) -
	CONVERT(Decimal(20,2), ((b.peotkt2 + b.peomco2 + b.peoxo2 + peovch2) - (b.peotktvoid2 + peomcovoid2 + peoxovoid2 + peovchvoid2)))) as YTDMargin,
	CONVERT(Decimal(20,2), CASE
		WHEN (CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) -
			CONVERT(Decimal(20,2), ((b.peotkt2 + b.peomco2 + b.peoxo2 + peovch2) - (b.peotktvoid2 + peomcovoid2 + peoxovoid2 + peovchvoid2)))
			= CONVERT(Decimal(20,2), 0)) THEN CONVERT(Decimal(20, 2), 0)
		WHEN (CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) = CONVERT(Decimal(20,2), 0))
			THEN CONVERT(Decimal(20, 2), -100)
		ELSE ((CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) - CONVERT(Decimal(20,2), ((b.peotkt2 + b.peomco2 + b.peoxo2 + peovch2) - (b.peotktvoid2 + peomcovoid2 + peoxovoid2 + peovchvoid2))))
			/ CONVERT(Decimal(20,2), (b.peoinv2 - b.peocn2)) * 100) END) as [YTDYield(%)]
FROM
#tmp_result_nor b
DROP TABLE #tmp_result_nor
END






