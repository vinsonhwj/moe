﻿
CREATE  procedure dbo.sp_RptMasterPnrEnquiryReport
@COMPANYCODE varchar(2),
@MASTERPNR varchar(10),
@START_DATE varchar(10),
@END_DATE varchar(25),
@SEARCh_MODE char(1),
@SHOW_DIFF char(1)

AS

CREATE TABLE #TmpMaster
(
	Masterpnr varchar(10) null default ''
)

IF @SHOW_DIFF = 'N'
BEGIN
	IF @SEARCh_MODE = 'C'
	BEGIN
		IF RTRIM(LTRIM(@MASTERPNR)) <> ''
			INSERT INTO #TmpMaster
			SELECT distinct Masterpnr 
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND Createon between @START_DATE AND @END_DATE AND Masterpnr = @MASTERPNR
		ELSE
			INSERT INTO #TmpMaster
			SELECT distinct Masterpnr 
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND Createon between @START_DATE AND @END_DATE

	END
	ELSE
	BEGIN
		IF RTRIM(LTRIM(@MASTERPNR)) <> ''
			INSERT INTO #TmpMaster
			SELECT distinct Masterpnr 
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND DepartDate between @START_DATE AND @END_DATE AND Masterpnr = @MASTERPNR
		ELSE
			INSERT INTO #TmpMaster
			SELECT distinct Masterpnr 
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND DepartDate between @START_DATE AND @END_DATE
	END
END
ELSE
BEGIN
	IF @SEARCh_MODE = 'C'
	BEGIN
		IF RTRIM(LTRIM(@MASTERPNR)) <> ''
			INSERT INTO #TmpMaster
			SELECT Masterpnr
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND Createon between @START_DATE AND @END_DATE AND masterpnr = @MASTERPNR
			GROUP BY Masterpnr
			having (count(Masterpnr) > 1)
		ELSE
			INSERT INTO #TmpMaster
			SELECT Masterpnr
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND Createon between @START_DATE AND @END_DATE
			GROUP BY Masterpnr
			having (count(Masterpnr) > 1)

	END
	ELSE
	BEGIN
		IF RTRIM(LTRIM(@MASTERPNR)) <> ''
			INSERT INTO #TmpMaster
			SELECT Masterpnr
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND DepartDate between @START_DATE AND @END_DATE AND masterpnr = @MASTERPNR
			GROUP BY Masterpnr
			having (count(Masterpnr) > 1)
		ELSE
			INSERT INTO #TmpMaster
			SELECT Masterpnr
			FROM PeoMstr (nolock) WHERE COMPANYCODE = @COMPANYCODE AND DepartDate between @START_DATE AND @END_DATE
			GROUP BY Masterpnr
			having (count(Masterpnr) > 1)
	END



END


SELECT 
Mstr.Masterpnr AS [Master Pnr]
, Mstr.Bkgref as [Bkg Ref#]
, Mstr.TeamCode as [Team Code]
, Mstr.Staffcode as [Staff Code]
, Mstr.Invamt as [Total Invoice]
, Mstr.Docamt as [Total Document Amount],
Mstr.InvAmt - Mstr.Docamt as [Margin],
CONVERT(decimal(16,2),CASE WHEN Mstr.InvAmt = 0 THEN 0 ELSE ((Mstr.InvAmt - Mstr.Docamt) / Mstr.InvAmt) * 100 END) as [Yield(%)]
FROM Peomstr mstr (nolock), #TmpMaster MPnr
WHERE mstr.CompanyCode = @COMPANYCODE AND mstr.masterpnr = MPnr.masterpnr
ORDER BY Mstr.Masterpnr

DROP TABLE #TmpMaster



