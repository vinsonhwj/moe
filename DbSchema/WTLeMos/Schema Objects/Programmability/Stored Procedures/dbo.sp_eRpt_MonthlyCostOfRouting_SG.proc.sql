﻿
CREATE PROCEDURE dbo.sp_eRpt_MonthlyCostOfRouting_SG
(
	@COMPANY_CODE CHAR(2),
	@START_DATE DATETIME,
	@END_DATE DATETIME
)
AS
BEGIN
	DECLARE @lnum_netfare decimal(18,2);
	DECLARE @lnum_voidfare decimal(18,2);
	DECLARE @lstr_Currxonum varchar(10);
	DECLARE @lstr_Prexonum varchar(10);
	declare @lstr_currdac varchar(3)
	DECLARE @ldt_Issuedate DATETIME;
	DECLARE @ldt_PreIssuedate DATETIME;
	DECLARE @ldt_Voiddate DATETIME;
	DECLARE @ldt_PreVoiddate DATETIME;
	DECLARE @lstr_Routing varchar(500);
	DECLARE @lstr_oac varchar(3);
	DECLARE @lstr_dac varchar(3);
	DECLARE @lnum_totalcost decimal(16,2);

	CREATE TABLE #tmpReport
	(
		Routing varchar(500) NOT NULL,
		TxnYear INT NOT NULL,
		TxnMonth INT NOT NULL,
		amt DECIMAL(16,2)
	);

	INSERT INTO #tmpReport
	SELECT routing, YEAR(issueon), MONTH(issueon), SUM(netfare + totaltax) as amt
	FROM peotkt t (nolock)
	WHERE companycode = @COMPANY_CODE
	AND issueon BETWEEN @START_DATE AND @END_DATE
	GROUP BY routing, YEAR(issueon), MONTH(issueon);

	INSERT INTO #tmpReport
	SELECT routing, YEAR(voidon), MONTH(voidon), SUM(netfare + totaltax) * -1 as amt
	FROM peotkt t (nolock)
	WHERE companycode = @COMPANY_CODE
	AND voidon BETWEEN @START_DATE AND @END_DATE
	GROUP BY routing, YEAR(voidon), MONTH(voidon);

	--XO
	CREATE TABLE #tmpXoReport
	(
		Xonum varchar(10) NOT NULL,
		Routing varchar(500) NOT NULL,
		TxnYear INT NOT NULL,
		TxnMonth INT NOT NULL,
		NetFare decimal(16,2) NOT NULL
	);

	DECLARE lcur_xorouting CURSOR
	FOR
	SELECT xo.xonum, xo.createon, xo.voidon, substring(seg.servname,16,3) as departcity, substring(seg.servname,22,3) as ArrivalCity
		, isnull((case when detail.costcurr = 'HKD' then (detail.costamt + detail.taxamt)* detail.qty else detail.hkdamt end ),0) as hkdamt
	FROM peoxo xo (nolock)
		INNER JOIN peoxoseg seg (nolock) ON xo.companycode = seg.companycode AND xo.xonum = seg.xonum 
		LEFT OUTER JOIN peoxodetail detail (nolock) ON detail.companycode = seg.companycode AND detail.xonum = seg.xonum AND detail.segnum = seg.segnum
	WHERE xo.companycode = @COMPANY_CODE
	AND (
		xo.createon BETWEEN @START_DATE AND @END_DATE
		OR
		xo.VOIDON BETWEEN @START_DATE AND @END_DATE
	)
	AND seg.seRVtype = 'AIR'
	AND suppcode <> 'htatkt'
	ORDER BY seg.xonum, seg.segnum;

	OPEN lcur_xorouting;

	FETCH lcur_xorouting
	INTO @lstr_Currxonum, @ldt_Issuedate, @ldt_Voiddate, @lstr_oac, @lstr_dac, @lnum_netfare;

	WHILE @@fetch_status = 0
	BEGIN

		IF @lstr_Prexonum <> @lstr_Currxonum
		BEGIN 
			IF @lstr_routing <> ''
			BEGIN
				INSERT INTO #tmpXoReport (Xonum, Routing, TxnYear, TxnMonth, netfare) values (@lstr_Prexonum, @lstr_Routing, YEAR(@ldt_PreIssuedate), MONTH(@ldt_PreIssuedate), @lnum_totalcost);
				
				IF (@ldt_PreVoiddate IS NOT NULL)
				BEGIN
					INSERT INTO #tmpXoReport (Xonum, Routing, TxnYear, TxnMonth, netfare) values (@lstr_Prexonum, @lstr_Routing, YEAR(@ldt_PreVoiddate), MONTH(@ldt_PreVoiddate), @lnum_totalcost * -1);
				END
			END
			
			SELECT @lstr_routing = '';
			SELECT @lnum_totalcost = 0;
		END

		IF @lstr_routing = ''
		BEGIN
			SELECT @lstr_routing = @lstr_oac  + '/' + @lstr_dac
		END
		ELSE
		BEGIN
			if @lstr_currdac = @lstr_oac
				SELECT @lstr_routing = @lstr_routing  + '/' + @lstr_dac
			else
				SELECT @lstr_routing = @lstr_routing  + '-' + @lstr_oac  + '/' + @lstr_dac
		end
		

		SELECT @lstr_currdac = @lstr_dac,
			@lstr_Prexonum = @lstr_Currxonum,
			@ldt_PreIssuedate = @ldt_Issuedate,
			@ldt_PreVoiddate = @ldt_Voiddate,
			@lnum_totalcost = @lnum_totalcost + @lnum_netfare

		FETCH lcur_xorouting
		INTO @lstr_Currxonum, @ldt_Issuedate, @ldt_Voiddate, @lstr_oac, @lstr_dac, @lnum_netfare;
	END

	CLOSE lcur_xorouting;
	DEALLOCATE lcur_xorouting;

	if @lstr_routing <> ''
	begin
		INSERT INTO #tmpXoReport (Xonum, Routing, TxnYear, TxnMonth, netfare) values (@lstr_Prexonum, @lstr_Routing, YEAR(@ldt_PreIssuedate), MONTH(@ldt_PreIssuedate), @lnum_totalcost);
		
		IF (@ldt_PreVoiddate IS NOT NULL)
		BEGIN
			INSERT INTO #tmpXoReport (Xonum, Routing, TxnYear, TxnMonth, netfare) values (@lstr_Prexonum, @lstr_Routing, YEAR(@ldt_PreVoiddate), MONTH(@ldt_PreVoiddate), @lnum_totalcost * -1);
		END
	END

	--MCO
	INSERT INTO #tmpReport (Routing, TxnYear, TxnMonth, amt)
	SELECT '** MCO **', YEAR(t.issueon), MONTH(t.issueon), ISNULL(SUM(ISNULL(costamt,0) + ISNULL(taxamt,0)),0) AS amt
	FROM peomco t (nolock)
	WHERE Companycode = @COMPANY_CODE
	AND t.issueon BETWEEN @START_DATE AND @END_DATE
	GROUP BY YEAR(t.issueon), MONTH(t.issueon);

	INSERT INTO #tmpReport (Routing, TxnYear, TxnMonth, amt)
	SELECT '** MCO **', YEAR(t.voidon), MONTH(t.voidon), ISNULL(SUM((ISNULL(costamt,0) + ISNULL(taxamt,0)) * -1),0) AS amt
	FROM peomco t (nolock)
	WHERE Companycode = @COMPANY_CODE
	AND t.voidon BETWEEN @START_DATE AND @END_DATE
	GROUP BY YEAR(t.voidon), MONTH(t.voidon);
	
	DELETE FROM eRpt_MonthlyCostOfRouting_SG;
	
	INSERT INTO eRpt_MonthlyCostOfRouting_SG (Routing, TxnYear, TxnMonth, TxnAmt)
	SELECT Routing, TxnYear, TxnMonth, SUM(amt) AS TxnAmt
	FROM (
		SELECT Routing, TxnYear, TxnMonth, amt
		FROM #tmpReport
		
		UNION ALL
		
		SELECT Routing, TxnYear, TxnMonth, SUM(netfare) AS amt
		FROM #tmpXoReport
		GROUP BY Routing, TxnYear, TxnMonth
	) a
	GROUP BY Routing, TxnYear, TxnMonth;
	
	DROP TABLE #tmpReport;
	DROP TABLE #tmpXoReport;
END
