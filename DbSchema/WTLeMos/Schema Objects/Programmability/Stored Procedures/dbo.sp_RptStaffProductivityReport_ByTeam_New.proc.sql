﻿

--exec  			sp_RptStaffProductivityReport_ByTeam_New 'SG', '2008-03-01', '2008-03-31 23:59:59.000'

--sp_helptext sp_RptStaffProductivityReport_ByTeam_New 

CREATE                       PROCEDURE dbo.sp_RptStaffProductivityReport_ByTeam_New

@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25)

AS

DECLARE @lstr_Staffcode char(10),@lstr_TeamCode char(3), @lnum_Cost decimal(16,2), @lnum_PaxTotal integer
DECLARE @FinancalYear datetime, @ldt_StartDate datetime, @lstr_InvType char(3)

-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------



print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT 
a.companycode, a.teamcode, a.bkgref, a.staffcode, b.invnum, b.createon, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), #tempStaff staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND a.staffcode = staff.staffcode
GROUP BY a.companycode,a.teamcode,  a.bkgref, a.StaffCode, b.invtype, b.invnum, b.createon
ORDER BY 1, 2


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Teamcode varchar(10) not null
,	Staffcode varchar(10) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)
--Ticket
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), #tempstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.issueon between @FinancalYear AND @END_DATE




insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.voidon, (tkt.netfare+tkt.totaltax) * -1 cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), #tempstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.voidon between @FinancalYear AND @END_DATE




print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.createon, xo.hkdamt cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.createon between @FinancalYear AND @END_DATE

select 'XO', sum(cost) from #tmpDoc


insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.voidon, xo.hkdamt * -1 cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.voidon between @FinancalYear AND @END_DATE



print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.createon, vch.hkdamt cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND vch.createon between @FinancalYear AND @END_DATE



insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.voidon, vch.hkdamt * -1 cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND vch.voidon between @FinancalYear AND @END_DATE



print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.issueon, mco.costamt + mco.taxamt cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.issueon between @FinancalYear AND @END_DATE



insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.voidon, (mco.costamt + mco.taxamt) * -1 cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.voidon between @FinancalYear AND @END_DATE




print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	TeamCode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)

insert into #TmpSellAmt
(Companycode, teamcode)
select distinct s.companycode, s.teamcode from peostaff s(nolock), #tempStaff staff
where s.companycode = @COMPANY_CODE and s.staffcode = staff.staffcode

--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDPrice = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode


--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, count(1) as counts
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, count(1) paxcount
FROM peopax pax(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
AND inv.createon between @START_DATE AND @END_DATE
GROUP BY inv.companycode, inv.Teamcode) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode




SELECT Companycode
, teamcode as code, PaxTotal as [Total Pax]
, CASE when Price <> 0 then CONVERT(VARCHAR,Price) else '-' END as Price
, CASE when Dep <> 0 then CONVERT(VARCHAR,Dep) else '-' END as Dep
, CASE WHEN cost <> 0 THEN CONVERT(VARCHAR,cost) else '-' END AS Cost
, CASE WHEN price-cost <> 0 THEN CONVERT(VARCHAR,price-cost) ELSE '-' END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then CONVERT(VARCHAR,YTDPrice) else '-' END as YTDPrice
, CASE when YTDDep <> 0 then CONVERT(VARCHAR,YTDDep) else '-' END as YTDDep
, CASE WHEN YTDcost <> 0 THEN CONVERT(VARCHAR,YTDcost) else '-' END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN CONVERT(VARCHAR,YTDprice-YTDcost) ELSE '-' END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]
, CASE WHEN TktCount <> 0 THEN CONVERT(VARCHAR,TktCount) else '-' END AS TktCount
FROM #TmpSellAmt
ORDER BY teamcode

print 'End of Generating Report at ' + convert(varchar,getdate(),109)

DROP TABLE #tempStaff
DROP TABLE #TmpdOC
DROP TABLE #TmpInvTotal
DROP TABLE #TmpSellAmt

