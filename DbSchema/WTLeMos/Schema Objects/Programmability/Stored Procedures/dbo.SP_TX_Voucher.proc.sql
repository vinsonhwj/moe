﻿




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_TX_Voucher]
	-- Add the parameters for the stored procedure here
    @companycode NVARCHAR(2) ,
    @bkgref NVARCHAR(10) ,
    @segnum NVARCHAR(5)
AS 
    BEGIN
--建保存结果的临时表
	CREATE TABLE #resultHtlDetail
	(
		[BKGREF] [nvarchar](10) NOT NULL,
		[COMPANYCODE] [nvarchar](2) NOT NULL,
		[SEQTYPE] [nvarchar](5) NOT NULL,
		[SEGNUM] [nvarchar](5) NOT NULL,
		[SEQNUM] [nvarchar](5) NOT NULL,
		[QTY] [int] NULL,
		[ARRDATE] [datetime] NULL,
		[DEPARTDATE] [datetime] NULL,
		[RMNTS] [decimal](4, 0) NULL,
		[RATENAME] [nvarchar](40) NULL,
		[ITEMNAME] [nvarchar](100) NULL,
		[CURR] [nvarchar](3) NULL,
		[AMT] [decimal](16, 2) NULL,
		[TOTALAMT] [decimal](16, 2) NULL,
		[TAXCURR] [nvarchar](3) NULL,
		[TAXAMT] [decimal](16, 2) NULL,
		[itemtype] [nchar](1) NULL
		
	)
	--建保存临时数据的临时表
	SELECT * INTO #tmpHtlDetail FROM #resultHtlDetail

	DECLARE @sBKGREF NVARCHAR(10),
			@sCOMPANYCODE NVARCHAR(2),
			@sSEQTYPE NVARCHAR(5),
			@sSEGNUM NVARCHAR(5),
			@sSEQNUM NVARCHAR(5),   
			@sQTY INT, 
			@sARRDATE DATETIME,
			@sDEPARTDATE DATETIME,
			@sRMNTS DECIMAL(4,0),
			@sRATENAME NVARCHAR(40), 
			@sITEMNAME NVARCHAR(100),
			@sCURR NVARCHAR(3),
			@sAMT DECIMAL(16,2),
			@sTOTALAMT DECIMAL(16,2), 
 			@sTAXCURR NVARCHAR(3),
 			@sTAXAMT DECIMAL(16,2),
 			@sItemType NCHAR(1)
 			
 			
 	DECLARE	@bCount int,
 			@aCount INT
 			
 	INSERT #tmpHtlDetail (BKGREF, COMPANYCODE, SEQTYPE, SEGNUM, SEQNUM, QTY, ARRDATE, DEPARTDATE, RMNTS, RATENAME,
 	                            ITEMNAME, CURR, AMT, TOTALAMT, TAXCURR, TAXAMT, itemtype)
 	SELECT BKGREF,COMPANYCODE,SEQTYPE,SEGNUM,SEQNUM,QTY, ARRDATE,DEPARTDATE,RMNTS,RATENAME, 
		ITEMNAME,CURR,AMT,TOTALAMT, 
 		TAXCURR,TAXAMT ,itemtype 
	FROM dbo.PeoHtlDetail WHERE COMPANYCODE=@Companycode AND BKGREF=@BkgRef AND SEGNUM =@SegNum AND SEQTYPE ='COST'
 	
 	SET @aCount =0
 	SELECT @bCount=COUNT(*) FROM #tmpHtlDetail
 		
WHILE @aCount<>@bCount
	BEGIN
	IF @aCount<>0
	BEGIN
		DELETE #tmpHtlDetail 
		INSERT #tmpHtlDetail (BKGREF, COMPANYCODE, SEQTYPE, SEGNUM, SEQNUM, QTY, ARRDATE, DEPARTDATE, RMNTS, RATENAME,
		                            ITEMNAME, CURR, AMT, TOTALAMT, TAXCURR, TAXAMT, itemtype)
		SELECT BKGREF, COMPANYCODE, SEQTYPE, SEGNUM, SEQNUM, QTY, ARRDATE, DEPARTDATE, RMNTS,
		        RATENAME, ITEMNAME, CURR, AMT, TOTALAMT, TAXCURR, TAXAMT, itemtype FROM #resultHtlDetail
	END 
	SELECT @bCount=COUNT(*) FROM #resultHtlDetail
	
	DELETE #resultHtlDetail 
	
	DECLARE MyCursor CURSOR	FOR 
	SELECT BKGREF,COMPANYCODE,SEQTYPE,SEGNUM,MIN(SEQNUM)SEQNUM,SUM(QTY)QTY, ARRDATE,DEPARTDATE,RMNTS,MAX(RATENAME)RATENAME, 
		ITEMNAME,CURR,SUM(AMT) AMT,SUM(TOTALAMT) TOTALAMT, 
 		TAXCURR,SUM(TAXAMT) TAXAMT ,itemtype
	FROM #tmpHtlDetail WHERE COMPANYCODE=@Companycode AND BKGREF=@BkgRef AND SEGNUM =@SegNum AND SEQTYPE ='COST'
	GROUP BY BKGREF,COMPANYCODE,SEQTYPE,SEGNUM,ARRDATE,DEPARTDATE,RMNTS,ITEMNAME,itemtype,CURR,TAXCURR
	order by ARRDATE,DEPARTDATE,SEQNUM 

	--打开一个游标	
	OPEN MyCursor

	--循环一个游标
	
	FETCH NEXT FROM  MyCursor INTO @sBKGREF,@sCOMPANYCODE,@sSEQTYPE,@sSEGNUM,@sSEQNUM,@sQTY,
			@sARRDATE,@sDEPARTDATE,@sRMNTS,@sRATENAME,@sITEMNAME,@sCURR,@sAMT,@sTOTALAMT,
			@sTAXCURR,@sTAXAMT,@sItemType
	WHILE @@FETCH_STATUS =0
		BEGIN
			DECLARE @rowcount int
			select @rowcount =COUNT(1) from #resultHtlDetail 
			where BKGREF=@sBKGREF AND COMPANYCODE=@sCOMPANYCODE AND QTY=@sQTY AND DEPARTDATE=@sARRDATE AND ITEMNAME=@sITEMNAME
				AND itemtype=@sItemType
				
			if @rowcount>0
			begin
				update #resultHtlDetail set DEPARTDATE =@sDEPARTDATE,RMNTS=RMNTS+@sRMNTS, Amt=Amt +@sAmt,TOTALAMT=TOTALAMT+@sTOTALAMT,
					TAXAMT=TAXAMT+@sTAXAMT
				where BKGREF=@sBKGREF AND COMPANYCODE=@sCOMPANYCODE AND QTY=@sQTY AND DEPARTDATE=@sARRDATE AND ITEMNAME=@sITEMNAME
				AND itemtype=@sItemType

			end
			else
			begin
				INSERT #resultHtlDetail (BKGREF, COMPANYCODE, SEQTYPE, SEGNUM, SEQNUM, QTY, ARRDATE, DEPARTDATE, RMNTS, RATENAME,
				                            ITEMNAME, CURR, AMT, TOTALAMT, TAXCURR, TAXAMT, itemtype)
				VALUES  (@sBKGREF,@sCOMPANYCODE,@sSEQTYPE,@sSEGNUM,@sSEQNUM,@sQTY,
							@sARRDATE,@sDEPARTDATE,@sRMNTS,@sRATENAME,@sITEMNAME,@sCURR,@sAMT,@sTOTALAMT,
							@sTAXCURR,@sTAXAMT,@sItemType)
			END
			
			  print '游标成功取出一条数据' 
			  print @sARRDATE
              print @sDEPARTDATE

			FETCH NEXT FROM  MyCursor INTO  @sBKGREF,@sCOMPANYCODE,@sSEQTYPE,@sSEGNUM,@sSEQNUM,@sQTY,
			@sARRDATE,@sDEPARTDATE,@sRMNTS,@sRATENAME,@sITEMNAME,@sCURR,@sAMT,@sTOTALAMT,
			@sTAXCURR,@sTAXAMT,@sItemType
		END	

	--关闭游标
	CLOSE MyCursor
	--释放资源
	DEALLOCATE MyCursor
	
	SELECT @aCount=COUNT(*) FROM #resultHtlDetail
	
END

SELECT       CASE WHEN QTY > 2 THEN 2
                     WHEN QTY <= 2 THEN QTY
                END AS QtyOfHotel ,
                SEGNUM ,
                SEQNUM ,
                ISNULL(QTY, 0) AS QTY ,
                ISNULL(ARRDATE, '') AS ARRDATE ,
                ISNULL(DEPARTDATE, '') AS DEPARTDATE ,
                ISNULL(RMNTS, 0) AS rmnts ,
                RATENAME ,
                ITEMNAME ,
                ISNULL(CURR, '') AS SELLCURR ,
                ISNULL(AMT, 0) AS SELLAMT ,
                ISNULL(TOTALAMT, 0) AS TOTALAMT ,
                ISNULL(TAXCURR, '') AS TAXCURR ,
                ISNULL(TAXAMT, 0) AS TAXamt ,
                ISNULL(ItemType, '') AS ItemType
        FROM    #tmpHtlDetail
        WHERE   SEQTYPE = 'COST'
                AND bkgref = @bkgref
                AND SEGNUM IN ( @segnum, '' )
                AND companycode = @companycode
        ORDER BY seqnum

END







