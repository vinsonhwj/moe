﻿



CREATE PROCEDURE dbo.SP_MODIFY_STAFFCODE
AS
/*
AGC	GCH
CAJ	SOO
CAL	ALC
CMK	MIK
CPH	PEI
FJF	JES
FLS	LSM
FMM	WAN
FSL	SAN
GEM	EMA
GJC	JSC
HMC	MIC
ICL	CAR
IGL	GIL
IGO	ONN
IPK	WPK
IYG	YVN
PSH	SHA
PVL	VIV
*/
DECLARE @cc VARCHAR(10)
DECLARE @FROMstaffcode VARCHAR(10)
DECLARE @FROMbranchcode VARCHAR(10)
DECLARE @FROMteamcode VARCHAR(10)
DECLARE @TOstaffcode VARCHAR(10)
SELECT @cc = 			'MH'
SELECT @FROMstaffcode = 	'PVL'
SELECT @FROMbranchcode = branchcode, @FROMteamcode=teamcode FROM dbo.peostaff WHERE COMPANYCODE=@cc AND staffcode=@FROMstaffcode  
SELECT @TOstaffcode = 		'VIV'
IF EXISTS 
	( SELECT COMPANYCODE, branchcode, teamcode, staffcode, staffname
	  	, * FROM dbo.peostaff WHERE COMPANYCODE=@cc AND staffcode=@FROMstaffcode 
		AND branchcode=@FROMbranchcode AND teamcode=@FROMteamcode )
BEGIN 
	SELECT 'UPDATE dbo.peostaff'
 	UPDATE dbo.peostaff SET staffcode=@TOstaffcode WHERE COMPANYCODE=@cc AND staffcode=@FROMstaffcode
		AND branchcode=@FROMbranchcode AND teamcode=@FROMteamcode
	
	IF EXISTS 
		( SELECT COMPANYCODE, Branchcode, teamcode, staffcode, login
			, * FROM dbo.login WHERE COMPANYCODE=@cc 
			AND ( staffcode=@FROMstaffcode or login=@FROMstaffcode)
			AND teamcode=@FROMteamcode AND branchcode=@FROMbranchcode
		)
	BEGIN 
		SELECT 'UPDATE dbo.login'
		UPDATE dbo.shadowpassword SET login=@TOstaffcode WHERE COMPANYCODE=@cc
		AND login=@FROMstaffcode
		UPDATE dbo.access SET login=@TOstaffcode WHERE COMPANYCODE=@cc
		AND login=@FROMstaffcode
		UPDATE dbo.login SET login=@TOstaffcode, staffcode=@TOstaffcode WHERE COMPANYCODE=@cc
		AND ( staffcode=@FROMstaffcode or login=@FROMstaffcode)
		AND teamcode=@FROMteamcode AND branchcode=@FROMbranchcode
	END
END




