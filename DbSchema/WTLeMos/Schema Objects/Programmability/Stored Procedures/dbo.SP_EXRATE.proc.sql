﻿



/* Microsoft SQL Server - Scripting			*/
/* Server: WMWEB					*/
/* Database: TPEONY					*/
/* Creation Date 2/21/02 2:59:59 AM 			*/
/****** Object:  Stored Procedure dbo.SP_EXRATE    Script Date: 2/21/02 3:00:00 AM ******/
CREATE PROCEDURE SP_EXRATE
	@OAMT DECIMAL(16,2),
	@OCURRCODE CHAR(3),
	@NCURRCODE CHAR(3)
AS
BEGIN
	IF @OCURRCODE = @NCURRCODE
		RETURN @OAMT
	DECLARE @TMPAMT DECIMAL(16,2), @RATE DECIMAL(10,6)
	SELECT @TMPAMT = @OAMT
	IF @OCURRCODE = 'HKD'
	BEGIN
		SELECT @RATE = EXRATE.EXRATE FROM EXRATE (NOLOCK)
		WHERE EXRATE.TARGETCURR = @NCURRCODE
		SELECT @TMPAMT = @TMPAMT * @RATE
	END
	ELSE
	BEGIN
		SELECT @RATE = EXRATE.EXRATE FROM EXRATE (NOLOCK)
		WHERE EXRATE.SOURCECURR = @OCURRCODE
		SELECT @TMPAMT = @TMPAMT / @RATE
		
		SELECT @RATE = EXRATE.EXRATE FROM EXRATE (NOLOCK)
		WHERE EXRATE.TARGETCURR = @NCURRCODE
		SELECT @TMPAMT = @TMPAMT * @RATE
	END
	RETURN @TMPAMT
END




