﻿
CREATE                        PROCEDURE dbo.sp_RptStaffProductivityReport_ByBranch_Old
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25)

AS

DECLARE @lstr_Staffcode char(10), @lstr_BranchCode char(3), @lstr_CltCode char(6), @lnum_Cost decimal(16,2)
, @ldt_DepartDate datetime, @lstr_bkgref nvarchar(20), @lnum_PaxTotal integer, @lstr_InvType char(3)
DECLARE @FinancalYear datetime, @ldt_StartDate datetime

-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------
CREATE TABLE #TmpSellAmt
(
	CompanyCode varchar(10) null default ''
,	Branchcode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
, 	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
)

INSERT INTO #TmpSellAmt (CompanyCode, Branchcode)
SELECT distinct s.COMPANYCODE, s.branchcode 
FROM Peostaff s (nolock), #tempStaff Staff
WHERE COMPANYCODE = @COMPANY_CODE AND s.Staffcode = Staff.Staffcode


print 'Start Report at ' + convert(varchar,getdate(),109)
------------------Invoice --------------------------------------------------
SELECT 
s.Branchcode, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, cast('0' as decimal(16,2)) as cost, isnull(b.invType,'') as invType--, cast('0' as decimal(16,2)) as YTDPrice, cast('0' as decimal(16,2)) as YTDCost
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND s.companycode = a.companycode and s.staffcode = a.staffcode
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @START_DATE AND @END_DATE
AND a.staffcode = Staff.Staffcode
GROUP BY s.Branchcode, b.invType
ORDER BY 1

DECLARE lcur_inv cursor for
SELECT branchcode, sell, InvType  FROM #tmpInvTotal (nolock)
OPEN lcur_inv
FETCH lcur_inv into @lstr_BranchCode, @lnum_Cost, @lstr_InvType
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
	begin
		if @lstr_InvType <> 'DEP'
			update #TmpSellAmt set #TmpSellAmt.Price = #TmpSellAmt.Price+@lnum_Cost
			where branchcode=@lstr_BranchCode
		else
			update #TmpSellAmt set #TmpSellAmt.Dep = #TmpSellAmt.Dep+@lnum_Cost
			where branchcode=@lstr_BranchCode

	end
	else
	begin
		if @lstr_InvType <> 'DEP'
			insert into #TmpSellAmt (BranchCode, Price) 
			values
			(@lstr_Branchcode,@lnum_Cost)
		else
			insert into #TmpSellAmt (BranchCode, Dep) 
			values
			(@lstr_Branchcode,@lnum_Cost)

	end
	FETCH lcur_inv into @lstr_BranchCode, @lnum_Cost, @lstr_InvType
end

close lcur_inv
deallocate lcur_inv
drop table #tmpInvTotal 



print 'Start Pax at ' + convert(varchar,getdate(),109)
----------Pax-------------------------------------------------------
SELECT s.Branchcode, count(1) paxTotal
INTO #TmpPaxTotal
FROM peopax pax(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE pax.companycode = mstr.companycode and pax.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND pax.createon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_pax cursor for
SELECT branchcode, paxTotal  FROM #TmpPaxTotal (nolock)
OPEN lcur_pax
FETCH lcur_pax into @lstr_BranchCode, @lnum_PaxTotal
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.PaxTotal = #TmpSellAmt.PaxTotal+@lnum_PaxTotal
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, PaxTotal) 
		values
		(@lstr_Branchcode,@lnum_PaxTotal)
	FETCH lcur_pax into @lstr_BranchCode, @lnum_PaxTotal
end

close lcur_pax
deallocate lcur_pax
drop table #TmpPaxTotal

print 'Start Tkt at ' + convert(varchar,getdate(),109)
----------TKT-------------------------------------------------------
SELECT s.Branchcode, sum(tkt.netfare+tkt.totaltax) cost  
INTO #TmpTktAmt
FROM peotkt tkt(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.createon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_Tkt cursor for
SELECT branchcode, cost  FROM #TmpTktAmt (nolock)
OPEN lcur_Tkt
FETCH lcur_Tkt into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode, 0,@lnum_Cost)
	FETCH lcur_Tkt into @lstr_BranchCode, @lnum_Cost
end

close lcur_Tkt
deallocate lcur_Tkt
drop table #TmpTktAmt

print 'Start Tkt void at ' + convert(varchar,getdate(),109)
----------TKT Void-------------------------------------------------------
SELECT s.Branchcode
, sum(((tkt.netfare+tkt.totaltax) * -1)) cost  
INTO #TmpTktAmtVoid
FROM peotkt tkt(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.voidon between @START_DATE and @END_DATE
GROUP BY s.Branchcode

DECLARE lcur_TktVoid cursor for
SELECT branchcode, cost  FROM #TmpTktAmtVoid (nolock)
OPEN lcur_TktVoid
FETCH lcur_TktVoid into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
	begin
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	end
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode, 0,@lnum_Cost)
	FETCH lcur_TktVoid into @lstr_BranchCode, @lnum_Cost
end

close lcur_TktVoid
deallocate lcur_TktVoid
drop table #TmpTktAmtVoid

print 'Start Xo at ' + convert(varchar,getdate(),109)
----------xo-------------------------------------------------------
SELECT s.Branchcode, sum(xo.hkdamt) cost  
INTO #TmpxoAmt
FROM peoxo xo(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff 
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND xo.createon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_xo cursor for
SELECT branchcode, cost  FROM #TmpxoAmt (nolock)
OPEN lcur_xo
FETCH lcur_xo into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode, 0,@lnum_Cost)
	FETCH lcur_xo into @lstr_BranchCode, @lnum_Cost
end

close lcur_xo
deallocate lcur_xo
drop table #TmpxoAmt

print 'Start Xo void at ' + convert(varchar,getdate(),109)
----------xo Void-------------------------------------------------------
SELECT s.Branchcode, sum((xo.hkdamt) * -1) cost  
INTO #TmpxoAmtVoid
FROM peoxo xo(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.voidon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_xoVoid cursor for
SELECT branchcode, cost  FROM #TmpxoAmtVoid (nolock)
OPEN lcur_xoVoid
FETCH lcur_xoVoid into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode,  0,@lnum_Cost)
	fetch lcur_xoVoid into @lstr_BranchCode, @lnum_Cost
end

close lcur_xoVoid
deallocate lcur_xoVoid
drop table #TmpxoAmtVoid

print 'Start Voucher at ' + convert(varchar,getdate(),109)
----------vch-------------------------------------------------------
SELECT s.Branchcode, sum(vch.hkdamt) cost  
INTO #TmpvchAmt
FROM peovch vch(nolock), peomstr mstr (nolock), Peostaff s (nolock),#tempStaff Staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND vch.createon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_vch cursor for
SELECT branchcode, cost  FROM #TmpvchAmt (nolock)
OPEN lcur_vch
FETCH lcur_vch into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode,  0,@lnum_Cost)
	fetch lcur_vch into @lstr_BranchCode, @lnum_Cost
end

close lcur_vch
deallocate lcur_vch
drop table #TmpvchAmt

print 'Start Voucher Void at ' + convert(varchar,getdate(),109)
----------vch Void-------------------------------------------------------
SELECT s.Branchcode, sum((vch.hkdamt) * -1) cost  
INTO #TmpvchAmtVoid
FROM peovch vch(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND vch.voidon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_vchVoid cursor for
SELECT branchcode, cost  FROM #TmpvchAmtVoid (nolock)
OPEN lcur_vchVoid
FETCH lcur_vchVoid into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode,  0,@lnum_Cost)
	fetch lcur_vchVoid into @lstr_BranchCode, @lnum_Cost
end

close lcur_vchVoid
deallocate lcur_vchVoid
drop table #TmpvchAmtVoid

print 'Start Mco at ' + convert(varchar,getdate(),109)
----------mco-------------------------------------------------------
SELECT s.Branchcode, sum(mco.costamt + mco.taxamt) cost  
INTO #TmpmcoAmt
FROM peomco mco(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND mco.createon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_mco cursor for
SELECT branchcode, cost  FROM #TmpmcoAmt (nolock)
OPEN lcur_mco
FETCH lcur_mco into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode,  0,@lnum_Cost)
	fetch lcur_mco into @lstr_BranchCode, @lnum_Cost
end

close lcur_mco
deallocate lcur_mco
drop table #TmpmcoAmt

print 'Start Mco void at ' + convert(varchar,getdate(),109)
----------mco Void-------------------------------------------------------
SELECT s.Branchcode, sum((mco.costamt + mco.taxamt) * -1) cost  
INTO #TmpmcoAmtVoid
FROM peomco mco(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff 
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.voidon between @START_DATE and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_mcoVoid cursor for
SELECT branchcode, cost  FROM #TmpmcoAmtVoid (nolock)
OPEN lcur_mcoVoid
FETCH lcur_mcoVoid into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, Price, Cost) 
		values
		(@lstr_Branchcode,  0,@lnum_Cost)
	fetch lcur_mcoVoid into @lstr_BranchCode, @lnum_Cost
end

close lcur_mcoVoid
deallocate lcur_mcoVoid
drop table #TmpmcoAmtVoid

print 'Start YTD at ' + convert(varchar,getdate(),109)



-- YEAR TO DATE
SELECT 
s.Branchcode, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, b.invType
INTO #tmpYTDSelling
FROM peomstr a (nolock) ,peoinv b (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND s.companycode = a.companycode and s.staffcode = a.staffcode
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND a.staffcode = Staff.Staffcode
GROUP BY s.Branchcode, b.InvType
ORDER BY 1

DECLARE lcur_YTDSelling cursor for
SELECT branchcode, sell, invType  FROM #tmpYTDSelling (nolock)
OPEN lcur_YTDSelling
FETCH lcur_YTDSelling into @lstr_BranchCode, @lnum_Cost, @lstr_InvType
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
	begin
		if @lstr_InvType <> 'DEP'
			update #TmpSellAmt set #TmpSellAmt.YTDPrice = #TmpSellAmt.YTDPrice+@lnum_Cost
			where branchcode=@lstr_BranchCode
		else
			update #TmpSellAmt set #TmpSellAmt.YTDDep = #TmpSellAmt.YTDDep+@lnum_Cost
			where branchcode=@lstr_BranchCode
	end
	else
	begin
		if @lstr_InvType <> 'DEP'
			insert into #TmpSellAmt (BranchCode, YTDPrice) 
			values
			(@lstr_Branchcode,@lnum_Cost)
		else
			insert into #TmpSellAmt (BranchCode, YTDDep) 
			values
			(@lstr_Branchcode,@lnum_Cost)
	end
	FETCH lcur_YTDSelling into @lstr_BranchCode, @lnum_Cost, @lstr_InvType
end

close lcur_YTDSelling
deallocate lcur_YTDSelling
drop table #tmpYTDSelling 

print 'Start  TKT YTD at ' + convert(varchar,getdate(),109)

----------TKT-------------------------------------------------------
SELECT s.Branchcode, sum(tkt.netfare+tkt.totaltax) cost  
INTO #TmpYTDTktAmt
FROM peotkt tkt(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.createon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_TktYTD cursor for
SELECT branchcode, cost  FROM #TmpYTDTktAmt (nolock)
OPEN lcur_TktYTD
FETCH lcur_TktYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode, @lnum_Cost)
	FETCH lcur_TktYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_TktYTD
deallocate lcur_TktYTD
drop table #TmpYTDTktAmt

print 'Start  TKT Void YTD at ' + convert(varchar,getdate(),109)

----------TKT Void-------------------------------------------------------
SELECT s.Branchcode
, sum(((tkt.netfare+tkt.totaltax) * -1)) cost  
INTO #TmpTktAmtVoidYTD
FROM peotkt tkt(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND tkt.voidon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode

DECLARE lcur_TktVoidYTD cursor for
SELECT branchcode, cost  FROM #TmpTktAmtVoidYTD (nolock)
OPEN lcur_TktVoidYTD
FETCH lcur_TktVoidYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
	begin
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	end
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode, @lnum_Cost)
	FETCH lcur_TktVoidYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_TktVoidYTD
deallocate lcur_TktVoidYTD
drop table #TmpTktAmtVoidYTD

print 'Start  XO YTD at ' + convert(varchar,getdate(),109)

----------xo-------------------------------------------------------
SELECT s.Branchcode, sum(xo.hkdamt) cost  
INTO #TmpxoAmtYTD
FROM peoxo xo(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff 
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND xo.createon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_xoYTD cursor for
SELECT branchcode, cost  FROM #TmpxoAmtYTD (nolock)
OPEN lcur_xoYTD
FETCH lcur_xoYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode, @lnum_Cost)
	FETCH lcur_xoYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_xoYTD
deallocate lcur_xoYTD
drop table #TmpxoAmtYTD

print 'Start  XO Void YTD at ' + convert(varchar,getdate(),109)

----------xo Void-------------------------------------------------------
SELECT s.Branchcode, sum((xo.hkdamt) * -1) cost  
INTO #TmpxoAmtVoidYTD
FROM peoxo xo(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND xo.voidon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_xoVoidYTD cursor for
SELECT branchcode, cost  FROM #TmpxoAmtVoidYTD (nolock)
OPEN lcur_xoVoidYTD
FETCH lcur_xoVoidYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where branchcode=@lstr_BranchCode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode,  @lnum_Cost)
	fetch lcur_xoVoidYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_xoVoidYTD
deallocate lcur_xoVoidYTD
drop table #TmpxoAmtVoidYTD

print 'Start  VCH YTD at ' + convert(varchar,getdate(),109)

----------vch-------------------------------------------------------
SELECT s.Branchcode, sum(vch.hkdamt) cost  
INTO #TmpvchAmtYTD
FROM peovch vch(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND vch.createon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_vchYTD cursor for
SELECT branchcode, cost  FROM #TmpvchAmtYTD (nolock)
OPEN lcur_vchYTD
FETCH lcur_vchYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode,  @lnum_Cost)
	fetch lcur_vchYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_vchYTD
deallocate lcur_vchYTD
drop table #TmpvchAmtYTD

print 'Start  VCH Void YTD at ' + convert(varchar,getdate(),109)

----------vch Void-------------------------------------------------------
SELECT s.Branchcode, sum((vch.hkdamt) * -1) cost  
INTO #TmpvchAmtVoidYTD
FROM peovch vch(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND vch.voidon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_vchVoidYTD cursor for
SELECT branchcode, cost  FROM #TmpvchAmtVoidYTD (nolock)
OPEN lcur_vchVoidYTD
FETCH lcur_vchVoidYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode,  @lnum_Cost)
	fetch lcur_vchVoidYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_vchVoidYTD
deallocate lcur_vchVoidYTD
drop table #TmpvchAmtVoidYTD

print 'Start  MCO YTD at ' + convert(varchar,getdate(),109)

----------mco-------------------------------------------------------
SELECT s.Branchcode, sum(mco.costamt + mco.taxamt) cost  
INTO #TmpmcoAmtYTD
FROM peomco mco(nolock), peomstr mstr (nolock), Peostaff s (nolock), #tempStaff Staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode = Staff.Staffcode
AND mco.createon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_mcoYTD cursor for
SELECT branchcode, cost  FROM #TmpmcoAmtYTD (nolock)
OPEN lcur_mcoYTD
FETCH lcur_mcoYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode,  @lnum_Cost)
	fetch lcur_mcoYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_mcoYTD
deallocate lcur_mcoYTD
drop table #TmpmcoAmtYTD

print 'Start  MCO Void YTD at ' + convert(varchar,getdate(),109)

----------mco Void-------------------------------------------------------
SELECT s.Branchcode, sum((mco.costamt + mco.taxamt) * -1) cost  
INTO #TmpmcoAmtVoidYTD
FROM peomco mco(nolock), peomstr mstr (nolock), Peostaff s (nolock),#tempStaff Staff 
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND s.companycode = mstr.companycode and s.staffcode= mstr.staffcode
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mco.voidon between @FinancalYear and @END_DATE
GROUP BY s.Branchcode


DECLARE lcur_mcoVoidYTD cursor for
SELECT branchcode, cost  FROM #TmpmcoAmtVoidYTD (nolock)
OPEN lcur_mcoVoidYTD
FETCH lcur_mcoVoidYTD into @lstr_BranchCode, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where BranchCode = @lstr_Branchcode)
		update #TmpSellAmt set #TmpSellAmt.YTDcost = #TmpSellAmt.YTDcost+@lnum_Cost
		where BranchCode = @lstr_Branchcode
	else
		insert into #TmpSellAmt (BranchCode, YTDCost) 
		values
		(@lstr_Branchcode,  @lnum_Cost)
	fetch lcur_mcoVoidYTD into @lstr_BranchCode, @lnum_Cost
end

close lcur_mcoVoidYTD
deallocate lcur_mcoVoidYTD
drop table #TmpmcoAmtVoidYTD
-- END OF YEAR TO DATE

print 'END  YTD at ' + convert(varchar,getdate(),109)


SELECT Companycode
, branchcode as code, PaxTotal as [Total Pax]
, CASE when Price <> 0 then CONVERT(VARCHAR,Price) else '-' END as Price
, CASE when Dep <> 0 then CONVERT(VARCHAR,Dep) else '-' END as Dep
, CASE WHEN cost <> 0 THEN CONVERT(VARCHAR,cost) else '-' END AS Cost
, CASE WHEN price-cost <> 0 THEN CONVERT(VARCHAR,price-cost) ELSE '-' END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then CONVERT(VARCHAR,YTDPrice) else '-' END as YTDPrice
, CASE when YTDDep <> 0 then CONVERT(VARCHAR,YTDDep) else '-' END as YTDDep
, CASE WHEN YTDcost <> 0 THEN CONVERT(VARCHAR,YTDcost) else '-' END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN CONVERT(VARCHAR,YTDprice-YTDcost) ELSE '-' END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]
FROM #TmpSellAmt
ORDER BY branchcode


-- Drop Table
drop table #TmpSellAmt;
drop table #tempstaff;





















