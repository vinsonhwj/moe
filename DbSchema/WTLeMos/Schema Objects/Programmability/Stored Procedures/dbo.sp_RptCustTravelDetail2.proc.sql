﻿CREATE            PROCEDURE dbo.sp_RptCustTravelDetail2
@COMPANY_CODE NCHAR(4),
@CLT_CODE varchar(1000),
@START_DATE VARCHAR(10),
@END_DATE VARCHAR(25),
@EXCLUDE_CN CHAR(1)

AS

CREATE TABLE #TMPTRVLRRT
(
	CompanyCode nchar(4) null,
	BkgRef nvarchar(20) null default '',
	Invnum nvarchar(10) null,
	Seqnum int null,
	CltCode nchar(16) null,
	IssueDate datetime null,
	PaxName nvarchar(100) null default '',
	Routing nvarchar(65) null default '', 
	Class varchar(50) null default '',
	DepartDate datetime null,
	GrossTktFare decimal(16,2) null default 0.0,
	Discount decimal(16,2) null default 0.0,
	NetTktFare decimal(16,2) null default 0.0,
	ReasonCode nvarchar(6) null default '',
	COD1 nvarchar(40) null default '',
	COD2 nvarchar(40) null default '',
	COD3 nvarchar(40) null default '',
	CarTrfDesc nvarchar(200) null default '',
	CarAmt decimal(16,2) null default 0.0,
	VisaDesc nvarchar(200) null default '',
	VisaAmt decimal(16,2) null default 0.0,
	HotelDesc nvarchar(200) null default '',
	HotelAmt decimal(16,2) null default 0.0,
	RailDesc nvarchar(200) null default '',
	RailAmt decimal(16,2) null default 0.0,
	TaxTotal decimal(16,2) null default 0.0,
	TicAmt decimal(16,2) null default 0.0,
	TranCharge decimal(16,2) null default 0.0,
	MiscDesc nvarchar(200) null default '',
	MiscAmt decimal(16,2) null default 0.0,
	InvTotal decimal(16,2) null default 0.0,
	InvTax decimal(16,2) null default 0.0,
	TicketNo nvarchar(10) null default '',
	Aircode nvarchar(500) null  default '',
	FareType nvarchar(1) null default '',
	TicketType varchar(3) null default ''
	
)
-- Split Staffcode into  Temp Table
Declare @TargetCode varchar(10)

create table #TmpClient (cltcode varchar(10))
While (@CLT_CODE <> '')
Begin
	If left(@CLT_CODE,1) = '|'
	Begin
		Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE)-1))
	End
	Else
	Begin
		If @TargetCode = '' or CharIndex('|', @CLT_CODE) = 0
			Select @TargetCode=@CLT_CODE
		else
			Select @TargetCode = (Select substring(@CLT_CODE, 1, CharIndex('|', @CLT_CODE)-1))

		INSERT INTO #TmpClient VALUES (@TargetCode)

		If (Select CharIndex('|', @CLT_CODE)) > 0
			Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE) - Len(@TargetCode) - 1))
		Else
			Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE) - Len(@TargetCode)))
		
	End
End

IF @EXCLUDE_CN = 'N'
BEGIN
	INSERT INTO #TMPTRVLRRT (CompanyCode, Bkgref, Invnum, Seqnum, CltCode, IssueDate, InvTotal, InvTax, Paxname)
	SELECT inv.CompanyCode, inv.Bkgref, inv.Invnum, 1, inv.CltCode, inv.CreateOn, inv.hkdAmt, inv.Taxamt,
	(SELECT TOP 1 replace(substring(pax.paxname, 1, charindex('/', pax.paxname, charindex('/', pax.paxname) + 1) - 1), '/', ' ') as Paxname
	 FROM peoinpax pax WHERE pax.CompanyCode = inv.Companycode AND pax.invnum = inv.invnum) as PaxName
	FROM Peoinv inv, #TmpClient Clt (nolock)
	WHERE inv.cltcode = clt.cltcode
	AND inv.COMPANYCODE = @COMPANY_CODE
	AND inv.CreateOn BETWEEN @START_DATE AND @END_DATE
	AND inv.VoidOn is null
END
ELSE
BEGIN
	INSERT INTO #TMPTRVLRRT (CompanyCode, Bkgref, Invnum, Seqnum, CltCode, IssueDate, InvTotal, InvTax, Paxname)
	SELECT inv.CompanyCode, inv.Bkgref, inv.Invnum, 1, inv.CltCode, inv.CreateOn, inv.hkdAmt, inv.Taxamt,
	(SELECT TOP 1 replace(substring(pax.paxname, 1, charindex('/', pax.paxname, charindex('/', pax.paxname) + 1) - 1), '/', ' ') as Paxname
	 FROM peoinpax pax WHERE pax.CompanyCode = inv.Companycode AND pax.invnum = inv.invnum) as PaxName
	FROM Peoinv inv, #TmpClient Clt (nolock)
	WHERE inv.cltcode = clt.cltcode
	AND inv.COMPANYCODE = @COMPANY_CODE
	AND inv.CreateOn BETWEEN @START_DATE AND @END_DATE
	AND SUBSTRING(inv.invnum,2,1) = 'I'
	AND inv.VoidOn is null

END



--Start Ticket
DECLARE @lnum_Seqnum int
DECLARE @lstr_CompanyCode nchar(4)
DECLARE @lstr_paxname nvarchar(100)
DECLARE @lstr_Invnum nchar(16)
DECLARE @lstr_TmpValue nvarchar(300)
DECLARE @lstr_TempInvnum nchar(16)
DECLARE @lstr_COD1 nvarchar(40)
DECLARE @lstr_COD2 nvarchar(40)
DECLARE @lstr_COD3 nvarchar(40)
DECLARE @lstr_ReasonCode nvarchar(6)
DECLARE @lstr_Ticket nchar(10)
DECLARE @lstr_Airline nchar(500)
DECLARE @ldt_DepartOn Datetime
DECLARE @lnum_FullFare decimal(16,2)
DECLARE @lnum_NetFare decimal(16,2)
DECLARE @lnum_Discount decimal(16,2)
DECLARE @lstr_Routing nvarchar(65)
DECLARE @lstr_Class varchar(50)
DECLARE @lstr_FareType nchar(1)
DECLARE @lstr_bkgref nchar(30)
DECLARE @lnum_TaxTotal decimal(16,2)
DECLARE @lstr_CltCode nchar(16)
DECLARE @ldt_IssueDate datetime
DECLARE @lstr_TicketType varchar(3)

SET @lnum_Seqnum = 1

SELECT tkt.CompanyCode, tkt.Ticket, dbo.f_TicketAirlineMerge(tkt.companycode, tkt.ticket,tkt.tktseq)  as Airline, tkt.PaxName, inv.Invnum
, tkt.DepartOn, isnull(tkt.FullFare,0) as FullFare, isnull(tkt.PaidFareWOTax,0) as NetFare, tkt.Routing, tkt.discount
, dbo.f_TicketClassMerge(tkt.companycode, tkt.ticket,tkt.tktseq)  as class
, tkt.FareType, isnull(tkt.AirReason,'') as AirReason, tkt.COD1, tkt.COD2, tkt.COD3
, tkt.bkgref, tkt.TotalTax, inv.IssueDate, inv.CltCode, '' as TicketType
INTO #tmpTkt
FROM #TMPTRVLRRT inv, peotkt tkt (nolock), peoinvtktRef invTkt(nolock)
WHERE inv.companycode = invTkt.companycode 
AND inv.invnum = invTkt.invnum
AND tkt.CompanyCode = invTkt.CompanyCode 
AND tkt.Ticket = invTkt.Ticket
AND tkt.TktSeq = invTkt.TktSeq
AND inv.companycode = invTkt.companycode 
AND isnull(invTkt.TicketType,'') <> 'MIS'
UNION
SELECT tkt.CompanyCode, tkt.Ticket, dbo.f_TicketAirlineMerge(tkt.companycode, tkt.ticket,tkt.tktseq)  as Airline, tkt.PaxName, inv.Invnum
, tkt.DepartOn, isnull(tkt.FullFare,0) as FullFare, isnull(tkt.PaidFareWOTax,0) as NetFare, tkt.Routing, tkt.discount
, dbo.f_TicketClassMerge(tkt.companycode, tkt.ticket,tkt.tktseq)  as class, tkt.FareType, isnull(tkt.AirReason,'') as AirReason, tkt.COD1, tkt.COD2, tkt.COD3
, tkt.bkgref, tkt.TotalTax, inv.IssueDate, inv.Cltcode, '' as TicketType
FROM #TMPTRVLRRT inv, peotkt tkt (nolock), peoinvtkt invTkt(nolock)
WHERE inv.companycode = invTkt.companycode 
AND inv.invnum = invTkt.invnum
AND tkt.CompanyCode = invTkt.CompanyCode 
AND tkt.Ticket = invTkt.Ticket
AND tkt.TktSeq = invTkt.TktSeq
AND inv.companycode = invTkt.companycode 
UNION
SELECT tkt.CompanyCode, tkt.Ticket, tkt.Airline, tkt.PaxName, inv.Invnum
, tkt.DepartOn, isnull(tkt.FullFare,0) as FullFare, isnull(tkt.PaidFareWOTax,0) as NetFare, tkt.Routing, tkt.discount
, (select class from peomistktdetail detail (nolock) where companycode = tkt.companycode and ticket = tkt.ticket and tktseq = tkt.tktseq and segnum = (select min(segnum) from peotktdetail (nolock) where companycode = detail.companycode and ticket = detail
.ticket and tktseq = detail.tktseq)) as class
, tkt.FareType, isnull(tkt.AirReason,'') as AirReason, tkt.COD1, tkt.COD2, tkt.COD3
, tkt.bkgref, tkt.TotalTax, inv.IssueDate, inv.CltCode, 'MIS' as TicketType
FROM #TMPTRVLRRT inv, peomistkt tkt (nolock), peoinvtktRef invTkt(nolock)
WHERE inv.companycode = invTkt.companycode 
AND inv.invnum = invTkt.invnum
AND tkt.CompanyCode = invTkt.CompanyCode 
AND tkt.Ticket = invTkt.Ticket
AND tkt.TktSeq = invTkt.TktSeq
AND inv.companycode = invTkt.companycode 
AND isnull(invTkt.TicketType,'') = 'MIS'


DECLARE lcur_Tkt cursor for 
SELECT CompanyCode, Invnum, Ticket, Airline, PaxName, DepartOn,
FullFare, NetFare, Routing, Discount,Class, FareType, AirReason, COD1, COD2, COD3, Bkgref, TotalTax, IssueDate, CltCode, TicketType
FROM #tmpTkt
ORDER BY CompanyCode, Invnum


open lcur_Tkt
fetch lcur_Tkt into  @lstr_CompanyCode, @lstr_Invnum, @lstr_Ticket, @lstr_Airline, @lstr_PaxName, @ldt_DepartOn, @lnum_FullFare, @lnum_NetFare, @lstr_Routing, @lnum_Discount, @lstr_Class, @lstr_FareType, @lstr_ReasonCode, @lstr_COD1, @lstr_COD2, @lstr_COD3, @lstr_bkgref, @lnum_TaxTotal, @ldt_IssueDate, @lstr_CltCode, @lstr_TicketType
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF @lstr_COD1 = '' and @lstr_COD3 = '' and @lstr_COD3 = '' 
	BEGIN
		SELECT @lstr_COD1 = COD1, @lstr_COD2 = COD2, @lstr_COD3 = COD3 
		FROM peomis 
		WHERE companycode = @lstr_CompanyCode AND bkgref = @lstr_Bkgref
	END

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = TicketNo FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET TicketNo = @lstr_Ticket,
			Aircode = @lstr_Airline,
			PaxName = @lstr_PaxName,
			DepartDate = @ldt_DepartOn,
			GrossTktFare = @lnum_FullFare,
			NetTktFare = @lnum_NetFare,
			Discount = @lnum_Discount,
			Routing = @lstr_Routing,
			Class = @lstr_Class,
			FareType = @lstr_FareType,
			ReasonCode = @lstr_ReasonCode,
			COD1 = @lstr_COD1,
			COD2 = @lstr_COD2,
			COD3 = @lstr_COD3,
			TaxTotal = @lnum_TaxTotal,
			TicketType = isnull(@lstr_TicketType,'')
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Bkgref, Invnum, Seqnum, TicketNo, Aircode, PaxName,
			 DepartDate, GrossTktFare, NetTktFare, Discount, Routing,Class, FareType, ReasonCode, COD1, COD2, COD3, TaxTotal, IssueDate, CltCode, TicketType)
			VALUES
			(@lstr_CompanyCode, @lstr_Bkgref, @lstr_Invnum, @lnum_Seqnum, @lstr_Ticket, @lstr_Airline, @lstr_PaxName,
			 @ldt_DepartOn, @lnum_FullFare, @lnum_NetFare, @lnum_Discount, @lstr_Routing, @lstr_Class, @lstr_FareType,
                         @lstr_ReasonCode, @lstr_COD1, @lstr_COD2, @lstr_COD3, @lnum_TaxTotal, @ldt_IssueDate, @lstr_CltCode, @lstr_TicketType)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Bkgref, Invnum, Seqnum, TicketNo, Aircode, PaxName,
		 DepartDate, GrossTktFare, NetTktFare, Discount, Routing,Class, FareType, ReasonCode, COD1, COD2, COD3, TaxTotal, IssueDate, CltCode, TicketType)
		VALUES
		(@lstr_CompanyCode, @lstr_Bkgref, @lstr_Invnum, @lnum_Seqnum, @lstr_Ticket, @lstr_Airline, @lstr_PaxName,
		 @ldt_DepartOn, @lnum_FullFare, @lnum_NetFare, @lnum_Discount, @lstr_Routing, @lstr_Class, @lstr_FareType,
                 @lstr_ReasonCode, @lstr_COD1, @lstr_COD2, @lstr_COD3, @lnum_TaxTotal, @ldt_IssueDate, @lstr_CltCode, @lstr_TicketType)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Tkt into  @lstr_CompanyCode, @lstr_Invnum, @lstr_Ticket, @lstr_Airline, @lstr_PaxName, @ldt_DepartOn, @lnum_FullFare, @lnum_NetFare, @lstr_Routing, @lnum_Discount, @lstr_Class, @lstr_FareType, @lstr_ReasonCode, @lstr_COD1, @lstr_COD2, @lstr_COD3, @lstr_bkgref, @lnum_TaxTotal, @ldt_IssueDate, @lstr_CltCode, @lstr_TicketType
end

close lcur_Tkt
deallocate lcur_Tkt
drop table #tmpTkt
--End of Ticket


--Hotel Start
DECLARE @lstr_HotelName nvarchar(400)
DECLARE @lnum_htlAmt decimal(16,2)

SELECT htl.CompanyCode, htl.Invnum, htl.Servname as HtlName, sum(detail.HKDAmt) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref
INTO #tmpHtl
FROM #TMPTRVLRRT inv, PEOINVSEG htl (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = htl.companycode 
AND inv.invnum = htl.invnum
AND htl.CompanyCode = detail.CompanyCode 
AND htl.Invnum = detail.Invnum
AND htl.SegNum = Detail.Segnum
AND htl.SERVTYPE = 'HTL'
GROUP BY htl.CompanyCode, htl.Servname, htl.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref
UNION
SELECT htl.CompanyCode, htl.Invnum, htl.Servname as HtlName, sum(detail.HKDAmt) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref
FROM #TMPTRVLRRT inv, PEOINVSEG htl (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = htl.companycode 
AND inv.invnum = htl.invnum
AND htl.CompanyCode = detail.CompanyCode 
AND htl.Invnum = detail.Invnum
AND htl.SegNum = Detail.Segnum
AND htl.SERVTYPE = 'OTH'
AND htl.SegType = 'HTL'
GROUP BY htl.CompanyCode, htl.Servname, htl.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName , inv.bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_Htl cursor for 
SELECT CompanyCode, Invnum, HtlName, SellAmt, CltCode, IssueDate, PaxName, bkgref
FROM #tmpHtl
ORDER BY CompanyCode, Invnum


open lcur_Htl
fetch lcur_Htl into  @lstr_CompanyCode, @lstr_Invnum, @lstr_HotelName, @lnum_htlAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = HotelDesc FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET HotelDesc = @lstr_HotelName,
			HotelAmt = @lnum_htlAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, HotelDesc, HotelAmt, CltCode, IssueDate, PaxName, bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_HotelName, @lnum_htlAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, HotelDesc, HotelAmt, CltCode, IssueDate, PaxName, bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_HotelName, @lnum_htlAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)

	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Htl into  @lstr_CompanyCode, @lstr_Invnum, @lstr_HotelName, @lnum_htlAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
end

close lcur_Htl
deallocate lcur_Htl
drop table #tmpHtl
--End of Hotel


--Car Start
DECLARE @lstr_CarName nvarchar(400)
DECLARE @lnum_carAmt decimal(16,2)

SELECT car.CompanyCode, car.Invnum, car.ServDesc as carName, sum(detail.HKDAmt) as SellAmt 
, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref
INTO #tmpcar
FROM #TMPTRVLRRT inv, PEOINVSEG car (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = car.companycode 
AND inv.invnum = car.invnum
AND car.CompanyCode = detail.CompanyCode 
AND car.Invnum = detail.Invnum
AND car.SegNum = Detail.Segnum
AND car.SERVTYPE = 'OTH'
AND car.SegType = 'CAR'
GROUP BY car.CompanyCode, car.ServDesc, car.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_car cursor for 
SELECT CompanyCode, Invnum, carName, SellAmt, CltCode, IssueDate, PaxName, bkgref
FROM #tmpcar
ORDER BY CompanyCode, Invnum


open lcur_car
fetch lcur_car into  @lstr_CompanyCode, @lstr_Invnum, @lstr_CarName, @lnum_carAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = CarTrfDesc FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET CarTrfDesc = @lstr_CarName,
			CarAmt = @lnum_carAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, CarTrfDesc, CarAmt, CltCode, IssueDate, PaxName, Bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_CarName, @lnum_carAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, CarTrfDesc, CarAmt, CltCode, IssueDate, PaxName, Bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_CarName, @lnum_carAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_car into  @lstr_CompanyCode, @lstr_Invnum, @lstr_CarName, @lnum_carAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
end

close lcur_car
deallocate lcur_car
drop table #tmpcar
--End of Car

--Visa Start
DECLARE @lstr_VisaName nvarchar(400)
DECLARE @lnum_VisaAmt decimal(16,2)

SELECT Visa.CompanyCode, Visa.Invnum, Visa.ServDesc as VisaName, sum(detail.HKDAmt) as SellAmt, inv.bkgref
, inv.CltCode, inv.IssueDate, inv.PaxName 
INTO #tmpVisa
FROM #TMPTRVLRRT inv, PEOINVSEG Visa (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Visa.companycode 
AND inv.invnum = Visa.invnum
AND Visa.CompanyCode = detail.CompanyCode 
AND Visa.Invnum = detail.Invnum
AND Visa.SegNum = Detail.Segnum
AND Visa.SERVTYPE = 'OTH'
AND Visa.SegType = 'XOV'
GROUP BY Visa.CompanyCode, Visa.ServDesc, Visa.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_Visa cursor for 
SELECT CompanyCode, Invnum, VisaName, SellAmt, CltCode, IssueDate, PaxName, bkgref
FROM #tmpVisa
ORDER BY CompanyCode, Invnum


open lcur_Visa
fetch lcur_Visa into  @lstr_CompanyCode, @lstr_Invnum, @lstr_VisaName, @lnum_VisaAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = VisaDesc FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET VisaDesc = @lstr_VisaName,
			VisaAmt = @lnum_VisaAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, VisaDesc, VisaAmt, CltCode, IssueDate, PaxName, bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_VisaName, @lnum_VisaAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, VisaDesc, VisaAmt, CltCode, IssueDate, PaxName, bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_VisaName, @lnum_VisaAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Visa into  @lstr_CompanyCode, @lstr_Invnum, @lstr_VisaName, @lnum_VisaAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
end

close lcur_Visa
deallocate lcur_Visa
drop table #tmpVisa
--End of Visa


--Rail Start
DECLARE @lstr_RailName nvarchar(400)
DECLARE @lnum_RailAmt decimal(16,2)

SELECT Rail.CompanyCode, Rail.Invnum, Rail.ServDesc as RailName, sum(detail.HKDAmt) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.Bkgref
INTO #tmpRail
FROM #TMPTRVLRRT inv, PEOINVSEG Rail (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Rail.companycode 
AND inv.invnum = Rail.invnum
AND Rail.CompanyCode = detail.CompanyCode 
AND Rail.Invnum = detail.Invnum
AND Rail.SegNum = Detail.Segnum
AND Rail.SERVTYPE = 'OTH'
AND (Rail.SegType = 'RAL' OR Rail.SegType = 'MTR' OR Rail.SegType = 'SEA' OR Rail.SegType = 'SED' )
GROUP BY Rail.CompanyCode, Rail.ServDesc, Rail.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.Bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_Rail cursor for 
SELECT CompanyCode, Invnum, RailName, SellAmt, CltCode, IssueDate, PaxName, Bkgref
FROM #tmpRail
ORDER BY CompanyCode, Invnum


open lcur_Rail
fetch lcur_Rail into  @lstr_CompanyCode, @lstr_Invnum, @lstr_RailName, @lnum_RailAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = RailDesc FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET RailDesc = @lstr_RailName,
			RailAmt = @lnum_RailAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, RailDesc, RailAmt,CltCode, IssueDate, PaxName, bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_RailName, @lnum_RailAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, RailDesc, RailAmt,CltCode, IssueDate, PaxName, bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_RailName, @lnum_RailAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)	
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Rail into  @lstr_CompanyCode, @lstr_Invnum, @lstr_RailName, @lnum_RailAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
end

close lcur_Rail
deallocate lcur_Rail
drop table #tmpRail
--End of Rail


--Tic Start
DECLARE @lnum_TicAmt decimal(16,2)

SELECT Tic.CompanyCode, Tic.Invnum, sum(detail.HKDAmt) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref
INTO #tmpTic
FROM #TMPTRVLRRT inv, PEOINVSEG Tic (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Tic.companycode 
AND inv.invnum = Tic.invnum
AND Tic.CompanyCode = detail.CompanyCode 
AND Tic.Invnum = detail.Invnum
AND Tic.SegNum = Detail.Segnum
AND Tic.SERVTYPE = 'OTH'
AND Tic.SegType IN ('TIC','TIS','UTS')
GROUP BY Tic.CompanyCode, Tic.ServDesc, Tic.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_Tic cursor for 
SELECT CompanyCode, Invnum, SellAmt, CltCode, IssueDate, PaxName, bkgref
FROM #tmpTic
ORDER BY CompanyCode, Invnum


open lcur_Tic
fetch lcur_Tic into  @lstr_CompanyCode, @lstr_Invnum, @lnum_TicAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = convert(varchar,TicAmt) FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = '0.00'
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET 
			TicAmt = @lnum_TicAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, TicAmt, CltCode, IssueDate, PaxName, bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lnum_TicAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, TicAmt, CltCode, IssueDate, PaxName, bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lnum_TicAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Tic into  @lstr_CompanyCode, @lstr_Invnum, @lnum_TicAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_bkgref
end

close lcur_Tic
deallocate lcur_Tic
drop table #tmpTic
--End of Tic


--Transaction Charge Start
DECLARE @lnum_TranCharge decimal(16,2)


SELECT Txn.CompanyCode, Txn.Invnum,  sum(detail.HKDAmt) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.BkgRef
INTO #tmpTxn
FROM #TMPTRVLRRT inv, PEOINVSEG Txn (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Txn.companycode 
AND inv.invnum = Txn.invnum
AND Txn.CompanyCode = detail.CompanyCode 
AND Txn.Invnum = detail.Invnum
AND Txn.SegNum = Detail.Segnum
AND Txn.SERVTYPE = 'OTH'
AND Txn.SegType = 'TXN'
GROUP BY Txn.CompanyCode, Txn.ServDesc, Txn.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.BkgRef
union
SELECT Txn.CompanyCode, Txn.Invnum, sum(detail.amtfee) as SellAmt, inv.CltCode, inv.IssueDate, inv.PaxName, inv.BkgRef
FROM #TMPTRVLRRT inv, PEOINVSEG Txn (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Txn.companycode 
AND inv.invnum = Txn.invnum
AND Txn.CompanyCode = detail.CompanyCode 
AND Txn.Invnum = detail.Invnum
AND Txn.SegNum = Detail.Segnum
AND Txn.SERVTYPE = 'AIR'
AND Txn.SegType = 'AIR'
AND detail.amtfee > 0
GROUP BY Txn.CompanyCode, Txn.ServDesc, Txn.Invnum, inv.CltCode, inv.IssueDate, inv.PaxName, inv.BkgRef
order by 2


SET @lnum_Seqnum = 1

DECLARE lcur_Txn cursor for 
SELECT CompanyCode, Invnum, SellAmt, CltCode, IssueDate, PaxName, bkgref
FROM #tmpTxn
ORDER BY CompanyCode, Invnum


open lcur_Txn
fetch lcur_Txn into  @lstr_CompanyCode, @lstr_Invnum, @lnum_TranCharge, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = convert(varchar,TranCharge) FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = '0.00'
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET 
			TranCharge = TranCharge + @lnum_TranCharge
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, TranCharge, CltCode, IssueDate, PaxName, BkgRef)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lnum_TranCharge, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_BkgRef)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, TranCharge, CltCode, IssueDate, PaxName, BkgRef)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lnum_TranCharge, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Txn into  @lstr_CompanyCode, @lstr_Invnum, @lnum_TranCharge, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
end

close lcur_Txn
deallocate lcur_Txn
drop table #tmpTxn
--End of Transaction Charge


--Misc Start
DECLARE @lstr_MiscName nvarchar(400)
DECLARE @lnum_MiscAmt decimal(16,2)

SELECT Misc.CompanyCode, Misc.Invnum, convert(nvarchar(200), Misc.ServDesc)  as MiscName, sum(detail.HKDAmt) as SellAmt,
inv.CltCode, inv.IssueDate, inv.PaxName , inv.bkgref
INTO #tmpMisc
FROM #TMPTRVLRRT inv, PEOINVSEG Misc (nolock), peoInvdetail detail (nolock) 
WHERE inv.companycode = Misc.companycode 
AND inv.invnum = Misc.invnum
AND Misc.CompanyCode = detail.CompanyCode 
AND Misc.Invnum = detail.Invnum
AND Misc.SegNum = Detail.Segnum
AND Misc.SERVTYPE IN ('OTH','AIR')
AND Misc.SegType NOT IN ('RAL','SEA','MTR','HTL','XOV','CAR','TIC','TXN','TIS','UTS')
GROUP BY Misc.CompanyCode, Misc.ServDesc, Misc.Invnum,inv.CltCode, inv.IssueDate, inv.PaxName, inv.bkgref

SET @lnum_Seqnum = 1

DECLARE lcur_Misc cursor for 
SELECT CompanyCode, Invnum, MiscName, SellAmt, CltCode, IssueDate, PaxName, Bkgref
FROM #tmpMisc
ORDER BY CompanyCode, Invnum


open lcur_Misc
fetch lcur_Misc into  @lstr_CompanyCode, @lstr_Invnum, @lstr_MiscName, @lnum_MiscAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
while @@fetch_status = 0
begin
	
	IF @lstr_TempInvnum <> @lstr_Invnum
		SET @lnum_Seqnum = 1

	IF EXISTS(SELECT * FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum)
	BEGIN		
		SELECT @lstr_TmpValue = MiscDesc FROM #TMPTRVLRRT WHERE CompanyCode = @lstr_CompanyCode AND Invnum = @lstr_Invnum AND seqnum = @lnum_Seqnum
		IF @lstr_TmpValue = ''
		BEGIN
			UPDATE #TMPTRVLRRT	
			SET MiscDesc = @lstr_MiscName,
			MiscAmt = @lnum_MiscAmt
			WHERE CompanyCode = @lstr_CompanyCode
			AND Invnum = @lstr_Invnum
			AND Seqnum = @lnum_Seqnum

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

		END
		ELSE
		BEGIN
			SELECT @lnum_Seqnum = @lnum_Seqnum + 1

			INSERT INTO #TMPTRVLRRT
			(CompanyCode, Invnum, Seqnum, MiscDesc, MiscAmt, CltCode, IssueDate, PaxName, Bkgref)
			VALUES
			(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_MiscName, @lnum_MiscAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)

			SELECT @lnum_Seqnum = @lnum_Seqnum + 1
		END
	
	END
	ELSE
	BEGIN
		INSERT INTO #TMPTRVLRRT
		(CompanyCode, Invnum, Seqnum, MiscDesc, MiscAmt, CltCode, IssueDate, PaxName, Bkgref)
		VALUES
		(@lstr_CompanyCode, @lstr_Invnum, @lnum_Seqnum, @lstr_MiscName, @lnum_MiscAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref)
	END

	SELECT @lstr_TempInvnum = @lstr_Invnum

	fetch lcur_Misc into  @lstr_CompanyCode, @lstr_Invnum, @lstr_MiscName, @lnum_MiscAmt, @lstr_CltCode, @ldt_IssueDate, @lstr_PaxName, @lstr_Bkgref
end

close lcur_Misc
deallocate lcur_Misc
drop table #tmpMisc
--End of Misc


SELECT Invnum, bkgref, isnull(CltCode, '') as CltCode, isnull(convert(char(10),IssueDate,103),'') as IssueDate,
PaxName, Class, Routing, isnull(convert(char(10),DepartDate,103),'') as DepartDate, GrossTktFare, Discount, NetTktFare,
CarTrfDesc, CarAmt, VisaDesc, VisaAmt, HotelDesc, HotelAmt, RailDesc, RailAmt, TaxTotal, TicAmt,
TranCharge, MiscDesc, MiscAmt, InvTotal, case when TicketType = 'MIS' then '* ' + TicketNo else TicketNo end as TicketNo, 
Aircode, ReasonCode, COD1, COD2, COD3, case FareType when 'N' then 'Normal' WHEN 'S' THEN 'Special' ELSE '' END AS FareType
FROM #TMPTRVLRRT 
ORDER BY Invnum, SeqNum

DROP TABLE #TMPTRVLRRT
















