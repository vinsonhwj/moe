﻿CREATE PROCEDURE [dbo].[sp_RetrieveVoucher]
@CompanyCode varchar(20),
@VCHNUM varchar(20)
AS
BEGIN

declare @SEGNUM varchar(20)
declare @BKGREF varchar(20)

select @SEGNUM=SEGNUM from PEOVCHDETAIL where CompanyCode=@CompanyCode and VCHNUM=@VCHNUM
select @BKGREF=BKGREF from PEOVCH where CompanyCode=@CompanyCode and VCHNUM=@VCHNUM

select  COMPANYCODE,CLTCODE,CLTNAME 
from CUSTOMER 
where COMPANYCODE = @CompanyCode  order by CLTCODE asc

select companycode, bkgref, docnum, isnull(bannertype, '') as bannertype 
from DocumentBanner 
where companycode = @CompanyCode and bkgref = @BKGREF and docnum = @VCHNUM

select LOCALCURR from company where COMPANYCODE=@CompanyCode

select companycode from peomstr where companycode=@CompanyCode and BKGREF=@BKGREF

select * from peovchseg 
where vchnum = @VCHNUM and companycode = @CompanyCode and servtype not in ('HTO','HTL','HTI')

SELECT * 
FROM PEOVCH (NOLOCK) 
WHERE VCHNUM = @VCHNUM AND COMPANYCODE=@CompanyCode

Select distinct RateName 
From peovchdetail 
Where VCHNUM=@VCHNUM and CompanyCode=@CompanyCode and IsNull(RateName,'') <> ''

SELECT ISNULL(PAXNAME,'') AS PAXNAME 
FROM PEOINPAX (NOLOCK) 
WHERE COMPANYCODE=@CompanyCode AND INVNUM=@VCHNUM

SELECT ITEMNAME as roomtype,RMNTS AS RMNTS,'' AS AGETYPE,IsNull(QTY,0) as QTY,SEQNUM,
ISNULL(RATEName,'') AS SERVTYPE,ISNULL(COSTCURR,'HKD') AS COSTCURR,
ISNULL(COSTAMT,0) AS COSTAMT,ISNULL(HKDAMT,0) AS HKDAMT,isnull(taxcurr,'HKD') AS TAXCURR,
isnull(taxamt,0) AS TAXAMT  FROM PEOVCHDETAIL (NOLOCK) 
WHERE COMPANYCODE = @CompanyCode AND VCHNUM=@VCHNUM order by segnum, seqnum

SELECT Description 
FROM PeonyRef 
WHERE DocNum = @VCHNUM AND CompanyCode = @CompanyCode AND RIGHT(Description, 10) = ' - printed' 

SELECT VoidBy 
FROM PeoVch WITH(NOLOCK) 
WHERE CompanyCode = @CompanyCode AND bkgref=@BKGREF AND VchNum = @VCHNUM AND VoidOn IS NOT NULL AND VoidOn <> '' AND VoidBy IS NOT NULL AND VoidBy <> '' 

END