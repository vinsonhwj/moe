﻿
create procedure dbo.sp_RptTicketProductivityReport
@COMPANYCODE varchar(2)
,@START_DATE varchar(10)
,@END_DATE varchar(25)

as 

SELECT @END_DATE = @END_DATE + ' 23:59:59.000'

select t.issueon, left(m.teamcode,1) as [Bkg Branch], m.staffcode, left(t.teamcode,1) as [Tkt Branch], t.createby as [Tkt Agent]
, t.suppcode, m.cltode, t.bkgref, t.airline, t.aircode, t.ticket, t.paxname, t.routing, t.class
, t.fullfare, t.netfare, t.totaltax, t.tour_code, t.formpay as FOP
, t.endorsements, isnull(FareType,'') as [Ticket Type], isnull(t.tkttype,'') as [Payment Type], t.FareCalData, t.intdom as [Trip Type], t.sourcesystem as [CRS Code]
from peotkt t(nolock), peomstr m (nolock)
where t.companycode = m.companycode and t.bkgref = m.bkgref
and t.voidon is null
and t.companycode = @COMPANYCODE and t.issueon between @START_DATE AND @END_DATE
ORDER BY t.issueon

