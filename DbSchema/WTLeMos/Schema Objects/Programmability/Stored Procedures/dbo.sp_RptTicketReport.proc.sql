﻿
CREATE procedure dbo.sp_RptTicketReport
@COMPANYCODE varchar(2)
,@START_DATE varchar(10)
,@END_DATE varchar(25)

as 

SELECT @END_DATE = @END_DATE + ' 23:59:59.000'

select t.issueon, t.bkgref as booking, t.airline, t.aircode, t.ticket, t.paxname, t.routing, t.class, t.fullfare, t.paidfarewotax, t.totaltax, t.formpay as FOP
from peotkt t(nolock), peomstr m (nolock)
where t.companycode = m.companycode and t.bkgref = m.bkgref
and t.voidon is null
and t.companycode = @COMPANYCODE and t.issueon between @START_DATE AND @END_DATE
ORDER BY t.issueon

