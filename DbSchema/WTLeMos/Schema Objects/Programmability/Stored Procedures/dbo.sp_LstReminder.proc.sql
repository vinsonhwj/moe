﻿CREATE PROCEDURE dbo.sp_LstReminder
@CompanyCode varchar(2)
,@StaffCode varchar(10)

AS 

DECLARE @lstr_ViewList varchar(3)

SELECT @lstr_ViewList = viewReminder from company (nolock) WHERE companycode = @CompanyCode

print @lstr_ViewList

DECLARE @tbl_Peomstr TABLE 
( COMPANYCODE 		VARCHAR(2)
 , TEAMCODE 		VARCHAR(3)
 , STAFFCODE 		VARCHAR(10)
 , CREATEBY		VARCHAR(10)
 , BKGREF		VARCHAR(10)
 , MASTERPNR  		VARCHAR(10)
 , BOOKINGDATE 		DATETIME
 , DEPARTDATE   	DATETIME
 , INVOICE		DECIMAL(16,2)
 , DOCUMENT		DECIMAL(16,2)
 , TOTALINVCOUNT 	INT
 , TOTALDOCCOUNT 	INT
 , QINVCOUNT 		INT
 , QDOCCOUNT		INT
)

print 'M: Get Un-invoice/Un-Document List #:' + convert(varchar,getdate(),113)

INSERT INTO @tbl_Peomstr
SELECT m.Companycode, m.TeamCode, m.Staffcode, m.CreateBy, m.Bkgref, m.Masterpnr
, m.CreateOn, m.DepartDate, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as InvAmt
, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as DocAmt
, isnull(m.invcount, 0) as InvCount
, isnull(m.doccount, 0) as DocCount
, isnull(q.invcount,0) as qInvCount
, isnull(q.doccount,0) as qDocCount
FROM peomstr m (nolock), abnormalque q (nolock)
WHERE m.Companycode = q.Companycode and m.Masterpnr = q.Masterpnr
AND m.CompanyCode = @CompanyCode AND m.staffcode = @StaffCode 

--List 1 (Un-invoice)
print '1: Get Un-invoice List #:' + convert(varchar,getdate(),113)
SELECT Companycode, TeamCode, StaffCode, CreateBy, Bkgref, Masterpnr
, BookingDate, DepartDate as sdeparture
, convert(varchar(11),departdate,106) as depart
, invoice, document, totalinvcount, totaldoccount
FROM @tbl_Peomstr 
WHERE qInvCount = 0 and qDocCount > 0


--List 2 (Un-document)
print '2: Get Un-Document List #:' + convert(varchar,getdate(),113)
SELECT Companycode, TeamCode, StaffCode, CreateBy, Bkgref, Masterpnr
, BookingDate, DepartDate as sdeparture
, convert(varchar(11),departdate,106) as depart
, invoice, document, totalinvcount, totaldoccount
FROM @tbl_Peomstr 
WHERE qInvCount > 0 and qDocCount = 0

--List 3 (Profit Margin)
if substring(@lstr_ViewList,3,1) = 'M' 
begin
	print '3: Get Profit Margin List #:' + convert(varchar,getdate(),113)
	
	--Old
/*
	select bkgref ,departdate as depart from peomstr WITH (NOLOCK) 
	 where staffcode = (select staffcode from login WITH (NOLOCK) where companycode = @CompanyCode and login = @StaffCode)
	and companycode = @CompanyCode
	and MasterPNR in ( select bkgref from PeoPMQ WITH (NOLOCK) where Status <> 2 AND Status <> 4 AND Status <> 9 AND CancelBkg <> 'Y' AND companycode = N'WM')
*/	
	
	--New

	SELECT mstr.bkgref, mstr.departdate as depart
	FROM peomstr mstr(nolock), peopmq pmq(nolock)
	WHERE pmq.companycode = mstr.companycode AND pmq.bkgref = mstr.masterpnr
	AND mstr.companycode = @CompanyCode AND pmq.status not in (2,4,9)
	AND mstr.staffcode = @StaffCode AND cancelBkg <> 'Y'


end
else
begin
	print '3: Get Abnormal List #:' + convert(varchar,getdate(),113)
	
	SELECT Companycode, TeamCode, StaffCode, CreateBy, Bkgref, Masterpnr
	, BookingDate, DepartDate as sdeparture
	, convert(varchar(11),departdate,106) as depart
	, invoice, document, totalinvcount, totaldoccount
	FROM @tbl_Peomstr 
	WHERE qInvCount > 0 and qDocCount > 0
end


print 'E: End of Reminder #:' + convert(varchar,getdate(),113)




