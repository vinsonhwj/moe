﻿
CREATE PROCEDURE dbo.sp_GetTicketRouting
@CompanyCode varchar(2),
@Ticket varchar(10),
@TktSeq varchar(2),
@out_Routing varchar(100) output
AS

DECLARE @DepartCity CHAR(3), @ArrivalCity CHAR(3), 
        @TmpDes CHAR(3), @TmpStr varchar(100)

	--PRINT 'Ticket No:' + @Ticket + ', Ticket Seq = ' + @TicketSeq
	SET  @TmpStr = ''
	
	DECLARE ticketDetail CURSOR FOR
	SELECT DepartCity, ArrivalCity FROM peotktdetail
	WHERE Ticket = @Ticket AND TktSeq = @TktSeq and companycode=@CompanyCode
	ORDER BY SegNum
	
	OPEN ticketDetail
	
	FETCH NEXT FROM ticketDetail
	INTO @DepartCity, @ArrivalCity
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	--   Print 'Departure City:' + @DepartCity + ', Arrival City: ' + @ArrivalCity
	   IF @TmpStr = ''
	      BEGIN
	         SET @TmpStr = @DepartCity + '/' + @ArrivalCity
	      END
	   ELSE
	      BEGIN
	        IF rtrim(@TmpDes) = rtrim(@DepartCity)
	           BEGIN
	             SET @TmpStr = @TmpStr + '/' + @ArrivalCity
	           END
	        ELSE  
	           BEGIN
		     SET @TmpStr = @TmpStr + '-' + @DepartCity + '/' + @ArrivalCity
	           END
	        END
	
	
	   SET @TmpDes = @ArrivalCity
	   
	   -- This is executed as long as the previous fetch succeeds.
	   FETCH NEXT FROM ticketDetail
	   INTO @DepartCity, @ArrivalCity
	END
	
	--PRINT 'Routing = ' + @TmpStr
--        PRINT ''

	CLOSE ticketDetail
	DEALLOCATE ticketDetail

        select @out_Routing = Left(@TmpStr,65)

-- This is executed as long as the previous fetch succeeds.



