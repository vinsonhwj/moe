﻿
CREATE                 PROCEDURE dbo.sp_RptBookActivityByTxnDate
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@STAFF_CODE  VARCHAR(2000),
@SHOW_ZERO   CHAR(1)

AS

DECLARE @lstr_Staffcode char(10), @lstr_TeamCode char(3), @lstr_CltCode char(6), @lnum_Cost decimal(16,2)
, @ldt_DepartDate datetime, @lstr_bkgref nvarchar(20)
-- VARIABLE FOR CALCUATE PERCENTAGE
DECLARE @lnum_BkgInvTotal decimal(16,2), @lnum_BkgDocTotal  decimal(16,2), @lnum_CltCount integer

-- Split Staffcode into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpStaff (staffcode nvarchar(50) )
While (@STAFF_CODE <> '')
Begin
	If left(@STAFF_CODE,1) = '|'
	Begin
		Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@STAFF_CODE, 0, CharIndex('|', @STAFF_CODE)))
	
		If @TargetCode = ''
			Select @TargetCode=@STAFF_CODE

		INSERT INTO #TmpStaff VALUES (@TargetCode)

		If (Select CharIndex('|', @STAFF_CODE)) > 0
			Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE) - Len(@TargetCode) - 1))
		Else
			Select @STAFF_CODE = (Select Right(@STAFF_CODE, Len(@STAFF_CODE) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table


------------------Main Part --------------------------------------------------
SELECT 
a.bkgref,a.staffcode, a.teamcode,a.cltode as cltcode, a.departdate,
sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell, cast('0' as decimal(16,2)) as cost
INTO #TmpSellAmt 
FROM peomstr a (nolock) ,peoinv b (nolock), #TmpStaff staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @START_DATE AND @END_DATE
AND a.staffcode = staff.staffcode
GROUP BY a.bkgref,a.staffcode, a.teamcode,a.cltode, a.departdate
ORDER BY a.bkgref, a.staffcode, a.teamcode
--------------
--select * from #tmpSellAmt
----------------

----------TKT-------------------------------------------------------
SELECT tkt.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum(tkt.netfare+tkt.totaltax) cost  
INTO #TmpTktAmt
FROM peotkt tkt(nolock), peomstr mstr (nolock) 
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND tkt.createon between @START_DATE and @END_DATE
GROUP BY tkt.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_Tkt cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpTktAmt (nolock)
OPEN lcur_Tkt
FETCH lcur_Tkt into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_Tkt into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_Tkt
deallocate lcur_Tkt
drop table #TmpTktAmt

----------TKT Void-------------------------------------------------------
SELECT tkt.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum(((tkt.netfare+tkt.totaltax) * -1)) cost  
INTO #TmpTktAmtVoid
FROM peotkt tkt(nolock), peomstr mstr (nolock) 
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND tkt.voidon between @START_DATE and @END_DATE
GROUP BY tkt.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate

DECLARE lcur_TktVoid cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpTktAmtVoid (nolock)
OPEN lcur_TktVoid
FETCH lcur_TktVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
	begin
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	end
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_TktVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_TktVoid
deallocate lcur_TktVoid
drop table #TmpTktAmtVoid


----------xo-------------------------------------------------------
SELECT xo.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum(xo.hkdamt) cost  
INTO #TmpxoAmt
FROM peoxo xo(nolock), peomstr mstr (nolock) 
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND xo.createon between @START_DATE and @END_DATE
GROUP BY xo.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_xo cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpxoAmt (nolock)
OPEN lcur_xo
FETCH lcur_xo into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_xo into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_xo
deallocate lcur_xo
drop table #TmpxoAmt

----------xo Void-------------------------------------------------------
SELECT xo.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum((xo.hkdamt) * -1) cost  
INTO #TmpxoAmtVoid
FROM peoxo xo(nolock), peomstr mstr (nolock) 
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND xo.voidon between @START_DATE and @END_DATE
GROUP BY xo.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_xoVoid cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpxoAmtVoid (nolock)
OPEN lcur_xoVoid
FETCH lcur_xoVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_xoVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_xoVoid
deallocate lcur_xoVoid
drop table #TmpxoAmtVoid

----------vch-------------------------------------------------------
SELECT vch.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum(vch.hkdamt) cost  
INTO #TmpvchAmt
FROM peovch vch(nolock), peomstr mstr (nolock) 
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND vch.createon between @START_DATE and @END_DATE
GROUP BY vch.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_vch cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpvchAmt (nolock)
OPEN lcur_vch
FETCH lcur_vch into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_vch into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_vch
deallocate lcur_vch
drop table #TmpvchAmt

----------vch Void-------------------------------------------------------
SELECT vch.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum((vch.hkdamt) * -1) cost  
INTO #TmpvchAmtVoid
FROM peovch vch(nolock), peomstr mstr (nolock) 
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND vch.voidon between @START_DATE and @END_DATE
GROUP BY vch.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_vchVoid cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpvchAmtVoid (nolock)
OPEN lcur_vchVoid
FETCH lcur_vchVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_vchVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_vchVoid
deallocate lcur_vchVoid
drop table #TmpvchAmtVoid


----------mco-------------------------------------------------------
SELECT mco.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum(mco.costamt + mco.taxamt) cost  
INTO #TmpmcoAmt
FROM peomco mco(nolock), peomstr mstr (nolock) 
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND mco.createon between @START_DATE and @END_DATE
GROUP BY mco.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_mco cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpmcoAmt (nolock)
OPEN lcur_mco
FETCH lcur_mco into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_mco into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_mco
deallocate lcur_mco
drop table #TmpmcoAmt

----------mco Void-------------------------------------------------------
SELECT mco.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, 
mstr.departdate, sum((mco.costamt + mco.taxamt) * -1) cost  
INTO #TmpmcoAmtVoid
FROM peomco mco(nolock), peomstr mstr (nolock) 
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  in (SELECT distinct staffcode FROM #TmpStaff) 
AND mco.voidon between @START_DATE and @END_DATE
GROUP BY mco.bkgref, mstr.teamcode, mstr.staffcode, mstr.cltode, mstr.departdate


DECLARE lcur_mcoVoid cursor for
SELECT bkgref, teamcode, staffcode, cltode, departdate, cost  FROM #TmpmcoAmtVoid (nolock)
OPEN lcur_mcoVoid
FETCH lcur_mcoVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
while @@fetch_status = 0
begin

	if exists (select * from #TmpSellAmt where bkgref = @lstr_bkgref)
		update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		where bkgref=@lstr_bkgref
	else
		insert into #TmpSellAmt 
		values
		(@lstr_bkgref, @lstr_staffcode, @lstr_teamcode, @lstr_cltCode, @ldt_DepartDate, 0,@lnum_Cost)
	fetch lcur_mcoVoid into @lstr_bkgref, @lstr_teamcode, @lstr_staffcode, @lstr_cltCode, @ldt_DepartDate, @lnum_Cost
end

close lcur_mcoVoid
deallocate lcur_mcoVoid
drop table #TmpmcoAmtVoid


------- Execute Result SQL and prepare Dynamic SQL
select a.bkgref, a.teamcode, a.staffcode, a.cltcode as cltode, convert(varchar(15),departdate,106) as departdate
, a.sell as BA_TotalInv, a.cost as BA_TotalDoc
, sell-cost as BA_PMargin,case isnull(sell,0) when 0 then 0 else (convert(decimal(16,2),(sell-cost)*100/sell)) end BA_Yield
into #tmpResult
from #TmpSellAmt a
order by a.bkgref,a.teamcode

IF @SHOW_ZERO = 'Y'
BEGIN
	select a.bkgref, a.teamcode, a.staffcode, a.cltode, departdate
	, case when BA_TotalInv = 0 then '-' else convert(varchar, BA_TotalInv) end as BA_TotalInv
	, case when BA_TotalDoc = 0 then '-' else convert(varchar, BA_TotalDoc) end as BA_TotalDoc
	, case when BA_PMargin = 0 then '-' else convert(varchar, BA_PMargin) end as BA_PMargin
	, case when BA_TotalInv = 0 AND BA_TotalDoc = 0 then '-' when BA_TotalInv = 0 AND BA_TotalDoc > 0 then '-100' when BA_TotalDoc = 0 then '100.00' when BA_Yield = 0 then '-' else convert(varchar, BA_Yield) end as BA_Yield 
	from #tmpResult a
	WHERE BA_PMargin = 0
	order by a.bkgref,a.teamcode
END
ELSE
BEGIN
	select a.bkgref, a.teamcode, a.staffcode, a.cltode, departdate
	, case when BA_TotalInv = 0 then '-' else convert(varchar, BA_TotalInv) end as BA_TotalInv
	, case when BA_TotalDoc = 0 then '-' else convert(varchar, BA_TotalDoc) end as BA_TotalDoc
	, case when BA_PMargin = 0 then '-' else convert(varchar, BA_PMargin) end as BA_PMargin
	, case when BA_TotalInv = 0 AND BA_TotalDoc = 0 then '-' when BA_TotalInv = 0 AND BA_TotalDoc > 0 then '-100' when BA_TotalDoc = 0 then '100.00' when BA_Yield = 0 then '-' else convert(varchar, BA_Yield) end as BA_Yield 
	from #tmpResult a
	order by a.bkgref,a.teamcode
END

-- Drop Table
drop table #TmpSellAmt;
drop table #tmpResult;
drop table #TmpStaff;









