﻿
/*
Logic:
Select all booking that satisfy the folling criteria
1. issue AT LEAST ONE Invoice or Credit Note
2. NEVER issue document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE             PROCEDURE dbo.sp_DocumentReminderNew
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS


select 
m.companycode as companycode, 
m.teamcode as teamcode, 
m.staffcode as staffcode
, m.createby, m.bkgref, m.masterpnr
, m.createon 
, m.departdate, m.invamt, m.invtax, m.docamt, m.doctax, m.invcount, m.doccount
into #tmpDocument
from dbo.peomstr m (nolock), mstrpnrtotal ttl (nolock)
where m.companycode = ttl.companycode and m.masterpnr = ttl.masterpnr  and ttl.isLast = 'Y'
AND isnull(ttl.invcount,0) > 0 and isnull(ttl.doccount,0) = 0
AND m.companycode = @COMPANY_CODE AND m.staffcode = @STAFF_CODE AND ttl.ApproveBy is null

SELECT
ltrim(rtrim(companycode)) as companycode, 
ltrim(rtrim(teamcode)) as teamcode, 
ltrim(rtrim(staffcode)) as staffcode
, createby, bkgref, masterpnr
, createon as BookingDate
, departdate as sdeparture
, convert(char(11), departdate, 106) as depart
, Cast(isnull(invamt, .00) + isnull(invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(docamt, .00) + isnull(doctax, .00) as decimal(20,2)) as [Document]
, isnull(invcount, 0) as totalinvCount
, isnull(doccount, 0) as totaldocCount
into #tmpDocumentReminder
FROM #tmpDocument
order by teamcode, staffcode, masterpnr

IF @EXCLUDE_NOTE = 'N' 
	begin


		select * from #tmpDocumentReminder where 
	        not ( Invoice = 0 and Document = 0) 
        	and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = @COMPANY_CODE))

		DROP TABLE #tmpDocument
		DROP TABLE #tmpDocumentReminder
	end
ELSE
   --exclude note
   begin
	
		select  a.* into #tmpDocumentReminderENR from (
		SELECT 
		undoc.* 
		from #tmpDocumentReminder undoc 
		where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode)
		Union
		select  
		undoc.* 
		from #tmpDocumentReminder undoc, peoPFQue pfq
		where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref
		and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode)
		and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document < 0
		) as a  
		order by a.teamcode, a.staffcode, a.masterpnr

		select * from #tmpDocumentReminderENR where 
	        not ( Invoice = 0 and Document = 0) 
        	and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = @COMPANY_CODE))

DROP TABLE #tmpDocumentReminder
DROP TABLE #tmpDocumentReminderENR
DROP TABLE #tmpDocument
end














