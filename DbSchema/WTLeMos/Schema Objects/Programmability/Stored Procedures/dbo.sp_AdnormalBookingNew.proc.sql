﻿
CREATE          PROCEDURE dbo.sp_AdnormalBookingNew
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS

DECLARE @cStaff_Code CHAR(20);
SELECT @cStaff_Code = CONVERT(CHAR(20), @STAFF_CODE);

SELECT 
b.companycode as companycode, 
b.teamcode as teamcode, 
b.staffcode as staffcode
, b.createby, b.bkgref, b.masterpnr
, b.createon 
, b.departdate, b.invamt, b.invtax, b.docamt, b.doctax, b.invcount, b.doccount
into #tmpDocument
FROM dbo.peomstr b  (NoLock), MstrPnrTotal ttl(nolock) 
where b.CompanyCode = ttl.Companycode and b.masterpnr = ttl.masterpnr and ttl.isLast = 'Y'
AND b.companycode = @COMPANY_CODE AND b.staffcode = @cStaff_Code
--AND ((isnull(ttl.invamt,0) + isnull(ttl.invtax,0)) - (isnull(ttl.docamt,0) + isnull(ttl.doctax,0)) < 0) 
AND isnull(b.invcount,0) > 0 and isnull(b.doccount,0) > 0
AND ttl.profit < 0
order by b.teamcode, b.staffcode, b.masterpnr


SELECT
ltrim(rtrim(companycode)) as companycode, 
ltrim(rtrim(teamcode)) as teamcode, 
ltrim(rtrim(staffcode)) as staffcode
, createby, bkgref, masterpnr
, createon as BookingDate
, departdate as sdeparture
, convert(char(11), departdate, 106) as depart
, Cast(isnull(invamt, .00) + isnull(invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(docamt, .00) + isnull(doctax, .00) as decimal(20,2)) as [Document]
, isnull(invcount, 0) as totalinvCount
, isnull(doccount, 0) as totaldocCount
into #tmpAdnormalBooking
FROM #tmpDocument
order by teamcode, staffcode, masterpnr




IF @EXCLUDE_NOTE = 'N' 
	begin

		
				
		select  * from #tmpAdnormalBooking where companycode = @COMPANY_CODE and teamcode in (select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is null and viewteam is null)  
		union all  
		select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is null)  
		and branchcode in (select distinct viewbranch from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is null)  
		union all  
		select teamcode from team where   
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)  
		and branchcode in (select distinct viewbranch from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)  
		and teamcode in (select distinct viewteam from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)) 		
		
		DROP TABLE #tmpAdnormalBooking
		DROP TABLE #tmpDocument
	end
ELSE
   --exclude note
   begin
   
		select  a.* from (
		SELECT 
		ab.* 
		from #tmpAdnormalBooking ab 
		where ab.bkgref not in (select distinct bkgref from peoPFQue where companycode = ab.companycode)
		Union
		select  
		ab.* 
		from #tmpAdnormalBooking ab, peoPFQue pfq
		where ab.companycode = pfq.companycode and ab.bkgref = pfq.bkgref
		and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = ab.bkgref and pfq1.companycode = ab.companycode)
		and ( ab.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - ab.Document < 0
		) as a  
		where a.teamcode in
		(select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is null and viewteam is null)  
		union all  
		select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is null)  
		and branchcode in (select distinct viewbranch from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is null)  
		union all  
		select teamcode from team where   
		companycode in (select distinct viewcompany from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)  
		and branchcode in (select distinct viewbranch from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)  
		and teamcode in (select distinct viewteam from viewother where login = @cStaff_Code and viewbranch is not null and viewteam is not null)) 				
		order by a.teamcode, a.staffcode, a.masterpnr


		DROP TABLE #tmpAdnormalBooking
		DROP TABLE #tmpDocument
end

