﻿CREATE PROCEDURE [dbo].[sp_Rpt_Outstanding_Invoice_Report]
(
	@CompanyCode CHAR(2),
	@DueDays INT,
	@RefCodeStart VARCHAR(10) = '',
	@RefCodeEnd VARCHAR(10) = 'ZZZZZZZZZZ',
	@StaffList varchar(4000),
	@RunBy varchar(10)
)
AS
BEGIN
	IF (@RefCodeStart IS NULL)
		SELECT @RefCodeStart = '';
	
	IF (@RefCodeEnd IS NULL)
		SELECT @RefCodeEnd = 'ZZZZZZZZZZ';


	IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_VIEW' and RightsCode='VIEW')
	BEGIN
		SELECT tmp.BkgRef, tmp.InvNum, tmp.CreateOn as InvDate, tmp.hkdamt, tmp.oAmt, tmp.attn, tmp.overdue, tmp.BookingOwner, tmp.CreateBy
			FROM (
				SELECT i.BkgRef, i.InvNum, i.CreateOn, i.hkdamt,
					CASE
						WHEN r.PaidAmt IS NOT NULL THEN i.hkdamt - r.PaidAmt
						WHEN ar.oAmtPrm IS NOT NULL THEN ar.oAmtPrm
						ELSE i.hkdamt
					END AS oAmt,
					i.attn, DATEDIFF(dd, i.CreateOn, GETDATE()) AS overdue, os.StaffName AS BookingOwner, cs.StaffName AS CreateBy
				FROM peoinv i (nolock)
					INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
					LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
					LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = i.CompanyCode AND cs.StaffCode = i.CreateBy
					LEFT OUTER JOIN (
						SELECT CompanyCode, InvNum, SUM(PaidAmt) AS PaidAmt
						FROM peoinvrept (nolock)
						WHERE ISNULL(void2jba,'')=''
						GROUP BY CompanyCode, InvNum
					) r ON r.CompanyCode = i.CompanyCode AND r.InvNum = i.InvNum
					LEFT OUTER JOIN peoarref ar ON ar.CompanyCode = i.CompanyCode AND ar.InvNum = i.InvNum
				WHERE i.CompanyCode = @CompanyCode
				AND ISNULL(i.Void2JBA, '') = ''
				AND DATEDIFF(dd, i.CreateOn, GETDATE()) >= @DueDays
				AND i.CltCode BETWEEN @RefCodeStart AND @RefCodeEnd
			) tmp
			WHERE tmp.oAmt <> 0
			ORDER BY tmp.overdue DESC, tmp.CreateOn, tmp.InvNum	
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_TEAM' and RightsCode='VIEW')
		BEGIN
			SELECT tmp.BkgRef, tmp.InvNum, tmp.CreateOn as InvDate, tmp.hkdamt, tmp.oAmt, tmp.attn, tmp.overdue, tmp.BookingOwner, tmp.CreateBy
			FROM (
				SELECT i.BkgRef, i.InvNum, i.CreateOn, i.hkdamt,
					CASE
						WHEN r.PaidAmt IS NOT NULL THEN i.hkdamt - r.PaidAmt
						WHEN ar.oAmtPrm IS NOT NULL THEN ar.oAmtPrm
						ELSE i.hkdamt
					END AS oAmt,
					i.attn, DATEDIFF(dd, i.CreateOn, GETDATE()) AS overdue, os.StaffName AS BookingOwner, cs.StaffName AS CreateBy
				FROM peoinv i (nolock)
					INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
					LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
					LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = i.CompanyCode AND cs.StaffCode = i.CreateBy
					LEFT OUTER JOIN (
						SELECT CompanyCode, InvNum, SUM(PaidAmt) AS PaidAmt
						FROM peoinvrept (nolock)
						WHERE ISNULL(void2jba,'')=''
						GROUP BY CompanyCode, InvNum
					) r ON r.CompanyCode = i.CompanyCode AND r.InvNum = i.InvNum
					LEFT OUTER JOIN peoarref ar ON ar.CompanyCode = i.CompanyCode AND ar.InvNum = i.InvNum
				WHERE i.CompanyCode = @CompanyCode
				AND ISNULL(i.Void2JBA, '') = ''
				AND DATEDIFF(dd, i.CreateOn, GETDATE()) >= @DueDays
				AND i.CltCode BETWEEN @RefCodeStart AND @RefCodeEnd
				AND m.TeamCode in (Select TeamCode From Login Where CompanyCode=@CompanyCode and Login=@RunBy)
			) tmp
			WHERE tmp.oAmt <> 0
			ORDER BY tmp.overdue DESC, tmp.CreateOn, tmp.InvNum			

		END
		ELSE
		BEGIN
			SELECT tmp.BkgRef, tmp.InvNum, tmp.CreateOn as InvDate, tmp.hkdamt, tmp.oAmt, tmp.attn, tmp.overdue, tmp.BookingOwner, tmp.CreateBy
			FROM (
				SELECT i.BkgRef, i.InvNum, i.CreateOn, i.hkdamt,
					CASE
						WHEN r.PaidAmt IS NOT NULL THEN i.hkdamt - r.PaidAmt
						WHEN ar.oAmtPrm IS NOT NULL THEN ar.oAmtPrm
						ELSE i.hkdamt
					END AS oAmt,
					i.attn, DATEDIFF(dd, i.CreateOn, GETDATE()) AS overdue, os.StaffName AS BookingOwner, cs.StaffName AS CreateBy
				FROM peoinv i (nolock)
					INNER JOIN peomstr m (nolock) ON m.CompanyCode = i.CompanyCode AND m.BkgRef = i.BkgRef
					LEFT OUTER JOIN peostaff os (nolock) ON os.CompanyCode = m.CompanyCode AND os.StaffCode = m.StaffCode
					LEFT OUTER JOIN peostaff cs (nolock) ON cs.CompanyCode = i.CompanyCode AND cs.StaffCode = i.CreateBy
					LEFT OUTER JOIN (
						SELECT CompanyCode, InvNum, SUM(PaidAmt) AS PaidAmt
						FROM peoinvrept (nolock)
						WHERE ISNULL(void2jba,'')=''
						GROUP BY CompanyCode, InvNum
					) r ON r.CompanyCode = i.CompanyCode AND r.InvNum = i.InvNum
					LEFT OUTER JOIN peoarref ar ON ar.CompanyCode = i.CompanyCode AND ar.InvNum = i.InvNum
				WHERE i.CompanyCode = @CompanyCode
				AND ISNULL(i.Void2JBA, '') = ''
				AND DATEDIFF(dd, i.CreateOn, GETDATE()) >= @DueDays
				AND i.CltCode BETWEEN @RefCodeStart AND @RefCodeEnd
				AND m.StaffCode=@RunBy
			) tmp
			WHERE tmp.oAmt <> 0
			ORDER BY tmp.overdue DESC, tmp.CreateOn, tmp.InvNum			

		END
	END

	
END