﻿

CREATE procedure dbo.sp_RptAirlineSalesReportByTxnDate
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@WITH_MCO    CHAR(1)

AS

SELECT air.aircode, air.airline,
(SELECT COUNT(tkt.bkgref) FROM peotkt tkt (nolock) 
 WHERE air.airline = tkt.aircode and  tkt.companycode = @COMPANY_CODE 
 AND tkt.voidon is null 
 AND tkt.createon  between @START_DATE AND @END_DATE and 
 tkt.tktseq = (select max(tkt2.tktseq) from peotkt tkt2 (nolock) where tkt.companycode = tkt2.companycode and tkt.ticket = tkt2.ticket)
) as [TktCount],
(SELECT isnull(SUM(tkt.netfare + tkt.totaltax),0) FROM peotkt tkt  (nolock)
 WHERE air.airline = tkt.aircode and  tkt.companycode = @COMPANY_CODE 
 AND tkt.voidon is null 
 AND tkt.createon  between @START_DATE AND @END_DATE and 
 tkt.tktseq = (select max(tkt2.tktseq) from peotkt tkt2 (nolock) where tkt.companycode = tkt2.companycode and tkt.ticket = tkt2.ticket)
) as [TktAmount],
(SELECT COUNT(bkgref) FROM peomco mco (nolock)
 WHERE air.airline = mco.aircode and  mco.companycode = @COMPANY_CODE 
 AND mco.voidon is null 
 AND mco.createon  between @START_DATE AND @END_DATE and 
 mco.mcoseq = (select max(mco2.mcoseq) from peomco mco2 (nolock) where mco.companycode = mco2.companycode and mco.mconum = mco2.mconum)
) as [McoCount],
(SELECT isnull(SUM(mco.costamt + mco.taxamt),0) FROM peomco mco (nolock) 
 WHERE air.airline = mco.aircode and  mco.companycode = @COMPANY_CODE 
 AND mco.voidon is null 
 AND mco.createon  between @START_DATE AND @END_DATE and 
 mco.mcoseq = (select max(mco2.mcoseq) from peomco mco2 (nolock) where mco.companycode = mco2.companycode and mco.mconum = mco2.mconum)
) as [McoAmount]
INTO #tmpRptRtn
FROM airline air (nolock)
GROUP BY air.aircode,air.airline
ORDER BY air.aircode

IF @WITH_MCO = 'Y'
  SELECT aircode as [Air Code], airline as [Air Line],
         TktCount + McoCount as [Total Count], 0.0 as CountP, 
         TktAmount + McoAmount as [Total Amount], 0.0 as AmtP 
  FROM #tmpRptRtn
  WHERE TktCount <> 0 AND TktAmount <> 0 AND McoCount <> 0 and McoAmount <>0
ELSE
  SELECT aircode as [Air Code], airline as [Air Line],
         TktCount as [Total Count], 0.0 as CountP, 
         TktAmount as [Total Amount], 0.0 as AmtP 
  FROM #tmpRptRtn
  WHERE TktCount <> 0 AND TktAmount <> 0

DROP TABLE #tmpRptRtn



--exec sp_RptAirlineSalesReportByTxnDate 'wm','2006-09-01','2006-09-30 23:59:59.999','N'


