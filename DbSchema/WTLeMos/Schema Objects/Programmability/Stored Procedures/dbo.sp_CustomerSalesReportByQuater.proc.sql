﻿











CREATE              PROCEDURE sp_CustomerSalesReportByQuater
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@TEAM_CODE   VARCHAR(2000)

AS

DECLARE @lstr_TeamCode char(3), @lstr_CltCode char(6), @lstr_CltName nvarchar(100), @lnum_Cost decimal(16,2), @lnum_SellTotal decimal(16,2), @lnum_Sell decimal(16,2),@lnum_Month int, @lstr_bkgref nvarchar(20), @lnum_invamt decimal(16,2)
-- VARIABLE FOR CALCUATE PERCENTAGE
DECLARE @lnum_BkgInvTotal decimal(16,2), @lnum_BkgDocTotal  decimal(16,2), @lnum_CltCount integer

-- Split TeamCode into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpTeam (teamcode nvarchar(50) )
While (@TEAM_CODE <> '')
Begin
	If left(@TEAM_CODE,1) = '|'
	Begin
		Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@TEAM_CODE, 0, CharIndex('|', @TEAM_CODE)))
	
		If @TargetCode = ''
			Select @TargetCode=@TEAM_CODE

		INSERT INTO #TmpTeam VALUES (@TargetCode)

		If (Select CharIndex('|', @TEAM_CODE)) > 0
			Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE) - Len(@TargetCode) - 1))
		Else
			Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table


------------------Main Part --------------------------------------------------
SELECT 
DATEPART(MONTH,b.createon) create_mth,a.teamcode,b.cltcode,isnull(c.cltname,'No Client Name') as cltname,
sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell, cast('0' as decimal(16,2)) as cost
INTO #TmpSellAmt 
FROM peomstr a (nolock) ,peoinv b (nolock)
left outer join customer c (nolock) on b.companycode=c.companycode  AND b.cltcode=c.cltcode 
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @START_DATE AND @END_DATE
AND a.teamcode in (SELECT distinct teamcode FROM #tmpTeam)
GROUP BY datepart(month,b.createon),a.teamcode,b.cltcode,c.cltname
ORDER BY a.teamcode,b.cltcode,c.cltname,datepart(month,b.createon)

--------------
--select * from #tmpSellAmt
----------------

----------TKT-------------------------------------------------------
select distinct tkttmp.create_mth, tkttmp.bkgref, isnull(tkttmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, tkttmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = tkttmp.companycode and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpTktAmt
from
(select DATEPART(MONTH,tkt.createon) create_mth, mstr.teamcode, tkt.companycode, 
  tkt.bkgref, sum(tkt.netfare+tkt.totaltax) cost  
 from peotkt tkt(nolock), peomstr mstr (nolock) 
 WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and tkt.createon between @START_DATE and @END_DATE
 group by tkt.companycode, tkt.bkgref, mstr.teamcode, DATEPART(MONTH,tkt.createon)
) tkttmp
left outer join  peoinv i (nolock)  
on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
--Debug Start
declare @lnum_TktTotal decimal(16,2),@lnum_TktCostTotal decimal(16,2)
select @lnum_TktTotal = 0
select @lnum_TktCostTotal = 0
--Debug End
*/


DECLARE lcur_Tkt cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpTktAmt (nolock)
open lcur_Tkt
fetch lcur_Tkt into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--Debug Start
	select @lnum_TktCostTotal = @lnum_TktCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktCostTotal as varchar(20))
	--Debug End
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and  datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpTktAmt where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug Start
			/*
			select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			*/
			--Debug End

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_TktLost cursor for
			SELECT cltcode from #TmpTktAmt (nolock) where bkgref = @lstr_BkgRef
			open lcur_TktLost
			fetch lcur_TktLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_TktLost into @lstr_CltCode
			end
			close lcur_TktLost
			deallocate lcur_TktLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found

		--Debug Start 
		/*
		select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', @@This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
		*/
		--Debug End

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpTktAmt where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin
			declare @lnum_TSellAmt decimal(16,2), @lnum_TCostAmt decimal(16,2)

			--Debug Start
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			*/
			--Debug End
			
			--Debug Start
			/*
			select @lnum_TktTotal = @lnum_TktTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			*/
			--Debug End

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			--Debug Start
			/*
			select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			*/
			--Debug End
				
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_Tkt into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

close lcur_Tkt
deallocate lcur_Tkt
drop table #TmpTktAmt

--Debug Start
--select sum(cost) from #TmpSellAmt
--Debug End


----------TKT void-------------------------------------------------------
select distinct tkttmp.create_mth, tkttmp.bkgref, isnull(tkttmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, tkttmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = tkttmp.companycode and inv.bkgref = tkttmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpTktAmtVoid
from
(select DATEPART(MONTH,tkt.voidon) create_mth, mstr.teamcode, tkt.companycode, 
  tkt.bkgref, sum((tkt.netfare+tkt.totaltax) * -1) cost  
 from peotkt tkt(nolock), peomstr mstr (nolock) 
 WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and tkt.voidon between @START_DATE and @END_DATE
 group by tkt.companycode, tkt.bkgref, mstr.teamcode, DATEPART(MONTH,tkt.voidon)
) tkttmp
left outer join  peoinv i (nolock)  
on i.companycode = tkttmp.companycode and i.bkgref = tkttmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
select @lnum_TktTotal = 0
select @lnum_TktCostTotal = 0
*/

DECLARE lcur_TktVoid cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpTktAmtVoid (nolock)
open lcur_TktVoid
fetch lcur_TktVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--DEBUG
	select @lnum_TktCostTotal = @lnum_TktCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktCostTotal as varchar(20)) + 
              'Month = ' + cast(@lnum_Month as varchar(10))
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpTktAmtVoid where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug 
			/*
			select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			*/
			---------------------------------------------------------

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_TktVoidLost cursor for
			SELECT cltcode from #TmpTktAmtVoid (nolock) where bkgref = @lstr_BkgRef
			open lcur_TktVoidLost
			fetch lcur_TktVoidLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				--print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode--	+ ', Create Month = ' + cast(@lnum_Month as varchar(2))
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_TktVoidLost into @lstr_CltCode
			end
			close lcur_TktVoidLost
			deallocate lcur_TktVoidLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found
		--Debug 
		/*
		select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
		*/
		---------------------------------------------------------

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			end
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpTktAmtVoid where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin

			--Debug Mode
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			select @lnum_TktTotal = @lnum_TktTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', ++This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode	+ ', Create Month = ' + cast(@lnum_Month as varchar(10))
			*/
			---------------------------------------------------------

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			/*
			select @lnum_TktTotal = @lnum_TktTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_TktTotal as varchar(20))
			*/
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_TktVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

--Debug
--select sum(cost) from #TmpSellAmt


close lcur_TktVoid
deallocate lcur_TktVoid
drop table #TmpTktAmtVoid

----------Mco-------------------------------------------------------
select distinct Mcotmp.create_mth, Mcotmp.bkgref, isnull(Mcotmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, Mcotmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = Mcotmp.companycode and inv.bkgref = Mcotmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpMcoAmt
from
(select DATEPART(MONTH,Mco.createon) create_mth, mstr.teamcode, Mco.companycode, 
  Mco.bkgref, sum(mco.costamt + mco.taxamt) cost  
 from peoMco Mco(nolock), peomstr mstr (nolock) 
 WHERE Mco.companycode = mstr.companycode and Mco.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and Mco.createon between @START_DATE and @END_DATE
 group by Mco.companycode, Mco.bkgref, mstr.teamcode, DATEPART(MONTH,Mco.createon)
) Mcotmp
left outer join  peoinv i (nolock)  
on i.companycode = Mcotmp.companycode and i.bkgref = Mcotmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
--Debug Start
declare @lnum_McoTotal decimal(16,2),@lnum_McoCostTotal decimal(16,2)
select @lnum_McoTotal = 0
select @lnum_McoCostTotal = 0
--Debug End
*/


DECLARE lcur_Mco cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpMcoAmt (nolock)
open lcur_Mco
fetch lcur_Mco into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--Debug Start
	select @lnum_McoCostTotal = @lnum_McoCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoCostTotal as varchar(20))
	--Debug End
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and  datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpMcoAmt where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug Start
			/*
			select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			*/
			--Debug End

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_McoLost cursor for
			SELECT cltcode from #TmpMcoAmt (nolock) where bkgref = @lstr_BkgRef
			open lcur_McoLost
			fetch lcur_McoLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_McoLost into @lstr_CltCode
			end
			close lcur_McoLost
			deallocate lcur_McoLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found

		--Debug Start 
		/*
		select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', @@This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
		*/
		--Debug End

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpMcoAmt where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin
			--Debug Start
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			*/
			--Debug End
			
			--Debug Start
			/*
			select @lnum_McoTotal = @lnum_McoTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			*/
			--Debug End

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			--Debug Start
			/*
			select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			*/
			--Debug End
				
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_Mco into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

close lcur_Mco
deallocate lcur_Mco
drop table #TmpMcoAmt

--Debug Start
--select sum(cost) from #TmpSellAmt
--Debug End


----------Mco void-------------------------------------------------------
select distinct Mcotmp.create_mth, Mcotmp.bkgref, isnull(Mcotmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, Mcotmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = Mcotmp.companycode and inv.bkgref = Mcotmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpMcoAmtVoid
from
(select DATEPART(MONTH,Mco.voidon) create_mth, mstr.teamcode, Mco.companycode, 
  Mco.bkgref, sum((mco.costamt + mco.taxamt) * -1) cost  
 from peoMco Mco(nolock), peomstr mstr (nolock) 
 WHERE Mco.companycode = mstr.companycode and Mco.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and Mco.voidon between @START_DATE and @END_DATE
 group by Mco.companycode, Mco.bkgref, mstr.teamcode, DATEPART(MONTH,Mco.voidon)
) Mcotmp
left outer join  peoinv i (nolock)  
on i.companycode = Mcotmp.companycode and i.bkgref = Mcotmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
select @lnum_McoTotal = 0
select @lnum_McoCostTotal = 0
*/

DECLARE lcur_McoVoid cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpMcoAmtVoid (nolock)
open lcur_McoVoid
fetch lcur_McoVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--DEBUG
	select @lnum_McoCostTotal = @lnum_McoCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoCostTotal as varchar(20)) + 
              'Month = ' + cast(@lnum_Month as varchar(10))
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpMcoAmtVoid where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug 
			/*
			select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			*/
			---------------------------------------------------------

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_McoVoidLost cursor for
			SELECT cltcode from #TmpMcoAmtVoid (nolock) where bkgref = @lstr_BkgRef
			open lcur_McoVoidLost
			fetch lcur_McoVoidLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				--print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode--	+ ', Create Month = ' + cast(@lnum_Month as varchar(2))
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_McoVoidLost into @lstr_CltCode
			end
			close lcur_McoVoidLost
			deallocate lcur_McoVoidLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found
		--Debug 
		/*
		select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
		*/
		---------------------------------------------------------

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			end
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpMcoAmtVoid where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin

			--Debug Mode
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			select @lnum_McoTotal = @lnum_McoTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', ++This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode	+ ', Create Month = ' + cast(@lnum_Month as varchar(10))
			*/
			---------------------------------------------------------

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			/*
			select @lnum_McoTotal = @lnum_McoTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_McoTotal as varchar(20))
			*/
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_McoVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

--Debug
--select sum(cost) from #TmpSellAmt


close lcur_McoVoid
deallocate lcur_McoVoid
drop table #TmpMcoAmtVoid

----------XO-------------------------------------------------------
select distinct XOtmp.create_mth, XOtmp.bkgref, isnull(XOtmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, XOtmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = XOtmp.companycode and inv.bkgref = XOtmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpXOAmt
from
(select DATEPART(MONTH,XO.createon) create_mth, mstr.teamcode, XO.companycode, 
  XO.bkgref, sum(XO.hkdamt) cost  
 from peoXO XO(nolock), peomstr mstr (nolock) 
 WHERE XO.companycode = mstr.companycode and XO.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and XO.createon between @START_DATE and @END_DATE
 group by XO.companycode, XO.bkgref, mstr.teamcode, DATEPART(MONTH,XO.createon)
) XOtmp
left outer join  peoinv i (nolock)  
on i.companycode = XOtmp.companycode and i.bkgref = XOtmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
--Debug Start
declare @lnum_XOTotal decimal(16,2),@lnum_XOCostTotal decimal(16,2)
select @lnum_XOTotal = 0
select @lnum_XOCostTotal = 0
--Debug End
*/


DECLARE lcur_XO cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpXOAmt (nolock)
open lcur_XO
fetch lcur_XO into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--Debug Start
	select @lnum_XOCostTotal = @lnum_XOCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOCostTotal as varchar(20))
	--Debug End
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and  datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpXOAmt where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug Start
			/*
			select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			*/
			--Debug End

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_XOLost cursor for
			SELECT cltcode from #TmpXOAmt (nolock) where bkgref = @lstr_BkgRef
			open lcur_XOLost
			fetch lcur_XOLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_XOLost into @lstr_CltCode
			end
			close lcur_XOLost
			deallocate lcur_XOLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found

		--Debug Start 
		/*
		select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', @@This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
		*/
		--Debug End

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpXOAmt where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin
			--Debug Start
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			*/
			--Debug End
			
			--Debug Start
			/*
			select @lnum_XOTotal = @lnum_XOTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			*/
			--Debug End

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			--Debug Start
			/*
			select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			*/
			--Debug End
				
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_XO into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

close lcur_XO
deallocate lcur_XO
drop table #TmpXOAmt

--Debug Start
--select sum(cost) from #TmpSellAmt
--Debug End


----------XO void-------------------------------------------------------
select distinct XOtmp.create_mth, XOtmp.bkgref, isnull(XOtmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, XOtmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = XOtmp.companycode and inv.bkgref = XOtmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpXOAmtVoid
from
(select DATEPART(MONTH,XO.voidon) create_mth, mstr.teamcode, XO.companycode, 
  XO.bkgref, sum((XO.hkdamt) * -1) cost  
 from peoXO XO(nolock), peomstr mstr (nolock) 
 WHERE XO.companycode = mstr.companycode and XO.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and XO.voidon between @START_DATE and @END_DATE
 group by XO.companycode, XO.bkgref, mstr.teamcode, DATEPART(MONTH,XO.voidon)
) XOtmp
left outer join  peoinv i (nolock)  
on i.companycode = XOtmp.companycode and i.bkgref = XOtmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
select @lnum_XOTotal = 0
select @lnum_XOCostTotal = 0
*/

DECLARE lcur_XOVoid cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpXOAmtVoid (nolock)
open lcur_XOVoid
fetch lcur_XOVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--DEBUG
	select @lnum_XOCostTotal = @lnum_XOCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOCostTotal as varchar(20)) + 
              'Month = ' + cast(@lnum_Month as varchar(10))
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpXOAmtVoid where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug 
			/*
			select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			*/
			---------------------------------------------------------

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_XOVoidLost cursor for
			SELECT cltcode from #TmpXOAmtVoid (nolock) where bkgref = @lstr_BkgRef
			open lcur_XOVoidLost
			fetch lcur_XOVoidLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				--print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode--	+ ', Create Month = ' + cast(@lnum_Month as varchar(2))
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_XOVoidLost into @lstr_CltCode
			end
			close lcur_XOVoidLost
			deallocate lcur_XOVoidLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found
		--Debug 
		/*
		select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
		*/
		---------------------------------------------------------

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			end
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpXOAmtVoid where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin

			--Debug Mode
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			select @lnum_XOTotal = @lnum_XOTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', ++This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode	+ ', Create Month = ' + cast(@lnum_Month as varchar(10))
			*/
			---------------------------------------------------------

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			/*
			select @lnum_XOTotal = @lnum_XOTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_XOTotal as varchar(20))
			*/
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_XOVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

--Debug
--select sum(cost) from #TmpSellAmt


close lcur_XOVoid
deallocate lcur_XOVoid
drop table #TmpXOAmtVoid



----------Vch-------------------------------------------------------
select distinct Vchtmp.create_mth, Vchtmp.bkgref, isnull(Vchtmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, Vchtmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = Vchtmp.companycode and inv.bkgref = Vchtmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpVchAmt
from
(select DATEPART(MONTH,Vch.createon) create_mth, mstr.teamcode, Vch.companycode, 
  Vch.bkgref, sum(Vch.hkdamt) cost  
 from peoVch Vch(nolock), peomstr mstr (nolock) 
 WHERE Vch.companycode = mstr.companycode and Vch.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and Vch.createon between @START_DATE and @END_DATE
 group by Vch.companycode, Vch.bkgref, mstr.teamcode, DATEPART(MONTH,Vch.createon)
) Vchtmp
left outer join  peoinv i (nolock)  
on i.companycode = Vchtmp.companycode and i.bkgref = Vchtmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
--Debug Start
declare @lnum_VchTotal decimal(16,2),@lnum_VchCostTotal decimal(16,2)
select @lnum_VchTotal = 0
select @lnum_VchCostTotal = 0
--Debug End
*/


DECLARE lcur_Vch cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpVchAmt (nolock)
open lcur_Vch
fetch lcur_Vch into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--Debug Start
	select @lnum_VchCostTotal = @lnum_VchCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchCostTotal as varchar(20))
	--Debug End
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and  datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpVchAmt where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug Start
			/*
			select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			*/
			--Debug End

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_VchLost cursor for
			SELECT cltcode from #TmpVchAmt (nolock) where bkgref = @lstr_BkgRef
			open lcur_VchLost
			fetch lcur_VchLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_VchLost into @lstr_CltCode
			end
			close lcur_VchLost
			deallocate lcur_VchLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found

		--Debug Start 
		/*
		select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', @@This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
		*/
		--Debug End

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpVchAmt where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin

			--Debug Start
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			*/
			--Debug End
			
			--Debug Start
			/*
			select @lnum_VchTotal = @lnum_VchTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			*/
			--Debug End

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			--Debug Start
			/*
			select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			*/
			--Debug End
				
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_Vch into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

close lcur_Vch
deallocate lcur_Vch
drop table #TmpVchAmt

--Debug Start
--select sum(cost) from #TmpSellAmt
--Debug End


----------Vch void-------------------------------------------------------
select distinct Vchtmp.create_mth, Vchtmp.bkgref, isnull(Vchtmp.teamcode,'') teamcode, 
       isnull(i.cltcode,'') cltcode, Vchtmp.cost,
(select isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = Vchtmp.companycode and inv.bkgref = Vchtmp.bkgref and inv.cltcode = i.cltcode
) invamt
into #TmpVchAmtVoid
from
(select DATEPART(MONTH,Vch.voidon) create_mth, mstr.teamcode, Vch.companycode, 
  Vch.bkgref, sum((Vch.hkdamt) * -1) cost  
 from peoVch Vch(nolock), peomstr mstr (nolock) 
 WHERE Vch.companycode = mstr.companycode and Vch.bkgref = mstr.bkgref
 and mstr.teamcode  in (SELECT distinct teamcode FROM #tmpTeam) 
 and Vch.voidon between @START_DATE and @END_DATE
 group by Vch.companycode, Vch.bkgref, mstr.teamcode, DATEPART(MONTH,Vch.voidon)
) Vchtmp
left outer join  peoinv i (nolock)  
on i.companycode = Vchtmp.companycode and i.bkgref = Vchtmp.bkgref 
and DATEPART(MONTH,i.createon) =create_mth 

/*
select @lnum_VchTotal = 0
select @lnum_VchCostTotal = 0
*/

DECLARE lcur_VchVoid cursor for
select bkgref, teamcode,cltcode,cost,invamt, create_mth from #TmpVchAmtVoid (nolock)
open lcur_VchVoid
fetch lcur_VchVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt, @lnum_Month
while @@fetch_status = 0
begin
	/*
	--DEBUG
	select @lnum_VchCostTotal = @lnum_VchCostTotal + @lnum_Cost
	print '##Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchCostTotal as varchar(20)) + 
              'Month = ' + cast(@lnum_Month as varchar(10))
	*/

        -- WHEN Client Code is empty
	if @lstr_CltCode = ''
	begin
		-- Check Exist in Invoice, 
		if exists (select * from peoinv(nolock) where companycode = @COMPANY_CODE AND bkgref = @lstr_bkgref and datepart(month,createon) = @lnum_Month)		
                --CltCode found in PeoInv
		begin
			SELECT @lnum_CltCount = count(invamt), @lnum_BkgDocTotal = sum(cost) from #TmpVchAmtVoid where bkgref = @lstr_bkgref
			SELECT @lnum_Cost = @lnum_BkgDocTotal / @lnum_CltCount  --Calcuate Cost
			
			--Debug 
			/*
			select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', ##This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			*/
			---------------------------------------------------------

			--------------
			--Loop Table for Update Clt Cost
			DECLARE lcur_VchVoidLost cursor for
			SELECT cltcode from #TmpVchAmtVoid (nolock) where bkgref = @lstr_BkgRef
			open lcur_VchVoidLost
			fetch lcur_VchVoidLost into @lstr_CltCode
			while @@fetch_status = 0
			begin
				--print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode--	+ ', Create Month = ' + cast(@lnum_Month as varchar(2))
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month

				fetch lcur_VchVoidLost into @lstr_CltCode
			end
			close lcur_VchVoidLost
			deallocate lcur_VchVoidLost
                        --End of Loop
		end
		else
		--Get Cltcode from peomstr when invoice cannot be found
		--Debug 
		/*
		select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
		print 'Bkgref = ' + @lstr_bkgref + ', This @@Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
		*/
		---------------------------------------------------------

		begin
			SELECT @lstr_CltCode = cltode FROM peomstr (nolock) where companycode = @COMPANY_CODE and bkgref = @lstr_bkgref
			if exists (select * from #TmpSellAmt(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)
			begin
				update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost 
		                where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			end
			else
			begin
				select @lstr_CltName = 'No Client Code'
				select @lstr_CltName = cltname from customer where companycode = @COMPANY_CODE and cltcode = @lstr_CltCode
				insert into #TmpSellAmt values (@lnum_Month,@lstr_TeamCode,@lstr_CltCode,@lstr_CltName,0.00,@lnum_Cost)
			end	
		end
	end
	else
	--CltCode has found
	begin

	    	SELECT @lnum_BkgInvTotal = sum(invamt) from #TmpVchAmtVoid where bkgref = @lstr_bkgref and create_mth=@lnum_Month
		SELECT @lnum_BkgDocTotal = @lnum_Cost
		if @lnum_BkgInvTotal > 0 
		begin

			--Debug Mode
			/*
			select @lnum_TSellAmt = sell, @lnum_TCostAmt = cost from #TmpSellAmt
			where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
			if exists (select * from #tmpT where bkgref = @lstr_bkgref)
			begin
			print 'Client Code = ' + @lstr_CltCode
			print 'Bkg Ref = ' + @lstr_bkgref 
			print 'Sell=' + cast(@lnum_invamt as varchar(20)) + ' -- Cost = ' +  cast(@lnum_TCostAmt as varchar(20))
			print 'Bkg Invoice Total=' + cast(@lnum_BkgInvTotal as varchar(20)) + ' -- Bkg Doc Total:' +  cast(@lnum_BkgDocTotal as varchar(20))
			print 'Answer = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20))
			end
			select @lnum_VchTotal = @lnum_VchTotal + ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
			print 'Bkgref = ' + @lstr_bkgref + ', ++This Cost = ' + cast( ((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal) as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			print 'TeamCode = ' + 	@lstr_TeamCode + ', CltCode = ' + @lstr_CltCode	+ ', Create Month = ' + cast(@lnum_Month as varchar(10))
			*/
			---------------------------------------------------------

			--End of Debug Mode

			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+((@lnum_invamt/@lnum_BkgInvTotal) * @lnum_BkgDocTotal)
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end
		else
		begin
			-- Invoice Amount < 0
			/*
			select @lnum_VchTotal = @lnum_VchTotal + @lnum_Cost
			print 'Bkgref = ' + @lstr_bkgref + ', This Cost = ' + cast( @lnum_Cost as varchar(20)) + ', Accumlate Amount = ' + cast(@lnum_VchTotal as varchar(20))
			*/
			update #TmpSellAmt set #TmpSellAmt.cost = #TmpSellAmt.cost+@lnum_Cost
		        where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
		end

	end

	fetch lcur_VchVoid into @lstr_bkgref, @lstr_TeamCode,@lstr_CltCode,@lnum_Cost,@lnum_invamt,@lnum_Month
end

--Debug
--select sum(cost) from #TmpSellAmt


close lcur_VchVoid
deallocate lcur_VchVoid
drop table #TmpVchAmtVoid



---------------------------------------------------------------------------------


------- Execute Result SQL and prepare Dynamic SQL
select a.*, --c.cltname,
(select sum(sell) from #TmpSellAmt b where b.cltcode = a.cltcode) as SellTotal,
sell-cost profit,case isnull(sell,0) when 0 then 0 else ((sell-cost)*100/sell) end yield,
cast('0' as decimal(16,2)) as Cost2 
into #TmpRptResult
from #TmpSellAmt a
--left outer join customer c on c.companycode = @COMPANY_CODE and a.cltcode = c.cltcode
order by a.teamcode,a.cltcode,create_mth


declare lcur_Cost cursor for
select teamcode,cltcode,SellTotal,Cost, Sell, create_mth from #TmpRptResult (nolock)
open lcur_Cost
fetch lcur_Cost into @lstr_TeamCode,@lstr_CltCode,@lnum_SellTotal, @lnum_Cost, @lnum_Sell, @lnum_Month
while @@fetch_status = 0
begin
	if exists (select * from #TmpRptResult(nolock) where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month)

		declare @lnum_procost decimal(16,2)
		select @lnum_procost = @lnum_Cost
/*                if @lnum_SellTotal = 0
                	select @lnum_procost = 0
		else
			select @lnum_procost = (@lnum_Cost/@lnum_SellTotal)* @lnum_Sell*/

		update #TmpRptResult set 
                #TmpRptResult.cost2 = @lnum_procost 
		, #TmpRptResult.profit = #TmpRptResult.sell - @lnum_procost
		, #TmpRptResult.Yield = case isnull(#TmpRptResult.sell,0) when 0 then 0 else ((#TmpRptResult.sell-@lnum_procost)*100/#TmpRptResult.sell) end
		where teamcode=@lstr_TeamCode and cltcode=@lstr_CltCode and create_mth=@lnum_Month
	fetch lcur_Cost into @lstr_TeamCode,@lstr_CltCode,@lnum_SellTotal, @lnum_Cost, @lnum_Sell, @lnum_Month
end

close lcur_Cost
deallocate lcur_Cost

--select * from #TmpSellAmt
--select * from #TmpRptResult
------------
--select cltcode, sell, cost, cost2, SellTotal from #TmpRptResult order by cltcode
-------------

-- Variable for Dynaminc SQL
DECLARE @lstr_SelectDynamicSQL as NVARCHAR(100);
DECLARE @lstr_DynamicSQL as NVARCHAR(600);
DECLARE @lstr_DynamicSQL1 as NVARCHAR(4000);
DECLARE @lstr_DynamicSQL2 as NVARCHAR(4000);
DECLARE @lstr_FromDynamicSQL AS NVARCHAR(100);
DECLARE @lstr_Month as VARCHAR(10);
DECLARE @li_Count AS INTEGER;
DECLARE @lstr_MonthName CHAR(3);

----------------------------------------------


-- Start Dynamic SQL
SET @lstr_SelectDynamicSQL  = 'SELECT distinct cltcode as [Client Code], cltname as [Client Name]';
SET @lstr_DynamicSQL1 = '';
SET @lstr_DynamicSQL2 = '';
SET @lstr_FromDynamicSQL = ' ,''0'' AS [Total Sell], ''0'' as [Cost], ''0'' as [Profit],''0'' as [Yield] FROM #TmpRptResult a';
SET @li_Count = 1;


DECLARE lCur_Month cursor for
select DISTINCT create_mth from #TmpRptResult (nolock) order by create_mth
open lCur_Month
fetch lCur_Month into @lstr_Month
while @@fetch_status = 0
begin
	select @lstr_MonthName = UPPER(LEFT(DATENAME(MONTH,'2006-' + @lstr_Month + '-1'),3))
	SELECT @lstr_DynamicSQL = ',(SELECT isnull(sum(sell),0) FROM #TmpRptResult t WHERE t.cltcode = a.cltcode  and t.create_mth = ' + @lstr_Month +') as [' + @lstr_MonthName + ' Sell] ' +
	',(SELECT isnull(sum(cost2),0) FROM #TmpRptResult t WHERE t.cltcode = a.cltcode  and t.create_mth = ' + @lstr_Month +') as [Cost-' + @lstr_MonthName + '] ' +
	',(SELECT isnull(sum(profit),0) FROM #TmpRptResult t WHERE t.cltcode = a.cltcode  and t.create_mth = ' + @lstr_Month +') as [Profit-' + @lstr_MonthName + '] ' +
	',(SELECT isnull(sum(yield),0) FROM #TmpRptResult t WHERE t.cltcode = a.cltcode  and t.create_mth = ' + @lstr_Month +') as [Yield-' + @lstr_MonthName + '] '
	
	IF @li_Count <= 6
	BEGIN
		SELECT @lstr_DynamicSQL1 = @lstr_DynamicSQL1 + @lstr_DynamicSQL
	END
	ELSE
		SELECT @lstr_DynamicSQL2 = @lstr_DynamicSQL2 + @lstr_DynamicSQL
	
	SELECT @li_Count = @li_Count + 1;
	fetch lCur_Month into @lstr_Month
end

close lCur_Month
deallocate lCur_Month

-- Execute Dynamic SQL
EXEC(@lstr_SelectDynamicSQL + @lstr_DynamicSQL1 + @lstr_DynamicSQL2 +@lstr_FromDynamicSQL);

-- Drop Table
drop table #TmpSellAmt;
drop table #TmpRptResult;
drop table #tmpTeam;












