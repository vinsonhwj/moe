﻿








create                    PROCEDURE dbo.sp_RptCustomerSalesDetailReportByTxnDate
@COMPANY_CODE	CHAR(2),
@START_DATE  	CHAR(10),
@END_DATE    	CHAR(25),
@CLT_CODE   	VARCHAR(50)
AS



CREATE TABLE #TmpSellAmt
(
	Bkgref varchar(10) default ''
,	CltCode varchar(50) default ''
,	CltName nvarchar(200) default 'No Client Name'
,	Invamt decimal(16,2) default 0.0
,	Invcount integer default 0
,	Costamt decimal(16,2) default 0.0
)

print '## Start Report Time:' + convert(varchar,getdate(),109)
------------------Selling Part --------------------------------------------------
INSERT INTO #TmpSellAmt
SELECT bkgref, c.cltcode, c.cltname
,sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
,count(invnum) as Invcount, cast('0' as decimal(16,2)) as cost
FROM peoinv b(nolock)
LEFT OUTER JOIN Customer c (nolock) ON c.companycode = b.companycode and c.cltcode = b.cltcode
WHERE b.companycode = @COMPANY_CODE
AND b.createon BETWEEN @START_DATE AND @END_DATE
AND b.cltcode = @CLT_CODE
GROUP BY b.bkgref, c.cltcode, c.cltname

print '## Start Tkt Time:' + convert(varchar,getdate(),109)


---Cost--------------------------------------------------------------
CREATE TABLE #TmpCostAmt
(
	bkgref varchar(10) default ''
,	cltcode varchar(50) default ''
,	invTotal decimal(16,2) default 0
,	cost decimal(16,2) default 0
,	invamt decimal(16,2) default 0
)

----------TKT-------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(tkt.netfare+tkt.totaltax),0)
 from peotkt tkt (nolock)
 where tkt.companycode = i.companycode and tkt.bkgref = i.bkgref
 AND tkt.createon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode


print '## Start Loop Tkt Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE

-- Void Tkt-----------------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(((tkt.netfare+tkt.totaltax)*-1)),0)
 from peotkt tkt (nolock)
 where tkt.companycode = i.companycode and tkt.bkgref = i.bkgref
 AND tkt.voidon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode

print '## Start Loop Void Tkt Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE


----------Mco-------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(mco.costamt + mco.taxamt),0)
 from peoMco Mco (nolock)
 where Mco.companycode = i.companycode and Mco.bkgref = i.bkgref
 AND Mco.createon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode


print '## Start Loop Mco Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE

-- Void Mco-----------------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(((mco.costamt + mco.taxamt)*-1)),0)
 from peoMco Mco (nolock)
 where Mco.companycode = i.companycode and Mco.bkgref = i.bkgref
 AND Mco.voidon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode

print '## Start Loop Void Mco Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE



----------Xo-------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(xo.hkdamt),0)
 from peoXo Xo (nolock)
 where Xo.companycode = i.companycode and Xo.bkgref = i.bkgref
 AND Xo.createon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode


print '## Start Loop Xo Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE

-- Void Xo-----------------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(((xo.hkdamt)*-1)),0)
 from peoXo Xo (nolock)
 where Xo.companycode = i.companycode and Xo.bkgref = i.bkgref
 AND Xo.voidon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode

print '## Start Loop Void Xo Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE



----------Vch-------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(Vch.hkdamt),0)
 from peoVch Vch (nolock)
 where Vch.companycode = i.companycode and Vch.bkgref = i.bkgref
 AND Vch.createon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode


print '## Start Loop Vch Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE

-- Void Vch-----------------------------------------------------------------
INSERT INTO #TmpCostAmt
select distinct i.bkgref, 
       isnull(i.cltcode,'') cltcode,
(select 
 isnull(sum(case substring(inv.invnum,2,1) when 'I' then inv.hkdamt else inv.hkdamt * (-1) end),0)
 from peoinv inv (nolock)
 where inv.companycode = i.companycode 
 and inv.bkgref = i.bkgref and inv.cltcode = i.cltcode
 AND inv.createon BETWEEN @START_DATE AND @END_DATE
) as TotalInvamt,
(select isnull(sum(((Vch.hkdamt)*-1)),0)
 from peoVch Vch (nolock)
 where Vch.companycode = i.companycode and Vch.bkgref = i.bkgref
 AND Vch.voidon BETWEEN @START_DATE AND @END_DATE
) as Costamt, 
isnull(sum(case substring(i.invnum,2,1) when 'I' then i.hkdamt else i.hkdamt * (-1) end),0) as invamt
from  peoinv i (nolock)
where 
i.companycode = @COMPANY_CODE 
AND i.createon BETWEEN @START_DATE AND @END_DATE
AND i.cltcode = @CLT_CODE
group by i.COMPANYCODE, i.bkgref, i.cltcode

print '## Start Loop Void Vch Time:' + convert(varchar,getdate(),109)
exec sp_RptCustomerSalesDetailReportByTxnDate_CalCostProportion @COMPANY_CODE



DROP TABLE #TmpCostAmt;
	

SELECT bkgref as [Bkgref]
, Cltcode as [CltCode]
, CltName as [CltName]
, InvCount as [InvCount]
, CONVERT(Decimal(20,2), Invamt) as [Price], CONVERT(Decimal(20,2), CostAmt) as [Cost], CONVERT(Decimal(20,2), Invamt - CostAmt) as [Margin]
, CONVERT(Decimal(20,2), case isnull(Invamt,0) when 0 then 0 else ((Invamt-CostAmt)*100/Invamt) end) as [Yield(%)]
FROM #TmpSellAmt


-- Drop Table
DROP TABLE #TmpSellAmt;


















