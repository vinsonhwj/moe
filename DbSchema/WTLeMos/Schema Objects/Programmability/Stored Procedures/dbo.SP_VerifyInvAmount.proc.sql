﻿
CREATE procedure [dbo].[SP_VerifyInvAmount]

 @companycode nvarchar(2),
 @invnum nvarchar(10)
as

 DECLARE @Total Decimal(16,2)

 select @Total = sum((isnull(d.hkdamt,0)+isnull(d.amtfee,0)) * CASE WHEN s.SERVTYPE = 'HTL' THEN 1 ELSE isnull(d.qty,1) END)
   from peoinvseg s 
   inner join peoinvdetail d on s.invnum = d.invnum and s.companycode=d.companycode and s.segnum = d.segnum
    where s.invnum=@invnum and s.companycode=@companycode
 SELECT 1 FROM
 peoinv i
 WHERE
 i.companycode = @companycode and i.invnum = @invnum
 and i.hkdamt >= (@Total - 1) AND i.hkdamt <= (@Total + 1)