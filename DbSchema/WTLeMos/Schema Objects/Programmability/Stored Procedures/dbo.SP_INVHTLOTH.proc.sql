﻿



create PROCEDURE dbo.SP_INVHTLOTH
	@companycode varchar(2),
	@invnum varchar(10),
	@output_itin VARCHAR(50) OUTPUT
AS
BEGIN
	declare @tmpItin varchar(50)
	declare @strBuffer varchar(4)
	set @output_itin = ''
	set @tmpItin = ''
	set @strBuffer = ''
	
	declare itin_cursor CURSOR FOR
	select ltrim(rtrim(SEGTYPE)) as itin
	from peoinvseg (NOLOCK) where servtype <> 'AIR' and invnum = @invnum
	and companycode = @companycode
	order by segnum asc	
	open itin_cursor
	fetch next from itin_cursor into @tmpItin
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	-- This is executed as long as the previous fetch succeeds.
		if @tmpItin = 'CAR' or @tmpItin = 'XOV' or @tmpItin = 'HTL' or @tmpItin = 'HTI' or @tmpItin = 'HTO' or @tmpItin = 'INS'
			begin
				if @tmpItin = 'XOV' begin set @tmpItin = 'VISA' end
				if @tmpItin = 'HTI' or @tmpItin = 'HTO' or @tmpItin = 'HTL' begin set @tmpItin = 'HOTEL' end
				if @tmpItin = 'INS' begin set @tmpItin = 'INSU' end
				if @output_itin <> ''
					begin
						set @output_itin = @output_itin + '/' + @tmpItin
					end
				else
					begin
						set @output_itin = @tmpItin
					end
			end
		else
			begin
				set @strBuffer = 'MISC'
			end
		-- set @output_itin = @output_itin + '-' + @tmpItin 
   		FETCH NEXT FROM itin_cursor into @tmpItin
	END
	if @strBuffer <> ''
		begin
			if @output_itin <> ''
				begin
					set @output_itin = @output_itin + '/' + @strBuffer
				end
			else	
				begin
					set @output_itin = @strBuffer
				end
		end
	deallocate itin_cursor
END




