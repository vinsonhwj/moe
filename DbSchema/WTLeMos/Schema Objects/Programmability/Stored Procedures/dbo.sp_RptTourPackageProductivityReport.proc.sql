﻿CREATE procedure dbo.sp_RptTourPackageProductivityReport
@COMPANY_CODE varchar(2)
, @START_DATE varchar(10)
, @END_DATE varchar(25) 
, @TEAMCODE varchar(300)
, @TOURCODES varchar(1000)
, @CITYCODE varchar(3)





AS 

DECLARE @FinancalYear datetime
DECLARE @ldt_StartDate datetime


-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------



-- Split TeamCode into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpTeam (teamcode nvarchar(50) )
While (@TEAMCODE <> '')
Begin
	If left(@TEAMCODE,1) = '|'
	Begin
		Select @TEAMCODE = (Select Right(@TEAMCODE, Len(@TEAMCODE)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@TEAMCODE, 0, CharIndex('|', @TEAMCODE)))
	
		If @TargetCode = ''
			Select @TargetCode=@TEAMCODE

		INSERT INTO #TmpTeam VALUES (@TargetCode)

		If (Select CharIndex('|', @TEAMCODE)) > 0
			Select @TEAMCODE = (Select Right(@TEAMCODE, Len(@TEAMCODE) - Len(@TargetCode) - 1))
		Else
			Select @TEAMCODE = (Select Right(@TEAMCODE, Len(@TEAMCODE) - Len(@TargetCode)))
		
	End
End


if not exists (select teamcode from #TmpTeam)
begin
	insert into #TmpTeam select teamcode from team (nolock) where CompanyCode = @COMPANY_CODE
end

Declare @TargetTour varchar(50)

CREATE TABLE #TmpTour (tourcode nvarchar(50) )
While (@TOURCODES <> '')
Begin
	If left(@TOURCODES,1) = '|'
	Begin
		Select @TOURCODES = (Select Right(@TOURCODES, Len(@TOURCODES)-1))
	End
	Else
	Begin
		Select @TargetTour = (Select substring(@TOURCODES, 0, CharIndex('|', @TOURCODES)))
	
		If @TargetTour = ''
			Select @TargetTour=@TOURCODES

		INSERT INTO #TmpTour VALUES (@TargetTour)

		If (Select CharIndex('|', @TOURCODES)) > 0
			Select @TOURCODES = (Select Right(@TOURCODES, Len(@TOURCODES) - Len(@TargetTour) - 1))
		Else
			Select @TOURCODES = (Select Right(@TOURCODES, Len(@TOURCODES) - Len(@TargetTour)))
		
	End
End


CREATE TABLE #TmpMstr
(
	CompanyCode varchar(2) not null 
,	Bkgref varchar(10) not null
,	SourceSystem varchar(20) not null
,	StaffCode varchar(10) not null
,	TeamCode varchar(10) not null
,	TourCode varchar(100) not null
)


print 'Start Retrieve Peomstr at ' + convert(varchar,getdate(),109)

if not exists (SELECT tourcode FROM #TmpTour)
begin
	insert into #TmpMstr
	SELECT mstr.companycode, mstr.bkgref, mstr.SourceSystem, mstr.StaffCode, Mstr.teamcode, mstr.TourCode
	FROM peomstr mstr(nolock), peoinv inv (nolock),#tmpTeam Team  
	WHERE inv.CompanyCode = mstr.CompanyCode and inv.Bkgref = mstr.Bkgref
	AND mstr.companycode = @COMPANY_CODE
	AND mstr.Teamcode = Team.TeamCode
	AND inv.CreateOn between @START_DATE AND @END_DATE
	AND mstr.TourCode <> ''
end
else
begin
	insert into #TmpMstr
	SELECT mstr.companycode, mstr.bkgref, mstr.SourceSystem, mstr.StaffCode, Mstr.teamcode, mstr.TourCode
	FROM peomstr mstr(nolock), peoinv inv (nolock),#tmpTeam Team, #tmpTour tour
	WHERE inv.CompanyCode = mstr.CompanyCode and inv.Bkgref = mstr.Bkgref
	AND mstr.companycode = @COMPANY_CODE
	AND mstr.Teamcode = Team.TeamCode
	AND mstr.Tourcode = tour.TourCode
	AND inv.CreateOn between @START_DATE AND @END_DATE
	AND mstr.TourCode <> ''
end


select distinct CompanyCode, tourcode into #TmpMstrTourCode from #TmpMstr

print 'Get Retrieve YTD Booking for the Tour at ' + convert(varchar,getdate(),109)

delete from #TmpMstr

insert into #TmpMstr
SELECT distinct mstr.companycode, mstr.bkgref, mstr.SourceSystem, mstr.StaffCode, Mstr.teamcode, mstr.TourCode
FROM peomstr mstr(nolock), peoinv inv (nolock),#tmpTeam Team, #TmpMstrTourCode tour
WHERE inv.CompanyCode = mstr.CompanyCode and inv.Bkgref = mstr.Bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.Teamcode = Team.TeamCode
AND mstr.Tourcode = tour.TourCode
AND inv.CreateOn between @FinancalYear AND @END_DATE
AND mstr.TourCode <> ''


print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT a.companycode, a.teamcode, a.bkgref, a.TourCode, a.staffcode, b.invnum, b.createon
into #tmpInvoice
FROM #tmpMstr a (nolock) ,peoinv b (nolock)
WHERE b.companycode=a.companycode AND b.bkgref=a.bkgref 
AND b.createon BETWEEN @FinancalYear AND @END_DATE



SELECT 
a.companycode, a.teamcode, a.bkgref, a.staffcode, a.TourCode, a.invnum, a.createon, case substring(a.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM #tmpInvoice a (nolock) ,peoinv b (nolock)
WHERE a.companycode=b.companycode AND a.invnum=b.invnum
ORDER BY 1, 2


drop table #tmpInvoice


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Teamcode varchar(10) not null
,	TourCode varchar(50) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)


--Ticket
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.TourCode, tkt.Ticket, tkt.tktseq, tkt.issueon, tkt.voidon
into #tmpTicket
FROM peotkt tkt(nolock), #tmpMstr mstr (nolock)
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND tkt.issueon between @FinancalYear AND @END_DATE


insert into #tmpDoc
select mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.TourCode, mstr.Ticket, mstr.issueon,tkt.netfare+tkt.totaltax cost
, 'TKT', case when mstr.voidon is null then 'N' else 'Y' end
from #tmpTicket mstr, peotkt tkt (nolock)
where tkt.companycode = mstr.companycode and tkt.ticket = mstr.ticket and tkt.tktseq = mstr.tktseq

drop table #tmpTicket

SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.TourCode, tkt.Ticket, tkt.tktseq, tkt.issueon, tkt.voidon
into #tmpTicketVoid
FROM peotkt tkt(nolock), #tmpMstr mstr (nolock)
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND tkt.voidon between @FinancalYear AND @END_DATE


insert into #tmpDoc
select mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.TourCode, mstr.Ticket, mstr.voidon, (tkt.netfare+tkt.totaltax) * -1 cost
, 'TKT', case when mstr.voidon is null then 'N' else 'Y' end
from #tmpTicketVoid mstr, peotkt tkt (nolock)
where tkt.companycode = mstr.companycode and tkt.ticket = mstr.ticket and tkt.tktseq = mstr.tktseq

drop table #tmpTicketVoid

print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, xo.xonum, xo.createon, xo.voidon
into #tmpXo
FROM peoxo xo(nolock), #tmpMstr mstr (nolock)
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND xo.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.xonum, mstr.createon, xo.hkdamt cost  , 'XO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpXo mstr (nolock), peoxo xo (nolock)
where xo.companycode = mstr.companycode and xo.xonum = mstr.xonum

drop table #tmpXo

SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, xo.xonum, xo.createon, xo.voidon
into #tmpXoVoid
FROM peoxo xo(nolock), #tmpMstr mstr (nolock)
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND xo.voidon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.xonum, mstr.voidon, xo.hkdamt * -1 cost  , 'XO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpXoVoid mstr (nolock), peoxo xo (nolock)
where xo.companycode = mstr.companycode and xo.xonum = mstr.xonum

drop table #tmpXoVoid


print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, vch.vchnum, vch.createon, vch.voidon
into #tmpVoucher
FROM peovch vch(nolock), #tmpMstr mstr (nolock)
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND vch.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.vchnum, mstr.createon, vch.hkdamt cost  , 'VCH'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpVoucher mstr (nolock), peovch vch (nolock)
where vch.companycode = mstr.companycode and vch.vchnum = mstr.vchnum

drop table #tmpVoucher


SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, vch.vchnum, vch.createon, vch.voidon
into #tmpVoucherVoid
FROM peovch vch(nolock), #tmpMstr mstr (nolock)
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND vch.voidon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.vchnum, mstr.voidon, vch.hkdamt * -1 cost  , 'VCH'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpVoucherVoid mstr (nolock), peovch vch (nolock)
where vch.companycode = mstr.companycode and vch.vchnum = mstr.vchnum

drop table #tmpVoucherVoid

print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mco.mconum, mco.mcoseq, mco.createon, mco.voidon
into #tmpMco
FROM peomco mco(nolock), #tmpMstr mstr (nolock)
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mco.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.mconum, mstr.createon, mco.costamt + mco.taxamt cost  , 'MCO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpMco mstr (nolock), peomco mco (nolock)
where mco.companycode = mstr.companycode and mco.mconum = mstr.mconum and mco.mcoseq = mstr.mcoseq

drop table #tmpMco

SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mco.mconum, mco.mcoseq, mco.createon, mco.voidon
into #tmpMcoVoid
FROM peomco mco(nolock), #tmpMstr mstr (nolock)
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mco.voidon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.TourCode, mstr.mconum, mstr.voidon, (mco.costamt + mco.taxamt) * -1 cost  , 'MCO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpMcoVoid mstr (nolock), peomco mco (nolock)
where mco.companycode = mstr.companycode and mco.mconum = mstr.mconum and mco.mcoseq = mstr.mcoseq

drop table #tmpMcoVoid

print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	TeamCode nvarchar(50) null default ''
,	TourCode nvarchar(50) null default ''
,	TourDesc nvarchar(200) null default ''
,	CityCode1 varchar(3) null default ''
,	CityCode2 varchar(3) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
)

if @CITYCODE = ''
begin
	insert into #TmpSellAmt
	(Companycode, teamcode, TourCode, TourDesc, CityCode1, CityCode2)
	select distinct m.Companycode, m.teamcode, m.TourCode, isnull(t.TourDesc,''), isnull(t.CityCode1,''), isnull(t.CityCode2,'')
	FROM #TmpMstr m
	left outer join tourcode t (nolock) on t.CompanyCode = m.CompanyCode and t.tourcode = m.TourCode
end
else
begin
	insert into #TmpSellAmt
	(Companycode, teamcode, TourCode, TourDesc, CityCode1, CityCode2)
	select distinct m.Companycode, m.teamcode, m.TourCode, isnull(t.TourDesc,''), isnull(t.CityCode1,''), isnull(t.CityCode2,'')
	FROM #TmpMstr m
	left outer join tourcode t (nolock) on t.CompanyCode = m.CompanyCode and t.tourcode = m.TourCode
	WHERE t.TourDesc like '%' + @CITYCODE + '%'

end
--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, TourCode, Sum(Sell) as Sell
From #TmpInvTotal 
Group By CompanyCode, TeamCode, TourCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.TourCode = t.TourCode



--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, TourCode, Sum(Sell) as Sell
From #TmpInvTotal 
WHERE createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, TourCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.TourCode = t.TourCode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, TourCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, TeamCode, TourCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.TourCode = t.TourCode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, TourCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, TourCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.TourCode = t.TourCode

SELECT distinct CompanyCode, Bkgref, TEamCode, TourCode into #tmpMTDMstr FROM #tmpInvTotal

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, inv.TourCode, count(1) paxcount
FROM peopax pax(nolock), #tmpMTDMstr inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref and pax.sourcesystem <> 'PEONYHOTEL'
GROUP BY inv.companycode, inv.Teamcode, inv.TourCode ) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode and i.TourCode = t.TourCode



SELECT rpt.TourCode as [Tour Code], rpt.TourDesc as [Description], rpt.CityCode1 as [City Code 1], rpt.CityCode2 as [City Code 1], rpt.TeamCode as [Team Code], PaxTotal as [Pax Count]
, CASE when Price <> 0 then Price else 0 END as Price
, CASE WHEN cost <> 0 THEN cost else 0 END AS Cost
, CASE WHEN price-cost <> 0 THEN price-cost ELSE 0 END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then YTDPrice else 0 END as [YTD Price]
, CASE WHEN YTDcost <> 0 THEN YTDcost else 0 END AS [YTD Cost]
, CASE WHEN YTDprice-YTDcost <> 0 THEN YTDprice-YTDcost ELSE 0 END as [YTD Margin]
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTD Yield(%)]
FROM #TmpSellAmt Rpt
ORDER BY rpt.TourCode, rpt.TeamCode



-- Drop Table

drop table #TmpSellAmt;
drop table #tmpMstr;
DROP TABLE #TmpTour;
DROP TABLE #TmpTeam;
DROP TABLE #TmpMstrTourCode;
DROP TABLE #tmpMTDMstr;







