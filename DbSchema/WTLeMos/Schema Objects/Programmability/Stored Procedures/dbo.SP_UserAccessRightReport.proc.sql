﻿



Create  PROCEDURE dbo.SP_UserAccessRightReport
@CompanyCode nchar(2),
@ModuleCode nvarchar(12),
@Login nvarchar(12),
@TeamCode  nchar(7),
@GroupID nchar(3),
@Suspend nchar(1)
AS
BEGIN
--Declare @MC varchar(20)
--Declare @LN varchar(20)
select @ModuleCode = '%' + ltrim(rtrim(@ModuleCode)) + '%'
select @Login = '%' + ltrim(rtrim(@Login)) + '%'
select @TeamCode = '%' + ltrim(rtrim(@TeamCode)) + '%'
select @GroupID = '%' + ltrim(rtrim(@GroupID)) + '%'
If @Suspend = 'Y' 
select c.companycode, m.modulecode, m.moduledesc, a.login, l.teamcode,l.groupid,
'status' = case
when((l.effectiveon <= getdate() or l.effectiveon is null) and l.islock = 'N'  and (l.suspendon > getdate() or l.suspendon is null))  then 'Active' 
else 'InActive' end, 
(select staffname from peostaff where staffcode = l.staffcode and companycode = c.companycode) as Staffname,
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='ADD' and accessrights='1') As 'ADD',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEW' and accessrights='1' ) As 'VIEW',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MDY' and accessrights='1' ) As 'MDY',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='DEL' and accessrights='1') As 'DEL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VNET' and accessrights='1') As 'VNET',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VSELL' and accessrights='1' ) As 'VSELL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MNET' and accessrights='1' ) As 'MNET',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MSELL' and accessrights='1' ) As 'MSELL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VRATE' and accessrights='1' ) As 'VRATE',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='PTSEG' and accessrights='1') As 'PTSEG',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='ISSUE' and accessrights='1' ) As 'ISSUE',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VOID' and accessrights='1' ) As 'VOID',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEWA' and accessrights='1') As 'VIEWA',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='AIYA' and accessrights='1' ) As 'AIYA',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='HTMLP' and accessrights='1' ) As 'HTMLP',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEWL' and accessrights='1' ) As 'VIEWL'
 from module m
 inner join companymodule c on m.modulecode = c.modulecode
left outer join access a on a.companycode = c.companycode
right outer join login l on l.companycode = a.companycode and l.login = a.login
where c.companycode = @CompanyCode and c.modulecode like @ModuleCode and a.accessrights='1' and l.login like @Login and l.teamcode like ltrim(rtrim(@TeamCode))
and l.groupid like ltrim(rtrim(@GroupID)) --and ((l.effectiveon <= getdate() or l.effectiveon is null) and l.islock = 'N'  and (l.suspendon > getdate() or l.suspendon is null))
group by c.companycode, m.modulecode, m.moduledesc, a.login, l.teamcode, l.groupid, l.staffcode, l.effectiveon, l.islock, l.suspendon
order by a.login, c.companycode, m.modulecode, m.moduledesc
If @Suspend = 'N'
select c.companycode, m.modulecode, m.moduledesc, a.login, l.teamcode,l.groupid,
'status' = case
when((l.effectiveon <= getdate() or l.effectiveon is null) and l.islock = 'N'  and (l.suspendon > getdate() or l.suspendon is null))  then 'Active' 
else 'InActive' end, 
(select staffname from peostaff where staffcode = l.staffcode and companycode = c.companycode) as Staffname,
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='ADD' and accessrights='1') As 'ADD',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEW' and accessrights='1' ) As 'VIEW',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MDY' and accessrights='1' ) As 'MDY',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='DEL' and accessrights='1') As 'DEL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VNET' and accessrights='1') As 'VNET',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VSELL' and accessrights='1' ) As 'VSELL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MNET' and accessrights='1' ) As 'MNET',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='MSELL' and accessrights='1' ) As 'MSELL',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VRATE' and accessrights='1' ) As 'VRATE',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='PTSEG' and accessrights='1') As 'PTSEG',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='ISSUE' and accessrights='1' ) As 'ISSUE',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VOID' and accessrights='1' ) As 'VOID',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEWA' and accessrights='1') As 'VIEWA',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='AIYA' and accessrights='1' ) As 'AIYA',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='HTMLP' and accessrights='1' ) As 'HTMLP',
(select count(*) from access where login = a.login and companycode = c.companycode and modulecode=m.modulecode and RIGHTSCODE='VIEWL' and accessrights='1' ) As 'VIEWL'
 from module m
inner join companymodule c on m.modulecode = c.modulecode
left outer join access a on a.companycode = c.companycode
right outer join login l on l.companycode = a.companycode and l.login = a.login
where c.companycode = @CompanyCode and c.modulecode like @ModuleCode and a.accessrights='1' and l.login like @Login and l.teamcode like ltrim(rtrim(@TeamCode))
and l.groupid like ltrim(rtrim(@GroupID)) and ((l.effectiveon <= getdate() or l.effectiveon is null) and l.islock = 'N'  and (l.suspendon > getdate() or l.suspendon is null))
group by c.companycode, m.modulecode, m.moduledesc, a.login, l.teamcode, l.groupid, l.staffcode, l.effectiveon, l.islock, l.suspendon
order by a.login, c.companycode, m.modulecode, m.moduledesc
END




