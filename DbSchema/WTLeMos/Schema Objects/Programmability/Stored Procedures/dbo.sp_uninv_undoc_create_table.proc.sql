﻿



CREATE PROCEDURE sp_uninv_undoc_create_table AS
declare 
	@ls_bkgref char(8),
	@ld_total decimal(16,2)
truncate table ubdlaubil
print 'undoc - inv'
select * from ubdlaubil (nolock)
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,Cltname,Cltcode ,Invdate,Invnum,Invamt,Docdate,Docnum,Docamt,void2jba,branch) 
select b.companycode, b.teamcode,b.staffcode,b.bkgref,isnull(a.CLTNAME,''),a.CLTCODE,convert(char(11),a.createon),a.invnum,a.hkdamt,'','',0,void2jba,substring(b.teamcode, 1, 1)
from peoinv a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref and substring(a.invnum,2,1) in ('I') and
(((b.invamt <> 0 and b.invamt is not null) or (b.invcount <> 0 and b.invcount is not null)) and 
(b.doccount = 0 or b.doccount is null)) and a.hkdamt <> 0
print 'undoc - inv done'
print 'undoc - crn'
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,Cltname,Cltcode ,Invdate,Invnum,Invamt,Docdate,Docnum,Docamt,void2jba,branch) 
select b.companycode, b.teamcode,b.staffcode,b.bkgref,isnull(a.CLTNAME,''),a.CLTCODE,convert(char(11),a.createon),a.invnum,(a.hkdamt * (-1)),'','',0,void2jba,substring(b.teamcode, 1, 1)
from peoinv a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref and substring(a.invnum,2,1) not in ('I') and
(((b.invamt <> 0 and b.invamt is not null) or (b.invcount <> 0 and b.invcount is not null)) and 
(b.doccount = 0 or b.doccount is null)) and a.hkdamt <> 0
print 'undoc - crn done'
print 'uninv - vch'
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,docdate,docnum,docamt,Cltname,Cltcode,Invdate,Invnum,Invamt,branch)
select b.companycode, b.teamcode,b.staffcode,b.bkgref,convert(char(11),a.createon),a.vchnum,a.hkdamt,'','','','',0,substring(b.teamcode, 1, 1)
from peovch a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref  and a.hkdamt <> 0 and 
(b.invcount = 0 or b.invcount is null) and
((b.docamt <> 0 and b.docamt is not null) or (b.doccount <> 0 and b.doccount is not null))
and (a.voidby is null or a.voidby = ' ')
print 'uninv - vch done'
print 'uninv - xo'
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,docdate,docnum,docamt,Cltname,Cltcode,Invdate,Invnum,Invamt,branch)
select b.companycode, b.teamcode,b.staffcode,b.bkgref,convert(char(11),a.createon),a.xonum,a.hkdamt,'','','','',0,substring(b.teamcode, 1, 1)
from peoxo a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref  and a.hkdamt <> 0 and
(b.invcount = 0 or b.invcount is null) and
((b.docamt <> 0 and b.docamt is not null) or (b.doccount <> 0 and b.doccount is not null))
and (a.voidby is null or a.voidby = ' ')
print 'uninv - xo done'
print 'uninv - tkt'
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,docdate,docnum,docamt,Cltname,Cltcode,Invdate,Invnum,Invamt,branch)
select b.companycode, b.teamcode,b.staffcode,b.bkgref,convert(char(11),a.departon),a.ticket,(a.netfare + a.totaltax),'','','','',0,substring(b.teamcode, 1, 1)
from peotkt a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref  and ((a.netfare + a.totaltax) <> 0) and
(b.invcount = 0 or b.invcount is null) and
((b.docamt <> 0 and b.docamt is not null) or (b.doccount <> 0 and b.doccount is not null))
and (a.voidby is null or a.voidby = ' ')
print 'uninv - tkt done'
print 'uninv - mco'
insert into ubdlaubil(companycode, Team ,Staff ,BKGREF,docdate,docnum,docamt,Cltname,Cltcode,Invdate,Invnum,Invamt,branch)
select b.companycode, b.teamcode,b.staffcode,b.bkgref,convert(char(11),a.issueon),a.mconum,a.hkdamt,'','','','',0,substring(b.teamcode, 1, 1)
from peomco a (nolock), peomstr b (nolock)
where a.bkgref=b.bkgref and a.hkdamt <> 0 and
(b.invcount = 0 or b.invcount is null) and
((b.docamt <> 0 and b.docamt is not null) or (b.doccount <> 0 and b.doccount is not null))
and (a.voidby is null or a.voidby = ' ')
print 'uninv - mco done'
declare undoccurr cursor for
select bkgref from ubdlaubil(nolock) where substring(invnum ,2,1) = 'I' order by team,staff,bkgref
open undoccurr
fetch undoccurr into @ls_bkgref
while @@fetch_status = 0
begin
	select @ld_total = sum(invamt) from ubdlaubil(nolock) where bkgref= @ls_bkgref and invnum<> ' ' and invnum is not null
	if @ld_total = 0
		delete from ubdlaubil where bkgref = @ls_bkgref and invnum<> ' ' and invnum is not null
	fetch undoccurr into @ls_bkgref
end
close undoccurr
deallocate undoccurr




