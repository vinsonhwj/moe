﻿
CREATE   PROCEDURE dbo.sp_RptTravelActivityReportInvoice
@COMPANYCODE varchar(2),
@START_DATE varchar(10),
@END_DATE varchar(25),
@CLT_CODE varchar(10),
@PAXNAME nvarchar(100)



AS

CREATE TABLE #tmpRptResult
(
	  Companycode  	varchar(2) not null default ''
	, Teamcode 	varchar(10) not null default ''
	, Staffcode	varchar(10) not null default ''
	, Createon 	datetime null
	, Mstrinvnum    varchar(10) not null default ''
	, Cltcode       varchar(10) not null default ''
	, Pax           nvarchar(100) not null default ''
	, pnr           varchar(6) not null default ''
	, ticket        varchar(10) not null default ''
	, routing       varchar(100) not null default ''
	, flight        varchar(10) not null default ''
	, class        	varchar(10) not null default ''
	, depart        varchar(20) not null default ''
	, arrival       varchar(20) not null default ''
	, servtype      varchar(50) not null default ''
	, airfare       decimal(16,2) not null default 0
	, taxamt        decimal(16,2) not null default 0
	, amtfee        decimal(16,2) not null default 0
	, othcharge     decimal(16,2) not null default 0
	, invnum        varchar(10) not null default ''
	, COD1		varchar(30) not null default ''
	, COD2		varchar(30) not null default ''
	, COD3		varchar(30) not null default ''

)

DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_preInvnum varchar(10)
DECLARE @lstr_Invnum varchar(10)
DECLARE @lstr_Ticket varchar(10)
DECLARE @lstr_SegType varchar(10)
DECLARE @lstr_Routing varchar(100)
DECLARE @lstr_Flight varchar(10)
DECLARE @lstr_Class varchar(10)
DECLARE @lstr_Depart varchar(10)
DECLARE @lstr_Arrival varchar(10)
DECLARE @lnum_SellAmt decimal(16,2)
DECLARE @lnum_TaxAmt decimal(16,2)
DECLARE @lnum_Amtfee decimal(16,2)
DECLARE @lnum_OtherCharge decimal(16,2)


--Retrieve Master Record
if ltrim(rtrim(@CLT_CODE)) <> ''
begin
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.Createon, ISNULL(inv.Mstrinvnum,'') as Mstrinvnum, inv.Cltcode,
		isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		from peoinpax (nolock)
		where invnum = inv.invnum and companycode = inv.companycode order by paxseq asc), '') as pax,
	       Mstr.pnr, inv.Invnum, inv.COD1, inv.COD2, inv.COD3
	INTO #tmpResult
	FROM Peoinv inv (nolock), Peomstr mstr (nolock)
	WHERE inv.Companycode = mstr.Companycode
	AND inv.bkgref = mstr.bkgref
	AND inv.Companycode = @COMPANYCODE
	AND inv.Createon between @START_DATE AND @END_DATE
	AND inv.Cltcode = @CLT_CODE
	AND substring(invnum, 2, 1) = 'I'
	AND isnull(void2jba, '') = ''

	if ltrim(rtrim(@PAXNAME)) <> ''
	begin
		INSERT INTO #tmpRptResult
		(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum, COD1, COD2, COD3)
		SELECT CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum, COD1, COD2, COD3
		FROM #tmpResult
		WHERE pax like '%' + @PAXNAME + '%';
	end
	else
	begin
		INSERT INTO #tmpRptResult
		(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum, COD1, COD2, COD3)
		SELECT CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum, COD1, COD2, COD3
		FROM #tmpResult
	end
	DROP TABLE #tmpResult
end
else
--Pax Only
begin

	SELECT DISTINCT inv.companycode, inv.invnum
	INTO #tmpPax
	FROM Peoinv inv (nolock), Peomstr mstr (nolock), peoinpax pax (nolock) 
	WHERE inv.Companycode = mstr.Companycode AND inv.bkgref = mstr.bkgref
	AND pax.Companycode = inv.Companycode AND pax.invnum = inv.invnum
	AND replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ') like '%' + @PAXNAME + '%'
	AND inv.Companycode = @COMPANYCODE
	AND inv.Createon between @START_DATE AND @END_DATE
	AND substring(inv.invnum, 2, 1) = 'I'
	AND isnull(inv.void2jba, '') = ''


	INSERT INTO #tmpRptResult
	(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum)
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.Createon, ISNULL(inv.Mstrinvnum,''), inv.Cltcode,
		isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		from peoinpax (nolock)
		where invnum = inv.invnum and companycode = inv.companycode order by paxseq asc), '') as pax,
	       Mstr.pnr, inv.Invnum
	FROM Peoinv inv (nolock), Peomstr mstr (nolock), #tmpPax pax (nolock)
	WHERE inv.Companycode = mstr.Companycode
	AND inv.bkgref = mstr.bkgref
	AND inv.Companycode = pax.Companycode AND inv.invnum = pax.invnum

	DROP TABLE #tmpPax

end




--Start Ticket (UATP)
SELECT @lstr_preInvnum = ''
SELECT @lstr_Invnum = ''

SELECT ref.Companycode, ref.Invnum, ref.Ticket 
INTO #tmpTktRef
FROM peoinvtktref ref (nolock), #tmpRptResult rpt
WHERE ref.companycode = rpt.Companycode AND ref.invnum = rpt.invnum



DECLARE lcur_Tktref CURSOR FOR 
SELECT Companycode, Invnum, Ticket FROM #tmpTktRef ORDER BY Companycode, Invnum

OPEN lcur_Tktref
FETCH lcur_Tktref INTO @lstr_Companycode, @lstr_Invnum, @lstr_Ticket
WHILE @@FETCH_STATUS = 0
BEGIN
	if (@lstr_Invnum <> @lstr_preInvnum)
	begin
		UPDATE #tmpRptResult SET Ticket = @lstr_Ticket 
		WHERE CompanyCode = @lstr_Companycode AND invnum = @lstr_Invnum
	end

	SELECT @lstr_preInvnum = @lstr_Invnum
	FETCH lcur_Tktref INTO @lstr_Companycode, @lstr_Invnum, @lstr_Ticket

END
CLOSE lcur_Tktref
DEALLOCATE lcur_Tktref
DROP TABLE #tmpTktRef


--Start Ticket (NORMAL INVOICE)
SELECT @lstr_preInvnum = ''
SELECT @lstr_Invnum = ''

SELECT tkt.Companycode,  tkt.Invnum, tkt.Ticket 
INTO #tmpTkt
FROM peoinvtkt tkt (nolock), #tmpRptResult rpt
WHERE tkt.companycode = rpt.Companycode AND tkt.invnum = rpt.invnum

DECLARE lcur_Tkt CURSOR FOR 
SELECT Companycode, Invnum, Ticket FROM #tmpTkt ORDER BY Companycode, Invnum

OPEN lcur_Tkt
FETCH lcur_Tkt INTO @lstr_Companycode, @lstr_Invnum, @lstr_Ticket
WHILE @@FETCH_STATUS = 0
BEGIN
	if (@lstr_preInvnum <> @lstr_Invnum)
	begin
		UPDATE #tmpRptResult SET Ticket = @lstr_Ticket 
		WHERE CompanyCode = @lstr_Companycode AND invnum = @lstr_Invnum
	end

	SELECT @lstr_preInvnum = @lstr_Invnum
	FETCH lcur_Tkt INTO @lstr_Companycode, @lstr_Invnum, @lstr_Ticket

END
CLOSE lcur_Tkt
DEALLOCATE lcur_Tkt
DROP TABLE #tmpTkt





--Calculate Segment Amount 
SELECT rpt.Companycode, rpt.Invnum, seg.Segtype, seg.Segnum, seg.servtype, seg.servname, Servdesc,
isnull(sum(detail.sellamt),0) as sellamt, isnull(sum(detail.taxamt),0) as taxamt, isnull(sum(detail.amtfee),0) as amtfee, 
isnull(sum(detail.qty),0) as qty, isnull(sum(detail.rmnts),0) as rmnts
INTO #tmpSegment
FROM Peoinvseg seg (nolock), Peoinvdetail detail (nolock), #tmpRptResult rpt
WHERE seg.Companycode = rpt.Companycode AND seg.Invnum = rpt.Invnum
AND detail.Companycode = seg.Companycode AND detail.Invnum = seg.Invnum AND detail.segnum = seg.segnum
AND seg.servtype in ('AIR', 'HTL', 'OTH')
GROUP BY rpt.Companycode, rpt.Invnum, seg.SegType, seg.servtype, seg.Segnum, seg.servname, Servdesc

--SELECT * FROM #tmpSegment

DECLARE lcur_Segment CURSOR FOR
SELECT Companycode, Invnum FROM #tmpRptResult ORDER BY Companycode, Invnum

OPEN lcur_Segment
FETCH lcur_Segment INTO @lstr_Companycode, @lstr_Invnum
WHILE @@FETCH_STATUS = 0
BEGIN

	SELECT @lstr_SegType = 'OTH';
	SELECT @lstr_Routing = 'MISC';
	SELECT @lstr_Flight = 'MISC';
	SELECT @lstr_Class = 'MISC';
	SELECT @lstr_Depart = 'MISC';
	SELECT @lstr_Arrival = 'MISC';
	SELECT @lnum_SellAmt = 0;
	SELECT @lnum_TaxAmt = 0;
	SELECT @lnum_Amtfee = 0;
	SELECT @lnum_OtherCharge = 0;

	-- Air
	if exists (SELECT servtype from #tmpSegment where companycode = @lstr_Companycode and invnum = @lstr_Invnum)
	begin
		--//Routing, Flight, Class, Depart Date
		SELECT TOP 1  @lstr_SegType = case when segtype <> 'SVC' and segtype <> 'DOC' then servtype  
					           when segtype = 'DOC' or segtype = 'SVC' then 'OTH' end,
			      @lstr_Routing = (convert(char(7), replace(right(rtrim(servname), 9), '   ', '/') )),
			      @lstr_Flight = substring(servname, 1, 2) + ' ' + substring(servname, 3, 3),
			   @lstr_Class = substring(servname, 10, 1),
			      @lstr_Depart = substring(servname, 16, 10)
                FROM #tmpSegment 
                WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'AIR' 
                ORDER BY segnum ASC


		--//Arrival Date
		SELECT TOP 1 @lstr_Arrival = substring(servdesc, 8, 10)
                FROM #tmpSegment 
                WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'AIR' 
                ORDER BY segnum DESC

	end
	else
	begin

		--//Depart Date
		SELECT TOP 1 @lstr_Depart = substring(servdesc, 1, 10)
                FROM #tmpSegment 
                WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'HTL' 
                ORDER BY segnum ASC

		--//Arrival Date
		SELECT TOP 1 @lstr_Arrival = substring(servdesc, 13, 10)
                FROM #tmpSegment 
                WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'HTL' 
                ORDER BY segnum DESC

	end

	--//Sell Amount
	SELECT @lnum_SellAmt = SUM(sellamt * qty), @lnum_TaxAmt = SUM(taxamt * qty)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'AIR' 

	--//Amount Fee
	SELECT @lnum_Amtfee = isnull(SUM(amtfee * qty),0)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'AIR' AND segtype <> 'SVC'  AND segType <> 'DOC' 


	SELECT @lnum_Amtfee = @lnum_Amtfee + isnull(SUM(sellamt * qty),0)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'AIR' AND segtype = 'SVC'  AND segType = 'DOC'

	SELECT @lnum_Amtfee = @lnum_Amtfee + isnull(SUM((sellamt + taxamt) * qty),0)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'OTH'  AND segType = 'HLE'

	--//Other Charge
	SELECT @lnum_OtherCharge = SUM((sellamt + taxamt)* qty * rmnts)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'HTL'


	SELECT @lnum_OtherCharge = SUM((sellamt + taxamt)* qty)
	FROM #tmpSegment 
        WHERE Companycode = @lstr_CompanyCode and Invnum = @lstr_Invnum AND servtype = 'OTH'  AND segType <> 'HLE'


	UPDATE #tmpRptResult SET 
	Servtype = @lstr_SegType,
	Routing = @lstr_Routing, 
	Flight = @lstr_Flight,
	Class = @lstr_Class,
	Depart = @lstr_Depart,
	Arrival = @lstr_Arrival,
	airfare = isnull(@lnum_sellamt,0),
	Taxamt = isnull(@lnum_Taxamt,0),
	amtfee = isnull(@lnum_Amtfee,0),
	othcharge = isnull(@lnum_Othercharge,0)
	WHERE  CompanyCode = @lstr_Companycode AND invnum = @lstr_Invnum


	FETCH lcur_Segment INTO @lstr_Companycode, @lstr_Invnum
END
CLOSE lcur_Segment
DEALLOCATE lcur_Segment
DROP TABLE #tmpSegment


SELECT Companycode, Teamcode, Staffcode, Createon, SERVTYPE, convert(varchar(10),createon,103) as issuedate
, mstrinvnum, cltcode, pax, pnr, Ticket, Routing, Flight, Class, Depart, Arrival
, CASE WHEN airfare = 0 THEN '-' ELSE CAST(airfare AS VARCHAR(25)) end as airfare
, CASE WHEN taxamt = 0 THEN '-' ELSE CAST(taxamt AS VARCHAR(25)) end as taxes
, CASE WHEN amtfee = 0 THEN '-' ELSE CAST(amtfee AS VARCHAR(25)) end as fee
, CASE WHEN othcharge = 0 THEN '-' ELSE CAST(othcharge AS VARCHAR(25)) end as othcharge
, invnum, cod1, cod2, cod3
FROM #tmpRptResult order by cltcode ,createon asc, invnum;



DROP TABLE #tmpRptResult;









--exec sp_RptTravelActivityReportInvoice 'sg','2007-12-01','2007-12-31 23:59:59.000','HB0053',''








