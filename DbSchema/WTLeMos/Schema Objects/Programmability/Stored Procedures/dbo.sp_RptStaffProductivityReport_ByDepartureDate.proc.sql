﻿
CREATE  PROCEDURE dbo.sp_RptStaffProductivityReport_ByDepartureDate
@START_DATE CHAR(10),
@END_DATE CHAR(25),
@TEAM_CODE VarChar(10),
@COMPANY_CODE VarChar(2),
@STAFF_CODE VarChar(10)
AS

DECLARE @FinancalYear datetime
DECLARE @ldt_StartDate datetime
DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_TeamCode varchar(10)
DECLARE @lstr_StaffCode varchar(10) 
DECLARE @lnum_TTLInvamt decimal(16,2) 
DECLARE @lnum_TTLDocamt decimal(16,2)
DECLARE @lnum_TTLDepamt decimal(16,2) 
DECLARE @lnum_TTLPaxCount int
DECLARE @lnum_YTDInvamt decimal(16,2) 
DECLARE @lnum_YTDDocamt decimal(16,2)
DECLARE @lnum_YTDDepamt decimal(16,2) 
DECLARE @lnum_YTDPaxCount int

print 'Start Report at ' + convert(varchar,getdate(),109)


CREATE TABLE #tempStaff
(
	CompanyCode varchar(2) not null default ''
,	Teamcode varchar(10) not null default ''
,	Staffcode varchar(10) not null default ''
)

if ltrim(rtrim(@STAFF_CODE)) <> ''
begin
	INSERT INTO #tempStaff 
	(Companycode, TeamCode, StaffCode)
	SELECT Companycode, Teamcode, Staffcode FROM peostaff (nolock) WHERE companycode = @COMPANY_CODE AND Staffcode = @STAFF_CODE
end
else
begin
	INSERT INTO #tempStaff 
	(Companycode, TeamCode, StaffCode)
	SELECT Companycode, TeamCode, staffcode FROM peostaff (nolock) WHERE companycode = @COMPANY_CODE AND teamcode = @TEAM_CODE
end

--INSERT INTO #tmpReportResult (Companycode, TeamCode, Staffcode) SELECT CompanyCode, Teamcode, Staffcode from #tempStaff


-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end


print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT 
a.companycode, a.teamcode, a.bkgref, a.staffcode, b.invnum, a.departdate, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), #tempStaff staff
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND a.departdate BETWEEN @FinancalYear AND @END_DATE
AND a.staffcode = staff.staffcode
GROUP BY a.companycode,a.teamcode, a.bkgref, a.StaffCode, b.invtype, b.invnum, a.departdate
ORDER BY 1, 2


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Teamcode varchar(10) not null
,	Staffcode varchar(10) not null
,	Docnum varchar(10) not null
,	DepartDate datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)
--Ticket
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, mstr.departdate, tkt.netfare+tkt.totaltax cost, 'TKT'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), #tempstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.departdate between @FinancalYear AND @END_DATE
--AND tkt.issueon between @FinancalYear AND @END_DATE
AND tkt.voidon is NULL



print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, mstr.departdate, xo.hkdamt cost  , 'XO'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.createon, xo.hkdamt cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.departdate between @FinancalYear AND @END_DATE
--AND xo.createon between @FinancalYear AND @END_DATE
AND xo.voidon is NULL




print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, mstr.departdate, vch.hkdamt cost  , 'VCH'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.createon, vch.hkdamt cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.departdate between @FinancalYear AND @END_DATE
--AND vch.createon between @FinancalYear AND @END_DATE
AND vch.voidon is NULL





print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mstr.departdate, mco.costamt + mco.taxamt cost, 'MCO' 
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.issueon, mco.costamt + mco.taxamt cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.departdate between @FinancalYear AND @END_DATE
--AND mco.issueon between @FinancalYear AND @END_DATE
AND mco.voidon is NULL




print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report

CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	TeamCode nvarchar(10) null default ''
,	StaffCode nvarchar(10) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
,	YTDPax integer null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)



insert into #TmpSellAmt
--(Companycode, teamcode, staffcode)
(Companycode, teamcode, staffcode)
select distinct s.companycode, s.teamcode, s. staffcode from peostaff s(nolock), #tempStaff staff
where s.companycode = @COMPANY_CODE and s.staffcode = staff.staffcode

--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode



--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDPrice = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and departdate between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (YTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and departdate between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as cost
--(Select CompanyCode, TeamCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as costs
--(Select CompanyCode, TeamCode, Sum(cost) as costs
From #TmpDoc 
WHERE DepartDate between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, count(1) as counts
--(Select CompanyCode, TeamCode, count(1) as counts
From #TmpDoc 
WHERE DepartDate between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, inv.staffcode, count(1) paxcount
FROM peopax pax(nolock), peomstr mstr(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
and inv.companycode = mstr.companycode and inv.bkgref = mstr.bkgref
AND mstr.departdate between @START_DATE AND @END_DATE
GROUP BY inv.companycode, inv.Teamcode, inv.StaffCode ) i 
--GROUP BY inv.companycode, inv.Teamcode) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--WHERE i.companycode = t.companycode and i.teamcode = t.teamcode



--YTD Passenger
UPDATE #TmpSellAmt set YTDPax = YTDPax + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, inv.staffcode, count(1) paxcount
FROM peopax pax(nolock), peomstr mstr(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
and inv.companycode = mstr.companycode and inv.bkgref = mstr.bkgref
AND mstr.departdate between @FinancalYear AND @END_DATE
GROUP BY inv.companycode, inv.Teamcode, inv.StaffCode ) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode




SELECT Companycode, TeamCode + '/' + Staffcode as code, staffcode
, CASE WHEN PaxTotal <> 0 THEN CONVERT(CHAR, PaxTotal) else '0' END AS [Total Pax]
, CASE WHEN TktCount <> 0 THEN CONVERT(CHAR, TktCount) else '0' END AS [TktCount]
, CASE WHEN Price <> 0 THEN CONVERT(CHAR, Price) else '0' END AS [Price]
, CASE WHEN Dep <> 0 THEN CONVERT(CHAR, Dep) else '0' END AS [Dep]
, CASE WHEN Cost <> 0 THEN CONVERT(CHAR, Cost) else '0' END AS [Cost]
, CASE WHEN Price-Cost <> 0 THEN CONVERT(VARCHAR,Price-Cost) ELSE '0' END as Margin
, CASE WHEN Price <> 0 THEN (convert(CHAR,convert(decimal(16,2),(Price-Cost)*100/Price))) ELSE '0' END [Yield(%)]
, CASE WHEN YTDPax <> 0 THEN CONVERT(CHAR, YTDPax) else '0' END AS [YTD Total Pax]
, CASE WHEN YTDPrice <> 0 THEN CONVERT(CHAR, YTDPrice) else '0' END AS [YTDPrice]
, CASE WHEN YTDDep <> 0 THEN CONVERT(CHAR, YTDDep) else '0' END AS [YTDDep]
, CASE WHEN YTDCost <> 0 THEN CONVERT(CHAR, YTDCost) else '0' END AS [YTDCost]
, CASE WHEN YTDPrice-YTDCost <> 0 THEN CONVERT(VARCHAR,YTDPrice-YTDCost) ELSE '0' END as YTDMargin
, CASE WHEN YTDPrice <> 0 THEN (convert(CHAR,convert(decimal(16,2),(YTDPrice-YTDCost)*100/YTDPrice))) ELSE '0' END [YTDYield(%)]


FROM #TmpSellAmt
--FROM #tmpReportResult





DROP TABLE #tempStaff
DROP TABLE #TmpSellAmt
--DROP TABLE #tmpReportResult












