﻿












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Search_Booking]
	-- Add the parameters for the stored procedure here
    @companycode NVARCHAR(2) ,
    @bkgref NVARCHAR(15) ,
    @contactperson NVARCHAR(40) ,
    @PaxLName NVARCHAR(20) ,
    @PaxFName NVARCHAR(40) ,
    @staffcode NCHAR(10) ,
    @teamcode NVARCHAR(5) ,
    @cltcode NVARCHAR(8) ,
    @tourcode NVARCHAR(15) ,
    @phone NVARCHAR(20) ,
    @departFrom VARCHAR(22) ,
    @departTo VARCHAR(22) ,
    @deadlineFrom VARCHAR(22) ,
    @deadlineTo VARCHAR(22) ,
    @createonFrom VARCHAR(22) ,
    @createonTo VARCHAR(22) ,
    @oderby NVARCHAR(2) ,
    @status NVARCHAR(2)
AS 
    DECLARE @sql NVARCHAR(4000)
    DECLARE @sqlwhere NVARCHAR(4000)
  
    DECLARE @b_Havepax NVARCHAR(2)
    SET @b_Havepax = 'N'
    DECLARE @Exit_Clt NVARCHAR(2)
    SET @Exit_Clt = 'N'

    SELECT  @sql = 'SELECT DISTINCT
                m.COMPANYCODE AS COMPANYCODE,
                m.BKGREF AS BKGREF ,
                m.CLTODE,
               isnull( m.STAFFCODE,'''' )AS STAFFCODE ,
               isnull( m.DEPARTDATE,'''') AS DEPARTDATE,
               isnull(m.teamcode,'''') AS teamcode ,
               isnull( P.PaxLname,'''') AS PaxLname,
               isnull(P.paxFname,'''') AS PaxFname,
               isnull(P.Mobile,'''') AS Telephone,
               isnull( P.Email,'''') AS Email
                
      FROM      dbo.PEOMSTR m
                                  
                                    LEFT JOIN peoProfile P ON m.COMPANYCODE = P.CompanyCode
                                                              AND m.ProfileID = P.ProfileID
                                      '
               

    SELECT  @sqlwhere = 'where  m.COMPANYCODE=''' + @companycode + ''''

    IF @bkgref <> '' 
        BEGIN
            IF LEN(@bkgref) = '10' 
                SELECT  @sqlwhere = @sqlwhere + ' and  m.BKGREF = '''
                        + @bkgref + ''' '
            ELSE 
                SELECT  @sqlwhere = @sqlwhere + ' and m.BKGREF LIKE ''%'
                        + @bkgref + ''' '
        END
  
    IF @contactperson <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere
                    + ' AND replace(m.CONTACTPERSON,'' '', '''') LIKE N''%'
                    + REPLACE(ISNULL(@contactperson, ''''), ' ', '') + '%'''
                                         
        END
                         
                     
    IF @departFrom <> ''
        AND @departTo <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND ( m.DepartDate >= '''
                    + @departFrom + ''' AND m.DepartDate <= ''' + @departTo
                    + ''')'
        END
                         
    IF @deadlineFrom <> ''
        AND @deadlineTo <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND ( m.Deadline >= '''
                    + @deadlineFrom + ''' AND m.Deadline <= ''' + @deadlineTo
                    + ''')'  
        END
			      
                             
    IF @createonFrom <> ''
        AND @createonTo <> '' 
        BEGIN
          
			
            SELECT  @sqlwhere = @sqlwhere + ' AND ( m.Createon >= '''
                    + @createonFrom + ''' AND m.Createon <= ''' + @createonTo
                    + ''')'
			
           
        END
    
    IF @staffcode <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND m.STAFFCODE = '''
                    + @staffcode + ''''
        END
    IF @teamcode <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND m.TEAMCODE = ''' + @teamcode
                    + ''''
        END     
    IF @tourcode <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND m.TOURCODE = ''' + @tourcode
                    + ''''
        END
    IF @PaxFName <> '' 
        BEGIN
            SET @b_Havepax = 'Y'   
            SELECT  @sqlwhere = @sqlwhere
                    + ' AND REPLACE(PEOPAX.PAXFNAME,'' '','''') LIKE '''
                    + REPLACE(ISNULL(@PaxFName, ''''), ' ', '') + '%'''
        END	  
    IF @PaxLName <> '' 
        BEGIN 
            SET @b_Havepax = 'Y'
            SELECT  @sqlwhere = @sqlwhere + ' AND PEOPAX.PAXLNAME LIKE '''
                    + @PaxLName + '%'''
        END 
    IF @cltcode <> '' 
        BEGIN
            SET @Exit_Clt = 'Y'
        END
    ELSE 
        BEGIN
            SET @Exit_Clt = 'N'
        END
    IF @Exit_Clt = 'Y' 
        BEGIN
            SELECT  @sql = @sql
                    + ' LEFT JOIN dbo.PEOINV I ON m.COMPANYCODE = I.COMPANYCODE
                                                              AND m.BKGREF = I.BKGREF '
            SELECT  @sqlwhere = @sqlwhere + ' AND I.CLTCODE =''' + @cltcode
                    + ''' '
        END
    IF @phone <> '' 
        BEGIN
            SELECT  @sqlwhere = @sqlwhere + ' AND m.PHONE =''' + @phone + ''''
        END
               
   
    SELECT  @sqlwhere = ( CASE WHEN @status = '1'
                               THEN @sqlwhere
                                    + ' AND (m.IsClosed=''N'' or m.IsClosed Is Null)'
                               WHEN @status = '2'
                               THEN @sqlwhere + ' AND m.IsClosed=''Y'''
                               ELSE @sqlwhere
                          END )  
        

    IF @b_Havepax = 'Y' 
        BEGIN
            SELECT  @sql = @sql
                    + '  LEFT JOIN PEOPAX ON  m.COMPANYCODE=PEOPAX.COMPANYCODE AND m.BKGREF=PEOPAX.BKGREF  '
                           
            SELECT  @sql = @sql + @sqlwhere
            
        END
    ELSE 
        BEGIN
                
            
            SELECT  @sql = @sql + @sqlwhere 
                    
                   
        END
                
             
      
    SELECT  @sql = ( CASE WHEN @oderby <> '0'
                          THEN ( CASE WHEN @oderby = '1'
                                      THEN @sql + 'ORDER BY m.DEPARTDATE'
                                      WHEN @oderby = '2'
                                      THEN @sql + 'ORDER BY m.CLTODE'
                                      WHEN @oderby = '3'
                                      THEN @sql + 'ORDER BY m.STAFFCODE'
                                      ELSE @sql + 'ORDER BY m.TEAMCODE'
                                 END )
                          ELSE @sql
                     END )

    --SELECT  @sql
    EXEC(@sql)

 
        
	
--SELECT * FROM 
--(
--SELECT DISTINCT MS.COMPANYCODE,MS.BkgRef,PR.PaxLname, PR.PaxFName,
--MS.DepartDate,
--MS.StaffCode,MS.TeamCode, 
--PR.Mobile, 
--PR.Email,MS.CLTODE
-- FROM peomstr AS MS LEFT JOIN peoprofile AS PR
--ON MS.COMPANYCODE = PR.CompanyCode AND MS.ProfileID = PR.ProfileID
--LEFT JOIN dbo.PEOPAX AS PA
--ON MS.CompanyCode = PA.COMPANYCODE AND MS.BKGREF =PA.BKGREF
--LEFT JOIN dbo.PEOINV AS INV
--ON MS.COMPANYCODE = INV.COMPANYCODE  AND MS.BKGREF =INV.BKGREF
--WHERE MS.COMPANYCODE =@companycode 
--AND MS.BKGREF LIKE (CASE WHEN LEN(ISNULL(@bkgref,''))=10 THEN @bkgref ELSE '%'+ISNULL(@bkgref,'') END)
--AND REPLACE(MS.CONTACTPERSON,' ','') LIKE '%'+REPLACE(ISNULL(@contactperson,''),' ','')+'%'
--AND PA.PAXLNAME LIKE ISNULL(@PaxLName,'')+'%'
--AND REPLACE(PA.PAXFNAME,' ','') LIKE REPLACE(ISNULL(@PaxFName,''),' ','')+'%'
--AND MS.STAFFCODE=(CASE WHEN ISNULL(@staffcode,'')='' THEN MS.STAFFCODE ELSE @staffcode end)
--AND MS.TEAMCODE=(CASE WHEN ISNULL(@teamcode,'')='' THEN MS.TEAMCODE ELSE @teamcode end)
--AND Inv.CLTCODE=(CASE WHEN ISNULL(@cltcode,'')='' THEN Inv.CLTCODE ELSE @cltcode end)
--AND MS.TOURCODE =(CASE WHEN ISNULL(@tourcode,'')='' THEN MS.TOURCODE ELSE @tourcode end)
--AND MS.PHONE=(CASE WHEN ISNULL(@phone,'')='' THEN MS.PHONE ELSE @phone end)
--AND MS.DEPARTDATE>=(CASE WHEN ISNULL(@departFrom,'')='' THEN MS.DEPARTDATE ELSE @departFrom END)
--AND MS.DEPARTDATE<=(CASE WHEN ISNULL(@departTo,'')='' THEN MS.DEPARTDATE ELSE @departTo END)
--AND MS.Deadline>=(CASE WHEN ISNULL(@deadlineFrom,'')='' THEN MS.Deadline ELSE @deadlineFrom END)
--AND MS.Deadline <=(CASE WHEN ISNULL(@deadlineTo,'')='' THEN MS.Deadline ELSE @deadlineTo END)
--AND MS.CreateOn >=(CASE WHEN ISNULL(@createonFrom,'')='' THEN MS.CreateOn  ELSE @createonFrom END)
--AND MS.CreateOn <=(CASE WHEN ISNULL(@createonTo,'')='' THEN MS.CreateOn  ELSE @createonTo END)
--AND ISNULL(MS.IsClosed,'') =(CASE @status WHEN '0' THEN ISNULL(MS.IsClosed,'') 
--										WHEN '1' THEN ISNULL(MS.IsClosed,'N')
--										ELSE 'Y' END)
--) AS R
--ORDER BY (CASE @oderby WHEN '0' THEN COMPANYCODE WHEN '1' THEN CONVERT(varchar(100),DEPARTDATE,21) WHEN '2' THEN CLTODE  ELSE STAFFCODE+TEAMCODE END)














