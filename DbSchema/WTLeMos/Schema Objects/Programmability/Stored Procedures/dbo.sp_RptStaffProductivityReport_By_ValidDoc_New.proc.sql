﻿
CREATE	                       PROCEDURE dbo.sp_RptStaffProductivityReport_By_ValidDoc_New
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@TEAM_CODE CHAR(10),
@STAFF_CODE CHAR(10)


AS

DECLARE @lstr_Staffcode varchar(5)
DECLARE @lnum_Cost decimal(16,2)
DECLARE @ldt_DepartDate datetime
DECLARE @lstr_bkgref nvarchar(20)
DECLARE @lnum_PaxTotal integer
DECLARE @FinancalYear datetime
DECLARE @ldt_StartDate datetime
DECLARE @lstr_invtype varchar(10)


print 'Start Prepare Data at ' + convert(varchar,getdate(),109)
-- INIT TMP TABLE VALUES
CREATE TABLE #tempStaff (StaffCode varchar(10));

IF RTRIM(@TEAM_CODE) <> ''
BEGIN 
	IF RTRIM(@STAFF_CODE) <> ''
	BEGIN
		INSERT INTO #tempStaff VALUES (@STAFF_CODE);
	END
	ELSE
	BEGIN
		INSERT INTO #tempStaff 
		SELECT DISTINCT STAFFCODE FROM PEOSTAFF (NOLOCK) 
		 WHERE COMPANYCODE = @COMPANY_CODE AND TEAMCODE=@TEAM_CODE
	END

END 
ELSE
BEGIN
	IF RTRIM(@STAFF_CODE) <> ''
	BEGIN
		INSERT INTO #tempStaff VALUES (@STAFF_CODE);
	END

END



-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------

print 'Start Retrieve Peomstr at ' + convert(varchar,getdate(),109)


SELECT companycode, bkgref, teamcode, mstr.staffcode
into #tmpMstr
from peomstr mstr(nolock),#tempStaff staff  
where companycode = @COMPANY_CODE
and mstr.staffcode = staff.staffcode

print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT a.companycode, a.teamcode, a.bkgref, a.staffcode, b.invnum, b.createon
into #tmpInvoice
FROM #tmpMstr a (nolock) ,peoinv b (nolock)
WHERE b.companycode=a.companycode AND b.bkgref=a.bkgref 
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND b.voidon is null


SELECT 
a.companycode, a.teamcode, a.bkgref, a.staffcode, a.invnum, a.createon, case substring(a.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM #tmpInvoice a (nolock) ,peoinv b (nolock)
WHERE a.companycode=b.companycode AND a.invnum=b.invnum
ORDER BY 1, 2

drop table #tmpInvoice


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Teamcode varchar(10) not null
,	Staffcode varchar(10) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)


--Ticket
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.tktseq, tkt.issueon, tkt.voidon
into #tmpTicket
FROM peotkt tkt(nolock), #tmpMstr mstr (nolock)
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND tkt.issueon between @FinancalYear AND @END_DATE
AND tkt.voidon is null


insert into #tmpDoc
select mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mstr.Ticket, mstr.issueon,tkt.netfare+tkt.totaltax cost
, 'TKT', case when mstr.voidon is null then 'N' else 'Y' end
from #tmpTicket mstr, peotkt tkt (nolock)
where tkt.companycode = mstr.companycode and tkt.ticket = mstr.ticket and tkt.tktseq = mstr.tktseq

drop table #tmpTicket


print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.createon, xo.voidon
into #tmpXo
FROM peoxo xo(nolock), #tmpMstr mstr (nolock)
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND xo.createon between @FinancalYear AND @END_DATE
AND xo.voidon is null

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, mstr.xonum, mstr.createon, xo.hkdamt cost  , 'XO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpXo mstr (nolock), peoxo xo (nolock)
where xo.companycode = mstr.companycode and xo.xonum = mstr.xonum

drop table #tmpXo



print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.createon, vch.voidon
into #tmpVoucher
FROM peovch vch(nolock), #tmpMstr mstr (nolock)
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND vch.createon between @FinancalYear AND @END_DATE
AND vch.voidon is null

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, mstr.vchnum, mstr.createon, vch.hkdamt cost  , 'VCH'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpVoucher mstr (nolock), peovch vch (nolock)
where vch.companycode = mstr.companycode and vch.vchnum = mstr.vchnum

drop table #tmpVoucher


print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, mco.mconum, mco.mcoseq, mco.createon, mco.voidon
into #tmpMco
FROM peomco mco(nolock), #tmpMstr mstr (nolock)
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mco.createon between @FinancalYear AND @END_DATE
AND mco.voidon is null

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, mstr.mconum, mstr.createon, mco.costamt + mco.taxamt cost  , 'MCO'
, case when mstr.voidon is null then 'N' else 'Y' end
from #tmpMco mstr (nolock), peomco mco (nolock)
where mco.companycode = mstr.companycode and mco.mconum = mstr.mconum and mco.mcoseq = mstr.mcoseq

drop table #tmpMco



print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	TeamCode nvarchar(50) null default ''
,	StaffCode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)

insert into #TmpSellAmt
--(Companycode, teamcode, staffcode)
(Companycode, teamcode, staffcode)
select distinct s.companycode, s.teamcode, s. staffcode from peostaff s(nolock), #tempStaff staff
where s.companycode = @COMPANY_CODE and s.staffcode = staff.staffcode

--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode



--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDDep = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as cost
--(Select CompanyCode, TeamCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, Sum(cost) as costs
--(Select CompanyCode, TeamCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, TeamCode, StaffCode, count(1) as counts
--(Select CompanyCode, TeamCode, count(1) as counts
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, TeamCode, StaffCode ) i
--Group By CompanyCode, TeamCode ) i
where i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.teamcode, inv.staffcode, count(1) paxcount
--(SELECT inv.companycode, inv.teamcode, count(1) paxcount
FROM peopax pax(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
AND inv.createon between @START_DATE AND @END_DATE
GROUP BY inv.companycode, inv.Teamcode, inv.StaffCode ) i 
--GROUP BY inv.companycode, inv.Teamcode) i 
WHERE i.companycode = t.companycode and i.teamcode = t.teamcode and i.staffcode = t.staffcode
--WHERE i.companycode = t.companycode and i.teamcode = t.teamcode





SELECT staff.teamcode + '/' + rpt.staffcode as code, PaxTotal as [PaxTotal]
, CASE when TktCount <> 0 then TktCount else 0 END as TktCount
, CASE when Price <> 0 then Price else 0 END as Price
, CASE when Dep <> 0 then Dep else 0 END as Dep
, CASE WHEN cost <> 0 THEN cost else 0 END AS Cost
, CASE WHEN price-cost <> 0 THEN price-cost ELSE 0 END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then YTDPrice else 0 END as YTDPrice
, CASE when YTDDep <> 0 then YTDDep else 0 END as YTDDep
, CASE WHEN YTDcost <> 0 THEN YTDcost else 0 END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN YTDprice-YTDcost ELSE 0 END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]
FROM #TmpSellAmt Rpt, Peostaff staff (nolock)
WHERE staff.companycode = rpt.companycode and staff.staffcode = rpt.staffcode
ORDER BY 1, 2

/*
SELECT staff.teamcode + '/' + rpt.staffcode as staff, PaxTotal as [PaxTotal]
, Price, Dep, Cost, YTDPrice, YTDDep,YTDCost
FROM #TmpSellAmt Rpt, Peostaff staff (nolock)
WHERE staff.companycode = rpt.companycode and staff.staffcode = rpt.staffcode
ORDER BY 1, 2
*/


-- Drop Table
drop table #TmpSellAmt;
drop table #tempStaff;
drop table #tmpMstr;











