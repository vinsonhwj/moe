﻿
CREATE         procedure [dbo].[sp_CalTotalMstrPnr]
	@COMPANYCODE varchar(2) = '',
	@BKGREF varchar(10) = '',
	@LOGIN varchar(10)

as
declare @lstr_MasterPnr varchar(10)
declare @ttlinvamt decimal(20, 2)
declare @ttlinvtax decimal(20, 2)
declare @ttldocamt decimal(20, 2)
declare @ttldoctax decimal(20, 2)
declare @ttlinvcount integer
declare @ttldoccount integer
declare @ttlinvgst decimal(20, 2)
declare @ttldocgst decimal(20, 2)
declare @lstr_Approveby varchar(10)

declare @ttl_mstrpnr_invamt decimal(20, 2)
declare @ttl_mstrpnr_invtax decimal(20, 2)
declare @ttl_mstrpnr_docamt decimal(20, 2)
declare @ttl_mstrpnr_doctax decimal(20, 2)
declare @ttl_mstrpnr_invcount integer
declare @ttl_mstrpnr_doccount integer
declare @ttl_mstrpnr_Approveby varchar(10)
declare @ldt_ApproveOn datetime

declare @localcurr varchar(3)
declare @lstr_segnum varchar(4)
declare @lstr_MaxSeg varchar(4)

declare @ttlreptamt decimal(20, 2)
declare @ttlrepttax decimal(20, 2)
declare @ttl_mstrpnr_reptamt decimal(20, 2)
declare @ttl_mstrpnr_repttax decimal(20, 2)
declare @ttlreptcount integer
declare @ttl_mstrpnr_reptCount integer

SELECT @lstr_segnum = '0001'

print '- Prepare Data # ' + convert(varchar,getdate(),113)

SELECT @lstr_MasterPnr = masterpnr, @lstr_ApproveBy = approveby FROM peomstr (nolock) WHERE companycode = @COMPANYCODE AND bkgref = @BKGREF
SELECT @localcurr = localcurr FROM company (nolock) WHERE companycode = @COMPANYCODE

if @lstr_MasterPnr <> ''
begin

	--Get New Total Amount (Using Masterpnr)
	SELECT @ttlinvamt = isnull(sum(invamt),0)
	, @ttlinvtax = isnull(sum(invtax),0)
	, @ttldocamt = isnull(sum(docamt),0)
	, @ttldoctax = isnull(sum(doctax),0)
	, @ttlinvcount = isnull(sum(invcount),0)
	, @ttldoccount = isnull(sum(doccount),0)
	, @ttlinvgst = isnull(sum(ttlinvgst),0)
	, @ttldocgst = isnull(sum(ttldocgst),0)
	, @ttlReptAmt = isnull(sum(reptamt),0)
	, @ttlReptTax = isnull(sum(repttax),0)
	, @ttlreptcount = isnull(sum(reptcount),0)
	FROM peomstr (nolock) 
	WHERE companycode = @COMPANYCODE and masterpnr = @lstr_MasterPnr
	
	--Get Current Mstrpnr Amount
/*
	SELECT @lstr_segnum = right(rtrim('0000' +convert(varchar,isnull(segnum+1,1))),4)
	, @ldt_Approveon = ApproveOn
	, @ttl_mstrpnr_Approveby = isnull(Approveby,'')
	, @ttl_mstrpnr_invcount = isnull(invcount,0)
	, @ttl_mstrpnr_doccount = isnull(doccount,0)
	, @ttl_mstrpnr_invamt = isnull(invamt,0)
	, @ttl_mstrpnr_Invtax = isnull(invtax,0)
	, @ttl_mstrpnr_Docamt = isnull(docamt,0)
	, @ttl_mstrpnr_Doctax = isnull(doctax,0)
	FROM MstrPnrTotal (nolock)
	WHERE COMPANYCODE = @COMPANYCODE AND masterpnr = @lstr_MasterPnr AND islast = 'Y'
*/	
	
	select @lstr_segnum = right(rtrim('0000' +convert(varchar,isnull(max(segnum)+1,1))),4), 
	@lstr_MaxSeg = right(rtrim('0000' +convert(varchar,isnull(max(segnum),1))),4)
	FROM MstrPnrTotal (nolock)
	WHERE COMPANYCODE = @COMPANYCODE AND masterpnr = @lstr_MasterPnr
	GROUP BY companycode, masterpnr

	SELECT @ldt_Approveon = ApproveOn
	, @ttl_mstrpnr_Approveby = isnull(Approveby,'')
	, @ttl_mstrpnr_invcount = isnull(invcount,0)
	, @ttl_mstrpnr_doccount = isnull(doccount,0)
	, @ttl_mstrpnr_invamt = isnull(invamt,0)
	, @ttl_mstrpnr_Invtax = isnull(invtax,0)
	, @ttl_mstrpnr_Docamt = isnull(docamt,0)
	, @ttl_mstrpnr_Doctax = isnull(doctax,0)
	, @ttl_mstrpnr_reptAmt = isnull(reptamt,0)
	, @ttl_mstrpnr_reptTax = isnull(repttax,0)
	, @ttl_mstrpnr_reptCount = isnull(reptCount,0)
	FROM MstrPnrTotal (nolock)
	WHERE COMPANYCODE = @COMPANYCODE AND masterpnr = @lstr_MasterPnr AND segnum = @lstr_MaxSeg


	print '- Prepare Data End # ' + convert(varchar,getdate(),113) + ' ' + @lstr_segnum

	
	if (@lstr_ApproveBy <> @ttl_mstrpnr_Approveby or @ttlinvcount <> @ttl_mstrpnr_invcount or @ttldoccount <> @ttl_mstrpnr_doccount
	or @ttlinvamt <> @ttl_mstrpnr_invamt or @ttlinvtax <> @ttl_mstrpnr_Invtax or @ttldocamt <> @ttl_mstrpnr_Docamt or @ttldoctax <> @ttl_mstrpnr_Doctax
	or @ttlreptcount <> @ttl_mstrpnr_reptCount or @ttlreptamt <> @ttl_mstrpnr_reptamt or @ttlreptTax <> @ttl_mstrpnr_reptTax
	) OR  @lstr_segnum = '0001'
	begin 
		print 'Process- Start # '  + convert(varchar,getdate(),113)
		if @lstr_segnum <> '0001'
		begin
			UPDATE MstrPnrTotal set ISLAST = null WHERE companycode = @companycode and masterpnr = @lstr_MasterPnr
		end

		INSERT INTO MstrPnrTotal 
		(CompanyCode, Masterpnr, SegNum, InvCount, DocCount, TtlInvGst, TtlDocGst, InvCurr, InvAmt, InvTaxCurr, InvTax, DocCurr, DocAmt, DocTax, IsLast, 
			CreateOn, CreateBy, ApproveOn, ApproveBy, Profit, ReptCurr, ReptAmt, ReptTaxCurr, ReptTax, ReptCount)
		Select @companycode, @lstr_MasterPnr, @lstr_segnum,@ttlinvcount,@ttldoccount
	        , @ttlinvgst,@ttldocgst,@localcurr, @ttlinvamt, @localcurr, @ttlinvtax
	        , @localcurr, @ttldocamt, @ttldoctax, 'Y',getdate(),@LOGIN,@ldt_ApproveOn
	        , @lstr_Approveby, (@ttlinvamt+@ttlinvtax)-(@ttldocamt+@ttldoctax)
		, @localcurr, @ttlReptAmt, @localcurr, @ttlReptTax, @ttlReptCount


		print 'Abnromal Quene - Start # '  + convert(varchar,getdate(),113)

		if ((@ttlinvcount > 0 and @ttldoccount = 0) or (@ttlinvcount = 0 and @ttldoccount > 0)) and not ((@ttlinvamt + @ttlinvtax) = 0 and (@ttldocamt + @ttldoctax = 0))
		begin 
			--Move into Abnormal Quene
			if not exists (select companycode from abnormalque (nolock) where companycode = @companycode and masterpnr = @lstr_MasterPnr)
			begin
				insert into abnormalque
				values
				(@companycode, @lstr_MasterPnr, @ttlinvcount,@ttldoccount, @ttlinvamt, @ttlinvtax
			        , @ttldocamt, @ttldoctax, (@ttlinvamt+@ttlinvtax)-(@ttldocamt+@ttldoctax)
				, case when (@ttlinvamt+@ttlinvtax) = 0 then -100 else (((@ttlinvamt+@ttlinvtax)-(@ttldocamt+@ttldoctax)) / (@ttlinvamt+@ttlinvtax)) * 100 end
				,getdate(),@LOGIN, null, null)	
			end
			else
			begin
				update abnormalque
				set invcount = @ttlinvcount
				, doccount = @ttldoccount
				, invamt = @ttlinvamt
				, invtax = @ttlinvtax
				, docamt = @ttldocamt
				, doctax = @ttldoctax
				, margin = (@ttlinvamt+@ttlinvtax)-(@ttldocamt+@ttldoctax)
				, Yield = case when (@ttlinvamt+@ttlinvtax) = 0 then -100 else (((@ttlinvamt+@ttlinvtax)-(@ttldocamt+@ttldoctax)) / (@ttlinvamt+@ttlinvtax)) * 100 end
				, UpdateOn = getdate()
				, UpdateBy = @login
				where companycode = @companycode
			 	and Masterpnr = @lstr_MasterPnr

			end
		end
		else
		begin
			--Remove From Abnormal Quene
			delete from abnormalque where companycode = @companycode and masterpnr = @lstr_MasterPnr
		end
		print 'Process- End # '  + convert(varchar,getdate(),113)
	end
	
end
