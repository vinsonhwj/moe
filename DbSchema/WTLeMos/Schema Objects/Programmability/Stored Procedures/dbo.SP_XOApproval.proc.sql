﻿
CREATE PROCEDURE [dbo].[SP_XOApproval]
    @CompanyCode CHAR(2) ,
    @OwnerStaff VARCHAR(10) ,
    @ActionStaff VARCHAR(10) ,
    @CurrentXOAmt DECIMAL(16, 2) ,
    @ExceedAmount DECIMAL(16, 2) OUTPUT,
    @LimitAmount DECIMAL(16, 2) OUTPUT
AS 
BEGIN
    SET NOCOUNT ON ;
    IF EXISTS(SELECT TOP 1 ApprovalCode FROM dbo.XOApproval WHERE CompanyCode=@CompanyCode AND ApprovalCode=@ActionStaff)
    BEGIN
		SELECT  @LimitAmount = CASE WHEN @OwnerStaff = @ActionStaff
									   THEN S.SelfValue
									   ELSE S.AppValue
								  END
		FROM    dbo.XOApproval AS A WITH (NOLOCK)
		INNER JOIN dbo.XOApprovalSetup AS S WITH (NOLOCK)
		ON      A.CompanyCode = S.CompanyCode
				AND A.ApprovalLevel = S.ApprovalLevel
		WHERE   A.CompanyCode = @CompanyCode
				AND A.ApprovalCode = @ActionStaff
	END
	ELSE
	BEGIN
		SELECT  @LimitAmount = CASE WHEN @OwnerStaff = @ActionStaff
									   THEN S.SelfValue
									   ELSE S.AppValue
								  END
		FROM    dbo.XOApprovalSetup AS S WITH (NOLOCK)
		WHERE   CompanyCode = @CompanyCode
				AND ApprovalLevel=1
	END	
            
    SET @ExceedAmount = @CurrentXOAmt - @LimitAmount
END
