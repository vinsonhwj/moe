﻿--exec sp_RptAirlineClassSalesProdReport 'wm','2006-11-01','2008-01-16','CX','Q','Y'

CREATE     procedure dbo.sp_RptAirlineClassSalesProdReport
@COMPANY_CODE NCHAR(4),
@START_DATE VARCHAR(25),
@END_DATE VARCHAR(25),
@AIRLINE NVARCHAR(6),
@REPORT_TYPE CHAR(1),
@INCLUDE_WP CHAR(1)

AS

CREATE TABLE #tmpReport
(
	Airline nvarchar(6) null default ''
,	Class nvarchar(2) null default ''
,	BrATktCount decimal(9,0) null default 0
,	BrATktAmt decimal(16,2) null default 0.0
,	BrATktPercent decimal(5,1) null default 0.0
,	BrBTktCount decimal(9,0) null default 0
,	BrBTktAmt decimal(16,2) null default 0.0
,	BrBTktPercent decimal(5,1) null default 0.0
,	BrCTktCount decimal(9,0) null default 0
,	BrCTktAmt decimal(16,2) null default 0.0
,	BrCTktPercent decimal(5,1) null default 0.0
,	BrDTktCount decimal(9,0) null default 0
,	BrDTktAmt decimal(16,2) null default 0.0
,	BrDTktPercent decimal(5,1) default 0.0
,	BrETktCount decimal(9,0) null default 0
,	BrETktAmt decimal(16,2) null default 0.0
,	BrETktPercent decimal(5,1) null default 0.0
,  	RoutingTktTotal decimal(9,0) null default 0
,	RoutingAmtTotal decimal(16,2) null default 0
,	RoutingPercent decimal(5,1) null default 0.0
)

-- Init Value
DECLARE @lstr_BranchCodeA nchar(6)
DECLARE @lstr_BranchCodeB nchar(6)
DECLARE @lstr_BranchCodeC nchar(6)
DECLARE @lstr_BranchCodeD nchar(6)
DECLARE @lstr_BranchCodeE nchar(6)
DECLARE @lstr_TeamCodeWholeSale nchar(6)
DECLARE @lstr_TeamCodeWholeSaleP nchar(6)
DECLARE @lstr_Class nvarchar(2)
DECLARE @lstr_Airline nvarchar(6)
DECLARE @lnum_TktTotal decimal(9,0)
DECLARE @lnum_RoutingTktTotal decimal(9,0)
DECLARE @lnum_TktAmt decimal(16,2)

SELECT @lstr_BranchCodeA = 'H'
SELECT @lstr_BranchCodeB = 'H'
SELECT @lstr_BranchCodeC = 'Y'
SELECT @lstr_BranchCodeD = 'C'
SELECT @lstr_BranchCodeE = 'T'
SELECT @lstr_TeamCodeWholeSale = 'HWT'
SELECT @lstr_TeamCodeWholeSaleP = 'HWP'
--END Init Value
DECLARE @lstr_topSQL varchar(3000)


-- Retrieve Top 100 Routing
CREATE TABLE #tmpAirline
(
	Airline nvarchar(6) null default ''
,	TktTotal decimal(9,0) null default 0
,	TktAmt decimal(16,2) null default 0
)

SELECT @lstr_topSQL = 'INSERT INTO #tmpAirline' +
' SELECT Airline, Count(*) as TktTotal, sum(netfare) as amt ' +
' FROM peotkt (nolock) ' +
' WHERE companycode = ''' + @COMPANY_CODE + '''' +
' AND IssueOn between ''' + @START_DATE + ''' AND ''' + @END_DATE + '''' 
IF LTRIM(RTRIM(@AIRLINE)) <> ''
	SELECT @lstr_topSQL = @lstr_topSQL + 'AND Airline = ''' + @AIRLINE + ''''
SELECT @lstr_topSQL = @lstr_topSQL + ' GROUP BY Airline ' 

PRINT @lstr_topSQL
EXEC(@lstr_topSQL)

--Get Airline
SELECT @lstr_topSQL = 'INSERT INTO #tmpReport ' +
'(Airline, Class, RoutingTktTotal, RoutingAmtTotal, RoutingPercent) ' +
'SELECT rpt.Airline, isnull(tkt.Class,'''') as class, count(*) as TktTotal, sum(tkt.Netfare) as amt, '
IF @REPORT_TYPE = 'Q'
	SELECT @lstr_topSQL = @lstr_topSQL + 'CASE WHEN rpt.TktTotal > 0 THEN (COUNT(tkt.Airline) / rpt.TktTotal) * 100 else 0 end as Per '
ELSE
	SELECT @lstr_topSQL = @lstr_topSQL + 'CASE WHEN rpt.TktAmt > 0 THEN (sum(tkt.Netfare) / rpt.TktAmt) * 100 else 0 end as Per ' 

SELECT @lstr_topSQL = @lstr_topSQL + 'FROM #tmpAirline rpt, peotkt tkt (nolock), peomstr mstr (nolock) ' +
'WHERE  ' +
'tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref ' +
' AND tkt.Airline = rpt.Airline ' +
' AND mstr.companycode = ''' + @COMPANY_CODE + '''' +
' AND tkt.IssueOn between ''' + @START_DATE + ''' AND ''' + @END_DATE + '''' 
IF LTRIM(RTRIM(@AIRLINE)) <> ''
	SELECT @lstr_topSQL = @lstr_topSQL + ' AND rpt.Airline = ''' + @AIRLINE + ''''
SELECT @lstr_topSQL = @lstr_topSQL + ' GROUP BY rpt.Airline, isnull(tkt.Class,''''), rpt.TktAmt, rpt.TktTotal '

PRINT @lstr_topSQL
EXEC(@lstr_topSQL)




-- Head Office (Without WholeSale)
CREATE TABLE #tmpBrA
(
	Airline nvarchar(6) null default ''
,	Class nvarchar(2) null default ''
,	TktTotal decimal(9,0) null default 0
,	amt decimal(16,2) null default 0.0
,	RoutingTktTotal decimal(16,2) null default 0.0
)
IF @INCLUDE_WP = 'Y'
begin
	INSERT INTO #tmpBrA
	SELECT tkt.Airline, isnull(tkt.Class,'') as class, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Airline = rpt.Airline
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeA AND mstr.TeamCode <> @lstr_TeamCodeWholeSale AND mstr.TeamCode <> @lstr_TeamCodeWholeSaleP
	GROUP BY tkt.Airline, rpt.RoutingTktTotal, isnull(tkt.Class,'')
end
else
begin
	INSERT INTO #tmpBrA
	SELECT tkt.Airline, isnull(tkt.Class,'') as class, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Airline = rpt.Airline
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeA AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
	GROUP BY tkt.Airline, rpt.RoutingTktTotal, isnull(tkt.Class,'')
end
DECLARE lcur_BrA cursor for
SELECT Class, TktTotal, amt, Airline
FROM #tmpBrA

open lcur_BrA
fetch lcur_BrA into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
while @@fetch_status = 0
begin
	IF @REPORT_TYPE <> 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrATktCount = @lnum_TktTotal,
		BrATktAmt = @lnum_TktAmt,
		BrATktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrATktCount = @lnum_TktTotal,
		BrATktAmt = @lnum_TktAmt,
		BrATktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline

	END
	fetch lcur_BrA into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
END

CLOSE lcur_BrA
DEALLOCATE lcur_BrA
DROP TABLE #tmpBrA;
--END OF BrA

-- Wholesales
CREATE TABLE #tmpBrB
(
	Class nvarchar(2) null default ''
,	Airline nvarchar(6) null default ''
,	TktTotal decimal(9,0) null default 0
,	amt decimal(16,2) null default 0.0
)
IF @INCLUDE_WP = 'Y'
BEGIN
	INSERT INTO #tmpBrB
	SELECT isnull(tkt.Class,'') as Class, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt
	FROM peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
--	AND tkt.Airline = rpt.Airline
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND mstr.TeamCode = @lstr_TeamCodeWholeSale AND mstr.TeamCode = @lstr_TeamCodeWholeSaleP
	GROUP BY isnull(tkt.Class,''), tkt.Airline
END
ELSE
BEGIN
	INSERT INTO #tmpBrB
	SELECT isnull(tkt.Class,'') as Class, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt
	FROM peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
--	AND tkt.Airline = rpt.Airline
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND mstr.TeamCode = @lstr_TeamCodeWholeSale
	GROUP BY isnull(tkt.Class,''), tkt.Airline
END



DECLARE lcur_BrB cursor for
SELECT Class, TktTotal, amt,  Airline
FROM #tmpBrB

open lcur_BrB
fetch lcur_BrB into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
while @@fetch_status = 0
begin
	print @lstr_Airline + ' - ' + @lstr_Class
	print convert(varchar,@lnum_TktTotal)
	print convert(varchar,@lnum_TktAmt)
	IF @REPORT_TYPE <> 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrBTktCount = @lnum_TktTotal,
		BrBTktAmt = @lnum_TktAmt,
		BrBTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrBTktCount = @lnum_TktTotal,
		BrBTktAmt = @lnum_TktAmt,
		BrBTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline

	END
	print 'done'
	fetch lcur_BrB into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
END

CLOSE lcur_BrB
DEALLOCATE lcur_BrB
DROP TABLE #tmpBrB;
--END OF BrB

-- Yat Fat
SELECT isnull(tkt.Class,'') as class, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt
INTO #tmpBrC
FROM peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeC AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY isnull(tkt.Class,''), tkt.Airline

DECLARE lcur_BrC cursor for
SELECT Class, TktTotal, amt, Airline
FROM #tmpBrC

open lcur_BrC
fetch lcur_BrC into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
while @@fetch_status = 0
begin

	IF @REPORT_TYPE <> 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrCTktCount = @lnum_TktTotal,
		BrCTktAmt = @lnum_TktAmt,
		BrCTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrCTktCount = @lnum_TktTotal,
		BrCTktAmt = @lnum_TktAmt,
		BrCTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline

	END

	fetch lcur_BrC into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
END

CLOSE lcur_BrC
DEALLOCATE lcur_BrC
DROP TABLE #tmpBrC;
--END OF BrC

-- BranchD
SELECT isnull(tkt.Class,'') as class, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt
INTO #tmpBrD
FROM peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeD AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY isnull(tkt.Class,''), tkt.Airline

DECLARE lcur_BrD cursor for
SELECT Class, TktTotal, amt, Airline
FROM #tmpBrD

open lcur_BrD
fetch lcur_BrD into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
while @@fetch_status = 0
begin

	IF @REPORT_TYPE <> 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrDTktCount = @lnum_TktTotal,
		BrDTktAmt = @lnum_TktAmt,
		BrDTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrDTktCount = @lnum_TktTotal,
		BrDTktAmt = @lnum_TktAmt,
		BrDTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline

	END

	fetch lcur_BrD into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
END

CLOSE lcur_BrD
DEALLOCATE lcur_BrD
DROP TABLE #tmpBrD;
--END OF BrD

-- BranchE
SELECT isnull(tkt.Class,'') as class, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt
INTO #tmpBrE
FROM peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeE AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY isnull(tkt.Class,''), tkt.Airline

DECLARE lcur_BrE cursor for
SELECT Class, TktTotal, amt, Airline
FROM #tmpBrE

open lcur_BrE
fetch lcur_BrE into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
while @@fetch_status = 0
begin

	IF @REPORT_TYPE <> 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrETktCount = @lnum_TktTotal,
		BrETktAmt = @lnum_TktAmt,
		BrETktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrETktCount = @lnum_TktTotal,
		BrETktAmt = @lnum_TktAmt,
		BrETktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Class = @lstr_Class
		AND Airline = @lstr_Airline

	END

	fetch lcur_BrE into @lstr_Class, @lnum_TktTotal, @lnum_TktAmt, @lstr_Airline
END

CLOSE lcur_BrE
DEALLOCATE lcur_BrE
DROP TABLE #tmpBrE;
--END OF BrE

SELECT rpt.*
INTO #tmpResult
FROM #tmpReport rpt, #tmpAirline Airline
WHERE Airline.Airline = Airline.Airline
order by Airline.TktAmt desc, rpt.RoutingAmtTotal desc;

SELECT rpt.*
FROM #tmpReport rpt, #tmpAirline Airline
WHERE rpt.Airline = Airline.Airline
order by Airline.TktAmt desc, rpt.RoutingAmtTotal desc;

DROP TABLE #tmpAirline;
DROP TABLE #tmpResult;
DROP TABLE #tmpReport;







