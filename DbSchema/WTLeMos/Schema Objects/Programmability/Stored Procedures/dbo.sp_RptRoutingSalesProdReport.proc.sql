﻿



CREATE     procedure dbo.sp_RptRoutingSalesProdReport
@COMPANY_CODE NCHAR(4),
@START_DATE VARCHAR(25),
@END_DATE VARCHAR(25),
@ROUTING NVARCHAR(200),
@SHOW_RECORDS varchar(3),
@REPORT_TYPE CHAR(1),
@INCLUDE_WP CHAR(1)
AS


CREATE TABLE #tmpReport
(
	Seqno int null default 0
,	Routing NVARCHAR(130) null default ''
,	BrATktCount decimal(9,0) null default 0
,	BrATktAmt decimal(16,2) null default 0.0
,	BrATktPercent decimal(6,2) null default 0.0
,	BrBTktCount decimal(9,0) null default 0
,	BrBTktAmt decimal(16,2) null default 0.0
,	BrBTktPercent decimal(6,2) null default 0.0
,	BrCTktCount decimal(9,0) null default 0
,	BrCTktAmt decimal(16,2) null default 0.0
,	BrCTktPercent decimal(6,2) null default 0.0
,	BrDTktCount decimal(9,0) null default 0
,	BrDTktAmt decimal(16,2) null default 0.0
,	BrDTktPercent decimal(6,2) default 0.0
,	BrETktCount decimal(9,0) null default 0
,	BrETktAmt decimal(16,2) null default 0.0
,	BrETktPercent decimal(6,2) null default 0.0
,  	RoutingTktTotal decimal(9,0) null default 0
,	RoutingAmtTotal decimal(16,2) null default 0
,	RoutingPercent decimal(6,2) null default 0.0
)

-- Init Value
DECLARE @lstr_BranchCodeA nchar(6)
DECLARE @lstr_BranchCodeB nchar(6)
DECLARE @lstr_BranchCodeC nchar(6)
DECLARE @lstr_BranchCodeD nchar(6)
DECLARE @lstr_BranchCodeE nchar(6)
DECLARE @lstr_TeamCodeWholeSale nchar(6)
DECLARE @lstr_TeamCodeWholeSaleP nchar(6)
DECLARE @lstr_Routing NVARCHAR(130)
DECLARE @lnum_TktTotal decimal(9,0)
DECLARE @lnum_RoutingTktTotal decimal(9,0)
DECLARE @lnum_RoutingAmtTotal decimal(16,2)
DECLARE @lnum_TktAmt decimal(16,2)
DECLARE @lnum_TktCount decimal(16,2)
DECLARE @lnum_RoutingTotalCount decimal(16,2)

SELECT @lstr_BranchCodeA = 'H'
SELECT @lstr_BranchCodeB = 'H'
SELECT @lstr_BranchCodeC = 'Y'
SELECT @lstr_BranchCodeD = 'C'
SELECT @lstr_BranchCodeE = 'T'
SELECT @lstr_TeamCodeWholeSale = 'HWT'
SELECT @lstr_TeamCodeWholeSaleP = 'HWP'
--END Init Value

DECLARE @lstr_topSQL varchar(500)
--PRINT 'START RET'
-- Retrieve Top 100 Routing
IF LTRIM(RTRIM(@ROUTING)) <> ''
BEGIN 
	INSERT INTO #tmpReport
	(Routing, RoutingTktTotal, RoutingAmtTotal)
	SELECT Routing, Count(*) as TktTotal, sum(netfare) as amt
	FROM peotkt (nolock)
	WHERE COMPANYCODE = @COMPANY_CODE
	AND IssueOn BETWEEN @START_DATE AND @END_DATE
	AND LTRIM(Rtrim(Routing)) = LTRIM(RTRIM(@ROUTING))
	GROUP BY Routing
END
ELSE
BEGIN
	SELECT @lstr_topSQL = 'INSERT INTO #tmpReport ' +
	'(Routing, RoutingTktTotal, RoutingAmtTotal) ' +
	' SELECT TOP ' + @SHOW_RECORDS + ' Routing, Count(*) as TktTotal, sum(netfare) as amt ' +
	' FROM peotkt (nolock) ' +
	' WHERE companycode = ''' + @COMPANY_CODE + '''' +
	' AND IssueOn between ''' + @START_DATE + ''' AND ''' + @END_DATE + '''' +
	' GROUP BY Routing ' 
	
	IF @REPORT_TYPE = 'Q'
		SELECT @lstr_topSQL = @lstr_topSQL + ' ORDER BY 2 DESC'
	ELSE
		SELECT @lstr_topSQL = @lstr_topSQL + ' ORDER BY 3 DESC'
	
	EXEC(@lstr_topSQL)
END
--Update Routing Percentage
SELECT @lnum_RoutingAmtTotal = SUM(RoutingAmtTotal), @lnum_RoutingTotalCount = sum(RoutingTktTotal) FROM #tmpReport

DECLARE lcur_Routing cursor for
SELECT Routing, RoutingAmtTotal, RoutingTktTotal
FROM #tmpReport
--PRINT 'START TOTAL'
open lcur_Routing
fetch lcur_Routing into @lstr_Routing, @lnum_TktAmt, @lnum_TktCount
while @@fetch_status = 0
begin

	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		RoutingPercent = CASE WHEN @lnum_RoutingTotalCount <= 0 THEN 0 ELSE ((@lnum_TktCount/@lnum_RoutingTotalCount) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		RoutingPercent = CASE WHEN @lnum_RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/@lnum_RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_Routing into @lstr_Routing, @lnum_TktAmt, @lnum_TktCount
END

CLOSE lcur_Routing
DEALLOCATE lcur_Routing
--End of Update Routing Percentage
--PRINT 'START A'
-- Head Office (Without WholeSale)
CREATE TABLE #tmpBrA
(
	Routing NVARCHAR(130) null default ''
,	TktTotal decimal(9,0) null default 0
,	amt decimal(16,2) null default 0.0
,	RoutingAmtTotal decimal(16,2) null default 0.0
)
IF @INCLUDE_WP = 'Y'
BEGIN
	INSERT INTO #tmpBrA
	SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt, rpt.RoutingAmtTotal
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Routing = rpt.Routing
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeA AND mstr.TeamCode <> @lstr_TeamCodeWholeSale AND mstr.TeamCode <> @lstr_TeamCodeWholeSaleP
	GROUP BY rpt.Routing, rpt.RoutingAmtTotal
END
ELSE
BEGIN
	INSERT INTO #tmpBrA
	SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt, rpt.RoutingAmtTotal
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Routing = rpt.Routing
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeA AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
	GROUP BY rpt.Routing, rpt.RoutingAmtTotal
END

DECLARE lcur_BrA cursor for
SELECT Routing, TktTotal, amt, RoutingAmtTotal
FROM #tmpBrA

open lcur_BrA
fetch lcur_BrA into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingAmtTotal
while @@fetch_status = 0
begin

	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrATktCount = @lnum_TktTotal,
		BrATktAmt = @lnum_TktAmt,
		BrATktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrATktCount = @lnum_TktTotal,
		BrATktAmt = @lnum_TktAmt,
		BrATktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_BrA into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingAmtTotal
END

CLOSE lcur_BrA
DEALLOCATE lcur_BrA
DROP TABLE #tmpBrA;
--END OF BrA


-- BrB
CREATE TABLE #tmpBrB
(
	Routing NVARCHAR(130) null default ''
,	TktTotal decimal(9,0) null default 0
,	amt decimal(16,2) null default 0.0
)
IF @INCLUDE_WP = 'Y'
BEGIN
	INSERT INTO #tmpBrB
	SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Routing = rpt.Routing
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND mstr.TeamCode = @lstr_TeamCodeWholeSale AND mstr.TeamCode = @lstr_TeamCodeWholeSaleP
	GROUP BY rpt.Routing
END
ELSE
BEGIN
	INSERT INTO #tmpBrB
	SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt
	FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
	WHERE 
	tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
	AND tkt.Routing = rpt.Routing
	AND mstr.companycode = @COMPANY_CODE
	AND tkt.IssueOn between @START_DATE AND @END_DATE
	AND mstr.TeamCode = @lstr_TeamCodeWholeSale
	GROUP BY rpt.Routing
END

DECLARE lcur_BrB cursor for
SELECT Routing, TktTotal, amt 
FROM #tmpBrB
--PRINT 'START B'
open lcur_BrB
fetch lcur_BrB into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
while @@fetch_status = 0
begin
	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrBTktCount = @lnum_TktTotal,
		BrBTktAmt = @lnum_TktAmt,
		BrBTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrBTktCount = @lnum_TktTotal,
		BrBTktAmt = @lnum_TktAmt,
		BrBTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_BrB into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
END

CLOSE lcur_BrB
DEALLOCATE lcur_BrB
DROP TABLE #tmpBrB;
--END OF BrB

-- Yat Fat
--PRINT 'START C1'
SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt
INTO #tmpBrC
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeC
GROUP BY rpt.Routing

DECLARE lcur_BrC cursor for
SELECT Routing, TktTotal, amt 
FROM #tmpBrC
--PRINT 'START C'
open lcur_BrC
fetch lcur_BrC into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
while @@fetch_status = 0
begin
	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrCTktCount = @lnum_TktTotal,
		BrCTktAmt = @lnum_TktAmt,
		BrCTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrCTktCount = @lnum_TktTotal,
		BrCTktAmt = @lnum_TktAmt,
		BrCTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_BrC into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
END

CLOSE lcur_BrC
DEALLOCATE lcur_BrC
DROP TABLE #tmpBrC;
--END OF BrC

-- BrD
SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt
INTO #tmpBrD
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeD
GROUP BY rpt.Routing

DECLARE lcur_BrD cursor for
SELECT Routing, TktTotal, amt 
FROM #tmpBrD

--PRINT 'START D'
open lcur_BrD
fetch lcur_BrD into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
while @@fetch_status = 0
begin
	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrDTktCount = @lnum_TktTotal,
		BrDTktAmt = @lnum_TktAmt,
		BrDTktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrDTktCount = @lnum_TktTotal,
		BrDTktAmt = @lnum_TktAmt,
		BrDTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_BrD into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
END

CLOSE lcur_BrD
DEALLOCATE lcur_BrD
DROP TABLE #tmpBrD;
--END OF BrD

-- BrE
SELECT rpt.Routing, Count(*) as TktTotal, sum(tkt.netfare) as amt
INTO #tmpBrE
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeE
GROUP BY rpt.Routing

DECLARE lcur_BrE cursor for
SELECT Routing, TktTotal, amt 
FROM #tmpBrE
--PRINT 'START E'
open lcur_BrE
fetch lcur_BrE into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
while @@fetch_status = 0
begin
	IF @REPORT_TYPE = 'Q'
	BEGIN
		UPDATE 	#tmpReport SET
		BrETktCount = @lnum_TktTotal,
		BrETktAmt = @lnum_TktAmt,
		BrETktPercent = CASE WHEN #tmpReport.RoutingTktTotal <= 0 THEN 0 ELSE ((@lnum_TktTotal/#tmpReport.RoutingTktTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END
	ELSE
	BEGIN
		UPDATE 	#tmpReport SET
		BrETktCount = @lnum_TktTotal,
		BrETktAmt = @lnum_TktAmt,
		BrETktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
		WHERE 
		Routing = @lstr_Routing
	END

	fetch lcur_BrE into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt
END

CLOSE lcur_BrE
DEALLOCATE lcur_BrE
DROP TABLE #tmpBrE;
--END OF BrE
--PRINT 'START S'

-- UPDATE SEQNO
DECLARE @lnum_Seqno int 
DECLARE lcur_Seq cursor for
SELECT Routing FROM #tmpReport

SELECT @lnum_Seqno = 0

open lcur_Seq
fetch lcur_Seq into @lstr_Routing
while @@fetch_status = 0
BEGIN
	SELECT @lnum_Seqno = @lnum_Seqno + 1
	UPDATE #tmpReport 
	SET seqno = @lnum_Seqno
	WHERE routing = @lstr_Routing

	fetch lcur_Seq into @lstr_Routing
END

CLOSE lcur_Seq
DEALLOCATE lcur_Seq
--PRINT 'START T'

SELECT *
FROM #tmpReport;

