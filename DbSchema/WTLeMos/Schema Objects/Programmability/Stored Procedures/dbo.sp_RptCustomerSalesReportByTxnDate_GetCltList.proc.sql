﻿



create    procedure dbo.sp_RptCustomerSalesReportByTxnDate_GetCltList
@COMPANY_CODE varchar(2),
@CLT_CODE varchar(50),
@CLT_MODE CHAR(1),
@STAFF_CODE varchar(10)
as

if @CLT_MODE = 'M'
begin

	DECLARE @lstrMstrClientCode varchar(20);
	
	if exists (select * from MstrClientForRpt WHERE companycode = @COMPANY_CODE and mstrclientcode=@CLT_CODE)
	begin
		SELECT @lstrMstrClientCode = @CLT_CODE
	end
	else
	begin
		if exists (select * from MstrClientForRpt WHERE companycode = @COMPANY_CODE and cltcode=@CLT_CODE)
		begin 
			SELECT @lstrMstrClientCode = MstrClientCode FROM MstrClientForRpt WHERE companycode = @COMPANY_CODE and cltcode=@CLT_CODE
		end
		ELSE
		begin
			INSERT INTO #TmpClient VALUES (@CLT_CODE)
		end
	end
	
	if rtrim(ltrim(@lstrMstrClientCode)) <> ''
	begin
		INSERT INTO #TmpClient 
		Select c.CLTCODE	  
		from customer c (nolock), viewOther v (nolock), MstrClientForRpt r (nolock) 
		where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
		AND c.companycode = r.companycode and c.cltcode = r.cltcode
		AND IsNull(v.ViewCompany, '') = ''  
		AND r.mstrclientcode = @lstrMstrClientCode
		UNION 
		Select c.CLTCODE
		from customer c (nolock), viewOther v (nolock), MstrClientForRpt r (nolock) 
		where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
		AND c.CompanyCode=v.ViewCompany  
		AND c.companycode = r.companycode and c.cltcode = r.cltcode
		AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') = '' 
		AND r.mstrclientcode = @lstrMstrClientCode
		UNION 
		Select c.CLTCODE
		from customer c (nolock), viewOther v (nolock), MstrClientForRpt r (nolock) 
		where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
		AND c.CompanyCode=v.ViewCompany and c.BranchCode=v.ViewBranch 
		AND c.companycode = r.companycode and c.cltcode = r.cltcode
		AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') <> '' and IsNull(v.ViewTeam, '') = '' 
		AND r.mstrclientcode = @lstrMstrClientCode
		UNION 
		Select c.CLTCODE
		from customer c (nolock), viewOther v (nolock), MstrClientForRpt r (nolock) 
		where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
		AND c.CompanyCode=v.ViewCompany and c.BranchCode=v.ViewBranch  AND c.TeamCode = v.ViewTeam
		AND c.companycode = r.companycode and c.cltcode = r.cltcode
		AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') <> '' and IsNull(v.ViewTeam, '') <> '' 
		AND r.mstrclientcode = @lstrMstrClientCode
	end

end
else
begin

	INSERT INTO #TmpClient 
	Select c.CLTCODE	  
	from customer c (nolock), viewOther v (nolock)
	where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
	AND IsNull(v.ViewCompany, '') = ''  
	AND c.cltcode = @CLT_CODE
	UNION 
	Select c.CLTCODE
	from customer c (nolock), viewOther v (nolock)
	where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
	AND c.CompanyCode=v.ViewCompany  
	AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') = '' 
	AND c.cltcode = @CLT_CODE
	UNION 
	Select c.CLTCODE
	from customer c (nolock), viewOther v (nolock)
	where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
	AND c.CompanyCode=v.ViewCompany and c.BranchCode=v.ViewBranch 
	AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') <> '' and IsNull(v.ViewTeam, '') = '' 
	AND c.cltcode = @CLT_CODE
	UNION 
	Select c.CLTCODE
	from customer c (nolock), viewOther v (nolock)
	where v.login = @STAFF_CODE and v.companycode = @COMPANY_CODE 
	AND c.CompanyCode=v.ViewCompany and c.BranchCode=v.ViewBranch  AND c.TeamCode = v.ViewTeam
	AND IsNull(v.ViewCompany, '') <> '' and IsNull(v.ViewBranch, '') <> '' and IsNull(v.ViewTeam, '') <> '' 
	AND c.cltcode = @CLT_CODE

end





