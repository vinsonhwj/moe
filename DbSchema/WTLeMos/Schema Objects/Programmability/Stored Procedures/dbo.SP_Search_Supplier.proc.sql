﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Search_Supplier]
	-- Add the parameters for the stored procedure here
    @companycode CHAR(2) ,
    @suppcode VARCHAR(10) ,
    @suppname NVARCHAR(100)  ,
    @suppcity VARCHAR(10)
AS -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
    SET NOCOUNT ON ;

    -- Insert statements for procedure here
--    DECLARE @sql VARCHAR(2000)
--    IF @suppcity <> '' 
--        BEGIN
--            SELECT  @sql = 'select s.* from supplier s,city_EN_US  c'
--
--            SELECT  @sql = @sql + '  where s.companycode = ''' + @companycode
--                    + '''and s.city = c.citycode '
--            SELECT  @sql = @sql + 'and s.city=''' + @suppcity + ''''
--        END
--    ELSE 
--        BEGIN
--            SELECT  @sql = 'select s.* from supplier s' 
--            SELECT  @sql = @sql + ' where s.companycode = ''' + @companycode
--                    + ''''
--        END
--             
--    IF ISNULL(@suppcode,'') <> ''
--        OR ISNULL(@suppname,'') <> '' 
--        BEGIN
--            SELECT  @sql = @sql + ' and (s.suppcode=''' + @suppcode
--                    + ''' or s.suppname like ''%' + @suppname + '%'')'
--          
--        END
--
--    
--    SELECT  @sql = @sql + 'order by s.suppcode'   
--      
--   -- SELECT @sql
--    
--    EXEC(@sql)
    
    
     SELECT s.*
     FROM   supplier s
            LEFT JOIN city_EN_US c ON s.COMPANYCODE = c.COUNTRYCODE
                                      AND s.CITY = c.CITYCODE
     WHERE  s.companycode = '' + @companycode+ ''
            AND ISNULL(s.city, '') = ( CASE WHEN ISNULL(@suppcity, '') = ''
                                            THEN ISNULL(s.city, '')
                                            ELSE @suppcity
                                       END )
            AND ( SUPPCODE = ( CASE WHEN ISNULL(@suppcode, '') = ''
                                    THEN SUPPCODE
                                    ELSE @suppcode
                               END )
                  AND  suppname LIKE ( CASE WHEN ISNULL(@suppname, '') = ''
                                          THEN suppname
                                          ELSE '%' + @suppname + '%'
                                     END )
                )
     ORDER BY s.suppcode               
   









