﻿
create procedure dbo.sp_RptStatementItemisationReport
@COMPANYCODE varchar(2),
@CLTCODE varchar(10),
@START_DATE varchar(10),
@END_DATE varchar(25)

AS



CREATE TABLE #tmpResult
(
	Companycode 	varchar(2) not null default ''
,	Invnum		varchar(10) not null default ''
,	Teamcode 	varchar(10) not null default ''
,	Staffcode 	varchar(10) not null default ''
,	Createon 	datetime not null
,	Cltcode		varchar(10) not null default ''
, 	ServType 	varchar(3) not null default ''
,	Traveller	nvarchar(100) not null default ''
,	Contactperson	nvarchar(200) not null default ''
,	COD1		nvarchar(40) not null default '-'
,	COD2		nvarchar(40) not null default '-'
,	COD3		nvarchar(40) not null default '-'
,	Routing		varchar(200) not null default '-'
,	Other		varchar(500) not null default 'MISC'
,	Departure 	varchar(15) not null default ''
,	Supplier	varchar(20) not null default 'MISC'
,	Amount		decimal(16,2) not null default 0
,	AmtFee		decimal(16,2) not null default 0
,	Ticket 		varchar(10) not null default 'MISC'
,	Class		varchar(10) not null default 'MISC'
,	Flight		varchar(10) not null default 'MISC'
)


--AIR
INSERT INTO #tmpResult
(Companycode, Staffcode, Teamcode, Cltcode, Invnum, Traveller, Contactperson, COD1, COD2, COD3, ServType, Routing, Departure, Amount, AmtFee, Createon)
SELECT inv.companycode, inv.staffcode, inv.teamcode, inv.cltcode, inv.Invnum
	, mstr.firstpax, mstr.contactperson, isnull(inv.cod1,''), isnull(inv.cod2,''), isnull(inv.cod3,'')
	, seg.servtype, replace(right(rtrim(seg.servname), 9), '   ', '/')  as routing
	, substring(seg.servdesc, 8, 9) AS departure, detail.qty * detail.rmnts * (detail.sellamt + detail.taxamt)
	, detail.qty * detail.rmnts * (detail.amtfee), inv.createon
FROM peomstr mstr (nolock), peoinv inv (nolock), peoinvseg seg (nolock), peoinvdetail detail (nolock)
WHERE inv.companycode = mstr.companycode AND inv.bkgref = mstr.bkgref
AND seg.companycode = inv.companycode AND seg.invnum = inv.invnum
AND detail.companycode = seg.companycode AND detail.invnum = seg.invnum AND detail.segnum = seg.segnum
AND mstr.companycode = @COMPANYCODE
AND inv.createon BETWEEN @START_DATE AND @END_DATE
AND inv.cltcode = @CLTCODE
AND seg.ServType = 'AIR'
AND isnull(inv.void2jba,'') = ''

--OTHER
INSERT INTO #tmpResult
(Companycode, Staffcode, Teamcode, Cltcode, Invnum, Traveller, Contactperson, COD1, COD2, COD3, ServType, Other, Departure, Amount, Createon)
SELECT inv.companycode, inv.staffcode, inv.teamcode, inv.cltcode, inv.Invnum
	, mstr.firstpax,  mstr.contactperson, isnull(inv.cod1,''), isnull(inv.cod2,''), isnull(inv.cod3,''), seg.servtype
	, isnull( seg.servdesc,(select description from othersegtype_EN_US (READPAST) where segtype = seg.segtype))
	, replace(convert(char, mstr.DepartDate, 106), ' ', '') AS departure, detail.qty * detail.rmnts * (detail.sellamt + detail.taxamt + detail.amtfee)
	, inv.createon
FROM peomstr mstr (nolock), peoinv inv (nolock), peoinvseg seg (nolock), peoinvdetail detail (nolock)
WHERE inv.companycode = mstr.companycode AND inv.bkgref = mstr.bkgref
AND seg.companycode = inv.companycode AND seg.invnum = inv.invnum
AND detail.companycode = seg.companycode AND detail.invnum = seg.invnum AND detail.segnum = seg.segnum
AND mstr.companycode = @COMPANYCODE
AND inv.createon BETWEEN @START_DATE AND @END_DATE
AND inv.cltcode = @CLTCODE
AND seg.ServType = 'OTH'
AND isnull(inv.void2jba,'') = ''

--HOTEL
INSERT INTO #tmpResult
(Companycode, Staffcode, Teamcode, Cltcode, Invnum, Traveller, Contactperson, COD1, COD2, COD3, ServType, Other, Departure, Amount, Createon)
SELECT inv.companycode, inv.staffcode, inv.teamcode, inv.cltcode, inv.Invnum
	, mstr.firstpax,  mstr.contactperson, isnull(inv.cod1,''), isnull(inv.cod2,''), isnull(inv.cod3,''), seg.servtype
	, 'Hotel', left(seg.servdesc, 9) AS departure, detail.sellamt + detail.taxamt + detail.amtfee
	, inv.createon
FROM peomstr mstr (nolock), peoinv inv (nolock), peoinvseg seg (nolock), peoinvdetail detail (nolock)
WHERE inv.companycode = mstr.companycode AND inv.bkgref = mstr.bkgref
AND seg.companycode = inv.companycode AND seg.invnum = inv.invnum
AND detail.companycode = seg.companycode AND detail.invnum = seg.invnum AND detail.segnum = seg.segnum
AND mstr.companycode = @COMPANYCODE
AND inv.createon BETWEEN @START_DATE AND @END_DATE
AND inv.cltcode = @CLTCODE
AND seg.ServType = 'HTL'
AND isnull(inv.void2jba,'') = ''


SELECT * FROM #tmpResult order by Createon, Invnum


DROP TABLE #tmpResult





