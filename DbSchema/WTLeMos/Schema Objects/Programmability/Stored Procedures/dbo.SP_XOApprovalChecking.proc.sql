﻿

CREATE PROCEDURE [dbo].[SP_XOApprovalChecking]
    @CompanyCode CHAR(2) ,
    @DataType CHAR(1), --S=StaffCode(OwnerStaff) B=Booking No
    @DataValue VARCHAR(10) ,
    @ActionStaff VARCHAR(10) ,
    @CurrentXOAmt DECIMAL(16, 2),
    @OpenXOAmount DECIMAL(16, 2) OUTPUT,
    @ExceedAmount DECIMAL(16, 2) OUTPUT,
    @LimitAmount  DECIMAL(16, 2) OUTPUT
AS 
BEGIN
    SET NOCOUNT ON ;
    
    IF @DataType='B'
    BEGIN
		SELECT @DataValue=STAFFCODE FROM dbo.PEOMSTR (NOLOCK) WHERE COMPANYCODE=@CompanyCode AND BKGREF=@DataValue
    END	
    
    EXEC dbo.SP_XOOpenBookingAmount @CompanyCode, @DataValue, @OpenXOAmount OUTPUT
    
    SELECT @CurrentXOAmt=@CurrentXOAmt+@OpenXOAmount
    
    EXEC dbo.SP_XOApproval @CompanyCode, @DataValue, @ActionStaff, @CurrentXOAmt, @ExceedAmount OUTPUT, @LimitAmount OUTPUT
    
END

