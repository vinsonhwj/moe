﻿
Create Procedure dbo.SP_Disable_InvoiceIssueRights
AS
BEGIN 
	Select * into dbo.Access_IssInv From Access
	Where CompanyCode='SG' and ModuleCode='ISSINV' and RightsCode In ('ISSUE','VOID')
	
	Delete From Access
	Where CompanyCode='SG' and ModuleCode='ISSINV' and RightsCode In ('ISSUE','VOID')
END


