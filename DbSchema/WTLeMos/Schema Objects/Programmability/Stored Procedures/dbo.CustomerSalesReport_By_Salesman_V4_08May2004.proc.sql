﻿CREATE PROCEDURE CustomerSalesReport_By_Salesman_V4_08May2004
-- Customer Sales Report By NXT
@StartDate CHAR(10),
@EndDate CHAR(10),
@SalesMan VARCHAR(10) = '%',
@cltCode VarChar(10) = '%'
AS
BEGIN
SELECT        I.Bkgref, i. CLTCODE,
                              (SELECT        Sum(SellAmt)
                                FROM              PeoInv INV
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' AND  Inv.cltCode= i.cltcode
				AND convert(char(10), INV.CreateOn, 111) BETWEEN @StartDate AND @EndDate 
				 ) 
                          AS [Total Invoice],
                              (SELECT         SUM(SellAmt)
                                FROM              PeoInv VINV
                                WHERE          I.BkgRef = 	VINV.Bkgref AND SubString(vinv.InvNum, 2, 1) = 'C'  AND  vInv.cltCode = i.cltcode
--and inv.teamcode = i.teamcode
				AND convert(char(10), VINV.CreateOn, 111) BETWEEN @StartDate AND @EndDate  ) 
                          AS [Total Credit Note],
		( Select Sum(sellAmt) 
	               From peoinv inv 
		 Where inv.bkgref=I.bkgRef AND convert(char(10), INV.CreateOn, 111) BETWEEN @StartDate AND @EndDate 
		AND inv.voidon is null AND Inv.cltCode=i.cltCode And SubString(inv.InvNum, 2, 1) = 'I'
                           ) As costNo,		
		( Select Sum(sellAmt) 
	               From peoinv inv 
		 Where inv.bkgref=I.bkgRef AND convert(char(10), INV.CreateOn, 111) BETWEEN @StartDate AND @EndDate 
		AND inv.voidon is null AND SubString(inv.InvNum, 2, 1) = 'I'
                           ) As TotalcostNo,		
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoMco MCO
                                WHERE          I.BkgRef = MCO.Bkgref  AND VoidOn IS NULL
				AND convert(char(10), MCO.CreateOn, 111) BETWEEN @StartDate AND @EndDate)  AS [Total MCO],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoVch V
                                WHERE          I.BkgRef = V.Bkgref  AND VoidOn IS NULL
				AND convert(char(10), V.CreateOn, 111) BETWEEN @StartDate AND @EndDate)  AS [Total Voucher],
                              (SELECT         SUM(HKDAmt)
                                FROM              PeoXO X
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND convert(char(10), X.CreateOn, 111) BETWEEN @StartDate AND @EndDate)  AS [Total XO],
                              (SELECT         SUM(NetFare+TotalTax)
                                FROM              PeoTKT T
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND convert(char(10), T.CreateOn, 111) BETWEEN @StartDate AND @EndDate) AS [Total TKT],
                              (SELECT         COUNT(*)
                                FROM              PeoPax P
                                WHERE          I.BkgRef = P.Bkgref AND VoidOn IS NULL
				AND convert(char(10), P.CreateOn, 111) BETWEEN @StartDate AND @EndDate ) AS [Total Pax]
INTO #BookingSum
FROM              dbo.peoinv I
WHERE	 I.cltCode in (select cltcode from salesman where salesname like  @Salesman  and cltCode like @cltcode)
and I.bkgref in (Select distinct bkgref from peoinv where convert(char(10), I.CreateOn, 111)  BETWEEN @StartDate AND @EndDate)
AND convert(char(10), I.CreateOn, 111)  BETWEEN @StartDate AND @EndDate
and companyCode='SG'
GROUP BY I.BKGREF, I.CLTCODE
--SELECT * FROM #BOOKINGSUM 
Select a.cltcode,a.bkgref, b.teamcode,  b.staffCode,convert(char(11), b.departdate,113) as departDate, 
SUM([Total Pax]) AS [Total Pax],
CAST(
ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) 
 AS Decimal(12,2) ) [Price],
CAST(
(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0)+ISNULL(SUM( [Total MCO]), 0))
*
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
Else
	Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
End
 AS Decimal(12,2) 
) [Cost],
CAST(
	(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)
   	  *
	  CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
 	  Else
		Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	 End
                )
	 AS Decimal(12,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(
                 ( ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)) )
	    *
	   CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
	   Else
  		Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	  End
                 = 0 THEN 
	CAST(
		0
	 AS Decimal(12,2) )
WHEN 	Convert(Decimal,ISNULL(SUM([Total Invoice]), 0))  -  Convert(Decimal,ISNULL(SUM([Total Credit Note]), 0)) <1 THEN
	CAST(
		-100
	 AS Decimal(12,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)) 
	             *
		   CASE WHEN Max(TotalcostNo)=0 Then
			Convert(Decimal,1)
		   Else
  			Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
		  End
	) /
		(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  * 100
	AS Decimal(12,2) )
END
,
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
ELSE
	Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
END AS proportion
INTO #BookingSumDtl
FROM 
 #BookingSum a , peomstr b
WHERE a.bkgref = b.bkgref
AND ISNULL(a.[Total Invoice], 0)  -  ISNULL(a.[Total Credit Note], 0) >0
GROUP BY a.cltcode,a.bkgref, b.teamcode, b.departdate, b.staffCode 
ORDER BY a.cltcode, a.bkgref, b.teamcode, b.staffCode
SELECT C.cltName,A.cltcode AS [Client Code],SUM(Price) AS Price,SUM(cost) AS Cost,  SUM([Total Pax]) AS [Total Pax],
	 SUM(Price)-Sum(Cost) AS [Margin], (SUM(Price)-Sum(Cost)) / Sum(Price) * 100 AS [Yield(%)]
FROM #BookingSumDtl a, Customer c (noLock)
WHERE A.cltCode = C.cltCode
GROUP BY A.cltCode, C.cltName
DROP TABLE #BookingSum
DROP TABLE #BookingSumDtl
END
