﻿



CREATE PROCEDURE SP_INVITIN
	@companycode varchar(2),
	@invnum varchar(10),
	@output_itin VARCHAR(50) OUTPUT
AS
BEGIN
	declare @tmpItin varchar(50)
	-- declare @output_itin varchar(50)
	set @output_itin = ''
	set @tmpItin = ''
	
	declare itin_cursor CURSOR FOR
	select convert(char(7), replace(right(rtrim(servname), 9), '   ', '/') ) as itin
	from peoinvseg (NOLOCK) where servtype = 'AIR' and invnum = @invnum -- and invnum = 'HI00001479' -- 
	and companycode = @companycode
	order by segnum asc	
	open itin_cursor
	fetch next from itin_cursor into @tmpItin
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	-- This is executed as long as the previous fetch succeeds.
		if right(@output_itin, 3) = left(@tmpItin, 3)
			begin
			set @output_itin = @output_itin + substring(@tmpitin, 4, 50)
			end
		else
			begin
				if @output_itin = ''
					begin
					set @output_itin = @tmpItin
					end
				else
					begin
					set @output_itin = @output_itin + '-' + @tmpItin 
					end
			end
		-- set @output_itin = @output_itin + '-' + @tmpItin 
   		FETCH NEXT FROM itin_cursor into @tmpItin
	END
	-- select @output_itin as output
	deallocate itin_cursor
END




