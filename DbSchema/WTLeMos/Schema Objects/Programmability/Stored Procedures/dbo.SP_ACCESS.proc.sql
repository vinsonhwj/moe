﻿



CREATE PROCEDURE SP_ACCESS  
	@COMPANYCODE    CHAR(2),
	@LOGIN          NVARCHAR(10) 
     --   @MODULECODE     NVARCHAR(10) 
AS 
select d.MODULECODE,d.rightscode,d.moduledesc,d.rightsdesc,e.companycode,e.login 
	,case when e.rightscode is null then 'N' else 'Y' end as RightsFlag 
	,case when e.modulecode is null then 'N' else 'Y' end as ModuleFlag,e.accessrights 
from  companymodule f, (
select a.MODULECODE,a.rightscode,b.moduledesc,c.rightsdesc
	from modulerights a ,module b, rightsref c  
	where a.modulecode=b.modulecode and a.rightscode=c.rightscode ) d 
left join access e on d.modulecode=e.modulecode   and d.rightscode=e.rightscode
	 and e.LOGIN=@LOGIN AND e.COMPANYCODE=@COMPANYCODE 
	 and e.accessrights=1
where f.COMPANYCODE=@COMPANYCODE  and f.modulecode=d.modulecode 
 order by d.modulecode,d.rightscode




