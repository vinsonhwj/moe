﻿



--=============================
CREATE PROCEDURE SP_Sales_ByDataRange
  @AS_DATERANGEF varCHAR(20),
  @AS_DATERANGET varCHAR(20),
  @AS_COMPANY   varCHAR(2),
  @AS_BRANCHCODE varCHAR(3),
  @AS_TEAMCODE   varCHAR(5),
  @AS_STAFFCODE varCHAR(10)
 
AS
DECLARE @AS_SQL VARCHAR(4000)
DECLARE @AS_SQL1 VARCHAR(1000)
DECLARE @AS_SQL2 VARCHAR(1000)
DECLARE @AS_SQL3 VARCHAR(1000)
--INV
SELECT @AS_SQL1='select b.bkgref,a.SELLCURR,sum(isnull(a.sellamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invamount,sum(isnull(a.taxamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invtax '
SELECT @AS_SQL1=@AS_SQL1+'INTO #TEMP '
SELECT @AS_SQL1=@AS_SQL1+'from peoinvdetail a,peoinv b,team C,BRANCH d '
SELECT @AS_SQL1=@AS_SQL1+'where a.invnum=b.invnum '
SELECT @AS_SQL1=@AS_SQL1+' and (b.teamcode=c.teamcode '
SELECT @AS_SQL1=@AS_SQL1+' and b.companycode=c.companycode) '
SELECT @AS_SQL1=@AS_SQL1+' and (c.BRANCHCODE=d.BRANCHCODE '
SELECT @AS_SQL1=@AS_SQL1+' and c.companycode=d.companycode) '
SELECT @AS_SQL1=@AS_SQL1+' and b.voidon is null '
--PRINT @AS_SQL1
SELECT @AS_SQL1=@AS_SQL1+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL1=@AS_SQL1+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+convert(varchar(20),dateadd(dd,1,@AS_DATERANGET))+'"'
IF @AS_STAFFCODE<>'ALL' and @AS_STAFFCODE<>''
   SELECT @AS_SQL1=@AS_SQL1 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL1=@AS_SQL1+' and b.TEAMCODE "'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and  @AS_BRANCHCODE<>''
   SELECT @AS_SQL1=@AS_SQL1+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL1=@AS_SQL1+' AND b.invtype="I"'
SELECT @AS_SQL1 =@AS_SQL1 +' group by b.bkgref, a.SELLCURR'
--PRINT @AS_SQL1
--EXEC(@AS_SQL1)
--CRN
SELECT @AS_SQL2=' union all select b.bkgref,a.SELLCURR,-sum(isnull(a.sellamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invamount,-sum(isnull(a.taxamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invtax '
--SELECT @AS_SQL2= @AS_SQL2+'INTO #TEMPCRN '
SELECT @AS_SQL2=@AS_SQL2+'from peoinvdetail a,peoinv b,team C,BRANCH d '
SELECT @AS_SQL2=@AS_SQL2+'where a.invnum=b.invnum '
SELECT @AS_SQL2=@AS_SQL2+' and (b.teamcode=c.teamcode '
SELECT @AS_SQL2=@AS_SQL2+' and b.companycode=c.companycode) '
SELECT @AS_SQL2=@AS_SQL2+' and (c.BRANCHCODE=d.BRANCHCODE '
SELECT @AS_SQL2=@AS_SQL2+' and c.companycode=d.companycode) '
SELECT @AS_SQL2=@AS_SQL2+' and b.voidon is null '
SELECT @AS_SQL2=@AS_SQL2+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL2=@AS_SQL2+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+convert(varchar(20),dateadd(dd,1,@AS_DATERANGET))+'"'
IF @AS_STAFFCODE<>'ALL' and  @AS_STAFFCODE<>''
   SELECT @AS_SQL2=@AS_SQL2 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL2=@AS_SQL2+' and b.TEAMCODE "'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and  @AS_BRANCHCODE<>''
   SELECT @AS_SQL2=@AS_SQL2+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL2= @AS_SQL2+' AND b.invtype="C"'
SELECT @AS_SQL2 =@AS_SQL2 +' group by b.bkgref,a.SELLCURR'
--PRINT @AS_SQL2
--EXEC(@AS_SQL2)
--RPT
SELECT @AS_SQL3=' union all select b.bkgref,a.SELLCURR,sum(isnull(a.sellamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invamount,sum(isnull(a.taxamt,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) as invtax '
--SELECT @AS_SQL3= @AS_SQL3+'INTO #TEMPRPT '
SELECT @AS_SQL3=@AS_SQL3+'from peorptdetail a,peorpt b,team C,BRANCH d '
SELECT @AS_SQL3=@AS_SQL3+'where a.invnum=b.invnum '
SELECT @AS_SQL3=@AS_SQL3+' and (b.teamcode=c.teamcode '
SELECT @AS_SQL3=@AS_SQL3+' and b.companycode=c.companycode) '
SELECT @AS_SQL3=@AS_SQL3+' and (c.BRANCHCODE=d.BRANCHCODE '
SELECT @AS_SQL3=@AS_SQL3+' and c.companycode=d.companycode) '
SELECT @AS_SQL3=@AS_SQL3+' and b.voidon is null '
SELECT @AS_SQL3=@AS_SQL3+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL3=@AS_SQL3+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+convert(varchar(20),dateadd(dd,1,@AS_DATERANGET))+'"'
IF @AS_STAFFCODE<>'ALL' and  @AS_STAFFCODE<>''
   SELECT @AS_SQL3=@AS_SQL3 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL3=@AS_SQL3+' and b.TEAMCODE "'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and @AS_BRANCHCODE<>''
   SELECT @AS_SQL3=@AS_SQL3+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL3 =@AS_SQL3 +' group by b.bkgref,a.SELLCURR'
--EXEC(@AS_SQL3)
--SALES 
SELECT @AS_SQL=' select bkgref,SELLCURR,sum(invamount) as invamount,sum(invtax) as invtax,sum(invamount+invtax) as total from #TEMP group by bkgref,SELLCURR'
SELECT @AS_SQL=@AS_SQL1+@AS_SQL2+@AS_SQL3+@AS_SQL
--PRINT @AS_SQL
EXEC(@AS_SQL)




