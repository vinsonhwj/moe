﻿






CREATE      PROCEDURE dbo.sp_AdnormalBooking
@COMPANY_CODE VARCHAR(2),
@STAFF_CODE   NCHAR(20),
@EXCLUDE_NOTE CHAR(1)
AS

IF @EXCLUDE_NOTE = 'N' 
	begin
		SELECT 
		ltrim(rtrim(b.companycode)) as companycode		
		, ltrim(rtrim(b.TEAMCODE)) as teamcode
		, ltrim(rtrim(b.STAFFCODE)) as staffcode
		, ltrim(rtrim(b.createby)) as createby
		, b.BKGREF , b.MASTERPNR
		, convert(datetime, b.createon, 111) as BookingDate		
		, b.DEPARTDATE AS sDeparture 	-- for searching
		, CONVERT(CHAR(11), b.DEPARTDATE, 106) AS Depart	-- for display
		, CAST(ISNULL(SUM(b.[invamt]), 0) + ISNULL(SUM(b.[invtax]), 0) AS Decimal(20, 2)) AS Invoice
		, CAST(ISNULL(SUM(b.[docamt]), 0)  + ISNULL(SUM(b.[doctax]), 0) AS Decimal(20,2)) AS Document
		into #tmpAdnormalBooking
		FROM dbo.peomstr b  (NoLock) 
		where b.companycode = @COMPANY_CODE AND b.staffcode = @STAFF_CODE 
		GROUP BY  b.BKGREF, b.TEAMCODE, b.STAFFCODE, b.DEPARTDATE, 
		b.MASTERPNR, b.createon, b.companycode, b.createby		
		having(
			(select 
		 	CAST(ISNULL(SUM(b1.[invamt]), 0) + ISNULL(SUM(b1.[invtax]), 0) AS Decimal(20, 2)) 
			- CAST(ISNULL(SUM(b1.[docamt]), 0)  + ISNULL(SUM(b1.[doctax]), 0) AS Decimal(20,2))
			from dbo.peomstr b1
			where b1.masterpnr = b.masterpnr
			and b1.companycode = b.companycode and b1.staffcode = b.staffcode
			group by b1.masterpnr
			) < 0
		and	-- not in Uninvoice reminder queue
			(
			sum(isnull(b.INVCOUNT, 0))
			) > 0
		and -- not in Undocument reminder queue
			(
			sum(isnull(b.DOCCOUNT, 0))
			) > 0
		)
		order by b.teamcode, b.staffcode, b.masterpnr

		
				
		select  * from #tmpAdnormalBooking where companycode = @COMPANY_CODE and teamcode in (select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is null and viewteam is null)  
		union all  
		select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is null)  
		and branchcode in (select distinct viewbranch from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is null)  
		union all  
		select teamcode from team where   
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)  
		and branchcode in (select distinct viewbranch from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)  
		and teamcode in (select distinct viewteam from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)) 		
		
		DROP TABLE #tmpAdnormalBooking

	end
ELSE
   --exclude note
   begin
   
   
   		SELECT 
		ltrim(rtrim(b.companycode)) as companycode		
		, ltrim(rtrim(b.TEAMCODE)) as teamcode
		, ltrim(rtrim(b.STAFFCODE)) as staffcode
		, ltrim(rtrim(b.createby)) as createby
		, b.BKGREF , b.MASTERPNR
		, convert(datetime, b.createon, 111) as BookingDate		
		, b.DEPARTDATE AS sDeparture 	-- for searching
		, CONVERT(CHAR(11), b.DEPARTDATE, 106) AS Depart	-- for display
		, CAST(ISNULL(SUM(b.[invamt]), 0) + ISNULL(SUM(b.[invtax]), 0) AS Decimal(20, 2)) AS Invoice
		, CAST(ISNULL(SUM(b.[docamt]), 0)  + ISNULL(SUM(b.[doctax]), 0) AS Decimal(20,2)) AS Document
		into #tmpAdnormalBookingEx
		FROM dbo.peomstr b  (NoLock) 
		where b.companycode = @COMPANY_CODE AND b.staffcode = @STAFF_CODE 
		GROUP BY  b.BKGREF, b.TEAMCODE, b.STAFFCODE, b.DEPARTDATE, 
		b.MASTERPNR, b.createon, b.companycode, b.createby		
		having(
			(select 
		 	CAST(ISNULL(SUM(b1.[invamt]), 0) + ISNULL(SUM(b1.[invtax]), 0) AS Decimal(20, 2)) 
			- CAST(ISNULL(SUM(b1.[docamt]), 0)  + ISNULL(SUM(b1.[doctax]), 0) AS Decimal(20,2))
			from dbo.peomstr b1
			where b1.masterpnr = b.masterpnr
			and b1.companycode = b.companycode and b1.staffcode = b.staffcode
			group by b1.masterpnr
			) < 0
		and	-- not in Uninvoice reminder queue
			(
			sum(isnull(b.INVCOUNT, 0))
			) > 0
		and -- not in Undocument reminder queue
			(
			sum(isnull(b.DOCCOUNT, 0))
			) > 0
		)
		order by b.teamcode, b.staffcode, b.masterpnr
 
   
   
		select  a.* from (
		SELECT 
		ab.* 
		from #tmpAdnormalBookingEx ab 
		where ab.bkgref not in (select distinct bkgref from peoPFQue where companycode = ab.companycode)
		Union
		select  
		ab.* 
		from #tmpAdnormalBookingEx ab, peoPFQue pfq
		where ab.companycode = pfq.companycode and ab.bkgref = pfq.bkgref
		and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = ab.bkgref and pfq1.companycode = ab.companycode)
		and ( ab.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - ab.Document < 0
		) as a  
		where a.teamcode in
		(select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is null and viewteam is null)  
		union all  
		select teamcode from team where  
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is null)  
		and branchcode in (select distinct viewbranch from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is null)  
		union all  
		select teamcode from team where   
		companycode in (select distinct viewcompany from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)  
		and branchcode in (select distinct viewbranch from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)  
		and teamcode in (select distinct viewteam from viewother where login = @STAFF_CODE and viewbranch is not null and viewteam is not null)) 				
		order by a.teamcode, a.staffcode, a.masterpnr


		DROP TABLE #tmpAdnormalBookingEx
end








