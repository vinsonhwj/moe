﻿

CREATE PROCEDURE [dbo].[sp_RetrieveInvoice_bak_ric]
@CompanyCode varchar(20),
@InvNum varchar(20)
AS
BEGIN

declare @BkgRef varchar(20)
select @BkgRef=BkgRef from PeoInv where CompanyCode=@CompanyCode and InvNum=@InvNum

DECLARE @InvTypeCode NVARCHAR(4)
DECLARE @ActionCode NVARCHAR(5)
SELECT @InvTypeCode=InvTypeCode ,@ActionCode=ActionCode FROM 
(
 SELECT TOP 1
        ISNULL(invtypecode, 'INOR') AS InvTypeCode,
       ISNULL(actioncode, 'INV') AS ActionCode
 FROM   peonyref
 WHERE  companycode = @CompanyCode
        AND docnum = @InvNum
 ORDER BY createon ASC
) AS RS

select MstrInvNum,IsNull(InvFormat, 'D') As InvFormat, IsNull(Void2JBA,'') as Void2JBA, 
CompanyCode,BkgRef,InvNum,IsNull(Rmk1,'') as Rmk1,IsNull(Rmk2,'') as Rmk2,IsNull(Rmk3,'') as 
Rmk3,IsNull(Rmk4,'') as Rmk4,IsNull(Rmk5,'') as Rmk5,IsNull(Rmk6,'') as Rmk6,CltCode,CltName,CltAddr,Phone,Fax,CustRef,InvType,AnalysisCode,ATTN,IsNull(VoidBy,'') as VoidBy,IsNull(VoidReason,'') as 
VoidReason,  IsNull((rtrim(convert(char,day(chq_issuedate))) + '/' + rtrim(convert(char,month(chq_issuedate)))+ '/' + rtrim(convert(char,year(chq_issuedate)))),'') as Chq_Dte,  IsNull(Payer,'') as 
Payer,IsNull(BankCode,'') as BankCode, IsNull(Pay_Rmk,'') as Pay_Rmk, IsNull(ChqNo,'') as ChqNo, CreateOn,UpdateOn, IsNull(PayDesc,'') as PayDesc,  IsNull(SellCurr,'') as SellCurr, IsNull(SellAmt,0) as 
SellAmt ,IsNull(InvDTCurr,'') as InvDTCurr, IsNull(InvDTSum,0) as InvDTSum, IsNull(CNReason, '') as CNReason ,IsNull(GSTAmt,0) as GSTAmt, IsNull(GSTPct,0) as GSTPct, '' as InvTc 
, ISNULL(IsHidebreakdown,0) AS IsHidebreakdown,DisCreditInfo,@InvTypeCode AS InvTypeCode,@ActionCode AS ActionCode
from PeoInv (nolock) where CompanyCode =@CompanyCode and BkgRef=@BkgRef and InvNum = @InvNum 
ORDER BY InvNum desc 

select IsNull(cust.TeamCode,'') TeamCode from customer cust(nolock), peoinv inv (nolock) 
where cust.CompanyCode=inv.CompanyCode and cust.CltCode = inv.cltcode
and cust.CompanyCode = @CompanyCode and inv.Invnum = @InvNum

select CompanyName,IsNull(ADDR1,'')+', '+IsNull(ADDR2,'')+', '+IsNull(ADDR3,'')+'<br> '+IsNull(ADDR4,'') as Address 
from company (nolock) where CompanyCode=@CompanyCode

select InvNum,SegNum,ServType,IsNull(SegType,'') As SegType, IsNull(ServName,'') AS ServName,IsNull(ServDesc,'') AS ServDesc 
from PeoInvSEG where InvNum=@InvNum and CompanyCode=@CompanyCode

select IsNull(c.GSTPct,0) as GSTPct, IsNull(b.SegType,'') as SegType,a.GSTAmt, IsNull(a.ShowDetailFee,'N') as ShowDetailFee, IsNull(AmtFee,0) as AmtFee, 
a.CompanyCode,a.InvNum,a.SegNum,a.SeqNum,a.ItemName,a.RateName,a.UnitPrice,IsNull(a.Qty,0) as Qty,IsNull(a.Rmnts,0) as Rmnts,IsNull(a.SellCurr,'') as SellCurr,IsNull(a.SellAmt,0) as 
SellAmt,IsNull(a.TaxCurr,'') as TaxCurr,IsNull(a.TaxAmt,0) as TaxAmt, case when ShowDetailFee = 'N' then  a.AmtFee + a.SellAmt else a.SellAmt end as ShowAmt,a.HKDAmt,case when b.ServType='HTL' then 
(IsNull(a.SellAmt,0)+IsNull(a.TaxAmt,0)) else (IsNull(a.SellAmt,0)+IsNull(a.TaxAmt,0)) end as  TotalAmt, b.ServType from PeoInvdetail a,PeoInvseg b, PeoInv c where a.CompanyCode=b.CompanyCode 
and b.CompanyCode=c.CompanyCode and a.InvNum=b.InvNum and b.InvNum=c.InvNum and a.SegNum=b.SegNum and  a.CompanyCode=@CompanyCode and a.InvNum in (@InvNum) order by a.SegNum,a.SeqNum 

select isnull(dbo.f_SplitPeoinvPax(b.paxname,'1'),'') as PaxLname, isnull(dbo.f_SplitPeoinvPax(b.paxname,'2'),'') as PaxFName
, isnull(dbo.f_SplitPeoinvPax(b.paxname,'4'),'') as PaxType,  isnull(dbo.f_SplitPeoinvPax(b.paxname,'3'),'') as PaxTitle
, IsNull(b.Curr,'') as Curr, IsNull(b.Sell,0) as Sell 
from peoinpax b (nolock), PeoInv c (nolock) 
where b.CompanyCode=c.CompanyCode and b.InvNum=c.InvNum and b.InvNum=@InvNum and b.CompanyCode = @CompanyCode


select RTRIM(peostaff.StaffName + '/' + login.TeamCode) AS [DisplayName], login.branchcode, replace(IsNull(peostaff.Phone,''),'()','') as Phone,StaffName,Email from peostaff, login, PeoInv  where login.login=PeoInv.createby and 
login.CompanyCode=PeoInv.CompanyCode and  login.StaffCode= peostaff.StaffCode and login.CompanyCode=peostaff.CompanyCode  and PeoInv.InvNum=@InvNum and PeoInv.CompanyCode=@CompanyCode 

select IsNull(CR_Card_Holder,'') as CR_Card_Holder, IsNull(CR_Card_NO,'') as CR_Card_NO,IsNull(CR_Card_Type, 'AX') as CR_Card_Type, IsNull(CR_Card_Expiry,'') as CR_Card_Expiry, IsNull(CR_Amt,0) as CR_Amt, 
IsNull(CR_Curr,'') as CR_Curr , IsNull(CR_Approval_Code, '') as CR_Approval_Code, IsNull(CR_TA, '') as CR_TA from PeoInv where InvNum=@InvNum and CompanyCode=@CompanyCode and CR_Card_NO<>''

/*
select IsNull(a.AirCode,'') as AirCode, IsNull(b.Ticket,'') as Ticket, '' as TicketType  from peotkt a, PeoInvTKT  b where a.Ticket =b.Ticket and a.CompanyCode = b.CompanyCode  and a.CompanyCode =@CompanyCode and 
b.InvNum=@InvNum AND a.TktSeq = b.TktSeq order by b.Ticket, b.TktSeq
*/
select isnull(a.aircode,'') as AirCode, isnull(b.ticket,'') as Ticket, case when isnull(b.TicketType,'') = 'MIS' then '*' else '' end as TicketType  from peotkt a, PEOINVTKTREF  b where a.ticket =b.ticket 
and a.companycode = b.companycode  and a.companycode =@CompanyCode and b.invnum=@InvNum AND a.TktSeq = b.TktSeq  UNION select isnull(a.aircode,'') as aircode, isnull(b.ticket,'') as ticket, case when 
isnull(b.TicketType,'') = 'MIS' then '*' else '' end as TicketType  from peomistkt a, PEOINVTKTREF  b where a.ticket =b.ticket and a.companycode = b.companycode  and a.companycode =@CompanyCode and 
b.invnum=@InvNum AND a.TktSeq = b.TktSeq order by 2
SELECT Description FROM PeonyRef WHERE DocNum = @InvNum AND CompanyCode = @CompanyCode AND RIGHT(Description, 10) = ' - printed' 

SELECT BkgRef,PNR,Crssinein,COD1,COD2,COD3,Deadline,Cltode as CltCode,CltName,CltAddr,StaffCode,TeamCode,FirstPax,Phone,Fax,Email,MasterPNR,TourCode,ContactPerson, 
 DepartDate,TtlSellCurr,TtlSellAmt,TtlSellTaxCurr,TtlSellTax,TtlCostCurr,TtlCostAmt,TtlCostTaxCurr,TtlCostTax,InvCurr,InvAmt,InvTaxCurr,InvTax, 
 DocCurr,DocAmt,DocTaxCurr,DocTax,TtlSellGST,TtlCostGST,TtlInvGST,TtlDocGST,InvCount,DocCount,Rmk1,Rmk2,Rmk3,Rmk4,SourceSystem,Source,SourceRef, 
 Pseudo,MstrRemark,BookingNo,Pseudo_Inv,Crssinein_Inv,Agency_Arc_Iata_Num,Iss_Off_Code,SalesPerson 
 FROM PeoMstr (NOLOCK) 
 WHERE CompanyCode = @CompanyCode 
 AND BkgRef = @BkgRef

select a.CltCode, CONVERT (Char(11),DATEADD ("D",b.Crterms, a.CreateOn ),113) as DueDate, b.Crterms from PeoInv a(noLock), PeoCltCrlmt b (nolock)
where a.InvNum=@InvNum and a.CltCode=b.CltCode and a.CompanyCode = b.CompanyCode and b.CompanyCode=@CompanyCode

SELECT Void2JBA FROM PeoInv (nolock) WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef AND InvNum = @InvNum AND VoidOn IS NOT NULL AND VoidOn <> '' AND VoidBy IS NOT NULL AND VoidBy <> '' 

select top 1 * from WP_eInvoice (nolock) where InvNum = @InvNum and CompanyCode = @CompanyCode

SELECT TOP 1
        ISNULL(invtypecode, 'INOR') AS InvTypeCode,
        ISNULL(actioncode, 'INV') AS ActionCode
FROM    peonyref
WHERE   companycode = @CompanyCode
        AND docnum = @InvNum
ORDER BY createon ASC 

END




