﻿
CREATE PROCEDURE dbo.sp_RecalTicketPaidFare
@Companycode varchar(2)
, @Invnum varchar(10)
, @InvType varchar(3)

AS

DECLARE @tbl_InvoiceTicket TABLE 
(
	COMPANYCODE	VARCHAR(2)
,	INVNUM		VARCHAR(10)
,	TICKET		VARCHAR(10)
)


DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_Ticket varchar(10)
DECLARE @lstr_Tktseq varchar(3)
DECLARE @lnum_SellFare decimal(16,2)

if @invtype = 'UAT' 
begin
	insert into @tbl_InvoiceTicket select companycode, invnum, ticket from peoinvtkt (nolock) where companycode = @Companycode and invnum = @Invnum
end
else
begin
	insert into @tbl_InvoiceTicket select companycode, invnum, ticket from peoinvtktref (nolock) where  companycode = @Companycode and invnum = @Invnum

end

DECLARE lcur_InvTicket CURSOR FOR 
select t.companycode, t.Ticket, t.tktseq, sum(ad.amt + ad.amtfee) as fee
from peotkt t (nolock), peotktdetail td (nolock), peoair a (nolock), peoairdetail ad (nolock), @tbl_InvoiceTicket tTicket
where t.companycode = tTicket.companycode and t.ticket = tTicket.Ticket
and t.companycode = td.companycode and t.ticket = td.ticket and t.tktseq = td.tktseq
and a.companycode = td.companycode and a.bkgref = t.bkgref and a.Flight = td.Flight and a.class = td.class and a.departdate = td.departdate
and ad.companycode = a.companycode and ad.bkgref = a.bkgref and ad.segnum = a.segnum and ad.agetype = t.paxair
and t.voidon is null and ad.seqtype = 'SELL'
group by t.companycode, t.ticket, t.tktseq

OPEN lcur_InvTicket
FETCH lcur_InvTicket INTO @lstr_Companycode, @lstr_Ticket, @lstr_Tktseq, @lnum_SellFare
WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE peotkt set paidfareWOTax = @lnum_SellFare
	WHERE Companycode = @lstr_Companycode AND Ticket = @lstr_Ticket AND tktseq = @lstr_Tktseq AND paidfarewotax = 0

	FETCH lcur_InvTicket INTO @lstr_Companycode, @lstr_Ticket, @lstr_Tktseq, @lnum_SellFare
END
CLOSE lcur_InvTicket
DEALLOCATE lcur_InvTicket






