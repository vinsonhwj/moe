﻿

CREATE procedure dbo.sp_RptMisViewReportInvoice
  @COMPANYCODE varchar(2)
, @START_DATE varchar(10)
, @END_DATE varchar(25)
, @CLT_CODE varchar(1000)

as 

-- Split Staffcode into  Temp Table
Declare @TargetCode varchar(10)

create table #TmpClient (cltcode varchar(10))
While (@CLT_CODE <> '')
Begin
	If left(@CLT_CODE,1) = '|'
	Begin
		Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE)-1))
	End
	Else
	Begin
		If @TargetCode = '' or CharIndex('|', @CLT_CODE) = 0
			Select @TargetCode=@CLT_CODE
		else
			Select @TargetCode = (Select substring(@CLT_CODE, 1, CharIndex('|', @CLT_CODE)-1))

		INSERT INTO #TmpClient VALUES (@TargetCode)

		If (Select CharIndex('|', @CLT_CODE)) > 0
			Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE) - Len(@TargetCode) - 1))
		Else
			Select @CLT_CODE = (Select Right(@CLT_CODE, Len(@CLT_CODE) - Len(@TargetCode)))
		
	End
End



select t.companycode, t.bkgref, t.ticket, t.tktseq, t.issueon, m.cltode, m.pnr, t.voidon, m.contactperson
into #tmpTicket
from peotkt t (nolock), peomstr m (nolock), #tmpClient c
where 
t.companycode = m.companycode and t.bkgref = m.bkgref
and t.companycode = @COMPANYCODE and t.issueon between @START_DATE and @END_DATE
and m.cltode = c.cltcode
and t.voidon is null

select d.ticket, d.tktseq, d.segnum, d.airline, d.flight, d.class, d.departcity, d.arrivalcity, c1.countrycode as departcountry, c2.countrycode as arrivalcountry
,d.departdate, d.arrdate,  convert(int, case when charindex('.',d.ElapsedTime) > 0 then substring(d.ElapsedTime,0,charindex('.',d.ElapsedTime))
	                   when charindex('*',d.ElapsedTime) > 0 then '0'
		           when len(d.ElapsedTime) <= 3 then left(d.ElapsedTime,1)
	    	           else left(d.ElapsedTime,2) end  ) as ElapsedTime
into #tmpTicketDetail
from peotktdetail d (nolock), #tmpTicket t, city_en_us c1 (nolock), city_en_us c2 (nolock)
where d.companycode = t.companycode and d.ticket = t.ticket and d.tktseq = t.tktseq
and c1.citycode = d.departcity
and c2.citycode = d.arrivalcity


create table #tmpTicketInv
(
	Ticket varchar(10) not null
,	Tktseq varchar(10) not null
,	InvNum varchar(10) not null
,	InvDate datetime not null
,	InvBy	varchar(10) not null
,	COD1 varchar(20) not null
,	COD2 varchar(20) not null
,	COD3 varchar(20) not null
)

insert into #tmpTicketInv
select t.ticket, t.tktseq, i.invnum, i.createon, i.createby, isnull(i.cod1,''), isnull(i.cod2,''), isnull(i.cod3,'')
from #tmpTicket t
inner join peoinvtktref ti (nolock) on ti.companycode = t.companycode and ti.ticket = t.ticket and ti.tktseq = t.tktseq
inner join peoinv i (nolock) on i.companycode = ti.companycode and i.invnum = ti.invnum and i.void2jba is null

insert into #tmpTicketInv
select t.ticket, t.tktseq, i.invnum, i.createon, i.createby, isnull(i.cod1,''), isnull(i.cod2,''), isnull(i.cod3,'')
from #tmpTicket t
inner join peoinvtkt ti (nolock) on ti.companycode = t.companycode and ti.ticket = t.ticket and ti.tktseq = t.tktseq
inner join peoinv i (nolock) on i.companycode = ti.companycode and i.invnum = ti.invnum and i.void2jba is null

CREATE TABLE #tmpReport
(
	Companycode	varchar(2) not null default ''
,	Ticket		varchar(10) not null default ''
,	Tktseq		varchar(3) not null default ''
,	Bkgref		varchar(10) not null default ''
,	CltCode		varchar(10) not null default ''
,	PNR		varchar(6) not null default ''
,	ContactPerson	varchar(80) not null default ''
,	IssueOn		DateTime null
,	Paxname		varchar(100) not null default ''
,	COD1		varchar(50) not null default ''
,	COD2		varchar(50) not null default ''
,	COD3		varchar(50) not null default ''
,	COD4		varchar(50) not null default ''
,	COD5		varchar(50) not null default ''
,	COD6		varchar(50) not null default ''
,	PublishedFare	decimal(16,2) not null default 0
,	SellingFare	decimal(16,2) not null default 0
,	FullFare	decimal(16,2) not null default 0
,	FullCurr	varchar(3) not null default ''
,	PaidCurr	varchar(3) not null default ''
,	AirCode		varchar(10) not null default ''
,	Airline		varchar(10) not null default ''
,	Routing		varchar(120) not null default ''
,	Class		varchar(1) not null default ''
,	DepartDate	datetime null
,	ArrivalDate	Datetime null
,	InvoiceDate	Datetime null
,	InvoiceBy	varchar(10) not null default ''
,	Invoice		varchar(10) not null default ''
,	Tax		decimal(16,2) not null default 0
,	FareType	varchar(1) not null default ''
,	TotalTax	decimal(16,2) not null default 0
,	SellCurr	varchar(10) not null default ''
,	PaidFareWOTax	decimal(16,2) not null default 0
,	CorpCurr	varchar(10) not null default ''
,	CorpFare	decimal(16,2) not null default 0
,	GDS		varchar(20) not null default ''
,	LowFareWOTax 	decimal(16,2) not null default 0
,	CorpFareWOTax	decimal(16,2) not null default 0
,	TktType		varchar(1) not null default ''
,	Destination	varchar(20) not null default ''
,	AirReason	varchar(2) not null default ''
,	iss_in_exchange	varchar(10) not null default ''
,	iss_Fullfare	decimal(16,2) not null default 0
,	i_cod1		varchar(20) not null default ''
,	i_cod2		varchar(20) not null default ''
,	i_cod3		varchar(30) not null default ''
)

insert into #tmpReport
select m.companycode, m.ticket, m.tktseq,  m.bkgref, m.cltode, m.pnr, m.contactperson, m.issueon, isnull(t.paxname,'')
, isnull(t.cod1,''), isnull(t.cod2,''), isnull(t.cod3,''), isnull(t.cod4,''), isnull(t.cod5,''), isnull(t.cod6,'')
, convert(int,IsNull(FullFareWOTax, 0)) as PublishedFare, convert(int,IsNull(MISpaidfare, 0)) as SellingFare
, convert(int,IsNull(FullFareWOTax, 0)) as FullFare, IsNull(FullCurr, '') as FullCurr, IsNull(PaidCurr, '') as PaidCurr
, IsNull(t.AirCode, ''), IsNull(t.AirLine, '') as AirLine,IsNull(Routing, '') as Routing 
, isnull(d.class,'') as Class, d.departdate,  r.Arrdate, i.invdate, isnull(i.invby,'') as InvoiceBy, isnull(i.invnum,'') as Invnum
, convert(int,IsNull(FullFareTax, 0)) as Tax,case when (IsNull(FareType, '') = 'N' or IsNull(FareType, '') = '') then 'N' Else 'S' End as FareType
, convert(int,IsNull(TotalTax, 0)) as TotalTax, IsNull(SellCurr,'') as SellCurr ,convert(int,IsNull(PaidFareWOTax,0)) as PaidFareWOTax ,IsNull(CorpCurr,'') as CorpCurr ,convert(int,IsNull(CorpFare,0)) as CorpFare 
, CASE T.sourcesystem WHEN 'eMOS' THEN 'M' WHEN 'GLO' THEN 'G' WHEN 'IUR' THEN 'B' WHEN 'AIR' THEN 'A' ELSE '' END AS GDS 
, convert(int,ISNULL(LowFareWOTax,0)) as LowFareWOTax, convert(int,ISNULL(CorpFareWOTax,0)) as CorpFareWOTax ,ISNULL(tkttype,'P') as TktType, IsNull(Destination, '') as Destination
, case when rtrim(AirReason) = '99' then 'LF' else rtrim(AirReason) end as AirReason
, isnull(substring(iss_in_exchange,4,10),''), 0, isnull(i.cod1,''), isnull(i.cod2,''), isnull(i.cod3,'')
from #tmpTicket m
inner join peotkt t (nolock) on t.companycode = m.companycode and t.ticket = m.ticket and t.tktseq = m.tktseq
inner join #tmpTicketDetail d (nolock) on d.ticket = m.ticket and d.tktseq = m.tktseq 
and d.segnum = (select min(segnum) from #tmpTicketDetail d1 where d.ticket = d1.ticket and d.tktseq = d1.tktseq)
inner join #tmpTicketDetail r (nolock) on r.ticket = m.ticket and r.tktseq = m.tktseq 
and r.segnum = (select max(segnum) from #tmpTicketDetail d2 where r.ticket = d2.ticket and r.tktseq = d2.tktseq)
left outer join #tmpTicketInv i  on i.ticket = m.ticket

update #tmpReport set PublishedFare = 
case when r.FullFareWOTax = 0 then 0
     when r.FullFareWOTax - t.PublishedFare >= 0 then  r.FullFareWOTax - t.PublishedFare
     else r.FullFareWOTax
end
,iss_Fullfare = r.FullFareWOTax
from #tmpReport t, peotkt r (nolock)
where r.companycode = t.companycode and r.ticket = t.iss_in_exchange and r.voidon is null
and t.iss_in_exchange <> ''


--Mis Ticket
select t.companycode, t.bkgref, t.ticket, t.tktseq, t.issueon, m.cltode, m.pnr, t.voidon, m.contactperson
into #tmpMisTicket
from peomistkt t (nolock), peomstr m (nolock), #tmpClient c
where 
t.companycode = m.companycode and t.bkgref = m.bkgref
and t.companycode = @COMPANYCODE and t.issueon between @START_DATE and @END_DATE
and m.cltode = c.cltcode
and t.voidon is null

select d.ticket, d.tktseq, d.segnum, d.airline, d.flight, d.class, d.departcity, d.arrivalcity, c1.countrycode as departcountry, c2.countrycode as arrivalcountry
,d.departdate, d.arrdate,  convert(int, case when charindex('.',d.ElapsedTime) > 0 then substring(d.ElapsedTime,0,charindex('.',d.ElapsedTime))
	                   when charindex('*',d.ElapsedTime) > 0 then '0'
		           when len(d.ElapsedTime) <= 3 then left(d.ElapsedTime,1)
	    	           else left(d.ElapsedTime,2) end  ) as ElapsedTime
into #tmpMisTicketDetail
from peomistktdetail d (nolock), #tmpMisTicket t, city_en_us c1 (nolock), city_en_us c2 (nolock)
where d.companycode = t.companycode and d.ticket = t.ticket and d.tktseq = t.tktseq
and c1.citycode = d.departcity
and c2.citycode = d.arrivalcity




create table #tmpMisTicketInv
(
	Ticket varchar(10) not null
,	Tktseq varchar(10) not null
,	InvNum varchar(10) not null
,	InvDate datetime not null
,	InvBy	varchar(10) not null
,	COD1 varchar(20) not null
,	COD2 varchar(20) not null
,	COD3 varchar(20) not null

)

insert into #tmpMisTicketInv
select t.ticket, t.tktseq, i.invnum, i.createon, i.createby, isnull(i.cod1,''), isnull(i.cod2,''), isnull(i.cod3,'')
from #tmpMisTicket t
inner join peoinvtktref ti (nolock) on ti.companycode = t.companycode and ti.ticket = t.ticket and ti.tktseq = t.tktseq and tickettype = 'MIS'
inner join peoinv i (nolock) on i.companycode = ti.companycode and i.invnum = ti.invnum and i.void2jba is null


insert into #tmpReport
select m.companycode, m.ticket, m.tktseq,  m.bkgref, m.cltode, m.pnr, m.contactperson, m.issueon, isnull(t.paxname,'')
, isnull(t.cod1,''), isnull(t.cod2,''), isnull(t.cod3,''), isnull(t.cod4,''), isnull(t.cod5,''), isnull(t.cod6,'')
, convert(int,IsNull(FullFareWOTax, 0)) as PublishedFare, convert(int,IsNull(MISpaidfare, 0)) as SellingFare
, convert(int,IsNull(FullFareWOTax, 0)) as FullFare, IsNull(FullCurr, '') as FullCurr, IsNull(PaidCurr, '') as PaidCurr
, IsNull(t.AirCode, ''), IsNull(t.AirLine, '') as AirLine,IsNull(Routing, '') as Routing 
, isnull(d.class,'') as Class, d.departdate,  r.Arrdate, i.invdate, isnull(i.invby,'') as InvoiceBy, isnull(i.invnum,'') as Invnum
, convert(int,IsNull(FullFareTax, 0)) as Tax,case when (IsNull(FareType, '') = 'N' or IsNull(FareType, '') = '') then 'N' Else 'S' End as FareType
, convert(int,IsNull(TotalTax, 0)) as TotalTax, IsNull(SellCurr,'') as SellCurr ,convert(int,IsNull(PaidFareWOTax,0)) as PaidFareWOTax ,IsNull(CorpCurr,'') as CorpCurr ,convert(int,IsNull(CorpFare,0)) as CorpFare 
, CASE T.sourcesystem WHEN 'eMOS' THEN 'M' WHEN 'GLO' THEN 'G' WHEN 'IUR' THEN 'B' WHEN 'AIR' THEN 'A' ELSE '' END AS GDS 
, convert(int,ISNULL(LowFareWOTax,0)) as LowFareWOTax, convert(int,ISNULL(CorpFareWOTax,0)) as CorpFareWOTax ,ISNULL(tkttype,'P') as TktType, IsNull(Destination, '') as Destination
, case when rtrim(AirReason) = '99' then 'LF' else rtrim(AirReason) end as AirReason,'', 0, isnull(i.cod1,''), isnull(i.cod2,''), isnull(i.cod3,'')
from #tmpMisTicket m
inner join peomistkt t (nolock) on t.companycode = m.companycode and t.ticket = m.ticket and t.tktseq = m.tktseq
inner join #tmpMisTicketDetail d (nolock) on d.ticket = m.ticket and d.tktseq = m.tktseq 
and d.segnum = (select min(segnum) from #tmpMisTicketDetail d1 where d.ticket = d1.ticket and d.tktseq = d1.tktseq)
inner join #tmpMisTicketDetail r (nolock) on r.ticket = m.ticket and r.tktseq = m.tktseq 
and r.segnum = (select max(segnum) from #tmpMisTicketDetail d2 where r.ticket = d2.ticket and r.tktseq = d2.tktseq)
left outer join #tmpMisTicketInv i  on i.ticket = m.ticket


update #tmpReport set 
  cod1 = case when t.cod1 = '' then b.cod1 else b.cod1 end
, cod2 = case when t.cod2 = '' then b.cod2 else b.cod2 end
, cod3 = case when t.cod3 = '' then b.cod3 else b.cod3 end
from #tmpReport t, peomis b (nolock)
where t.companycode = b.companycode and t.bkgref = b.bkgref




select r.*
, isnull(mc.btiClient,'') as bticode
, isnull(c.cltname,'') as CltName
, isnull(convert(varchar,DepartDate,106),'') as DepartOn
, isnull(convert(varchar,ArrivalDate,106),'') as ArrDate
, isnull(convert(varchar,InvoiceDate,106),'') as InvDate
from #tmpReport r
left outer join customer c (nolock) ON c.companycode = r.companycode and c.cltcode = r.cltcode
left outer join misclient mc (nolock) on mc.companycode = c.companycode and mc.wrsclient = c.cltcode




drop table #TmpClient
drop table #TmpTicket
drop table #tmpTicketDetail
drop table #tmpTicketInv
drop table #TmpMisTicket
drop table #tmpMisTicketDetail
drop table #tmpMisTicketInv
drop table #tmpReport















