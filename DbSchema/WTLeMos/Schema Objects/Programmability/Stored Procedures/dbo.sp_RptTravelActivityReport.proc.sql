﻿

CREATE   PROCEDURE [dbo].[sp_RptTravelActivityReport]
@COMPANYCODE varchar(2),
@START_DATE varchar(10),
@END_DATE varchar(25),
@CLT_CODE varchar(10),
@PAXNAME nvarchar(100)


AS





CREATE TABLE #tmpRptResult
(
	  Companycode  	varchar(2) not null default ''
	, Teamcode 	varchar(10) not null default ''
	, Staffcode	varchar(10) not null default ''
	, Createon 	datetime null
	, Mstrinvnum    varchar(10) not null default ''
	, Cltcode       varchar(10) not null default ''
	, Pax           nvarchar(100) not null default ''
	, pnr           varchar(6) not null default ''
	, ticket        varchar(10) not null default ''
	, routing       varchar(100) not null default ''
	, flight        varchar(10) not null default ''
	, class        	varchar(10) not null default ''
	, depart        varchar(20) not null default ''
	, arrival       varchar(20) not null default ''
	, servtype      varchar(50) not null default ''
	, airfare       decimal(16,2) not null default 0
	, taxamt        decimal(16,2) not null default 0
	, amtfee        decimal(16,2) not null default 0
	, othcharge     decimal(16,2) not null default 0
	, invnum        varchar(10) not null default ''
	, TicketPax     nvarchar(100) not null default ''
)

DECLARE @lstr_Companycode varchar(2)
DECLARE @lstr_preInvnum varchar(10)
DECLARE @lstr_Invnum varchar(10)
DECLARE @lstr_Ticket varchar(10)
DECLARE @lstr_SegType varchar(10)
DECLARE @lstr_Routing varchar(100)
DECLARE @lstr_Flight varchar(10)
DECLARE @lstr_Class varchar(10)
DECLARE @lstr_Depart varchar(10)
DECLARE @lstr_Arrival varchar(10)
DECLARE @lnum_SellAmt decimal(16,2)
DECLARE @lnum_TaxAmt decimal(16,2)
DECLARE @lnum_Amtfee decimal(16,2)
DECLARE @lnum_OtherCharge decimal(16,2)


--Retrieve Master Record
if ltrim(rtrim(@CLT_CODE)) <> ''
begin
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.Createon, ISNULL(inv.Mstrinvnum,'') as Mstrinvnum, inv.Cltcode,
		isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		from peoinpax (nolock)
		where invnum = inv.invnum and companycode = inv.companycode order by paxseq asc), '') as pax,
	       Mstr.pnr, inv.Invnum
	INTO #tmpResult
	FROM Peoinv inv (nolock), Peomstr mstr (nolock)
	WHERE inv.Companycode = mstr.Companycode
	AND inv.bkgref = mstr.bkgref
	AND inv.Companycode = @COMPANYCODE
	AND inv.Createon between @START_DATE AND @END_DATE
	AND inv.Cltcode = @CLT_CODE
	AND substring(invnum, 2, 1) = 'I'
	AND isnull(void2jba, '') = ''

	if ltrim(rtrim(@PAXNAME)) <> ''
	begin
		INSERT INTO #tmpRptResult
		(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum)
		SELECT CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum
		FROM #tmpResult
		WHERE pax like '%' + @PAXNAME + '%';
	end
	else
	begin
		INSERT INTO #tmpRptResult
		(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum)
		SELECT CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum
		FROM #tmpResult
	end
end
else
--Pax Only
begin

	SELECT DISTINCT inv.companycode, inv.invnum
	INTO #tmpPax
	FROM Peoinv inv (nolock), Peomstr mstr (nolock), peoinpax pax (nolock) 
	WHERE inv.Companycode = mstr.Companycode AND inv.bkgref = mstr.bkgref
	AND pax.Companycode = inv.Companycode AND pax.invnum = inv.invnum
	AND replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ') like '%' + @PAXNAME + '%'
	AND inv.Companycode = @COMPANYCODE
	AND inv.Createon between @START_DATE AND @END_DATE
	AND substring(inv.invnum, 2, 1) = 'I'
	AND isnull(inv.void2jba, '') = ''


	INSERT INTO #tmpRptResult
	(CompanyCOde, TeamCode, Staffcode, Createon, Mstrinvnum, Cltcode, pax, pnr, Invnum)
	SELECT inv.CompanyCode, inv.Teamcode, inv.Staffcode, inv.Createon, ISNULL(inv.Mstrinvnum,''), inv.Cltcode,
		isnull((select top 1 replace(substring(paxname, 1, charindex('/', paxname, charindex('/', paxname) + 1) - 1), '/', ' ')
		from peoinpax (nolock)
		where invnum = inv.invnum and companycode = inv.companycode order by paxseq asc), '') as pax,
	       Mstr.pnr, inv.Invnum
	FROM Peoinv inv (nolock), Peomstr mstr (nolock), #tmpPax pax (nolock)
	WHERE inv.Companycode = mstr.Companycode
	AND inv.bkgref = mstr.bkgref
	AND inv.Companycode = pax.Companycode AND inv.invnum = pax.invnum

	DROP TABLE #tmpPax

end

--Get Ticket
print 'Get Ticket Start @ ' + convert(varchar,getdate(),113)
UPDATE #tmpRptResult set Ticket = ref.Ticket
FROM #tmpRptResult t, peoinvtktref ref (nolock)
WHERE t.CompanyCode = ref.companycode and t.invnum = ref.invnum


UPDATE #tmpRptResult set Ticket = ref.Ticket
FROM #tmpRptResult t, peoinvtkt ref (nolock)
WHERE t.CompanyCode = ref.companycode and t.invnum = ref.invnum


UPDATE #tmpRptResult set TicketPax = peotkt.PaxName
FROM #tmpRptResult t, peotkt (nolock)
WHERE t.CompanyCode = peotkt.companycode and t.ticket = peotkt.ticket  and voidon is null

UPDATE #tmpRptResult set TicketPax = peomistkt.PaxName
FROM #tmpRptResult t, peomistkt (nolock)
WHERE t.CompanyCode = peomistkt.companycode and t.ticket = peomistkt.ticket  and voidon is null

print 'Get Ticket End @ ' + convert(varchar,getdate(),113)

print 'Get Segment Start @ ' + convert(varchar,getdate(),113)
--Calculate Segment Amount 
SELECT rpt.Companycode, rpt.Invnum, seg.Segtype, seg.Segnum, seg.servtype, seg.servname, Servdesc, detail.detltype,
isnull(sum(detail.sellamt),0) as sellamt, isnull(sum(detail.taxamt),0) as taxamt, isnull(sum(detail.amtfee),0) as amtfee, 
isnull(sum(detail.qty),0) as qty, isnull(sum(detail.rmnts),0) as rmnts
INTO #tmpSegment
FROM Peoinvseg seg (nolock) 
inner join #tmpRptResult rpt on  seg.Companycode = rpt.Companycode AND seg.Invnum = rpt.Invnum
left outer join Peoinvdetail detail (nolock) on detail.Companycode = seg.Companycode AND detail.Invnum = seg.Invnum AND detail.segnum = seg.segnum
WHERE 
seg.servtype in ('AIR', 'HTL', 'OTH')
GROUP BY rpt.Companycode, rpt.Invnum, seg.SegType, seg.servtype, seg.Segnum, seg.servname, Servdesc, detail.detltype
print 'Get Segment End @ ' + convert(varchar,getdate(),113)
--SELECT * FROM #tmpSegment

print 'Prepare Report Start @ ' + convert(varchar,getdate(),113)
declare @lnum_Segnum int
select @lnum_Segnum = 1
--Air Routing
while @lnum_Segnum < 10
begin

	UPDATE  #tmpRptResult set 
	Routing = case when rtrim(r.routing) = '' then (convert(char(7), replace(right(rtrim(s.servname), 9), '   ', '/') ))
                       when right(rtrim(r.routing),3) = left((convert(char(7), replace(right(rtrim(s.servname), 9), '   ', '/') )),3) then rtrim(r.routing) + right((convert(char(7), replace(right(rtrim(s.servname), 9), '   ', '/') )),4)
                  else rtrim(r.routing) + '-' + (convert(char(7), replace(right(rtrim(s.servname), 9), '   ', '/') )) end
	FROM #tmpRptResult r, #tmpSegment s 
	WHERE r.invnum = s.invnum
	and s.servtype = 'AIR' and s.segtype ='AIR' and s.segnum = right(('00000' + convert(varchar,@lnum_Segnum)),5)


	select @lnum_Segnum = @lnum_Segnum + 1
end

update #tmpRptResult set Flight = substring(servname, 1, 2) + ' ' + substring(servname, 3, 3)
, Class = substring(servname, 10, 1)
, Depart = substring(servname, 16, 10)
, Servtype = case when s.segtype <> 'SVC' and s.segtype <> 'DOC' then s.servtype  
		  when s.segtype = 'DOC' or s.segtype = 'SVC' then 'OTH' end
FROM #tmpRptResult r, #tmpSegment s
WHERE r.invnum = s.invnum
AND s.servtype = 'AIR'
AND s.segnum = (select min(segnum) from #tmpSegment where invnum = s.invnum and servtype = 'AIR')

update #tmpRptResult set Arrival = substring(servdesc, 8, 10)
FROM #tmpRptResult r, #tmpSegment s
WHERE r.invnum = s.invnum
AND s.servtype = 'AIR'
AND s.segnum = (select max(segnum) from #tmpSegment where invnum = s.invnum and servtype = 'AIR')


update #tmpRptResult set Depart = substring(servdesc, 1, 10)
FROM #tmpRptResult r, #tmpSegment s
WHERE r.invnum = s.invnum
AND s.servtype = 'HTL'
AND s.segnum = (select min(segnum) from #tmpSegment where invnum = s.invnum and servtype = 'HTL')

update #tmpRptResult set Arrival = substring(servdesc, 13, 10)
FROM #tmpRptResult r, #tmpSegment s
WHERE r.invnum = s.invnum
AND s.servtype = 'HTL'
AND s.segnum = (select min(segnum) from #tmpSegment where invnum = s.invnum and servtype = 'HTL')


update #tmpRptResult set airfare = s.sellamt, taxamt = s.taxamt
FROM #tmpRptResult r
, (select invnum, sum(sellamt * qty) as sellamt, sum(taxamt * qty) as taxamt
   from #tmpSegment where servtype = 'AIR' 
   group by invnum) s
WHERE r.invnum = s.invnum

update #tmpRptResult set amtfee = s.amtfee
FROM #tmpRptResult r
, (select invnum, sum(amtfee * qty) as amtfee
   from #tmpSegment where servtype = 'AIR' AND segtype <> 'SVC'  AND segType <> 'DOC' 
   group by invnum) s
WHERE r.invnum = s.invnum

update #tmpRptResult set amtfee = s.amtfee
FROM #tmpRptResult r
, (select invnum, SUM((sellamt + taxamt) * qty) as amtfee
   from #tmpSegment where servtype = 'OTH'  AND segType = 'HLE'
   group by invnum) s
WHERE r.invnum = s.invnum

update #tmpRptResult set othcharge = s.OtherCharge
FROM #tmpRptResult r
, (select invnum, SUM((sellamt + taxamt)* qty * rmnts) as OtherCharge
   from #tmpSegment where servtype = 'HTL'
   group by invnum) s
WHERE r.invnum = s.invnum

update #tmpRptResult set othcharge = s.OtherCharge
FROM #tmpRptResult r
, (select invnum, SUM((sellamt + taxamt)* qty) as OtherCharge
   from #tmpSegment where servtype = 'OTH'  AND segType <> 'HLE'
   group by invnum) s
WHERE r.invnum = s.invnum

UPDATE #tmpRptResult SET ServType = 'OTH'
WHERE servtype = ''


UPDATE #tmpRptResult SET Ticket = 'MISC', Routing = 'MISC', Flight = 'MISC', Class = 'MISC' ,depart = 'MISC', ARRIVAL = 'MISC'
WHERE servtype = 'OTH'


update #tmpRptResult set amtfee = amtfee + c.airfare
from #tmpRptResult t,
(select companycode, mstrinvnum, airfare from #tmpRptResult where mstrinvnum <> '' and mstrinvnum <> invnum) c
where t.companycode = c.companycode and t.invnum = c.mstrinvnum

UPDATE #tmpRptResult set routing = 'MISC', flight ='MISC', class='MISC',arrival='MISC', airfare = 0  where mstrinvnum <> invnum and othcharge > 0 and mstrinvnum <> ''

DELETE FROM #tmpRptResult  where mstrinvnum <> invnum and othcharge = 0 and mstrinvnum <> ''
print 'Prepare Report End @ ' + convert(varchar,getdate(),113)


print 'Retrieve Report Start @ ' + convert(varchar,getdate(),113)
SELECT Companycode, Teamcode, Staffcode, Createon, SERVTYPE, convert(varchar(10),createon,103) as issuedate
, mstrinvnum, cltcode, pax, pnr, Ticket, rtrim(Routing) as Routing, rtrim(Flight) as Flight, rtrim(Class) as Class, Depart, Arrival
, CASE WHEN airfare = 0 THEN '-' ELSE CAST(airfare AS VARCHAR(25)) end as airfare
, CASE WHEN taxamt = 0 THEN '-' ELSE CAST(taxamt AS VARCHAR(25)) end as taxes
, CASE WHEN amtfee = 0 THEN '-' ELSE CAST(amtfee AS VARCHAR(25)) end as fee
, CASE WHEN othcharge = 0 THEN '-' ELSE CAST(othcharge AS VARCHAR(25)) end as othcharge
, invnum, (select count(*) from peoinpax where invnum = #tmpRptResult.invnum and companycode = #tmpRptResult.companycode) as invpaxnum,TicketPax
FROM #tmpRptResult 
order by createon asc, cltcode ,invnum;

print 'Retrieve Report End @ ' + convert(varchar,getdate(),113)


DROP TABLE #tmpResult
DROP TABLE #tmpRptResult;
DROP TABLE #tmpSegment




