﻿
CREATE                        PROCEDURE dbo.sp_RptStaffProductivityReport_ByBranch
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25)

AS

DECLARE @lstr_Staffcode char(10), @lstr_BranchCode char(3), @lstr_CltCode char(6), @lnum_Cost decimal(16,2)
, @ldt_DepartDate datetime, @lstr_bkgref nvarchar(20), @lnum_PaxTotal integer, @lstr_InvType char(3)
DECLARE @FinancalYear datetime, @ldt_StartDate datetime

-- YEAR TO DATE
select @ldt_StartDate = convert(datetime,@START_DATE)
select @FinancalYear = convert(datetime, substring(@START_DATE, 1 , 4) + '-07-01')
select @FinancalYear = case when month(@ldt_StartDate) < 7 then dateadd(year,-1,@FinancalYear) else @FinancalYear end
---------------------------------------------------------------------------------------------



print 'Start Retrieve Invoice at ' + convert(varchar,getdate(),109)
--Invoice
SELECT 
--a.companycode, a.teamcode, a.bkgref, a.staffcode, b.invnum, b.createon, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
a.companycode, staff.branchcode, a.bkgref, a.staffcode, b.invnum, b.createon, sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
, isnull(b.invtype,'') as invtype
INTO #tmpInvTotal
FROM peomstr a (nolock) ,peoinv b (nolock), peostaff staff (nolock) 
--FROM peomstr a (nolock) ,peoinv b (nolock), #tempStaff staff, 
WHERE a.companycode=b.companycode AND a.bkgref=b.bkgref 
AND a.companycode = @COMPANY_CODE
AND b.createon BETWEEN @FinancalYear AND @END_DATE
AND a.staffcode = staff.staffcode
AND a.companycode = staff.companycode
GROUP BY a.companycode, staff.branchcode,  a.bkgref, a.StaffCode, b.invtype, b.invnum, b.createon
ORDER BY 1, 2


CREATE TABLE #tmpDoc
(
	Companycode varchar(2) not null
,	Bkgref varchar(10) not null
,	Branchcode varchar(10) not null
,	Staffcode varchar(10) not null
,	Docnum varchar(10) not null
,	Issueon datetime not null
,	cost decimal(16,2) not null
,	DocType varchar(3) not null
,	isVoid varchar(1) not null
)

print 'Start Retrieve Ticket at ' + convert(varchar,getdate(),109)
--Ticket
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, tkt.Ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost, 'TKT'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.issueon, tkt.netfare+tkt.totaltax cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peotkt tkt(nolock), peomstr mstr (nolock), #tempstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND tkt.issueon between @FinancalYear AND @END_DATE


insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, tkt.Ticket, tkt.voidon, (tkt.netfare+tkt.totaltax) * -1 cost, 'TKT'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, tkt.Ticket, tkt.voidon, (tkt.netfare+tkt.totaltax) * -1 cost, 'TKT'
, case when voidon is null then 'N' else 'Y' end
FROM peotkt tkt(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peotkt tkt(nolock), peomstr mstr (nolock), #tempstaff staff
WHERE tkt.companycode = mstr.companycode and tkt.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND tkt.voidon between @FinancalYear AND @END_DATE


print 'Start Retrieve XO at ' + convert(varchar,getdate(),109)
--XO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, xo.xonum, xo.createon, xo.hkdamt cost  , 'XO'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.createon, xo.hkdamt cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peoxo xo(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND xo.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, xo.xonum, xo.voidon, xo.hkdamt * -1 cost  , 'XO'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, xo.xonum, xo.voidon, xo.hkdamt * -1 cost  , 'XO'
, case when voidon is null then 'N' else 'Y' end
FROM peoxo xo(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peoxo xo(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE xo.companycode = mstr.companycode and xo.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND xo.voidon between @FinancalYear AND @END_DATE

print 'Start Retrieve Voucher at ' + convert(varchar,getdate(),109)
--Voucher
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, vch.vchnum, vch.createon, vch.hkdamt cost  , 'VCH'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.createon, vch.hkdamt cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peovch vch(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND vch.createon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, vch.vchnum, vch.voidon, vch.hkdamt * -1 cost  , 'VCH'
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode,mstr.StaffCode, vch.vchnum, vch.voidon, vch.hkdamt * -1 cost  , 'VCH'
, case when voidon is null then 'N' else 'Y' end
FROM peovch vch(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peovch vch(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE vch.companycode = mstr.companycode and vch.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND vch.voidon between @FinancalYear AND @END_DATE

print 'Start Retrieve MCO at ' + convert(varchar,getdate(),109)
--MCO
insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, mco.mconum, mco.issueon, mco.costamt + mco.taxamt cost, 'MCO' 
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.issueon, mco.costamt + mco.taxamt cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peomco mco(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND mco.issueon between @FinancalYear AND @END_DATE

insert into #tmpDoc
SELECT mstr.companycode, mstr.bkgref, staff.Branchcode, mstr.StaffCode, mco.mconum, mco.voidon, (mco.costamt + mco.taxamt) * -1 cost, 'MCO' 
--SELECT mstr.companycode, mstr.bkgref, mstr.teamcode, mstr.StaffCode, mco.mconum, mco.voidon, (mco.costamt + mco.taxamt) * -1 cost, 'MCO' 
, case when voidon is null then 'N' else 'Y' end
FROM peomco mco(nolock), peomstr mstr (nolock), peostaff staff (nolock)
--FROM peomco mco(nolock), peomstr mstr (nolock), #tempStaff staff
WHERE mco.companycode = mstr.companycode and mco.bkgref = mstr.bkgref
AND mstr.companycode = @COMPANY_CODE
AND mstr.staffcode  = staff.staffcode
AND mstr.companycode = staff.companycode
AND mco.voidon between @FinancalYear AND @END_DATE

print 'Start Generating Report at ' + convert(varchar,getdate(),109)

--Start Generating for Report
CREATE TABLE #TmpSellAmt
(
	Companycode nvarchar(10) null default ''
,	BranchCode nvarchar(50) null default ''
,       PaxTotal integer null default 0
,	Price decimal(16,2) null default 0
,	Dep decimal(16,2) null default 0
,	Cost decimal(16,2) null default 0
, 	YTDPrice decimal(16,2) null default 0
,	YTDDep decimal(16,2) null default 0
,	YTDCost decimal(16,2) null default 0
,	TktCount int null default 0
)

insert into #TmpSellAmt
(Companycode, Branchcode)
select distinct s.companycode, s.branchcode from peostaff s(nolock)
--select distinct s.companycode, s.teamcode from peostaff s(nolock), #tempStaff staff
where s.companycode = @COMPANY_CODE --and s.staffcode = staff.staffcode

--Selling (YTD Selling)
UPDATE #TmpSellAmt set YTDPrice = YTDPrice + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (YTD Deposit)
UPDATE #TmpSellAmt set YTDPrice = YTDDep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Selling)
UPDATE #TmpSellAmt set Price = Price + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE <> 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Selling (MTD Deposit)
UPDATE #TmpSellAmt set Dep = Dep + i.sell
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(Sell) as Sell
--(Select CompanyCode, TeamCode, Sum(Sell) as Sell
From #TmpInvTotal 
where INVTYPE = 'DEP'
and createon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Cost (YTD Cost)
UPDATE #TmpSellAmt set YTDCost = YTDCost + i.cost
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, Sum(cost) as cost
--(Select CompanyCode, TeamCode, Sum(cost) as cost
From #TmpDoc 
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Cost (MTD Cost)
UPDATE #TmpSellAmt set Cost = Cost + i.costs
From #TmpSellAmt t,
--(Select CompanyCode, TeamCode, Sum(cost) as costs
(Select CompanyCode, BranchCode, Sum(cost) as costs
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode


--Ticket Count
UPDATE #TmpSellAmt set TktCount = TktCount + i.counts
From #TmpSellAmt t,
(Select CompanyCode, BranchCode, count(1) as counts
--(Select CompanyCode, TeamCode, count(1) as counts
From #TmpDoc 
WHERE issueon between @START_DATE AND @END_DATE
AND isVoid = 'N'
AND DocType = 'TKT'
AND cost >= 0
Group By CompanyCode, BranchCode ) i
where i.companycode = t.companycode and i.Branchcode = t.Branchcode
--Group By CompanyCode, TeamCode ) i
--where i.companycode = t.companycode and i.teamcode = t.teamcode

--Passenger
UPDATE #TmpSellAmt set PaxTotal = PaxTotal + i.paxcount
From #TmpSellAmt t,
(SELECT inv.companycode, inv.Branchcode, count(1) paxcount
--(SELECT inv.companycode, inv.teamcode, count(1) paxcount
FROM peopax pax(nolock), #TmpInvTotal inv
WHERE pax.companycode = inv.companycode and pax.bkgref = inv.bkgref
AND inv.createon between @START_DATE AND @END_DATE
AND pax.companycode = inv.companycode
GROUP BY inv.companycode, inv.Branchcode) i 
WHERE i.companycode = t.companycode and i.Branchcode = t.Branchcode
--GROUP BY inv.companycode, inv.Teamcode) i 
--WHERE i.companycode = t.companycode and i.teamcode = t.teamcode




SELECT Companycode
, branchcode as code, PaxTotal as [Total Pax]
, TktCount as [TktCount]
, CASE when Price <> 0 then CONVERT(VARCHAR,Price) else '-' END as Price
, CASE when Dep <> 0 then CONVERT(VARCHAR,Dep) else '-' END as Dep
, CASE WHEN cost <> 0 THEN CONVERT(VARCHAR,cost) else '-' END AS Cost
, CASE WHEN price-cost <> 0 THEN CONVERT(VARCHAR,price-cost) ELSE '-' END as Margin
, CASE isnull(Price,0) when 0 then 0 else (convert(decimal(16,2),(Price-cost)*100/price)) end [Yield(%)]
, CASE when YTDPrice <> 0 then CONVERT(VARCHAR,YTDPrice) else '-' END as YTDPrice
, CASE when YTDDep <> 0 then CONVERT(VARCHAR,YTDDep) else '-' END as YTDDep
, CASE WHEN YTDcost <> 0 THEN CONVERT(VARCHAR,YTDcost) else '-' END AS YTDCost
, CASE WHEN YTDprice-YTDcost <> 0 THEN CONVERT(VARCHAR,YTDprice-YTDcost) ELSE '-' END as YTDMargin
, CASE isnull(YTDPrice,0) when 0 then 0 else (convert(decimal(16,2),(YTDPrice-YTDcost)*100/YTDprice)) end [YTDYield(%)]

FROM #TmpSellAmt
ORDER BY branchcode


-- Drop Table
drop table #TmpSellAmt;
--drop table #tempstaff;

























