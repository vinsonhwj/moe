﻿
--exec CustomerSalesReport_By_Salesman_V6 '2010-09-01','2010-09-30','','%HS0291%','sg','cds'


CREATE   PROCEDURE CustomerSalesReport_By_Salesman_V6
-- Customer Sales Report By NXT
@StartDate CHAR(10),
@EndDate CHAR(10),
@SalesMan VARCHAR(10),
@cltCode VarChar(10) = '%',
@CompanyCode varChar(2)  = 'SG',
@Login nvarchar(20)
AS
BEGIN




DECLARE @StartDateTime DATETIME, @EndDateTime DATETIME;

SELECT @StartDateTime = @StartDate, @EndDateTime = DATEADD(dd, 1, CONVERT(DATETIME, @EndDate));

CREATE TABLE #Salesman
(
	Salesman nchar(200)
)

Create Table #TmpTeam (TeamCode varchar(10) not null);

if ltrim(rtrim(@SalesMan)) = ''
begin
---- Modify at 09Apr2010 for migrate with Booking Activity Report By Customer
	insert into #TmpTeam
	select s.TeamCode
	from viewother o (nolock), team s (nolock) 
	where s.companycode = o.viewcompany
	and o.COMPANYCODE = @CompanyCode AND o.login = @Login AND o.viewbranch is nulL
	
	insert into #TmpTeam
	select s.TeamCode
	from viewother o (nolock), team s (nolock) 
	where s.companycode = o.viewcompany AND s.BranchCode = o.ViewBranch
	and o.COMPANYCODE = @CompanyCode AND o.login = @Login AND o.viewteam is null


	insert into #TmpTeam
	select s.TeamCode
	from viewother o (nolock), team s (nolock) 
	where s.companycode = o.viewcompany AND s.BranchCode = o.ViewBranch and s.teamcode = o.viewteam
	and o.COMPANYCODE = @CompanyCode AND o.login = @Login AND o.viewteam is not null

	insert into #Salesman select Staffcode from peostaff s(nolock),#TmpTeam t  where s.CompanyCode = @CompanyCode and s.Teamcode = t.TeamCode

--	if exists (SELECT Login FROM StaffSalesmanMap WHERE CompanyCode=@CompanyCode and Login=@Login)
--	begin
--		insert into #Salesman
--		select salesman from StaffSalesmanMap WHERE CompanyCode=@CompanyCode and Login=@Login
--	end
---- End Modify at 09Apr2010 for migrate with Booking Activity Report By Customer
end
else
begin 
	insert into #Salesman values (@Salesman)
end




SELECT I.Bkgref, i. CLTCODE,
	(	SELECT Sum(SellAmt)
		FROM PeoInv INV (nolock)
		WHERE I.BkgRef = INV.Bkgref
		AND SubString(inv.InvNum, 2, 1) = 'I'
		AND Inv.cltCode= i.cltcode
		AND INV.CreateOn BETWEEN @StartDateTime AND @EndDateTime 
	)  AS [Total Invoice],
	(	SELECT SUM(SellAmt)
		FROM PeoInv VINV (nolock)
		WHERE I.BkgRef = VINV.Bkgref
		AND SubString(vinv.InvNum, 2, 1) = 'C'
		AND vInv.cltCode = i.cltcode
		AND VINV.CreateOn BETWEEN @StartDateTime AND @EndDateTime  
	) AS [Total Credit Note],
	(	Select Sum(sellAmt) 
		From peoinv inv (nolock)
		Where inv.bkgref=I.bkgRef
		AND INV.CreateOn BETWEEN @StartDateTime AND @EndDateTime 
		AND inv.voidon IS NULL
		AND Inv.cltCode = i.cltCode
		And SubString(inv.InvNum, 2, 1) = 'I'
	) As costNo,		
	(	Select Sum(sellAmt) 
		From peoinv inv (nolock)
		Where inv.bkgref=I.bkgRef
		AND INV.CreateOn BETWEEN @StartDateTime AND @EndDateTime 
		AND inv.voidon is null
		AND SubString(inv.InvNum, 2, 1) = 'I'
	) As TotalcostNo,
	(	SELECT SUM(HKDAmt)
		FROM PeoMco MCO (nolock)
		WHERE I.BkgRef = MCO.Bkgref
		AND VoidOn IS NULL
		AND MCO.CreateOn BETWEEN @StartDateTime AND @EndDateTime
	)  AS [Total MCO],
	(	SELECT SUM(HKDAmt)
		FROM PeoVch V (nolock)
		WHERE I.BkgRef = V.Bkgref
		AND VoidOn IS NULL
		AND V.CreateOn BETWEEN @StartDateTime AND @EndDateTime
	)  AS [Total Voucher],
	(	SELECT SUM(HKDAmt)
		FROM PeoXO X (nolock)
		WHERE I.BkgRef = X.Bkgref
		AND VoidOn IS NULL
		AND X.CreateOn BETWEEN @StartDateTime AND @EndDateTime
	)  AS [Total XO],
	(	SELECT SUM(NetFare+TotalTax)
		FROM PeoTKT T (nolock)
		WHERE I.BkgRef = T.Bkgref AND VoidOn IS NULL
		AND T.CreateOn BETWEEN @StartDateTime AND @EndDateTime
	) AS [Total TKT],
	(	SELECT COUNT(*)
		FROM PeoPax P (nolock)
		WHERE P.BkgRef = I.Bkgref AND VoidOn IS NULL
		AND P.CreateOn BETWEEN @StartDateTime AND @EndDateTime
	) AS [Total Pax]
INTO #BookingSum
FROM dbo.peoinv I (nolock)
WHERE I.cltCode IN (
	SELECT cltode
	FROM peomstr (nolock)
	WHERE companyCode=@CompanyCode
	AND (
		salesperson IN (SELECT salesman from #Salesman)
		OR ISNULL(salesperson,'') = LTRIM(RTRIM(@SalesMan)) 
	)
	AND cltode like @cltcode
)
AND I.CreateOn  BETWEEN @StartDateTime AND @EndDateTime
AND I.companyCode = @CompanyCode
GROUP BY I.BKGREF, I.CLTCODE


--SELECT * FROM #BOOKINGSUM 
Select a.cltcode,a.bkgref, b.teamcode,  b.staffCode,convert(char(11), b.departdate,113) as departDate, 
SUM([Total Pax]) AS [Total Pax],
CAST(
ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) 
 AS Decimal(12,2) ) [Price],
CAST(
(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0)+ISNULL(SUM( [Total MCO]), 0))
*
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
Else
	Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
End
 AS Decimal(12,2) 
) [Cost],
CAST(
	(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)
   	  *
	  CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
 	  Else
		Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	 End
                )
	 AS Decimal(12,2) )  [Margin],
[Yield(%)] = 
CASE 
WHEN 	(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(
                 ( ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)) )
	    *
	   CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
	   Else
  		Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
	  End
                 = 0 THEN 
	CAST(
		0
	 AS Decimal(12,2) )
WHEN 	Convert(Decimal,ISNULL(SUM([Total Invoice]), 0))  -  Convert(Decimal,ISNULL(SUM([Total Credit Note]), 0)) <1 THEN
	CAST(
		-100
	 AS Decimal(12,2) )
ELSE
	CAST(
 	(
		(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) + ISNULL(SUM( [Total MCO]), 0)) 
	             *
		   CASE WHEN Max(TotalcostNo)=0 Then
			Convert(Decimal,1)
		   Else
  			Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
		  End
	) /
		(Case When (ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0)) =0 Then 1 Else (ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0)) End)  * 100

	AS Decimal(12,2) )
END
,
CASE WHEN Max(TotalcostNo)=0 Then
		Convert(Decimal,1)
ELSE
	Convert(Decimal,(Case When Max(costNo)=0 Then 0 Else Max(CostNo) End )) / Convert(Decimal,Case When Max(TotalcostNo)=0 Then 1 Else Max(TotalCostNo) End)
END AS proportion
INTO #BookingSumDtl
FROM 
 #BookingSum a , peomstr b (nolock)
WHERE a.bkgref = b.bkgref and b.companycode=@companyCode
--AND ISNULL(a.[Total Invoice], 0)  -  ISNULL(a.[Total Credit Note], 0) >0
GROUP BY a.cltcode,a.bkgref, b.teamcode, b.departdate, b.staffCode 
ORDER BY a.cltcode, a.bkgref, b.teamcode, b.staffCode


SELECT C.cltName,A.cltcode AS [Client Code],SUM( isnull(Price,0.00) ) AS Price,SUM( isnull(cost,0.00) ) AS Cost,  SUM( isnull([Total Pax], 0) ) AS [Total Pax],
	 SUM( isnull(Price,0.00) )-Sum( isnull(cost,0.00) ) AS [Margin], (SUM( isnull(Price,0.00) )-Sum( isnull(cost,0.00) )) / (Case When Sum( isnull(Price,0.00) ) =0 Then 1 Else Sum( isnull(Price,0.00) ) End) * 100 AS [Yield(%)]
Into #BookingResult
FROM #BookingSumDtl a, Customer c (noLock)
WHERE A.cltCode = C.cltCode
GROUP BY A.cltCode, C.cltName

Select * from #BookingResult
--Select * from #BookingSumDtl


DROP TABLE #BookingSum
DROP TABLE #BookingSumDtl
DROP TABLE #Salesman
DROP TABLE #BookingResult
DROP TABLE #TmpTeam

END



