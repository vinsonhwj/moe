﻿

CREATE  PROCEDURE dbo.CustomerSalesReport_By_DepartureDate
@StartDate CHAR(10),
@EndDate CHAR(10),
@TeamCode VarChar(10) = '%',
@CompanyCode VarChar(2) = 'SG', 
@Report_TypeCode VarChar(5) = 'SPRBC',
@StaffCode VarChar(5) = '%'		
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
select @TeamCode =  '%' + @TeamCode + '%'
select @StaffCode = '%' + @StaffCode + '%'


		
SELECT         I.Bkgref, 
		I.CompanyCode,						
		I.StaffCode,
		I.TeamCode,
                              (SELECT         SUM(InvAmt + InvTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @StartDate AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode) 
                          AS [Total Invoice],
				ISNULL((SELECT sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
				FROM peoinv b (nolock)
				WHERE b.companycode = I.companycode AND b.bkgref = I.bkgref and b.invtype ='DEP'), 0)
			  AS [Total Dep],
                              (SELECT         SUM(DocAmt + DocTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @StartDate AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode) 
                          AS [Total DocAmt],
                              ISNULL((SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND I.CompanyCode = p.CompanyCode
				AND P.CreateOn BETWEEN @StartDate AND @enddayofyear), 0) 
	        AS [Total Pax]
INTO #BookingSum
FROM              dbo.peomstr I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
AND I.companycode = @CompanyCode
GROUP BY I.Bkgref, I.CompanyCode, I.TeamCode, I.StaffCode
SELECT         I.Bkgref, 
		I.CompanyCode,						
		I.StaffCode,
		I.TeamCode,
                              ISNULL((SELECT         SUM(InvAmt + InvTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @firstdayofyear AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode), 0) 
                          AS [Total Invoice],
				ISNULL((SELECT sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
				FROM peoinv b (nolock)
				WHERE b.companycode = I.companycode AND b.bkgref = I.bkgref and b.invtype ='DEP') , 0)
			  AS [Total Dep],
                              ISNULL((SELECT         SUM(DocAmt + DocTax)
                                FROM              PeoMstr m (nolock)
                                WHERE          I.BkgRef = m.Bkgref AND I.CompanyCode = m.CompanyCode
				AND m.CreateOn BETWEEN @firstdayofyear AND @enddayofyear 
				AND I.TeamCode = m.TeamCode AND I.StaffCode = m.StaffCode), 0) 
                          AS [Total DocAmt],
                              ISNULL((SELECT         COUNT(*)
                                FROM              PeoPax P (nolock)
                                WHERE          I.BkgRef = P.Bkgref AND I.CompanyCode = p.CompanyCode
				AND P.CreateOn BETWEEN @firstdayofyear AND @enddayofyear), 0) 
	        AS [Total Pax]
INTO #BookingSum_year_to_date
FROM              dbo.peomstr I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
AND I.companycode = @CompanyCode
GROUP BY I.Bkgref, I.CompanyCode, I.TeamCode, I.StaffCode
SELECT 
m.CompanyCode,
m.TeamCode,
m.StaffCode,
CONVERT(Decimal(20,2), SUM(b.[Total Pax])) AS [Total Pax],
CONVERT(Decimal(20,2), SUM(b.[Total Invoice]-b.[Total Dep])) AS [Price],
CONVERT(Decimal(20,2), SUM(b.[Total Dep])) AS [Dep],
CONVERT(Decimal(20,2), SUM(b.[Total DocAmt])) AS [Cost],
(CONVERT(Decimal(20,2), SUM(b.[Total Invoice]-b.[Total Dep])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) AS [Margin],
CONVERT(Decimal(20,2), CASE 
	WHEN (CONVERT(Decimal(20,0),(CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt])))) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), 0) 
	WHEN (CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), -100) 
	ELSE
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) /
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) * 100) END) AS [Yield(%)]
INTO #result_normal
FROM 
#BookingSum b,
peomstr m (nolock)
WHERE
m.CompanyCode = b.CompanyCode
AND m.bkgRef = b.bkgRef
AND m.TeamCode Like @TeamCode
AND m.StaffCode Like @StaffCode
GROUP BY m.CompanyCode, m.TeamCode, m.StaffCode
SELECT 
m.CompanyCode,
m.TeamCode,
m.StaffCode,
CONVERT(Decimal(20,2), SUM(b.[Total Pax])) AS [Total Pax],
CONVERT(Decimal(20,2), SUM(b.[Total Invoice]-b.[Total Dep])) AS [Price],
CONVERT(Decimal(20,2), SUM(b.[Total Dep])) AS [Dep],
CONVERT(Decimal(20,2), SUM(b.[Total DocAmt])) AS [Cost],
(CONVERT(Decimal(20,2), SUM(b.[Total Invoice]-b.[Total Dep])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) AS [Margin],
CONVERT(Decimal(20,2), CASE 
	WHEN (CONVERT(Decimal(20,0),(CONVERT(Decimal(20,0), SUM(b.[Total Invoice])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt])))) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), 0) 
	WHEN (CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) = CONVERT(Decimal(20,0), 0)) THEN
		CONVERT(Decimal(20,0), -100) 
	ELSE
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) - CONVERT(Decimal(20,0), SUM(b.[Total DocAmt]))) /
		(CONVERT(Decimal(20,0), SUM(b.[Total Invoice]-b.[Total Dep])) * 100) END) AS [Yield(%)]
INTO #result_year_to_date
FROM 
#BookingSum_year_to_date b,
peomstr m (nolock)
WHERE
m.CompanyCode = b.CompanyCode
AND m.bkgRef = b.bkgRef
AND m.TeamCode Like @TeamCode
AND m.StaffCode Like @StaffCode
GROUP BY m.CompanyCode, m.TeamCode, m.StaffCode
IF @Report_TypeCode = 'SPRBC' 
BEGIN
SELECT ltrim(rtrim(a.companycode)) as companycode
, ltrim(rtrim(a.teamcode)) + '/' + ltrim(rtrim(a.staffcode)) as code
, ltrim(rtrim(a.staffcode)) as staffcode
, case when sum(a.[Total Pax]) <> 0 then 
convert(char, sum(a.[Total Pax])) else '-' end as [Total Pax]
, case when sum(a.[Price]) <> 0 then
convert(char, sum(a.[Price])) else '-' end as Price
, case when sum(a.[Dep]) <> 0 then
convert(char, sum(a.[Dep])) else '-' end as [Dep]

, case when sum(a.[Cost]) <> 0 then 
convert(char, sum(a.[Cost])) else '-' end as Cost 
, case when sum(a.[Margin]) <> 0 then
convert(char, sum(a.[Margin])) else '-' end as Margin
,case when sum(a.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(a.[Margin])/sum(a.[Price])) * 100)) else '-' end as [Yield(%)]
, case when sum(b.Price) <> 0 then 
convert(char, sum(b.Price)) else '-' end as YTDPrice
, case when sum(b.Dep) <> 0 then 
convert(char, sum(b.Dep)) else '-' end as [YTDDep]
, case when sum(b.Cost) <> 0 then
convert(char, sum(b.Cost)) else '-' end as YTDCost
, case when sum(b.Margin) <> 0 then 
convert(char, sum(b.Margin)) else '-' end as YTDMargin
,case when sum(b.[Price]) <> 0 then
convert(char, convert(decimal(20,2), (sum(b.[Margin])/sum(b.[Price])) * 100)) else '-' end as [YTDYield(%)]
FROM 
#result_normal a, #result_year_to_date b
WHERE a.companycode = b.companycode
and a.teamcode = b.teamcode
and a.staffcode = b.staffcode
GROUP BY a.companycode, a.teamcode, a.staffcode
ORDER BY a.staffcode
END
DROP TABLE #BookingSum
DROP TABLE #BookingSum_year_to_date
DROP TABLE #result_normal
DROP TABLE #result_year_to_date
END



