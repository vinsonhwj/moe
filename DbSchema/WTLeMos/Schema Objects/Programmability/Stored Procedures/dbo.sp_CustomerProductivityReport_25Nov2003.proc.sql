﻿create PROCEDURE dbo.sp_CustomerProductivityReport_25Nov2003
@StartDate CHAR(10),
@EndDate CHAR(10),
@cltCode VarChar(10) = '%',
@companycode VARCHAR(10) = 'SG'
AS
BEGIN
declare @firstdayofyear CHAR(10)
declare @enddayofyear CHAR(22)
if substring(@StartDate, 1 , 4) = '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-09-01'
	end
if substring(@StartDate, 1 , 4) <> '2003' 
	begin
		select @firstdayofyear = substring(@StartDate, 1 , 4) + '-01-01'
	end
select @enddayofyear = @EndDate + ' 23:59:59.99'
select @cltCode = '%' + @cltCode + '%'
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
		i.cltcode,
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock) 
                                WHERE          I.BkgRef = INV.Bkgref and i.cltcode = inv.cltcode AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref and i.cltcode = vinv.cltcode AND SubString(vinv.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @StartDate AND @enddayofyear
				)  as [TOTAL Price],
							
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @StartDate AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @StartDate AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(NetFare,0)+isnull(TotalTax,0)),0)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @StartDate AND @enddayofyear) 
				+
				(SELECT         isnull(SUM(isnull(mco.costamt,0) + isnull(mco.taxamt,0)),0)
                                FROM              peomco mco (NoLock)
                                WHERE          I.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL
				AND mco.createon between @StartDate AND @enddayofyear) AS [Total Cost]
				,
				(SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @StartDate AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref AND SubString(VINV.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @StartDate AND @enddayofyear
				)  as [TOTAL BKGREFPrice]
into #Booksum_by_cltcode
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @StartDate AND @enddayofyear
and I.companycode = @CompanyCode
and I.cltcode like @cltCode
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
, I.cltcode
SELECT         I.Bkgref, 
		-- i. TeamCode, 								-- commented by IVT
		i.companycode,									-- added by IVT
		i.cltcode,
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock) 
             WHERE          I.BkgRef = INV.Bkgref and i.cltcode = inv.cltcode AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref and i.cltcode = vinv.cltcode AND SubString(vinv.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				)  as [TOTAL Price],
							
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoVch V (nolock)
                                WHERE          I.BkgRef = V.Bkgref AND VoidOn IS NULL
				AND V.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(HKDAmt,0)), 0)
                                FROM              PeoXO X (nolock)
                                WHERE          I.BkgRef = X.Bkgref AND VoidOn IS NULL
				AND X.CreateOn BETWEEN @firstdayofyear AND @enddayofyear)  
				+
                              (SELECT         isnull(SUM(isnull(NetFare,0)+isnull(TotalTax,0)),0)
                                FROM              PeoTKT T (nolock)
                                WHERE          I.BkgRef = T.Bkgref AND VoidOn IS NULL
				AND T.CreateOn BETWEEN @firstdayofyear AND @enddayofyear) 
				+
				(SELECT         isnull(SUM(isnull(mco.costamt,0) + isnull(mco.taxamt,0)),0)
                                FROM              peomco mco (NoLock)
                                WHERE          I.BkgRef = mco.Bkgref AND mco.VoidOn IS NULL
				AND mco.createon between @firstdayofyear AND @enddayofyear) AS [Total Cost]
				,
				(SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv INV (nolock)
                                WHERE          I.BkgRef = INV.Bkgref AND SubString(inv.InvNum, 2, 1) = 'I' 
				AND INV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				 )  - 
                              (SELECT         isnull(SUM(isnull(SellAmt, 0)), 0)
                                FROM              PeoInv VINV (nolock)
                                WHERE          I.BkgRef = VINV.Bkgref AND SubString(VINV.InvNum, 2, 1) = 'C' 
				AND VINV.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
				)  as [TOTAL BKGREFPrice]
into #Booksum_by_cltcodeYTD
FROM              dbo.peoinv I (nolock)
WHERE	 
I.CreateOn BETWEEN @firstdayofyear AND @enddayofyear
and I.companycode = @CompanyCode
and I.cltCode in (select distinct cltcode from #Booksum_by_cltcode (nolock))
GROUP BY I.BKGREF	-- , I.TeamCode									-- comment by IVT
, I.companycode												-- added by IVT
, I.cltcode
-- companycode,cltcode,cltname,Price,Cost,Margin,Yield(%),proportion
select 
ltrim(rtrim(t.companycode)) as companycode
,ltrim(rtrim(t.cltcode)) as cltcode
, (select ltrim(rtrim(isnull(cltname, cltcode))) from customer where cltcode = t.cltcode) as cltname
,
case when sum(t.[total price]) <> 0 then cast(sum(t.[total price]) as varchar)
else '-' end as [Price]
, 
case when sum(t.[total cost]) <> 0 then cast(sum(t.[total cost]) as varchar) 
else '-' end as [Cost]
, 
case when sum(t.[total margin]) <> 0 then cast(sum(t.[total margin]) as varchar)
else '-' end as [Margin]
, 
case when sum(t.[total price]) = 0 or sum(t.[total margin]) = 0 then '-' else
cast(
case when sum(t.[total price]) <> 0 then
cast(sum(t.[total margin]) * 100 / sum(t.[total price]) as decimal(20,2)) else
cast(0 as decimal(20,2)) end as varchar) 
end as [Yield(%)]
,
case when sum(t.[total ytdprice]) <> 0 then cast(sum(t.[total ytdprice]) as varchar)
else '-' end as [YTDPrice]
, 
case when sum(t.[total ytdcost]) <> 0 then cast(sum(t.[total ytdcost]) as varchar) 
else '-' end as [YTDCost]
, 
case when sum(t.[total ytdmargin]) <> 0 then cast(sum(t.[total ytdmargin]) as varchar)
else '-' end as [YTDMargin]
, 
case when sum(t.[total ytdprice]) = 0 or sum(t.[total ytdmargin]) = 0 then '-' else
cast(
case when sum(t.[total ytdprice]) <> 0 then
cast(sum(t.[total ytdmargin]) * 100 / sum(t.[total ytdprice]) as decimal(20,2)) else
cast(0 as decimal(20,2)) end  as varchar)
end as [YTDYield(%)]
from (
select 
b.companycode
, b.bkgref
, b.cltcode
, b.[TOTAL Price]
, 
case 
when b.[Total bkgrefprice] <> 0 then
cast(b.[Total Cost] * (b.[Total Price]/b.[TOTAL BKGREFPrice]) as decimal(20,2))
else cast(0 as decimal(20,2)) end as [Total Cost]
,
b.[Total Price] - 
(case 
when b.[Total bkgrefprice] <> 0 then
cast(b.[Total Cost] * (b.[Total Price]/b.[TOTAL BKGREFPrice]) as decimal(20,2))
else cast(0 as decimal(20,2)) end)  as [Total Margin]
-- YTD
, y.[TOTAL Price] as [Total YTDPrice]
, 
case 
when y.[Total bkgrefprice] <> 0 then
cast(y.[Total Cost] * (y.[Total Price]/y.[TOTAL BKGREFPrice]) as decimal(20,2))
else cast(0 as decimal(20,2)) end as [Total YTDCost]
,
y.[Total Price] - 
(case 
when y.[Total bkgrefprice] <> 0 then
cast(y.[Total Cost] * (y.[Total Price]/y.[TOTAL BKGREFPrice]) as decimal(20,2))
else cast(0 as decimal(20,2)) end)  as [Total YTDMargin]
from #Booksum_by_cltcode b (nolock), #Booksum_by_cltcodeYTD y (nolock)
where b.companycode = y.companycode and b.bkgref = y.bkgref and b.cltcode = y.cltcode
) as t 
group by t.companycode, t.cltcode
order by sum(t.[total Price]) desc
drop table #Booksum_by_cltcode
drop table #Booksum_by_cltcodeYTD
END
