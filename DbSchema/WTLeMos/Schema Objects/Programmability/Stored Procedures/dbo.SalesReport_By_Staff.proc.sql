﻿

CREATE PROCEDURE SalesReport_By_Staff

@StartDate CHAR(10),
@EndDate CHAR(10),
@StaffCode VarChar(10) = '%'

 AS

BEGIN
select B.BKGREF, B.TEAMCODE,b. Staffcode, 

-- CONVERT(CHAR(3), SUM([Total Pax])) [Total Pax],

CAST(
ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) 
 AS Decimal(12,2) ) [Invoice],
CAST(
ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0)
 AS Decimal(12,2) ) [Document]
/*
,
CAST(
	(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0))
	 AS Decimal(12,2) )  [Margin],

[Yield(%)] = 
CASE 

WHEN 	(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) )  = 0 THEN 
	CAST(
		0
	 AS Decimal(12,2) )


WHEN 	ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(12,2) )

ELSE

	CAST(
 	(
		(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0))
	) /
		(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  * 100
	AS Decimal(12,2) )

END
*/
From 
--select * from
BookSum_by_Staff b, customer c
where b.cltode = c.cltcode
and b.BookingDate BETWEEN @StartDate AND @EndDate
and b.StaffCode like @StaffCode
group by B.BKGREF, B.TEAMCODE,b. Staffcode

END

