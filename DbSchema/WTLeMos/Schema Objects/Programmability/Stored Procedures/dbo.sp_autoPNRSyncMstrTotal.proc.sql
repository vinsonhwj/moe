﻿CREATE     PROCEDURE sp_autoPNRSyncMstrTotal
AS 
	BEGIN
		SELECT COUNT(*) as Total FROM wmiur..peomstr iur
		LEFT OUTER JOIN wmemos..peomstr wme ON wme.bkgref = iur.bkgref AND wme.COMPANYCODE=iur.COMPANYCODE
		WHERE wme.bkgref is null and iur.createon >= '2006-04-01'
		AND iur.companycode = 'WM' or iur.companycode = 'JT'
	END

