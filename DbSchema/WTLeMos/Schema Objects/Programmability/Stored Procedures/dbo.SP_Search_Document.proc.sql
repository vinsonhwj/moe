﻿



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Search_Document]
	-- Add the parameters for the stored procedure here
    @companycode NVARCHAR(2) ,
    @actioncode NVARCHAR(5) ,
    @docnum NVARCHAR(20)
AS 
    DECLARE @SQL NVARCHAR(4000)
    IF @actioncode = 'TKT' 
        BEGIN 
            SELECT  @SQL = 'select TOP 100 BKGREF,left(DocNum, 10) as docnum,Description as Description,ACTIONCODE from peonyref where COmpanycode='''
                    + @companycode + ''''
        END
    ELSE 
        BEGIN
            SELECT  @SQL = 'select TOP 100 BKGREF,DocNum,Description,ACTIONCODE from peonyref where COmpanycode='''
                    + @companycode + ''''
        END
    IF @actioncode <> '' 
        BEGIN
            IF @actioncode = 'INV' 
                BEGIN
                    SELECT  @SQL = @SQL
                            + 'and (actioncode like N''%INV%'' or actioncode like N''%RPT%'' or actioncode like N''%DEP%'')'
                END
               
            IF @actioncode = 'IRW' 
                BEGIN
                    SELECT  @SQL = @SQL
                            + 'and ((actioncode like N''%INV%'' or actioncode like N''%RPT%'') and substring(docnum, 2, 1) <> ''C'')' 
                END
                
            IF @actioncode = 'CRN' 
                BEGIN 
                    SELECT  @SQL = @SQL
                            + ' and (actioncode like N''%CRN%'' or actioncode = ''VINV'' or actioncode like N''%DPC%'')'
                END
            IF @actioncode = 'MCO' 
                BEGIN
                    SELECT  @SQL = @SQL + ' and actioncode = N''MPD'''
                END
            IF @actioncode = 'TKT' 
                BEGIN
                    SELECT  @SQL = @SQL + ' and actioncode like N''%TKT'''
                END
            IF @actioncode = 'VCH' 
                BEGIN
                    SELECT  @SQL = @SQL
                            + ' and ( actioncode like N''%VCH'' or actioncode like N''%CAR'' )'
                END
                
            IF @actioncode = 'XO' 
                BEGIN
                    SELECT  @SQL = @SQL + ' and actioncode like N''%XO%'''
                END
            
            
        END
    IF @docnum <> '' 
        BEGIN
           
            SELECT  @SQL = ( CASE WHEN @actioncode = 'INV'
                                       OR @actioncode = 'CRN'
                                       OR @actioncode = 'XO'
                                       OR @actioncode = 'VCH'
                                       OR @actioncode ='IRW'
                                  THEN ( CASE WHEN LEN(@docnum) = '10'
                                              THEN @SQL + 'and DocNum='''
                                                   + @docnum + ''''
                                              ELSE @SQL + 'and DocNum LIKE''%'
                                                   + @docnum + ''''
                                         END )
                                  WHEN @actioncode = 'MCO'
                                  THEN ( CASE WHEN LEN(@docnum) = '14'
                                              THEN @SQL
                                                   + ' and Left(DocNum,10) = '''
                                                   + @docnum + ''''
                                              ELSE @SQL + 'and DocNum LIKE'''
                                                   + @docnum + '%'''
                                         END )
                               
                                  WHEN @actioncode = 'TKT'
                                  THEN @SQL + 'and DocNum LIKE ''' + @docnum
                                       + '%'''
                                  ELSE @SQL + 'and DocNum LIKE''%' + @docnum
                                       + ''''
                             END )
           
           
             
        END
     SELECT @SQL=@SQL+'order by DocNum DESC, Createon DESC'
        
    --SELECT  @SQL
    EXEC(@SQL)
       
     
       
     
       


