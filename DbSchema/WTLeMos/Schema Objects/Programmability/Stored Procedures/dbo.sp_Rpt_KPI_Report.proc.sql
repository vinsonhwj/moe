﻿
CREATE PROCEDURE [dbo].sp_Rpt_KPI_Report
(
	@CompanyCode CHAR(2),
	@StaffCode varchar(3),
	@CommMonth varchar(7),	-- Format 2011/12
	@RunBy varchar(10)
)
AS
BEGIN
	
	Select @CommMonth=Left(convert(varchar(7),convert(datetime,@CommMonth + '/01'),111),7)
	

	Declare @CommLevel varchar(5)

	Select @CommLevel=IsNull(CommLevel,1) from peostaff
	Where CompanyCode=@CompanyCode and StaffCode=@StaffCode

	Create Table #TmpBkg
	(
		CompanyCode varchar(2), BookingType varchar(1), MasterPnr varchar(10), Sales decimal(16,2), Cost decimal(16,2), CardCharge decimal(16,2)
	)

	IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_VIEW' and RightsCode='VIEW')
	BEGIN
		Insert into #TmpBkg
		Select mstr.CompanyCode, 'N' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax) as Sales, (mstr.TtlDocAmt + mstr.TtlDocTax) as Cost, convert(decimal(16,2),0) as CardCharge
		From peoMstr_CloseHistory mstr (nolock)
			Inner Join peomstr m (nolock)
			on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
		Where mstr.CompanyCode=@CompanyCode and m.StaffCode=@StaffCode 
			and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
			and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
			and (Select count(1) From peoMstr_CloseHistory 
				Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
				=0
		UNION
		Select mstr.CompanyCode, 'O' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax), (mstr.TtlDocAmt + mstr.TtlDocTax), 0
		From peoMstr_CloseHistory mstr (nolock)
			Inner Join peomstr m (nolock)
			on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
		Where mstr.CompanyCode=@CompanyCode and m.StaffCode=@StaffCode
			and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
			and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
			and (Select count(1) From peoMstr_CloseHistory 
				Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
				>0
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT LOGIN FROM ACCESS (nolock) WHERE CompanyCode=@CompanyCode and Login=@RunBy and ModuleCode='RPT_TEAM' and RightsCode='VIEW')
		BEGIN
			Insert into #TmpBkg
			Select mstr.CompanyCode, 'N' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax) as Sales, (mstr.TtlDocAmt + mstr.TtlDocTax) as Cost, convert(decimal(16,2),0) as CardCharge
			From peoMstr_CloseHistory mstr (nolock)
				Inner Join peomstr m (nolock)
				on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
			Where mstr.CompanyCode=@CompanyCode and m.StaffCode=@StaffCode 
				and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
				and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
				and (Select count(1) From peoMstr_CloseHistory (nolock)
					Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
					=0
				AND m.TeamCode in (Select TeamCode From Login (nolock) Where CompanyCode=@CompanyCode and Login=@RunBy)
			UNION
			Select mstr.CompanyCode, 'O' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax), (mstr.TtlDocAmt + mstr.TtlDocTax), 0
			From peoMstr_CloseHistory mstr (nolock)
				Inner Join peomstr m (nolock)
				on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
			Where mstr.CompanyCode=@CompanyCode and m.StaffCode=@StaffCode
				and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
				and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
				and (Select count(1) From peoMstr_CloseHistory (nolock)
					Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
					>0
				AND m.TeamCode in (Select TeamCode From Login (nolock) Where CompanyCode=@CompanyCode and Login=@RunBy)
		END
		ELSE
		BEGIN
			Insert into #TmpBkg
			Select mstr.CompanyCode, 'N' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax) as Sales, (mstr.TtlDocAmt + mstr.TtlDocTax) as Cost, convert(decimal(16,2),0) as CardCharge
			From peoMstr_CloseHistory mstr (nolock)
				Inner Join peomstr m (nolock)
				on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
			Where mstr.CompanyCode=@CompanyCode -- and m.StaffCode=@StaffCode 
				and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
				and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
				and (Select count(1) From peoMstr_CloseHistory (nolock)
					Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
					=0
				AND m.StaffCode=@RunBy
			UNION
			Select mstr.CompanyCode, 'O' as BookingType, mstr.MasterPnr, (mstr.TtlReptAmt + mstr.TtlReptTax), (mstr.TtlDocAmt + mstr.TtlDocTax), 0
			From peoMstr_CloseHistory mstr (nolock)
				Inner Join peomstr m (nolock)
				on mstr.CompanyCode=m.CompanyCode and mstr.MasterPnr=m.MasterPnr
			Where mstr.CompanyCode=@CompanyCode -- and m.StaffCode=@StaffCode
				and mstr.CreateOn=(Select Max(CreateOn) From peoMstr_CloseHistory Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111)=@CommMonth)
				and mstr.ActionType='C' and convert(varchar(7),mstr.CreateOn,111)=@CommMonth
				and (Select count(1) From peoMstr_CloseHistory (nolock)
					Where CompanyCode=mstr.CompanyCode and MasterPnr=mstr.MasterPnr and ActionType='C' and convert(varchar(7),CreateOn,111) < convert(varchar(7),mstr.CreateOn,111) )
					>0
				AND m.StaffCode=@RunBy
		END
	END
		
	Update #TmpBkg
	Set Sales=t.Sales - (Select top 1 (TtlReptAmt + TtlReptTax) From peoMstr_CloseHistory (nolock)
				Where CompanyCode=t.CompanyCode and MasterPnr=t.MasterPnr and convert(varchar(7),CreateOn,111) < @CommMonth
					and ActionType='C' 
				Order By CreateOn Desc),
		Cost=t.Cost - (Select top 1 (TtlDocAmt + TtlDocTax) From peoMstr_CloseHistory (nolock)
				Where CompanyCode=t.CompanyCode and MasterPnr=t.MasterPnr and convert(varchar(7),CreateOn,111) < @CommMonth
					and ActionType='C' 
				Order By CreateOn Desc)
	From #TmpBkg t
	Where BookingType='O'
	

	Select BookingType, Sum(Sales) as Sales, Sum(Cost) as Cost, Sum(CardCharge) as CardCharge,
		(Sum(Sales) - Sum(Cost)) as Profit, (Case When Sum(Sales)<>0 Then ((Sum(Sales) - Sum(Cost))/Sum(Sales)) * 100 Else 0 End) as CommGP,
		Count(MasterPnr) as BookCount, ( (Sum(Sales) - Sum(Cost)) / Count(MasterPnr) ) as BookAvg,
		(Select (Sum(Sales) - Sum(Cost)) From #TmpBkg) as TotalProfit
	Into #TmpBkgResult
	From #TmpBkg
	Group By BookingType

	Select BookingType, Sales, Cost, CardCharge, Profit as Profit, CommGP, BookCount, BookAvg, 
		(TotalProfit * IsNull((Select CommRate/100 From CommissionMstr 
					Where CompanyCode=@CompanyCode and convert(int,StaffLevel)=@CommLevel and r.TotalProfit between StartRange and EndRange),0)) as CommPaid
	From #TmpBkgResult r
END
