﻿



CREATE PROCEDURE dbo.Staff_Productivity_ByCust
@createonfrom char(30),
@createonto char(30),
@YTDcreateonfrom char(30),
@YTDcreateonto char(30),
@staffcodefrom varchar(10),
@staffcodeto varchar(10),
@companycode varchar(2)
 AS
BEGIN
select a.companycode,a.bkgref,a.staffcode,a.cltode as cltcode,
	(select isnull(sum(hkdamt),0) from peoinv where companycode=a.companycode and bkgref = a.bkgref  and createon >=@createonfrom and createon <=@createonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I') as bkg_sales,
	(select isnull(sum(hkdamt),0) from peoinv where companycode=a.companycode and bkgref = a.bkgref  and createon >=@createonfrom and createon <=@createonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='C') as bkg_CN,
	isnull((select sum(netfare+totaltax) from peotkt where companycode=a.companycode and bkgref = a.bkgref and createon between @createonfrom and @createonto and voidon is null),0) +	
	isnull((select sum(hkdamt) from peovch where companycode=a.companycode and bkgref = a.bkgref and createon between @createonfrom and @createonto and voidon is null),0)+
	isnull((select sum(hkdamt) from peoxo where companycode=a.companycode and bkgref = a.bkgref and createon between @createonfrom and @createonto and voidon is null ),0)+
	isnull((select sum(hkdamt) from peomco where companycode=a.companycode and bkgref = a.bkgref and  createon between @createonfrom and @createonto  and voidon is null ),0) as total_cost
into #SPR_cost
from peomstr a
where	a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
group by a.bkgref,a.companycode,a.staffcode,a.cltode
order by a.bkgref
select a.companycode,a.bkgref,a.staffcode,a.cltode as cltcode,
	(select isnull(sum(hkdamt),0) from peoinv where companycode=a.companycode and bkgref = a.bkgref  and createon >=@YTDcreateonfrom and createon <= @YTDcreateonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I') as YTD_bkg_sales,
	(select isnull(sum(hkdamt),0) from peoinv where companycode=a.companycode and bkgref = a.bkgref  and createon >=@YTDcreateonfrom and createon <= @YTDcreateonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='C') as YTD_bkg_CN,
	isnull((select sum(netfare+totaltax) from peotkt where companycode=a.companycode and bkgref = a.bkgref and createon between @YTDcreateonfrom and @YTDcreateonto and voidon is null),0) +	
	isnull((select sum(hkdamt) from peovch where companycode=a.companycode and bkgref = a.bkgref and createon between @YTDcreateonfrom and @YTDcreateonto and voidon is null),0)+
	isnull((select sum(hkdamt) from peoxo where companycode=a.companycode and bkgref = a.bkgref and createon between @YTDcreateonfrom and @YTDcreateonto and voidon is null ),0)+
	isnull((select sum(hkdamt) from peomco where companycode=a.companycode and bkgref = a.bkgref and  createon between @YTDcreateonfrom and @YTDcreateonto and voidon is null ),0) as YTD_total_cost
into #YTD_SPR_cost
from peomstr a
where	a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
group by a.bkgref,a.companycode,a.staffcode,a.cltode
order by a.bkgref
select a.companycode,a.bkgref,c.cltcode,
	isnull((select sum(hkdamt) from peoinv where bkgref = a.bkgref and cltcode = c.cltcode and createon >=@createonfrom and createon <=@createonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I'),0) as total_sales,
	isnull((select sum(hkdamt) from peoinv where bkgref = a.bkgref and cltcode = c.cltcode and createon >=@createonfrom and createon <=@createonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='C'),0) as total_CN,
	(select count(distinct cltcode) from peoinv where bkgref = a.bkgref and createon >=@createonfrom and createon <=@createonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I') as cltcount
into #SPR_sales
from peomstr a
inner join peoinv c on a.companycode = c.companycode and a.bkgref = c.bkgref
where	a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
group by a.companycode,a.bkgref,c.cltcode
order by a.bkgref,c.cltcode
select a.companycode,a.bkgref,c.cltcode,
	isnull((select sum(hkdamt) from peoinv where bkgref = a.bkgref and cltcode = c.cltcode and createon >=@YTDcreateonfrom and createon <=@YTDcreateonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I'),0) as YTD_total_sales,
	isnull((select sum(hkdamt) from peoinv where bkgref = a.bkgref and cltcode = c.cltcode and createon >=@YTDcreateonfrom and createon <=@YTDcreateonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='C'),0) as YTD_total_CN,
	(select count(distinct cltcode) from peoinv where bkgref = a.bkgref and createon >=@YTDcreateonfrom and createon <=@YTDcreateonto and voidon is null and ISNULL( RTRIM(VOID2JBA), '') = '' and substring(invnum,2,1)='I') as YTD_cltcount
into #YTD_SPR_sales
from peomstr a
inner join peoinv c on a.companycode = c.companycode and a.bkgref = c.bkgref
where	a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
group by a.companycode,a.bkgref,c.cltcode
order by a.bkgref,c.cltcode
select a.companycode,
	a.bkgref,
	a.staffcode,	
	a.teamcode,
	case when isnull(c.cltcode,'nil') = 'nil' then b.cltcode
	else c.cltcode end as cltcode,
	isnull(c.total_sales,0) as total_sales,
	isnull(c.total_CN,0) as total_CN,
	isnull(c.cltcount,0) as cltcount,
	isnull(b.bkg_sales,0) as bkg_sales,
	isnull(b.bkg_CN,0) as bkg_CN ,
	isnull(b.total_cost,0) as total_cost,
	(case when c.cltcode is not  null then
		(case when (b.bkg_sales) = 0 then
			case when c.cltcount=0 then 0 else (1.0/c.cltcount) end
		else (c.total_sales-c.total_cn)/(b.bkg_sales-b.bkg_cn) end)
	else 1 end) as cost_per
into #SPR
from peomstr a
inner join #spr_cost b on a.companycode = b.companycode and a.bkgref = b.bkgref
left join #spr_sales c on a.companycode = c.companycode and a.bkgref = c.bkgref
where a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
select a.companycode,
	a.bkgref,
	a.staffcode,	
	a.teamcode,
	case when isnull(c.cltcode,'nil') = 'nil' then b.cltcode
	else c.cltcode end as cltcode,
	isnull(c.YTD_total_sales,0) as YTD_total_sales,
	isnull(c.YTD_total_CN,0) as YTD_total_CN,
	isnull(c.YTD_cltcount,0) as YTD_cltcount,
	isnull(b.YTD_bkg_sales,0) as YTD_bkg_sales,
	isnull(b.YTD_bkg_CN,0) as YTD_bkg_CN ,
	isnull(b.YTD_total_cost,0) as YTD_total_cost,
	(case when c.cltcode is not  null then
		(case when (b.YTD_bkg_sales) = 0 then
			case when c.YTD_cltcount=0 then 0 else (1.0/c.YTD_cltcount) end
		else (c.YTD_total_sales-c.YTD_total_cn)/(b.YTD_bkg_sales-b.YTD_bkg_cn) end)
	else 1 end) as YTD_cost_per
into #YTD_SPR
from peomstr a
inner join #YTD_spr_cost b on a.companycode = b.companycode and a.bkgref = b.bkgref
left join #YTD_spr_sales c on a.companycode = c.companycode and a.bkgref = c.bkgref
where a.companycode=@companycode and a.staffcode in (select distinct staffcode from peostaff where companycode=@companycode)
select	e.companycode,d.staffcode,d.cltcode, 
	(select isnull(a.cltname,'') from customer a where a.cltcode=d.cltcode) as cltname,
	isnull(sum(d.total_sales-d.total_CN),0) as total_sales,
	isnull(sum(d.total_cost*d.cost_per),0) as total_cost,
	isnull((sum(d.total_sales-d.total_CN)-sum(d.total_cost*d.cost_per)),0) as margin,
	case when isnull(sum(d.total_sales-d.total_CN),0) = 0 then 0
	 else (sum(d.total_sales-d.total_CN)-sum(d.total_cost*d.cost_per))/sum(d.total_sales-d.total_CN) end as yield,
	(select sum(a.total_sales-a.total_CN) from #SPR a where a.staffcode = d.staffcode) as T_total_sales,
	(select sum(a.total_cost*a.cost_per) from #SPR a where a.staffcode = d.staffcode and a.cltcode is not null) as T_total_cost,
	(select sum(a.total_sales-a.total_CN)-sum(a.total_cost*a.cost_per) from #SPR a where a.staffcode = d.staffcode) as T_margin,
	case when (select isnull(sum(a.total_sales-a.total_CN),0) from #SPR a where a.staffcode = d.staffcode)  = 0 then 0
	 else (select (sum(a.total_sales-a.total_CN)-sum(a.total_cost*a.cost_per))/sum(a.total_sales-a.total_CN) from #SPR a where a.staffcode = d.staffcode) end as T_yield,
	isnull(sum(e.YTD_total_sales-e.YTD_total_CN),0) as YTD_total_sales,
	isnull(sum(e.YTD_total_cost*e.YTD_cost_per),0) as YTD_total_cost,
	(isnull(sum(e.YTD_total_sales-e.YTD_total_CN),0)-isnull(sum(e.YTD_total_cost*e.YTD_cost_per),0)) as YTD_margin,
	case when isnull(sum(e.YTD_total_sales-e.YTD_total_CN),0) = 0 then 0
	 else (sum(e.YTD_total_sales-e.YTD_total_CN)-sum(e.YTD_total_cost*e.YTD_cost_per))/sum(e.YTD_total_sales-e.YTD_total_CN) end as YTD_yield,
	(select sum(a.YTD_total_sales-a.YTD_total_CN) from #YTD_SPR a where a.staffcode =d.staffcode) as YTD_T_total_sales,
	(select sum(a.YTD_total_cost*a.YTD_cost_per) from #YTD_SPR a where a.staffcode  =d.staffcode and a.cltcode is not null) as YTD_T_total_cost,
	(select sum(a.YTD_total_sales-a.YTD_total_CN)-sum(a.YTD_total_cost*a.YTD_cost_per) from #YTD_SPR a where a.staffcode =d.staffcode) as YTD_T_margin,
	case when (select isnull(sum(a.YTD_total_sales-a.YTD_total_CN),0) from #YTD_SPR a where a.staffcode  =d.staffcode)  = 0 then 0
	 else (select (sum(a.YTD_total_sales-a.YTD_total_CN)-sum(a.YTD_total_cost*a.YTD_cost_per))/sum(a.YTD_total_sales-a.YTD_total_CN) from #YTD_SPR a where a.staffcode=d.staffcode) end as YTD_T_yield
from #YTD_SPR e
left outer join #spr d on e.companycode = d.companycode and e.bkgref=d.bkgref and e.staffcode = d.staffcode and e.teamcode = d.teamcode and e.cltcode=d.cltcode
where
e.staffcode between @staffcodefrom and @staffcodeto
group by e.companycode,d.staffcode,d.cltcode
order by d.staffcode,d.cltcode
END




