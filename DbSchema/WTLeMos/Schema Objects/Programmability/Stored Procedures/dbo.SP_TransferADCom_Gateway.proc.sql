﻿


CREATE PROCEDURE	SP_TransferADCom_Gateway
AS
BEGIN


DECLARE		@CompanyCode	varchar(2),
		@InputType	char(5),
		@TripNo		char(10),
		@BkgRef		char(10),
		@Login		char(3),
		@CurrCode	char(3)

------- Declare For PeoPax ------------------------------
DECLARE		@Peopax_BkgRef varchar(10), @Peopax_CompanyCode varchar(2), @Peopax_PaxLName nvarchar(20), @Peopax_PaxFName nvarchar(40), 
		@Peopax_PaxTitle nvarchar(4), @Peopax_PaxAir nvarchar(3), @Peopax_PaxHotel nvarchar(3), @Peopax_PaxOther nvarchar(3), 
		@Peopax_PaxAge int, @Peopax_Rmk1 nvarchar(20), @Peopax_Rmk2 nvarchar(20), @Peopax_SourceSystem nvarchar(10), 
		@Peopax_Nationality nvarchar(30), @Peopax_PassportNo nvarchar(30), 
		@Peopax_PassportExpireDate datetime, @Peopax_DateOfBirth datetime, @Peopax_Visa nvarchar(1), @Peopax_ContactHomeTel nvarchar(20), 
		@Peopax_ContactMobileTel nvarchar(20), @Peopax_ContactOfficeTel nvarchar(20), @Peopax_Email nvarchar(70), 
		@Peopax_Address1 nvarchar(50), @Peopax_Address2 nvarchar(50), @Peopax_Address3 nvarchar(50), @Peopax_Address4 nvarchar(50), 
		@Peopax_Source nvarchar(30), @Peopax_SourceRef nvarchar(12), @Peopax_MemberShipNo nvarchar(16), @Peopax_Deleted nvarchar(1), 
		@Peopax_eVisa_Import nvarchar(1), @Peopax_eVisa_Import_DateTime datetime, @Peopax_SourceType nvarchar(1), @Peopax_ADComTxNo nvarchar(12),
		@Peopax_SegNum nvarchar(5)

------- Declare For PeoHtl ------------------------------
DECLARE		@PeoHtl_CompanyCode nvarchar(2), @PeoHtl_BkgRef nvarchar(10), @PeoHtl_SegNum nvarchar(5), @PeoHtl_CountryCode nvarchar(3), 
		@PeoHtl_CityCode nvarchar(3), @PeoHtl_ADComTxNo nvarchar(12), @PeoHtl_ADComSeqNum nvarchar(5), @PeoHtl_HotelCode nvarchar(8), 
		@PeoHtl_ArrDate datetime, @PeoHtl_ArrTime nvarchar(6), @PeoHtl_DepartDate datetime, @PeoHtl_DepartTime nvarchar(6), 
		@PeoHtl_ETA nvarchar(14), @PeoHtl_ETD nvarchar(14), @PeoHtl_SellCurr nvarchar(3), @PeoHtl_CostCurr nvarchar(3), 
		@PeoHtl_TotalCost decimal, @PeoHtl_TotalSell decimal, @PeoHtl_TotalCTax decimal, @PeoHtl_TotalSTax decimal, @PeoHtl_FormPay nvarchar(50), 
		@PeoHtl_Rmk1 nvarchar(50), @PeoHtl_Rmk2 nvarchar(50), @PeoHtl_InvRmk1 nvarchar(50), @PeoHtl_InvRmk2 nvarchar(50), 
		@PeoHtl_VchRmk1 nvarchar(80), @PeoHtl_VchRmk2 nvarchar(80), @PeoHtl_WestelNo nvarchar(10), @PeoHtl_SourceSystem nvarchar(10), 
		@PeoHtl_SPCL_Request nvarchar(255), @PeoHtl_HtlReference nvarchar(20), @PeoHtl_HtlCfmBy nvarchar(30), 
		@PeoHtl_VoidBy nvarchar(10), @PeoHtl_VoidOn datetime, @PeoHtl_SourceRef nvarchar(12), @PeoHtl_GstTax nchar(1), 
		@PeoHtl_ShowDetailFee nchar(1), @PeoHtl_SuppCode nvarchar(8), @PeoHtl_HotelVoucherType nchar(1), @PeoHtl_RoomTypeCode nchar(1), 
		@PeoHtl_FullRoomRate decimal, @PeoHtl_DailyRate decimal, @PeoHtl_LowRoomRate decimal, 
		@PeoHtl_RateTypeCode nchar(1), @PeoHtl_COD1 nvarchar(15), @PeoHtl_COD2 nvarchar(15), @PeoHtl_COD3 nvarchar(15), 
		@PeoHtl_IsRecalable nvarchar(2), @PeoHtl_ReasonCode nvarchar(2), @PeoHtl_FromAD nvarchar(1)


------- Declare For PeoHtl ------------------------------
DECLARE		@PeoHtlDetail_CompanyCode nvarchar(2), @PeoHtlDetail_BkgRef nvarchar(10), @PeoHtlDetail_SeqType nvarchar(5), 
		@PeoHtlDetail_SegNum nvarchar(5), @PeoHtlDetail_SeqNum nvarchar(5), @PeoHtlDetail_Qty int, @PeoHtlDetail_ArrDate datetime, 
		@PeoHtlDetail_DepartDate datetime, @PeoHtlDetail_Rmnts decimal, @PeoHtlDetail_RateName nvarchar(40), @PeoHtlDetail_ItemName nvarchar(1000), 
		@PeoHtlDetail_Status nvarchar(2), @PeoHtlDetail_Curr nvarchar(3), @PeoHtlDetail_Amt decimal, @PeoHtlDetail_TotalAmt decimal, 
		@PeoHtlDetail_TaxCurr nvarchar(3), @PeoHtlDetail_TaxAmt decimal, @PeoHtlDetail_ItemType nchar(1), @PeoHtlDetail_AmtFee decimal



------ GET ALL OLD BOOKING ON WMEMOS ---------------------------------
DECLARE 	GW_CURSOR CURSOR FOR
			Select 	M.CompanyCode, M.BkgRef, H.ADComTxNo, M.CreateBy 
			From 	PeoMstr M 
					Inner Join PeoHtl H On M.CompanyCode=H.CompanyCode And M.BkgRef=H.BkgRef 
			Where 	IsNull(H.ADComTxNo, '') <> '' And H.CreateOn Between '2006-04-01' and '2006-06-05 23:59:59.999'
			Group By 	M.CompanyCode, M.BkgRef, H.ADComTxNo, M.CreateBy
			Order By 	M.CompanyCode, M.BkgRef, H.ADComTxNo

OPEN GW_CURSOR
FETCH GW_CURSOR INTO	@CompanyCode, @BkgRef, @TripNo, @Login

WHILE @@FETCH_STATUS=0
BEGIN
	Declare	@SegCount 	int
	Declare	@SeqCount	int
	
	--Select @InputType = 'CHECK'
	--Select @CurrCode = 'HKD'
	---===================================================================
	--------------- Transfer AD Hotel Booking into WMeMos4Tsfr -----------------------
	--	exec WMeMos4Tsfr.dbo.SP_peohotel_to_Tsfr_GW @CompanyCode, @InputType, @TripNo, @BkgRef, @Login, @CurrCode

	SELECT '=================== BEGIN ======================='
	SELECT 'TRAN  START  Company: ' + @CompanyCode + '       BkgRef: ' + @BkgRef + '       ADNo: ' + @TripNo + '  '

	---===================================================================
	--------------- Transfer Pax ------------------------------------------------------------------
	SELECT 'TRAN  PAX'
	-------------- Get Existing Pax on WMeMos
	Select 	IsNull(BkgRef,'') AS BkgRef, IsNull(CompanyCode, '') as CompanyCode, IsNull(PaxLName, '') as PaxLName, 
		IsNull(PaxFName, '') as PaxFName, IsNull(PaxTitle, '') as PaxTitle, IsNull(PaxAir, '') as PaxAir, 
		IsNull(PaxHotel,'') as PaxHotel, IsNull(PaxOther,'') as PaxOther, IsNull(PaxAge, 0) as PaxAge,
		IsNull(Rmk1, '') as Rmk1, IsNull(Rmk2, '') as Rmk2, IsNull(SourceSystem, '') as SourceSystem, 
		IsNull(Nationality, '') as Nationality, IsNull(PassportNo, '') as PassportNo,
		IsNull(PassportExpireDate, convert(datetime, '1900-01-01')) as PassportExpireDate, IsNull(DateOfBirth, convert(datetime, '1900-01-01')) as DateOfBirth, 
		IsNull(Visa, '') as Visa,	IsNull(ContactHomeTel, '') as ContactHomeTel, IsNull(ContactMobileTel, '') as ContactMobileTel, IsNull(ContactOfficeTel, '') as ContactOfficeTel,
		IsNull(Email, '') as Email, IsNull(Address1, '') as Address1, IsNull(Address2, '') as Address2, IsNull(Address3, '') as Address3, IsNull(Address4, '') as Address4,
		IsNull(Source, '') as Source, IsNull(SourceRef, '') as SourceRef, IsNull(MemberShipNo, '') as MemberShipNo, IsNull(deleted, '0') as deleted,
		IsNull(evisa_import, '') as evisa_import, IsNull(evisa_import_datetime, convert(datetime, '1900-01-01')) as evisa_import_datetime, IsNull(SourceType, '') as SourceType, 
		IsNull(ADComTxNo, '') as ADComTxNo, IsNull(SegNum, '') as SegNum
	Into	#Tmp_OldPax
	From	PeoPax
	Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef And IsNull(SourceType, '') <> 'P'
	Order By	SegNum

	-- *********************************************************
	BEGIN TRAN
	-- *********************************************************
	-------------- Delete All Existing Pax on WMeMos
	Delete From PeoPax
	Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
	IF @@ERROR<>0 GOTO ERRHANDLE

	-------------- Insert All Pax on WMeMos
	Declare 	PAX_CURSOR CURSOR FOR
			Select 	BkgRef, CompanyCode, PaxLName, PaxFName, PaxTitle, PaxAir, PaxHotel, PaxOther, PaxAge, Rmk1, Rmk2, SourceSystem, 
				Nationality, PassportNo, PassportExpireDate, DateOfBirth, Visa, ContactHomeTel, ContactMobileTel, 
				ContactOfficeTel, Email, Address1, Address2, Address3, Address4, Source, SourceRef, MemberShipNo, Deleted, eVisa_Import, 
				eVisa_Import_DateTime, SourceType, ADComTxNo, SegNum
			From 	#Tmp_OldPax	
			Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
			UNION
			Select 	BkgRef, CompanyCode, PaxLName, PaxFName, PaxTitle, PaxAir, PaxHotel, PaxOther, PaxAge, Rmk1, Rmk2, SourceSystem, 
				Nationality, PassportNo, PassportExpireDate, DateOfBirth, Visa, ContactHomeTel, ContactMobileTel, 
				ContactOfficeTel, Email, Address1, Address2, Address3, Address4, Source, SourceRef, MemberShipNo, Deleted, eVisa_Import, 
				eVisa_Import_DateTime, 'P' as SourceType, @TripNo, SegNum
			From 	WMeMos4Tsfr.dbo.PeoPax
			Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
			Order By 	SourceType, SegNum
			
	OPEN PAX_CURSOR
	FETCH PAX_CURSOR INTO	@Peopax_BkgRef, @Peopax_CompanyCode, @Peopax_PaxLName, @Peopax_PaxFName, @Peopax_PaxTitle, @Peopax_PaxAir, 
				@Peopax_PaxHotel, @Peopax_PaxOther, @Peopax_PaxAge, @Peopax_Rmk1, @Peopax_Rmk2, @Peopax_SourceSystem, 
				@Peopax_Nationality, @Peopax_PassportNo, @Peopax_PassportExpireDate, @Peopax_DateOfBirth, 
				@Peopax_Visa, @Peopax_ContactHomeTel, @Peopax_ContactMobileTel, @Peopax_ContactOfficeTel, 
				@Peopax_Email, @Peopax_Address1, @Peopax_Address2, @Peopax_Address3, @Peopax_Address4, @Peopax_Source, 
				@Peopax_SourceRef, @Peopax_MemberShipNo, @Peopax_Deleted, @Peopax_eVisa_Import, @Peopax_eVisa_Import_DateTime, 
				@Peopax_SourceType, @Peopax_ADComTxNo, @Peopax_SegNum

	Select	@SegCount = 1
	WHILE @@FETCH_STATUS=0
	BEGIN
		Insert Into 	PeoPax
		(BkgRef, CompanyCode, PaxLName, PaxFName, PaxTitle, PaxAir, PaxHotel, PaxOther, PaxAge, Rmk1, Rmk2, SourceSystem, 
			Nationality, PassportNo, PassportExpireDate, DateOfBirth, Visa, ContactHomeTel, ContactMobileTel, 
			ContactOfficeTel, Email, Address1, Address2, Address3, Address4, Source, SourceRef, MemberShipNo, Deleted, eVisa_Import, 
			eVisa_Import_DateTime, SourceType, ADComTxNo, CreateOn, CreateBy, UpdateOn, UpdateBy, SegNum)
		Select 	@Peopax_BkgRef, @Peopax_CompanyCode, @Peopax_PaxLName, @Peopax_PaxFName, @Peopax_PaxTitle, @Peopax_PaxAir, 
			@Peopax_PaxHotel, @Peopax_PaxOther, @Peopax_PaxAge, @Peopax_Rmk1, @Peopax_Rmk2, @Peopax_SourceSystem, 
			@Peopax_Nationality, @Peopax_PassportNo, @Peopax_PassportExpireDate, @Peopax_DateOfBirth, 
			@Peopax_Visa, @Peopax_ContactHomeTel, @Peopax_ContactMobileTel, @Peopax_ContactOfficeTel, 
			@Peopax_Email, @Peopax_Address1, @Peopax_Address2, @Peopax_Address3, @Peopax_Address4, @Peopax_Source, 
			@Peopax_SourceRef, @Peopax_MemberShipNo, @Peopax_Deleted, @Peopax_eVisa_Import, @Peopax_eVisa_Import_DateTime, 
			@Peopax_SourceType, @Peopax_ADComTxNo, GetDate(), @Login, GetDate(), @Login, Right('00000' + rtrim(convert(varchar, @SegCount)), 5)
	
		IF @@ERROR<>0 GOTO ERRHANDLE

		Select	@SegCount = @SegCount + 1
		
		--------------- Move Next Pax -----------
		FETCH PAX_CURSOR INTO	@Peopax_BkgRef, @Peopax_CompanyCode, @Peopax_PaxLName, @Peopax_PaxFName, @Peopax_PaxTitle, @Peopax_PaxAir, 
				@Peopax_PaxHotel, @Peopax_PaxOther, @Peopax_PaxAge, @Peopax_Rmk1, @Peopax_Rmk2, @Peopax_SourceSystem, 
				@Peopax_Nationality, @Peopax_PassportNo, @Peopax_PassportExpireDate, @Peopax_DateOfBirth, 
				@Peopax_Visa, @Peopax_ContactHomeTel, @Peopax_ContactMobileTel, @Peopax_ContactOfficeTel, 
				@Peopax_Email, @Peopax_Address1, @Peopax_Address2, @Peopax_Address3, @Peopax_Address4, @Peopax_Source, 
				@Peopax_SourceRef, @Peopax_MemberShipNo, @Peopax_Deleted, @Peopax_eVisa_Import, @Peopax_eVisa_Import_DateTime, 
				@Peopax_SourceType, @Peopax_ADComTxNo, @Peopax_SegNum
	END
	CLOSE PAX_CURSOR
	DEALLOCATE PAX_CURSOR

	Drop Table #Tmp_OldPax
	---===================================================================
	
	---===================================================================
	--------------- Transfer HOTEL ------------------------------------------------------------------
	SELECT 'TRAN  HOTEL'
	-------------- Get Existing Hotel on WMeMos
	Select	IsNull(CompanyCode, '') as CompanyCode, IsNull(BkgRef, '') as BkgRef, IsNull(SegNum, '') as SegNum, IsNull(CountryCode, '') as CountryCode, 
		IsNull(CityCode,'') as CityCode, IsNull(ADComTxNo, '') as ADComTxNo, IsNull(ADComSeqNum, '') as ADComSeqNum, IsNull(HotelCode,'') as HotelCode,
		IsNull(ArrDate, convert(datetime, '1900-01-01')) as ArrDate, IsNull(ArrTime, '') as ArrTime, IsNull(DepartDate, convert(datetime, '1900-01-01')) as DepartDate,
		IsNull(DepartTime, '') as DepartTime, IsNull(ETA, '') as ETA, IsNull(ETD, '') as ETD, IsNull(SellCurr, '') as SellCurr, IsNull(CostCurr, '') as CostCurr, 
		IsNull(TotalCost, 0) as TotalCost, IsNull(TotalSell, 0) as TotalSell, IsNull(TotalCTax, 0) as TotalCTax, IsNull(TotalSTax, 0) as TotalSTax, 
		IsNull(FormPay, '') as FormPay, IsNull(Rmk1, '') as Rmk1, IsNull(Rmk2, '') as Rmk2, IsNull(InvRmk1, '') as InvRmk1, IsNull(InvRmk2, '') as InvRmk2,
		IsNull(VchRmk1, '') as VchRmk1, IsNull(VchRmk2, '') as VchRmk2, IsNull(WestelNo, '') as WestelNo, IsNull(SourceSystem, '') as SourceSystem,
		IsNull(SPCL_Request, '') as SPCL_Request, IsNull(HtlReference, '') as HtlReference, IsNull(HtlCfmBy, '') as HtlCfmBy, 
		IsNull(VoidBy, '') as VoidBy, IsNull(VoidOn, convert(datetime, '1900-01-01')) as VoidOn, 
		IsNull(SourceRef, '') as SourceRef, IsNull(GstTax, 'N') as GstTax, IsNull(ShowDetailFee, '') as ShowDetailFee, IsNull(SuppCode, '') as SuppCode,
		IsNull(HotelVoucherType, '') as HotelVoucherType, IsNull(RoomTypeCode, '') as RoomTypeCode, IsNull(FullRoomRate, 0) as FullRoomRate,
		IsNull(DailyRate, 0) as DailyRate, IsNull(LowRoomRate, 0) as LowRoomRate, IsNull(RateTypeCode, '') as RateTypeCode,
		IsNull(COD1, '') as COD1, IsNull(COD2, '') as COD2, IsNull(COD3, '') as COD3, IsNull(IsRecalable, '') as IsRecalable, IsNull(ReasonCode, '') as ReasonCode
	Into	#Tmp_OldHtl
	From	PeoHtl
	Where 	CompanyCode=@CompanyCode And BkgRef=@BkgRef And IsNull(SourceSystem, '') <> 'PEONYHOTEL'
	Order By	SegNum

	-------------- Get Existing Hotel Detail on WMeMos
	Select	IsNull(D.CompanyCode, '') as CompanyCode, IsNull(D.BkgRef, '') as BkgRef, IsNull(D.SeqType, '') as SeqType, IsNull(D.SegNum, '') as SegNum,
		IsNull(D.SeqNum, '') as SeqNum, IsNull(D.Qty, 0) as Qty, IsNull(D.ArrDate, convert(datetime, '1900-01-01')) as ArrDate, 
		IsNull(D.DepartDate, convert(datetime, '1900-01-01')) as DepartDate, IsNull(D.Rmnts, 0) as Rmnts, IsNull(D.RateName, '') as RateName,
		IsNull(D.ItemName, '') as ItemName, IsNull(D.Status, '') as Status, IsNull(D.Curr, '') as Curr, IsNull(D.Amt, 0) as Amt, IsNull(D.TotalAmt, 0) as TotalAmt,
		IsNull(D.TaxCurr, '') as TaxCurr, IsNull(D.TaxAmt, 0) as TaxAmt, IsNull(D.ItemType, '') as ItemType, IsNull(D.AmtFee, 0) as AmtFee
	Into 	#Tmp_OldHtlDetail
	From	PeoHtl H Inner Join PeoHtlDetail D On H.CompanyCode=D.CompanyCode And H.BkgRef=D.BkgRef And H.SegNum=D.SegNum
	Where 	H.CompanyCode=@CompanyCode And H.BkgRef=@BkgRef And IsNull(H.SourceSystem, '') <> 'PEONYHOTEL'
	Order By	D.SegNum, D.SeqNum
	
	-------------- Delete All Existing Hotel on WMeMos
	Delete From PeoHtlDetail
	Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
	IF @@ERROR<>0 GOTO ERRHANDLE

	Delete From PeoHtl
	Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
	IF @@ERROR<>0 GOTO ERRHANDLE

	-------------- Insert All Hotel on WMeMos
	Declare 	HOTEL_CURSOR CURSOR FOR
			Select 	CompanyCode, BkgRef, SegNum, CountryCode, CityCode, ADComTxNo, ADComSeqNum, HotelCode, ArrDate,
				ArrTime, DepartDate, DepartTime, ETA, ETD, SellCurr, CostCurr, TotalCost, TotalSell, TotalCTax, TotalSTax, FormPay, 
				Rmk1, Rmk2, InvRmk1, InvRmk2, VchRmk1, VchRmk2, WestelNo, SourceSystem, SPCL_Request, HtlReference, HtlCfmBy,
				VoidBy, VoidOn, SourceRef, GstTax, ShowDetailFee, SuppCode, HotelVoucherType, RoomTypeCode, FullRoomRate, 
				DailyRate, LowRoomRate, RateTypeCode, COD1, COD2, COD3, IsRecalable, ReasonCode, 'N' as FromAD
			From 	#Tmp_OldHtl	
			Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
			UNION
			Select 	CompanyCode, BkgRef, SegNum, CountryCode, CityCode, ADComTxNo, ADComSeqNum, HotelCode, ArrDate,
				ArrTime, DepartDate, DepartTime, ETA, ETD, SellCurr, CostCurr, TotalCost, TotalSell, TotalCTax, TotalSTax, FormPay, 
				Rmk1, Rmk2, InvRmk1, InvRmk2, VchRmk1, VchRmk2, WestelNo, SourceSystem, SPCL_Request, HtlReference, HtlCfmBy,
				VoidBy, VoidOn, SourceRef, GstTax, ShowDetailFee, SuppCode, HotelVoucherType, RoomTypeCode, FullRoomRate, 
				DailyRate, LowRoomRate, RateTypeCode, COD1, COD2, COD3, IsRecalable, ReasonCode, 'Y' as FromAD
			From 	WMeMos4Tsfr.dbo.PeoHtl
			Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef
			Order by	SourceSystem, SegNum
			
	OPEN HOTEL_CURSOR
	FETCH HOTEL_CURSOR INTO	@PeoHtl_CompanyCode, @PeoHtl_BkgRef, @PeoHtl_SegNum, @PeoHtl_CountryCode, @PeoHtl_CityCode, @PeoHtl_ADComTxNo, 
				@PeoHtl_ADComSeqNum, @PeoHtl_HotelCode, @PeoHtl_ArrDate, @PeoHtl_ArrTime, @PeoHtl_DepartDate, @PeoHtl_DepartTime, 
				@PeoHtl_ETA, @PeoHtl_ETD, @PeoHtl_SellCurr, @PeoHtl_CostCurr, @PeoHtl_TotalCost, @PeoHtl_TotalSell, @PeoHtl_TotalCTax, 
				@PeoHtl_TotalSTax, @PeoHtl_FormPay, @PeoHtl_Rmk1, @PeoHtl_Rmk2, @PeoHtl_InvRmk1, @PeoHtl_InvRmk2, @PeoHtl_VchRmk1, 
				@PeoHtl_VchRmk2, @PeoHtl_WestelNo, @PeoHtl_SourceSystem, @PeoHtl_SPCL_Request, @PeoHtl_HtlReference, @PeoHtl_HtlCfmBy,
				@PeoHtl_VoidBy, @PeoHtl_VoidOn, @PeoHtl_SourceRef, @PeoHtl_GstTax, @PeoHtl_ShowDetailFee, @PeoHtl_SuppCode, 
				@PeoHtl_HotelVoucherType, @PeoHtl_RoomTypeCode, @PeoHtl_FullRoomRate, @PeoHtl_DailyRate, @PeoHtl_LowRoomRate, 
				@PeoHtl_RateTypeCode, @PeoHtl_COD1, @PeoHtl_COD2, @PeoHtl_COD3, @PeoHtl_IsRecalable, @PeoHtl_ReasonCode, @PeoHtl_FromAD

	Select	@SegCount = 1
	WHILE @@FETCH_STATUS=0
	BEGIN
		Insert Into PeoHtl
		(CompanyCode, BkgRef, CountryCode, CityCode, ADComTxNo, ADComSeqNum, HotelCode, ArrDate,
			ArrTime, DepartDate, DepartTime, ETA, ETD, SellCurr, CostCurr, TotalCost, TotalSell, TotalCTax, TotalSTax, FormPay, 
			Rmk1, Rmk2, InvRmk1, InvRmk2, VchRmk1, VchRmk2, WestelNo, SourceSystem, SPCL_Request, HtlReference, HtlCfmBy,
			VoidBy, VoidOn, SourceRef, GstTax, ShowDetailFee, SuppCode, HotelVoucherType, RoomTypeCode, FullRoomRate, 
			DailyRate, LowRoomRate, RateTypeCode, COD1, COD2, COD3, IsRecalable, ReasonCode, CreateOn, CreateBy, UpdateOn, UpdateBy, SegNum)
		Select	@PeoHtl_CompanyCode, @PeoHtl_BkgRef, @PeoHtl_CountryCode, @PeoHtl_CityCode, @PeoHtl_ADComTxNo, 
			@PeoHtl_ADComSeqNum, @PeoHtl_HotelCode, @PeoHtl_ArrDate, @PeoHtl_ArrTime, @PeoHtl_DepartDate, @PeoHtl_DepartTime, 
			@PeoHtl_ETA, @PeoHtl_ETD, @PeoHtl_SellCurr, @PeoHtl_CostCurr, @PeoHtl_TotalCost, @PeoHtl_TotalSell, @PeoHtl_TotalCTax, 
			@PeoHtl_TotalSTax, @PeoHtl_FormPay, @PeoHtl_Rmk1, @PeoHtl_Rmk2, @PeoHtl_InvRmk1, @PeoHtl_InvRmk2, @PeoHtl_VchRmk1, 
			@PeoHtl_VchRmk2, @PeoHtl_WestelNo, @PeoHtl_SourceSystem, @PeoHtl_SPCL_Request, @PeoHtl_HtlReference, @PeoHtl_HtlCfmBy,
			@PeoHtl_VoidBy, @PeoHtl_VoidOn, @PeoHtl_SourceRef, @PeoHtl_GstTax, @PeoHtl_ShowDetailFee, @PeoHtl_SuppCode, 
			@PeoHtl_HotelVoucherType, @PeoHtl_RoomTypeCode, @PeoHtl_FullRoomRate, @PeoHtl_DailyRate, @PeoHtl_LowRoomRate, 
			@PeoHtl_RateTypeCode, @PeoHtl_COD1, @PeoHtl_COD2, @PeoHtl_COD3, @PeoHtl_IsRecalable, @PeoHtl_ReasonCode, 
			GetDate(), @Login, GetDate(), @Login, Right('00000' + rtrim(convert(varchar, @SegCount)), 5)
	
		IF @@ERROR<>0 GOTO ERRHANDLE
	
		IF (@PeoHtl_FromAD = 'Y')
		BEGIN
			Declare 	HOTELDETAIL_AD_CURSOR CURSOR FOR
					Select	CompanyCode, BkgRef, SeqType, SegNum, SeqNum, Qty, ArrDate, DepartDate, Rmnts, RateName,
						ItemName, Status, Curr, Amt, TotalAmt, TaxCurr, TaxAmt, ItemType, AmtFee
					From	WMeMos4Tsfr.dbo.PeoHtlDetail
					Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef And SegNum=@PeoHtl_SegNum
					Order By	SeqNum
			OPEN HOTELDETAIL_AD_CURSOR
			FETCH HOTELDETAIL_AD_CURSOR INTO	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, @PeoHtlDetail_SegNum, 
								@PeoHtlDetail_SeqNum, @PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
								@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
								@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
								@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee

			Select	@SeqCount = 1
			WHILE @@FETCH_STATUS=0
			BEGIN
				Insert Into PeoHtlDetail
				(CompanyCode, BkgRef, SeqType, Qty, ArrDate, DepartDate, Rmnts, RateName,
					ItemName, Status, Curr, Amt, TotalAmt, TaxCurr, TaxAmt, ItemType, AmtFee, SegNum, SeqNum)
				Select	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, 
					@PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
					@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
					@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
					@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee, 
					Right('00000' + rtrim(convert(varchar, @SegCount)), 5),Right('00000' + rtrim(convert(varchar, @SeqCount)), 5)

				IF @@ERROR<>0 GOTO ERRHANDLE

				Select	@SeqCount = @SeqCount + 1 
				--------------- Move Next Hotel Detail -----------
				FETCH HOTELDETAIL_AD_CURSOR INTO	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, @PeoHtlDetail_SegNum, 
									@PeoHtlDetail_SeqNum, @PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
									@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
									@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
									@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee
			END
			CLOSE HOTELDETAIL_AD_CURSOR
			DEALLOCATE HOTELDETAIL_AD_CURSOR
		END
		ELSE
		BEGIN
			Declare 	HOTELDETAIL_CURSOR CURSOR FOR
					Select	CompanyCode, BkgRef, SeqType, SegNum, SeqNum, Qty, ArrDate, DepartDate, Rmnts, RateName,
						ItemName, Status, Curr, Amt, TotalAmt, TaxCurr, TaxAmt, ItemType, AmtFee
					From	#Tmp_OldHtlDetail
					Where	CompanyCode=@CompanyCode And BkgRef=@BkgRef And SegNum=@PeoHtl_SegNum
					Order By	SeqNum
			OPEN HOTELDETAIL_CURSOR
			FETCH HOTELDETAIL_CURSOR INTO	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, @PeoHtlDetail_SegNum, 
							@PeoHtlDetail_SeqNum, @PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
							@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
							@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
							@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee

			Select	@SeqCount = 1
			WHILE @@FETCH_STATUS=0
			BEGIN
				Insert Into PeoHtlDetail
				(CompanyCode, BkgRef, SeqType, Qty, ArrDate, DepartDate, Rmnts, RateName,
					ItemName, Status, Curr, Amt, TotalAmt, TaxCurr, TaxAmt, ItemType, AmtFee, SegNum, SeqNum)
				Select	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, 
					@PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
					@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
					@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
					@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee, 
					Right('00000' + rtrim(convert(varchar, @SegCount)), 5),Right('00000' + rtrim(convert(varchar, @SeqCount)), 5)

				IF @@ERROR<>0 GOTO ERRHANDLE

				Select	@SeqCount = @SeqCount + 1 
				--------------- Move Next Hotel Detail -----------
				FETCH HOTELDETAIL_CURSOR INTO	@PeoHtlDetail_CompanyCode, @PeoHtlDetail_BkgRef, @PeoHtlDetail_SeqType, @PeoHtlDetail_SegNum, 
								@PeoHtlDetail_SeqNum, @PeoHtlDetail_Qty, @PeoHtlDetail_ArrDate, @PeoHtlDetail_DepartDate, 
								@PeoHtlDetail_Rmnts, @PeoHtlDetail_RateName, @PeoHtlDetail_ItemName, @PeoHtlDetail_Status, 
								@PeoHtlDetail_Curr, @PeoHtlDetail_Amt, @PeoHtlDetail_TotalAmt, @PeoHtlDetail_TaxCurr, 
								@PeoHtlDetail_TaxAmt, @PeoHtlDetail_ItemType, @PeoHtlDetail_AmtFee
			END
			CLOSE HOTELDETAIL_CURSOR
			DEALLOCATE HOTELDETAIL_CURSOR
		END

		---===================================================================
		--------------- Recalculate Hotel Segment  ------------------------------------------------------------------
		Declare @SegStr varchar(5)

		Select @SegStr = Right('00000' + rtrim(convert(varchar, @SegCount)), 5)
		exec SP_Recal_HOTEL @BkgRef, @CompanyCode, @SegStr
		IF @@ERROR<>0 GOTO ERRHANDLE

		---===================================================================
		Select	@SegCount = @SegCount + 1

		--------------- Move Next Hotel -----------
		FETCH HOTEL_CURSOR INTO	@PeoHtl_CompanyCode, @PeoHtl_BkgRef, @PeoHtl_SegNum, @PeoHtl_CountryCode, @PeoHtl_CityCode, @PeoHtl_ADComTxNo, 
					@PeoHtl_ADComSeqNum, @PeoHtl_HotelCode, @PeoHtl_ArrDate, @PeoHtl_ArrTime, @PeoHtl_DepartDate, @PeoHtl_DepartTime, 
					@PeoHtl_ETA, @PeoHtl_ETD, @PeoHtl_SellCurr, @PeoHtl_CostCurr, @PeoHtl_TotalCost, @PeoHtl_TotalSell, @PeoHtl_TotalCTax, 
					@PeoHtl_TotalSTax, @PeoHtl_FormPay, @PeoHtl_Rmk1, @PeoHtl_Rmk2, @PeoHtl_InvRmk1, @PeoHtl_InvRmk2, @PeoHtl_VchRmk1, 
					@PeoHtl_VchRmk2, @PeoHtl_WestelNo, @PeoHtl_SourceSystem, @PeoHtl_SPCL_Request, @PeoHtl_HtlReference, @PeoHtl_HtlCfmBy,
					@PeoHtl_VoidBy, @PeoHtl_VoidOn, @PeoHtl_SourceRef, @PeoHtl_GstTax, @PeoHtl_ShowDetailFee, @PeoHtl_SuppCode, 
					@PeoHtl_HotelVoucherType, @PeoHtl_RoomTypeCode, @PeoHtl_FullRoomRate, @PeoHtl_DailyRate, @PeoHtl_LowRoomRate, 
					@PeoHtl_RateTypeCode, @PeoHtl_COD1, @PeoHtl_COD2, @PeoHtl_COD3, @PeoHtl_IsRecalable, @PeoHtl_ReasonCode, @PeoHtl_FromAD

	END
	CLOSE HOTEL_CURSOR
	DEALLOCATE HOTEL_CURSOR

	Drop Table #Tmp_OldHtl
	Drop Table #Tmp_OldHtlDetail
	---===================================================================



	---===================================================================
	--------------- Recalculate Booking  ------------------------------------------------------------------
	exec sp_PEOMSTRsummary_trigger @CompanyCode, @BkgRef

	SELECT '=================== END ======================='

	IF @@ERROR<>0 GOTO ERRHANDLE
	---===================================================================

	IF @@ERROR = 0 GOTO SUCCESSHANDLE
	
	----- ERROR  HANLDER -------------------------
	ERRHANDLE:
		-- *********************************************************
		ROLLBACK TRAN
		-- *********************************************************
		Insert Into tmp_HtlTsfr
		(CompanyCode, BkgRef, TripNo, CreateOn, [Description])
		Select @CompanyCode, @BkgRef, @TripNo, GetDate(), 'ERROR'
		Drop Table #Tmp_OldPax			
		Drop Table #Tmp_OldHtl			
		Drop Table #Tmp_OldHtlDetail
		CLOSE PAX_CURSOR
		DEALLOCATE PAX_CURSOR
		CLOSE HOTELDETAIL_AD_CURSOR
		DEALLOCATE HOTELDETAIL_AD_CURSOR
		CLOSE HOTELDETAIL_CURSOR
		DEALLOCATE HOTELDETAIL_CURSOR
		CLOSE HOTEL_CURSOR
		DEALLOCATE HOTEL_CURSOR

	SUCCESSHANDLE:
		-- *********************************************************
		COMMIT TRAN
		-- *********************************************************
		Insert Into tmp_HtlTsfr
		(CompanyCode, BkgRef, TripNo, CreateOn, [Description])
		Select @CompanyCode, @BkgRef, @TripNo, GetDate(), 'Success'


	--------------- Move Next Record -----------
	FETCH GW_CURSOR INTO	@CompanyCode, @BkgRef, @TripNo, @Login
END
CLOSE GW_CURSOR
DEALLOCATE GW_CURSOR


END





