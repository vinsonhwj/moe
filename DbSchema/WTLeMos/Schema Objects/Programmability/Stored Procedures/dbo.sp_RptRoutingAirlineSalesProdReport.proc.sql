﻿


CREATE    procedure dbo.sp_RptRoutingAirlineSalesProdReport
@COMPANY_CODE NCHAR(4),
@START_DATE VARCHAR(25),
@END_DATE VARCHAR(25),
@AIRLINE NVARCHAR(6),
@SHOW_RECORDS varchar(3),
@REPORT_TYPE CHAR(1),
@INCLUDE_WP CHAR(1)

AS

CREATE TABLE #tmpReport
(
	SeqNo int null default 0
,	Routing NVARCHAR(130) null default ''
,	Airline nvarchar(6) null default ''
,	BrATktCount decimal(9,0) null default 0
,	BrATktAmt decimal(16,2) null default 0.0
,	BrATktPercent decimal(5,1) null default 0.0
,	BrBTktCount decimal(9,0) null default 0
,	BrBTktAmt decimal(16,2) null default 0.0
,	BrBTktPercent decimal(5,1) null default 0.0
,	BrCTktCount decimal(9,0) null default 0
,	BrCTktAmt decimal(16,2) null default 0.0
,	BrCTktPercent decimal(5,1) null default 0.0
,	BrDTktCount decimal(9,0) null default 0
,	BrDTktAmt decimal(16,2) null default 0.0
,	BrDTktPercent decimal(5,1) default 0.0
,	BrETktCount decimal(9,0) null default 0
,	BrETktAmt decimal(16,2) null default 0.0
,	BrETktPercent decimal(5,1) null default 0.0
,  	RoutingTktTotal decimal(9,0) null default 0
,	RoutingAmtTotal decimal(16,2) null default 0
,	RoutingPercent decimal(5,1) null default 0.0
)

-- Init Value
DECLARE @lstr_BranchCodeA nchar(6)
DECLARE @lstr_BranchCodeB nchar(6)
DECLARE @lstr_BranchCodeC nchar(6)
DECLARE @lstr_BranchCodeD nchar(6)
DECLARE @lstr_BranchCodeE nchar(6)
DECLARE @lstr_TeamCodeWholeSale nchar(6)
DECLARE @lstr_TeamCodeWholeSaleP nchar(6)
DECLARE @lstr_Routing NVARCHAR(130)
DECLARE @lstr_Airline nvarchar(6)
DECLARE @lnum_TktTotal decimal(9,0)
DECLARE @lnum_RoutingTktTotal decimal(9,0)
DECLARE @lnum_TktAmt decimal(16,2)

SELECT @lstr_BranchCodeA = 'H'
SELECT @lstr_BranchCodeB = 'H'
SELECT @lstr_BranchCodeC = 'Y'
SELECT @lstr_BranchCodeD = 'C'
SELECT @lstr_BranchCodeE = 'T'
SELECT @lstr_TeamCodeWholeSale = 'HWT'
SELECT @lstr_TeamCodeWholeSaleP = 'HWP'
--END Init Value
DECLARE @lstr_topSQL varchar(3000)


-- Retrieve Top 100 Routing
CREATE TABLE #tmpRouting
(
	Routing nvarchar(200) null default ''
,	TktTotal decimal(9,0) null default 0
,	TktAmt decimal(16,2) null default 0
)

SELECT @lstr_topSQL = 'INSERT INTO #tmpRouting' +
' SELECT TOP ' + @SHOW_RECORDS + ' Routing, Count(*) as TktTotal, sum(netfare) as amt ' +
' FROM peotkt (nolock) ' +
' WHERE companycode = ''' + @COMPANY_CODE + '''' +
' AND IssueOn between ''' + @START_DATE + ''' AND ''' + @END_DATE + '''' 
IF LTRIM(RTRIM(@AIRLINE)) <> ''
	SELECT @lstr_topSQL = @lstr_topSQL + 'AND Airline = ''' + @AIRLINE + ''''
SELECT @lstr_topSQL = @lstr_topSQL + ' GROUP BY Routing ' 

IF @REPORT_TYPE = 'Q'
	SELECT @lstr_topSQL = @lstr_topSQL + ' ORDER BY 2 DESC'
ELSE
	SELECT @lstr_topSQL = @lstr_topSQL + ' ORDER BY 3 DESC'

PRINT @lstr_topSQL
EXEC(@lstr_topSQL)

--Get Airline
SELECT @lstr_topSQL = 'INSERT INTO #tmpReport ' +
'(Routing, Airline, RoutingTktTotal, RoutingAmtTotal, RoutingPercent) ' +
'SELECT rpt.Routing, tkt.Airline, count(*) as TktTotal, sum(tkt.Netfare) as amt, '
IF @REPORT_TYPE = 'Q'
	SELECT @lstr_topSQL = @lstr_topSQL + 'CASE WHEN rpt.TktTotal > 0 THEN (COUNT(tkt.Airline) / rpt.TktTotal) * 100 else 0 end as Per '
ELSE
	SELECT @lstr_topSQL = @lstr_topSQL + 'CASE WHEN rpt.TktAmt > 0 THEN (sum(tkt.Netfare) / rpt.TktAmt) * 100 else 0 end as Per ' 

SELECT @lstr_topSQL = @lstr_topSQL + 'FROM #tmpRouting rpt, peotkt tkt (nolock), peomstr mstr (nolock) ' +
'WHERE  ' +
'tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref ' +
' AND tkt.Routing = rpt.Routing ' +
' AND mstr.companycode = ''' + @COMPANY_CODE + '''' +
' AND tkt.IssueOn between ''' + @START_DATE + ''' AND ''' + @END_DATE + '''' 
IF LTRIM(RTRIM(@AIRLINE)) <> ''
	SELECT @lstr_topSQL = @lstr_topSQL + ' AND Airline = ''' + @AIRLINE + ''''
SELECT @lstr_topSQL = @lstr_topSQL + ' GROUP BY rpt.Routing, tkt.Airline, rpt.TktAmt, rpt.TktTotal '

PRINT @lstr_topSQL
EXEC(@lstr_topSQL)




-- Head Office (Without WholeSale)
SELECT rpt.Routing, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
INTO #tmpBrA
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeA AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY rpt.Routing, rpt.RoutingTktTotal, tkt.Airline

DECLARE lcur_BrA cursor for
SELECT Routing, TktTotal, amt, RoutingTktTotal, Airline
FROM #tmpBrA

open lcur_BrA
fetch lcur_BrA into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
while @@fetch_status = 0
begin

	UPDATE 	#tmpReport SET
	BrATktCount = @lnum_TktTotal,
	BrATktAmt = @lnum_TktAmt,
	BrATktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
	WHERE 
	Routing = @lstr_Routing
	AND Airline = @lstr_Airline

	fetch lcur_BrA into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
END

CLOSE lcur_BrA
DEALLOCATE lcur_BrA
DROP TABLE #tmpBrA;
--END OF BrA

-- Wholesales
SELECT rpt.Routing, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
INTO #tmpBrB
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND mstr.TeamCode = @lstr_TeamCodeWholeSale
GROUP BY rpt.Routing, rpt.RoutingTktTotal, tkt.Airline

DECLARE lcur_BrB cursor for
SELECT Routing, TktTotal, amt, RoutingTktTotal, Airline
FROM #tmpBrB

open lcur_BrB
fetch lcur_BrB into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
while @@fetch_status = 0
begin

	UPDATE 	#tmpReport SET
	BrBTktCount = @lnum_TktTotal,
	BrBTktAmt = @lnum_TktAmt,
	BrBTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
	WHERE 
	Routing = @lstr_Routing
	AND Airline = @lstr_Airline

	fetch lcur_BrB into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
END

CLOSE lcur_BrB
DEALLOCATE lcur_BrB
DROP TABLE #tmpBrB;
--END OF BrB

-- Yat Fat
SELECT rpt.Routing, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
INTO #tmpBrC
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeC AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY rpt.Routing, rpt.RoutingTktTotal, tkt.Airline

DECLARE lcur_BrC cursor for
SELECT Routing, TktTotal, amt, RoutingTktTotal, Airline
FROM #tmpBrC

open lcur_BrC
fetch lcur_BrC into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
while @@fetch_status = 0
begin

	UPDATE 	#tmpReport SET
	BrCTktCount = @lnum_TktTotal,
	BrCTktAmt = @lnum_TktAmt,
	BrCTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
	WHERE 
	Routing = @lstr_Routing
	AND Airline = @lstr_Airline

	fetch lcur_BrC into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
END

CLOSE lcur_BrC
DEALLOCATE lcur_BrC
DROP TABLE #tmpBrC;
--END OF BrC

-- Central
SELECT rpt.Routing, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
INTO #tmpBrD
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeD AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY rpt.Routing, rpt.RoutingTktTotal, tkt.Airline

DECLARE lcur_BrD cursor for
SELECT Routing, TktTotal, amt, RoutingTktTotal, Airline
FROM #tmpBrD

open lcur_BrD
fetch lcur_BrD into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
while @@fetch_status = 0
begin

	UPDATE 	#tmpReport SET
	BrDTktCount = @lnum_TktTotal,
	BrDTktAmt = @lnum_TktAmt,
	BrDTktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
	WHERE 
	Routing = @lstr_Routing
	AND Airline = @lstr_Airline

	fetch lcur_BrD into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
END

CLOSE lcur_BrD
DEALLOCATE lcur_BrD
DROP TABLE #tmpBrD;
--END OF BrD

-- CWB
SELECT rpt.Routing, tkt.Airline, Count(*) as TktTotal, sum(tkt.Netfare) as amt, rpt.RoutingTktTotal
INTO #tmpBrE
FROM #tmpReport rpt, peotkt tkt(nolock), peomstr mstr (nolock)
WHERE 
tkt.companycode = mstr.companycode AND tkt.bkgref = mstr.bkgref
AND tkt.Routing = rpt.Routing
AND mstr.companycode = @COMPANY_CODE
AND tkt.IssueOn between @START_DATE AND @END_DATE
AND LEFT(mstr.TeamCode,1) = @lstr_BranchCodeD AND mstr.TeamCode <> @lstr_TeamCodeWholeSale
GROUP BY rpt.Routing, rpt.RoutingTktTotal, tkt.Airline

DECLARE lcur_BrE cursor for
SELECT Routing, TktTotal, amt, RoutingTktTotal, Airline
FROM #tmpBrE

open lcur_BrE
fetch lcur_BrE into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
while @@fetch_status = 0
begin

	UPDATE 	#tmpReport SET
	BrETktCount = @lnum_TktTotal,
	BrETktAmt = @lnum_TktAmt,
	BrETktPercent = CASE WHEN #tmpReport.RoutingAmtTotal <= 0 THEN 0 ELSE ((@lnum_TktAmt/#tmpReport.RoutingAmtTotal) * 100) END
	WHERE 
	Routing = @lstr_Routing
	AND Airline = @lstr_Airline

	fetch lcur_BrE into @lstr_Routing, @lnum_TktTotal, @lnum_TktAmt, @lnum_RoutingTktTotal, @lstr_Airline
END

CLOSE lcur_BrE
DEALLOCATE lcur_BrE
DROP TABLE #tmpBrE;
--END OF BrE

SELECT rpt.*
INTO #tmpResult
FROM #tmpReport rpt, #tmpRouting Routing
WHERE rpt.Routing = Routing.Routing
order by Routing.TktAmt desc, rpt.RoutingAmtTotal desc;

-- UPDATE SEQNO
DECLARE @lnum_Seqno int 
DECLARE @lstr_TmpRouting nvarchar(200)
DECLARE lcur_Seq cursor for
SELECT Routing from #tmpResult

SELECT @lstr_TmpRouting = ''
SELECT @lnum_Seqno = 0

open lcur_Seq
fetch lcur_Seq into @lstr_Routing
while @@fetch_status = 0
BEGIN
	if @lstr_TmpRouting <> @lstr_Routing
	BEGIN
		SELECT @lnum_Seqno = @lnum_Seqno + 1
		UPDATE #tmpReport 
		SET seqno = @lnum_Seqno
		WHERE routing = @lstr_Routing
	END

	SELECT @lstr_TmpRouting = @lstr_Routing
	fetch lcur_Seq into @lstr_Routing
END

CLOSE lcur_Seq
DEALLOCATE lcur_Seq
--PRINT 'START T'

SELECT rpt.*
FROM #tmpReport rpt, #tmpRouting Routing
WHERE rpt.Routing = Routing.Routing
order by Routing.TktAmt desc, rpt.RoutingAmtTotal desc;

/*
SELECT rpt.*
FROM #tmpReport rpt, #tmpRouting Routing
WHERE rpt.Routing = Routing.Routing
order by Routing.TktAmt desc, rpt.RoutingAmtTotal desc;
*/
DROP TABLE #tmpRouting;
DROP TABLE #tmpResult;
DROP TABLE #tmpReport;




