﻿










/*
Logic:
Select all booking that satisfy the folling criteria
1. NEVER issue Invoice or Credit Note
2. have issue AT LEAST ONE document, document type include XO, VCH, TKT and MCO
3. link with Master PNR
END Login
*/
CREATE                   PROCEDURE dbo.sp_AutomailUninvUnDoc
@COMPANY_CODE VARCHAR(2)
AS

truncate table automail_uninvundoc
-- UNINVOICE

select 
ltrim(rtrim(m.companycode)) as companycode, 
ltrim(rtrim(m.teamcode)) as teamcode, 
ltrim(rtrim(m.staffcode)) as staffcode
, m.createby, m.bkgref, m.masterpnr
,(select min(createon) from 
(
select bkgref, createon from peoxo x (nolock) where x.companycode = m.companycode and x.bkgref = m.bkgref
union 
select bkgref, createon from peovch v (nolock) where v.companycode = m.companycode and v.bkgref = m.bkgref
union
select bkgref, issueon as createon from peotkt t (nolock) where t.companycode = m.companycode and t.bkgref = m.bkgref
union
select bkgref, issueon as createon from peomco mco (nolock) where mco.companycode = m.companycode and mco.bkgref = m.bkgref

) a
group by bkgref) as BookingDate
, m.departdate as sdeparture
, convert(char(11), m.departdate, 106) as depart
, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
, isnull(m.invcount, 0) as totalinvCount
, isnull(m.doccount, 0) as totaldocCount
, (select min(createon) from peoinv v (nolock) where v.companycode = m.companycode and v.bkgref = m.bkgref) invdate
into #tmpSalesReminderEN
from dbo.peomstr m (nolock) 
where m.companycode = @COMPANY_CODE and (m.ApproveBy is null or m.ApproveBy = '') and
m.masterpnr in ( 
	select a.masterpnr
		from dbo.peomstr a (nolock)
        where a.companycode = m.companycode  and (m.ApproveBy is null or m.ApproveBy = '')
	group by a.masterpnr
	having ( sum(isnull(a.[invcount], 0)) = 0 and sum(isnull(a.[doccount],0)) > 0)
	)
order by m.teamcode, m.staffcode, m.masterpnr
	
	
select  a.* into #tmpSalesReminderENR from 
(
SELECT 
uninv.* 
from #tmpSalesReminderEN uninv
where uninv.bkgref not in (select distinct bkgref from peoPFQue where companycode = uninv.companycode)
Union
select  
uninv.* 
from #tmpSalesReminderEN uninv, peoPFQue pfq
where uninv.companycode = pfq.companycode and uninv.bkgref = pfq.bkgref
and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = uninv.bkgref and pfq1.companycode = uninv.companycode)
and ( uninv.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - uninv.Document < 0
) as a  
order by a.teamcode, a.staffcode, a.masterpnr

INSERT INTO automail_uninvundoc 
(COMPANYCODE,BRANCH,TEAM,STAFF,BKGREF,DOCDATE,DOCNUM,DOCAMT,INVNUM,INVAMT,MASTERPNR, invdate,DepartDate)
SELECT companycode, left(teamcode, 1),teamcode, staffcode, bkgref, BookingDate, totaldocCount, 
document, totalinvCount,Invoice,masterpnr, invdate,sdeparture
FROM #tmpSalesReminderENR where not ( Invoice = 0 and Document = 0)

DROP TABLE #tmpSalesReminderEN
DROP TABLE #tmpSalesReminderENR

--UNDOC
select 
ltrim(rtrim(m.companycode)) as companycode, 
ltrim(rtrim(m.teamcode)) as teamcode, 
ltrim(rtrim(m.staffcode)) as staffcode
, m.createby, m.bkgref, m.masterpnr
,(select min(createon) from 
( 
select bkgref, createon from peoxo x (nolock) where x.companycode = m.companycode and x.bkgref = m.bkgref
union 
select bkgref, createon from peovch v (nolock) where v.companycode = m.companycode and v.bkgref = m.bkgref
union
select bkgref, issueon as createon from peotkt t (nolock) where t.companycode = m.companycode and t.bkgref = m.bkgref
) a
group by bkgref) as BookingDate
, m.departdate as sdeparture
, convert(char(11), m.departdate, 106) as depart
, Cast(isnull(m.invamt, .00) + isnull(m.invtax, .00) as decimal(20,2)) as [Invoice]
, Cast(isnull(m.docamt, .00) + isnull(m.doctax, .00) as decimal(20,2)) as [Document]
, isnull(m.invcount, 0) as totalinvCount
, isnull(m.doccount, 0) as totaldocCount
, (select min(createon) from peoinv v (nolock) where v.companycode = m.companycode and v.bkgref = m.bkgref) invdate
into #tmpDocumentReminderEN
from dbo.peomstr m (nolock)
where m.companycode = @COMPANY_CODE AND (m.ApproveBy is null or m.ApproveBy = '') AND 
m.masterpnr in ( 
	select a.masterpnr
		from dbo.peomstr a (nolock)
        where a.companycode = m.companycode and a.masterpnr = m.masterpnr AND (m.ApproveBy is null or m.ApproveBy = '')
	group by a.masterpnr
	having ( sum(isnull(a.[invcount], 0)) > 0 and sum(isnull(a.[doccount],0)) = 0)
	)
order by m.teamcode, m.staffcode, m.masterpnr


select  a.* into #tmpDocumentReminderENR from (
SELECT 
undoc.* 
from #tmpDocumentReminderEN undoc 
where undoc.bkgref not in (select distinct bkgref from peoPFQue where companycode = undoc.companycode)
Union
select  
undoc.* 
from #tmpDocumentReminderEN undoc, peoPFQue pfq
where undoc.companycode = pfq.companycode and undoc.bkgref = pfq.bkgref
and pfq.seqnum = (select right ( '000' + convert(varchar(3),  max( convert(integer, seqnum) ) ) , 3)  from peoPFQue pfq1 where pfq1.bkgref = undoc.bkgref and pfq1.companycode = undoc.companycode)
and ( undoc.invoice - isnull(pfq.AbnormalAmount, 0.00) ) - undoc.Document < 0
) as a  
order by a.teamcode, a.staffcode, a.masterpnr


INSERT INTO automail_uninvundoc 
(COMPANYCODE,BRANCH,TEAM,STAFF,BKGREF,DOCDATE,DOCNUM,DOCAMT,INVNUM,INVAMT,MASTERPNR,invdate,DepartDate)
SELECT companycode, left(teamcode, 1),teamcode, staffcode, bkgref, BookingDate, totaldocCount, 
document, totalinvCount,Invoice,masterpnr, invdate, sdeparture
FROM #tmpDocumentReminderENR where 
not ( Invoice = 0 and Document = 0) 
and ( Invoice > ( select isnull(undoc_default_amt, 0 ) from company where companycode = @COMPANY_CODE))

DROP TABLE #tmpDocumentReminderEN
DROP TABLE #tmpDocumentReminderENR











