﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_OTH_PKG_BalanceAmt]
	@CompanyCode varchar(2),
	@InvNum NVARCHAR(10)
AS
BEGIN

	DECLARE @PKGAmt DECIMAL(18,2)
	DECLARE @TolPaidAmt DECIMAL (18,2)

	SELECT @PKGAmt=ISNULL(sum(D.SELLAMT*D.QTY),0) FROM dbo.PEOINVSEG AS S INNER JOIN dbo.PEOINVDETAIL AS D
		ON S.COMPANYCODE = D.COMPANYCODE AND S.INVNUM = D.INVNUM AND S.SEGNUM=D.SEGNUM
		WHERE S.COMPANYCODE=@CompanyCode AND S.INVNUM =@InvNum AND S.SERVTYPE='OTH' AND S.SEGTYPE='PKG'

	SELECT @TolPaidAmt=ISNULL(SUM(PaidAmt),0) FROM dbo.peoInvRept WHERE CompanyCode=@CompanyCode AND InvNum =@InvNum AND VOID2JBA IS NULL 

	SELECT (CASE WHEN @PKGAmt>@TolPaidAmt THEN @PKGAmt-@TolPaidAmt ELSE 0 END )AS PKGBalanceAmt

END
