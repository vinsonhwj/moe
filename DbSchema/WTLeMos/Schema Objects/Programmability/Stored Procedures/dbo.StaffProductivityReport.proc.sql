﻿

CREATE PROCEDURE StaffProductivityReport

@StartDate CHAR(10),
@EndDate CHAR(10),
@StaffCode VarChar(10) = '%'

AS

BEGIN


select  b.StaffCode,
CONVERT(CHAR(3), SUM([Total Pax])) [Total Pax],

CAST(
ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) 
 AS Decimal(12,2) ) [Price],
CAST(
ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0)
 AS Decimal(12,2) ) [Cost],
CAST(
	(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0))
	 AS Decimal(12,2) )  [Margin],

[Yield(%)] = 
CASE 

WHEN 	(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
	(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0) )  = 0 THEN 
	CAST(
		0
	 AS Decimal(12,2) )


WHEN 	ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0) = 0 THEN
	CAST(
		-100
	 AS Decimal(12,2) )

ELSE

	CAST(
 	(
		(ISNULL( SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  -
		(ISNULL(SUM([Total Voucher]),0) + ISNULL(SUM([Total XO]),0) + ISNULL(SUM( [Total TKT]), 0))
	) /
		(ISNULL(SUM([Total Invoice]), 0)  -  ISNULL(SUM([Total Credit Note]), 0))  * 100
	AS Decimal(12,2) )

END

From 
--select * from
 BookSum_By_Staff b
where BKGREF IN (
SELECT M.BKGREF FROM PEOMSTR M, PEOINV I
WHERE M.BKGREF = I.BKGREF
AND CONVERT(CHAR(10), I.CreateOn, 111) BETWEEN @StartDate AND @EndDate
AND M.StaffCode like @StaffCode
)
GROUP BY b.STAFFCODE


END

