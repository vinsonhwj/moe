﻿





CREATE                PROCEDURE dbo.sp_RptAnnualCustomerSalesReport
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(10),
@END_DATE    CHAR(25),
@TEAM_CODE   VARCHAR(2000)

AS

DECLARE @lstr_TeamCode char(3), @lstr_CltCode char(6), @lstr_CltName nvarchar(100), @lnum_Cost decimal(16,2), @lnum_SellTotal decimal(16,2), @lnum_Sell decimal(16,2),@lnum_Month int, @lstr_bkgref nvarchar(20), @lnum_invamt decimal(16,2)
-- VARIABLE FOR CALCUATE PERCENTAGE
DECLARE @lnum_BkgInvTotal decimal(16,2), @lnum_BkgDocTotal  decimal(16,2), @lnum_CltCount integer

-- Split TeamCode into  Temp Table
Declare @TargetCode varchar(10)

CREATE TABLE #TmpTeam (teamcode nvarchar(50) )
While (@TEAM_CODE <> '')
Begin
	If left(@TEAM_CODE,1) = '|'
	Begin
		Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@TEAM_CODE, 0, CharIndex('|', @TEAM_CODE)))
	
		If @TargetCode = ''
			Select @TargetCode=@TEAM_CODE

		INSERT INTO #TmpTeam VALUES (@TargetCode)

		If (Select CharIndex('|', @TEAM_CODE)) > 0
			Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE) - Len(@TargetCode) - 1))
		Else
			Select @TEAM_CODE = (Select Right(@TEAM_CODE, Len(@TEAM_CODE) - Len(@TargetCode)))
		
	End
End
--- end of Create Temp Table


------------------Main Part --------------------------------------------------
SELECT 
DATEPART(MONTH,b.createon) create_mth,b.cltcode,isnull(c.cltname,'No Client Name') as cltname,
sum(case substring(b.invnum,2,1) when 'I' then b.hkdamt else b.hkdamt * (-1) end) sell
INTO #TmpSellAmt 
FROM peoinv b (nolock), customer c (nolock), #tmpTeam Team
WHERE b.companycode=c.companycode  AND b.cltcode=c.cltcode 
AND b.companycode = @COMPANY_CODE
AND b.createon BETWEEN @START_DATE AND @END_DATE
AND c.teamcode = Team.TeamCode
AND b.invType <> 'DEP'
GROUP BY datepart(month,b.createon),b.cltcode,c.cltname
ORDER BY b.cltcode,c.cltname,datepart(month,b.createon)



------- Execute Result SQL and prepare Dynamic SQL

-- Variable for Dynaminc SQL
DECLARE @lstr_SelectDynamicSQL as NVARCHAR(100);
DECLARE @lstr_DynamicSQL as NVARCHAR(4000);
DECLARE @lstr_FromDynamicSQL AS NVARCHAR(100);
DECLARE @lstr_Month as VARCHAR(10);
DECLARE @li_Count AS INTEGER;
DECLARE @lstr_MonthName CHAR(3);
DECLARE @ldt_CurrMonth AS Datetime;

----------------------------------------------


-- Start Dynamic SQL
SET @lstr_SelectDynamicSQL  = 'SELECT distinct cltcode as [Client Code], cltname as [Client Name]';
SET @lstr_DynamicSQL = '';
SET @lstr_FromDynamicSQL = ' ,''0'' AS [Total Sell] FROM #TmpSellAmt a';
SET @li_Count = 1;
SET @ldt_CurrMonth = convert(datetime,@START_DATE)

while @ldt_CurrMonth < convert(datetime,@END_DATE)
begin 
	select @lstr_MonthName = UPPER(LEFT(DATENAME(MONTH,@ldt_CurrMonth),3))
	select @lstr_Month = CONVERT(VARCHAR(10),MONTH(@ldt_CurrMonth))

	SELECT @lstr_DynamicSQL = @lstr_DynamicSQL + 
        ',(SELECT isnull(sum(sell),0) FROM #TmpSellAmt c ' + 
        'WHERE c.cltcode = a.cltcode and c.create_mth = ' + @lstr_Month +') as [' + @lstr_MonthName + ' Sell] '

	SELECT @ldt_CurrMonth = DATEADD(MONTH,1,@ldt_CurrMonth)


end


EXEC(@lstr_SelectDynamicSQL + @lstr_DynamicSQL +@lstr_FromDynamicSQL);
-- Execute Dynamic SQL

-- Drop Table
drop table #TmpSellAmt;
drop table #tmpTeam;









