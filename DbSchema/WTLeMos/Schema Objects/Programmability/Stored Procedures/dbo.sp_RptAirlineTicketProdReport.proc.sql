﻿












CREATE               PROCEDURE dbo.sp_RptAirlineTicketProdReport
@COMPANY_CODE CHAR(2),
@START_DATE  CHAR(25),
@END_DATE    CHAR(25),
@AIRLINE NVARCHAR(3)

AS

------------------Main Part --------------------------------------------------
CREATE TABLE #TmpRptResult
(
	create_year int null default '2006'
,	create_mth int null default '1'
,	Airline nchar(3) null default ''
,	TktCount int null default 0
)

IF ISNULL(LTRIM(RTRIM(@AIRLINE)),'') = ''
BEGIN
	INSERT INTO #TmpRptResult
	SELECT
	DATEPART(YEAR,createon) as create_year, DATEPART(MONTH,createon) as create_mth, Airline, isnull(count(*),0) as TktCount
	FROM peotkt (nolock)
	WHERE companycode = @COMPANY_CODE
	AND createon BETWEEN @START_DATE AND @END_DATE
	AND voidon is null
	GROUP BY DATEPART(YEAR, createon), DATEPART(MONTH, createon), Airline
	ORDER BY DATEPART(YEAR, createon), DATEPART(MONTH, createon), Airline
END
ELSE
BEGIN
	INSERT INTO #TmpRptResult
	SELECT
	DATEPART(YEAR,createon) as create_year, DATEPART(MONTH,createon) as create_mth, Airline, isnull(count(*),0) as TktCount
	FROM peotkt (nolock)
	WHERE companycode = @COMPANY_CODE
	AND createon BETWEEN @START_DATE AND @END_DATE
	AND voidon is null
	AND Airline = @AIRLINE
	GROUP BY DATEPART(YEAR, createon), DATEPART(MONTH, createon), Airline
	ORDER BY DATEPART(YEAR, createon), DATEPART(MONTH, createon), Airline
END	
	
-- Variable for Dynaminc SQL
DECLARE @lstr_SelectDynamicSQL as NVARCHAR(100);
DECLARE @lstr_DynamicSQL as NVARCHAR(600);
DECLARE @lstr_DynamicSQL1 as NVARCHAR(4000);
DECLARE @lstr_DynamicSQL2 as NVARCHAR(4000);
DECLARE @lstr_FromDynamicSQL AS NVARCHAR(100);
DECLARE @lstr_Month as VARCHAR(10);
DECLARE @lstr_Year as VARCHAR(4);
DECLARE @li_Count AS INTEGER;
DECLARE @lstr_MonthName nvarCHAR(8);

----------------------------------------------


-- Start Dynamic SQL
SET @lstr_SelectDynamicSQL  = 'SELECT distinct Airline as [Airline]';
SET @lstr_DynamicSQL1 = '';
SET @lstr_DynamicSQL2 = '';
SET @lstr_FromDynamicSQL = ' FROM #TmpRptResult a';
SET @li_Count = 1;


DECLARE lCur_Month cursor for
select DISTINCT create_year, create_mth from #TmpRptResult (nolock) 
order by create_year, create_mth
open lCur_Month
fetch lCur_Month into @lstr_Year, @lstr_Month
while @@fetch_status = 0
begin
	select @lstr_MonthName = UPPER(LEFT(DATENAME(MONTH,'2006-' + @lstr_Month + '-1'),3)) + ' ''' + right(@lstr_Year,2)
	print @lstr_MonthName
	SELECT @lstr_DynamicSQL = ',ISNULL((SELECT isnull(TktCount,0) FROM #TmpRptResult t WHERE t.Airline = a.Airline  and t.create_mth = ' + @lstr_Month +' and t.create_year = ' + @lstr_Year + '),0) as [' + @lstr_MonthName + '] '
	
	IF @li_Count <= 10
	BEGIN
		SELECT @lstr_DynamicSQL1 = @lstr_DynamicSQL1 + @lstr_DynamicSQL
	END
	ELSE
		SELECT @lstr_DynamicSQL2 = @lstr_DynamicSQL2 + @lstr_DynamicSQL
	
	SELECT @li_Count = @li_Count + 1;
	fetch lCur_Month into @lstr_Year, @lstr_Month
end

close lCur_Month
deallocate lCur_Month

-- Execute Dynamic SQL
EXEC(@lstr_SelectDynamicSQL + @lstr_DynamicSQL1 + @lstr_DynamicSQL2 +@lstr_FromDynamicSQL);

-- Drop Table
drop table #TmpRptResult;











