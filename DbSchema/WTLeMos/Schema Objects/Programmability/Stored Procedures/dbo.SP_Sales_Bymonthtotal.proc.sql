﻿



CREATE PROCEDURE SP_Sales_Bymonthtotal
  @AS_MONTH varchar(2),
  @AS_YEAR varchar(4),
  @AS_COMPANY  VARCHAR(2),
  @AS_BRANCHCODE  VARCHAR(3),
  @AS_TEAMCODE   VARCHAR(5),
  @AS_STAFFCODE VARCHAR(10)
AS
--INV
DECLARE @AS_DATERANGEF varchar(10)
DECLARE @AS_DATERANGET varchar(30)
DECLARE @AS_SQL VARCHAR(8000)
DECLARE @AS_SQL1 VARCHAR(3000)
DECLARE @AS_SQL2 VARCHAR(3000)
DECLARE @AS_SQL3 VARCHAR(3000)
SELECT @AS_DATERANGEF=@AS_YEAR+'.'+'01'  +'.01'
--SELECT @AS_DATERANGEF=@AS_YEAR+'.'+@AS_MONTH  +'.01'
DECLARE @AD_DATERANGET datetime
--SELECT CONVERT(datetime,'2003/02/28',110)	
SELECT @AD_DATERANGET=CONVERT( datetime,@AS_YEAR+'.'+@AS_MONTH  +'.01',110)	
SELECT @AD_DATERANGET=DATEADD(month, 1, @AD_DATERANGET)
SELECT @AD_DATERANGET=DATEADD(day, -1, @AD_DATERANGET)
if @AD_DATERANGET>getdate()
   SELECT @AD_DATERANGET=getdate()
SELECT @AS_DATERANGET=CONVERT(varchar(10),@AD_DATERANGET,102)+' 23:59:59'	
SELECT @AS_SQL1='select a.createby,SUM(isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totalamount,SUM(isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totaltax,sum((isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1)) as Grandtotal,'
SELECT @AS_SQL1=@AS_SQL1+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invamount,'
SELECT @AS_SQL1=@AS_SQL1+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invtax,'
SELECT @AS_SQL1=@AS_SQL1+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then (isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as total '
SELECT @AS_SQL1=@AS_SQL1+'INTO #temp '
SELECT @AS_SQL1=@AS_SQL1+'from peoinvdetail a,peoinv b,team C,BRANCH d '
SELECT @AS_SQL1=@AS_SQL1+'where a.invnum=b.invnum'
SELECT @AS_SQL1=@AS_SQL1+' and (b.teamcode=c.teamcode'
SELECT @AS_SQL1=@AS_SQL1+' and b.companycode=c.companycode)'
SELECT @AS_SQL1=@AS_SQL1+' and (c.BRANCHCODE=d.BRANCHCODE'
SELECT @AS_SQL1=@AS_SQL1+' and c.companycode=d.companycode)'
SELECT @AS_SQL1=@AS_SQL1+' and b.voidon is null'
SELECT @AS_SQL1=@AS_SQL1+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL1=@AS_SQL1+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+@AS_DATERANGET+'"'
IF @AS_STAFFCODE<>'ALL' and @AS_STAFFCODE<>''
   SELECT @AS_SQL1=@AS_SQL1 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL1=@AS_SQL1+' and b.TEAMCODE="'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and @AS_BRANCHCODE<>''
   SELECT @AS_SQL1=@AS_SQL1+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL1=@AS_SQL1+' AND b.invtype="I"'
SELECT @AS_SQL1=@AS_SQL1 +' group by a.createby'
--CRN
SELECT @AS_SQL2=' union all select a.createby,-SUM(isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totalamount,-SUM(isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totaltax,-sum((isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1)) as Grandtotal,'
SELECT @AS_SQL2=@AS_SQL2+'-sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invamount,'
SELECT @AS_SQL2=@AS_SQL2+'-sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invtax,'
SELECT @AS_SQL2=@AS_SQL2+'-sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then (isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as total '
--SELECT @AS_SQL2=@AS_SQL2+'INTO #C '
SELECT @AS_SQL2=@AS_SQL2+'from peoinvdetail a,peoinv b,team C,BRANCH d '
SELECT @AS_SQL2=@AS_SQL2+'where a.invnum=b.invnum'
SELECT @AS_SQL2=@AS_SQL2+' and (b.teamcode=c.teamcode'
SELECT @AS_SQL2=@AS_SQL2+' and b.companycode=c.companycode)'
SELECT @AS_SQL2=@AS_SQL2+' and (c.BRANCHCODE=d.BRANCHCODE'
SELECT @AS_SQL2=@AS_SQL2+' and c.companycode=d.companycode)'
SELECT @AS_SQL2=@AS_SQL2+' and b.voidon is null'
SELECT @AS_SQL2=@AS_SQL2+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL2=@AS_SQL2+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+@AS_DATERANGET+'"'
IF @AS_STAFFCODE<>'ALL' and @AS_STAFFCODE<>''
   SELECT @AS_SQL2=@AS_SQL2 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL2=@AS_SQL2+' and b.TEAMCODE="'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and @AS_BRANCHCODE<>''
   SELECT @AS_SQL2=@AS_SQL2+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL2=@AS_SQL2+' AND b.invtype="C"'
SELECT @AS_SQL2 =@AS_SQL2 +' group by a.createby'
--RPT
SELECT @AS_SQL3=' union all select a.createby,SUM(isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totalamount,SUM(isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1)) AS totaltax,sum((isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1)) as Grandtotal,'
SELECT @AS_SQL3=@AS_SQL3+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.SELLAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invamount,'
SELECT @AS_SQL3=@AS_SQL3+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then isnull(a.taxAMT,0)*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as invtax,'
SELECT @AS_SQL3=@AS_SQL3+'sum( case when year(a.CREATEON)='+@AS_YEAR+' and month(a.CREATEON)='+@AS_MONTH+' then (isnull(a.SELLAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,1) else 0 end ) as total '
--SELECT @AS_SQL3=@AS_SQL3+'INTO #R '
SELECT @AS_SQL3=@AS_SQL3+'from peorptdetail a,peorpt b,team C,BRANCH d '
SELECT @AS_SQL3=@AS_SQL3+'where a.invnum=b.invnum'
SELECT @AS_SQL3=@AS_SQL3+' and (b.teamcode=c.teamcode'
SELECT @AS_SQL3=@AS_SQL3+' and b.companycode=c.companycode)'
SELECT @AS_SQL3=@AS_SQL3+' and (c.BRANCHCODE=d.BRANCHCODE'
SELECT @AS_SQL3=@AS_SQL3+' and c.companycode=d.companycode)'
SELECT @AS_SQL3=@AS_SQL3+' and b.voidon is null '
SELECT @AS_SQL3=@AS_SQL3+' and b.companycode="'+@AS_COMPANY+'"'
SELECT @AS_SQL3=@AS_SQL3+' and a.CREATEON between "'+ @AS_DATERANGEF+'" and "'+@AS_DATERANGET+'"'
IF @AS_STAFFCODE<>'ALL' and @AS_STAFFCODE<>''
   SELECT @AS_SQL3=@AS_SQL3 +' and b.STAFFCODE="'+@AS_STAFFCODE+'"'
IF @AS_TEAMCODE<>'ALL' and @AS_TEAMCODE<>''
   SELECT @AS_SQL3=@AS_SQL3+' and b.TEAMCODE="'+@AS_TEAMCODE+'"'
IF @AS_BRANCHCODE<>'ALL' and @AS_BRANCHCODE<>''
   SELECT @AS_SQL3=@AS_SQL3+' and c.BRANCHCODE="'+@AS_BRANCHCODE+'"'
SELECT @AS_SQL3 =@AS_SQL3 +' group by a.createby'
--SALES 
SELECT @AS_SQL=' select createby,sum(totalamount) as totalamount,sum(totaltax) as totaltax ,sum(totalamount+totaltax) as Grandtotal,sum(totaltax) as totaltax,sum(invamount) as invamount,sum(invtax) as invtax ,sum(invamount+invtax) as total from #temp group by createby'
SELECT @AS_SQL=@AS_SQL1+@AS_SQL2+@AS_SQL3+@AS_SQL
EXEC(@AS_SQL)




