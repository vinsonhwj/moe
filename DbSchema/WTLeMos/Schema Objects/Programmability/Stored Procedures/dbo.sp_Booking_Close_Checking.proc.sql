﻿CREATE  procedure [dbo].[sp_Booking_Close_Checking]
	@Companycode varchar(2), 
	@MasterPNR nvarchar(10), 
	@StaffCode nchar(10)
as

BEGIN
	Declare @sResult varchar(1) --'Y' is can close ,'N' can't close 
	Declare @sResultDesc varchar(100)

	Declare @ModuleCode nvarchar(10)
	
	DECLARE @sStaffCode nchar(10)
	DECLARE @sIsClosed varchar(1)
	
	SET @ModuleCode='BKGMNT'
	SET @sResult='Y'
	
	SELECT @sResult = IsNull((SELECT (CASE WHEN ISNULL(IsClosed,'N') = 'Y' THEN 'C'			-- Booking Already Closed
					WHEN IsNull(ProfileID,0) = 0 THEN 'F'				-- Missing Profile ID
					ELSE 'Y' END)
				FROM dbo.PEOMSTR 
				WHERE CompanyCode=@CompanyCode And BkgRef=@MasterPnr),'N')

	IF @sResult = 'Y'
	BEGIN
		SELECT @sResult = IsNull((SELECT (CASE WHEN SUM(ISNULL(INVAMT,0) + ISNULL(INVTAX,0)) - SUM(ISNULL(ReptAmt,0) + ISNULL(ReptTax,0))<>0
						THEN 'R' 						-- Receipt not settled yet
						ELSE 'Y' END)
					FROM dbo.PEOMSTR 
					WHERE CompanyCode=@CompanyCode And MasterPnr=@MasterPnr),'N')
	END

	IF @sResult = 'Y'
	BEGIN
		Declare @yield DECIMAL (18,4)
		
		SELECT 	@yield=(CASE WHEN ((ReptAmt  + ReptTax ) - ttlinvgst)=0 THEN 1 ELSE 
			(((ReptAmt + ReptTax ) - ttlinvgst) - ((docamt + doctax) - ttldocgst))/((ReptAmt  + ReptTax ) - ttlinvgst)
			END )
		FROM (	select 
				SUM(isnull(ReptAmt, 0)) as ReptAmt,  SUM(isnull(ReptTax, 0)) as ReptTax, 
				SUM(isnull(ttlinvgst, 0)) as ttlinvgst,  SUM(isnull(docamt, 0)) as docamt, 
				SUM(isnull(doctax, 0)) as doctax, SUM(isnull(ttldocgst, 0)) as  ttldocgst
			from peomstr where MASTERPNR = @MasterPNR  and companycode =@Companycode
		) AS a
		
		SELECT @sResult = IsNull((SELECT (CASE WHEN NOT (PRFTDOWN<=@yield*100 AND @yield*100<=PRFTUP) THEN 'G' 
						ELSE 'Y' END)						-- Invalid booking GP 
					FROM dbo.PEOMSTR AS m 
						INNER JOIN dbo.Customer AS c
						ON m.COMPANYCODE = c.COMPANYCODE AND c.CLTCODE=m.CLTODE 
					WHERE MASTERPNR = @MasterPNR and m.companycode =@Companycode),'N')
	END

	IF @sResult = 'Y'
	BEGIN
		DECLARE @isExists varchar(1)

		SELECT m.CompanyCode,m.MasterPnr,mc.CreateOn AS LastCreateOn ,COUNT(*) AS Time ,
			(CASE WHEN YEAR(mc.CreateOn)=Year(GETDATE()) AND MONTH(mc.CreateOn)=MONTH(GETDATE()) THEN COUNT(*) ELSE COUNT(*)+1 END ) NeedTime
		INTO #tmpHistory FROM (
			SELECT CompanyCode,MasterPnr ,YEAR(CreateOn)AS Year,MONTH(CreateOn) Month
			FROM dbo.peoMstr_CloseHistory 
			WHERE CompanyCode =@Companycode AND MasterPnr =@MasterPNR
				AND ActionType ='C' AND CreateBy =@StaffCode GROUP BY CompanyCode,MasterPnr,YEAR(CreateOn),MONTH(CreateOn)
		)AS m LEFT JOIN 
		(
			SELECT TOP 1* FROM dbo.peoMstr_CloseHistory WHERE CompanyCode =@Companycode AND MasterPnr =@MasterPNR
				AND ActionType ='C' AND CreateBy =@StaffCode ORDER BY CreateOn DESC
		) AS mc
		ON m.CompanyCode = mc.CompanyCode AND m.MasterPnr = mc.MasterPnr 
		GROUP BY m.CompanyCode,m.MasterPnr,mc.CreateOn 

		SELECT @isExists=(CASE WHEN COUNT(*)>0 THEN 'Y' ELSE 'N' END ) FROM #tmpHistory 
		IF @isExists ='N'
		BEGIN
			INSERT #tmpHistory  (CompanyCode,MasterPnr,LastCreateOn,Time,NeedTime) 
			VALUES (
			@Companycode, -- CompanyCode - nvarchar(2)
			@MasterPNR , -- MasterPnr - nvarchar(10)
			NULL , -- LastCreateOn - null
			0, -- Time - null
			1-- NeedTime - null
			) 
		END 

		SELECT COMPANYCODE,LOGIN ,MODULECODE ,RIGHTSCODE ,
			(CASE RIGHTSCODE WHEN 'CLS1' THEN 1 WHEN 'CLS2' THEN 2 ELSE 3 END)CanTime
		INTO #tmpAccess FROM dbo.ACCESS WHERE COMPANYCODE =@Companycode AND LOGIN =@StaffCode AND MODULECODE =@ModuleCode 
			AND RIGHTSCODE IN ('CLS1','CLS2','CLS3') AND ACCESSRIGHTS =1

		SELECT @sResult = IsNull((SELECT (CASE WHEN COUNT(*)<=0 THEN 'A' 			-- Access denied 
						ELSE 'Y' END) 
					FROM #tmpHistory AS h 
						INNER JOIN #tmpAccess AS a
						ON h.CompanyCode = a.COMPANYCODE AND h.NeedTime =a.CanTime),'N') 

		DROP TABLE #tmpHistory 
		DROP TABLE #tmpAccess 
	END
	
	IF @sResult = 'Y'
	BEGIN
		IF EXISTS(	Select c.PaxLName
				From	(Select rtrim(p.PaxLName) as PaxLName, rtrim(p.PaxFName) as PaxFName, rtrim(PaxType) as PaxType 
						From peoPax p 
							Inner Join peoMstr m
							on p.CompanyCode=m.CompanyCode and p.BkgRef=m.BkgRef 
						Where m.CompanyCode=@CompanyCode and m.MasterPnr=@MasterPNR and m.BkgRef<>MasterPnr) c
					Left Outer Join
					(Select rtrim(p.PaxLName) as PaxLName, rtrim(p.PaxFName) as PaxFName, rtrim(PaxType) as PaxType 
						From peoPax p 
							Inner Join peoMstr m
							on p.CompanyCode=m.CompanyCode and p.BkgRef=m.BkgRef 
						Where m.CompanyCode=@CompanyCode and m.BkgRef=@MasterPNR) m
					On c.PaxLName=m.PaxLName and c.PaxFName=m.PaxFName and c.PaxType=m.PaxType
				Where m.PaxLName is Null)
			Select @sResult = 'P'								-- Pax information not match 
	END


	Select @sResultDesc = (Case When @sResult='C' Then 'Booking already closed' 
					When @sResult='F' Then 'Missing profile ID'
					When @sResult='R' Then 'Receipt not settled yet'
					When @sResult='G' Then 'Invalid booking GP'
					When @sResult='A' Then 'Access denied'
					When @sResult='Y' Then 'Booking close successfully'
					Else 'Booking close unsuccessfully'
				End)

	SELECT @sResult AS Result, @sResultDesc as ResultDesc
END





