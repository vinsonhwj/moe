﻿


CREATE PROCEDURE SP_FixPeonyRef
AS

BEGIN

SELECT         I.invnum,i.createon,
                              (SELECT         SUM((SellAmt+amtFee) * QTY * RMNTS)
                                FROM              PeoinvDetail INV
                                WHERE          I.invnum = INV.invNum
				 ) 
                          AS [InvSellAmt],
                              (SELECT         max(SellAmt)
                                FROM              peonyRef Ref
                                WHERE          I.invnum = Ref.docnum )
                          AS [RefSellAmt],
     (SELECT         SUM(sellExcludeTax)
                                FROM              peoinv Inv
                                WHERE          I.invnum = Inv.invnum )
                          AS [InvAmt]
INTO #TempPeonyRef
FROM              dbo.peoinv I
GROUP BY I.invNum, i.createon

--invnum, invSellAmt, RefSellamt


select * into #wrongPeonyRef from #TempPeonyRef
where InvAmt<>RefSellAmt and RefSellAmt<>0 


update peonyref
set sellamt = (select isnull(sellexcludeTax,0) as sellexcludeTax from peoinv 
 	                 where invnum in (select invnum from peoinv where invnum= peonyref.docnum and sellAmt is not null )  
		and invnum = peonyref.DocNum 
	)
where invTypeCode is not null 
and docnum in 
	(select invnum from #wrongPeonyRef)

/*
select refid, actionCode, [description] from peonyRef_NXT where docnum in
(select docnum from #wrongPeonyRef
where invTypeCode is not null)
*/
/*
select * from peonyref_NXT where docnum in 
(select docnum from #wrongPeonyRef
where invTypeCode is not null)


update peonyref_NXT 
set sellamt = (select isnull(sellexcludeTax,0) as sellexcludeTax from peoinv 
 	                 where invnum in (select invnum from peoinv where invnum= peonyref_NXT.docnum and sellAmt is not null )  
		and invnum = peonyref_NXT.DocNum 
	)
where invTypeCode is not null 
and docnum in 
	(select invnum from #wrongPeonyRef)
*/

DROP TABLE #TempPeonyRef
DROP TABLE #wrongPeonyRef

END


