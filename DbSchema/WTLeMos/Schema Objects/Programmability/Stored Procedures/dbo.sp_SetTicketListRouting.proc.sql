﻿CREATE PROCEDURE [dbo].[sp_SetTicketListRouting]

 @CompanyCode varchar(2),
 @Ticketlist varchar(100)
as
--set @CompanyCode = 'wm'
--set @Ticketlist = '7000000001|7000000002'

Declare @TargetCode varchar(10)

CREATE TABLE #TmpTicket (ticket varchar(10) )
While (@Ticketlist <> '')
Begin
	If left(@Ticketlist,1) = '|'
	Begin
		Select @Ticketlist = (Select Right(@Ticketlist, Len(@Ticketlist)-1))
	End
	Else
	Begin
		Select @TargetCode = (Select substring(@Ticketlist, 0, CharIndex('|', @Ticketlist)))
	
		If @TargetCode = ''
			Select @TargetCode=@Ticketlist

		INSERT INTO #TmpTicket VALUES (@TargetCode)

		If (Select CharIndex('|', @Ticketlist)) > 0
			Select @Ticketlist = (Select Right(@Ticketlist, Len(@Ticketlist) - Len(@TargetCode) - 1))
		Else
			Select @Ticketlist = (Select Right(@Ticketlist, Len(@Ticketlist) - Len(@TargetCode)))
		
	End
End

--select * from #TmpTicket

DECLARE @DepartCity CHAR(3), @ArrivalCity CHAR(3), 
        @TmpDes CHAR(3), @TmpStr varchar(100)

	--PRINT 'Ticket No:' + @Ticket + ', Ticket Seq = ' + @TicketSeq
	SET  @TmpStr = ''
	
	DECLARE ticketDetail CURSOR FOR
	SELECT DepartCity, ArrivalCity FROM peotktdetail (nolock)
	WHERE Ticket in (select ticket from #TmpTicket)  and companycode=@CompanyCode
	ORDER BY departdate
	
	OPEN ticketDetail
	
	FETCH NEXT FROM ticketDetail
	INTO @DepartCity, @ArrivalCity
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	--   Print 'Departure City:' + @DepartCity + ', Arrival City: ' + @ArrivalCity
	   IF @TmpStr = ''
	      BEGIN
	         SET @TmpStr = @DepartCity + '/' + @ArrivalCity
	      END
	   ELSE
	      BEGIN
	        IF rtrim(@TmpDes) = rtrim(@DepartCity)
	           BEGIN
	             SET @TmpStr = @TmpStr + '/' + @ArrivalCity
	           END
	        ELSE  
	           BEGIN
		     SET @TmpStr = @TmpStr + '-' + @DepartCity + '/' + @ArrivalCity
	           END
	        END
	
	
	   SET @TmpDes = @ArrivalCity
	   
	   -- This is executed as long as the previous fetch succeeds.
	   FETCH NEXT FROM ticketDetail
	   INTO @DepartCity, @ArrivalCity
	END
	
	--PRINT 'Routing = ' + @TmpStr
--        PRINT ''

	CLOSE ticketDetail
	DEALLOCATE ticketDetail

drop table #TmpTicket
	SELECT @TmpStr