﻿CREATE PROCEDURE [dbo].[sp_RetrieveXo]
@CompanyCode varchar(20),
@XoNum varchar(20)
AS
BEGIN

declare @BkgRef varchar(20)

select @BkgRef=BkgRef from Peoxo where CompanyCode=@CompanyCode and XONum=@XONum

select companycode from peomstr where companycode=@CompanyCode and BKGREF=@BkgRef

SELECT isnull(gsthotel, '0') as gsthotel, isnull(gstother, '0') as gstother, isnull(staffname, '') as staffname , 
isnull(stafftel, '') as stafftel , COMPANYCODE,XONUM,BKGREF,PNR,
ISNULL(INVNUM,'') AS INVNUM,ISNULL(SUPPCODE,'') AS SUPPCODE,
ISNULL(SUPPNAME,'') AS SUPPNAME,ISNULL(SUPPADDR,'') AS SUPPADDR,
ISNULL(SUPPTEL,'') AS SUPPTEL,ISNULL(SUPPFAX,'') AS SUPPFAX,
ISNULL(STAFFCODE,'') AS STAFFCODE,ISNULL(TEAMCODE,'') AS TEAMCODE,
ISNULL(AIRCODE,'') AS AIRCODE,isNull(DEPARTDATE,'') as Departdate,
ISNULL(FORMPAY,'') AS FORMPAY,ISNULL(COSTCURR,'') AS COSTCURR,
ISNULL(COSTAMT,0) AS COSTAMT,ISNULL(TAXCURR,'') AS TAXCURR,
ISNULL(TAXAMT,0) AS TAXAMT,ISNULL(HKDAMT,0) AS HKDAMT,
ISNULL(REMARK1,'-') AS REMARK1,ISNULL(REMARK2,'-') AS REMARK2,
ISNULL(CFMBY,'') AS CFMBY,ISNULL(XOREF,'') AS XOREF,
ISNULL(RMK1,'-') AS RMK1,ISNULL(RMK2,'-') AS RMK2,ISNULL(RMK3,'-') AS RMK3,
ISNULL(RMK4,'-') AS RMK4,ISNULL(RMK5,'-') AS RMK5,ISNULL(RMK6,'-') AS RMK6,
ISNULL(PRINTTEAM,'') AS PRINTTEAM,isnull(VOIDBY,'') as VOIDBY,isnull(VoidREASON,'') as VoidREASON , 
isnull(gstpct, '0') as gstpct, createon, createby, IsNULL(ATTN,'') as ATTN, IsNull(AirRmk, '') as AirRmk 
FROM PEOXO (NOLOCK) 
WHERE XONUM=@XoNum AND COMPANYCODE=@CompanyCode

SELECT  isNull(a.teamcode,'') as teamCode, isNull(Staffname,'') as staffname  FROM login a, peoxo b WHERE  (a.COMPANYCODE = b.companycode) AND (a.STAFFCODE = b.staffcode) and b.xonum=@XoNum

--select IsNull(TeamCode,'') TeamCode from customer where CompanyCode=@CompanyCode and CltCode in (select CltCode from PeoInv where CompanyCode=@CompanyCode and InvNum in (@XoNum))

select CompanyName,IsNull(ADDR1,'')+', '+IsNull(ADDR2,'')+', '+IsNull(ADDR3,'')+'<br> '+IsNull(ADDR4,'') as Address from company (nolock) where CompanyCode=@CompanyCode
select isnull(remark,'') as remark,  XONUM,SEGNUM,SERVTYPE,ISNULL(SERVNAME,'') AS SERVNAME,ISNULL(SERVDESC,'') AS SERVDESC  from PEOXOSEG where XONUM=@XoNum AND COMPANYCODE=@CompanyCode

--select IsNull(c.GSTPct,0) as GSTPct, IsNull(b.SegType,'') as SegType,b.GSTAmt, IsNull(a.ShowDetailFee,'N') as ShowDetailFee, IsNull(AmtFee,0) as AmtFee, 
--a.CompanyCode,a.InvNum,a.SegNum,a.SeqNum,a.ItemName,a.RateName,a.UnitPrice,IsNull(a.Qty,0) as Qty,IsNull(a.Rmnts,0) as Rmnts,IsNull(a.SellCurr,'') as SellCurr,IsNull(a.SellAmt,0) as 
--SellAmt,IsNull(a.TaxCurr,'') as TaxCurr,IsNull(a.TaxAmt,0) as TaxAmt, case when ShowDetailFee = 'N' then  a.AmtFee + a.SellAmt else a.SellAmt end as ShowAmt,a.HKDAmt,case when b.ServType='HTL' then 
--(IsNull(a.SellAmt,0)+IsNull(a.TaxAmt,0)) else (IsNull(a.SellAmt,0)+IsNull(a.TaxAmt,0))*IsNull(a.Qty,0) end as  TotalAmt, b.ServType from PeoInvdetail a,PeoInvseg b, PeoInv c where a.CompanyCode=b.CompanyCode 
--and b.CompanyCode=c.CompanyCode and a.InvNum=b.InvNum and b.InvNum=c.InvNum and a.SegNum=b.SegNum and  a.CompanyCode=@CompanyCode and a.InvNum in (@XoNum) order by a.SegNum,a.SeqNum 
--
--
--select IsNull(a.PaxLName,'') as PaxLName, IsNull(a.PaxFName,'') as PaxFName, IsNull(a.PaxType,'') as PaxType,a.PaxTitle, IsNull(b.Curr,'') as Curr, IsNull(b.Sell,0) as Sell from peopax a, peoinpax b, PeoInv c  where 
--c.CompanyCode=a.CompanyCode and a.CompanyCode=b.CompanyCode and c.BkgRef=a.BkgRef and c.InvNum=b.InvNum and a.paxnum=b.paxseq and b.InvNum=@XoNum and b.CompanyCode = @CompanyCode

--select RTRIM(peostaff.StaffName + '/' + login.TeamCode) AS [DisplayName], replace(IsNull(peostaff.Phone,''),'()','') as Phone,StaffName,Email from peostaff, login, PeoInv  where login.login=PeoInv.createby and 
--login.CompanyCode=PeoInv.CompanyCode and  login.StaffCode= peostaff.StaffCode and login.CompanyCode=peostaff.CompanyCode  and PeoInv.InvNum=@XoNum and PeoInv.CompanyCode=@CompanyCode 
--
--select IsNull(CR_Card_Holder,'') as CR_Card_Holder, IsNull(CR_Card_NO,'') as CR_Card_NO,IsNull(CR_Card_Type, 'AX') as CR_Card_Type, IsNull(CR_Card_Expiry,'') as CR_Card_Expiry, IsNull(CR_Amt,0) as CR_Amt, 
--IsNull(CR_Curr,'') as CR_Curr , IsNull(CR_Approval_Code, '') as CR_Approval_Code, IsNull(CR_TA, '') as CR_TA from PeoInv where InvNum=@XoNum and CompanyCode=@CompanyCode and CR_Card_NO<>''
--
--select isnull(a.aircode,'') as AirCode, isnull(b.ticket,'') as Ticket, case when isnull(b.TicketType,'') = 'MIS' then '*' else '' end as TicketType  from peotkt a, PEOINVTKTREF  b where a.ticket =b.ticket 
--and a.companycode = b.companycode  and a.companycode =@CompanyCode and b.invnum=@XoNum AND a.TktSeq = b.TktSeq  UNION select isnull(a.aircode,'') as aircode, isnull(b.ticket,'') as ticket, case when 
--isnull(b.TicketType,'') = 'MIS' then '*' else '' end as TicketType  from peomistkt a, PEOINVTKTREF  b where a.ticket =b.ticket and a.companycode = b.companycode  and a.companycode =@CompanyCode and 
--b.invnum=@XoNum AND a.TktSeq = b.TktSeq order by 2
--SELECT Description FROM PeonyRef WHERE DocNum = @XoNum AND CompanyCode = @CompanyCode AND RIGHT(Description, 10) = ' - printed' 
--
SELECT BkgRef,PNR,Crssinein,COD1,COD2,COD3,Deadline,Cltode as CltCode,CltName,CltAddr,StaffCode,TeamCode,FirstPax,Phone,Fax,Email,MasterPNR,TourCode,ContactPerson, 
 DepartDate,TtlSellCurr,TtlSellAmt,TtlSellTaxCurr,TtlSellTax,TtlCostCurr,TtlCostAmt,TtlCostTaxCurr,TtlCostTax,InvCurr,InvAmt,InvTaxCurr,InvTax, 
 DocCurr,DocAmt,DocTaxCurr,DocTax,TtlSellGST,TtlCostGST,TtlInvGST,TtlDocGST,InvCount,DocCount,Rmk1,Rmk2,Rmk3,Rmk4,SourceSystem,Source,SourceRef, 
 Pseudo,MstrRemark,BookingNo,Pseudo_Inv,Crssinein_Inv,Agency_Arc_Iata_Num,Iss_Off_Code,SalesPerson 
 FROM PeoMstr (NOLOCK) 
 WHERE CompanyCode = @CompanyCode 
 AND BkgRef = @BkgRef
--
--select a.CltCode, CONVERT (Char(11),DATEADD ("D",b.Crterms, a.CreateOn ),113) as DueDate, b.Crterms from PeoInv a(noLock), PeoCltCrlmt b  where a.InvNum=@XoNum and a.CltCode=b.CltCode and 
--a.CompanyCode = b.CompanyCode and b.CompanyCode=@CompanyCode
--
--SELECT Void2JBA FROM PeoInv WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef AND InvNum = @XoNum AND VoidOn IS NOT NULL AND VoidOn <> '' AND VoidBy IS NOT NULL AND VoidBy <> '' 
--
--select top 1 * from WP_eInvoice where InvNum = @XoNum and CompanyCode = @CompanyCode

select a.COMPANYCODE,a.XONUM,a.SEGNUM,a.SEQNUM,a.ItemName,a.RateName,isnull(a.QTY,0) as qty,isnull(a.rmnts,0) as rmnts,isnull(a.COSTCURR,'') as COSTcurr,isnull(a.COSTAMT,0) as COSTAMT,isnull(a.TAXCURR,'') as taxcurr,isnull(a.TAXAMT,0) as taxamt,a.HKDAMT,(isnull(a.COSTAMT,0)+isnull(a.taxAMT,0))*isnull(a.qty,0)*isnull(a.rmnts,0) as  TotalAMT,b.servtype FROM PEOXODETAIL a,PEOXOSEG b where a.companycode=b.companycode and a.XONUM=b.XONUM and a.segnum=b.segnum and  a.COMPANYCODE=@COMPANYCODE and a.XONUM=@XONUM order by a.segnum,a.seqnum 
SELECT ISNULL(PAXNAME,'') AS PAXNAME FROM PEOINPAX (NOLOCK) WHERE COMPANYCODE=@COMPANYCODE AND INVNUM=@XONum

 SELECT Description FROM PeonyRef WHERE DocNum = @XoNum AND CompanyCode = @CompanyCode AND RIGHT(Description, 10) = ' - printed' 

 SELECT VoidBy FROM PeoXO WITH(NOLOCK) WHERE CompanyCode = @CompanyCode AND BkgRef = @BkgRef AND XONum = @XONum AND VoidOn IS NOT NULL AND VoidOn <> '' AND VoidBy IS NOT NULL AND VoidBy <> '' 




END
--SELECT isnull(gsthotel, '0') as gsthotel, isnull(gstother, '0') as gstother, isnull(staffname, '') as staffname , isnull(stafftel, '') as stafftel , COMPANYCODE,XONUM,BKGREF,PNR,ISNULL(INVNUM,'') AS INVNUM,ISNULL(SUPPCODE,'') AS SUPPCODE,ISNULL(SUPPNAME,'') AS SUPPNAME,ISNULL(SUPPADDR,'') AS SUPPADDR,ISNULL(SUPPTEL,'') AS SUPPTEL,ISNULL(SUPPFAX,'') AS SUPPFAX,ISNULL(STAFFCODE,'') AS STAFFCODE,ISNULL(TEAMCODE,'') AS TEAMCODE,ISNULL(AIRCODE,'') AS AIRCODE,isNull(DEPARTDATE,'') as Departdate,ISNULL(FORMPAY,'') AS FORMPAY,ISNULL(COSTCURR,'') AS COSTCURR,ISNULL(COSTAMT,0) AS COSTAMT,ISNULL(TAXCURR,'') AS TAXCURR,ISNULL(TAXAMT,0) AS TAXAMT,ISNULL(HKDAMT,0) AS HKDAMT,ISNULL(REMARK1,'-') AS REMARK1,ISNULL(REMARK2,'-') AS REMARK2,ISNULL(CFMBY,'') AS CFMBY,ISNULL(XOREF,'') AS XOREF,ISNULL(RMK1,'-') AS RMK1,ISNULL(RMK2,'-') AS RMK2,ISNULL(RMK3,'-') AS RMK3,ISNULL(RMK4,'-') AS RMK4,ISNULL(RMK5,'-') AS RMK5,ISNULL(RMK6,'-') AS RMK6,ISNULL(PRINTTEAM,'') AS PRINTTEAM,isnull(VOIDBY,'') as VOIDBY,isnull(VoidREASON,'') as VoidREASON , isnull(gstpct, '0') as gstpct, createon, createby, IsNULL(ATTN,'') as ATTN, IsNull(AirRmk, '') as AirRmk FROM PEOXO (NOLOCK) WHERE XONUM=@XoNum AND COMPANYCODE=N'WM'