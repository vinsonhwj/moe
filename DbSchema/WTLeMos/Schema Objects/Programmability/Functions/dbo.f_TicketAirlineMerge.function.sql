﻿create function [dbo].[f_TicketAirlineMerge](@companycode varchar(2), @ticket varchar(10), @tktseq varchar(2))   
  returns varchar(8000)   
  as 
  begin 
      declare @str varchar(8000)   
      set @str=''
		select @str =@str+Airline+'/' from dbo.peotktdetail where companycode = @companycode and ticket =@ticket and tktseq = @tktseq      
      set @str=left(@str,len(@str)-1)   
      return(@str)   
  End 