﻿
CREATE function [dbo].[f_TicketClassMerge](@companycode varchar(2), @ticket varchar(10), @tktseq varchar(2))   
  returns varchar(50)   
  as 
  begin 
      declare @str varchar(50)   
      set @str=''
	  select @str =@str+class+'/' from dbo.peotktdetail where companycode = @companycode and ticket =@ticket and tktseq = @tktseq      
      set @str=left(@str,len(@str)-1)   
      return(@str)   
  End 