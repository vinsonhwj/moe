﻿CREATE FUNCTION dbo.f_SplitPeoinvPax
(
 @InPaxStr NVARCHAR(2000)
, @Field int 
)
RETURNS nvarchar(100)
AS
BEGIN
 DECLARE @PaxStr NVARCHAR(2000);
 DECLARE @TargetCode NVARCHAR(100);
 DECLARE @ChrInx INT;
 DECLARE @Position int;
 DECLARE @RtnValues nvarchar(100);
 
 SET @PaxStr = @InPaxStr;
 SET @Position = 1
 
 WHILE (@PaxStr <> '')
 BEGIN
  SET @ChrInx = CHARINDEX('/', @PaxStr);
  
  IF (@ChrInx = 0)
  BEGIN
   SET @TargetCode = LTRIM(RTRIM(@PaxStr));
   SET @PaxStr = '';
  END
  ELSE
  BEGIN
   SET @TargetCode = SUBSTRING(@PaxStr, 1, @ChrInx - 1);
   SET @TargetCode = LTRIM(RTRIM(@TargetCode));
   SET @PaxStr = SUBSTRING(@PaxStr, @ChrInx + 1, LEN(@PaxStr));
  END
  
  IF @Position = @Field
  BEGIN
   SET @RtnValues = @TargetCode
  END
  SET @Position = @Position +1
  
  
  /*
  IF (@TargetCode <> '' AND LEN(@TargetCode) < 9)
  BEGIN
   IF NOT EXISTS (
    SELECT NULL
    FROM @retClientTbl
    WHERE ClientCode = @TargetCode
   )
   BEGIN
    INSERT INTO @retClientTbl VALUES (@TargetCode);
   END
  END*/
 END
 
 RETURN @RtnValues
END