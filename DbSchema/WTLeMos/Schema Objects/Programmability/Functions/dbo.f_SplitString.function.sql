﻿
CREATE FUNCTION [dbo].[f_SplitString]
(
	@src VARCHAR(4000),
	@delim CHAR(1)
)
RETURNS @result TABLE
(
	[value] VARCHAR(100)
)
AS
BEGIN
	DECLARE @idx INT;
	DECLARE @tmp VARCHAR(100);
	
	WHILE (LEN(@src) > 0)
	BEGIN
		SELECT @idx = CHARINDEX(@delim, @src);
		
		IF (@idx > 0)
		BEGIN
			INSERT INTO @result
			VALUES (SUBSTRING(@src, 1, @idx - 1));
		
			SELECT @src = SUBSTRING(@src, @idx + 1, LEN(@src));
		END
		ELSE
		BEGIN
			INSERT INTO @result
			VALUES (@src);
		
			SELECT @src = '';
		END
	END

	RETURN;
END
