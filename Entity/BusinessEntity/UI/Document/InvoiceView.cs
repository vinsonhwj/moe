﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Element;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class InvoiceView
    {
        public InvViewInfo Info { get; set; }

        public InvViewPaxs PassengerList { get; set; }

        public List<InvViewTicket> TicketList { get; set; }

        public InvViewAirSegments AirList { get; set; }

        public InvViewHtlSegments HtlList { get; set; }

        public InvViewOthSegments OtherList { get; set; }

        public List<InvViewCreditCard> CreditCardList { get; set; }

        public List<InvViewCombine> CombineInv { get; set; }
    }
}
