﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class XoSaveInput
    {
        public XoPreviewInput PreviewInput { get; set; }
        public XoPreview Preview { get; set; }
        public string ProfitMarginReason { get; set; }
        public bool IsCopy { get; set; }
        public string noofCopy { get; set; }
        public string Email { get; set; }

    }
}
