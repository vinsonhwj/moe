﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.UI.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class VCHPreview
    {
        #region Information
        public string CompanyCode { get; set; }
        public string BookingNum { get; set; }
        public string Payment { get; set; }
        public string CltCode { get; set; }
        //public string SuppCode { get; set; }
        //public string SuppName { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        //public string HtlRemark1 { get; set; }
        //public string HtlRemark2 { get; set; }

        //public string HtlAddress { get; set; }
        //public string HtlAddress_CN { get; set; }
        //public string HtlAddress_TW { get; set; }
        //public string HtlName { get; set; }
        //public string HtlName_CN { get; set; }
        //public string HtlName_TW { get; set; }
        //public string HtlTelephone { get; set; }
        //public string HtlCountryCode { get; set; }
        //public string HtlCfmBy { get; set; }

        //public string HtlETA { get; set; }
        //public DateTime HtlArrDate { get; set; }
        //public string HtlArrTime { get; set; }
        //public string HtlETD { get; set; }
        //public DateTime HtlDapartDate { get; set; }
        //public string HtlDapartTime { get; set; }

        //public string HtlReference { get; set; }
        //public string HtlSpecialRequest { get; set; }
        //public string HtlWithBreakfast { get; set; }
        //public string HtlWithTransfer { get; set; }
        //public string HtlRateName { get; set; }

        public sqlVchHotels HtlServiceDetails { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal ExcludeTaxAmount { get; set; }
        public decimal TaxAmount { get; set; }

        public bool ChangePaxCount { get; set; }
        public bool PassPMQ { get; set; }
        public string PreviewMD5 { get; set; }
        #endregion

        public WSResult Result { get; set; }

        public List<Element.VCHPassenger> Passengers { get; set; }
        public List<Element.VCHHotel> Hotels { get; set; }
        public List<Element.VCHOther> Others { get; set; }

        //public sqlVchHotels Hotels { get; set; }

        //public List<Element.VchPreviewHotelDetail> HotelList { get; set; }

        //public List<Element.VchPreviewOther> OtherWaiList { get; set; }

        //public List<Element.VchPreviewOtherDetail> OtherList { get; set; }

        public VCHPreview()
        {
            Result = new WSResult();
            Hotels = new List<VCHHotel>();
            Others = new List<VCHOther>();
        }
    }
}
