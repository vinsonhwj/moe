﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Voucher
{
    public class VCHCreate
    {
        public VCHSelected SelectedKey { get; set; }
        public string PMQReason { get; set; }
        public string PreviewMD5 { get; set; }
        public string AirRemark1 { get; set; }
        public string AirRemark2 { get; set; }
        public string HotelRemark1 { get; set; }
        public string HotelRemark2 { get; set; }
        public string OtherRemark1 { get; set; }
        public string OtherRemark2 { get; set; }

        public VCHCreate()
        {

        }
    }
}
