﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewPax
    {
        public String PAXLNAME { get; set; }
        public String PAXFNAME { get; set; }
        public String PAXTITLE { get; set; }
        public String PAXagetype { get; set; }
        public String PAXageAir { get; set; }
        public String PAXageOther { get; set; }
    }
    public class InvViewPaxs : List<InvViewPax>
    { }
}
