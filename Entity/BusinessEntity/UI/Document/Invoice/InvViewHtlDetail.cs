﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewHtlDetail
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }

        public Decimal GstPct { get; set; }
        public Decimal GstAmt { get; set; }
        public String ItemName { get; set; }
        public Decimal Qty { get; set; }
        public Decimal Rmnts { get; set; }
        public String SellCurr { get; set; }
        public Decimal SellAmt { get; set; }
        public Decimal TaxAmt { get; set; }
        public Decimal TotalAmt { get; set; }
    }
    public class InvViewHtlDetails : List<InvViewHtlDetail>
    { }
}
