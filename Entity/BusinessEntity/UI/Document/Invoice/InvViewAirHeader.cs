﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewAirHeader
    {
        public Int32 GroupId { get; set; }
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }

        public string Airline { get; set; }
        public string Flight { get; set; }
        public string AirClass { get; set; }
        public string DepartDate { get; set; }
        public string DepartCity { get; set; }
        public string ArrivalCity { get; set; }
    }
    //public class InvViewAirHeaders : List<InvViewAirHeader>
    //{ }
}
