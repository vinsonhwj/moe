﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewCreditCard
    {
        public string CardType { get; set; }
        public string CardNum { get; set; }
        public DateTime Expiry_dte { get; set; }
        public string CardHolder { get; set; } 
    }
}
