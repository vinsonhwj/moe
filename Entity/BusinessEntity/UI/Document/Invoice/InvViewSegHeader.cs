﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewSegHeader
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }
        public String SERVTYPE { get; set; }
        public String segType { get; set; }
        public String SERVNAME { get; set; }
        public String SERVDESC { get; set; }
        public String WithBkf { get; set; }
        public String WithTfr { get; set; }
        public Int32 GroupId { get; set; }
        
        public string Ext { get; set; }

    }
}
