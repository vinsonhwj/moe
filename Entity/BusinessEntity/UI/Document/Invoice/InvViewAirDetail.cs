﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewAirDetail
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }

        public String ItemName { get; set; }
        public Decimal Qty { get; set; }
        public String ShowDetailFee { get; set; }
        public Decimal AmtFee { get; set; }
        public String SellCurr { get; set; }
        public Decimal SellAmt { get; set; }
        public String TaxCurr { get; set; }
        public Decimal TaxAmt { get; set; }
    }
    public class InvViewAirDetails : List<InvViewAirDetail>
    { }
}
