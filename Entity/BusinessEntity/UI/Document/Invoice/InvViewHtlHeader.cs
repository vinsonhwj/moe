﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewHtlHeader
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }

        public string ServName { get; set; }

        public string WithBkfAndTfr { get; set; }

        public bool WithBkfAndTfrVisible { get; set; }
    }
}
