﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewHtlSegment
    {
        public InvViewHtlHeader Header { get; set; }

        public InvViewHtlDetails Details { get; set; }

    }
    public class InvViewHtlSegments : List<InvViewHtlSegment>
    { }
}
