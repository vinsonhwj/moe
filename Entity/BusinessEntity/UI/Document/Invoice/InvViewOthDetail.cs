﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewOthDetail
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }

        public Decimal gstPct { get; set; }
        public Decimal gstamt { get; set; }
        public String ItemName { get; set; }
        public Decimal qty { get; set; }
        public String sellcurr { get; set; }
        public Decimal SELLAMT { get; set; }
        public Decimal taxamt { get; set; }
    }
    public class InvViewOthDetails : List<InvViewOthDetail>
    {
    }
}
