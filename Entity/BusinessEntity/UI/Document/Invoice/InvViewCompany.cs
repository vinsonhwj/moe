﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewCompany
    {
        public String COMPANYCODE { get; set; }
        public String COMPANYNAME { get; set; }
        public String ADDR1 { get; set; }
        public String ADDR2 { get; set; }
        public String ADDR3 { get; set; }
        public String ADDR4 { get; set; }
    }
}
