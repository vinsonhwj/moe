﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewAirSegment
    {
        public InvViewAirHeader Header { get; set; }

        public InvViewAirDetails Details { get; set; }

    }
    public class InvViewAirSegments : List<InvViewAirSegment>
    { }
}
