﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewTicket
    {
        public string AirCode { get; set; }

        public string PaxName { get; set; }

        public string Ticket { get; set; }
    }
}
