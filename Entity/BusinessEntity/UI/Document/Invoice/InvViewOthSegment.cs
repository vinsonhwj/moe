﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewOthSegment
    {
         public InvViewOthHeader Header { get; set; }

        //public String INVNUM { get; set; }
        //public String SEGNUM { get; set; }

        //public string ServDesc { get; set; }

        //private int _DetailCount;
        //public int DetailCount
        //{
        //    get { return Details == null ? 0 : Details.Count; }
        //}

        public InvViewOthDetails Details { get; set; }
    }
    public class InvViewOthSegments : List<InvViewOthSegment>
    { }
}
