﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Invoice
{
    public class InvViewOthHeader
    {
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }

        public string ServDesc { get; set; }
    }
}
