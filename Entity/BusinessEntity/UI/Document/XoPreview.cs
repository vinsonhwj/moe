﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class XoPreview
    {
        #region Infomation
        public string CompanyCode { get; set; }
        public string BookingNum { get; set; }

        public string ClientName { get; set; } //lb_clientname_title
        public string ClientCode { get; set; } //lb_clientcode_title
        public string Address1 { get; set; } //lb_address1_title
        public string Address2 { get; set; } //lb_address2_title
        public string Address3 { get; set; } //lb_address3_title
        public string Address4 { get; set; } //lb_address4_title
        public string Attn { get; set; } //lb_attn1_title
        public string Ref { get; set; } //lb_ref_title_tel
        public string Consultant { get; set; } //lb_attn1_title
        public string PrintedDate { get; set; } //lb_printeddate_title
        public string Tel { get; set; } //lb_tel_title
        public string Currency { get; set; } //lb_currhead_title
        public string Fax { get; set; } //lb_fax_title
        public string Phone { get; set; }
        public string FormofPayment { get; set; }
        //public string OtherRemark1 { get; set; } //lb_otherremark1_show
        //public string OthereRemark2 { get; set; } //lb_otherremark2_show
        //public string OtherDefaultCurr { get; set; } //lb_other_defaultcurr
        public string DefaultCurr { get; set; }
        public decimal AirTotalAmount { get; set; } //lb_Totalamount6_vle
        public string AirCurr { get; set; }
        public decimal HtlTotalAmount { get; set; }
        public string HtlCurr { get; set; }
        public decimal OthTotalAmount { get; set; }
        public string OthCurr { get; set; }
        public string GrandTotalCurr { get; set; }
        public decimal GrandTotalAmount { get; set; }

        #endregion

        public Element.XoPreviewMsg PreviewMsg { get; set; }

        public List<Element.XoPreviewPax> PassengerList { get; set; }

        public List<Element.XoPreviewAir> AirList { get; set; }

        public List<Element.XoPreviewHotel> HotelList { get; set; }

        public List<Element.XoPreviewOther> OtherList { get; set; }

    }
}
