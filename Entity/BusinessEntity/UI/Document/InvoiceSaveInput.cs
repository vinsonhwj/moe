﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class InvoiceSaveInput : DALEntity.Document.InvoiceUIForSave
    {
        InvoicePreviewInput _PreviewInput;
        public new InvoicePreviewInput PreviewInput
        {
            get
            {
                return _PreviewInput;
            }
            set
            {
                _PreviewInput = value;
                base.PreviewInput = (InvoiceUIForPreview)value;
            }
        }
        public InvoicePreview Preview { get; set; }

    }
}
