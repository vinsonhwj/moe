﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VchPreviewHotelHeader
    {
        public String hotelcode { get; set; }
        public String SEGNUM { get; set; }
        public String COUNTRYCODE { get; set; }
        public String CITYCODE { get; set; }
        public DateTime ARRDATE { get; set; }
        public String ARRTIME { get; set; }
        public String eta { get; set; }
        public DateTime departdate { get; set; }
        public String departtime { get; set; }
        public String etd { get; set; }
        public String rmk1 { get; set; }
        public String rmk2 { get; set; }
        public String spcl_request { get; set; }
        public String HTLREFERENCE { get; set; }
        public String HTLCFMBY { get; set; }
        public String hotelname { get; set; }
        public String newcountryname { get; set; }
        public String newcityname { get; set; }

        public VchPreviewHotelHeader()
        {

        }
        public VchPreviewHotelHeader(sqlVchHotel htl)
        {
            this.hotelcode = htl.hotelcode;
            this.SEGNUM = htl.SEGNUM;
            this.COUNTRYCODE = htl.COUNTRYCODE;
            this.CITYCODE = htl.CITYCODE;
            this.ARRDATE = htl.ARRDATE;
            this.ARRTIME = htl.ARRTIME;
            this.eta = htl.eta;
            this.departdate = htl.departdate;
            this.departtime = htl.departtime;
            this.etd = htl.etd;
            this.rmk1 = htl.rmk1;
            this.rmk2 = htl.rmk2;
            this.spcl_request = htl.spcl_request;
            this.HTLREFERENCE = htl.HTLREFERENCE;
            this.HTLCFMBY = htl.HTLCFMBY;
            this.hotelname = htl.hotelname;
            this.newcountryname = htl.newcountryname;
            this.newcityname = htl.newcityname;
        }

    }
}
