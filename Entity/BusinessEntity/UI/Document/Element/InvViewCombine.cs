﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvViewCombine
    {
        public string InvNum { get; set; }
        public string CardType { get; set; }
        public string CardNum { get; set; }
        public string AccessStr { get; set; }
        public DateTime CardExpiry { get; set; }
        public string CardHolder { get; set; }
        public decimal CardAmt { get; set; }
    }
}
