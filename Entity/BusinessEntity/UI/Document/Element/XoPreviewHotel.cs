﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity.Document.Element;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewHotel
    {
        public XoPreviewHotelHeader Header { get; set; }
        public List<XoPreviewHotelDetail> Detail { get; set; }

        public XoPreviewHotel()
        { }

      
        public XoPreviewHotel(sqlXoHotel hotel, sqlXoHotelDetails HotelDetails)
        {
            this.Header = new XoPreviewHotelHeader(hotel);
            this.Detail = XoPreviewHotelDetail.Parse(HotelDetails);
        }

        public static List<XoPreviewHotel> Parse(sqlXoHotels hotels, sqlXoHotelDetails HotelDetails)
        {
            List<XoPreviewHotel> xoHotel = new List<XoPreviewHotel>();
            if (hotels != null && HotelDetails !=null)
            {
                foreach (sqlXoHotel h in hotels)
                {
                    sqlXoHotelDetails htlDetails = new sqlXoHotelDetails();
                    foreach (sqlXoHotelDetail d in htlDetails)
                    {
                        
                        htlDetails.Add(d);
                    }
                    if (h != null && htlDetails.Count > 0)
                    {
                        xoHotel.Add(new XoPreviewHotel(h, htlDetails));
                    }
                }
            }

            return xoHotel;
        }
    }
}
