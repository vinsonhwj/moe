﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewHotelHeader
    {
        //public string BkgRef { get; set; }
        public string SegNum { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string NewCityName { get; set; }
        public string NewCountryName { get; set; }
        public DateTime ArrDate { get; set; }
        public DateTime DepartDate { get; set; }

        public XoPreviewHotelHeader()
        { }

        public XoPreviewHotelHeader(sqlXoHotel htl)
        {
            this.HotelCode = htl.hotelcode;
            this.HotelName = htl.HotelName;
            this.NewCityName = htl.newcityname;
            this.NewCountryName = htl.newcountryname;
            this.ArrDate = htl.ARRDATE;
            this.DepartDate = htl.departdate;
        }
    }
}
