﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewAir
    {
        public InvPreviewAirHeader Header { get; set; }

        public List<InvPreviewAirDetail> Detail { get; set; }

        public InvPreviewAir()
        { }
        public InvPreviewAir(tbPEOAIR air, sqlInvAirDetails airDetails)
        {
            this.Header = new InvPreviewAirHeader(air);
            this.Detail =InvPreviewAirDetail.Parse(airDetails);
        }

        public static List<InvPreviewAir> Parse(tbPEOAIRs airs, sqlInvAirDetails airDetails)
        {
            List<InvPreviewAir> invAir = new List<InvPreviewAir>();
            if (airs != null && airDetails != null)
            {
                foreach (tbPEOAIR a in airs)
                {
                    sqlInvAirDetails dlst = new sqlInvAirDetails();
                    foreach (sqlInvAirDetail d in airDetails)
                    {
                        if (d.COMPANYCODE == a.CompanyCode && d.BKGREF == a.BkgRef && d.segnum == a.SegNum)
                        {
                            dlst.Add(d);
                        }
                    }
                    if (a != null && dlst.Count > 0)
                    {
                        invAir.Add(new InvPreviewAir(a, dlst));
                    }
                }
            }

            return invAir;
        }

    }
}
