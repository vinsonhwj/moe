﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VchPreviewOtherHeader
    {
        public String SupplierCode { get; set; }
        public String BkgRef { get; set; }
        public String CompanyCode { get; set; }
        public String SegNum { get; set; }
        public String SegType { get; set; }
        public DateTime OtherDate { get; set; }
        public String MasterDesc { get; set; }
        public String Descript1 { get; set; }
        public String Descript2 { get; set; }
        public String Descript3 { get; set; }
        public String Descript4 { get; set; }

        public VchPreviewOtherHeader()
        { }
        public VchPreviewOtherHeader(tbPEOOTHER other)
        {
            this.SupplierCode = other.suppliercode;
            this.BkgRef = other.BKGREF;
            this.CompanyCode = other.COMPANYCODE;
            this.SegNum = other.SEGNUM;
            this.SegType = other.SEGTYPE;
            this.OtherDate = other.OTHERDATE;
            this.MasterDesc = other.masterDesc;
            this.Descript1 = other.DESCRIPT1;
            this.Descript2 = other.DESCRIPT2;
            this.Descript3 = other.DESCRIPT3;
            this.Descript4 = other.DESCRIPT4;
        }
    }
}
