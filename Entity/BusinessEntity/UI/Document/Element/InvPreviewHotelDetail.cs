﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewHotelDetail
    {
        public decimal GSTTaxRate { get; set; }
        public string GSTTax { get; set; }
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SegNum { get; set; }
        public string SeqNum { get; set; }
        public string SeqType { get; set; }
        public decimal RMNTS { get; set; }
        public DateTime ArrDate { get; set; }
        public DateTime DepartDate { get; set; }
        public int Qty { get; set; }
        public string RateName { get; set; }
        public string RoomType { get; set; }
        public string SellCurr { get; set; }
        public decimal SellAmt { get; set; }
        public string TaxCurr { get; set; }
        public decimal TaxAmt { get; set; }
        public string ItemType { get; set; }

        public InvPreviewHotelDetail()
        { }
        public InvPreviewHotelDetail(sqlInvHotelDetail detail)
        {
            this.GSTTaxRate = detail.GSTTAXRate;
            this.GSTTax = detail.GSTTAX;
            this.CompanyCode = detail.coMPANYCODE;
            this.BkgRef = detail.BKGREF;
            this.SegNum = detail.SEGNUM;
            this.SeqNum = detail.SEQNUM;
            this.SeqType = detail.SEQTYPE;
            this.RMNTS = detail.RMNTS;
            this.ArrDate = detail.ARRDATE;
            this.DepartDate = detail.DEPARTDATE;
            this.Qty = detail.QTY;
            this.RateName = detail.RATENAME;
            this.RoomType = detail.ROOMTYPE;
            this.SellCurr = detail.SELLCURR;
            this.SellAmt = detail.SELLAMT;
            this.TaxCurr = detail.TAXCURR;
            this.TaxAmt = detail.TAXAMT;
            this.ItemType = detail.itemtype;
        }
        internal static List<InvPreviewHotelDetail> Parse(sqlInvHotelDetails details)
        {
            List<InvPreviewHotelDetail> invHtlDetails = new List<InvPreviewHotelDetail>();

            if (details != null)
            {
                foreach (sqlInvHotelDetail d in details)
                {
                    invHtlDetails.Add(new InvPreviewHotelDetail(d));
                }
            }
            return invHtlDetails;
        }
    }


}
