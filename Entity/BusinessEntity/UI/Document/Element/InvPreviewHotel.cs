﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.BLLEntity;
using Westminster.MOE.Entity.DALEntity.Document.Element;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewHotel
    {
        public InvPreviewHotelHeader Header { get; set; }
        public List<InvPreviewHotelDetail> Detail { get; set; }

        public InvPreviewHotel()
        { }
        public InvPreviewHotel(InvPreviewHtl htlinfo)
        {
            if (htlinfo != null)
            {
                this.Header = new InvPreviewHotelHeader(htlinfo.Header);
                this.Detail = InvPreviewHotelDetail.Parse(htlinfo.Details);
            }
        }
        public InvPreviewHotel(sqlInvHotel htl, sqlInvHotelDetails details)
        {
            this.Header = new InvPreviewHotelHeader(htl);
            this.Detail = InvPreviewHotelDetail.Parse(details);
        }

        public static List<InvPreviewHotel> Parse(List<InvPreviewHtl> hotels)
        {
            List<InvPreviewHotel> invHtl = new List<InvPreviewHotel>();
            if (hotels != null)
            {
                foreach (InvPreviewHtl a in hotels)
                {
                    invHtl.Add(new InvPreviewHotel(a));
                }
            }
            return invHtl;
        }
    }
}
