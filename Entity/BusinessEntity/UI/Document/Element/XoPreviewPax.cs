﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
   public  class XoPreviewPax
    {
       public string BkgRef { get; set; }
       public string CompanyCode { get; set; }
       public string SegNum { get; set; }
       public string PaxNum { get; set; }
       public string PaxLName { get; set; }
       public string PaxFName { get; set; }
       public string PaxTitle { get; set; }
       public string PaxType { get; set; }
       public string PaxAir { get; set; }
       public string PaxOther { get; set; }

       public XoPreviewPax()
       { 
       }

       public XoPreviewPax(tbPEOPAX pax)
       {
           this.BkgRef = pax.BKGREF;
           this.CompanyCode = pax.COMPANYCODE;
           this.SegNum = pax.SEGNUM;
           this.PaxNum = pax.PAXNUM;
           this.PaxLName = pax.PAXLNAME;
           this.PaxFName = pax.PAXFNAME;
           this.PaxTitle = pax.PAXTITLE;
           this.PaxType = pax.PAXTYPE;
           this.PaxAir = pax.PAXAIR;
           this.PaxOther = pax.PAXOTHER;
 
       }

       public static List<Element.XoPreviewPax> Parse(tbPEOPAXs paxs)
       {
           List<Element.XoPreviewPax> xoPaxs = new List<XoPreviewPax>();
           if (paxs != null)
           {
               foreach (tbPEOPAX p in paxs)
               {
                   xoPaxs.Add(new XoPreviewPax(p));
               }
           }
           return xoPaxs;
       }

    }
}
