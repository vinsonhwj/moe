﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewOther
    {
        public InvPreviewOtherHeader Header { get; set; }
        public List<InvPreviewOtherDetail> Detail { get; set; }

        public InvPreviewOther() { }
        public InvPreviewOther(sqlInvOther other, sqlInvOtherDetails details)
        {
            this.Header = new InvPreviewOtherHeader(other);
            this.Detail = InvPreviewOtherDetail.Parse(details);
        }

        public static List<Element.InvPreviewOther> Parse(sqlInvOthers others, sqlInvOtherDetails otherDetails)
        {
            List<Element.InvPreviewOther> list = new List<InvPreviewOther>();
            if (others != null && otherDetails != null)
            {
                foreach (sqlInvOther o in others)
                {
                    sqlInvOtherDetails dlst = new sqlInvOtherDetails();
                    foreach (sqlInvOtherDetail d in otherDetails)
                    {
                        if (d.COMPANYCODE == o.COMPANYCODE && d.BKGREF == o.BKGREF && d.segnum == o.SEGNUM)
                        {
                            dlst.Add(d);
                        }
                    }
                    list.Add(new InvPreviewOther(o, dlst));
                }
            }
            return list;
        }
    }
}
