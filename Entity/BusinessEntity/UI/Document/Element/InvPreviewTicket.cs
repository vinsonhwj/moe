﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewTicket
    {
        public string AirCode { get; set; }
        public string TicketType { get; set; }
        public string TicketNo { get; set; }

        public InvPreviewTicket()
        { }
        public InvPreviewTicket(sqlInvTicket tkt)
        {
            this.AirCode = tkt.AirCode;
            this.TicketType = tkt.TicketType;
            this.TicketNo = tkt.ticket;
        }

        public static List<Element.InvPreviewTicket> Parse(sqlInvTickets tktList)
        {
            List<Element.InvPreviewTicket> list = new List<InvPreviewTicket>();

            if (tktList != null)
            {
                foreach (sqlInvTicket t in tktList)
                {
                    list.Add(new InvPreviewTicket(t));
                }
            }
            return list;
        }
    }
}
