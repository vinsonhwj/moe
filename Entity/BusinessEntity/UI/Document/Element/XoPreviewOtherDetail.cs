﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
   public  class XoPreviewOtherDetail
    {
       public string AgeType { get; set; }
       public string Curr { get; set; }
       public decimal Amt { get; set; }
       public decimal TaxAmt { get; set; }
       public int QTY { get; set; }
      
       public XoPreviewOtherDetail()
       { }

       public XoPreviewOtherDetail(sqlXoOtherDetail detail)
       {
           this.AgeType = detail.agetype;
           this.Curr = detail.curr;
           this.Amt = detail.amt;
           this.TaxAmt = detail.taxamt;
           this.QTY = detail.QTY;
       }
       public static List<XoPreviewOtherDetail> Parse(sqlXoOtherDetails details)
       {
           List<XoPreviewOtherDetail> xoOthDetails = new List<XoPreviewOtherDetail>();
           if (details != null)
           {
               foreach (sqlXoOtherDetail d in details)
               {
                   xoOthDetails.Add(new XoPreviewOtherDetail(d));
               }
           }
           return xoOthDetails;
       }
    }
}
