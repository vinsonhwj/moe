﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
   public class XoPreviewOther
    {
       public XoPreviewOtherHeader Header { get; set; }
       public List<XoPreviewOtherDetail> Detail { get; set; }

       public XoPreviewOther()
       { }
       public XoPreviewOther(tbPEOOTHER other, sqlXoOtherDetails othDetails)
       {
           this.Header = new XoPreviewOtherHeader(other);
           this.Detail = XoPreviewOtherDetail.Parse(othDetails);
       }
       public static List<XoPreviewOther> Parse(tbPEOOTHERs others, sqlXoOtherDetails othDetails)
       {
           List<XoPreviewOther> xoOther = new List<XoPreviewOther>();
           if (others != null && othDetails != null)
           {
               foreach (tbPEOOTHER oth in others)
               {
                   sqlXoOtherDetails dlst = new sqlXoOtherDetails();
                   foreach (sqlXoOtherDetail d in othDetails)
                   {
                       if (d.COMPANYCODE == oth.COMPANYCODE && d.BKGREF == oth.BKGREF && d.segnum == oth.SEGNUM)
                       {
                           dlst.Add(d);
                       }
                   }
                   if (oth != null && dlst.Count > 0)
                   {
                       xoOther.Add(new XoPreviewOther(oth, dlst));
                   }
               }
           }
           return xoOther;
       }
    }
}
