﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvViewInfo
    {
        #region PeoInv


        public String InvFormat { get; set; }
        public String void2JBA { get; set; }
        public String COMPANYCODE { get; set; }
        public String BKGREF { get; set; }
        public String INVNUM { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String RMK3 { get; set; }
        public String RMK4 { get; set; }
        public String RMK5 { get; set; }
        public String RMK6 { get; set; }
        public String CLTCODE { get; set; }
        public String CLTNAME { get; set; }
        public String CLTADDR1 { get; set; }
        public String CLTADDR2 { get; set; }
        public String CLTADDR3 { get; set; }
        public String CLTADDR4 { get; set; }
        public String PHONE { get; set; }
        public String FAX { get; set; }
        public String CUSTREF { get; set; }
        public String invtype { get; set; }
        public String analysiscode { get; set; }
        public String ATTN { get; set; }
        public String voidby { get; set; }
        public String voidreason { get; set; }
        public DateTime chq_dte { get; set; }
        public String payer { get; set; }
        public String bankcode { get; set; }
        public String pay_rmk { get; set; }
        public String chqNo { get; set; }
        public DateTime createOn { get; set; }
        public String createBy { get; set; }
        public DateTime updateon { get; set; }
        public String PAYDESC { get; set; }
        public String sellCurr { get; set; }
        public Decimal sellamt { get; set; }
        public String invdtcurr { get; set; }
        public Decimal invdtsum { get; set; }
        public String CNReason { get; set; }
        public String InvTC { get; set; }
        public String TeamCode { get; set; }
        public String IsPackage { get; set; }
        public String PackageDesc1 { get; set; }
        public String PackageDesc2 { get; set; }
        public String IsMICE { get; set; }
        public String MICEDesc { get; set; }
        public String IsBkgClass { get; set; }
        public decimal GstAmt { get; set; }
        public decimal GstPct { get; set; }
        public DateTime DueDay { get; set; }
        #endregion

        #region Other
        public string Consultant { get; set; }// lb_BookingHolder
        public string StaffPhone { get; set; }// lb_did_vle
        public string PrintedBy { get; set; }
        public string IssuedBy { get; set; }//lb_Consultant_title
        public string InvTypeCode { get; set; }
        public String InvoiceTitle { get; set; }//lb_INVOICE
        #endregion
    }
}
