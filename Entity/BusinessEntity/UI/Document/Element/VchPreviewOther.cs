﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VchPreviewOther
    {
        public VchPreviewOtherHeader Header { get; set; }
        public List<VchPreviewOtherDetail> Detail { get; set; }

        public VchPreviewOther() { }
        public VchPreviewOther(tbPEOOTHER other, sqlVchOtherDetails details)
        {
            this.Header = new VchPreviewOtherHeader(other);
            this.Detail = VchPreviewOtherDetail.Parse(details);
        }

        public static List<Element.VchPreviewOther> Parse(tbPEOOTHERs others, sqlVchOtherDetails otherdetails)
        {
            List<Element.VchPreviewOther> list = new List<VchPreviewOther>();
            if (others != null && otherdetails != null)
            {
                foreach (tbPEOOTHER o in others)
                {
                    sqlVchOtherDetails dlst = new sqlVchOtherDetails();
                    foreach (sqlVchOtherDetail d in otherdetails)
                    {
                        if (d.COMPANYCODE == o.COMPANYCODE && d.BKGREF == o.BKGREF && d.segnum == o.SEGNUM)
                        {
                            dlst.Add(d);
                        }
                    }
                    list.Add(new VchPreviewOther(o, dlst));
                }
            }
            return list;
        }

    }
}
