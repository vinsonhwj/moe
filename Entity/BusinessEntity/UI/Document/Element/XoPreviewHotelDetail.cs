﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
   public class XoPreviewHotelDetail
    {
       public string roomType { get; set; }
       public DateTime ArrDate { get; set; }
       public DateTime DepartDate { get; set; }
       public string RateName { get; set; }
       public int  QTY { get; set; }
       public string CostCurr { get; set; }
       public decimal CostAmt { get; set; }
       public decimal TaxAmt { get; set; }
       public decimal RMNTS { get; set; }
       public string TaxCurr { get; set; }

       public XoPreviewHotelDetail()
       { }

       public XoPreviewHotelDetail(sqlXoHotelDetail xoHotelDetail)
       {

           this.roomType = xoHotelDetail.ROOMTYPE;
           this.ArrDate = xoHotelDetail.ARRDATE;
           this.DepartDate = xoHotelDetail.DEPARTDATE;
           this.RateName = xoHotelDetail.RATENAME;
           this.QTY = xoHotelDetail.QTY;
           this.CostCurr = xoHotelDetail.COSTCURR;
           this.CostAmt = xoHotelDetail.COSTAMT;
           this.TaxAmt = xoHotelDetail.taxamt;
           this.RMNTS = xoHotelDetail.rmnts;
           this.TaxCurr = xoHotelDetail.taxcurr;
       }
       public static List<XoPreviewHotelDetail> Parse(sqlXoHotelDetails xoDetails)
       {
           List<XoPreviewHotelDetail> xoHtlDetails = new List<XoPreviewHotelDetail>();
           if (xoDetails != null)
           {
               foreach (sqlXoHotelDetail d in xoDetails)
               {
                   xoHtlDetails.Add(new XoPreviewHotelDetail(d));
               }
           }
           return xoHtlDetails;
       }

    
    }
}
