﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewOtherHeader
    {
        public Int32 Seq { get; set; }
        public String BkgRef { get; set; }
        public String CompanyCode { get; set; }
        public String SegNum { get; set; }
        public String Descript1 { get; set; }
        public String Descript2 { get; set; }
        public String Descript3 { get; set; }
        public String Descript4 { get; set; }
        public String MasterDesc { get; set; }

        public InvPreviewOtherHeader()
        { }
        public InvPreviewOtherHeader(sqlInvOther other)
        {
            this.Seq = other.seq;
            this.SegNum = other.SEGNUM;
            this.CompanyCode = other.COMPANYCODE;
            this.BkgRef = other.BKGREF;
            this.Descript1 = other.DESCRIPT1;
            this.Descript2 = other.DESCRIPT2;
            this.Descript3 = other.DESCRIPT3;
            this.Descript4 = other.DESCRIPT4;
            this.MasterDesc = other.masterDesc;
        }
    }
}
