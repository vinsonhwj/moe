﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VCHHotel
    {
        public string HtlCode { get; set; }
        public string SegNum { get; set; }
        public string HtlRemark1 { get; set; }
        public string HtlRemark2 { get; set; }

        public string HtlAddress { get; set; }
        //public string HtlAddress_CN { get; set; }
        //public string HtlAddress_TW { get; set; }
        public string HtlName { get; set; }
        public string HtlName_CN { get; set; }
        public string HtlName_TW { get; set; }

        public string HtlTelephone { get; set; }
        public string HtlFax { get; set; }
        public string HtlCountryCode { get; set; }

        public string HtlCfmBy { get; set; }

        public string HtlETA { get; set; }
        public DateTime HtlArrDate { get; set; }
        public string HtlArrTime { get; set; }
        public string HtlETD { get; set; }
        public DateTime HtlDapartDate { get; set; }
        public string HtlDapartTime { get; set; }

        public string HtlReference { get; set; }
        public string HtlSpecialRequest { get; set; }
        //public string HtlWithBreakfast { get; set; }
        //public string HtlWithTransfer { get; set; }
        public string HtlRateName { get; set; }
        public string OurRef { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }

        public VCHHotel()
        {

        }
        //public VCHHtl(tbPEOHTL htl, tbPEOHOTEL hotel)
        //{
        //    this.HtlRemark1 = htl.RMK1;
        //    this.HtlRemark2 = htl.RMK2;

        //    this.HtlAddress = hotel.ADDR1;
        //    this.HtlAddress_CN = hotel.ADDR1_ZHCN;
        //    this.HtlAddress_TW = hotel.ADDR1_ZHTW;
        //    this.HtlName = hotel.HOTELNAME;
        //    this.HtlName_CN = hotel.HOTELNAME_ZHCN;
        //    this.HtlName_TW = hotel.HOTELNAME_ZHTW;
        //    this.HtlTelephone = hotel.GeneralPhone;
        //    this.HtlCountryCode = hotel.COUNTRYCODE;
        //    this.HtlCfmBy = htl.HTLCFmby;

        //    this.HtlETA = htl.ETA;
        //    this.HtlArrDate = htl.ARRDATE;
        //    this.HtlArrTime = htl.ARRTIME;
        //    this.HtlETD = htl.ETD;
        //    this.HtlDapartTime = htl.DEPARTTIME;
        //    this.HtlDapartDate = htl.DEPARTDATE;

        //    this.HtlReference = htl.BKGREF;
        //    this.HtlSpecialRequest = htl.SPCL_REQUEST;
        //    this.HtlWithBreakfast = htl.WithBkf;
        //    this.HtlWithTransfer = htl.WithTfr;
        //    this.HtlRateName = htl.RateTypeCode;

        //}
        //public static List<Element.VCHHtl> Parse(tbPEOHTLs htls,tbPEOHOTELs hotels)
        //{
        //    List<VCHHtl> vchhtls = new List<VCHHtl>();
        //    if (htls != null && hotels != null)
        //    {

        //    }
        //}
    }
}
