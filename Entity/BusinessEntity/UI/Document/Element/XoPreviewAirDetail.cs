﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewAirDetail
    {
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SegNum { get; set; }
        public string AgeType { get; set; }
        public string Curr { get; set; }
        public decimal Amt { get; set; }
        public decimal TaxAmt { get; set; }
        public int QTY { get; set; }

        public XoPreviewAirDetail()
        { }
        public XoPreviewAirDetail(sqlXoAirDetail detail)
        {
            this.CompanyCode = detail.COMPANYCODE;
            this.BkgRef = detail.BKGREF;
            this.SegNum = detail.segnum;
            this.AgeType = detail.agetype;
            this.Curr = detail.curr;
            this.Amt = detail.amt;
            this.TaxAmt = detail.taxamt;
            this.QTY = detail.QTY;
        }

        public static List<XoPreviewAirDetail> Parse(sqlXoAirDetails airDetails)
        {
            List<XoPreviewAirDetail> xoAirDetails = new List<XoPreviewAirDetail>();
            if (airDetails != null)
            {
                foreach (sqlXoAirDetail d in airDetails)
                {
                    xoAirDetails.Add(new XoPreviewAirDetail(d));
                }
            }
            return xoAirDetails;
        }
    }
}
