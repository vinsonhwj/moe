﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvMsg
    {
        public string Msg { get; set; }
        public StatusType Status { get; set; }


        public enum StatusType
        {
            Error,
            Warning,
            Success,
        }
    }
}
