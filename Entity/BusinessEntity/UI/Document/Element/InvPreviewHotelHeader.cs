﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewHotelHeader
    {
        public int Seq { get; set; }
        public string SegNum { get; set; }
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public DateTime ArrDate { get; set; }
        public string ArrTime { get; set; }
        public string Eta { get; set; }
        public DateTime DepartDate { get; set; }
        public string DepartTime { get; set; }
        public string Etd { get; set; }
        public string ReasonCode { get; set; }
        public string Rmk1 { get; set; }
        public string Rmk2 { get; set; }
        public string Spcl_Request { get; set; }
        public string HtlReference { get; set; }
        public string HtlCfmBy { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string NewCountryName { get; set; }
        public string NewCityName { get; set; }
        
        public InvPreviewHotelHeader()
        { }
        public InvPreviewHotelHeader(sqlInvHotel htl)
        {
            this.Seq = htl.seq;
            this.SegNum = htl.SEGNUM;
            this.CountryCode = htl.COUNTRYCODE;
            this.CityCode = htl.CITYCODE;
            this.ArrDate = htl.ARRDATE;
            this.ArrTime = htl.ARRTIME;
            this.Eta = htl.eta;
            this.DepartDate = htl.departdate;
            this.DepartTime = htl.departtime;
            this.Etd = htl.etd;
            this.ReasonCode = htl.ReasonCode;
            this.Rmk1 = htl.rmk1;
            this.Rmk2 = htl.rmk2;
            this.Spcl_Request = htl.spcl_request;
            this.HtlReference = htl.HTLREFERENCE;
            this.HtlCfmBy = htl.HTLCFMBY;
            this.HotelCode = htl.hotelcode;
            this.HotelName = htl.hotelname;
            this.NewCountryName = htl.newcountryname;
            this.NewCityName = htl.newcityname;
        }


    }
}
