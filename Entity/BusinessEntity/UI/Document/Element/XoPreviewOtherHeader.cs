﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewOtherHeader
    {
        public String Descript1 { get; set; }
        public String MasterDesc { get; set; }
        public String SegNum { get; set; }
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegType { get; set; }

        public XoPreviewOtherHeader()
        { }

        public XoPreviewOtherHeader(tbPEOOTHER other)
        {
            this.Descript1 =other.DESCRIPT1 ;
            this.MasterDesc =other.masterDesc ;
            this.SegNum=other.SEGNUM;
            this.CompanyCode =other.COMPANYCODE;
            this.BkgRef =other.BKGREF ;
            this.SegType=other.SEGTYPE ;

            
        }

    }
}
