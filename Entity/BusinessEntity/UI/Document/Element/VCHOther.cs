﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VCHOther
    {
        public string SegNum { get; set; }
        public string SegType { get; set; }
        public string MasterDesc { get; set; }

        public VCHOther()
        {

        }
    }
}
