﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document.Element;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VCHPassenger
    {
        public string PaxLName { get; set; }
        public string PaxFName { get; set; }
        public string PaxTitle { get; set; }
        public string PaxType { get; set; }
        public short PaxAge { get; set; }

        public VCHPassenger()
        {

        }
        public VCHPassenger(tbPEOPAX pax)
        {
            this.PaxLName = pax.PAXLNAME;
            this.PaxFName = pax.PAXFNAME;
            this.PaxTitle = pax.PAXTITLE;
            this.PaxType = pax.PAXTYPE;
            this.PaxAge = pax.PAXAGE;
        }
        public static List<Element.VCHPassenger> Parse(tbPEOPAXs paxs)
        {
            List<VCHPassenger> vchPaxs = new List<VCHPassenger>();
            if (paxs != null)
            {
                foreach (tbPEOPAX p in paxs)
                {
                    vchPaxs.Add(new VCHPassenger(p));
                }
            }
            return vchPaxs;
        }

    }

}
