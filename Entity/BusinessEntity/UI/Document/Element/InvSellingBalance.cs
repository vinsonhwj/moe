﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvSellingBalance
    {
        private string mycc; // company ID
        private string mybkg; // bkgref
        private string myinvoiceCURR;  // company local currency
        private decimal myinvoiceAMT; // the total selling (invoice) amount of the current issue invoice
        public bool isViolated; // retrun value 
        public string debugmsg; // system debug message
        private Config Config; // the connection string for emos database

        public InvSellingBalance(string companycode, string bkgref, string invoiceCurr, decimal invoiceamt, Config config)
        {
            mycc = companycode;
            mybkg = bkgref;
            myinvoiceCURR = invoiceCurr;
            myinvoiceAMT = invoiceamt;
            isViolated = false;
            debugmsg = "OK";
            Config = config;
        }

        public bool CheckMyStatus()
        {
            // this function is used to check weather the object is in normal status for return reliable value
            bool ok = true;
            // return
            string errmsg = "";

            if (string.IsNullOrEmpty(mycc) || string.IsNullOrEmpty(mybkg) || string.IsNullOrEmpty(myinvoiceCURR) || string.IsNullOrEmpty(debugmsg) || Config == null)
            {
                debugmsg += " checkMyStatus Error: initial common variable failure error.\\n";
                ok = false;
            }
            if (!string.IsNullOrEmpty(errmsg.Trim()))
            {
                WriteDebugMsg (errmsg.Trim());
            }

            return ok;
        }
        private void WriteDebugMsg(string errmsg)
        {
            if ((errmsg != null))
            {
                if (!string.IsNullOrEmpty(errmsg))
                {
                    debugmsg = (debugmsg.Trim() == "OK" ? errmsg : debugmsg + errmsg);
                }
            }
        }
    }
}

/*
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using eMosCommonLibrary.eMosDataBase;

namespace eMOSIssueDocControl
{

	public class invoiceSellingBalanceHandler
	{
		// this module is used to check the Total invoice amount against total selling with a booking

			// company ID
		private string mycc;
			// bkgref
		private string mybkg;
			// company local currency
		private string myinvoiceCURR;
			// the total selling (invoice) amount of the current issue invoice
		private double myinvoiceAMT;
			// retrun value 
		public bool isViolated;
			// system debug message
		public string debugmsg;
			// the connection string for emos database
		private string mystrconn_emos;
			// database connection object
		private ConnectDB dbconn_emos;

		public invoiceSellingBalanceHandler(string companycode, string bkgref, string invoiceCurr, double invoiceamt, string strconn)
		{
			mycc = companycode;
			mybkg = bkgref;
			myinvoiceCURR = invoiceCurr;
			myinvoiceAMT = Convert.ToDouble(invoiceamt);
			isViolated = false;
			debugmsg = "OK";
			mystrconn_emos = strconn;
			dbconn_emos = new ConnectDB();

			if (checkMyStatus()) {
				isViolated = checkViolation();
			}
		}

		private bool checkViolation()
		{
			bool violated = false;
			System.Data.SqlClient.SqlCommand MyCmd = null;
			System.Data.SqlClient.SqlDataReader MyReader = null;
			string sql = "";
			string errmsg = "";
			double ttlsum = Convert.ToDouble(0);
			string ttlcurr = string.Empty;
			eMosCommonLibrary.CurrHandler currhandler = new eMosCommonLibrary.CurrHandler();

			//sql = " select convert(decimal(16,2) , " & myinvoiceAMT & ") + convert(decimal(16,2) , inv.[Total Invoice] - inv.[Total Credit Note] - (sell.[Total selling] + sell.[Total selling tax])) "
			//sql &= " as ttlSum from view_booksellcost_detail sell , view_booksum_detail inv"
			//sql &= " where sell.companycode = '" & dbconn_emos.FixQuotes(mycc) & "' and inv.companycode = sell.companycode"
			//sql &= " and sell.bkgref = '" & dbconn_emos.FixQuotes(mybkg) & "' and inv.bkgref = sell.bkgref"

			sql = " SELECT invcurr, convert(decimal(16,2) , (M.INVAMT + M.INVTAX) - (M.TTLSELLAMT + M.TTLSELLTAX) ) ";
			sql += " as ttlSum FROM Peomstr M ";
			sql += " WHERE M.bkgref = '" + dbconn_emos.FixQuotes(mybkg) + "'";
			sql += " AND M.companycode = '" + dbconn_emos.FixQuotes(mycc) + "'";

			if (dbconn_emos.ConnectDB(mystrconn_emos) == "OK") {
				MyCmd = new System.Data.SqlClient.SqlCommand(sql.Trim(), dbconn_emos.MyConn);
				try {
					MyReader = MyCmd.ExecuteReader();
					if (MyReader.Read()) {
						ttlsum = Convert.ToDouble(MyReader["ttlSum"]);
						ttlcurr = Convert.ToString(MyReader["invcurr"]);
					} else {
						// exception
						errmsg += " checkViolation Error: no total selling or invoice amount information error.\\n";
					}
					MyReader.Close();
				} catch {
					errmsg += " checkViolation Error: " + Err().Description + "\\n";
				}
				MyCmd.Dispose();
				dbconn_emos.DisConnectDB();
			} else {
				// exception
				errmsg += " checkViolation Error: database connection failure error.\\n";
			}

			// handle Multi currency 
			myinvoiceAMT = Convert.ToDouble(Strings.FormatNumber((ttlcurr.Trim() != myinvoiceCURR.Trim() ? currhandler.calculateAMT(ttlcurr, myinvoiceCURR, myinvoiceAMT, mycc) : myinvoiceAMT), 2));

			if (Math.Round((myinvoiceAMT + ttlsum), 2) > Convert.ToDouble(System.Data.Common.roundUpTorrent)) {
				// violated inoivce selling balance
				violated = true;
			} else {
				violated = false;
			}

			return violated;
		}

		public bool checkMyStatus()
		{
			// this function is used to check weather the object is in normal status for return reliable value
			bool ok = true;
			// return
			string errmsg = "";
			// debug message

			string[] myarr = {
				mycc,
				mybkg,
				myinvoiceCURR,
				myinvoiceAMT,
				debugmsg,
				mystrconn_emos
			};
			if (dbconn_emos.atLeastOneEmptyString(myarr)) {
				debugmsg += " checkMyStatus Error: initial common variable failure error.\\n";
				ok = false;
			} else {
				if (!Information.IsNumeric(myinvoiceAMT)) {
					debugmsg += " checkMyStatus Error: wrong invoice amount error.\\n";
					ok = false;
				}
				if (dbconn_emos.ConnectDB(mystrconn_emos) != "OK") {
					debugmsg += " checkMyStatus Error: Database connection failure error.\\n";
					ok = false;
				} else {
					// connection successful
					dbconn_emos.DisConnectDB();
				}
			}

			if (!string.IsNullOrEmpty(errmsg.Trim())) {
				writeDebugMSG(errmsg.Trim());
			}

			return ok;
		}

		private void writeDebugMSG(string errmsg)
		{
			if ((errmsg != null)) {
				if (!string.IsNullOrEmpty(errmsg)) {
					debugmsg = (debugmsg.Trim() == "OK" ? errmsg : debugmsg + errmsg);
				}
			}
		}
	}

	public class documentCostingBalanceHandler
	{
		// this module is used to check the Total document amount against total costing with a booking

			// company ID
		private string mycc;
			// bkgref
		private string mybkg;
			// company local currency
		protected string mydocCURR;
			// the total selling (invoice) amount of the current issue invoice
		protected double mydocAMT;
			// retrun value 
		public bool isViolated;
			// system debug message
		public string debugmsg;
			// the connection string for emos database
		private string mystrconn_emos;
			// database connection object
		private ConnectDB dbconn_emos;

		protected double ttlsum = Convert.ToDouble(0);

		protected string ttlcurr = string.Empty;
		public documentCostingBalanceHandler(string companycode, string bkgref, string docCurr, double docAmt, string strconn)
		{
			mycc = companycode;
			mybkg = bkgref;
			mydocCURR = docCurr;
			mydocAMT = Convert.ToDouble(docAmt);
			ttlsum = Convert.ToDouble(0);
			ttlcurr = string.Empty;
			isViolated = false;
			debugmsg = "OK";
			mystrconn_emos = strconn;
			dbconn_emos = new ConnectDB();

			if (checkMyStatus()) {
				isViolated = checkViolation();
			}

		}

		private bool checkViolation()
		{
			bool violated = false;
			string sql = "";
			string errmsg = "";
			eMosCommonLibrary.CurrHandler currhandler = new eMosCommonLibrary.CurrHandler();

			//sql = " select convert(decimal(16,2) , " & myinvoiceAMT & ") + convert(decimal(16,2) , inv.[Total Invoice] - inv.[Total Credit Note] - (sell.[Total selling] + sell.[Total selling tax])) "
			//sql &= " as ttlSum from view_booksellcost_detail sell , view_booksum_detail inv"
			//sql &= " where sell.companycode = '" & dbconn_emos.FixQuotes(mycc) & "' and inv.companycode = sell.companycode"
			//sql &= " and sell.bkgref = '" & dbconn_emos.FixQuotes(mybkg) & "' and inv.bkgref = sell.bkgref"

			sql = " SELECT doccurr, convert(decimal(16,2) , (M.docAMT + M.docTAX) - (M.TTLcostAMT + M.TTLcostTAX) ) ";
			sql += " as ttlSum FROM Peomstr M ";
			sql += " WHERE M.bkgref = @bkgref";
			sql += " AND M.companycode = @companycode";

			if (dbconn_emos.ConnectDB(mystrconn_emos) == "OK") {
				dbconn_emos.CommandText = sql;
				dbconn_emos.AddParameter("@bkgref", mybkg.Trim());
				dbconn_emos.AddParameter("@companycode", mycc.Trim());

				try {
					dbconn_emos.ExecuteReader();
					if (dbconn_emos.MyDataReader.Read) {
						ttlsum = Convert.ToDouble(dbconn_emos.MyDataReader_Item("ttlSum"));
						ttlcurr = Convert.ToString(dbconn_emos.MyDataReader_Item("doccurr"));
					} else {
						// exception
						errmsg += " checkViolation Error: no total selling or invoice amount information error.\\n";
					}
				} catch {
					errmsg += " checkViolation Error: " + Err().Description + "\\n";
				}

				dbconn_emos.DisConnectDB();
			} else {
				// exception
				errmsg += " checkViolation Error: database connection failure error.\\n";
			}

			// handle Multi currency 
			mydocAMT = Convert.ToDouble(Strings.FormatNumber((ttlcurr.Trim() != mydocCURR.Trim() ? currhandler.calculateAMT(ttlcurr, mydocCURR, mydocAMT, mycc) : mydocAMT), 2));

			if (mydocAMT + ttlsum > Convert.ToDouble(System.Data.Common.roundUpTorrent)) {
				// violated inoivce selling balance
				violated = true;
			} else {
				violated = false;
			}

			return violated;

		}

		public bool checkMyStatus()
		{
			// this function is used to check weather the object is in normal status for return reliable value
			bool ok = true;
			// return
			string errmsg = "";
			// debug message

			string[] myarr = {
				mycc,
				mybkg,
				mydocCURR,
				mydocAMT,
				debugmsg,
				mystrconn_emos
			};
			if (dbconn_emos.atLeastOneEmptyString(myarr)) {
				debugmsg += " checkMyStatus Error: initial common variable failure error.\\n";
				ok = false;
			} else {
				if (!Information.IsNumeric(mydocAMT)) {
					debugmsg += " checkMyStatus Error: wrong invoice amount error.\\n";
					ok = false;
				}
				if (dbconn_emos.ConnectDB(mystrconn_emos) != "OK") {
					debugmsg += " checkMyStatus Error: Database connection failure error.\\n";
					ok = false;
				} else {
					// connection successful
					dbconn_emos.DisConnectDB();
				}
			}

			if (!string.IsNullOrEmpty(errmsg.Trim())) {
				writeDebugMSG(errmsg.Trim());
			}

			return ok;
		}

		private void writeDebugMSG(string errmsg)
		{
			if ((errmsg != null)) {
				if (!string.IsNullOrEmpty(errmsg)) {
					debugmsg = (debugmsg.Trim() == "OK" ? errmsg : debugmsg + errmsg);
				}
			}
		}

		#region "Public Accessor"
		public double totalDocumentAmount {
			get { return this.ttlsum; }
		}
		public string totalDocumentCurrency {
			get { return this.ttlcurr; }
		}
		#endregion

	}

}
//*/