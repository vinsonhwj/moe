﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VchPreviewOtherDetail
    {
        public String AgeType { get; set; }
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String Curr { get; set; }
        public Decimal Amt { get; set; }
        public String TaxCurr { get; set; }
        public Decimal TaxAmt { get; set; }
        public Decimal Qty { get; set; }

        public VchPreviewOtherDetail()
        {}
        public VchPreviewOtherDetail(sqlVchOtherDetail detail)
        {
            this.AgeType=detail.agetype;
            this.CompanyCode = detail.COMPANYCODE;
            this.BkgRef = detail.BKGREF;
            this.SegNum = detail.segnum;
            this.Curr = detail.curr;
            this.Amt = detail.amt;
            this.TaxCurr = detail.taxcurr;
            this.TaxAmt = detail.taxamt;
            this.Qty = detail.QTY;
        }
        internal static List<VchPreviewOtherDetail> Parse(sqlVchOtherDetails details)
        {
            List<VchPreviewOtherDetail> vchOthDetails = new List<VchPreviewOtherDetail>();

            if (details != null)
            {
                foreach (sqlVchOtherDetail d in details)
                {
                    vchOthDetails.Add(new VchPreviewOtherDetail(d));
                }
            }
            return vchOthDetails;
        }
    }
}
