﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
   public class XoPreviewAirHeader
    {
       public string BkgRef {get;set;}
       public string CompanyCode {get;set;}
       public string SegNum{get;set;}
       public string Flight { get; set; }
       public string Class { get; set; }
       public string DepartCity { get; set; }
       public string ArrivalCity { get; set; }
       public DateTime DepartDATE { get; set; }
       public string DepartTime { get; set; }
       public DateTime ArrDATE { get; set; }
       public string ArrTime { get; set; }
       public string PNR { get; set; }
       public string Vchrmk1 { get; set; }
       public string Vchrmk2 { get; set; }


       public XoPreviewAirHeader()
       { }

       public XoPreviewAirHeader(tbPEOAIR air)
       {
           this.BkgRef = air.BkgRef;
           this.CompanyCode = air.CompanyCode;
           this.SegNum = air.SegNum;
           this.Flight = air.Flight;
           this.Class = air.Class;
           this.DepartCity = air.DepartCity;
           this.ArrivalCity = air.ArrivalCity;
           this.DepartDATE = air.DepartDATE;
           this.DepartTime = air.DepartTime;
           this.ArrDATE = air.ArrDATE;
           this.ArrTime = air.ArrTime;
           this.PNR = air.PNR;
           this.Vchrmk1 = air.VCHRMK1;
           this.Vchrmk2 = air.VCHRMK2;
 
       }

    }
}
