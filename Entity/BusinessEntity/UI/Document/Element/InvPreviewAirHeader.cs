﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewAirHeader
    {
        public int Seq { get; set; }
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String Flight { get; set; }
        public String Class { get; set; }
        public String DepartCity { get; set; }
        public String ArrivalCity { get; set; }
        public DateTime DepartDATE { get; set; }
        public String DepartTime { get; set; }
        public DateTime ArrDATE { get; set; }
        public String ArrTime { get; set; }
        public String ReasonCode { get; set; }
        public String PNR { get; set; }

        public InvPreviewAirHeader()
        { }
        public InvPreviewAirHeader(tbPEOAIR air)
        {
            int s = 0;
            int.TryParse(air.SegNum, out s);
            this.Seq = s;
            this.CompanyCode = air.CompanyCode;
            this.BkgRef = air.BkgRef;
            this.SegNum = air.SegNum;
            this.Flight = air.Flight;
            this.Class = air.Class;
            this.DepartCity = air.DepartCity;
            this.ArrivalCity = air.ArrivalCity;
            this.DepartDATE = air.DepartDATE;
            this.DepartTime = air.DepartTime;
            this.ArrDATE = air.ArrDATE;
            this.ArrTime = air.ArrTime;
            this.ReasonCode = air.ReasonCode;
            this.PNR = air.PNR;
        }
    }
}
