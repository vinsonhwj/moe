﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewAir
    {
        public XoPreviewAirHeader Header { get; set; }

        public List<XoPreviewAirDetail> Detail { get; set; }

        public XoPreviewAir()
        { }
        public XoPreviewAir(tbPEOAIR air, sqlXoAirDetails airDetails)
        {
            this.Header = new XoPreviewAirHeader(air);
            this.Detail = XoPreviewAirDetail.Parse(airDetails);
        }
        public static List<XoPreviewAir> Parse(tbPEOAIRs airs, sqlXoAirDetails airDetails)
        {
            List<XoPreviewAir> xoAir = new List<XoPreviewAir>();
            if (airs != null && airDetails !=null)
            {
                foreach (tbPEOAIR a in airs)
                {
                    sqlXoAirDetails dlst = new sqlXoAirDetails();
                    foreach (sqlXoAirDetail d in airDetails)
                    {
                        if (d.COMPANYCODE == a.CompanyCode && d.BKGREF == a.BkgRef && d.segnum == a.SegNum)
                        {
                            dlst.Add(d);
                        }

                    }
                    if (a != null && dlst.Count > 0)
                    {
                        xoAir.Add(new XoPreviewAir(a, dlst));
                    }
                }
            }
            return xoAir;
        }
    }
}
