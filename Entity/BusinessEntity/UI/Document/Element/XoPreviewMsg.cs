﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class XoPreviewMsg
    {
        public string Msg { get; set; }
        public XoStatusType Status { get; set; }

        public enum XoStatusType
        {
            Error,
            Warning,
            Success,
        }
    }
}
