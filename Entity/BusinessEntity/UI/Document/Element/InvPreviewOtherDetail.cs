﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewOtherDetail
    {
        public decimal GSTTax { get; set; }
        public string AgeType { get; set; }
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SegNum { get; set; }
        public string Curr { get; set; }
        public decimal Amt { get; set; }
        public string TaxCurr { get; set; }
        public decimal TaxAmt { get; set; }
        public string GSTTaxShow { get; set; }
        public int QTY { get; set; }
        public string SegType { get; set; }

        public InvPreviewOtherDetail()
        { }
        public InvPreviewOtherDetail(sqlInvOtherDetail detail)
        {
            this.GSTTax = detail.GSTTAX;
            this.AgeType = detail.agetype;
            this.CompanyCode = detail.COMPANYCODE;
            this.BkgRef = detail.BKGREF;
            this.SegNum = detail.segnum;
            this.Curr = detail.curr;
            this.Amt = detail.amt;
            this.TaxCurr = detail.taxcurr;
            this.TaxAmt = detail.taxamt;
            this.GSTTaxShow = detail.gsttaxshow;
            this.QTY = detail.QTY;
            this.SegType = detail.segType;
        }
        internal static List<InvPreviewOtherDetail> Parse(sqlInvOtherDetails details)
        {
            List<InvPreviewOtherDetail> invOthDetails = new List<InvPreviewOtherDetail>();

            if (details != null)
            {
                foreach (sqlInvOtherDetail d in details)
                {
                    invOthDetails.Add(new InvPreviewOtherDetail(d));
                }
            }
            return invOthDetails;
        }

    }
}
