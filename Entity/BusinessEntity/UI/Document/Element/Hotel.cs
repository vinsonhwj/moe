﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.BLLEntity.Segment;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class Hotel
    {
        public tbPEOHOTEL HotelInfo { get; set; }
        public BookingHotel BookingHotel { get; set; }

        public Hotel()
        {

        }
    }
}
