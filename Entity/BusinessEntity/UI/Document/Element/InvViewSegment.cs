﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvViewSegment
    {
        public InvViewSegHeader Header { get; set; }

        public List<InvViewSegDetail> Details { get; set; }
    }
}
