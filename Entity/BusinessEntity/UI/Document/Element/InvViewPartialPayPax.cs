﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvViewPartialPayPax
    {
        public string PaxLName { get; set; }

        public string PaxFName { get; set; }

        public string PaxType { get; set; }

        public string Curr { get; set; }

        public decimal Sell { get; set; }
    }
}
