﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvPreviewAirDetail
    {
        public String ShowDetailFee;
        public String AgeType;
        public String CompanyCode;
        public String BkgRef;
        public String SegNum;
        public String Curr;
        public Decimal AmtFee;
        public Decimal Amt;
        public String TaxCurr;
        public Decimal TaxAmt;
        public Int32 UserQty;
        public Int32 AirQty;
        public Int32 Qty;
        public Decimal ShowAmt;

        public InvPreviewAirDetail()
        { }
        public InvPreviewAirDetail(sqlInvAirDetail detail)
        {
            this.ShowDetailFee = detail.showdetailfee;
            this.AgeType = detail.agetype;
            this.CompanyCode = detail.COMPANYCODE;
            this.BkgRef = detail.BKGREF;
            this.SegNum = detail.segnum;
            this.Curr = detail.curr;
            this.AmtFee = detail.amtfee;
            this.Amt = detail.amt;
            this.TaxCurr = detail.taxcurr;
            this.TaxAmt = detail.taxamt;
            this.UserQty = detail.User_QTY;
            this.AirQty = detail.AIR_QTY;
            this.ShowAmt = detail.showamt;
        }
        internal static List<InvPreviewAirDetail> Parse(sqlInvAirDetails airDetails)
        {
            List<InvPreviewAirDetail> invAirDetails = new List<InvPreviewAirDetail>();

            if (airDetails != null)
            {
                foreach (sqlInvAirDetail d in airDetails)
                {
                    invAirDetails.Add(new InvPreviewAirDetail(d));
                }
            }
            return invAirDetails;
        }
    }
}
