﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class VchPreviewHotelDetail
    {
        public Int32 QtyOfHotel { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }
        public Int32 QTY { get; set; }
        public DateTime ARRDATE { get; set; }
        public DateTime DEPARTDATE { get; set; }
        public Decimal rmnts { get; set; }
        public String ITEMNAME { get; set; }
        public String RATENAME { get; set; }
        public String COSTCURR { get; set; }
        public Decimal COSTAMT { get; set; }
        public String TAXCURR { get; set; }
        public Decimal TAXamt { get; set; }
        public Decimal TOTALAMT { get; set; }
        public String ItemType { get; set; }
        public VchPreviewHotelDetail()
        {

        }
        public VchPreviewHotelDetail(sqlVchHotelDetail detail)
        {
            this.QtyOfHotel = detail.QtyOfHotel;
            this.SEGNUM = detail.SEGNUM;
            this.SEQNUM = detail.SEQNUM;
            this.QTY = detail.QTY;
            this.ARRDATE = detail.ARRDATE;
            this.DEPARTDATE = detail.DEPARTDATE;
            this.rmnts = detail.rmnts;
            this.ITEMNAME = detail.ITEMNAME;
            this.RATENAME = detail.RATENAME;
            this.COSTCURR = detail.COSTCURR;
            this.COSTAMT = detail.COSTAMT;
            this.TAXCURR = detail.TAXCURR;
            this.TAXamt = detail.TAXamt;
            this.TOTALAMT = detail.TOTALAMT;
            this.ItemType = detail.ItemType;
        }
        internal static List<VchPreviewHotelDetail> Parse(sqlVchHotelDetails details)
        {
            List<VchPreviewHotelDetail> vchHtlDetails = new List<VchPreviewHotelDetail>();

            if (details != null)
            {
                foreach (sqlVchHotelDetail d in details)
                {
                    vchHtlDetails.Add(new VchPreviewHotelDetail(d));
                }
            }
            return vchHtlDetails;
        }
    }
    

}
