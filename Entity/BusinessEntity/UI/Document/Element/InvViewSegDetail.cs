﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document.Element
{
    public class InvViewSegDetail
    {
        public Decimal gstPct { get; set; }
        public String segType { get; set; }
        public Decimal gstamt { get; set; }
        public String showdetailfee { get; set; }
        public Decimal amtfee { get; set; }
        public String COMPANYCODE { get; set; }
        public String INVNUM { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }
        public String ItemName { get; set; }
        public String RateName { get; set; }
        public String itemtype { get; set; }
        public Decimal UNITPRICE { get; set; }
        public Decimal qty { get; set; }
        public Decimal rmnts { get; set; }
        public String sellcurr { get; set; }
        public Decimal SELLAMT { get; set; }
        public String taxcurr { get; set; }
        public Decimal taxamt { get; set; }
        public Decimal showamt { get; set; }
        public Decimal HKDAMT { get; set; }
        public Decimal TotalAMT { get; set; }
        public String servtype { get; set; }

       
    }
}
