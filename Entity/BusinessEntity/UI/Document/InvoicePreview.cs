﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class InvoicePreview
    {
        #region Infomation
        public string CompanyCode { get; set; }
        public string BookingNum { get; set; }

        public string ClientCode { get; set; }  //txt_clientcode_title
        public string ContactPerson { get; set; }//txt_CONTACTPERSON_title

        public string ClientName { get; set; } //lb_Clientname
        public string ClientNameTitle { get; set; }// lb_clientname_title
        public string Address1 { get; set; }//lb_address1_title
        public string Address2 { get; set; }//lb_address2_title
        public string Address3 { get; set; }//lb_address3_title
        public string Address4 { get; set; }//lb_address4_title

        public string BookingHolder { get; set; } //lb_BookingHolder
        public string Tel { get; set; }// lb_tel_title
        public string PrintedDate { get; set; }//lb_printeddate_title
        public string DefaultCurr { get; set; }//lb_defaultcurr_title
        public string Attn { get; set; } //lb_attn1_title
        public string Ref { get; set; }//lb_ref_title
        // public string ProfitMarginReason { get; set; }//ddl_ProfitMarginReason

        public string AirRemark1 { get; set; }//lb_airremark1_show
        public string AirRemark2 { get; set; }//lb_airremark2_show
        public string AirCurr { get; set; } //lb_air_curr
        // public string AirCurr2 { get; set; }// lb_air_curr_2
        public decimal totalamount4vle { get; set; }  //lb_totalamount4vle
        // public decimal totalamount4vle2 { get; set; }//lb_totalamount4vle_2

        // public string TicketDtl { get; set; } //lb_ticket_dtl

        public string HotelRemark1 { get; set; }//lb_hotelremark1_show
        public string HotelRemark2 { get; set; }//lb_hotelremark2_show
        public string HotelCurr { get; set; }//lb_hotel_curr
        //  public string HotelCurr2 { get; set; }//lb_hotel_curr_2
        public decimal Totalamount5vle { get; set; }//lb_Totalamount5vle
        // public string Totalamount5vle2 { get; set; }// lb_Totalamount5vle_2

        public string OtherRemark1 { get; set; }// lb_otherremark1_show
        public string OtherRemark2 { get; set; }// lb_otherremark2_show
        public string OtherCurr { get; set; }
        //  public string OtherCurr2 { get; set; }//lb_other_curr1_2
        public decimal Totalamount6_vle { get; set; }//lb_Totalamount6_vle
        //   public string Totalamount6vle2 { get; set; }// lb_Totalamount6_vle_2

        public string Consultant { get; set; }// lb_Consultant_title
        public string GSTTaxCurr { get; set; }//lb_GSTTAXCURR
        public string GrandTotalCurr { get; set; }//lb_other_curr2
        //  public string GrandTotalCurr2 { get; set; }//lb_other_curr2_2
        public decimal GrandTotalamountvle { get; set; }//lb_GrandTotalamountvle
        //  public string GrandTotalamountvle2 { get; set; }//lb_GrandTotalamountvle_2

        public decimal GrandTAXamt { get; set; }// Session("grandTaxamt")

        public string FormPayment { get; set; }//lb_form_payment_show
        public string Cash { get; set; }//  lb_cash_show

        public string ChequeNum { get; set; } //lb_cheque_num_show
        public string ChequeIssue { get; set; }//lb_cheque_issue_show
        public string ChequePayer { get; set; }//lb_cheque_payer_show
        public string ChequeBankCode { get; set; }//lb_cheque_bankCode_show
        #endregion

        public Element.InvMsg PreviewMsg { get; set; }

        public List<Element.InvPreviewPax> PassengerList { get; set; }

        public List<Element.InvPreviewAir> AirList { get; set; }

        public List<Element.InvPreviewHotel> HotelList { get; set; }

        public List<Element.InvPreviewOther> OtherList { get; set; }

        public List<Element.InvPreviewTicket> TicketList { get; set; }
    }
}
