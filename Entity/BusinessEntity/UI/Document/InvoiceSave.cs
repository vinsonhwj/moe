﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.UI.Document
{
    public class InvoiceSave
    {
        public Element.InvMsg SaveMsg { get; set; }

        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string InvoiceNum { get; set; }


    }
}
