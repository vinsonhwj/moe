﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.Segment
{
    public class BookingAir
    {
        public tbPEOAIR Header { get; set; }
        public tbPEOAIRDETAILs Details { get; set; }

        public BookingAir()
        {
            Details = new tbPEOAIRDETAILs();
        }

        public BookingAir(tbPEOAIR header, tbPEOAIRDETAILs details)
        {
            Header = header;
            Details = details;
        }
    }
}
