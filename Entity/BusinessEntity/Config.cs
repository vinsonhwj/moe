﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity;
using Westminster.MOE.Entity.DALEntity.Configuration;

namespace Westminster.MOE.Entity.BLLEntity
{
    public class Config
    {
        public DataConfigDictionary DatabaseList { get; set; }
        public WebSvcConfigDictionary WebSvcConfigList { get; set; }
        public DefaultSetting DefaultSettings { get; set; }
       
        public Config()
        {
            DatabaseList = new DataConfigDictionary();
            WebSvcConfigList = new WebSvcConfigDictionary();
            DefaultSettings = new DefaultSetting();
        }
      
    }
}
