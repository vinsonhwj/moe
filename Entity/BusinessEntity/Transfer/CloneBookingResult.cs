﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    public class CloneBookingResult
    {
        private string bkGref;

        public string BkGref
        {
            get { return bkGref; }
            set { bkGref = value; }
        }

        private bool isSuccess;

        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }

        private string msg;

        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }

    }
}
