﻿using System;
using System.Collections.Generic;
using Westminster.MOE.Entity.DALEntity.Transfer;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    [Serializable]
    public class RetrieveIURBooking
    {
        #region Attributes
        public UO_PeoMstr Peomstr { get; set; }
        public AirlineSegments AirlineSegmentsList { get; set; }
        public HotelSegments HotelSegmentsList { get; set; }
        public OtherSegments OtherSegmentsList { get; set; }
        public TicketSegments TicketSegmentsList { get; set; }
        public UO_PeoPaxs PassengersList { get; set; }
        public UO_CUSTOMER Customer { get; set; }
        public AutoBookingMessages Message { get; set; }
        public int Status { get; set; }
        public string Exception { get; set; }
        public string CompanyCode { get; set; }
        public string PNR { get; set; }
        public string Bkgref { get; set; }
        public string SourceSystem { get; set; }
        public Invoice_Types InvType { get; set; }
        #endregion

        public RetrieveIURBooking()
        {
            Peomstr = new UO_PeoMstr();
            TicketSegmentsList = new TicketSegments();
            AirlineSegmentsList = new AirlineSegments();
            HotelSegmentsList = new HotelSegments();
            OtherSegmentsList = new OtherSegments();
            PassengersList = new UO_PeoPaxs();
            Customer = new UO_CUSTOMER();
            Message = new AutoBookingMessages();
        }
    }
    public class UO_PeoMstr
    {
        public String COMPANYCODE { get; set; }
        public String BKGREF { get; set; }
        public String agency_arc_iata_num { get; set; }
        public String iss_off_code { get; set; }
        public String tranflag { get; set; }
        public String PNR { get; set; }
        public String CRSSINEIN { get; set; }
        public String COD1 { get; set; }
        public String COD2 { get; set; }
        public String CLTODE { get; set; }
        public String CLTNAME { get; set; }
        public String CLTADDR { get; set; }
        public String STAFFCODE { get; set; }
        public String TEAMCODE { get; set; }
        public String FIRSTPAX { get; set; }
        public String PHONE { get; set; }
        public String FAX { get; set; }
        public String EMAIL { get; set; }
        public String MASTERPNR { get; set; }
        public String TOURCODE { get; set; }
        public String CONTACTPERSON { get; set; }
        public DateTime DEPARTDATE { get; set; }
        public String TTLSELLCURR { get; set; }
        public Decimal TTLSELLAMT { get; set; }
        public String TTLSELLTAXCURR { get; set; }
        public Decimal TTLSELLTAX { get; set; }
        public String TTLCOSTCURR { get; set; }
        public Decimal TTLCOSTAMT { get; set; }
        public String TTLCOSTTAXCURR { get; set; }
        public Decimal TTLCOSTTAX { get; set; }
        public String INVCURR { get; set; }
        public Decimal INVAMT { get; set; }
        public String INVTAXCURR { get; set; }
        public Decimal INVTAX { get; set; }
        public String DOCCURR { get; set; }
        public Decimal DOCAMT { get; set; }
        public String DOCTAXCURR { get; set; }
        public Decimal DOCTAX { get; set; }
        public Int32 INVCOUNT { get; set; }
        public Int32 DOCCOUNT { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String RMK3 { get; set; }
        public String RMK4 { get; set; }
        public DateTime DEADLINE { get; set; }
        public String SOURCESYSTEM { get; set; }
        public String SOURCE { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String COD3 { get; set; }
        public String INSFLAG { get; set; }
        public DateTime TOEMOSON { get; set; }
        public String TOEMOSBY { get; set; }
        public String EMOSBKGREF { get; set; }
        public String IURFLAG { get; set; }
        public String pseudo { get; set; }
        public String pseudo_inv { get; set; }
        public String CRSSINEIN_inv { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PeoMstr()
        {

        }

    }

    //public class ConnectionInformation
    //{
    //    public Boolean IsSqlSentence { get; set; }
    //    public String TableName { get; set; }
    //    public String ConnectionKey { get; set; }
    //    public String[] PrimaryKeys { get; set; }
    //    public String ConnectionString { get; set; }

    //    public ConnectionInformation()
    //    {

    //    }

    //}

    public class AirlineSegment
    {
        public UO_PEOAIR UO_PEOAIR { get; set; }
        public List<UO_PEOAIRDETAIL> UOList_PEOAIRDETAIL { get; set; }

        public AirlineSegment()
        {
            UOList_PEOAIRDETAIL = new List<UO_PEOAIRDETAIL>();
        }

    }

    public class AirlineSegments : List<AirlineSegment> { }

    public class UO_PEOAIR
    {
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String PNR { get; set; }
        public String Flight { get; set; }
        public String Class { get; set; }
        public String DepartCity { get; set; }
        public String ArrivalCity { get; set; }
        public String Status { get; set; }
        public String NumOfPax { get; set; }
        public DateTime DepartDATE { get; set; }
        public String DepartTime { get; set; }
        public DateTime ArrDATE { get; set; }
        public String ArrTime { get; set; }
        public String ElapsedTime { get; set; }
        public String SSR { get; set; }
        public String Seat { get; set; }
        public String ReasonCode { get; set; }
        public Int32 StopOverNum { get; set; }
        public String StopOverCity { get; set; }
        public String Pcode { get; set; }
        public String SpecialRQ { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String INVRMK1 { get; set; }
        public String INVRMK2 { get; set; }
        public String VCHRMK1 { get; set; }
        public String VCHRMK2 { get; set; }
        public String SELLCURR { get; set; }
        public Decimal TOTALSELL { get; set; }
        public Decimal TOTALSTAX { get; set; }
        public String COSTCURR { get; set; }
        public Decimal TOTALCOST { get; set; }
        public Decimal TOTALCTAX { get; set; }
        public String SourceSystem { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public DateTime VoidOn { get; set; }
        public String VoidBy { get; set; }
        public String AirSeg { get; set; }
        public String DepartTerminal { get; set; }
        public String ArrivalTerminal { get; set; }
        public String OtherAirlinePNR { get; set; }
        public String EQP { get; set; }
        public String Service { get; set; }
        public String Suppcode { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOAIR()
        {

        }

    }

    public class UO_PEOAIRDETAIL
    {
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String SEQNUM { get; set; }
        public String SEQTYPE { get; set; }
        public Int32 QTY { get; set; }
        public String CURR { get; set; }
        public Decimal AMT { get; set; }
        public String TAXCURR { get; set; }
        public Decimal TAXAMT { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String AGETYPE { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOAIRDETAIL()
        {

        }

    }

    public class UO_PEOAIRDETAILs : List<UO_PEOAIRDETAIL> { }

    public class HotelSegment
    {
        public UO_PEOHTL PEOHTL { get; set; }
        public List<UO_PeoHtlDetail> PeoHtlDetail { get; set; }

        public HotelSegment()
        {
            PeoHtlDetail = new List<UO_PeoHtlDetail>();
        }

    }

    public class HotelSegments : List<HotelSegment> { }

    public class UO_PEOHTL
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String COUNTRYCODE { get; set; }
        public String CITYCODE { get; set; }
        public String ADCOMTXNO { get; set; }
        public String ADCOMSEQNUM { get; set; }
        public String HOTELCODE { get; set; }
        public DateTime ARRDATE { get; set; }
        public String ARRTIME { get; set; }
        public DateTime DEPARTDATE { get; set; }
        public String DEPARTTIME { get; set; }
        public String ETA { get; set; }
        public String ETD { get; set; }
        public String SELLCURR { get; set; }
        public String COSTCURR { get; set; }
        public Decimal TOTALCOST { get; set; }
        public Decimal TOTALSELL { get; set; }
        public Decimal TOTALCTAX { get; set; }
        public Decimal TOTALSTAX { get; set; }
        public String FORMPAY { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String INVRMK1 { get; set; }
        public String INVRMK2 { get; set; }
        public String VCHRMK1 { get; set; }
        public String VCHRMK2 { get; set; }
        public String WESTELNO { get; set; }
        public String SOURCESYSTEM { get; set; }
        public String SPCL_REQUEST { get; set; }
        public String HTLREFERENCE { get; set; }
        public String HTLCFmby { get; set; }
        public String VOIDBY { get; set; }
        public DateTime VOIDON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime CREATEON { get; set; }
        public String UPDATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String SOURCEREF { get; set; }
        public String GSTTAX { get; set; }
        public String showDetailFee { get; set; }
        public String SuppCode { get; set; }
        public String hotelVoucherType { get; set; }
        public String COD1 { get; set; }
        public String COD2 { get; set; }
        public String COD3 { get; set; }
        public String roomTypeCode { get; set; }
        public Decimal fullRoomRate { get; set; }
        public Decimal dailyRate { get; set; }
        public Decimal lowRoomRate { get; set; }
        public String RateTypeCode { get; set; }
        public String isRecalable { get; set; }
        public String ReasonCode { get; set; }
        public String HOTELNAME { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOHTL()
        {

        }

    }

    public class UO_PeoHtlDetail
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEQTYPE { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }
        public Int32 QTY { get; set; }
        public DateTime ARRDATE { get; set; }
        public DateTime DEPARTDATE { get; set; }
        public Decimal RMNTS { get; set; }
        public String RATENAME { get; set; }
        public String ITEMNAME { get; set; }
        public String STATUS { get; set; }
        public String CURR { get; set; }
        public Decimal AMT { get; set; }
        public Decimal TOTALAMT { get; set; }
        public String TAXCURR { get; set; }
        public Decimal TAXAMT { get; set; }
        public String CREATEBY { get; set; }
        public DateTime CREATEON { get; set; }
        public String UPDATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String itemtype { get; set; }
        public Decimal AMTFEE { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PeoHtlDetail()
        {

        }

    }

    public class UO_PeoHtlDetails : List<UO_PeoHtlDetail> { }

    public class OtherSegment
    {
        public UO_PEOOTHER PEOOTHER { get; set; }
        public List<UO_PEOOTHERDETAIL> List_PEOOTHERDETAIL { get; set; }

        public OtherSegment()
        {
            List_PEOOTHERDETAIL = new List<UO_PEOOTHERDETAIL>();
        }

    }

    public class OtherSegments : List<OtherSegment> { }

    public class UO_PEOOTHER
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String SEGTYPE { get; set; }
        public DateTime OTHERDATE { get; set; }
        public String DESCRIPT1 { get; set; }
        public String DESCRIPT2 { get; set; }
        public String DESCRIPT3 { get; set; }
        public String DESCRIPT4 { get; set; }
        public String CITYCODE { get; set; }
        public String COUNTRYCODE { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String INVRMK1 { get; set; }
        public String INVRMK2 { get; set; }
        public String VCHRMK1 { get; set; }
        public String VCHRMK2 { get; set; }
        public String SELLCURR { get; set; }
        public Decimal TOTALSELL { get; set; }
        public Decimal TOTALSTAX { get; set; }
        public String COSTCURR { get; set; }
        public Decimal TOTALCOST { get; set; }
        public Decimal TOTALCTAX { get; set; }
        public String PCODE { get; set; }
        public DateTime VOIDON { get; set; }
        public String VOIDBY { get; set; }
        public String SOURCESYSTEM { get; set; }
        public String suppliercode { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String GSTTAX { get; set; }
        public String showDetailFee { get; set; }
        public String masterDesc { get; set; }
        public String IsRecalable { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOOTHER()
        {

        }

    }

    public class UO_PEOOTHERDETAIL
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String SEQNUM { get; set; }
        public String SEQTYPE { get; set; }
        public String AGETYPE { get; set; }
        public Int32 QTY { get; set; }
        public String CURR { get; set; }
        public Decimal AMT { get; set; }
        public String TAXCURR { get; set; }
        public Decimal TAXAMT { get; set; }
        public DateTime VOIDON { get; set; }
        public String VOIDBY { get; set; }
        public String SOURCESYSTEM { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public Decimal AMTFEE { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOOTHERDETAIL()
        {

        }

    }

    public class UO_PEOOTHERDETAILs : List<UO_PEOOTHERDETAIL> { }

    public class TicketSegment
    {
        public UO_PeoTkt UO_PeoTkt { get; set; }
        public List<UO_PeoTktDetail> UOList_PeoTktDetail { get; set; }

        public TicketSegment()
        {
            UOList_PeoTktDetail = new List<UO_PeoTktDetail>();
        }

    }

    public class TicketSegments : List<TicketSegment> { }

    public class UO_PeoTkt
    {
        public String CompanyCode { get; set; }
        public String Ticket { get; set; }
        public String FareCalData { get; set; }
        public String pseudo { get; set; }
        public String sinein { get; set; }
        public String AirSeg { get; set; }
        public String TransferFlag { get; set; }
        public String nst { get; set; }
        public String SOURCESYSTEM { get; set; }
        public Decimal commission_per { get; set; }
        public String add_payment { get; set; }
        public String iss_off_code { get; set; }
        public String tour_code { get; set; }
        public String org_iss { get; set; }
        public String iss_in_exchange { get; set; }
        public String ControlNum { get; set; }
        public String ENDORSEMENTS { get; set; }
        public String AirReason { get; set; }
        public String Destination { get; set; }
        public String INTDOM { get; set; }
        public String PAXAIR { get; set; }
        public String Class { get; set; }
        public String tkttype { get; set; }
        public String SellCurr { get; set; }
        public Decimal SellFare { get; set; }
        public String CorpCurr { get; set; }
        public Decimal CorpFare { get; set; }
        public String COD4 { get; set; }
        public String COD5 { get; set; }
        public String COD6 { get; set; }
        public String tranflag { get; set; }
        public String TktSeq { get; set; }
        public String BKGREF { get; set; }
        public String PNR { get; set; }
        public String CltCode { get; set; }
        public String SuppCode { get; set; }
        public String InvNum { get; set; }
        public Decimal Jqty { get; set; }
        public String AirCode { get; set; }
        public String Airline { get; set; }
        public String PaxName { get; set; }
        public DateTime DepartOn { get; set; }
        public String COD1 { get; set; }
        public String COD2 { get; set; }
        public String COD3 { get; set; }
        public Decimal Commission { get; set; }
        public String LocalFareCurr { get; set; }
        public Decimal LocalFareAmt { get; set; }
        public String ForeignFareCurr { get; set; }
        public Decimal ForeignFareAmt { get; set; }
        public Decimal Discount { get; set; }
        public String FormPay { get; set; }
        public String Routing { get; set; }
        public String FullCurr { get; set; }
        public Decimal FullFare { get; set; }
        public String PaidCurr { get; set; }
        public Decimal PaidFare { get; set; }
        public String LowCurr { get; set; }
        public Decimal LowFare { get; set; }
        public String NetCurr { get; set; }
        public Decimal NetFare { get; set; }
        public String FareBasis { get; set; }
        public String FareType { get; set; }
        public Decimal TotalTax { get; set; }
        public Decimal Security { get; set; }
        public Decimal QSurcharge { get; set; }
        public String BranchCode { get; set; }
        public String TeamCode { get; set; }
        public DateTime IssueOn { get; set; }
        public String IssueBy { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public DateTime VoidOn { get; set; }
        public String VoidBy { get; set; }
        public String voidTeam { get; set; }
        public String voidreason { get; set; }
        public String TaxAmount1 { get; set; }
        public String TaxID1 { get; set; }
        public String TaxAmount2 { get; set; }
        public String TaxID2 { get; set; }
        public String TaxAmount3 { get; set; }
        public String TaxID3 { get; set; }
        public String FareCalType { get; set; }
        public String CompressPrintInd { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PeoTkt()
        {

        }

    }

    public class UO_PeoTktDetail
    {
        public String CompanyCode { get; set; }
        public String Ticket { get; set; }
        public String AirCode { get; set; }
        public String TktSeq { get; set; }
        public String SegNum { get; set; }
        public String Airline { get; set; }
        public String Flight { get; set; }
        public String Class { get; set; }
        public String Seat { get; set; }
        public String DepartCity { get; set; }
        public String ArrivalCity { get; set; }
        public DateTime DepartDATE { get; set; }
        public String DepartTime { get; set; }
        public DateTime ArrDATE { get; set; }
        public String ArrTime { get; set; }
        public String ElapsedTime { get; set; }
        public Int32 StopOverNum { get; set; }
        public String StopOverCity { get; set; }
        public Decimal FareAmt { get; set; }
        public String FareBasic { get; set; }
        public Decimal Commission { get; set; }
        public DateTime ValidStart { get; set; }
        public DateTime ValidEnd { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public String EQP { get; set; }
        public String Service { get; set; }
        public String status { get; set; }
        public String fare_basis_code { get; set; }
        public String DepartTerm { get; set; }
        public String ArrivalTerm { get; set; }
        public String freq_fight_num { get; set; }
        public String AirlinePNR { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PeoTktDetail()
        {

        }

    }

    public class UO_PeoTktDetails : List<UO_PeoTktDetail> { }

    public class UO_PeoPax
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String PAXNUM { get; set; }
        public String PAXLNAME { get; set; }
        public String PAXFNAME { get; set; }
        public String PAXTITLE { get; set; }
        public String PAXTYPE { get; set; }
        public String PAXAIR { get; set; }
        public String PAXHOTEL { get; set; }
        public String PAXOTHER { get; set; }
        public Int16 PAXAGE { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String VOIDON { get; set; }
        public DateTime VOIDBY { get; set; }
        public String SOURCESYSTEM { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PeoPax()
        {

        }

    }

    public class UO_PeoPaxs : List<UO_PeoPax> { }

    public class UO_CUSTOMER
    {
        public String COMPANYCODE { get; set; }
        public String CLTCODE { get; set; }
        public String CLTNAME { get; set; }
        public String BRANCHCODE { get; set; }
        public String TEAMCODE { get; set; }
        public String MSTRCLT { get; set; }
        public String ADDR1 { get; set; }
        public String ADDR2 { get; set; }
        public String ADDR3 { get; set; }
        public String ADDR4 { get; set; }
        public String ADDR5 { get; set; }
        public String POSTAL { get; set; }
        public String CITY { get; set; }
        public String COUNTRY { get; set; }
        public String BILLADDR1 { get; set; }
        public String BILLADDR2 { get; set; }
        public String BILLADDR3 { get; set; }
        public String BILLADDR4 { get; set; }
        public String BILLADDR5 { get; set; }
        public String BILLPOSTAL { get; set; }
        public String BILLCITY { get; set; }
        public String BILLCOUNTRY { get; set; }
        public String STATUS { get; set; }
        public String CONTACT1 { get; set; }
        public String CONTACT2 { get; set; }
        public String JOBTITLE1 { get; set; }
        public String JOBTITLE2 { get; set; }
        public String PHONE1 { get; set; }
        public String FAX1 { get; set; }
        public String CRTERMS { get; set; }
        public Decimal PRFTUP { get; set; }
        public Decimal PRFTDOWN { get; set; }
        public String CURRENCY { get; set; }
        public Decimal PFDAY { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String email { get; set; }
        public String CustRemarks { get; set; }
        public String GEMSCODE { get; set; }
        public String CLTNAME1 { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_CUSTOMER()
        {

        }

    }

    public enum AutoBookingMessages
    {
        Success = 0,
        Warning_Pnr_Not_Found = 1,
        Warning_Ticket_Not_Found = 2,
        Error_Client_Not_Found = 3,
        Error_Ticket_Invoiced = 4,
        EInvoice_Exception = 5,
        Unknown_Error = 6,
    }
    //public enum GDS_Types
    //{
    //    Abacus = 0,
    //    Galileo = 1,
    //    TravelSky = 2,
    //}
    public enum GDS_Types
    {
        Abacus,
        Galileo,
        Amadeus,
        TravelSky,
        Worldspan,
    }

}
