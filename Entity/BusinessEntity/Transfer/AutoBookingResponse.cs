﻿using System;
using System.Collections.Generic;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    public class AutoBookingResponse
    {
        public AutoBookingMessages Message { get; set; }
        public Int32 Status { get; set; }
        public String Invnum { get; set; }
        public String Exception { get; set; }

        public AutoBookingResponse()
        {

        }
    }
    public class eMosParameter
    {
        public String Companycode { get; set; }
        public String Currency { get; set; }
        public String Remark { get; set; }
        public String CltCode { get; set; }
        public String CltRef { get; set; }
        public String Consultant { get; set; }
        public String RLOC { get; set; }
        public String Adcomtxno { get; set; }
        public String TSAno { get; set; }
        public String TourCode { get; set; }
        public String Email { get; set; }
        public Int32 SourceSystem { get; set; }
        public Boolean IsSubAgent { get; set; }
        public String TeamCode { get; set; }
        public String StaffCode { get; set; }
        public String SuppCode { get; set; }
        public String FollwUpBy { get; set; }
        public List<UO_Air> AirSegments { get; set; }
        public List<UO_Ticket> Tickets { get; set; }
        public List<UO_Other> OtherSegments { get; set; }
        public eInvCardType InvCardType { get; set; }

        public eMosParameter()
        {

        }
    }

    public class UO_Air
    {
        public String Airline { get; set; }
        public String Segnum { get; set; }
        public String Flight { get; set; }
        public String OAC { get; set; }
        public String OAC_Terminal { get; set; }
        public String DAC { get; set; }
        public String DAC_Terminal { get; set; }
        public String Class { get; set; }
        public DateTime DepartDateTime { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public Boolean IsGdsFare { get; set; }
        public Boolean IsDepartSegment { get; set; }

        public UO_Air()
        {

        }
    }

    public class UO_Airs : List<UO_Air> { }

    public class UO_Ticket
    {
        public String Airline { get; set; }
        public String Ticket { get; set; }
        public String[] SegnumList { get; set; }
        public String PaxGivenName { get; set; }
        public String PaxSurName { get; set; }
        public String PaxNum { get; set; }
        public String PaxType { get; set; }
        public String PaxTitle { get; set; }
        public Int32 PaxAge { get; set; }
        public String Currency { get; set; }
        public Decimal SellAmt { get; set; }
        public Decimal NetAmt { get; set; }
        public Decimal TotalTax { get; set; }
        public DateTime TicketIssueOn { get; set; }
        public Decimal NetNet { get; set; }
        public String TourCode { get; set; }
        public Decimal Commission { get; set; }
        public String Destination { get; set; }
        public String UATP_Card { get; set; }
        public String UATP_Card_Type { get; set; }
        public String UATP_Card_holder { get; set; }
        public String UATP_CardExpireDate { get; set; }
        public String INTDOM { get; set; }
        public DateTime DepartOn { get; set; }
        public String FormPay { get; set; }

        public UO_Ticket()
        {

        }
    }

    public class UO_Tickets : List<UO_Ticket> { }

    public class UO_Other
    {
        public String Segnum { get; set; }
        public String Servdesc { get; set; }
        public String Segtype { get; set; }
        public String Suppliercode { get; set; }
        public DateTime OtherDate { get; set; }
        public List<UO_OtherDetail> List_OtherDetail { get; set; }

        public UO_Other()
        {

        }
    }

    public class UO_Others : List<UO_Other> { }

    public class UO_OtherDetail
    {
        public String Segnum { get; set; }
        public String Seqnum { get; set; }
        public String AGETYPE { get; set; }
        public String SEQTYPE { get; set; }
        public Int32 QTY { get; set; }
        public String CURR { get; set; }
        public Decimal AMT { get; set; }
        public Decimal TAXAMT { get; set; }

        public UO_OtherDetail()
        {

        }
    }

    public class UO_OtherDetails : List<UO_OtherDetail> { }
   

    
    public enum eInvCardType
    {
        Paypal,
        Cash,
        CreditCard,
        Cheque,
        Others,
    }
  
    //public enum AutoBookingMessages
    //{
    //    Success,
    //    Warning_Pnr_Not_Found,
    //    Warning_Ticket_Not_Found,
    //    Error_Client_Not_Found,
    //    Error_Ticket_Invoiced,
    //    EInvoice_Exception,
    //    Unknown_Error,
    //}

}
