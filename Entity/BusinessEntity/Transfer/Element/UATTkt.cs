﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.BLLEntity.Transfer.Element
{
    [Serializable]
    public class UATTkt
    {
        public tbPEOTKT PeoTkt { get; set; }
        public tbPEOTKTDETAILs PeoTktDetails { get; set; }
    }
}
