﻿using System;
using System.Collections.Generic;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    public class TsfrHotel
    {
        public UOList_PEOPAX PEOPAX { get; set; }
        public UOList_PEOHTL PEOHTL { get; set; }
        public UOList_PeoHtlDetail PeoHtlDetail { get; set; }

        public TsfrHotel()
        {

        }
    }

    public class UOList_PEOPAX
    {
        public Int32 Capacity { get; set; }
        public Int32 Count { get; set; }
        public UO_PEOPAX Item { get; set; }

        public UOList_PEOPAX()
        {

        }
    }

    public class UO_PEOPAX
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String PAXNUM { get; set; }
        public String PAXLNAME { get; set; }
        public String PAXFNAME { get; set; }
        public String PAXTITLE { get; set; }
        public String PAXTYPE { get; set; }
        public String PAXAIR { get; set; }
        public String PAXHOTEL { get; set; }
        public String PAXOTHER { get; set; }
        public Int16 PAXAGE { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String VOIDON { get; set; }
        public DateTime VOIDBY { get; set; }
        public String SOURCESYSTEM { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String Nationality { get; set; }
        public String PassportNo { get; set; }
        public DateTime PassportExpireDate { get; set; }
        public DateTime DateofBirth { get; set; }
        public String Visa { get; set; }
        public String ContactHomeTel { get; set; }
        public String ContactMobileTel { get; set; }
        public String ContactOfficeTel { get; set; }
        public String Email { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String Address4 { get; set; }
        public String Source { get; set; }
        public String SourceRef { get; set; }
        public String MemberShipNo { get; set; }
        public String deleted { get; set; }
        public String evisa_import { get; set; }
        public DateTime evisa_import_datetime { get; set; }
        public String SOURCETYPE { get; set; }
        public String ADCOMTXNO { get; set; }
        public String PAXNAME1 { get; set; }
        public Object Item { get; set; }
        //public ConnectionInformation ConnInfo { get; set; }

        public UO_PEOPAX()
        {

        }
    }

   
    public class UOList_PEOHTL
    {
        public Int32 Capacity { get; set; }
        public Int32 Count { get; set; }
        public UO_PEOHTL Item { get; set; }

        public UOList_PEOHTL()
        {

        }
    }

   
    public class UOList_PeoHtlDetail
    {
        public Int32 Capacity { get; set; }
        public Int32 Count { get; set; }
        public UO_PeoHtlDetail Item { get; set; }

        public UOList_PeoHtlDetail()
        {

        }
    }

    

}
