﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Transfer;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    public class RetrieveBooking_IURParam
    {
        public string CompanyCode { get; set; }
        public string PNR { get; set; }
        public GDS_Types GDSType { get; set; }
        public string[] Tickets { get; set; }
        public string CltCode { get; set; }
        public string ADComTxNum { get; set; }
        public string TourCode { get; set; }
        public string LoginUser { get; set; }
        public string TSANumber { get; set; }
        public Invoice_Types InvType { get; set; }

        public RetrieveBooking_IURParam()
        {

        }
    }
}
