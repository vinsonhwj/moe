﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Transfer;

namespace Westminster.MOE.Entity.BLLEntity.Transfer
{
    public class AutoBookingParam
    {
        public RetrieveIURBooking Booking { get; set; }
        public eMosParameter eMosParam { get; set; }
        public List<AirlineSegment> AirSegment4Invoice { get; set; }
        public string InvRemark { get; set; }
        public string StaffCode { get; set; }
        public string TeamCode { get; set; }
        public bool IsAllowInvoice { get; set; }
        public bool IsAllowEInvoice { get; set; }
        public string Recipient { get; set; }
        public Invoice_Types InvType { get; set; }

        public AutoBookingParam()
        {

        }
    }
}
