﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.Configuration
{
    public class DefaultSetting
    {
        public string AutoBookingOwner { get; set; }
        public string AutoBookingOwnerTeamCode { get; set; }

        public DefaultSetting() { }
    }
}
