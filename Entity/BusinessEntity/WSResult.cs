﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;

namespace Westminster.MOE.Entity.BLLEntity
{
    public class WSResult
    {
        public MOEEnums.ResultStatus Status { get; set; }
        public string Msg { get; set; }
        public string ResultCode { get; set; }
        public string Ext1 { get; set; }
        public string Ext2 { get; set; }

        public WSResult()
        {
            Status = MOEEnums.ResultStatus.None;
            Msg = string.Empty;
        }
        public WSResult(MOEEnums.ResultStatus status, string msg)
        {
            Status = status;
            Msg = msg;
        }
    }
}
