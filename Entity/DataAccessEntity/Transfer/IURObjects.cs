﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    public class IURObjects
    {
        public tbPEOMSTR_IUR PEOMSTR { get; set; }

        public tbPEOPAX_IURs PaxList { get; set; }
        public tbPEOAIR_IURs AirList { get; set; }
        //public tbPEOHTLs HTLList { get; set; }
        //public tbPeoHtlDetails HTLDetailList { get; set; }
        public tbPEOAIRDETAIL_IURs AirDetailList { get; set; }
        public tbPeoAirPax_IURs PaxAirList { get; set; }

        //public tbPEOOTHERs OtherList { get; set; }
        //public tbPEOOTHERDETAILs OtherDetailList { get; set; }
        public tbPEOTKT_IURs TicketList { get; set; }
        public tbPEOTKTDETAIL_IURs TicketDetailList { get; set; }
        public tbpeotrans_IURs TranList { get; set; }
        public tbPEONYREF_IUR Nyref { get; set; }

        public tbPEOTKT_IUR VoidTicket { get; set; }

    }
}
