﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    public class CloneBookingInfo
    {
        public tbPEOSTAFF Staff { get; set; }
        public tbPEOMSTR Master { get; set; }
        public tbPEOPAXs PaxList { get; set; }
        public tbPEOAIRs AirList { get; set; }
        public tbPEOAIRDETAILs AirDetailList { get; set; }
        public tbPEOHTLs HtlList { get; set; }
        public tbPeoHtlDetails HtlDetailList { get; set; }
        public tbPEOOTHERs OtherList { get; set; }
        public tbPEOOTHERDETAILs OtherDetailList { get; set; }
        public tbLOGIN Login { get; set; }
        public tbPEONYREF NyRef { get; set; }
    }
}
