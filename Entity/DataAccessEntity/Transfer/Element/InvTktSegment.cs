﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Transfer.Element
{
    public class InvTktSegment
    {
        #region Property
        private string _BkgRef;
        public string BkgRef
        {
            get { return _BkgRef; }
            set { _BkgRef = value; }
        }

        private string _Ticket;
        public string Ticket
        {
            get { return _Ticket; }
            set { _Ticket = value; }
        }

        private DateTime _IssueOn;
        public DateTime IssueOn
        {
            get { return _IssueOn; }
            set { _IssueOn = value; }
        }

        private DateTime _FirstDepartOn;
        public DateTime FirstDepartOn
        {
            get { return _FirstDepartOn; }
            set { _FirstDepartOn = value; }
        }

        private string _DepartTime;
        public string DepartTime
        {
            get { return _DepartTime; }
            set { _DepartTime = value; }
        }

        private string _AirCode;
        public string AirCode
        {
            get { return _AirCode; }
            set { _AirCode = value; }
        }

        private string _AirLine;
        public string AirLine
        {
            get { return _AirLine; }
            set { _AirLine = value; }
        }

        private string _Flights;
        public string Flights
        {
            get { return _Flights; }
            set { _Flights = value; }
        }

        private string _Routing;
        public string Routing
        {
            get { return _Routing; }
            set { _Routing = value; }
        }

        private string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        private string _PaxName;
        public string PaxName
        {
            get { return _PaxName; }
            set { _PaxName = value; }
        }

        private string _CostCurr;
        public string CostCurr
        {
            get { return _CostCurr; }
            set { _CostCurr = value; }
        }

        private decimal _CostAmt;
        public decimal CostAmt
        {
            get { return _CostAmt; }
            set { _CostAmt = value; }
        }

        private decimal _CostTax;
        public decimal CostTax
        {
            get { return _CostTax; }
            set { _CostTax = value; }
        }

        private string _SellCurr;
        public string SellCurr
        {
            get { return _SellCurr; }
            set { _SellCurr = value; }
        }

        private decimal _SellAmt;
        public decimal SellAmt
        {
            get { return _SellAmt; }
            set { _SellAmt = value; }
        }

        private decimal _SellTax;
        public decimal SellTax
        {
            get { return _SellTax; }
            set { _SellTax = value; }
        }

        private string _INTDOM;
        public string INTDOM
        {
            get { return _INTDOM; }
            set { _INTDOM = value; }
        }

        private bool _IsMisTicket;
        public bool IsMisTicket
        {
            get { return _IsMisTicket; }
            set { _IsMisTicket = value; }
        }

        #endregion

        public InvTktSegment()
        {

        }

        //PEOTKT.BKGREF, PEOTKT.Ticket, PEOTKT.Airline, PEOTKT.Routing,
        //PEOTKT.FullFare AS Fare, PEOTKT.TotalTax AS Tax, PEOTKT.DepartOn,
        //PEOTKT.PaxName, peotkt.paxair, peotkt.INTDOM
    }
}
