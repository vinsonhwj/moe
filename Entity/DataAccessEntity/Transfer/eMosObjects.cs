﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;



namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    public class eMosObjects
    {
        public bool isCreate = false;
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SourcrPnr { get; set; }
        public tbPEOMSTR PEOMSTR { get; set; }
        public tbPEOPAXs PaxList { get; set; }

        public tbPEOAIRs AirList { get; set; }
        public tbPEOAIRDETAILs AirDetailList { get; set; }

        //public tbPEOHTLs HTLList { get; set; }
        //public tbPeoHtlDetails HTLDetailList { get; set; }

        public tbPEOOTHERs OtherList { get; set; }
        public tbPEOOTHERDETAILs OtherDetailList { get; set; }

        public tbPEOTKTs UpdateVoidTicketList { get; set; }
        public tbPEOTKTs TicketList { get; set; }
        public tbPEOTKTDETAILs TicketDetailList { get; set; }

        public eMosObjects()
            : this(false)
        {

        }

        public eMosObjects(bool instace)
        {
            if (instace)
            {
                PaxList = new tbPEOPAXs();
                AirList = new tbPEOAIRs();
                AirDetailList = new tbPEOAIRDETAILs();
                OtherList = new tbPEOOTHERs();
                OtherDetailList = new tbPEOOTHERDETAILs();
                UpdateVoidTicketList = new tbPEOTKTs();
                TicketList = new tbPEOTKTs();
                TicketDetailList = new tbPEOTKTDETAILs();
            }
        }

    }
}
