﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.IURDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    public class IURBookingInfo
    {
        public tbPEOMSTR_IUR Master { get; set; }
        public tbPEOPAX_IURs PaxList { get; set; }
        public AirSegments AirSegmentLst { get; set; }

        public IURBookingInfo()
        {
            AirSegmentLst = new AirSegments();
        }
        public class AirSegment
        {
            public tbPEOAIR_IUR Air { get; set; }
            public tbPEOAIRDETAIL_IURs AirDetailList { get; set; }
            public AirSegment()
            {
                AirDetailList = new tbPEOAIRDETAIL_IURs();
            }
        }
        public class AirSegments : List<AirSegment> { }
    }
}
