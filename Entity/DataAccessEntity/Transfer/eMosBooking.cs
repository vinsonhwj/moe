﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    /// <summary>
    /// For AutoTicket Only Now...
    /// </summary>
    public class eMosBooking
    {
        public string CompanyCode { get; set; }
        public string Bkgref { get; set; }
        public tbPEOMIS PeoMis { get; set; }
        public tbPEOMSTR PeoMstr { get; set; }
        public tbPEOPAXs PeoPaxList { get; set; }
        public eMosAirlineSegments AirlineSegments { get; set; }
        public eMosHotelSegments HotelSegments { get; set; }
        public eMosOtherSegments OtherSegments { get; set; }
        public eMosTicketSegments TicketSegments { get; set; }
        public tbPEOTKTs VoidTicket { get; set; }
        public eMosBooking(string companyCode, string bkgref)
        {
            CompanyCode = companyCode;
            Bkgref = bkgref;

        }
        public eMosBooking(string companyCode, string bkgref, bool instace)
        {
            CompanyCode = companyCode;
            Bkgref = bkgref;

            if (instace)
            {
                PeoMstr = new tbPEOMSTR();
                PeoMis = new tbPEOMIS();
                PeoPaxList = new tbPEOPAXs();
                AirlineSegments = new eMosAirlineSegments();
                HotelSegments = new eMosHotelSegments();
                OtherSegments = new eMosOtherSegments();
                TicketSegments = new eMosTicketSegments();
                VoidTicket = new tbPEOTKTs();
            }
        }
    }
    public class eMosAirlineSegment
    {
        public tbPEOAIR PeoAir { get; set; }
        public tbPEOAIRDETAILs PeoAirDetails { get; set; }
        public eMosAirlineSegment()
        {
            PeoAir = new tbPEOAIR();
            PeoAirDetails = new tbPEOAIRDETAILs();
        }
    }
    public class eMosAirlineSegments : List<eMosAirlineSegment> { }
    public class eMosHotelSegment
    {
        public tbPEOHTL PeoHtl { get; set; }
        public tbPeoHtlDetails PeoHtlDetails { get; set; }
        public eMosHotelSegment()
        {
            PeoHtl = new tbPEOHTL();
            PeoHtlDetails = new tbPeoHtlDetails();
        }
    }
    public class eMosHotelSegments : List<eMosHotelSegment> { }
    public class eMosOtherSegment
    {
        public tbPEOOTHER PeoOther { get; set; }

        public tbPEOOTHERDETAILs PeoOtherDetails { get; set; }
        public eMosOtherSegment()
        {
            PeoOther = new tbPEOOTHER();
            PeoOtherDetails = new tbPEOOTHERDETAILs();
        }
    }
    public class eMosOtherSegments : List<eMosOtherSegment> { }

    public class eMosTicketSegment
    {
        public tbPEOTKT PeoTkt { get; set; }

        public tbPEOTKTDETAILs PeoTktDetails { get; set; }
        public eMosTicketSegment()
        {
            PeoTkt = new tbPEOTKT();
            PeoTktDetails = new tbPEOTKTDETAILs();
        }
    }
    public class eMosTicketSegments : List<eMosTicketSegment> { }
}
