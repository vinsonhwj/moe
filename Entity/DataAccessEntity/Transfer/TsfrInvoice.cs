﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.DALEntity.Transfer.Element;

namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    [Serializable]
    public class TsfrInvoice
    {
        #region Attributes

        public tbPEOMSTR PEOMSTR_PNR { get; set; }
        public tbPEOMSTR PEOMSTR { get; set; }
        public tbPEOINV PeoInv { get; set; }
        public tbPEOINVSEGs PEOINVSEG { get; set; }
        public tbPEOINVDETAILs PEOINVDETAIL { get; set; }
        public tbPEOINPAXs PEOINPAX { get; set; }
        public tbPEOINVTKTrefs PEOINVTKTref { get; set; }
        public tbPEOINVTKTs PEOINVTKT { get; set; }
        public tbCreditcardsales Creditcardsales { get; set; }

        private List<InvTkt> _InvTkts;
        public List<InvTkt> InvTkts
        {
            get { return _InvTkts; }
            set { _InvTkts = value; }
        }

        private List<UATTkt> _UATTkts;
        public List<UATTkt> UATTkts
        {
            get { return _UATTkts; }
            set { _UATTkts = value; }
        }

        private List<InvTktSegment> _InvTktSegments;
        public List<InvTktSegment> InvTktSegments
        {
            get { return _InvTktSegments; }
            set { _InvTktSegments = value; }
        }
        #endregion
    }

    public enum InvoiceTypeEnum
    {
        Normal,
        Uatp,
        Credit,
        Deposit
    }
    public enum Invoice_Types
    {
        Normal = 0,
        CreditNotes = 1,
        UATP = 2,
        CreditCardSales = 3,
        Combined = 4,
        Deposit = 5,
        DepositCreditNote = 6,
        NRCC = 7,
    }
}
