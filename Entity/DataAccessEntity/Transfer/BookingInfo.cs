﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Transfer
{
    public class BookingInfo
    {
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SourcrPnr { get; set; }

        public tbPEOMSTR Master { get; set; }

        public tbPEOPAXs PaxList { get; set; }
        public tbPEOAIRs AirList { get; set; }
        public tbPEOAIRDETAILs AirDetailList { get; set; }

        public BookingInfo()
        {
            Master = new tbPEOMSTR();
            PaxList = new tbPEOPAXs();
            AirList = new tbPEOAIRs();
            AirDetailList = new tbPEOAIRDETAILs();
        }

        public class AirSegmentList
        {
            public tbPEOAIRs AirList { get; set; }
            public tbPEOAIRDETAILs AirDetailList { get; set; }
            public AirSegmentList()
            {
                AirList = new tbPEOAIRs();
                AirDetailList = new tbPEOAIRDETAILs();
            }
        }
        public class AirSegment
        {
            public tbPEOAIR Air { get; set; }
            public tbPEOAIRDETAILs AirDetailList { get; set; }
            public AirSegment()
            {
                Air = new tbPEOAIR();
                AirDetailList = new tbPEOAIRDETAILs();
            }
        }
    }
}
