﻿using System;
using System.Collections.Generic;
namespace Westminster.MOE.Entity.DALEntity.Gateway
{

    public class IURObject
    {
        public tbPEOMSTR PEOMSTR { get; set; }
        public tbPEOPAXs PaxList { get; set; }
        public tbPEOAIRs AirList { get; set; }
        public tbPEOAIRDETAILs AirDetailList { get; set; }
        public tbPeoAirPaxs PaxAirList { get; set; }
        public tbPEOTKTs TicketList { get; set; }
        public tbPEOTKTDETAILs TicketDetailList { get; set; }
        public tbpeotranss TranList { get; set; }
        public tbPEONYREF Nyref { get; set; }
        public tbPEOTKT VoidTicket { get; set; }

        public IURObject()
        {

        }

    }

    public class tbPEOMSTR
    {
        public String COMPANYCODE { get; set; }
        public String BKGREF { get; set; }
        public String PNR { get; set; }
        public String CRSSINEIN { get; set; }
        public String COD1 { get; set; }
        public String COD2 { get; set; }
        public String CLTODE { get; set; }
        public String CLTNAME { get; set; }
        public String CLTADDR { get; set; }
        public String STAFFCODE { get; set; }
        public String TEAMCODE { get; set; }
        public String FIRSTPAX { get; set; }
        public String PHONE { get; set; }
        public String FAX { get; set; }
        public String EMAIL { get; set; }
        public String MASTERPNR { get; set; }
        public String TOURCODE { get; set; }
        public String CONTACTPERSON { get; set; }
        public DateTime DEPARTDATE { get; set; }
        public String TTLSELLCURR { get; set; }
        public Decimal TTLSELLAMT { get; set; }
        public String TTLSELLTAXCURR { get; set; }
        public Decimal TTLSELLTAX { get; set; }
        public String TTLCOSTCURR { get; set; }
        public Decimal TTLCOSTAMT { get; set; }
        public String TTLCOSTTAXCURR { get; set; }
        public Decimal TTLCOSTTAX { get; set; }
        public String INVCURR { get; set; }
        public Decimal INVAMT { get; set; }
        public String INVTAXCURR { get; set; }
        public Decimal INVTAX { get; set; }
        public String DOCCURR { get; set; }
        public Decimal DOCAMT { get; set; }
        public String DOCTAXCURR { get; set; }
        public Decimal DOCTAX { get; set; }
        public Int32 INVCOUNT { get; set; }
        public Int32 DOCCOUNT { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String RMK3 { get; set; }
        public String RMK4 { get; set; }
        public DateTime DEADLINE { get; set; }
        public String SOURCESYSTEM { get; set; }
        public String SOURCE { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String COD3 { get; set; }
        public String INSFLAG { get; set; }
        public DateTime TOEMOSON { get; set; }
        public String TOEMOSBY { get; set; }
        public String EMOSBKGREF { get; set; }
        public String IURFLAG { get; set; }
        public String pseudo { get; set; }
        public String pseudo_inv { get; set; }
        public String CRSSINEIN_inv { get; set; }
        public String agency_arc_iata_num { get; set; }
        public String iss_off_code { get; set; }
        public String tranflag { get; set; }

        public tbPEOMSTR()
        {

        }

    }
    public class tbPEOMSTRs : List<tbPEOMSTR> { }

    public class tbPEONYREF
    {
        public Decimal REFID { get; set; }
        public String COMPANYCODE { get; set; }
        public String BKGREF { get; set; }
        public String STAFFCODE { get; set; }
        public String TEAMCODE { get; set; }
        public String ACTIONCODE { get; set; }
        public String CLTCODE { get; set; }
        public String SUPPCODE { get; set; }
        public String DocNum { get; set; }
        public String SellCurr { get; set; }
        public Decimal SellAmt { get; set; }
        public String SellTaxCurr { get; set; }
        public Decimal SellTaxAmt { get; set; }
        public String CostCurr { get; set; }
        public Decimal CostAmt { get; set; }
        public String CostTaxCurr { get; set; }
        public Decimal CostTaxAmt { get; set; }
        public String Description { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }

        public tbPEONYREF()
        {

        }

    }
    public class tbPEONYREFs : List<tbPEONYREF> { }

    public class tbpeotrans
    {
        public String bkgref { get; set; }
        public String companycode { get; set; }
        public String PAXNUM { get; set; }
        public String segnum { get; set; }
        public String CITYCode { get; set; }
        public String CarType { get; set; }
        public String DeptTime { get; set; }
        public String FromType { get; set; }
        public String FromAdd { get; set; }
        public String ToType { get; set; }
        public String ToAdd { get; set; }
        public String FlightDetail { get; set; }
        public String CarNo { get; set; }
        public String ConfirmedBy { get; set; }
        public String tempkey { get; set; }
        public DateTime ConfirmedDate { get; set; }
        public String createon { get; set; }
        public String createby { get; set; }
        public String updateon { get; set; }
        public String updateby { get; set; }

        public tbpeotrans()
        {

        }

    }
    public class tbpeotranss : List<tbpeotrans> { }

    public class tbPEOTKTDETAIL
    {
        public String CompanyCode { get; set; }
        public String Ticket { get; set; }
        public String AirCode { get; set; }
        public String TktSeq { get; set; }
        public String SegNum { get; set; }
        public String Airline { get; set; }
        public String Flight { get; set; }
        public String Class { get; set; }
        public String Seat { get; set; }
        public String DepartCity { get; set; }
        public String ArrivalCity { get; set; }
        public DateTime DepartDATE { get; set; }
        public String DepartTime { get; set; }
        public DateTime ArrDATE { get; set; }
        public String ArrTime { get; set; }
        public String ElapsedTime { get; set; }
        public Int32 StopOverNum { get; set; }
        public String StopOverCity { get; set; }
        public Decimal FareAmt { get; set; }
        public String FareBasic { get; set; }
        public Decimal Commission { get; set; }
        public DateTime ValidStart { get; set; }
        public DateTime ValidEnd { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public String EQP { get; set; }
        public String Service { get; set; }
        public String status { get; set; }
        public String fare_basis_code { get; set; }
        public String DepartTerm { get; set; }
        public String ArrivalTerm { get; set; }
        public String freq_fight_num { get; set; }
        public String AirlinePNR { get; set; }

        public tbPEOTKTDETAIL()
        {

        }

    }
    public class tbPEOTKTDETAILs : List<tbPEOTKTDETAIL> { }

    public class tbPEOTKT
    {
        public String CompanyCode { get; set; }
        public String Ticket { get; set; }
        public String TktSeq { get; set; }
        public String BKGREF { get; set; }
        public String PNR { get; set; }
        public String CltCode { get; set; }
        public String SuppCode { get; set; }
        public String InvNum { get; set; }
        public Decimal Jqty { get; set; }
        public String AirCode { get; set; }
        public String Airline { get; set; }
        public String PaxName { get; set; }
        public DateTime DepartOn { get; set; }
        public String COD1 { get; set; }
        public String COD2 { get; set; }
        public String COD3 { get; set; }
        public String COD7 { get; set; }
        public Decimal Commission { get; set; }
        public String LocalFareCurr { get; set; }
        public Decimal LocalFareAmt { get; set; }
        public String ForeignFareCurr { get; set; }
        public Decimal ForeignFareAmt { get; set; }
        public Decimal Discount { get; set; }
        public String FormPay { get; set; }
        public String Routing { get; set; }
        public String FullCurr { get; set; }
        public Decimal FullFare { get; set; }
        public String PaidCurr { get; set; }
        public Decimal PaidFare { get; set; }
        public String LowCurr { get; set; }
        public Decimal LowFare { get; set; }
        public String NetCurr { get; set; }
        public Decimal NetFare { get; set; }
        public String FareBasis { get; set; }
        public String FareType { get; set; }
        public Decimal TotalTax { get; set; }
        public Decimal Security { get; set; }
        public Decimal QSurcharge { get; set; }
        public String BranchCode { get; set; }
        public String TeamCode { get; set; }
        public DateTime IssueOn { get; set; }
        public String IssueBy { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public DateTime VoidOn { get; set; }
        public String VoidBy { get; set; }
        public String voidTeam { get; set; }
        public String voidreason { get; set; }
        public String TaxAmount1 { get; set; }
        public String TaxID1 { get; set; }
        public String TaxAmount2 { get; set; }
        public String TaxID2 { get; set; }
        public String TaxAmount3 { get; set; }
        public String TaxID3 { get; set; }
        public String FareCalType { get; set; }
        public String CompressPrintInd { get; set; }
        public String FareCalData { get; set; }
        public String pseudo { get; set; }
        public String sinein { get; set; }
        public String AirSeg { get; set; }
        public String TransferFlag { get; set; }
        public String nst { get; set; }
        public String SOURCESYSTEM { get; set; }
        public Decimal commission_per { get; set; }
        public String add_payment { get; set; }
        public String iss_off_code { get; set; }
        public String tour_code { get; set; }
        public String org_iss { get; set; }
        public String iss_in_exchange { get; set; }
        public String ControlNum { get; set; }
        public String ENDORSEMENTS { get; set; }
        public String AirReason { get; set; }
        public String Destination { get; set; }
        public String INTDOM { get; set; }
        public String PAXAIR { get; set; }
        public String Class { get; set; }
        public String tkttype { get; set; }
        public String SellCurr { get; set; }
        public Decimal SellFare { get; set; }
        public String CorpCurr { get; set; }
        public Decimal CorpFare { get; set; }
        public String COD4 { get; set; }
        public String COD5 { get; set; }
        public String COD6 { get; set; }
        public String tranflag { get; set; }

        public tbPEOTKT()
        {

        }

    }
    public class tbPEOTKTs : List<tbPEOTKT> { }

    public class tbPeoAirPax
    {
        public String COMPANYCODE { get; set; }
        public String BKGREF { get; set; }
        public String SEGNUM { get; set; }
        public String PAXNUM { get; set; }
        public String Freq_Fight_num { get; set; }

        public tbPeoAirPax()
        {

        }

    }
    public class tbPeoAirPaxs : List<tbPeoAirPax> { }

    public class tbPEOAIRDETAIL
    {
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String SEQNUM { get; set; }
        public String SEQTYPE { get; set; }
        public Int32 QTY { get; set; }
        public String CURR { get; set; }
        public Decimal AMT { get; set; }
        public String TAXCURR { get; set; }
        public Decimal TAXAMT { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }
        public String AGETYPE { get; set; }

        public tbPEOAIRDETAIL()
        {

        }

    }
    public class tbPEOAIRDETAILs : List<tbPEOAIRDETAIL> { }

    public class tbPEOAIR
    {
        public String CompanyCode { get; set; }
        public String BkgRef { get; set; }
        public String SegNum { get; set; }
        public String PNR { get; set; }
        public String Flight { get; set; }
        public String Class { get; set; }
        public String DepartCity { get; set; }
        public String ArrivalCity { get; set; }
        public String Status { get; set; }
        public String NumOfPax { get; set; }
        public DateTime DepartDATE { get; set; }
        public String DepartTime { get; set; }
        public DateTime ArrDATE { get; set; }
        public String ArrTime { get; set; }
        public String ElapsedTime { get; set; }
        public String SSR { get; set; }
        public String Seat { get; set; }
        public String ReasonCode { get; set; }
        public Int32 StopOverNum { get; set; }
        public String StopOverCity { get; set; }
        public String Pcode { get; set; }
        public String SpecialRQ { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String INVRMK1 { get; set; }
        public String INVRMK2 { get; set; }
        public String VCHRMK1 { get; set; }
        public String VCHRMK2 { get; set; }
        public String SELLCURR { get; set; }
        public Decimal TOTALSELL { get; set; }
        public Decimal TOTALSTAX { get; set; }
        public String COSTCURR { get; set; }
        public Decimal TOTALCOST { get; set; }
        public Decimal TOTALCTAX { get; set; }
        public String SourceSystem { get; set; }
        public DateTime CreateOn { get; set; }
        public String CreateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public String UpdateBy { get; set; }
        public DateTime VoidOn { get; set; }
        public String VoidBy { get; set; }
        public String AirSeg { get; set; }
        public String DepartTerminal { get; set; }
        public String ArrivalTerminal { get; set; }
        public String OtherAirlinePNR { get; set; }
        public String EQP { get; set; }
        public String Service { get; set; }
        public String Suppcode { get; set; }

        public tbPEOAIR()
        {

        }

    }
    public class tbPEOAIRs : List<tbPEOAIR> { }

    public class tbPEOPAX
    {
        public String BKGREF { get; set; }
        public String COMPANYCODE { get; set; }
        public String SEGNUM { get; set; }
        public String PAXNUM { get; set; }
        public String PAXLNAME { get; set; }
        public String PAXFNAME { get; set; }
        public String PAXTITLE { get; set; }
        public String PAXTYPE { get; set; }
        public String PAXAIR { get; set; }
        public String PAXHOTEL { get; set; }
        public String PAXOTHER { get; set; }
        public Int16 PAXAGE { get; set; }
        public String RMK1 { get; set; }
        public String RMK2 { get; set; }
        public String VOIDON { get; set; }
        public DateTime VOIDBY { get; set; }
        public String SOURCESYSTEM { get; set; }
        public DateTime CREATEON { get; set; }
        public String CREATEBY { get; set; }
        public DateTime UPDATEON { get; set; }
        public String UPDATEBY { get; set; }

        public tbPEOPAX()
        {

        }

    }
    public class tbPEOPAXs : List<tbPEOPAX> { }


}
