﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class UIXoDatabaseInfo
    {
        public string Msg { get; set; }
        public StatusType Status { get; set; }


        public enum StatusType
        {
            Error,
            Warning,
            Success,
        }
    }
}
