﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class UIVoucherDatabaseInfo
    {
        public string Msg { get; set; }
        public StatusType Status { get; set; }

        public enum StatusType
        { 
            Error,
            Warning,
            Success
        }
    }
}
