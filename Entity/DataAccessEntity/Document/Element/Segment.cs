﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class Segment
    {
        public SegmentType Type { get; set; }
        public SegmentKey Data { get; set; }

        public class SegmentKey
        {
            //public string CompanyCode { get; set; }
            //public string BkgRef { get; set; }
            public string SegNum { get; set; }
        }

        public enum SegmentType
        {
            Air,
            Hotel,
            Other,
        }
    }
}
