﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class Ticket
    {
        public string CompanyCode { get; set; }
        public string TicketNum { get; set; }
        public string TicketSegNum { get; set; }
        public string TicketType { get; set; }
    }
}
