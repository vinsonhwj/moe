﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.BLLEntity.Document
{
    public class Air
    {
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string SegNum { get; set; }
    }
}
