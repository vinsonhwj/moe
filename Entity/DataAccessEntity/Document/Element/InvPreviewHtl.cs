﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class InvPreviewHtl
    {
        public sqlInvHotel Header { get; set; }
        public sqlInvHotelDetails Details { get; set; }
    }
}
