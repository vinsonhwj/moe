﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class Receipt
    {
        //public string Type { get; set; }

        //public string CardType { get; set; }
        //public string CardNum { get; set; }
        //public string ExpiryMonth { get; set; }
        //public string ExpiryYear { get; set; }
        //public string CardHolder { get; set; }
        //public string AppCode { get; set; }
        //public string TA { get; set; }
        public string FormsOfPayment { get; set; } //dr_form_payment
        public string CreditCardType { get; set; } //dr_card_type
        public string CreditCardNum { get; set; }  //txt_creditCard_num
        public string CreditCardHolder { get; set; }//txt_creditCard_holder
        public string ExpiryDateMonth { get; set; }//dr_card_mon
        public string ExpiryDateYear { get; set; }//dr_card_yr
        public string PaymentRemarks { get; set; }//txt_cash
        public string ChequeNum { get; set; }//txt_cheque_num
        public string IssueDate { get; set; }//txt_cheque_issue
        public string Payer { get; set; }//txt_cheque_payer
        public string BankCode { get; set; }//txt_cheque_bankCode
    }
}
