﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
   public class XoPreviewHtl
    {
       public sqlXoHotel Header { get; set; }
       public sqlXoHotelDetails Details { get; set; }
    }
}
