﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.BLLEntity;

namespace Westminster.MOE.Entity.DALEntity.Document.Element
{
    public class VchPreviewHtl
    {
        public sqlVchHotel Header { get; set; }
        public sqlVchHotelDetails Details { get; set; }
    }
}
