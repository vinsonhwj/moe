﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class BookingHotel
    {
        public tbPEOHTL Header { get; set; }
        public tbPeoHtlDetails Details { get; set; }

        public BookingHotel()
        {
            //Header = new tbPEOHTL();
            //Details = new tbPeoHtlDetails();
        }

        public BookingHotel(tbPEOHTL header, tbPeoHtlDetails details)
        {
            Header = header;
            Details = details;
        }
    }
}
