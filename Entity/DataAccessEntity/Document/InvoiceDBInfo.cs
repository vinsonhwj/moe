﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceDBInfo
    {
        public InvoiceDBInfo()
        {
            this.InvoiceDatabaseInfo = new Element.UIInvoiceDatabaseInfo();
        }

        public Element.UIInvoiceDatabaseInfo InvoiceDatabaseInfo { get; set; }

        public tbPEOINV PeoInv { get; set; }

        public tbPEOINPAXs PeoInPaxList { get; set; }

        public tbPEOINVSEGs PeoInvSegList { get; set; }

        public tbPEOINVDETAILs PeoInvDetailList { get; set; }

        public tbPEOOTHERs PeoOtherList { get; set; }

        public tbPEOOTHERDETAILs PeoOtherDetailList { get; set; }

        public tbPEONYREFs PeoNyRefList { get; set; }


    }
}
