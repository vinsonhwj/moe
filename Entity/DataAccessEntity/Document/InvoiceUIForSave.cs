﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceUIForSave
    {
        public string CompanyCode { get; set; }
        public string BkgNum { get; set; }

        public string ProfitMarginReason { get; set; }//ddl_ProfitMarginReason
        public bool ProfitMarginReasonVisible { get; set; }
        public string Conditions { get; set; }//ddlConditions
        public string Conditionsof { get; set; }//txtTerms
        public bool IsOthersConditions { get; set; }//cbOther
        public string OthersConditions { get; set; }//txtOther

        public int CurrencySelectedItem { get; set; }

        public decimal GrandTotalamountvle { get; set; }

        public decimal GSTPCT { get; set; }//Session("GSTPCT")

        public bool BlSinFlag { get; set; }

        public decimal GSTTax { get; set; }//txt_GSTTAX
       


        public InvoiceUIForPreview PreviewInput { get; set; }
    }
}
