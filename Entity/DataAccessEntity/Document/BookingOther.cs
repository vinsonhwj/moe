﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class BookingOther
    {
        public tbPEOOTHER Header { get; set; }
        public tbPEOOTHERDETAILs Details { get; set; }

        public BookingOther()
        {

        }

        public BookingOther(tbPEOOTHER header, tbPEOOTHERDETAILs details)
        {
            Header = header;
            Details = details;
        }
    }
}
