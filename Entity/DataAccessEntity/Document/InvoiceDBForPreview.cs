﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceDBForPreview
    {
        public InvoiceUIForPreview UIInfo { get; set; }

        public tbPEOPAXs Passengers { get; set; }

        public tbPEOAIRs Airs { get; set; }

        public sqlInvAirDetails AirDetails { get; set; }

        public List<Element.InvPreviewHtl> Hotels { get; set; }

        public sqlInvOthers Others { get; set; }

        public sqlInvOtherDetails OtherDetails { get; set; }

        public tbCOMPANY Company { get; set; }

        public tbPEOMSTR PeoMstr { get; set; }

        public tbPEOSTAFF PeoStaff { get; set; }

        public sqlInvTickets Tickets { get; set; }
    }
}
