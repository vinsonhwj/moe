﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
   public  class XoDBForPreview
    {
       public XoUIInfo UIXoInfo { get; set; }

       public tbPEOPAXs Passengers { get; set; }

       public tbPEOAIRs Airs { get; set; }

       public sqlXoAirDetails AirDetails { get; set; }

     //  public List<Element.XoPreviewHtl> Hotels { get; set; }

       public sqlXoHotels Hotels { get; set; }

       public sqlXoHotelDetails HtlDetails { get; set; }

       public tbPEOOTHERs Others { get; set; }

       public sqlXoOtherDetails OthDetails { get; set; }

       public tbCOMPANY Company { get; set; }

       public tbPEOMSTR PeoMstr { get; set; }

       public tbSUPPLIER Supplier { get; set; }

       public tbPEOSTAFF Staff { get; set; }

       public tbEXRATE Exrate { get; set; }
    }
}
