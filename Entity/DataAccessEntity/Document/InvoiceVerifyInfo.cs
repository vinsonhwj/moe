﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceVerifyInfo
    {
        public tbPEOMSTR PeoMstrEntity { get; set; }
        public tbTEAM PeoMstrTeam { get; set; }

        public int PeoInvCount { get; set; }

        public int AnalysisCodeCount { get; set; }
        public bool isAirSegmentTypeMatch { get; set; }

        public bool MatchAirAgeType { get; set; }
        public bool MatchOtherAgeType { get; set; }

        public List<string> CurrList { get; set; }

        public tbPEOSTAFF PeoStaff { get; set; }
        public tbCustomer Customer { get; set; }

        public tbPEOAIRDETAILs PeoAirDetails { get; set; }

        public tbCOMPANY Company { get; set; }

        public tbPEOMSTRs PeoMstrsForMasterPNR { get; set; }

        public bool PMQ_Extax { get; set; }

    }
}
