﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
   public class XoVerifyInfo
    {
        public tbPEOMSTR PeoMstrEntity { get; set; }
        public tbTEAM PeoMstrTeam { get; set; }

        public int PeoInvCount { get; set; }

        public int AnalysisCodeCount { get; set; }

        public tbPEOAIRDETAILs PeoAirDetails { get; set; }

        public int PaxSeletedCount { get; set; }

        public tbPEOAIRs PeoAirList { get; set; }

        public tbPEOOTHERs PeoOthers { get; set; }

        public tbPEOHOTELs PeoHotels { get; set; }

        public tbPEOOTHERDETAILs PeoOtherDetails { get; set; }

        public tbPEOPAX PeoOthMatchAgeType { get; set; }

        public tbPEOPAX PeoAirMatchAgeType { get; set; }

        public tbAirSegments tbAirSegments { get; set; }

        public tbAirSegments tbAirSegNotSelected { get; set; }

        public tbHotelSegments tbHotelSegments { get; set; }

        public tbOtherSegments tbOtherSegments { get; set; }


        public tbAirSegments_AgeTypes tbAirSegments_AgeTypes { get; set; }

      //  public tbCurrs tbCurrs { get; set; }

        public tbMultiCurrs tbMultiCurrs { get; set; }

        public XoVerifyInfo()
        {

        }
    }
}
