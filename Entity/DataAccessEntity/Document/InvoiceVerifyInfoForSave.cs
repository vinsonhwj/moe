﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceVerifyInfoForSave
    {
        public bool E_EINV { get; set; }
        public bool PMQ_EXTAX { get; set; }

        public tbCOMPANY Company { get; set; }
        public tbPEOMSTR PeoMstr { get; set; }

        public tbPEOMSTRs PeoMstrsForMasterPNR { get; set; }

        public tbCustomer Customer { get; set; }

        public tbPEOSTAFF PeoStaff { get; set; } //Get_ProfitMargin_UpperAndLower
    }
}
