﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document.Element;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    [Serializable]
   public class XoUIInfo
    {
        public string CompanyCode { get; set; }
        public string BookingNum { get; set; }
        public string Attn { get; set; }
        public string CustomerRef1 { get; set; }
        public string CustomerRef2 { get; set; }
        public string CustomerRef3 { get; set; }
        public string SuppCode { get; set; }
        public string AirRemarks { get; set; }
        public string XoRemarks { get; set; }
        public string AnaylsisCode { get; set; }
        public string PrintedDate { get; set; }
        public string FormPay { get; set; }

        public List<Passenger> Passengers { get; set; }

        public List<Segment> Segments { get; set; }

        public string Voidon { get; set; }
        public string UserCode { get; set; }
        public string SeqType { get; set; }
    }
}
