﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceUIForPreview
    {
        public string CompanyCode { get; set; }
        public string BookingNum { get; set; }

        public string TeamCode { get; set; }

        public string InvType { get; set; }
        //  public string Format { get; set; }
        public string CltCode { get; set; }
        public string CltName { get; set; }
        public string Attn { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string CustomerRef1 { get; set; }
        public string CustomerRef2 { get; set; }
        public string CustomerRef3 { get; set; }
        public string AnaylsisCode { get; set; }

        public string InvoiceDate { get; set; }

        public string ProfitMarginReason { get; set; }

        public string UserCode { get; set; }

        public string CONTACTPERSON { get; set; }

        public string Terms { get; set; }
        public bool hasOthersTerms { get; set; }
        public string OthersTerms { get; set; }

        public string DBTableSuffix { get; set; }

        public string GSTTax { get; set; }

        public bool IsAllowGroupInsurance { get; set; }

        public string OtherInsSegnum { get; set; }

        public string OtherInsCurrency { get; set; }

        public string RemaiksAir1 { get; set; }
        public string RemaiksAir2 { get; set; }
        public string RemaiksHtl1 { get; set; }
        public string RemaiksHtl2 { get; set; }
        public string RemaiksOth1 { get; set; }
        public string RemaiksOth2 { get; set; }

        public string IssuedBy { get; set; }

        public decimal RoundUpTorrent { get; set; }//web config roundUpTorrent

        public int CurrencySelectedItem { get; set; }

        public string RecInv { get; set; }//recinv  Request("RECINV")

        public string InvFormat { get; set; }//ddl_invFormat

        public string LocalCurr { get; set; }

        public string CityCode { get; set; } //Request.Cookies("code_name").Values("citycode")

        public string CountryCode { get; set; }

        public Element.Receipt ReceiptInfo { get; set; }

        public List<Element.Passenger> Passengers { get; set; }

        public List<Element.Segment> Segments { get; set; }

        public List<Element.Ticket> Tickets { get; set; }
    }
}
