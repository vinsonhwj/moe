﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class VCHSaveDB
    {
        public tbPEOVCH Header { get; set; }
        public tbDocumentBanner Banner { get; set; }
        public List<tbPEOVCHSEG> Segments { get; set; }
        public List<tbPEOVCHDETAIL> Details { get; set; }
        public List<tbPEOINPAX> Passengers { get; set; }
        public tbPEONYREF History { get; set; }

        public VCHSaveDB()
            : this(false)
        { 
        
        }

        public VCHSaveDB(bool instace)
        {
            if (instace)
            {
                Header = new tbPEOVCH();
                Banner = new tbDocumentBanner();
                Segments = new List<tbPEOVCHSEG>();
                Details = new List<tbPEOVCHDETAIL>(); ;
                Passengers = new List<tbPEOINPAX>();
                History = new tbPEONYREF();
            }
        }
    }
}
