﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;
using Westminster.MOE.Entity.DALEntity.Document.Voucher;

namespace Westminster.MOE.Entity.DALEntity.Document.Voucher
{
    public class VCHCreateDB
    {
        public VCHCreateDB()
            : this(false)
        {
        }
        public VCHCreateDB(bool instace)
        {
            if (instace)
            {
                Master = new tbPEOMSTR();
                Company = new tbCOMPANY();
                Staff = new tbPEOSTAFF();
                Mis = new tbPEOMIS();
            }
        }

        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }
        public string VouchNum { get; set; }
        public DateTime ServerDateTime { get; set; }
        public VCHPreviewDB PreviewDB { get; set; }
        public tbPEOMSTR Master { get; set; }
        public tbCOMPANY Company { get; set; }
        public tbPEOSTAFF Staff { get; set; }
        public tbPEOMIS Mis { get; set; }

    }
}
