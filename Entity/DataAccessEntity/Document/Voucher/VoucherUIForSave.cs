﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.DALEntity.Document.Voucher;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class VoucherUIForSave
    {
        public string CompanyCode { get; set; }
        public string BkgRef { get; set; }

        public VCHSelectedDB PreviewInput { get; set; }
    }
}
