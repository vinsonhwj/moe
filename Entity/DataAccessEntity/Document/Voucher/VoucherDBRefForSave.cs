﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class VoucherDBRefForSave
    {
        public VoucherUIForSave UIInput { get; set; }

        public tbPEOPAXs Passengers { get; set; }

        //public List<Element.VchPreviewHtl> Hotels { get; set; }

        public tbPEOOTHERs Others { get; set; }

        public sqlVchOtherDetails OtherDetails { get; set; }

        public sqlVchTitles Titles { get; set; }


    }
}
