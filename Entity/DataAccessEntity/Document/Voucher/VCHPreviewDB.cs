﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document.Voucher
{
    public class VCHPreviewDB
    {
        public tbPEOMSTR Master { get; set; }
        public tbPEOMSTR PMQMaster { get; set; }
        public tbCustomer PMQCustomer { get; set; }
        public tbPEOSTAFF PMQStaff { get; set; }
        public tbPEOPAXs Passengers { get; set; }

        public tbPEOHOTELs PeoHotels { get; set; }
        public tbPEOHTLs PeoHtls { get; set; }
        public tbPeoHtlDetails PeoHtlDetails { get; set; }
        public sqlVchHotels ServiceDetails { get; set; }

        public tbPEOOTHERs PeoOthers { get; set; }
        public tbPEOOTHERDETAILs PeoOtherDetails { get; set; }

        public decimal TotalAmount { get; set; }
        public tbcitys Citys { get; set; }
        public tbcountrys Countrys { get; set; }
        public DateTime ServerDateTime { get; set; }
        public string MD5 { get; set; }

        public VCHPreviewDB()
            : this(false)
        {

        }

        public VCHPreviewDB(bool instace)
        {
            if (instace)
            {
                Master = new tbPEOMSTR();
                Passengers = new tbPEOPAXs();

                PeoHotels = new tbPEOHOTELs();
                PeoHtls = new tbPEOHTLs();
                PeoHtlDetails = new tbPeoHtlDetails();
                ServiceDetails = new sqlVchHotels();

                PeoOthers = new tbPEOOTHERs();
                PeoOtherDetails = new tbPEOOTHERDETAILs();
            }
        }

    }
}
