﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
using Westminster.MOE.Entity.eMosDBEntity.SQL;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class VoucherVerifyInfo
    {
        public int chkNum { get; set; }

        public int DatalistItemsCount { get; set; }

        public tbPEOPAX PeoOthMatchAgeType { get; set; }

        public tbHotelSegment tbHotelSegments { get; set; }

        public tbOtherSegment tbOtherSegments { get; set; }
        
    }
}
