﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity.Document.Voucher
{
    [Serializable]
    public class VCHSelectedKey
    {
        public string CompanyCode { get; set; }
        public string BranchCode { get; set; }
        public string TeamCode { get; set; }
        public string UserCode { get; set; }
        public string BkgRef { get; set; }
        public string Payment { get; set; }
        public string CurrCode { get; set; }
        public string VoucherType { get; set; }
        public DateTime BackDate { get; set; }

        public List<Element.Passenger> Passengers { get; set; }
        public List<Element.Segment> Segments { get; set; }

        public VCHSelectedKey()
        {

        }
    }

}
