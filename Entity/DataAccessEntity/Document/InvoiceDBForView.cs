﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;
namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceDBForView
    {
        public tbPEOINV PeoInv { get; set; }

        public tbPEOINV PeoInvCombinPlus { get; set; }

        public tbPEOMSTR PeoMstr { get; set; }

        public tbCOMPANY Company { get; set; }

        public tbCustomer Customer { get; set; }

        public tbPEOSTAFF PeoStaffForInv { get; set; }

        public tbPEOSTAFF PeoStaffForMstr { get; set; }

        public tbLOGIN Login { get; set; }

        public tbPEONYREF PeoNyRef { get; set; }

        public tbPEOINPAXs PeoInvPaxList { get; set; }

        public tbPEOINVSEGs PeoInvSegList { get; set; }

        public tbPEOINVDETAILs PeoInvDetailList { get; set; }

        public tbPEOPAXs PeoPaxList { get; set; }

        public tbPEOTKTs PeoTKTList { get; set; }

        public tbPEOMISTKTs PeoMisTKTList { get; set; }

        public tbPEOINVs PeoCreditCardList { get; set; }

        public tbPEOINVs PeoCombineInv { get; set; }

        public DateTime DueDay { get; set; }

        public tbpeocltcrlmt Peocltcrlmt { get; set; }
    }
}
