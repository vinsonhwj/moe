﻿using System;
using System.Collections.Generic;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class InvoiceDBForSave
    {
        //public InvoiceDBForSave()
        //{
        //    this.InvoiceDatabaseInfo = new Element.UIInvoiceDatabaseInfo();
        //}

        //public Element.UIInvoiceDatabaseInfo InvoiceDatabaseInfo { get; set; }

        public tbPEOINV PeoInv { get; set; }

        public tbPEOMSTR UpdatePeoMstr { get; set; }//AnalysisCode WHERE CompanyCode AND Bkgref 

        public tbPEOINPAXs PeoInPaxList { get; set; }

        public tbPEOINVSEGs PeoInvSegList { get; set; }

        public tbPEOINVDETAILs PeoInvDetailList { get; set; }

        public tbPEOOTHER PeoOther { get; set; }

        public tbPEOOTHERDETAILs PeoOtherDetailList { get; set; }

        public tbPEONYREF PeoNyRef { get; set; }


    }
}
