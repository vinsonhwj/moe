﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Entity.eMosDBEntity.Table;

namespace Westminster.MOE.Entity.DALEntity.Document
{
    public class XoDBInfo
    {
        public XoDBInfo()
        {
            this.XoDatabaseInfo = new Element.UIXoDatabaseInfo();  
        }
        public Element.UIXoDatabaseInfo XoDatabaseInfo { get; set; }

        public tbPEOINV PeoInv { get; set; }

        public tbPEOINPAXs PeoInPaxList { get; set; }

        public tbPEOINVSEGs PeoInvSegList { get; set; }

        public tbPEOINVDETAILs PeoInvDetailList { get; set; }

        public tbPEOOTHERs PeoOtherList { get; set; }

        public tbPEOOTHERDETAILs PeoOtherDetailList { get; set; }

        public tbPEONYREFs PeoNyRefList { get; set; }

    }
}
