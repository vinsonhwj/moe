﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Westminster.MOE.Entity.DALEntity
{
    public class EnumTypes
    {
        public enum CompanyPreference
        {
            E_EINV,
        }

        public enum BindingName
        {
            BookingSoap,
            IURWebServiceSoap,
        }

        public enum DALConnectionType
        {
            eMos,
            MOE,
            IUR,
            Gateway,
        }
    }
}
