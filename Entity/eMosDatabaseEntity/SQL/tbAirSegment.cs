﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class tbAirSegment : BaseSqlTable<tbAirSegment>
    {
        public tbAirSegment()
            : base(CommandText)
        { }
        public const string CommandText = @"select isnull(vchrmk1, '') as vchrmk1, isnull(vchrmk2, '') as vchrmk2, a.SEGNUM,DepartCity,ArrivalCity,DepartDATE,DepartTime,Flight,Class,isnull(INVRMK1,'-') as INVRMK1,isnull(INVRMK2,'-') as INVRMK2,isnull(dt.curr,'') as curr, isnull(suppcode,'') as suppcode  
                                              from peoair a(nolock),peoairdetail dt 
                                              where  a.bkgref=dt.bkgref and a.segnum=dt.segnum 
                                              AND (a.segnum IN (SELECT VALUE FROM dbo.f_SplitString(@SegNum,',')) or @SegNum='' )
                                              and dt.seqnum= (select min(seqnum) from peoairdetail dt2 where dt.segnum=dt2.segnum and dt.bkgref=dt2.bkgref and dt.seqtype=dt2.seqtype and dt2.seqtype='COST') and  a.bkgref=@bkgRef order by a.DepartDATE asc,a.SEGNUM ASC";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            vchrmk1,
            /// <summary>
            ///
            /// </summary>
            vchrmk2,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            DepartCity,
            /// <summary>
            ///
            /// </summary>
            ArrivalCity,
            /// <summary>
            ///
            /// </summary>
            DepartDATE,
            /// <summary>
            ///
            /// </summary>
            DepartTime,
            /// <summary>
            ///
            /// </summary>
            Flight,
            /// <summary>
            ///
            /// </summary>
            Class,
            /// <summary>
            ///
            /// </summary>
            INVRMK1,
            /// <summary>
            ///
            /// </summary>
            INVRMK2,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            suppcode,
        }

        #region Model
        private String _vchrmk1;
        private String _vchrmk2;
        private String _segnum;
        private String _departcity;
        private String _arrivalcity;
        private DateTime _departdate;
        private String _departtime;
        private String _flight;
        private String _class;
        private String _invrmk1;
        private String _invrmk2;
        private String _curr;
        private String _suppcode;
        /// <summary>
        /// [Un-Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("vchrmk1", DbType.String, 60)]
        public String vchrmk1
        {
            set { _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("vchrmk2", DbType.String, 60)]
        public String vchrmk2
        {
            set { _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("DepartCity", DbType.String, 3)]
        public String DepartCity
        {
            set { _departcity = value; }
            get { return _departcity; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("ArrivalCity", DbType.String, 3)]
        public String ArrivalCity
        {
            set { _arrivalcity = value; }
            get { return _arrivalcity; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DepartDATE", DbType.DateTime, 8)]
        public DateTime DepartDATE
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("DepartTime", DbType.String, 4)]
        public String DepartTime
        {
            set { _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("Flight", DbType.String, 6)]
        public String Flight
        {
            set { _flight = value; }
            get { return _flight; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("Class", DbType.String, 2)]
        public String Class
        {
            set { _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK1", DbType.String, 100)]
        public String INVRMK1
        {
            set { _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK2", DbType.String, 100)]
        public String INVRMK2
        {
            set { _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.String, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Un-Null/nchar(8)]
        /// </summary>
        [FieldMapping("suppcode", DbType.StringFixedLength, 8)]
        public String suppcode
        {
            set { _suppcode = value; }
            get { return _suppcode; }
        }
        #endregion Model
    }
        [Serializable]
        public class tbAirSegments : BaseList<tbAirSegment, tbAirSegments> { }
        //public class SqlAirSegmentPage : PageResult<SqlAirSegment, SqlEntitys> { }
    

    
}
