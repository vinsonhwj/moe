﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;
namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class tbAirSegment_AgeType : BaseSqlTable<tbAirSegment_AgeType>
    {
        public tbAirSegment_AgeType()
            : base(CommandText)
        { }
        public const string CommandText = @"SELECT    a.*
                                                FROM      ( SELECT    SegNum ,
                                                                      AgeType ,
                                                                      Qty
                                                            FROM      peoairdetail
                                                            WHERE     bkgref = @bkgRef
                                                                      AND CompanyCode = @companyCode
                                                                      AND SeqType = 'COST'
                                                                      AND SegNum IN (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                                                          ) a ,
                                                          ( SELECT    SegNum ,
                                                                      AgeType ,
                                                                      Qty
                                                            FROM      peoairdetail
                                                            WHERE     bkgref = @bkgRef
                                                                      AND CompanyCode =@companyCode
                                                                      AND SeqType = 'COST'
                                                                      AND SegNum IN ( SELECT VALUE FROM dbo.f_SplitString(@SegNum,',') )
                                                          ) b
                                                WHERE     ( a.SegNum <> b.SegNum
                                                            AND a.AgeType = b.AgeType
                                                            AND a.Qty <> b.Qty
                                                          )
                                                          OR ( ( SELECT   COUNT(*)
                                                                 FROM     ( SELECT    SegNum ,
                                                                                      AgeType ,
                                                                                      Qty
                                                                            FROM      peoairdetail
                                                                            WHERE     bkgref = @bkgRef
                                                                                      AND CompanyCode = @companyCode
                                                                                      AND SeqType = 'COST'
                                                                                      AND SegNum IN (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                                                                          ) c
                                                                 WHERE    c.SegNum = b.SegNum
                                                                          AND c.AgeType = a.AgeType
                                                               ) = 0 )";


    //    public const string CommandText = @" SELECT a.* FROM  (select SegNum, AgeType, Qty from peoairdetail where bkgref=@bkgRef and CompanyCode=@companyCode  and SeqType='COST' and SegNum In (SELECT VALUE FROM dbo.f_SplitString(@SegNum,',') )  a,  (select SegNum, AgeType, Qty from peoairdetail where bkgref=@bkgRef and CompanyCode=@companyCode  and SeqType='COST' and SegNum In (SELECT VALUE FROM dbo.f_SplitString(@SegNum,',') )  b   WHERE (a.SegNum<>b.SegNum and a.AgeType=b.AgeType and a.Qty<>b.Qty) OR  ((select count(*) from  (select SegNum, AgeType, Qty from peoairdetail where bkgref=@bkgRef and CompanyCode=@companyCode  and SeqType='COST' and SegNum In (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))  c  where c.SegNum=b.SegNum and c.AgeType=a.AgeType) = 0 )";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            SegNum,
            /// <summary>
            ///
            /// </summary>
            AgeType,
            /// <summary>
            ///
            /// </summary>
            Qty,
        }

        #region Model
        private String _segnum;
        private String _agetype;
        private Int32 _qty;
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SegNum", DbType.String, 5)]
        public String SegNum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AgeType", DbType.String, 3)]
        public String AgeType
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("Qty", DbType.Int32, 10)]
        public Int32 Qty
        {
            set { _qty = value; }
            get { return _qty; }
        }
        #endregion Model
    }
    [Serializable]
    public class tbAirSegments_AgeTypes : BaseList<tbAirSegment_AgeType, tbAirSegments_AgeTypes> { }
}
