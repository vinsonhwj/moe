﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SP:sqlVchHotel
    /// </summary>
    [Serializable]
    public class sqlVchHotel : BaseSqlTable<sqlVchHotel>
    {
        public sqlVchHotel()
            : base(CommandText)
        { }
        public const string CommandText = @"exec sp_STInvoice_HotelSegNum_Wai  @GSTTAXRate,@Companycode,@BkgRef,@SegNum,@SeqType";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            GSTTAXRate,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            coMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            SEQTYPE,
            /// <summary>
            ///
            /// </summary>
            RMNTS,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ROOMTYPE,
            /// <summary>
            ///
            /// </summary>
            ITEMNAME,
            /// <summary>
            ///
            /// </summary>
            CURR,
            /// <summary>
            ///
            /// </summary>
            AMT,
            /// <summary>
            ///
            /// </summary>
            TOTALAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            itemtype,
            /// <summary>
            ///
            /// </summary>
            QtyOfHotel,
            /// <summary>
            ///
            /// </summary>
            QTYs,
        }

        #region Model
        private String _gsttaxrate;
        private String _gsttax;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _seqnum;
        private String _seqtype;
        private Decimal _rmnts;
        private DateTime _arrdate;
        private DateTime _departdate;
        private Int32 _qty;
        private String _ratename;
        private String _roomtype;
        private String _itemname;
        private String _curr;
        private Decimal _amt;
        private Decimal _totalamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private String _itemtype;
        private Int32 _qtyofhotel;
        private Int32 _qtys;
        /// <summary>
        /// [Allow Null/varchar(10)]
        /// </summary>
        [FieldMapping("GSTTAXRate", DbType.AnsiString, 10)]
        public String GSTTAXRate
        {
            set { _gsttaxrate = value; }
            get { return _gsttaxrate; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.StringFixedLength, 1)]
        public String GSTTAX
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("coMPANYCODE", DbType.String, 2)]
        public String coMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQTYPE", DbType.String, 5)]
        public String SEQTYPE
        {
            set { _seqtype = value; }
            get { return _seqtype; }
        }
        /// <summary>
        /// [Un-Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("RMNTS", DbType.Decimal, 4)]
        public Decimal RMNTS
        {
            set { _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ROOMTYPE", DbType.String, 100)]
        public String ROOMTYPE
        {
            set { _roomtype = value; }
            get { return _roomtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ITEMNAME", DbType.String, 100)]
        public String ITEMNAME
        {
            set { _itemname = value; }
            get { return _itemname; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CURR", DbType.String, 3)]
        public String CURR
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("AMT", DbType.Decimal, 14)]
        public Decimal AMT
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALAMT", DbType.Decimal, 14)]
        public Decimal TOTALAMT
        {
            set { _totalamt = value; }
            get { return _totalamt; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("itemtype", DbType.StringFixedLength, 1)]
        public String itemtype
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QtyOfHotel", DbType.Int32, 10)]
        public Int32 QtyOfHotel
        {
            set { _qtyofhotel = value; }
            get { return _qtyofhotel; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("QTYs", DbType.Int32, 10)]
        public Int32 QTYs
        {
            set { _qtys = value; }
            get { return _qtys; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlVchHotels : BaseList<sqlVchHotel, sqlVchHotels> { }
}

