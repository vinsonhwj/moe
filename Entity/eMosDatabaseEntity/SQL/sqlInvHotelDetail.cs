﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlHotelDetail
    /// </summary>
    [Serializable]
    public class sqlInvHotelDetail : BaseSqlTable<sqlInvHotelDetail>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            GSTTAXRate,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            coMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            SEQTYPE,
            /// <summary>
            ///
            /// </summary>
            RMNTS,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ROOMTYPE,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            SELLAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            itemtype,
        }

        #region Model
        private Decimal _gsttaxrate;
        private String _gsttax;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _seqnum;
        private String _seqtype;
        private Decimal _rmnts;
        private DateTime _arrdate;
        private DateTime _departdate;
        private Int32 _qty;
        private String _ratename;
        private String _roomtype;
        private String _sellcurr;
        private Decimal _sellamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private String _itemtype;
        /// <summary>
        /// [Un-Null/decimal(4,4)]
        /// </summary>
        [FieldMapping("GSTTAXRate", DbType.Decimal, 0)]
        public Decimal GSTTAXRate
        {
            set { _gsttaxrate = value; }
            get { return _gsttaxrate; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.StringFixedLength, 1)]
        public String GSTTAX
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("coMPANYCODE", DbType.String, 2)]
        public String coMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQTYPE", DbType.String, 5)]
        public String SEQTYPE
        {
            set { _seqtype = value; }
            get { return _seqtype; }
        }
        /// <summary>
        /// [Un-Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("RMNTS", DbType.Decimal, 4)]
        public Decimal RMNTS
        {
            set { _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ROOMTYPE", DbType.String, 100)]
        public String ROOMTYPE
        {
            set { _roomtype = value; }
            get { return _roomtype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SELLAMT", DbType.Decimal, 14)]
        public Decimal SELLAMT
        {
            set { _sellamt = value; }
            get { return _sellamt; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("itemtype", DbType.StringFixedLength, 1)]
        public String itemtype
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlInvHotelDetails : BaseList<sqlInvHotelDetail, sqlInvHotelDetails> { }
    public class sqlInvHotelDetailPage : PageResult<sqlInvHotelDetail, sqlInvHotelDetails> { }
}

