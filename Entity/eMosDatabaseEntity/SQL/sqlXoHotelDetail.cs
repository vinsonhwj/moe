﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class sqlXoHotelDetail :BaseSqlTable <sqlXoHotelDetail>
    {
        public sqlXoHotelDetail()
            : base(CommandText)
        { }

        public const string CommandText = @"select RATENAME,SEGNUM,SEQNUM,QTY,ARRDATE,DEPARTDATE,isnull(RMNTS,0) as rmnts,ITEMNAME as ROOMTYPE,isnull(CURR,'') as COSTCURR  ,isnull(AMT,0)  as COSTAMT,isnull(taxCURR,'') as taxcurr,isnull(taxAMT,0) as taxamt,TOTALAMT, (select isnull(b.gsttax, 'N') from peohtl b where b.bkgref = a.bkgref and b.segnum = a.segnum) as gsttax from peohtldetail a where SEQTYPE='COST' and bkgref=@bkgRef and SEGNUM =@hotelSegNumWai order by seqnum";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            rmnts,
            /// <summary>
            ///
            /// </summary>
            ROOMTYPE,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            COSTAMT,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            TOTALAMT,
            /// <summary>
            ///
            /// </summary>
            gsttax,
        }

        #region Model
        private String _ratename;
        private String _segnum;
        private String _seqnum;
        private Int32 _qty;
        private DateTime _arrdate;
        private DateTime _departdate;
        private Decimal _rmnts;
        private String _roomtype;
        private String _costcurr;
        private Decimal _costamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _totalamt;
        private String _gsttax;
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Un-Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("rmnts", DbType.Decimal, 4)]
        public Decimal rmnts
        {
            set { _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ROOMTYPE", DbType.String, 100)]
        public String ROOMTYPE
        {
            set { _roomtype = value; }
            get { return _roomtype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("COSTAMT", DbType.Decimal, 14)]
        public Decimal COSTAMT
        {
            set { _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 14)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALAMT", DbType.Decimal, 14)]
        public Decimal TOTALAMT
        {
            set { _totalamt = value; }
            get { return _totalamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("gsttax", DbType.StringFixedLength, 1)]
        public String gsttax
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        #endregion Model
    }
    [Serializable]
    public class sqlXoHotelDetails : BaseList<sqlXoHotelDetail, sqlXoHotelDetails> { }
    public class sqlXoHotelDetailPage : BaseList<sqlXoHotelDetail, sqlXoHotelDetails> { }

        

}
