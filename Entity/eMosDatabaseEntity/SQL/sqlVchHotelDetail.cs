﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlVchHotelDetail
    /// </summary>
    [Serializable]
    public class sqlVchHotelDetail : BaseSqlTable<sqlVchHotelDetail>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            QtyOfHotel,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            rmnts,
            /// <summary>
            ///
            /// </summary>
            ITEMNAME,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            COSTAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXamt,
            /// <summary>
            ///
            /// </summary>
            TOTALAMT,
            /// <summary>
            ///
            /// </summary>
            ItemType,
        }

        #region Model
        private Int32 _qtyofhotel;
        private String _segnum;
        private String _seqnum;
        private Int32 _qty;
        private DateTime _arrdate;
        private DateTime _departdate;
        private Decimal _rmnts;
        private String _itemname;
        private String _ratename;
        private String _costcurr;
        private Decimal _costamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _totalamt;
        private String _itemtype;
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QtyOfHotel", DbType.Int32, 10)]
        public Int32 QtyOfHotel
        {
            set { _qtyofhotel = value; }
            get { return _qtyofhotel; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Un-Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("rmnts", DbType.Decimal, 4)]
        public Decimal rmnts
        {
            set { _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ITEMNAME", DbType.String, 100)]
        public String ITEMNAME
        {
            set { _itemname = value; }
            get { return _itemname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("COSTAMT", DbType.Decimal, 14)]
        public Decimal COSTAMT
        {
            set { _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXamt", DbType.Decimal, 14)]
        public Decimal TAXamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALAMT", DbType.Decimal, 14)]
        public Decimal TOTALAMT
        {
            set { _totalamt = value; }
            get { return _totalamt; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("ItemType", DbType.StringFixedLength, 1)]
        public String ItemType
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlVchHotelDetails : BaseList<sqlVchHotelDetail, sqlVchHotelDetails> { }
    public class sqlVchHotelDetailPage : PageResult<sqlVchHotelDetail, sqlVchHotelDetails> { }
}


