﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;
namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
   public  class sqlXoAirDetail : BaseSqlTable<sqlXoAirDetail>
    {
        public sqlXoAirDetail()
            : base(CommandText)
        { }
        public const string CommandText = @"select  isnull(max(invrmk1),'') as invrmk1, isnull(max(invrmk2),'') as invrmk2, a.agetype,a.COMPANYCODE,a.BKGREF,max(a.segnum) as segnum,a.curr, sum(isnull(amt,0)) as amt,a.taxcurr, sum(isnull(a.taxamt,0)) as taxamt ,b.num as USER_QTY, a.qty as AIR_QTY, case when b.num > a.qty then a.qty else b.num end as QTY from  peoairdetail a, ( select paxair as agetype,count(paxair) as num from peopax  where bkgref=@BkgRef and SEGNUM in  (SELECT VALUE FROM dbo.f_SplitString(@PaxSegNumList,',')) group by paxair) b, (select max(isnull(invrmk1,'')) as invrmk1, max(isnull(invrmk2,'')) as invrmk2  from peoair where bkgref=@BkgRef and segnum in (SELECT VALUE FROM dbo.f_SplitString(@AirSegNumList,',') ) ) c where a.agetype=b.agetype  and a.bkgref=@BkgRef and a.SEGNUM in  (SELECT VALUE FROM dbo.f_SplitString(@AirSegNumList,',')) and a.seqtype='COST' group by a.agetype,a.COMPANYCODE,a.BKGREF,a.curr,a.taxcurr,b.num, a.qty 	order by a.agetype,a.segnum,a.COMPANYCODE,a.BKGREF,a.curr,a.taxcurr,b.num";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            invrmk1,
            /// <summary>
            ///
            /// </summary>
            invrmk2,
            /// <summary>
            ///
            /// </summary>
            agetype,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            segnum,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            amt,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            USER_QTY,
            /// <summary>
            ///
            /// </summary>
            AIR_QTY,
            /// <summary>
            ///
            /// </summary>
            QTY,
        }

        #region Model
        private String _invrmk1;
        private String _invrmk2;
        private String _agetype;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Int32 _user_qty;
        private Int32 _air_qty;
        private Int32 _qty;
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("invrmk1", DbType.String, 100)]
        public String invrmk1
        {
            set { _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("invrmk2", DbType.String, 100)]
        public String invrmk2
        {
            set { _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("agetype", DbType.String, 3)]
        public String agetype
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segnum", DbType.String, 5)]
        public String segnum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.String, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amt", DbType.Decimal, 36)]
        public Decimal amt
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 36)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("USER_QTY", DbType.Int32, 10)]
        public Int32 USER_QTY
        {
            set { _user_qty = value; }
            get { return _user_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("AIR_QTY", DbType.Int32, 10)]
        public Int32 AIR_QTY
        {
            set { _air_qty = value; }
            get { return _air_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlXoAirDetails : BaseList<sqlXoAirDetail, sqlXoAirDetails> { }
    public class sqlXoAirDetailPage : PageResult<sqlXoAirDetail, sqlXoAirDetails> { }
}
