﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlOther
    /// </summary>
    [Serializable]
    public class sqlInvOther : BaseSqlTable<sqlInvOther>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            seq,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEGTYPE,
            /// <summary>
            ///
            /// </summary>
            OTHERDATE,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT1,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT2,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT3,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT4,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            INVRMK1,
            /// <summary>
            ///
            /// </summary>
            INVRMK2,
            /// <summary>
            ///
            /// </summary>
            VCHRMK1,
            /// <summary>
            ///
            /// </summary>
            VCHRMK2,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALSELL,
            /// <summary>
            ///
            /// </summary>
            TOTALSTAX,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALCOST,
            /// <summary>
            ///
            /// </summary>
            TOTALCTAX,
            /// <summary>
            ///
            /// </summary>
            PCODE,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            suppliercode,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            showDetailFee,
            /// <summary>
            ///
            /// </summary>
            masterDesc,
            /// <summary>
            ///
            /// </summary>
            IsRecalable,
        }

        #region Model
        private Int32 _seq;
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _segtype;
        private DateTime _otherdate;
        private String _descript1;
        private String _descript2;
        private String _descript3;
        private String _descript4;
        private String _citycode;
        private String _countrycode;
        private String _rmk1;
        private String _rmk2;
        private String _invrmk1;
        private String _invrmk2;
        private String _vchrmk1;
        private String _vchrmk2;
        private String _sellcurr;
        private Decimal _totalsell;
        private Decimal _totalstax;
        private String _costcurr;
        private Decimal _totalcost;
        private Decimal _totalctax;
        private String _pcode;
        private DateTime _voidon;
        private String _voidby;
        private String _sourcesystem;
        private String _suppliercode;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _gsttax;
        private String _showdetailfee;
        private String _masterdesc;
        private String _isrecalable;
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("seq", DbType.Int32, 10)]
        public Int32 seq
        {
            set { _seq = value; }
            get { return _seq; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGTYPE", DbType.String, 5)]
        public String SEGTYPE
        {
            set { _segtype = value; }
            get { return _segtype; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("OTHERDATE", DbType.DateTime, 8)]
        public DateTime OTHERDATE
        {
            set { _otherdate = value; }
            get { return _otherdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT1", DbType.String, 60)]
        public String DESCRIPT1
        {
            set { _descript1 = value; }
            get { return _descript1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT2", DbType.String, 60)]
        public String DESCRIPT2
        {
            set { _descript2 = value; }
            get { return _descript2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT3", DbType.String, 60)]
        public String DESCRIPT3
        {
            set { _descript3 = value; }
            get { return _descript3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT4", DbType.String, 60)]
        public String DESCRIPT4
        {
            set { _descript4 = value; }
            get { return _descript4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.String, 3)]
        public String CITYCODE
        {
            set { _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.String, 3)]
        public String COUNTRYCODE
        {
            set { _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 60)]
        public String RMK1
        {
            set { _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 60)]
        public String RMK2
        {
            set { _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK1", DbType.String, 100)]
        public String INVRMK1
        {
            set { _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK2", DbType.String, 100)]
        public String INVRMK2
        {
            set { _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VCHRMK1", DbType.String, 60)]
        public String VCHRMK1
        {
            set { _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VCHRMK2", DbType.String, 60)]
        public String VCHRMK2
        {
            set { _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSELL", DbType.Decimal, 14)]
        public Decimal TOTALSELL
        {
            set { _totalsell = value; }
            get { return _totalsell; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSTAX", DbType.Decimal, 14)]
        public Decimal TOTALSTAX
        {
            set { _totalstax = value; }
            get { return _totalstax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCOST", DbType.Decimal, 14)]
        public Decimal TOTALCOST
        {
            set { _totalcost = value; }
            get { return _totalcost; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCTAX", DbType.Decimal, 14)]
        public Decimal TOTALCTAX
        {
            set { _totalctax = value; }
            get { return _totalctax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("PCODE", DbType.String, 10)]
        public String PCODE
        {
            set { _pcode = value; }
            get { return _pcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("suppliercode", DbType.String, 10)]
        public String suppliercode
        {
            set { _suppliercode = value; }
            get { return _suppliercode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.StringFixedLength, 1)]
        public String GSTTAX
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("showDetailFee", DbType.StringFixedLength, 1)]
        public String showDetailFee
        {
            set { _showdetailfee = value; }
            get { return _showdetailfee; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2000)]
        /// </summary>
        [FieldMapping("masterDesc", DbType.String, 2000)]
        public String masterDesc
        {
            set { _masterdesc = value; }
            get { return _masterdesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("IsRecalable", DbType.String, 4)]
        public String IsRecalable
        {
            set { _isrecalable = value; }
            get { return _isrecalable; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlInvOthers : BaseList<sqlInvOther, sqlInvOthers> { }
    public class sqlInvOtherPage : PageResult<sqlInvOther, sqlInvOthers> { }
}

