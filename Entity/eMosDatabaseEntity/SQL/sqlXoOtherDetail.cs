﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class sqlXoOtherDetail:BaseSqlTable <sqlXoOtherDetail>
    {
        public sqlXoOtherDetail()
            : base(CommandText)
        { }
        public const string CommandText = @"SELECT  a.agetype ,
                                                      a.COMPANYCODE ,
                                                      a.BKGREF ,
                                                      a.segnum ,
                                                      a.curr ,
                                                      SUM(ISNULL(amt, 0.00)) AS amt ,
                                                      a.taxcurr ,
                                                      SUM(ISNULL(a.taxamt, 0.00)) AS taxamt ,
                                                      b.num AS User_QTY ,
                                                      a.qty AS OTHER_QTY ,
                                                      CASE WHEN b.num > a.qty THEN a.qty
                                                           ELSE b.num
                                                      END AS QTY ,
                                                      ISNULL(c.gsttax, 'N') AS gsttax
                                              FROM    peoother c ,
                                                      peootherdetail a ,
                                                      ( SELECT    paxother AS agetype ,
                                                                  COUNT(paxother) AS num
                                                        FROM      peopax
                                                        WHERE     bkgref = @BkgRef
                                                                  AND SEGNUM IN (SELECT VALUE FROM dbo.f_SplitString(@PaxNumList,',') )  
                                                        GROUP BY  paxother
                                                      ) b
                                              WHERE   a.agetype = b.agetype
                                                      AND a.bkgref = @BkgRef
                                                      AND a.SEGNUM IN ( SELECT VALUE FROM dbo.f_SplitString(@OtherSegNumList,',') ) 
                                                      AND a.seqtype = 'COST'
                                                      AND c.bkgref = a.bkgref
                                                      AND c.segnum = a.segnum
                                              GROUP BY a.segnum ,
                                                      a.agetype ,
                                                      a.COMPANYCODE ,
                                                      a.BKGREF ,
                                                      a.curr ,
                                                      a.taxcurr ,
                                                      b.num ,
                                                      gsttax ,
                                                      a.qty
                                              ORDER BY a.segnum ,
                                                      a.agetype ,
                                                      a.COMPANYCODE ,
                                                      a.BKGREF ,
                                                      a.curr ,
                                                      a.taxcurr ,
                                                      b.num";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            agetype,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            segnum,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            amt,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            User_QTY,
            /// <summary>
            ///
            /// </summary>
            OTHER_QTY,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            gsttax,
        }

        #region Model
        private String _agetype;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Int32 _user_qty;
        private Int32 _other_qty;
        private Int32 _qty;
        private String _gsttax;
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("agetype", DbType.String, 6)]
        public String agetype
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segnum", DbType.String, 5)]
        public String segnum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.StringFixedLength, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amt", DbType.Decimal, 36)]
        public Decimal amt
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 36)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("User_QTY", DbType.Int32, 10)]
        public Int32 User_QTY
        {
            set { _user_qty = value; }
            get { return _user_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("OTHER_QTY", DbType.Int32, 10)]
        public Int32 OTHER_QTY
        {
            set { _other_qty = value; }
            get { return _other_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("gsttax", DbType.StringFixedLength, 1)]
        public String gsttax
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlXoOtherDetails : BaseList<sqlXoOtherDetail, sqlXoOtherDetails> { }
    public class sqlXoOtherDetailPage:BaseList <sqlXoOtherDetail,sqlXoOtherDetails>{ }

}
