﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;
namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class tbOtherSegment:BaseSqlTable<tbOtherSegment>
    {
        public tbOtherSegment()
            : base(CommandText) { }
        public const string CommandText = @"SELECT  a.BKGREF ,
                                                              a.COMPANYCODE ,
                                                              a.SEGNUM ,
                                                              a.SEGTYPE ,
                                                              a.OTHERDATE ,
                                                              ISNULL(masterDesc, '') AS masterDesc ,
                                                              DESCRIPT1 ,
                                                              DESCRIPT2 ,
                                                              DESCRIPT3 ,
                                                              descript4 + '<br>' + vchrmk1 + '<br>' + vchrmk2 AS DESCRIPT4 ,
                                                              ISNULL(INVRMK1, '-') AS invrmk1 ,
                                                              ISNULL(INVRMK2, '-') AS invrmk2 ,
                                                              ISNULL(dt.curr, '') AS curr ,
                                                              ISNULL(a.suppliercode, '') AS suppliercode
                                                      FROM    peoother a ( NOLOCK ) ,
                                                              peootherdetail dt ( NOLOCK )
                                                      WHERE   a.bkgref = dt.bkgref
                                                              AND a.segnum = dt.segnum
                                                              AND a.segnum in (SELECT VALUE FROM dbo.f_SplitString(@SegNum,','))
                                                              AND dt.seqnum = ( SELECT    MIN(seqnum)
                                                                                FROM      peootherdetail dt2
                                                                                WHERE     dt.segnum = dt2.segnum
                                                                                          AND dt.bkgref = dt2.bkgref
                                                                                          AND dt.seqtype = dt2.seqtype
                                                                                          AND dt2.seqtype = 'COST'
                                                                              )
                                                              AND a.bkgref = @bkgRef
                                                      ORDER BY a.OTHERDATE ASC ,
                                                              a.SEGNUM ASC";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEGTYPE,
            /// <summary>
            ///
            /// </summary>
            OTHERDATE,
            /// <summary>
            ///
            /// </summary>
            masterDesc,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT1,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT2,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT3,
            /// <summary>
            ///
            /// </summary>
            DESCRIPT4,
            /// <summary>
            ///
            /// </summary>
            invrmk1,
            /// <summary>
            ///
            /// </summary>
            invrmk2,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            suppliercode,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _segtype;
        private DateTime _otherdate;
        private String _masterdesc;
        private String _descript1;
        private String _descript2;
        private String _descript3;
        private String _descript4;
        private String _invrmk1;
        private String _invrmk2;
        private String _curr;
        private String _suppliercode;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGTYPE", DbType.String, 5)]
        public String SEGTYPE
        {
            set { _segtype = value; }
            get { return _segtype; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("OTHERDATE", DbType.DateTime, 8)]
        public DateTime OTHERDATE
        {
            set { _otherdate = value; }
            get { return _otherdate; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2000)]
        /// </summary>
        [FieldMapping("masterDesc", DbType.String, 2000)]
        public String masterDesc
        {
            set { _masterdesc = value; }
            get { return _masterdesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT1", DbType.String, 60)]
        public String DESCRIPT1
        {
            set { _descript1 = value; }
            get { return _descript1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT2", DbType.String, 60)]
        public String DESCRIPT2
        {
            set { _descript2 = value; }
            get { return _descript2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("DESCRIPT3", DbType.String, 60)]
        public String DESCRIPT3
        {
            set { _descript3 = value; }
            get { return _descript3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(188)]
        /// </summary>
        [FieldMapping("DESCRIPT4", DbType.String, 188)]
        public String DESCRIPT4
        {
            set { _descript4 = value; }
            get { return _descript4; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("invrmk1", DbType.String, 100)]
        public String invrmk1
        {
            set { _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("invrmk2", DbType.String, 100)]
        public String invrmk2
        {
            set { _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.StringFixedLength, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("suppliercode", DbType.String, 10)]
        public String suppliercode
        {
            set { _suppliercode = value; }
            get { return _suppliercode; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbOtherSegments : BaseList<tbOtherSegment, tbOtherSegments> { }
}
