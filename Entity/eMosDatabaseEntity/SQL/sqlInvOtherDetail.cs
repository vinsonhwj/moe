﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlOtherDetail
    /// </summary>
    [Serializable]
    public class sqlInvOtherDetail : BaseSqlTable<sqlInvOtherDetail>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            agetype,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            segnum,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            amt,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            gsttaxshow,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            segType,
        }

        #region Model
        private Decimal _gsttax;
        private String _agetype;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private String _gsttaxshow;
        private Int32 _qty;
        private String _segtype;
        /// <summary>
        /// [Un-Null/decimal(2,2)]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.Decimal, 0)]
        public Decimal GSTTAX
        {
            set { _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("agetype", DbType.String, 6)]
        public String agetype
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segnum", DbType.String, 5)]
        public String segnum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.StringFixedLength, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amt", DbType.Decimal, 36)]
        public Decimal amt
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 36)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("gsttaxshow", DbType.StringFixedLength, 1)]
        public String gsttaxshow
        {
            set { _gsttaxshow = value; }
            get { return _gsttaxshow; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segType", DbType.String, 5)]
        public String segType
        {
            set { _segtype = value; }
            get { return _segtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlInvOtherDetails : BaseList<sqlInvOtherDetail, sqlInvOtherDetails> { }
    public class sqlInvOtherDetailPage : PageResult<sqlInvOtherDetail, sqlInvOtherDetails> { }
}