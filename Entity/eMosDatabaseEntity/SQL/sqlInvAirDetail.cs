﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    public class sqlInvAirDetail : BaseSqlTable<sqlInvAirDetail>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            showdetailfee,
            /// <summary>
            ///
            /// </summary>
            agetype,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            segnum,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            amtfee,
            /// <summary>
            ///
            /// </summary>
            amt,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            User_QTY,
            /// <summary>
            ///
            /// </summary>
            AIR_QTY,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            showamt,
        }

        #region Model
        private String _showdetailfee;
        private String _agetype;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _curr;
        private Decimal _amtfee;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Int32 _user_qty;
        private Int32 _air_qty;
        private Int32 _qty;
        private Decimal _showamt;
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("showdetailfee", DbType.StringFixedLength, 1)]
        public String showdetailfee
        {
            set { _showdetailfee = value; }
            get { return _showdetailfee; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("agetype", DbType.String, 3)]
        public String agetype
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segnum", DbType.String, 5)]
        public String segnum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.String, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amtfee", DbType.Decimal, 36)]
        public Decimal amtfee
        {
            set { _amtfee = value; }
            get { return _amtfee; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amt", DbType.Decimal, 36)]
        public Decimal amt
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 36)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("User_QTY", DbType.Int32, 10)]
        public Int32 User_QTY
        {
            set { _user_qty = value; }
            get { return _user_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("AIR_QTY", DbType.Int32, 10)]
        public Int32 AIR_QTY
        {
            set { _air_qty = value; }
            get { return _air_qty; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("showamt", DbType.Decimal, 36)]
        public Decimal showamt
        {
            set { _showamt = value; }
            get { return _showamt; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlInvAirDetails : BaseList<sqlInvAirDetail, sqlInvAirDetails> { }
    public class sqlInvAirDetailPage : PageResult<sqlInvAirDetail, sqlInvAirDetails> { }
}

