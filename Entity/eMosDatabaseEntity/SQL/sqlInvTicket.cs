﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    public class sqlInvTicket : BaseSqlTable<sqlInvTicket>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            seq,
            /// <summary>
            ///
            /// </summary>
            paidFare,
            /// <summary>
            ///
            /// </summary>
            tktseq,
            /// <summary>
            ///
            /// </summary>
            routing,
            /// <summary>
            ///
            /// </summary>
            ticket,
            /// <summary>
            ///
            /// </summary>
            totaltax,
            /// <summary>
            ///
            /// </summary>
            fullcurr,
            /// <summary>
            ///
            /// </summary>
            fullfare,
            /// <summary>
            ///
            /// </summary>
            AirCode,
            /// <summary>
            ///
            /// </summary>
            TicketType,
            /// <summary>
            ///
            /// </summary>
            FormPay,
            /// <summary>
            ///
            /// </summary>
            iss_in_exchange,
        }

        #region Model
        private Int32 _seq;
        private Decimal _paidfare;
        private String _tktseq;
        private String _routing;
        private String _ticket;
        private Decimal _totaltax;
        private String _fullcurr;
        private Decimal _fullfare;
        private String _aircode;
        private String _tickettype;
        private String _formpay;
        private String _iss_in_exchange;
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("seq", DbType.Int32, 10)]
        public Int32 seq
        {
            set { _seq = value; }
            get { return _seq; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("paidFare", DbType.Decimal, 14)]
        public Decimal paidFare
        {
            set { _paidfare = value; }
            get { return _paidfare; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("tktseq", DbType.String, 2)]
        public String tktseq
        {
            set { _tktseq = value; }
            get { return _tktseq; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(65)]
        /// </summary>
        [FieldMapping("routing", DbType.String, 65)]
        public String routing
        {
            set { _routing = value; }
            get { return _routing; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ticket", DbType.String, 10)]
        public String ticket
        {
            set { _ticket = value; }
            get { return _ticket; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("totaltax", DbType.Decimal, 14)]
        public Decimal totaltax
        {
            set { _totaltax = value; }
            get { return _totaltax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("fullcurr", DbType.String, 3)]
        public String fullcurr
        {
            set { _fullcurr = value; }
            get { return _fullcurr; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("fullfare", DbType.Decimal, 14)]
        public Decimal fullfare
        {
            set { _fullfare = value; }
            get { return _fullfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AirCode", DbType.String, 3)]
        public String AirCode
        {
            set { _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Un-Null/varchar(1)]
        /// </summary>
        [FieldMapping("TicketType", DbType.AnsiString, 1)]
        public String TicketType
        {
            set { _tickettype = value; }
            get { return _tickettype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FormPay", DbType.String, 50)]
        public String FormPay
        {
            set { _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("iss_in_exchange", DbType.String, 100)]
        public String iss_in_exchange
        {
            set { _iss_in_exchange = value; }
            get { return _iss_in_exchange; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlInvTickets : BaseList<sqlInvTicket, sqlInvTickets> { }
    public class sqlInvTicketPage : PageResult<sqlInvTicket, sqlInvTickets> { }
}


