﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlVchTitle
    /// </summary>
    [Serializable]
    public class sqlVchTitle : BaseSqlTable<sqlVchTitle>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME,
            /// <summary>
            ///
            /// </summary>
            ADDR1,
            /// <summary>
            ///
            /// </summary>
            ADDR2,
            /// <summary>
            ///
            /// </summary>
            ADDR3,
            /// <summary>
            ///
            /// </summary>
            ADDR4,
            /// <summary>
            ///
            /// </summary>
            HTLDESC,
            /// <summary>
            ///
            /// </summary>
            GeneralPhone,
            /// <summary>
            ///
            /// </summary>
            GeneralFax,
            /// <summary>
            ///
            /// </summary>
            suppcode,
            /// <summary>
            ///
            /// </summary>
            countrycode,
            /// <summary>
            ///
            /// </summary>
            HotelName_Zhcn,
            /// <summary>
            ///
            /// </summary>
            Addr1_Zhcn,
            /// <summary>
            ///
            /// </summary>
            Addr2_Zhcn,
            /// <summary>
            ///
            /// </summary>
            Addr3_Zhcn,
            /// <summary>
            ///
            /// </summary>
            Addr4_Zhcn,
            /// <summary>
            ///
            /// </summary>
            HotelName_ZhTW,
            /// <summary>
            ///
            /// </summary>
            Addr1_ZhTW,
            /// <summary>
            ///
            /// </summary>
            Addr2_ZhTW,
            /// <summary>
            ///
            /// </summary>
            Addr3_ZhTW,
            /// <summary>
            ///
            /// </summary>
            Addr4_ZhTW,
        }

        #region Model
        private String _hotelcode;
        private String _hotelname;
        private String _addr1;
        private String _addr2;
        private String _addr3;
        private String _addr4;
        private String _htldesc;
        private String _generalphone;
        private String _generalfax;
        private String _suppcode;
        private String _countrycode;
        private String _hotelname_zhcn;
        private String _addr1_zhcn;
        private String _addr2_zhcn;
        private String _addr3_zhcn;
        private String _addr4_zhcn;
        private String _hotelname_zhtw;
        private String _addr1_zhtw;
        private String _addr2_zhtw;
        private String _addr3_zhtw;
        private String _addr4_zhtw;
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("HOTELCODE", DbType.String, 8)]
        public String HOTELCODE
        {
            set { _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("HOTELNAME", DbType.String, 200)]
        public String HOTELNAME
        {
            set { _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR1", DbType.String, 50)]
        public String ADDR1
        {
            set { _addr1 = value; }
            get { return _addr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR2", DbType.String, 50)]
        public String ADDR2
        {
            set { _addr2 = value; }
            get { return _addr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR3", DbType.String, 50)]
        public String ADDR3
        {
            set { _addr3 = value; }
            get { return _addr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR4", DbType.String, 50)]
        public String ADDR4
        {
            set { _addr4 = value; }
            get { return _addr4; }
        }
        /// <summary>
        /// [Allow Null/text(2147483647)]
        /// </summary>
        [FieldMapping("HTLDESC", DbType.String, 2147483647)]
        public String HTLDESC
        {
            set { _htldesc = value; }
            get { return _htldesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("GeneralPhone", DbType.String, 50)]
        public String GeneralPhone
        {
            set { _generalphone = value; }
            get { return _generalphone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("GeneralFax", DbType.String, 50)]
        public String GeneralFax
        {
            set { _generalfax = value; }
            get { return _generalfax; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("suppcode", DbType.String, 8)]
        public String suppcode
        {
            set { _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("countrycode", DbType.StringFixedLength, 3)]
        public String countrycode
        {
            set { _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(400)]
        /// </summary>
        [FieldMapping("HotelName_Zhcn", DbType.String, 400)]
        public String HotelName_Zhcn
        {
            set { _hotelname_zhcn = value; }
            get { return _hotelname_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr1_Zhcn", DbType.StringFixedLength, 80)]
        public String Addr1_Zhcn
        {
            set { _addr1_zhcn = value; }
            get { return _addr1_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr2_Zhcn", DbType.StringFixedLength, 80)]
        public String Addr2_Zhcn
        {
            set { _addr2_zhcn = value; }
            get { return _addr2_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr3_Zhcn", DbType.StringFixedLength, 80)]
        public String Addr3_Zhcn
        {
            set { _addr3_zhcn = value; }
            get { return _addr3_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr4_Zhcn", DbType.StringFixedLength, 80)]
        public String Addr4_Zhcn
        {
            set { _addr4_zhcn = value; }
            get { return _addr4_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(400)]
        /// </summary>
        [FieldMapping("HotelName_ZhTW", DbType.String, 400)]
        public String HotelName_ZhTW
        {
            set { _hotelname_zhtw = value; }
            get { return _hotelname_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr1_ZhTW", DbType.StringFixedLength, 80)]
        public String Addr1_ZhTW
        {
            set { _addr1_zhtw = value; }
            get { return _addr1_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr2_ZhTW", DbType.StringFixedLength, 80)]
        public String Addr2_ZhTW
        {
            set { _addr2_zhtw = value; }
            get { return _addr2_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr3_ZhTW", DbType.StringFixedLength, 80)]
        public String Addr3_ZhTW
        {
            set { _addr3_zhtw = value; }
            get { return _addr3_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("Addr4_ZhTW", DbType.StringFixedLength, 80)]
        public String Addr4_ZhTW
        {
            set { _addr4_zhtw = value; }
            get { return _addr4_zhtw; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlVchTitles : BaseList<sqlVchTitle, sqlVchTitles> { }
    public class sqlVchTitlePage : PageResult<sqlVchTitle, sqlVchTitles> { }
}


