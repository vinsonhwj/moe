﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlVchOtherDetail
    /// </summary>
    [Serializable]
    public class sqlVchOtherDetail : BaseSqlTable<sqlVchOtherDetail>
    {
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            agetype,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            segnum,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            amt,
            /// <summary>
            ///
            /// </summary>
            taxcurr,
            /// <summary>
            ///
            /// </summary>
            taxamt,
            /// <summary>
            ///
            /// </summary>
            QTY,
        }

        #region Model
        private String _agetype;
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Int32 _qty;
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("agetype", DbType.String, 6)]
        public String agetype
        {
            set { _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("segnum", DbType.String, 5)]
        public String segnum
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.StringFixedLength, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("amt", DbType.Decimal, 36)]
        public Decimal amt
        {
            set { _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("taxcurr", DbType.String, 3)]
        public String taxcurr
        {
            set { _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(38,2)]
        /// </summary>
        [FieldMapping("taxamt", DbType.Decimal, 36)]
        public Decimal taxamt
        {
            set { _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { _qty = value; }
            get { return _qty; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlVchOtherDetails : BaseList<sqlVchOtherDetail, sqlVchOtherDetails> { }
    public class sqlVchOtherDetailPage : PageResult<sqlVchOtherDetail, sqlVchOtherDetails> { }
}


