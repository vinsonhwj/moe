﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility.TableMapping;
using System.Data;
using hwj.DBUtility.Entity;
using hwj.DBUtility;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable]
    public class tbMultiCurr : BaseSqlTable<tbMultiCurr>
    {
        public tbMultiCurr()
            : base(CommandText)
        { }
        //        public const string CommandText = @" DECLARE @sql AS NVARCHAR(500)
        //                                              SELECT @sql=''
        //                                              SELECT @sql= (CASE WHEN @AirSegNumList='' THEN  @sql + ' select distinct isnull(curr,'''') as curr from peoairdetail where bkgref='''+ @bkgRef +''' and segnum in (SELECT VALUE FROM dbo.f_SplitString('''+ @AirSegNumList +''','','')) and seqtype=''COST''  UNION ' ELSE @sql END  )
        //                                              SELECT @sql =@sql +' select distinct  isnull(curr,'''') as curr from peohtldetail where bkgref='''+ @bkgRef +''' and segnum in (SELECT VALUE FROM dbo.f_SplitString('''+ @HotelSegNumList +''',',')) and seqtype=''COST''  union select distinct  isnull(curr,'''') as curr from peootherdetail where bkgref='''+ @bkgRef +''' and segnum in (SELECT VALUE FROM dbo.f_SplitString('''+ @OhterSegNumList +''',',')) and seqtype=''COST''  '
        //                                              EXEC (@sql)";

        public const string CommandText = @"select distinct isnull(curr,'') as curr from peoairdetail where bkgref= @bkgRef  and segnum in (SELECT VALUE FROM dbo.f_SplitString( @AirSegNumList ,',')) and seqtype='COST'
                                            AND @AirSegNumList!='' UNION
                                            select distinct  isnull(curr,'') as curr from peohtldetail where bkgref=@bkgRef  and segnum in (SELECT VALUE FROM dbo.f_SplitString( @HotelSegNumList ,',')) and seqtype='COST'  union select distinct  isnull(curr,'') as curr from peootherdetail where bkgref=@bkgRef  and segnum in (SELECT VALUE FROM dbo.f_SplitString( @OhterSegNumList ,',')) and seqtype='COST'";
        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            curr,
        }

        #region Model
        private String _curr;
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.String, 3)]
        public String curr
        {
            set { _curr = value; }
            get { return _curr; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbMultiCurrs : BaseList<tbMultiCurr, tbMultiCurrs> { }
}
