﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    /// <summary>
    /// SQL:sqlVchPax
    /// </summary>
    [Serializable]
    public class sqlVchPax : BaseSqlTable<sqlVchPax>
    {
        //public sqlVchPax()
        //    : base(CommandText)
        //{ }
        //public const string CommandText = @"select BKGREF,COMPANYCODE,SEGNUM,PAXNUM,PAXLNAME,PAXFNAME,PAXTITLE,PAXTYPE,ISNULL(PAXAIR,'') AS PAXAIR,ISNULL(PAXHOTEL,'') AS PAXHOTEL,ISNULL(PAXOTHER,'') AS PAXOTHER,Isnull(paxage,'') as paxage from peopax (NOLOCK) where bkgref=N'000001689' and SEGNUM in (N'00001',N'00002', '') order by segnum";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            PAXNUM,
            /// <summary>
            ///
            /// </summary>
            PAXLNAME,
            /// <summary>
            ///
            /// </summary>
            PAXFNAME,
            /// <summary>
            ///
            /// </summary>
            PAXTITLE,
            /// <summary>
            ///
            /// </summary>
            PAXTYPE,
            /// <summary>
            ///
            /// </summary>
            PAXAIR,
            /// <summary>
            ///
            /// </summary>
            PAXHOTEL,
            /// <summary>
            ///
            /// </summary>
            PAXOTHER,
            /// <summary>
            ///
            /// </summary>
            paxage,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _paxnum;
        private String _paxlname;
        private String _paxfname;
        private String _paxtitle;
        private String _paxtype;
        private String _paxair;
        private String _paxhotel;
        private String _paxother;
        private Int16 _paxage;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PAXNUM", DbType.String, 5)]
        public String PAXNUM
        {
            set { _paxnum = value; }
            get { return _paxnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PAXLNAME", DbType.String, 20)]
        public String PAXLNAME
        {
            set { _paxlname = value; }
            get { return _paxlname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("PAXFNAME", DbType.String, 40)]
        public String PAXFNAME
        {
            set { _paxfname = value; }
            get { return _paxfname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("PAXTITLE", DbType.String, 4)]
        public String PAXTITLE
        {
            set { _paxtitle = value; }
            get { return _paxtitle; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXTYPE", DbType.String, 3)]
        public String PAXTYPE
        {
            set { _paxtype = value; }
            get { return _paxtype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXAIR", DbType.String, 3)]
        public String PAXAIR
        {
            set { _paxair = value; }
            get { return _paxair; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXHOTEL", DbType.String, 3)]
        public String PAXHOTEL
        {
            set { _paxhotel = value; }
            get { return _paxhotel; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXOTHER", DbType.String, 3)]
        public String PAXOTHER
        {
            set { _paxother = value; }
            get { return _paxother; }
        }
        /// <summary>
        /// [Un-Null/smallint(5)]
        /// </summary>
        [FieldMapping("paxage", DbType.Int16, 5)]
        public Int16 paxage
        {
            set { _paxage = value; }
            get { return _paxage; }
        }
        #endregion Model

    }
    [Serializable]
    public class sqlVchPaxs : BaseList<sqlVchPax, sqlVchPaxs> { }
    public class sqlVchPaxPage : PageResult<sqlVchPax, sqlVchPaxs> { }
}

