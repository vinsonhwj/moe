﻿using System;
using System.Collections.Generic;
using System.Text;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;
using System.Data;

namespace Westminster.MOE.Entity.eMosDBEntity.SQL
{
    [Serializable ]
   public  class sqlXoHotel :BaseSqlTable <sqlXoHotel>
    {
        public sqlXoHotel()
            : base(CommandText)
        { }

        public const string CommandText = @"SELECT  a.hotelcode ,
                                                      ISNULL(vchrmk1, '') AS vchrmk1 ,
                                                      ISNULL(vchrmk2, '') AS vchrmk2 ,
                                                      a.SEGNUM ,
                                                      a.COUNTRYCODE ,
                                                      a.CITYCODE ,
                                                      ISNULL(a.ARRDATE, '') AS ARRDATE ,
                                                      a.ARRTIME ,
                                                      a.eta ,
                                                      ISNULL(a.departdate, '') AS departdate ,
                                                      a.departtime ,
                                                      a.etd ,
                                                      a.rmk1 ,
                                                      a.rmk2 ,
                                                      a.spcl_request ,
                                                      a.HTLREFERENCE ,
                                                      a.HTLCFMBY ,   
                                                      ISNULL(b.hotelname, ISNULL(a.hotelname, '')) AS HotelName ,
                                                      c.countryname AS newcountryname ,
                                                      d.cityname AS newcityname
                                              FROM    peohtl a
                                                      LEFT JOIN peohotel b ON a.hotelcode = b.hotelcode
                                                      JOIN country_EN_US c ON a.countrycode = c.countrycode
                                                      JOIN city_EN_US d ON a.citycode = d.citycode
                                              WHERE   bkgref = @bkgRef
                                                      AND companycode = @companyCode
                                                      AND SegNum IN ( SELECT VALUE FROM dbo.f_SplitString(@SegNum,',') )
                                              ORDER BY a.SEGNUM";

        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            hotelcode,
            /// <summary>
            ///
            /// </summary>
            vchrmk1,
            /// <summary>
            ///
            /// </summary>
            vchrmk2,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            ARRTIME,
            /// <summary>
            ///
            /// </summary>
            eta,
            /// <summary>
            ///
            /// </summary>
            departdate,
            /// <summary>
            ///
            /// </summary>
            departtime,
            /// <summary>
            ///
            /// </summary>
            etd,
            /// <summary>
            ///
            /// </summary>
            rmk1,
            /// <summary>
            ///
            /// </summary>
            rmk2,
            /// <summary>
            ///
            /// </summary>
            spcl_request,
            /// <summary>
            ///
            /// </summary>
            HTLREFERENCE,
            /// <summary>
            ///
            /// </summary>
            HTLCFMBY,
            /// <summary>
            ///
            /// </summary>
            HotelName,
            /// <summary>
            ///
            /// </summary>
            newcountryname,
            /// <summary>
            ///
            /// </summary>
            newcityname,
        }

        #region Model
        private String _hotelcode;
        private String _vchrmk1;
        private String _vchrmk2;
        private String _segnum;
        private String _countrycode;
        private String _citycode;
        private DateTime _arrdate;
        private String _arrtime;
        private String _eta;
        private DateTime _departdate;
        private String _departtime;
        private String _etd;
        private String _rmk1;
        private String _rmk2;
        private String _spcl_request;
        private String _htlreference;
        private String _htlcfmby;
        private String _hotelname;
        private String _newcountryname;
        private String _newcityname;
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("hotelcode", DbType.String, 8)]
        public String hotelcode
        {
            set { _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("vchrmk1", DbType.String, 80)]
        public String vchrmk1
        {
            set { _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("vchrmk2", DbType.String, 80)]
        public String vchrmk2
        {
            set { _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.String, 3)]
        public String COUNTRYCODE
        {
            set { _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.String, 3)]
        public String CITYCODE
        {
            set { _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("ARRTIME", DbType.String, 6)]
        public String ARRTIME
        {
            set { _arrtime = value; }
            get { return _arrtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("eta", DbType.String, 14)]
        public String eta
        {
            set { _eta = value; }
            get { return _eta; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("departdate", DbType.DateTime, 8)]
        public DateTime departdate
        {
            set { _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("departtime", DbType.String, 6)]
        public String departtime
        {
            set { _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("etd", DbType.String, 14)]
        public String etd
        {
            set { _etd = value; }
            get { return _etd; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("rmk1", DbType.String, 50)]
        public String rmk1
        {
            set { _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("rmk2", DbType.String, 50)]
        public String rmk2
        {
            set { _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("spcl_request", DbType.String, 255)]
        public String spcl_request
        {
            set { _spcl_request = value; }
            get { return _spcl_request; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("HTLREFERENCE", DbType.String, 20)]
        public String HTLREFERENCE
        {
            set { _htlreference = value; }
            get { return _htlreference; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("HTLCFMBY", DbType.String, 30)]
        public String HTLCFMBY
        {
            set { _htlcfmby = value; }
            get { return _htlcfmby; }
        }
       
        /// <summary>
        /// [Un-Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("HotelName", DbType.String, 200)]
        public String HotelName
        {
            set { _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Un-Null/nchar(40)]
        /// </summary>
        [FieldMapping("newcountryname", DbType.StringFixedLength, 40)]
        public String newcountryname
        {
            set { _newcountryname = value; }
            get { return _newcountryname; }
        }
        /// <summary>
        /// [Un-Null/nchar(40)]
        /// </summary>
        [FieldMapping("newcityname", DbType.StringFixedLength, 40)]
        public String newcityname
        {
            set { _newcityname = value; }
            get { return _newcityname; }
        }
        #endregion Model

    }
    public class sqlXoHotels : BaseList<sqlXoHotel, sqlXoHotels> { }
    public class sqlXoHotelPage : PageResult<sqlXoHotel, sqlXoHotels> { }
}
