﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:eReportSyncLog
    /// </summary>
    [Serializable]
    public class tbeReportSyncLog : BaseTable<tbeReportSyncLog>
    {
        public tbeReportSyncLog()
            : base(DBTableName)
        { }
        public const string DBTableName = "eReportSyncLog";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            id,
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            Docnum,
            /// <summary>
            ///
            /// </summary>
            SyncTable,
            /// <summary>
            ///
            /// </summary>
            SyncStatus,
            /// <summary>
            ///
            /// </summary>
            SyncDate,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
        }

        #region Model
        private Int32 _id;
        private String _companycode;
        private String _docnum;
        private String _synctable;
        private Int32 _syncstatus;
        private DateTime _syncdate;
        private DateTime _createon;
        /// <summary>
        /// [PK/Un-Null/int(10)]
        /// </summary>
        [FieldMapping("id", DbType.Int32, 10, Enums.DataHandle.UnInsert, Enums.DataHandle.UnUpdate)]
        public Int32 id
        {
            set { AddAssigned("id"); _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// [Un-Null/varchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.AnsiString, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/varchar(20)]
        /// </summary>
        [FieldMapping("Docnum", DbType.AnsiString, 20)]
        public String Docnum
        {
            set { AddAssigned("Docnum"); _docnum = value; }
            get { return _docnum; }
        }
        /// <summary>
        /// [Un-Null/varchar(20)]
        /// </summary>
        [FieldMapping("SyncTable", DbType.AnsiString, 20)]
        public String SyncTable
        {
            set { AddAssigned("SyncTable"); _synctable = value; }
            get { return _synctable; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("SyncStatus", DbType.Int32, 10)]
        public Int32 SyncStatus
        {
            set { AddAssigned("SyncStatus"); _syncstatus = value; }
            get { return _syncstatus; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("SyncDate", DbType.DateTime, 8)]
        public DateTime SyncDate
        {
            set { AddAssigned("SyncDate"); _syncdate = value; }
            get { return _syncdate; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbeReportSyncLogs : BaseList<tbeReportSyncLog, tbeReportSyncLogs> { }
    public class tbeReportSyncLogPage : PageResult<tbeReportSyncLog, tbeReportSyncLogs> { }
}

