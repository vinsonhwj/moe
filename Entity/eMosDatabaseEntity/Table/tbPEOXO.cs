﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOXO
    /// </summary>
    [Serializable]
    public class tbPEOXO : BaseTable<tbPEOXO>
    {
        public tbPEOXO()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOXO";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            XONUM,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            INVNUM,
            /// <summary>
            ///
            /// </summary>
            SUPPCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPNAME,
            /// <summary>
            ///
            /// </summary>
            SUPPADDR,
            /// <summary>
            ///
            /// </summary>
            SUPPTEL,
            /// <summary>
            ///
            /// </summary>
            SUPPFAX,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            AIRCODE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            FORMPAY,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            COSTAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            HKDAMT,
            /// <summary>
            ///
            /// </summary>
            REMARK1,
            /// <summary>
            ///
            /// </summary>
            REMARK2,
            /// <summary>
            ///
            /// </summary>
            CFMBY,
            /// <summary>
            ///
            /// </summary>
            XOREF,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            RMK3,
            /// <summary>
            ///
            /// </summary>
            RMK4,
            /// <summary>
            ///
            /// </summary>
            RMK5,
            /// <summary>
            ///
            /// </summary>
            RMK6,
            /// <summary>
            ///
            /// </summary>
            PRINTTEAM,
            /// <summary>
            ///
            /// </summary>
            VOID2JBA,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VOIDTEAM,
            /// <summary>
            ///
            /// </summary>
            VOIDREASON,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            gstpct,
            /// <summary>
            ///
            /// </summary>
            gstamt,
            /// <summary>
            ///
            /// </summary>
            airRmk,
            /// <summary>
            ///
            /// </summary>
            staffname,
            /// <summary>
            ///
            /// </summary>
            stafftel,
            /// <summary>
            ///
            /// </summary>
            gstair,
            /// <summary>
            ///
            /// </summary>
            gsthotel,
            /// <summary>
            ///
            /// </summary>
            gstother,
            /// <summary>
            ///
            /// </summary>
            CostExcludeTax,
            /// <summary>
            ///
            /// </summary>
            TOURCODE,
            /// <summary>
            ///
            /// </summary>
            salesperson,
            /// <summary>
            ///
            /// </summary>
            ATTN,
            /// <summary>
            ///
            /// </summary>
            IATA,
            /// <summary>
            ///
            /// </summary>
            Payment,
            /// <summary>
            ///
            /// </summary>
            ADULTNUM,
            /// <summary>
            ///
            /// </summary>
            CHILDNUM,
            /// <summary>
            ///
            /// </summary>
            HOTELADDR,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME,
            /// <summary>
            ///
            /// </summary>
            HOTELTEL,
            /// <summary>
            ///
            /// </summary>
            OurRef,
            /// <summary>
            ///
            /// </summary>
            hotelref,
            /// <summary>
            ///
            /// </summary>
            SPCL_REQUEST,
            /// <summary>
            ///
            /// </summary>
            ARRDTL,
            /// <summary>
            ///
            /// </summary>
            XORemark,
            /// <summary>
            ///
            /// </summary>
            DEPDTL,
        }

        #region Model
        private String _companycode;
        private String _xonum;
        private String _bkgref;
        private String _pnr;
        private String _invnum;
        private String _suppcode;
        private String _suppname;
        private String _suppaddr;
        private String _supptel;
        private String _suppfax;
        private String _staffcode;
        private String _teamcode;
        private String _aircode;
        private DateTime _departdate;
        private String _formpay;
        private String _costcurr;
        private Decimal _costamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _hkdamt;
        private String _remark1;
        private String _remark2;
        private String _cfmby;
        private String _xoref;
        private String _rmk1;
        private String _rmk2;
        private String _rmk3;
        private String _rmk4;
        private String _rmk5;
        private String _rmk6;
        private String _printteam;
        private String _void2jba;
        private DateTime _voidon;
        private String _voidby;
        private String _voidteam;
        private String _voidreason;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Decimal _gstpct;
        private Decimal _gstamt;
        private String _airrmk;
        private String _staffname;
        private String _stafftel;
        private Decimal _gstair;
        private Decimal _gsthotel;
        private Decimal _gstother;
        private Decimal _costexcludetax;
        private String _tourcode;
        private String _salesperson;
        private String _attn;
        private String _iata;
        private String _payment;
        private Decimal _adultnum;
        private Decimal _childnum;
        private String _hoteladdr;
        private String _hotelname;
        private String _hoteltel;
        private String _ourref;
        private String _hotelref;
        private String _spcl_request;
        private String _arrdtl;
        private String _xoremark;
        private String _depdtl;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("XONUM", DbType.String, 10)]
        public String XONUM
        {
            set { AddAssigned("XONUM"); _xonum = value; }
            get { return _xonum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("INVNUM", DbType.String, 10)]
        public String INVNUM
        {
            set { AddAssigned("INVNUM"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SUPPCODE", DbType.String, 8)]
        public String SUPPCODE
        {
            set { AddAssigned("SUPPCODE"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("SUPPNAME", DbType.String, 70)]
        public String SUPPNAME
        {
            set { AddAssigned("SUPPNAME"); _suppname = value; }
            get { return _suppname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(500)]
        /// </summary>
        [FieldMapping("SUPPADDR", DbType.String, 500)]
        public String SUPPADDR
        {
            set { AddAssigned("SUPPADDR"); _suppaddr = value; }
            get { return _suppaddr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("SUPPTEL", DbType.String, 20)]
        public String SUPPTEL
        {
            set { AddAssigned("SUPPTEL"); _supptel = value; }
            get { return _supptel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("SUPPFAX", DbType.String, 20)]
        public String SUPPFAX
        {
            set { AddAssigned("SUPPFAX"); _suppfax = value; }
            get { return _suppfax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.String, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AIRCODE", DbType.String, 3)]
        public String AIRCODE
        {
            set { AddAssigned("AIRCODE"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FORMPAY", DbType.String, 50)]
        public String FORMPAY
        {
            set { AddAssigned("FORMPAY"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("COSTAMT", DbType.Decimal, 14)]
        public Decimal COSTAMT
        {
            set { AddAssigned("COSTAMT"); _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("HKDAMT", DbType.Decimal, 14)]
        public Decimal HKDAMT
        {
            set { AddAssigned("HKDAMT"); _hkdamt = value; }
            get { return _hkdamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("REMARK1", DbType.String, 70)]
        public String REMARK1
        {
            set { AddAssigned("REMARK1"); _remark1 = value; }
            get { return _remark1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("REMARK2", DbType.String, 70)]
        public String REMARK2
        {
            set { AddAssigned("REMARK2"); _remark2 = value; }
            get { return _remark2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("CFMBY", DbType.String, 12)]
        public String CFMBY
        {
            set { AddAssigned("CFMBY"); _cfmby = value; }
            get { return _cfmby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("XOREF", DbType.String, 80)]
        public String XOREF
        {
            set { AddAssigned("XOREF"); _xoref = value; }
            get { return _xoref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 60)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 60)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK3", DbType.String, 60)]
        public String RMK3
        {
            set { AddAssigned("RMK3"); _rmk3 = value; }
            get { return _rmk3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK4", DbType.String, 60)]
        public String RMK4
        {
            set { AddAssigned("RMK4"); _rmk4 = value; }
            get { return _rmk4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK5", DbType.String, 60)]
        public String RMK5
        {
            set { AddAssigned("RMK5"); _rmk5 = value; }
            get { return _rmk5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK6", DbType.String, 60)]
        public String RMK6
        {
            set { AddAssigned("RMK6"); _rmk6 = value; }
            get { return _rmk6; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PRINTTEAM", DbType.String, 5)]
        public String PRINTTEAM
        {
            set { AddAssigned("PRINTTEAM"); _printteam = value; }
            get { return _printteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOID2JBA", DbType.String, 10)]
        public String VOID2JBA
        {
            set { AddAssigned("VOID2JBA"); _void2jba = value; }
            get { return _void2jba; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("VOIDTEAM", DbType.String, 5)]
        public String VOIDTEAM
        {
            set { AddAssigned("VOIDTEAM"); _voidteam = value; }
            get { return _voidteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VOIDREASON", DbType.String, 60)]
        public String VOIDREASON
        {
            set { AddAssigned("VOIDREASON"); _voidreason = value; }
            get { return _voidreason; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("gstpct", DbType.Decimal, 16)]
        public Decimal gstpct
        {
            set { AddAssigned("gstpct"); _gstpct = value; }
            get { return _gstpct; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("gstamt", DbType.Decimal, 16)]
        public Decimal gstamt
        {
            set { AddAssigned("gstamt"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("airRmk", DbType.String, 255)]
        public String airRmk
        {
            set { AddAssigned("airRmk"); _airrmk = value; }
            get { return _airrmk; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("staffname", DbType.String, 200)]
        public String staffname
        {
            set { AddAssigned("staffname"); _staffname = value; }
            get { return _staffname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("stafftel", DbType.String, 50)]
        public String stafftel
        {
            set { AddAssigned("stafftel"); _stafftel = value; }
            get { return _stafftel; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gstair", DbType.Decimal, 14)]
        public Decimal gstair
        {
            set { AddAssigned("gstair"); _gstair = value; }
            get { return _gstair; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gsthotel", DbType.Decimal, 14)]
        public Decimal gsthotel
        {
            set { AddAssigned("gsthotel"); _gsthotel = value; }
            get { return _gsthotel; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gstother", DbType.Decimal, 14)]
        public Decimal gstother
        {
            set { AddAssigned("gstother"); _gstother = value; }
            get { return _gstother; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("CostExcludeTax", DbType.Decimal, 16)]
        public Decimal CostExcludeTax
        {
            set { AddAssigned("CostExcludeTax"); _costexcludetax = value; }
            get { return _costexcludetax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("TOURCODE", DbType.String, 15)]
        public String TOURCODE
        {
            set { AddAssigned("TOURCODE"); _tourcode = value; }
            get { return _tourcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("salesperson", DbType.String, 20)]
        public String salesperson
        {
            set { AddAssigned("salesperson"); _salesperson = value; }
            get { return _salesperson; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("ATTN", DbType.String, 40)]
        public String ATTN
        {
            set { AddAssigned("ATTN"); _attn = value; }
            get { return _attn; }
        }
        /// <summary>
        /// [Allow Null/varchar(10)]
        /// </summary>
        [FieldMapping("IATA", DbType.AnsiString, 10)]
        public String IATA
        {
            set { AddAssigned("IATA"); _iata = value; }
            get { return _iata; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("Payment", DbType.String, 100)]
        public String Payment
        {
            set { AddAssigned("Payment"); _payment = value; }
            get { return _payment; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("ADULTNUM", DbType.Decimal, 3)]
        public Decimal ADULTNUM
        {
            set { AddAssigned("ADULTNUM"); _adultnum = value; }
            get { return _adultnum; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("CHILDNUM", DbType.Decimal, 3)]
        public Decimal CHILDNUM
        {
            set { AddAssigned("CHILDNUM"); _childnum = value; }
            get { return _childnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1000)]
        /// </summary>
        [FieldMapping("HOTELADDR", DbType.String, 1000)]
        public String HOTELADDR
        {
            set { AddAssigned("HOTELADDR"); _hoteladdr = value; }
            get { return _hoteladdr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("HOTELNAME", DbType.String, 100)]
        public String HOTELNAME
        {
            set { AddAssigned("HOTELNAME"); _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("HOTELTEL", DbType.String, 20)]
        public String HOTELTEL
        {
            set { AddAssigned("HOTELTEL"); _hoteltel = value; }
            get { return _hoteltel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("OurRef", DbType.String, 50)]
        public String OurRef
        {
            set { AddAssigned("OurRef"); _ourref = value; }
            get { return _ourref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("hotelref", DbType.String, 200)]
        public String hotelref
        {
            set { AddAssigned("hotelref"); _hotelref = value; }
            get { return _hotelref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("SPCL_REQUEST", DbType.String, 255)]
        public String SPCL_REQUEST
        {
            set { AddAssigned("SPCL_REQUEST"); _spcl_request = value; }
            get { return _spcl_request; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("ARRDTL", DbType.String, 30)]
        public String ARRDTL
        {
            set { AddAssigned("ARRDTL"); _arrdtl = value; }
            get { return _arrdtl; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("XORemark", DbType.String, 200)]
        public String XORemark
        {
            set { AddAssigned("XORemark"); _xoremark = value; }
            get { return _xoremark; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DEPDTL", DbType.String, 30)]
        public String DEPDTL
        {
            set { AddAssigned("DEPDTL"); _depdtl = value; }
            get { return _depdtl; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOXOs : BaseList<tbPEOXO, tbPEOXOs> { }
    public class tbPEOXOPage : PageResult<tbPEOXO, tbPEOXOs> { }
}

