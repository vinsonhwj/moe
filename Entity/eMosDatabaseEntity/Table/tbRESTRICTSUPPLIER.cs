﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:RESTRICTSUPPLIER
    /// </summary>
    [Serializable]
    public class tbRESTRICTSUPPLIER : BaseTable<tbRESTRICTSUPPLIER>
    {
        public tbRESTRICTSUPPLIER()
            : base(DBTableName)
        { }
        public const string DBTableName = "RESTRICTSUPPLIER";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPCODE,
        }

        #region Model
        private String _companycode;
        private String _suppcode;
        /// <summary>
        /// [Un-Null/nchar(8)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 8)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nchar(32)]
        /// </summary>
        [FieldMapping("SUPPCODE", DbType.StringFixedLength, 32)]
        public String SUPPCODE
        {
            set { AddAssigned("SUPPCODE"); _suppcode = value; }
            get { return _suppcode; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbRESTRICTSUPPLIERs : BaseList<tbRESTRICTSUPPLIER, tbRESTRICTSUPPLIERs> { }
    public class tbRESTRICTSUPPLIERPage : PageResult<tbRESTRICTSUPPLIER, tbRESTRICTSUPPLIERs> { }
}

