﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:shortform_prod_AirlineClass
    /// </summary>
    [Serializable]
    public class tbshortform_prod_AirlineClass : BaseTable<tbshortform_prod_AirlineClass>
    {
        public tbshortform_prod_AirlineClass()
            : base(DBTableName)
        { }
        public const string DBTableName = "shortform_prod_AirlineClass";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            Airline,
            /// <summary>
            ///
            /// </summary>
            Class,
            /// <summary>
            ///
            /// </summary>
            ClassType,
        }

        #region Model
        private String _airline;
        private String _class;
        private Int32 _classtype;
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("Airline", DbType.StringFixedLength, 2)]
        public String Airline
        {
            set { AddAssigned("Airline"); _airline = value; }
            get { return _airline; }
        }
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("Class", DbType.StringFixedLength, 2)]
        public String Class
        {
            set { AddAssigned("Class"); _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("ClassType", DbType.Int32, 10)]
        public Int32 ClassType
        {
            set { AddAssigned("ClassType"); _classtype = value; }
            get { return _classtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbshortform_prod_AirlineClasss : BaseList<tbshortform_prod_AirlineClass, tbshortform_prod_AirlineClasss> { }
    public class tbshortform_prod_AirlineClassPage : PageResult<tbshortform_prod_AirlineClass, tbshortform_prod_AirlineClasss> { }
}

