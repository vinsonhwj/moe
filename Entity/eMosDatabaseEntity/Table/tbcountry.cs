﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:country
    /// </summary>
    [Serializable]
    public class tbcountry : BaseTable<tbcountry>
    {
        public tbcountry()
            : base(DBTableName)
        { }
        public const string DBTableName = "country";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            SOURCECODE,
            /// <summary>
            ///
            /// </summary>
            REGIONCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYNAME,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
        }

        #region Model
        private String _sourcecode;
        private String _regioncode;
        private String _countrycode;
        private String _countryname;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [Un-Null/char(3)]
        /// </summary>
        [FieldMapping("SOURCECODE", DbType.AnsiStringFixedLength, 3)]
        public String SOURCECODE
        {
            set { AddAssigned("SOURCECODE"); _sourcecode = value; }
            get { return _sourcecode; }
        }
        /// <summary>
        /// [Un-Null/char(10)]
        /// </summary>
        [FieldMapping("REGIONCODE", DbType.AnsiStringFixedLength, 10)]
        public String REGIONCODE
        {
            set { AddAssigned("REGIONCODE"); _regioncode = value; }
            get { return _regioncode; }
        }
        /// <summary>
        /// [PK/Un-Null/char(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.AnsiStringFixedLength, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Un-Null/char(40)]
        /// </summary>
        [FieldMapping("COUNTRYNAME", DbType.AnsiStringFixedLength, 40)]
        public String COUNTRYNAME
        {
            set { AddAssigned("COUNTRYNAME"); _countryname = value; }
            get { return _countryname; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/char(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.AnsiStringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/char(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.AnsiStringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbcountrys : BaseList<tbcountry, tbcountrys> { }
    public class tbcountryPage : PageResult<tbcountry, tbcountrys> { }
}

