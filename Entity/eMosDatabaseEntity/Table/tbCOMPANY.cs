﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:COMPANY
    /// </summary>
    [Serializable]
    public class tbCOMPANY : BaseTable<tbCOMPANY>
    {
        public tbCOMPANY()
            : base(DBTableName)
        { }
        public const string DBTableName = "COMPANY";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            COMPANYNAME,
            /// <summary>
            ///
            /// </summary>
            ADDR1,
            /// <summary>
            ///
            /// </summary>
            ADDR2,
            /// <summary>
            ///
            /// </summary>
            ADDR3,
            /// <summary>
            ///
            /// </summary>
            ADDR4,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            POSTAL,
            /// <summary>
            ///
            /// </summary>
            ENCODING,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            LOCALCURR,
            /// <summary>
            ///
            /// </summary>
            FOREIGNCURR,
            /// <summary>
            ///
            /// </summary>
            ACTIVE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            CRS,
            /// <summary>
            ///
            /// </summary>
            Pseudo,
            /// <summary>
            ///
            /// </summary>
            emailaddress,
            /// <summary>
            ///
            /// </summary>
            undoc_default_amt,
            /// <summary>
            ///
            /// </summary>
            enablePMC,
            /// <summary>
            ///
            /// </summary>
            enableCLC,
            /// <summary>
            ///
            /// </summary>
            viewReminder,
            /// <summary>
            ///
            /// </summary>
            enableVRC,
            /// <summary>
            ///
            /// </summary>
            enableMSRS,
        }

        #region Model
        private String _companycode;
        private String _companyname;
        private String _addr1;
        private String _addr2;
        private String _addr3;
        private String _addr4;
        private String _citycode;
        private String _countrycode;
        private String _postal;
        private String _encoding;
        private Decimal _gsttax;
        private String _localcurr;
        private String _foreigncurr;
        private String _active;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _crs;
        private String _pseudo;
        private String _emailaddress;
        private Decimal _undoc_default_amt;
        private String _enablepmc;
        private String _enableclc;
        private String _viewreminder;
        private String _enablevrc;
        private String _enablemsrs;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("COMPANYNAME", DbType.String, 100)]
        public String COMPANYNAME
        {
            set { AddAssigned("COMPANYNAME"); _companyname = value; }
            get { return _companyname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR1", DbType.String, 50)]
        public String ADDR1
        {
            set { AddAssigned("ADDR1"); _addr1 = value; }
            get { return _addr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR2", DbType.String, 50)]
        public String ADDR2
        {
            set { AddAssigned("ADDR2"); _addr2 = value; }
            get { return _addr2; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR3", DbType.String, 50)]
        public String ADDR3
        {
            set { AddAssigned("ADDR3"); _addr3 = value; }
            get { return _addr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR4", DbType.String, 50)]
        public String ADDR4
        {
            set { AddAssigned("ADDR4"); _addr4 = value; }
            get { return _addr4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.String, 3)]
        public String CITYCODE
        {
            set { AddAssigned("CITYCODE"); _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.String, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("POSTAL", DbType.String, 10)]
        public String POSTAL
        {
            set { AddAssigned("POSTAL"); _postal = value; }
            get { return _postal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ENCODING", DbType.String, 10)]
        public String ENCODING
        {
            set { AddAssigned("ENCODING"); _encoding = value; }
            get { return _encoding; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,4)]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.Decimal, 14)]
        public Decimal GSTTAX
        {
            set { AddAssigned("GSTTAX"); _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("LOCALCURR", DbType.StringFixedLength, 3)]
        public String LOCALCURR
        {
            set { AddAssigned("LOCALCURR"); _localcurr = value; }
            get { return _localcurr; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("FOREIGNCURR", DbType.StringFixedLength, 3)]
        public String FOREIGNCURR
        {
            set { AddAssigned("FOREIGNCURR"); _foreigncurr = value; }
            get { return _foreigncurr; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("ACTIVE", DbType.StringFixedLength, 1)]
        public String ACTIVE
        {
            set { AddAssigned("ACTIVE"); _active = value; }
            get { return _active; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("CRS", DbType.String, 1)]
        public String CRS
        {
            set { AddAssigned("CRS"); _crs = value; }
            get { return _crs; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("Pseudo", DbType.String, 4)]
        public String Pseudo
        {
            set { AddAssigned("Pseudo"); _pseudo = value; }
            get { return _pseudo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("emailaddress", DbType.String, 50)]
        public String emailaddress
        {
            set { AddAssigned("emailaddress"); _emailaddress = value; }
            get { return _emailaddress; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("undoc_default_amt", DbType.Decimal, 16)]
        public Decimal undoc_default_amt
        {
            set { AddAssigned("undoc_default_amt"); _undoc_default_amt = value; }
            get { return _undoc_default_amt; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("enablePMC", DbType.StringFixedLength, 1)]
        public String enablePMC
        {
            set { AddAssigned("enablePMC"); _enablepmc = value; }
            get { return _enablepmc; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("enableCLC", DbType.StringFixedLength, 1)]
        public String enableCLC
        {
            set { AddAssigned("enableCLC"); _enableclc = value; }
            get { return _enableclc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("viewReminder", DbType.String, 3)]
        public String viewReminder
        {
            set { AddAssigned("viewReminder"); _viewreminder = value; }
            get { return _viewreminder; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("enableVRC", DbType.String, 1)]
        public String enableVRC
        {
            set { AddAssigned("enableVRC"); _enablevrc = value; }
            get { return _enablevrc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("enableMSRS", DbType.String, 1)]
        public String enableMSRS
        {
            set { AddAssigned("enableMSRS"); _enablemsrs = value; }
            get { return _enablemsrs; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbCOMPANYs : BaseList<tbCOMPANY, tbCOMPANYs> { }
    public class tbCOMPANYPage : PageResult<tbCOMPANY, tbCOMPANYs> { }
}

