﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:shortform_prod_city
    /// </summary>
    [Serializable]
    public class tbshortform_prod_city : BaseTable<tbshortform_prod_city>
    {
        public tbshortform_prod_city()
            : base(DBTableName)
        { }
        public const string DBTableName = "shortform_prod_city";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            code,
            /// <summary>
            ///
            /// </summary>
            name_e,
            /// <summary>
            ///
            /// </summary>
            name_c,
            /// <summary>
            ///
            /// </summary>
            name_c1,
            /// <summary>
            ///
            /// </summary>
            country_code,
            /// <summary>
            ///
            /// </summary>
            airport_city_code,
            /// <summary>
            ///
            /// </summary>
            system,
            /// <summary>
            ///
            /// </summary>
            DATA_EXTRA,
            /// <summary>
            ///
            /// </summary>
            modify_time,
            /// <summary>
            ///
            /// </summary>
            VERSION_EXTRA,
        }

        #region Model
        private String _code;
        private String _name_e;
        private String _name_c;
        private String _name_c1;
        private String _country_code;
        private String _airport_city_code;
        private Byte _system;
        private String _data_extra;
        private DateTime _modify_time;
        private String _version_extra;
        /// <summary>
        /// [Un-Null/varchar(3)]
        /// </summary>
        [FieldMapping("code", DbType.AnsiString, 3)]
        public String code
        {
            set { AddAssigned("code"); _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// [Un-Null/varchar(255)]
        /// </summary>
        [FieldMapping("name_e", DbType.AnsiString, 255)]
        public String name_e
        {
            set { AddAssigned("name_e"); _name_e = value; }
            get { return _name_e; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("name_c", DbType.String, 255)]
        public String name_c
        {
            set { AddAssigned("name_c"); _name_c = value; }
            get { return _name_c; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("name_c1", DbType.String, 255)]
        public String name_c1
        {
            set { AddAssigned("name_c1"); _name_c1 = value; }
            get { return _name_c1; }
        }
        /// <summary>
        /// [Un-Null/varchar(50)]
        /// </summary>
        [FieldMapping("country_code", DbType.AnsiString, 50)]
        public String country_code
        {
            set { AddAssigned("country_code"); _country_code = value; }
            get { return _country_code; }
        }
        /// <summary>
        /// [Allow Null/varchar(3)]
        /// </summary>
        [FieldMapping("airport_city_code", DbType.AnsiString, 3)]
        public String airport_city_code
        {
            set { AddAssigned("airport_city_code"); _airport_city_code = value; }
            get { return _airport_city_code; }
        }
        /// <summary>
        /// [Un-Null/tinyint(1)]
        /// </summary>
        [FieldMapping("system", DbType.Byte, 1)]
        public Byte system
        {
            set { AddAssigned("system"); _system = value; }
            get { return _system; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("DATA_EXTRA", DbType.String, 255)]
        public String DATA_EXTRA
        {
            set { AddAssigned("DATA_EXTRA"); _data_extra = value; }
            get { return _data_extra; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("modify_time", DbType.DateTime, 8)]
        public DateTime modify_time
        {
            set { AddAssigned("modify_time"); _modify_time = value; }
            get { return _modify_time; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("VERSION_EXTRA", DbType.String, 255)]
        public String VERSION_EXTRA
        {
            set { AddAssigned("VERSION_EXTRA"); _version_extra = value; }
            get { return _version_extra; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbshortform_prod_citys : BaseList<tbshortform_prod_city, tbshortform_prod_citys> { }
    public class tbshortform_prod_cityPage : PageResult<tbshortform_prod_city, tbshortform_prod_citys> { }
}

