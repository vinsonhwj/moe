﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOINV
    /// </summary>
    [Serializable]
    public class tbPEOINV : BaseTable<tbPEOINV>
    {
        public tbPEOINV()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOINV";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            INVNUM,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            CLTCODE,
            /// <summary>
            ///
            /// </summary>
            CLTNAME,
            /// <summary>
            ///
            /// </summary>
            CLTADDR,
            /// <summary>
            ///
            /// </summary>
            PHONE,
            /// <summary>
            ///
            /// </summary>
            FAX,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            CRTERMS,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            SELLAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            HKDAMT,
            /// <summary>
            ///
            /// </summary>
            ANALYSISCODE,
            /// <summary>
            ///
            /// </summary>
            CUSTREF,
            /// <summary>
            ///
            /// </summary>
            CONTACTPERSON,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            RMK3,
            /// <summary>
            ///
            /// </summary>
            RMK4,
            /// <summary>
            ///
            /// </summary>
            RMK5,
            /// <summary>
            ///
            /// </summary>
            RMK6,
            /// <summary>
            ///
            /// </summary>
            JBASESSION,
            /// <summary>
            ///
            /// </summary>
            JBADATE,
            /// <summary>
            ///
            /// </summary>
            PAYMENT,
            /// <summary>
            ///
            /// </summary>
            INVTYPE,
            /// <summary>
            ///
            /// </summary>
            PAYDESC,
            /// <summary>
            ///
            /// </summary>
            INVFORMAT,
            /// <summary>
            ///
            /// </summary>
            PrintTeam,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VoidTeam,
            /// <summary>
            ///
            /// </summary>
            VoidREASON,
            /// <summary>
            ///
            /// </summary>
            VOID2JBA,
            /// <summary>
            ///
            /// </summary>
            ARPurgeRef,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            ATTN,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            CR_card_holder,
            /// <summary>
            ///
            /// </summary>
            CR_card_no,
            /// <summary>
            ///
            /// </summary>
            CR_card_type,
            /// <summary>
            ///
            /// </summary>
            CR_card_expiry,
            /// <summary>
            ///
            /// </summary>
            CR_amt,
            /// <summary>
            ///
            /// </summary>
            CR_curr,
            /// <summary>
            ///
            /// </summary>
            CR_approval_code,
            /// <summary>
            ///
            /// </summary>
            CR_ta,
            /// <summary>
            ///
            /// </summary>
            RECINV,
            /// <summary>
            ///
            /// </summary>
            PAYER,
            /// <summary>
            ///
            /// </summary>
            CHQ_ISSUEDATE,
            /// <summary>
            ///
            /// </summary>
            BANKCODE,
            /// <summary>
            ///
            /// </summary>
            PAY_RMK,
            /// <summary>
            ///
            /// </summary>
            ChqNo,
            /// <summary>
            ///
            /// </summary>
            gstpct,
            /// <summary>
            ///
            /// </summary>
            gstamt,
            /// <summary>
            ///
            /// </summary>
            SellExcludeTax,
            /// <summary>
            ///
            /// </summary>
            mstrinvnum,
            /// <summary>
            ///
            /// </summary>
            invdtcurr,
            /// <summary>
            ///
            /// </summary>
            invdtsum,
            /// <summary>
            ///
            /// </summary>
            CNReason,
            /// <summary>
            ///
            /// </summary>
            InvTC,
            /// <summary>
            ///
            /// </summary>
            DisCreditInfo,
            /// <summary>
            ///
            /// </summary>
            IsHidebreakdown,
            /// <summary>
            ///
            /// </summary>
            IsPackage,
            /// <summary>
            ///
            /// </summary>
            PackageDesc1,
            /// <summary>
            ///
            /// </summary>
            PackageDesc2,
            /// <summary>
            ///
            /// </summary>
            IsMICE,
            /// <summary>
            ///
            /// </summary>
            MICEDesc,
            /// <summary>
            ///
            /// </summary>
            IsBkgClass,
            /// <summary>
            ///
            /// </summary>
            IsSendEmailNow,
        }

        #region Model
        private String _companycode;
        private String _invnum;
        private String _bkgref;
        private String _pnr;
        private String _cltcode;
        private String _cltname;
        private String _cltaddr;
        private String _phone;
        private String _fax;
        private String _staffcode;
        private String _teamcode;
        private String _crterms;
        private String _sellcurr;
        private Decimal _sellamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _hkdamt;
        private String _analysiscode;
        private String _custref;
        private String _contactperson;
        private String _rmk1;
        private String _rmk2;
        private String _rmk3;
        private String _rmk4;
        private String _rmk5;
        private String _rmk6;
        private String _jbasession;
        private String _jbadate;
        private String _payment;
        private String _invtype;
        private String _paydesc;
        private String _invformat;
        private String _printteam;
        private DateTime _voidon;
        private String _voidby;
        private String _voidteam;
        private String _voidreason;
        private String _void2jba;
        private String _arpurgeref;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _attn;
        private String _cod1;
        private String _cod2;
        private String _cod3;
        private String _cr_card_holder;
        private String _cr_card_no;
        private String _cr_card_type;
        private DateTime _cr_card_expiry;
        private Decimal _cr_amt;
        private String _cr_curr;
        private String _cr_approval_code;
        private String _cr_ta;
        private String _recinv;
        private String _payer;
        private DateTime _chq_issuedate;
        private String _bankcode;
        private String _pay_rmk;
        private String _chqno;
        private Decimal _gstpct;
        private Decimal _gstamt;
        private Decimal _sellexcludetax;
        private String _mstrinvnum;
        private String _invdtcurr;
        private Decimal _invdtsum;
        private String _cnreason;
        private String _invtc;
        private String _discreditinfo;
        private String _ishidebreakdown;
        private String _ispackage;
        private String _packagedesc1;
        private String _packagedesc2;
        private String _ismice;
        private String _micedesc;
        private String _isbkgclass;
        private String _issendemailnow;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("INVNUM", DbType.String, 10)]
        public String INVNUM
        {
            set { AddAssigned("INVNUM"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("CLTCODE", DbType.String, 8)]
        public String CLTCODE
        {
            set { AddAssigned("CLTCODE"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("CLTNAME", DbType.String, 40)]
        public String CLTNAME
        {
            set { AddAssigned("CLTNAME"); _cltname = value; }
            get { return _cltname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(160)]
        /// </summary>
        [FieldMapping("CLTADDR", DbType.String, 160)]
        public String CLTADDR
        {
            set { AddAssigned("CLTADDR"); _cltaddr = value; }
            get { return _cltaddr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE", DbType.String, 20)]
        public String PHONE
        {
            set { AddAssigned("PHONE"); _phone = value; }
            get { return _phone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("FAX", DbType.String, 20)]
        public String FAX
        {
            set { AddAssigned("FAX"); _fax = value; }
            get { return _fax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.String, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CRTERMS", DbType.String, 50)]
        public String CRTERMS
        {
            set { AddAssigned("CRTERMS"); _crterms = value; }
            get { return _crterms; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SELLAMT", DbType.Decimal, 14)]
        public Decimal SELLAMT
        {
            set { AddAssigned("SELLAMT"); _sellamt = value; }
            get { return _sellamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("HKDAMT", DbType.Decimal, 14)]
        public Decimal HKDAMT
        {
            set { AddAssigned("HKDAMT"); _hkdamt = value; }
            get { return _hkdamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ANALYSISCODE", DbType.String, 20)]
        public String ANALYSISCODE
        {
            set { AddAssigned("ANALYSISCODE"); _analysiscode = value; }
            get { return _analysiscode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(47)]
        /// </summary>
        [FieldMapping("CUSTREF", DbType.String, 47)]
        public String CUSTREF
        {
            set { AddAssigned("CUSTREF"); _custref = value; }
            get { return _custref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("CONTACTPERSON", DbType.String, 40)]
        public String CONTACTPERSON
        {
            set { AddAssigned("CONTACTPERSON"); _contactperson = value; }
            get { return _contactperson; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 50)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 50)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK3", DbType.String, 50)]
        public String RMK3
        {
            set { AddAssigned("RMK3"); _rmk3 = value; }
            get { return _rmk3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK4", DbType.String, 50)]
        public String RMK4
        {
            set { AddAssigned("RMK4"); _rmk4 = value; }
            get { return _rmk4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK5", DbType.String, 50)]
        public String RMK5
        {
            set { AddAssigned("RMK5"); _rmk5 = value; }
            get { return _rmk5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK6", DbType.String, 50)]
        public String RMK6
        {
            set { AddAssigned("RMK6"); _rmk6 = value; }
            get { return _rmk6; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("JBASESSION", DbType.String, 8)]
        public String JBASESSION
        {
            set { AddAssigned("JBASESSION"); _jbasession = value; }
            get { return _jbasession; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("JBADATE", DbType.String, 10)]
        public String JBADATE
        {
            set { AddAssigned("JBADATE"); _jbadate = value; }
            get { return _jbadate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAYMENT", DbType.String, 3)]
        public String PAYMENT
        {
            set { AddAssigned("PAYMENT"); _payment = value; }
            get { return _payment; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("INVTYPE", DbType.String, 3)]
        public String INVTYPE
        {
            set { AddAssigned("INVTYPE"); _invtype = value; }
            get { return _invtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("PAYDESC", DbType.String, 30)]
        public String PAYDESC
        {
            set { AddAssigned("PAYDESC"); _paydesc = value; }
            get { return _paydesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("INVFORMAT", DbType.String, 1)]
        public String INVFORMAT
        {
            set { AddAssigned("INVFORMAT"); _invformat = value; }
            get { return _invformat; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PrintTeam", DbType.String, 5)]
        public String PrintTeam
        {
            set { AddAssigned("PrintTeam"); _printteam = value; }
            get { return _printteam; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("VoidTeam", DbType.String, 5)]
        public String VoidTeam
        {
            set { AddAssigned("VoidTeam"); _voidteam = value; }
            get { return _voidteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VoidREASON", DbType.String, 60)]
        public String VoidREASON
        {
            set { AddAssigned("VoidREASON"); _voidreason = value; }
            get { return _voidreason; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOID2JBA", DbType.String, 10)]
        public String VOID2JBA
        {
            set { AddAssigned("VOID2JBA"); _void2jba = value; }
            get { return _void2jba; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ARPurgeRef", DbType.String, 10)]
        public String ARPurgeRef
        {
            set { AddAssigned("ARPurgeRef"); _arpurgeref = value; }
            get { return _arpurgeref; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("ATTN", DbType.String, 60)]
        public String ATTN
        {
            set { AddAssigned("ATTN"); _attn = value; }
            get { return _attn; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 20)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 20)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD3", DbType.String, 20)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("CR_card_holder", DbType.String, 100)]
        public String CR_card_holder
        {
            set { AddAssigned("CR_card_holder"); _cr_card_holder = value; }
            get { return _cr_card_holder; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("CR_card_no", DbType.String, 30)]
        public String CR_card_no
        {
            set { AddAssigned("CR_card_no"); _cr_card_no = value; }
            get { return _cr_card_no; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CR_card_type", DbType.String, 10)]
        public String CR_card_type
        {
            set { AddAssigned("CR_card_type"); _cr_card_type = value; }
            get { return _cr_card_type; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CR_card_expiry", DbType.DateTime, 8)]
        public DateTime CR_card_expiry
        {
            set { AddAssigned("CR_card_expiry"); _cr_card_expiry = value; }
            get { return _cr_card_expiry; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("CR_amt", DbType.Decimal, 16)]
        public Decimal CR_amt
        {
            set { AddAssigned("CR_amt"); _cr_amt = value; }
            get { return _cr_amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CR_curr", DbType.String, 3)]
        public String CR_curr
        {
            set { AddAssigned("CR_curr"); _cr_curr = value; }
            get { return _cr_curr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CR_approval_code", DbType.String, 10)]
        public String CR_approval_code
        {
            set { AddAssigned("CR_approval_code"); _cr_approval_code = value; }
            get { return _cr_approval_code; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("CR_ta", DbType.String, 15)]
        public String CR_ta
        {
            set { AddAssigned("CR_ta"); _cr_ta = value; }
            get { return _cr_ta; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("RECINV", DbType.StringFixedLength, 3)]
        public String RECINV
        {
            set { AddAssigned("RECINV"); _recinv = value; }
            get { return _recinv; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("PAYER", DbType.String, 50)]
        public String PAYER
        {
            set { AddAssigned("PAYER"); _payer = value; }
            get { return _payer; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CHQ_ISSUEDATE", DbType.DateTime, 8)]
        public DateTime CHQ_ISSUEDATE
        {
            set { AddAssigned("CHQ_ISSUEDATE"); _chq_issuedate = value; }
            get { return _chq_issuedate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BANKCODE", DbType.String, 50)]
        public String BANKCODE
        {
            set { AddAssigned("BANKCODE"); _bankcode = value; }
            get { return _bankcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PAY_RMK", DbType.String, 100)]
        public String PAY_RMK
        {
            set { AddAssigned("PAY_RMK"); _pay_rmk = value; }
            get { return _pay_rmk; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ChqNo", DbType.String, 10)]
        public String ChqNo
        {
            set { AddAssigned("ChqNo"); _chqno = value; }
            get { return _chqno; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)/Default:(0)]
        /// </summary>
        [FieldMapping("gstpct", DbType.Decimal, 16)]
        public Decimal gstpct
        {
            set { AddAssigned("gstpct"); _gstpct = value; }
            get { return _gstpct; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)/Default:(0)]
        /// </summary>
        [FieldMapping("gstamt", DbType.Decimal, 16)]
        public Decimal gstamt
        {
            set { AddAssigned("gstamt"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("SellExcludeTax", DbType.Decimal, 16)]
        public Decimal SellExcludeTax
        {
            set { AddAssigned("SellExcludeTax"); _sellexcludetax = value; }
            get { return _sellexcludetax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("mstrinvnum", DbType.String, 10)]
        public String mstrinvnum
        {
            set { AddAssigned("mstrinvnum"); _mstrinvnum = value; }
            get { return _mstrinvnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("invdtcurr", DbType.String, 3)]
        public String invdtcurr
        {
            set { AddAssigned("invdtcurr"); _invdtcurr = value; }
            get { return _invdtcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("invdtsum", DbType.Decimal, 16)]
        public Decimal invdtsum
        {
            set { AddAssigned("invdtsum"); _invdtsum = value; }
            get { return _invdtsum; }
        }
        /// <summary>
        /// [Allow Null/char(60)]
        /// </summary>
        [FieldMapping("CNReason", DbType.AnsiStringFixedLength, 60)]
        public String CNReason
        {
            set { AddAssigned("CNReason"); _cnreason = value; }
            get { return _cnreason; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2000)]
        /// </summary>
        [FieldMapping("InvTC", DbType.String, 2000)]
        public String InvTC
        {
            set { AddAssigned("InvTC"); _invtc = value; }
            get { return _invtc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("DisCreditInfo", DbType.String, 1)]
        public String DisCreditInfo
        {
            set { AddAssigned("DisCreditInfo"); _discreditinfo = value; }
            get { return _discreditinfo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("IsHidebreakdown", DbType.String, 1)]
        public String IsHidebreakdown
        {
            set { AddAssigned("IsHidebreakdown"); _ishidebreakdown = value; }
            get { return _ishidebreakdown; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("IsPackage", DbType.AnsiString, 1)]
        public String IsPackage
        {
            set { AddAssigned("IsPackage"); _ispackage = value; }
            get { return _ispackage; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PackageDesc1", DbType.String, 100)]
        public String PackageDesc1
        {
            set { AddAssigned("PackageDesc1"); _packagedesc1 = value; }
            get { return _packagedesc1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PackageDesc2", DbType.String, 100)]
        public String PackageDesc2
        {
            set { AddAssigned("PackageDesc2"); _packagedesc2 = value; }
            get { return _packagedesc2; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IsMICE", DbType.AnsiStringFixedLength, 1)]
        public String IsMICE
        {
            set { AddAssigned("IsMICE"); _ismice = value; }
            get { return _ismice; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("MICEDesc", DbType.String, 100)]
        public String MICEDesc
        {
            set { AddAssigned("MICEDesc"); _micedesc = value; }
            get { return _micedesc; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IsBkgClass", DbType.AnsiStringFixedLength, 1)]
        public String IsBkgClass
        {
            set { AddAssigned("IsBkgClass"); _isbkgclass = value; }
            get { return _isbkgclass; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IsSendEmailNow", DbType.AnsiStringFixedLength, 1)]
        public String IsSendEmailNow
        {
            set { AddAssigned("IsSendEmailNow"); _issendemailnow = value; }
            get { return _issendemailnow; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOINVs : BaseList<tbPEOINV, tbPEOINVs> { }
    public class tbPEOINVPage : PageResult<tbPEOINV, tbPEOINVs> { }
}

