﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{

    public partial class tbPEOTKT : BaseTable<tbPEOTKT>
    {
        public tbPEOTKT(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _companycode = string.Empty;
                _ticket = string.Empty;
                _tktseq = string.Empty;
                _bkgref = string.Empty;
                _pnr = string.Empty;
                _cltcode = string.Empty;
                _suppcode = string.Empty;
                _invnum = string.Empty;
                _jqty = 0;
                _aircode = string.Empty;
                _airline = string.Empty;
                _paxname = string.Empty;
                _cod1 = string.Empty;
                _cod2 = string.Empty;
                _cod3 = string.Empty;
                _cod7 = string.Empty;
                _commission = 0;
                _localfarecurr = string.Empty;
                _localfareamt = 0;
                _foreignfarecurr = string.Empty;
                _foreignfareamt = 0;
                _discount = 0;
                _formpay = string.Empty;
                _routing = string.Empty;
                _fullcurr = string.Empty;
                _fullfare = 0;
                _paidcurr = string.Empty;
                _paidfare = 0;
                _lowcurr = string.Empty;
                _lowfare = 0;
                _netcurr = string.Empty;
                _netfare = 0;
                _farebasis = string.Empty;
                _faretype = string.Empty;
                _totaltax = 0;
                _security = 0;
                _qsurcharge = 0;
                _branchcode = string.Empty;
                _teamcode = string.Empty;
                _issueby = string.Empty;
                _createby = string.Empty;
                _updateby = string.Empty;
                _voidby = string.Empty;
                _voidteam = string.Empty;
                _voidreason = string.Empty;
                _taxamount1 = string.Empty;
                _taxid1 = string.Empty;
                _taxamount2 = string.Empty;
                _taxid2 = string.Empty;
                _taxamount3 = string.Empty;
                _taxid3 = string.Empty;
                _farecaltype = string.Empty;
                _compressprintind = string.Empty;
                _farecaldata = string.Empty;
                _pseudo = string.Empty;
                _sinein = string.Empty;
                _nst = string.Empty;
                _sourcesystem = string.Empty;
                _commission_per = 0;
                _add_payment = string.Empty;
                _iss_off_code = string.Empty;
                _tour_code = string.Empty;
                _org_iss = string.Empty;
                _iss_in_exchange = string.Empty;
                _controlnum = string.Empty;
                _endorsements = string.Empty;
                _airreason = string.Empty;
                _destination = string.Empty;
                _intdom = string.Empty;
                _paxair = string.Empty;
                _class = string.Empty;
                _tkttype = string.Empty;
                _sellcurr = string.Empty;
                _sellfare = 0;
                _corpcurr = string.Empty;
                _corpfare = 0;
                _cod4 = string.Empty;
                _cod5 = string.Empty;
                _cod6 = string.Empty;

            }
        }

    }

}

