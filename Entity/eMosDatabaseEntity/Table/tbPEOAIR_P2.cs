﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    public partial class tbPEOAIR : BaseTable<tbPEOAIR>
    {
        public tbPEOAIR(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _companycode = string.Empty;
                _bkgref = string.Empty;
                _segnum = string.Empty;
                _pnr = string.Empty;
                _flight = string.Empty;
                _class = string.Empty;
                _departcity = string.Empty;
                _arrivalcity = string.Empty;
                _status = string.Empty;
                _numofpax = string.Empty;
                _departtime = string.Empty;
                _arrtime = string.Empty;
                _elapsedtime = string.Empty;
                _ssr = string.Empty;
                _seat = string.Empty;
                _reasoncode = string.Empty;
                _stopovernum = 0;
                _stopovercity = string.Empty;
                _pcode = string.Empty;
                _specialrq = string.Empty;
                _rmk1 = string.Empty;
                _rmk2 = string.Empty;
                _invrmk1 = string.Empty;
                _invrmk2 = string.Empty;
                _vchrmk1 = string.Empty;
                _vchrmk2 = string.Empty;
                _sellcurr = string.Empty;
                _totalsell = 0;
                _totalstax = 0;
                _costcurr = string.Empty;
                _totalcost = 0;
                _totalctax = 0;
                _sourcesystem = string.Empty;
                _createby = string.Empty;
                _updateby = string.Empty;
                _voidby = string.Empty;
                _departterminal = string.Empty;
                _arrivalterminal = string.Empty;
                _otherairlinepnr = string.Empty;
                _eqp = string.Empty;
                _service = string.Empty;
                _suppcode = string.Empty;
            }
        }
    }
}
