﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:shortform_prod_AirClassType
    /// </summary>
    [Serializable]
    public class tbshortform_prod_AirClassType : BaseTable<tbshortform_prod_AirClassType>
    {
        public tbshortform_prod_AirClassType()
            : base(DBTableName)
        { }
        public const string DBTableName = "shortform_prod_AirClassType";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            ClassType,
            /// <summary>
            ///
            /// </summary>
            ClassName,
        }

        #region Model
        private String _classtype;
        private String _classname;
        /// <summary>
        /// [Un-Null/varchar(1)]
        /// </summary>
        [FieldMapping("ClassType", DbType.AnsiString, 1)]
        public String ClassType
        {
            set { AddAssigned("ClassType"); _classtype = value; }
            get { return _classtype; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ClassName", DbType.String, 100)]
        public String ClassName
        {
            set { AddAssigned("ClassName"); _classname = value; }
            get { return _classname; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbshortform_prod_AirClassTypes : BaseList<tbshortform_prod_AirClassType, tbshortform_prod_AirClassTypes> { }
    public class tbshortform_prod_AirClassTypePage : PageResult<tbshortform_prod_AirClassType, tbshortform_prod_AirClassTypes> { }
}

