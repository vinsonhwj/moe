﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOVCH
    /// </summary>
    [Serializable]
    public class tbPEOVCH : BaseTable<tbPEOVCH>
    {
        public tbPEOVCH()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOVCH";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            VCHNUM,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            WESTELNO,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            VCHTYPE,
            /// <summary>
            ///
            /// </summary>
            SUPPCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPNAME,
            /// <summary>
            ///
            /// </summary>
            SUPPADDR,
            /// <summary>
            ///
            /// </summary>
            PHONE,
            /// <summary>
            ///
            /// </summary>
            FAX,
            /// <summary>
            ///
            /// </summary>
            HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME,
            /// <summary>
            ///
            /// </summary>
            HOTELADDR,
            /// <summary>
            ///
            /// </summary>
            HOTELTEL,
            /// <summary>
            ///
            /// </summary>
            HOTELFAX,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            ARRDESC,
            /// <summary>
            ///
            /// </summary>
            DEPDESC,
            /// <summary>
            ///
            /// </summary>
            REMARK1,
            /// <summary>
            ///
            /// </summary>
            REMARK2,
            /// <summary>
            ///
            /// </summary>
            CFMBY,
            /// <summary>
            ///
            /// </summary>
            FORMPAY,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            COSTAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            HKDAMT,
            /// <summary>
            ///
            /// </summary>
            VCHREF,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            REASONCODE,
            /// <summary>
            ///
            /// </summary>
            NUMCOPY,
            /// <summary>
            ///
            /// </summary>
            RATETYPE,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ADULTNUM,
            /// <summary>
            ///
            /// </summary>
            CHILDNUM,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            RMK3,
            /// <summary>
            ///
            /// </summary>
            RMK4,
            /// <summary>
            ///
            /// </summary>
            RMK5,
            /// <summary>
            ///
            /// </summary>
            RMK6,
            /// <summary>
            ///
            /// </summary>
            PrintTeam,
            /// <summary>
            ///
            /// </summary>
            SPCL_REQUEST,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VoidTeam,
            /// <summary>
            ///
            /// </summary>
            VoidREASON,
            /// <summary>
            ///
            /// </summary>
            VOID2JBA,
            /// <summary>
            ///
            /// </summary>
            ARPurgeRef,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            gstpct,
            /// <summary>
            ///
            /// </summary>
            gstamt,
            /// <summary>
            ///
            /// </summary>
            staffname,
            /// <summary>
            ///
            /// </summary>
            stafftel,
            /// <summary>
            ///
            /// </summary>
            vchremark,
            /// <summary>
            ///
            /// </summary>
            hotelref,
            /// <summary>
            ///
            /// </summary>
            CostExcludeTax,
            /// <summary>
            ///
            /// </summary>
            Attn,
            /// <summary>
            ///
            /// </summary>
            ARRDTL,
            /// <summary>
            ///
            /// </summary>
            DEPDTL,
            /// <summary>
            ///
            /// </summary>
            OurRef,
            /// <summary>
            ///
            /// </summary>
            Payment,
        }

        #region Model
        private String _companycode;
        private String _vchnum;
        private String _bkgref;
        private String _pnr;
        private String _westelno;
        private String _staffcode;
        private String _teamcode;
        private String _vchtype;
        private String _suppcode;
        private String _suppname;
        private String _suppaddr;
        private String _phone;
        private String _fax;
        private String _hotelcode;
        private String _hotelname;
        private String _hoteladdr;
        private String _hoteltel;
        private String _hotelfax;
        private DateTime _arrdate;
        private DateTime _departdate;
        private String _arrdesc;
        private String _depdesc;
        private String _remark1;
        private String _remark2;
        private String _cfmby;
        private String _formpay;
        private String _costcurr;
        private Decimal _costamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _hkdamt;
        private String _vchref;
        private String _cod1;
        private String _cod2;
        private String _reasoncode;
        private Decimal _numcopy;
        private String _ratetype;
        private String _ratename;
        private Decimal _adultnum;
        private Decimal _childnum;
        private String _rmk1;
        private String _rmk2;
        private String _rmk3;
        private String _rmk4;
        private String _rmk5;
        private String _rmk6;
        private String _printteam;
        private String _spcl_request;
        private DateTime _voidon;
        private String _voidby;
        private String _voidteam;
        private String _voidreason;
        private String _void2jba;
        private String _arpurgeref;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Decimal _gstpct;
        private Decimal _gstamt;
        private String _staffname;
        private String _stafftel;
        private String _vchremark;
        private String _hotelref;
        private Decimal _costexcludetax;
        private String _attn;
        private String _arrdtl;
        private String _depdtl;
        private String _ourref;
        private String _payment;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VCHNUM", DbType.String, 10)]
        public String VCHNUM
        {
            set { AddAssigned("VCHNUM"); _vchnum = value; }
            get { return _vchnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("WESTELNO", DbType.String, 10)]
        public String WESTELNO
        {
            set { AddAssigned("WESTELNO"); _westelno = value; }
            get { return _westelno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.String, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(7)]
        /// </summary>
        [FieldMapping("VCHTYPE", DbType.String, 7)]
        public String VCHTYPE
        {
            set { AddAssigned("VCHTYPE"); _vchtype = value; }
            get { return _vchtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SUPPCODE", DbType.String, 8)]
        public String SUPPCODE
        {
            set { AddAssigned("SUPPCODE"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("SUPPNAME", DbType.String, 70)]
        public String SUPPNAME
        {
            set { AddAssigned("SUPPNAME"); _suppname = value; }
            get { return _suppname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(120)]
        /// </summary>
        [FieldMapping("SUPPADDR", DbType.String, 120)]
        public String SUPPADDR
        {
            set { AddAssigned("SUPPADDR"); _suppaddr = value; }
            get { return _suppaddr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE", DbType.String, 20)]
        public String PHONE
        {
            set { AddAssigned("PHONE"); _phone = value; }
            get { return _phone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("FAX", DbType.String, 20)]
        public String FAX
        {
            set { AddAssigned("FAX"); _fax = value; }
            get { return _fax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("HOTELCODE", DbType.String, 8)]
        public String HOTELCODE
        {
            set { AddAssigned("HOTELCODE"); _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("HOTELNAME", DbType.String, 100)]
        public String HOTELNAME
        {
            set { AddAssigned("HOTELNAME"); _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1000)]
        /// </summary>
        [FieldMapping("HOTELADDR", DbType.String, 1000)]
        public String HOTELADDR
        {
            set { AddAssigned("HOTELADDR"); _hoteladdr = value; }
            get { return _hoteladdr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("HOTELTEL", DbType.String, 20)]
        public String HOTELTEL
        {
            set { AddAssigned("HOTELTEL"); _hoteltel = value; }
            get { return _hoteltel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("HOTELFAX", DbType.String, 20)]
        public String HOTELFAX
        {
            set { AddAssigned("HOTELFAX"); _hotelfax = value; }
            get { return _hotelfax; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { AddAssigned("ARRDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ARRDESC", DbType.String, 20)]
        public String ARRDESC
        {
            set { AddAssigned("ARRDESC"); _arrdesc = value; }
            get { return _arrdesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("DEPDESC", DbType.String, 20)]
        public String DEPDESC
        {
            set { AddAssigned("DEPDESC"); _depdesc = value; }
            get { return _depdesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("REMARK1", DbType.String, 20)]
        public String REMARK1
        {
            set { AddAssigned("REMARK1"); _remark1 = value; }
            get { return _remark1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("REMARK2", DbType.String, 20)]
        public String REMARK2
        {
            set { AddAssigned("REMARK2"); _remark2 = value; }
            get { return _remark2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("CFMBY", DbType.String, 12)]
        public String CFMBY
        {
            set { AddAssigned("CFMBY"); _cfmby = value; }
            get { return _cfmby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FORMPAY", DbType.String, 50)]
        public String FORMPAY
        {
            set { AddAssigned("FORMPAY"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("COSTAMT", DbType.Decimal, 14)]
        public Decimal COSTAMT
        {
            set { AddAssigned("COSTAMT"); _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("HKDAMT", DbType.Decimal, 14)]
        public Decimal HKDAMT
        {
            set { AddAssigned("HKDAMT"); _hkdamt = value; }
            get { return _hkdamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("VCHREF", DbType.String, 80)]
        public String VCHREF
        {
            set { AddAssigned("VCHREF"); _vchref = value; }
            get { return _vchref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 20)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 20)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("REASONCODE", DbType.String, 2)]
        public String REASONCODE
        {
            set { AddAssigned("REASONCODE"); _reasoncode = value; }
            get { return _reasoncode; }
        }
        /// <summary>
        /// [Allow Null/decimal(2,0)]
        /// </summary>
        [FieldMapping("NUMCOPY", DbType.Decimal, 2)]
        public Decimal NUMCOPY
        {
            set { AddAssigned("NUMCOPY"); _numcopy = value; }
            get { return _numcopy; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("RATETYPE", DbType.String, 1)]
        public String RATETYPE
        {
            set { AddAssigned("RATETYPE"); _ratetype = value; }
            get { return _ratetype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { AddAssigned("RATENAME"); _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("ADULTNUM", DbType.Decimal, 3)]
        public Decimal ADULTNUM
        {
            set { AddAssigned("ADULTNUM"); _adultnum = value; }
            get { return _adultnum; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("CHILDNUM", DbType.Decimal, 3)]
        public Decimal CHILDNUM
        {
            set { AddAssigned("CHILDNUM"); _childnum = value; }
            get { return _childnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 20)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 20)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK3", DbType.String, 20)]
        public String RMK3
        {
            set { AddAssigned("RMK3"); _rmk3 = value; }
            get { return _rmk3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK4", DbType.String, 20)]
        public String RMK4
        {
            set { AddAssigned("RMK4"); _rmk4 = value; }
            get { return _rmk4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK5", DbType.String, 20)]
        public String RMK5
        {
            set { AddAssigned("RMK5"); _rmk5 = value; }
            get { return _rmk5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK6", DbType.String, 20)]
        public String RMK6
        {
            set { AddAssigned("RMK6"); _rmk6 = value; }
            get { return _rmk6; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PrintTeam", DbType.String, 5)]
        public String PrintTeam
        {
            set { AddAssigned("PrintTeam"); _printteam = value; }
            get { return _printteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("SPCL_REQUEST", DbType.String, 255)]
        public String SPCL_REQUEST
        {
            set { AddAssigned("SPCL_REQUEST"); _spcl_request = value; }
            get { return _spcl_request; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("VoidTeam", DbType.String, 5)]
        public String VoidTeam
        {
            set { AddAssigned("VoidTeam"); _voidteam = value; }
            get { return _voidteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VoidREASON", DbType.String, 60)]
        public String VoidREASON
        {
            set { AddAssigned("VoidREASON"); _voidreason = value; }
            get { return _voidreason; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOID2JBA", DbType.String, 10)]
        public String VOID2JBA
        {
            set { AddAssigned("VOID2JBA"); _void2jba = value; }
            get { return _void2jba; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("ARPurgeRef", DbType.String, 10)]
        public String ARPurgeRef
        {
            set { AddAssigned("ARPurgeRef"); _arpurgeref = value; }
            get { return _arpurgeref; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gstpct", DbType.Decimal, 14)]
        public Decimal gstpct
        {
            set { AddAssigned("gstpct"); _gstpct = value; }
            get { return _gstpct; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gstamt", DbType.Decimal, 14)]
        public Decimal gstamt
        {
            set { AddAssigned("gstamt"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("staffname", DbType.String, 100)]
        public String staffname
        {
            set { AddAssigned("staffname"); _staffname = value; }
            get { return _staffname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("stafftel", DbType.String, 50)]
        public String stafftel
        {
            set { AddAssigned("stafftel"); _stafftel = value; }
            get { return _stafftel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("vchremark", DbType.String, 200)]
        public String vchremark
        {
            set { AddAssigned("vchremark"); _vchremark = value; }
            get { return _vchremark; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("hotelref", DbType.String, 200)]
        public String hotelref
        {
            set { AddAssigned("hotelref"); _hotelref = value; }
            get { return _hotelref; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("CostExcludeTax", DbType.Decimal, 16)]
        public Decimal CostExcludeTax
        {
            set { AddAssigned("CostExcludeTax"); _costexcludetax = value; }
            get { return _costexcludetax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("Attn", DbType.String, 40)]
        public String Attn
        {
            set { AddAssigned("Attn"); _attn = value; }
            get { return _attn; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)/Default:(null)]
        /// </summary>
        [FieldMapping("ARRDTL", DbType.String, 30)]
        public String ARRDTL
        {
            set { AddAssigned("ARRDTL"); _arrdtl = value; }
            get { return _arrdtl; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)/Default:(null)]
        /// </summary>
        [FieldMapping("DEPDTL", DbType.String, 30)]
        public String DEPDTL
        {
            set { AddAssigned("DEPDTL"); _depdtl = value; }
            get { return _depdtl; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("OurRef", DbType.String, 50)]
        public String OurRef
        {
            set { AddAssigned("OurRef"); _ourref = value; }
            get { return _ourref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("Payment", DbType.String, 100)]
        public String Payment
        {
            set { AddAssigned("Payment"); _payment = value; }
            get { return _payment; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOVCHs : BaseList<tbPEOVCH, tbPEOVCHs> { }
    public class tbPEOVCHPage : PageResult<tbPEOVCH, tbPEOVCHs> { }
}

