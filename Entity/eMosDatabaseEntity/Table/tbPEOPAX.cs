﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOPAX
    /// </summary>
    [Serializable]
    public partial class tbPEOPAX : BaseTable<tbPEOPAX>
    {
        public tbPEOPAX()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOPAX";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            PAXNUM,
            /// <summary>
            ///
            /// </summary>
            SourcePnr,
            /// <summary>
            ///
            /// </summary>
            PAXLNAME,
            /// <summary>
            ///
            /// </summary>
            PAXFNAME,
            /// <summary>
            ///
            /// </summary>
            PAXTITLE,
            /// <summary>
            ///
            /// </summary>
            PAXTYPE,
            /// <summary>
            ///
            /// </summary>
            PAXAIR,
            /// <summary>
            ///
            /// </summary>
            PAXHOTEL,
            /// <summary>
            ///
            /// </summary>
            PAXOTHER,
            /// <summary>
            ///
            /// </summary>
            PAXAGE,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            Nationality,
            /// <summary>
            ///
            /// </summary>
            PassportNo,
            /// <summary>
            ///
            /// </summary>
            PassportExpireDate,
            /// <summary>
            ///
            /// </summary>
            DateofBirth,
            /// <summary>
            ///
            /// </summary>
            Visa,
            /// <summary>
            ///
            /// </summary>
            ContactHomeTel,
            /// <summary>
            ///
            /// </summary>
            ContactMobileTel,
            /// <summary>
            ///
            /// </summary>
            ContactOfficeTel,
            /// <summary>
            ///
            /// </summary>
            Email,
            /// <summary>
            ///
            /// </summary>
            Address1,
            /// <summary>
            ///
            /// </summary>
            Address2,
            /// <summary>
            ///
            /// </summary>
            Address3,
            /// <summary>
            ///
            /// </summary>
            Address4,
            /// <summary>
            ///
            /// </summary>
            Source,
            /// <summary>
            ///
            /// </summary>
            SourceRef,
            /// <summary>
            ///
            /// </summary>
            MemberShipNo,
            /// <summary>
            ///
            /// </summary>
            deleted,
            /// <summary>
            ///
            /// </summary>
            evisa_import,
            /// <summary>
            ///
            /// </summary>
            evisa_import_datetime,
            /// <summary>
            ///
            /// </summary>
            SOURCETYPE,
            /// <summary>
            ///
            /// </summary>
            ADCOMTXNO,
            /// <summary>
            ///
            /// </summary>
            PAXNAME1,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _paxnum;
        private String _sourcepnr;
        private String _paxlname;
        private String _paxfname;
        private String _paxtitle;
        private String _paxtype;
        private String _paxair;
        private String _paxhotel;
        private String _paxother;
        private Int16 _paxage;
        private String _rmk1;
        private String _rmk2;
        private String _voidon;
        private DateTime _voidby;
        private String _sourcesystem;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _nationality;
        private String _passportno;
        private DateTime _passportexpiredate;
        private DateTime _dateofbirth;
        private String _visa;
        private String _contacthometel;
        private String _contactmobiletel;
        private String _contactofficetel;
        private String _email;
        private String _address1;
        private String _address2;
        private String _address3;
        private String _address4;
        private String _source;
        private String _sourceref;
        private String _membershipno;
        private String _deleted;
        private String _evisa_import;
        private DateTime _evisa_import_datetime;
        private String _sourcetype;
        private String _adcomtxno;
        private String _paxname1;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("PAXNUM", DbType.String, 5)]
        public String PAXNUM
        {
            set { AddAssigned("PAXNUM"); _paxnum = value; }
            get { return _paxnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SourcePnr", DbType.String, 10)]
        public String SourcePnr
        {
            set { AddAssigned("SourcePnr"); _sourcepnr = value; }
            get { return _sourcepnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PAXLNAME", DbType.String, 20)]
        public String PAXLNAME
        {
            set { AddAssigned("PAXLNAME"); _paxlname = value; }
            get { return _paxlname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("PAXFNAME", DbType.String, 40)]
        public String PAXFNAME
        {
            set { AddAssigned("PAXFNAME"); _paxfname = value; }
            get { return _paxfname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("PAXTITLE", DbType.String, 4)]
        public String PAXTITLE
        {
            set { AddAssigned("PAXTITLE"); _paxtitle = value; }
            get { return _paxtitle; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXTYPE", DbType.String, 3)]
        public String PAXTYPE
        {
            set { AddAssigned("PAXTYPE"); _paxtype = value; }
            get { return _paxtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXAIR", DbType.String, 3)]
        public String PAXAIR
        {
            set { AddAssigned("PAXAIR"); _paxair = value; }
            get { return _paxair; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXHOTEL", DbType.String, 3)]
        public String PAXHOTEL
        {
            set { AddAssigned("PAXHOTEL"); _paxhotel = value; }
            get { return _paxhotel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXOTHER", DbType.String, 3)]
        public String PAXOTHER
        {
            set { AddAssigned("PAXOTHER"); _paxother = value; }
            get { return _paxother; }
        }
        /// <summary>
        /// [Allow Null/smallint(5)]
        /// </summary>
        [FieldMapping("PAXAGE", DbType.Int16, 5)]
        public Int16 PAXAGE
        {
            set { AddAssigned("PAXAGE"); _paxage = value; }
            get { return _paxage; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 20)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 20)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.StringFixedLength, 10)]
        public String VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.DateTime, 8)]
        public DateTime VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("Nationality", DbType.String, 30)]
        public String Nationality
        {
            set { AddAssigned("Nationality"); _nationality = value; }
            get { return _nationality; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("PassportNo", DbType.String, 30)]
        public String PassportNo
        {
            set { AddAssigned("PassportNo"); _passportno = value; }
            get { return _passportno; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("PassportExpireDate", DbType.DateTime, 8)]
        public DateTime PassportExpireDate
        {
            set { AddAssigned("PassportExpireDate"); _passportexpiredate = value; }
            get { return _passportexpiredate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DateofBirth", DbType.DateTime, 8)]
        public DateTime DateofBirth
        {
            set { AddAssigned("DateofBirth"); _dateofbirth = value; }
            get { return _dateofbirth; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("Visa", DbType.String, 1)]
        public String Visa
        {
            set { AddAssigned("Visa"); _visa = value; }
            get { return _visa; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ContactHomeTel", DbType.String, 20)]
        public String ContactHomeTel
        {
            set { AddAssigned("ContactHomeTel"); _contacthometel = value; }
            get { return _contacthometel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ContactMobileTel", DbType.String, 20)]
        public String ContactMobileTel
        {
            set { AddAssigned("ContactMobileTel"); _contactmobiletel = value; }
            get { return _contactmobiletel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ContactOfficeTel", DbType.String, 20)]
        public String ContactOfficeTel
        {
            set { AddAssigned("ContactOfficeTel"); _contactofficetel = value; }
            get { return _contactofficetel; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("Email", DbType.String, 70)]
        public String Email
        {
            set { AddAssigned("Email"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Address1", DbType.String, 50)]
        public String Address1
        {
            set { AddAssigned("Address1"); _address1 = value; }
            get { return _address1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Address2", DbType.String, 50)]
        public String Address2
        {
            set { AddAssigned("Address2"); _address2 = value; }
            get { return _address2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Address3", DbType.String, 50)]
        public String Address3
        {
            set { AddAssigned("Address3"); _address3 = value; }
            get { return _address3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("Address4", DbType.String, 50)]
        public String Address4
        {
            set { AddAssigned("Address4"); _address4 = value; }
            get { return _address4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("Source", DbType.String, 30)]
        public String Source
        {
            set { AddAssigned("Source"); _source = value; }
            get { return _source; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("SourceRef", DbType.String, 12)]
        public String SourceRef
        {
            set { AddAssigned("SourceRef"); _sourceref = value; }
            get { return _sourceref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(16)]
        /// </summary>
        [FieldMapping("MemberShipNo", DbType.String, 16)]
        public String MemberShipNo
        {
            set { AddAssigned("MemberShipNo"); _membershipno = value; }
            get { return _membershipno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)/Default:(0)]
        /// </summary>
        [FieldMapping("deleted", DbType.String, 1)]
        public String deleted
        {
            set { AddAssigned("deleted"); _deleted = value; }
            get { return _deleted; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("evisa_import", DbType.String, 1)]
        public String evisa_import
        {
            set { AddAssigned("evisa_import"); _evisa_import = value; }
            get { return _evisa_import; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("evisa_import_datetime", DbType.DateTime, 8)]
        public DateTime evisa_import_datetime
        {
            set { AddAssigned("evisa_import_datetime"); _evisa_import_datetime = value; }
            get { return _evisa_import_datetime; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("SOURCETYPE", DbType.AnsiString, 1)]
        public String SOURCETYPE
        {
            set { AddAssigned("SOURCETYPE"); _sourcetype = value; }
            get { return _sourcetype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("ADCOMTXNO", DbType.String, 12)]
        public String ADCOMTXNO
        {
            set { AddAssigned("ADCOMTXNO"); _adcomtxno = value; }
            get { return _adcomtxno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)/Default:('')]
        /// </summary>
        [FieldMapping("PAXNAME1", DbType.String, 50)]
        public String PAXNAME1
        {
            set { AddAssigned("PAXNAME1"); _paxname1 = value; }
            get { return _paxname1; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOPAXs : BaseList<tbPEOPAX, tbPEOPAXs> { }
    public class tbPEOPAXPage : PageResult<tbPEOPAX, tbPEOPAXs> { }
}

