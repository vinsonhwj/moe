﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:Customer
    /// </summary>
    [Serializable]
    public class tbCustomer : BaseTable<tbCustomer>
    {
        public tbCustomer()
            : base(DBTableName)
        { }
        public const string DBTableName = "Customer";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            CLTCODE,
            /// <summary>
            ///
            /// </summary>
            CLTNAME,
            /// <summary>
            ///
            /// </summary>
            BRANCHCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            MSTRCLT,
            /// <summary>
            ///
            /// </summary>
            ADDR1,
            /// <summary>
            ///
            /// </summary>
            ADDR2,
            /// <summary>
            ///
            /// </summary>
            ADDR3,
            /// <summary>
            ///
            /// </summary>
            ADDR4,
            /// <summary>
            ///
            /// </summary>
            ADDR5,
            /// <summary>
            ///
            /// </summary>
            POSTAL,
            /// <summary>
            ///
            /// </summary>
            CITY,
            /// <summary>
            ///
            /// </summary>
            COUNTRY,
            /// <summary>
            ///
            /// </summary>
            BILLADDR1,
            /// <summary>
            ///
            /// </summary>
            BILLADDR2,
            /// <summary>
            ///
            /// </summary>
            BILLADDR3,
            /// <summary>
            ///
            /// </summary>
            BILLADDR4,
            /// <summary>
            ///
            /// </summary>
            BILLADDR5,
            /// <summary>
            ///
            /// </summary>
            BILLPOSTAL,
            /// <summary>
            ///
            /// </summary>
            BILLCITY,
            /// <summary>
            ///
            /// </summary>
            BILLCOUNTRY,
            /// <summary>
            ///
            /// </summary>
            STATUS,
            /// <summary>
            ///
            /// </summary>
            CONTACT1,
            /// <summary>
            ///
            /// </summary>
            CONTACT2,
            /// <summary>
            ///
            /// </summary>
            JOBTITLE1,
            /// <summary>
            ///
            /// </summary>
            JOBTITLE2,
            /// <summary>
            ///
            /// </summary>
            PHONE1,
            /// <summary>
            ///
            /// </summary>
            FAX1,
            /// <summary>
            ///
            /// </summary>
            CRTERMS,
            /// <summary>
            ///
            /// </summary>
            PRFTUP,
            /// <summary>
            ///
            /// </summary>
            PRFTDOWN,
            /// <summary>
            ///
            /// </summary>
            CURRENCY,
            /// <summary>
            ///
            /// </summary>
            PFDAY,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            email,
            /// <summary>
            ///
            /// </summary>
            CustRemarks,
            /// <summary>
            ///
            /// </summary>
            GEMSCODE,
        }

        #region Model
        private String _companycode;
        private String _cltcode;
        private String _cltname;
        private String _branchcode;
        private String _teamcode;
        private String _mstrclt;
        private String _addr1;
        private String _addr2;
        private String _addr3;
        private String _addr4;
        private String _addr5;
        private String _postal;
        private String _city;
        private String _country;
        private String _billaddr1;
        private String _billaddr2;
        private String _billaddr3;
        private String _billaddr4;
        private String _billaddr5;
        private String _billpostal;
        private String _billcity;
        private String _billcountry;
        private String _status;
        private String _contact1;
        private String _contact2;
        private String _jobtitle1;
        private String _jobtitle2;
        private String _phone1;
        private String _fax1;
        private String _crterms;
        private Decimal _prftup;
        private Decimal _prftdown;
        private String _currency;
        private Decimal _pfday;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _email;
        private String _custremarks;
        private String _gemscode;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(8)]
        /// </summary>
        [FieldMapping("CLTCODE", DbType.StringFixedLength, 8)]
        public String CLTCODE
        {
            set { AddAssigned("CLTCODE"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [PK/Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("CLTNAME", DbType.String, 100)]
        public String CLTNAME
        {
            set { AddAssigned("CLTNAME"); _cltname = value; }
            get { return _cltname; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("BRANCHCODE", DbType.StringFixedLength, 1)]
        public String BRANCHCODE
        {
            set { AddAssigned("BRANCHCODE"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [Un-Null/nchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.StringFixedLength, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(8)]
        /// </summary>
        [FieldMapping("MSTRCLT", DbType.StringFixedLength, 8)]
        public String MSTRCLT
        {
            set { AddAssigned("MSTRCLT"); _mstrclt = value; }
            get { return _mstrclt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR1", DbType.String, 50)]
        public String ADDR1
        {
            set { AddAssigned("ADDR1"); _addr1 = value; }
            get { return _addr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR2", DbType.String, 50)]
        public String ADDR2
        {
            set { AddAssigned("ADDR2"); _addr2 = value; }
            get { return _addr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR3", DbType.String, 50)]
        public String ADDR3
        {
            set { AddAssigned("ADDR3"); _addr3 = value; }
            get { return _addr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR4", DbType.String, 50)]
        public String ADDR4
        {
            set { AddAssigned("ADDR4"); _addr4 = value; }
            get { return _addr4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR5", DbType.String, 50)]
        public String ADDR5
        {
            set { AddAssigned("ADDR5"); _addr5 = value; }
            get { return _addr5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("POSTAL", DbType.String, 10)]
        public String POSTAL
        {
            set { AddAssigned("POSTAL"); _postal = value; }
            get { return _postal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITY", DbType.String, 3)]
        public String CITY
        {
            set { AddAssigned("CITY"); _city = value; }
            get { return _city; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRY", DbType.String, 3)]
        public String COUNTRY
        {
            set { AddAssigned("COUNTRY"); _country = value; }
            get { return _country; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BILLADDR1", DbType.String, 50)]
        public String BILLADDR1
        {
            set { AddAssigned("BILLADDR1"); _billaddr1 = value; }
            get { return _billaddr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BILLADDR2", DbType.String, 50)]
        public String BILLADDR2
        {
            set { AddAssigned("BILLADDR2"); _billaddr2 = value; }
            get { return _billaddr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BILLADDR3", DbType.String, 50)]
        public String BILLADDR3
        {
            set { AddAssigned("BILLADDR3"); _billaddr3 = value; }
            get { return _billaddr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BILLADDR4", DbType.String, 50)]
        public String BILLADDR4
        {
            set { AddAssigned("BILLADDR4"); _billaddr4 = value; }
            get { return _billaddr4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("BILLADDR5", DbType.String, 50)]
        public String BILLADDR5
        {
            set { AddAssigned("BILLADDR5"); _billaddr5 = value; }
            get { return _billaddr5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BILLPOSTAL", DbType.String, 10)]
        public String BILLPOSTAL
        {
            set { AddAssigned("BILLPOSTAL"); _billpostal = value; }
            get { return _billpostal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("BILLCITY", DbType.String, 3)]
        public String BILLCITY
        {
            set { AddAssigned("BILLCITY"); _billcity = value; }
            get { return _billcity; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("BILLCOUNTRY", DbType.String, 3)]
        public String BILLCOUNTRY
        {
            set { AddAssigned("BILLCOUNTRY"); _billcountry = value; }
            get { return _billcountry; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("STATUS", DbType.StringFixedLength, 10)]
        public String STATUS
        {
            set { AddAssigned("STATUS"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT1", DbType.String, 50)]
        public String CONTACT1
        {
            set { AddAssigned("CONTACT1"); _contact1 = value; }
            get { return _contact1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT2", DbType.String, 50)]
        public String CONTACT2
        {
            set { AddAssigned("CONTACT2"); _contact2 = value; }
            get { return _contact2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("JOBTITLE1", DbType.String, 30)]
        public String JOBTITLE1
        {
            set { AddAssigned("JOBTITLE1"); _jobtitle1 = value; }
            get { return _jobtitle1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("JOBTITLE2", DbType.String, 30)]
        public String JOBTITLE2
        {
            set { AddAssigned("JOBTITLE2"); _jobtitle2 = value; }
            get { return _jobtitle2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE1", DbType.String, 20)]
        public String PHONE1
        {
            set { AddAssigned("PHONE1"); _phone1 = value; }
            get { return _phone1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(16)]
        /// </summary>
        [FieldMapping("FAX1", DbType.String, 16)]
        public String FAX1
        {
            set { AddAssigned("FAX1"); _fax1 = value; }
            get { return _fax1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CRTERMS", DbType.String, 50)]
        public String CRTERMS
        {
            set { AddAssigned("CRTERMS"); _crterms = value; }
            get { return _crterms; }
        }
        /// <summary>
        /// [Allow Null/decimal(5,1)]
        /// </summary>
        [FieldMapping("PRFTUP", DbType.Decimal, 4)]
        public Decimal PRFTUP
        {
            set { AddAssigned("PRFTUP"); _prftup = value; }
            get { return _prftup; }
        }
        /// <summary>
        /// [Allow Null/decimal(5,1)]
        /// </summary>
        [FieldMapping("PRFTDOWN", DbType.Decimal, 4)]
        public Decimal PRFTDOWN
        {
            set { AddAssigned("PRFTDOWN"); _prftdown = value; }
            get { return _prftdown; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("CURRENCY", DbType.StringFixedLength, 3)]
        public String CURRENCY
        {
            set { AddAssigned("CURRENCY"); _currency = value; }
            get { return _currency; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("PFDAY", DbType.Decimal, 3)]
        public Decimal PFDAY
        {
            set { AddAssigned("PFDAY"); _pfday = value; }
            get { return _pfday; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("email", DbType.String, 80)]
        public String email
        {
            set { AddAssigned("email"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(300)]
        /// </summary>
        [FieldMapping("CustRemarks", DbType.String, 300)]
        public String CustRemarks
        {
            set { AddAssigned("CustRemarks"); _custremarks = value; }
            get { return _custremarks; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("GEMSCODE", DbType.String, 10)]
        public String GEMSCODE
        {
            set { AddAssigned("GEMSCODE"); _gemscode = value; }
            get { return _gemscode; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbCustomers : BaseList<tbCustomer, tbCustomers> { }
    public class tbCustomerPage : PageResult<tbCustomer, tbCustomers> { }
}

