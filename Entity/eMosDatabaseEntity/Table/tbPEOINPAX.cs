﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOINPAX
    /// </summary>
    [Serializable]
    public class tbPEOINPAX : BaseTable<tbPEOINPAX>
    {
        public tbPEOINPAX()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOINPAX";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            INVNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            PAXSEQ,
            /// <summary>
            ///
            /// </summary>
            PAXNAME,
            /// <summary>
            ///
            /// </summary>
            INVTYPE,
            /// <summary>
            ///
            /// </summary>
            curr,
            /// <summary>
            ///
            /// </summary>
            sell,
        }

        #region Model
        private String _companycode;
        private String _invnum;
        private String _seqnum;
        private Int16 _paxseq;
        private String _paxname;
        private String _invtype;
        private String _curr;
        private Decimal _sell;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("INVNUM", DbType.String, 10)]
        public String INVNUM
        {
            set { AddAssigned("INVNUM"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [PK/Un-Null/smallint(5)]
        /// </summary>
        [FieldMapping("PAXSEQ", DbType.Int16, 5)]
        public Int16 PAXSEQ
        {
            set { AddAssigned("PAXSEQ"); _paxseq = value; }
            get { return _paxseq; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PAXNAME", DbType.String, 100)]
        public String PAXNAME
        {
            set { AddAssigned("PAXNAME"); _paxname = value; }
            get { return _paxname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("INVTYPE", DbType.String, 1)]
        public String INVTYPE
        {
            set { AddAssigned("INVTYPE"); _invtype = value; }
            get { return _invtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("curr", DbType.String, 3)]
        public String curr
        {
            set { AddAssigned("curr"); _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("sell", DbType.Decimal, 18)]
        public Decimal sell
        {
            set { AddAssigned("sell"); _sell = value; }
            get { return _sell; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOINPAXs : BaseList<tbPEOINPAX, tbPEOINPAXs> { }
    public class tbPEOINPAXPage : PageResult<tbPEOINPAX, tbPEOINPAXs> { }
}

