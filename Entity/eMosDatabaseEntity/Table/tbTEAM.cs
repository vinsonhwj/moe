﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:TEAM
    /// </summary>
    [Serializable]
    public class tbTEAM : BaseTable<tbTEAM>
    {
        public tbTEAM()
            : base(DBTableName)
        { }
        public const string DBTableName = "TEAM";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BRANCHCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMNAME,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            crs,
            /// <summary>
            ///
            /// </summary>
            pseudo,
            /// <summary>
            ///
            /// </summary>
            email,
            /// <summary>
            ///
            /// </summary>
            cltcode1,
            /// <summary>
            ///
            /// </summary>
            cltcode2,
            /// <summary>
            ///
            /// </summary>
            cltcode3,
            /// <summary>
            ///
            /// </summary>
            IsRequireRef,
            /// <summary>
            ///
            /// </summary>
            TeamAddr1,
            /// <summary>
            ///
            /// </summary>
            TeamAddr2,
            /// <summary>
            ///
            /// </summary>
            TeamAddr3,
            /// <summary>
            ///
            /// </summary>
            TeamAddr4,
            /// <summary>
            ///
            /// </summary>
            TeamAddr1_ZHTW,
            /// <summary>
            ///
            /// </summary>
            TeamAddr2_ZHTW,
            /// <summary>
            ///
            /// </summary>
            TeamAddr3_ZHTW,
            /// <summary>
            ///
            /// </summary>
            TeamAddr4_ZHTW,
            /// <summary>
            ///
            /// </summary>
            ContactPhone,
            /// <summary>
            ///
            /// </summary>
            ContactFax,
            /// <summary>
            ///
            /// </summary>
            CompLogo,
            /// <summary>
            ///
            /// </summary>
            UseBookPseudo,
        }

        #region Model
        private String _companycode;
        private String _branchcode;
        private String _teamcode;
        private String _teamname;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _crs;
        private String _pseudo;
        private String _email;
        private String _cltcode1;
        private String _cltcode2;
        private String _cltcode3;
        private String _isrequireref;
        private String _teamaddr1;
        private String _teamaddr2;
        private String _teamaddr3;
        private String _teamaddr4;
        private String _teamaddr1_zhtw;
        private String _teamaddr2_zhtw;
        private String _teamaddr3_zhtw;
        private String _teamaddr4_zhtw;
        private String _contactphone;
        private String _contactfax;
        private String _complogo;
        private String _usebookpseudo;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("BRANCHCODE", DbType.StringFixedLength, 1)]
        public String BRANCHCODE
        {
            set { AddAssigned("BRANCHCODE"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.StringFixedLength, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("TEAMNAME", DbType.String, 20)]
        public String TEAMNAME
        {
            set { AddAssigned("TEAMNAME"); _teamname = value; }
            get { return _teamname; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("crs", DbType.String, 1)]
        public String crs
        {
            set { AddAssigned("crs"); _crs = value; }
            get { return _crs; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("pseudo", DbType.String, 5)]
        public String pseudo
        {
            set { AddAssigned("pseudo"); _pseudo = value; }
            get { return _pseudo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("email", DbType.String, 50)]
        public String email
        {
            set { AddAssigned("email"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/nchar(20)]
        /// </summary>
        [FieldMapping("cltcode1", DbType.StringFixedLength, 20)]
        public String cltcode1
        {
            set { AddAssigned("cltcode1"); _cltcode1 = value; }
            get { return _cltcode1; }
        }
        /// <summary>
        /// [Allow Null/nchar(20)]
        /// </summary>
        [FieldMapping("cltcode2", DbType.StringFixedLength, 20)]
        public String cltcode2
        {
            set { AddAssigned("cltcode2"); _cltcode2 = value; }
            get { return _cltcode2; }
        }
        /// <summary>
        /// [Allow Null/nchar(20)]
        /// </summary>
        [FieldMapping("cltcode3", DbType.StringFixedLength, 20)]
        public String cltcode3
        {
            set { AddAssigned("cltcode3"); _cltcode3 = value; }
            get { return _cltcode3; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("IsRequireRef", DbType.StringFixedLength, 1)]
        public String IsRequireRef
        {
            set { AddAssigned("IsRequireRef"); _isrequireref = value; }
            get { return _isrequireref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr1", DbType.String, 50)]
        public String TeamAddr1
        {
            set { AddAssigned("TeamAddr1"); _teamaddr1 = value; }
            get { return _teamaddr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr2", DbType.String, 50)]
        public String TeamAddr2
        {
            set { AddAssigned("TeamAddr2"); _teamaddr2 = value; }
            get { return _teamaddr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr3", DbType.String, 50)]
        public String TeamAddr3
        {
            set { AddAssigned("TeamAddr3"); _teamaddr3 = value; }
            get { return _teamaddr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr4", DbType.String, 50)]
        public String TeamAddr4
        {
            set { AddAssigned("TeamAddr4"); _teamaddr4 = value; }
            get { return _teamaddr4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr1_ZHTW", DbType.String, 50)]
        public String TeamAddr1_ZHTW
        {
            set { AddAssigned("TeamAddr1_ZHTW"); _teamaddr1_zhtw = value; }
            get { return _teamaddr1_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr2_ZHTW", DbType.String, 50)]
        public String TeamAddr2_ZHTW
        {
            set { AddAssigned("TeamAddr2_ZHTW"); _teamaddr2_zhtw = value; }
            get { return _teamaddr2_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr3_ZHTW", DbType.String, 50)]
        public String TeamAddr3_ZHTW
        {
            set { AddAssigned("TeamAddr3_ZHTW"); _teamaddr3_zhtw = value; }
            get { return _teamaddr3_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("TeamAddr4_ZHTW", DbType.String, 50)]
        public String TeamAddr4_ZHTW
        {
            set { AddAssigned("TeamAddr4_ZHTW"); _teamaddr4_zhtw = value; }
            get { return _teamaddr4_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("ContactPhone", DbType.String, 30)]
        public String ContactPhone
        {
            set { AddAssigned("ContactPhone"); _contactphone = value; }
            get { return _contactphone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("ContactFax", DbType.String, 30)]
        public String ContactFax
        {
            set { AddAssigned("ContactFax"); _contactfax = value; }
            get { return _contactfax; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("CompLogo", DbType.AnsiString, 1)]
        public String CompLogo
        {
            set { AddAssigned("CompLogo"); _complogo = value; }
            get { return _complogo; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)/Default:('Y')]
        /// </summary>
        [FieldMapping("UseBookPseudo", DbType.AnsiString, 1)]
        public String UseBookPseudo
        {
            set { AddAssigned("UseBookPseudo"); _usebookpseudo = value; }
            get { return _usebookpseudo; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbTEAMs : BaseList<tbTEAM, tbTEAMs> { }
    public class tbTEAMPage : PageResult<tbTEAM, tbTEAMs> { }
}

