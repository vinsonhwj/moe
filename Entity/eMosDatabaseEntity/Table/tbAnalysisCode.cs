﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:AnalysisCode
    /// </summary>
    [Serializable]
    public class tbAnalysisCode : BaseTable<tbAnalysisCode>
    {
        public tbAnalysisCode()
            : base(DBTableName)
        { }
        public const string DBTableName = "AnalysisCode";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            ANALYSISCODE,
            /// <summary>
            ///
            /// </summary>
            ANALYSISDESC,
            /// <summary>
            ///
            /// </summary>
            STARTDATE,
            /// <summary>
            ///
            /// </summary>
            ENDDATE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            SUSPENDED,
        }

        #region Model
        private String _companycode;
        private String _analysiscode;
        private String _analysisdesc;
        private DateTime _startdate;
        private DateTime _enddate;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Int32 _suspended;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("ANALYSISCODE", DbType.String, 15)]
        public String ANALYSISCODE
        {
            set { AddAssigned("ANALYSISCODE"); _analysiscode = value; }
            get { return _analysiscode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ANALYSISDESC", DbType.String, 100)]
        public String ANALYSISDESC
        {
            set { AddAssigned("ANALYSISDESC"); _analysisdesc = value; }
            get { return _analysisdesc; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("STARTDATE", DbType.DateTime, 8)]
        public DateTime STARTDATE
        {
            set { AddAssigned("STARTDATE"); _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("ENDDATE", DbType.DateTime, 8)]
        public DateTime ENDDATE
        {
            set { AddAssigned("ENDDATE"); _enddate = value; }
            get { return _enddate; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Un-Null/nchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.StringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.StringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("SUSPENDED", DbType.Int32, 10)]
        public Int32 SUSPENDED
        {
            set { AddAssigned("SUSPENDED"); _suspended = value; }
            get { return _suspended; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbAnalysisCodes : BaseList<tbAnalysisCode, tbAnalysisCodes> { }
    public class tbAnalysisCodePage : PageResult<tbAnalysisCode, tbAnalysisCodes> { }
}

