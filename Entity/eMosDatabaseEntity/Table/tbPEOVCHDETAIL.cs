﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOVCHDETAIL
    /// </summary>
    [Serializable]
    public class tbPEOVCHDETAIL : BaseTable<tbPEOVCHDETAIL>
    {
        public tbPEOVCHDETAIL()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOVCHDETAIL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            VCHNUM,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            DETLTYPE,
            /// <summary>
            ///
            /// </summary>
            DETLDESC,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            RMNTS,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ITEMNAME,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            COSTAMT,
            /// <summary>
            ///
            /// </summary>
            HKDAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            ITEMTYPE,
        }

        #region Model
        private String _companycode;
        private String _vchnum;
        private String _segnum;
        private String _seqnum;
        private String _detltype;
        private String _detldesc;
        private Decimal _qty;
        private Decimal _rmnts;
        private String _ratename;
        private String _itemname;
        private String _costcurr;
        private Decimal _costamt;
        private Decimal _hkdamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _itemtype;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VCHNUM", DbType.String, 10)]
        public String VCHNUM
        {
            set { AddAssigned("VCHNUM"); _vchnum = value; }
            get { return _vchnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("DETLTYPE", DbType.String, 10)]
        public String DETLTYPE
        {
            set { AddAssigned("DETLTYPE"); _detltype = value; }
            get { return _detltype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("DETLDESC", DbType.String, 70)]
        public String DETLDESC
        {
            set { AddAssigned("DETLDESC"); _detldesc = value; }
            get { return _detldesc; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("QTY", DbType.Decimal, 3)]
        public Decimal QTY
        {
            set { AddAssigned("QTY"); _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("RMNTS", DbType.Decimal, 3)]
        public Decimal RMNTS
        {
            set { AddAssigned("RMNTS"); _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(40)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 40)]
        public String RATENAME
        {
            set { AddAssigned("RATENAME"); _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("ITEMNAME", DbType.String, 100)]
        public String ITEMNAME
        {
            set { AddAssigned("ITEMNAME"); _itemname = value; }
            get { return _itemname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("COSTAMT", DbType.Decimal, 14)]
        public Decimal COSTAMT
        {
            set { AddAssigned("COSTAMT"); _costamt = value; }
            get { return _costamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("HKDAMT", DbType.Decimal, 14)]
        public Decimal HKDAMT
        {
            set { AddAssigned("HKDAMT"); _hkdamt = value; }
            get { return _hkdamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("ITEMTYPE", DbType.StringFixedLength, 2)]
        public String ITEMTYPE
        {
            set { AddAssigned("ITEMTYPE"); _itemtype = value; }
            get { return _itemtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOVCHDETAILs : BaseList<tbPEOVCHDETAIL, tbPEOVCHDETAILs> { }
    public class tbPEOVCHDETAILPage : PageResult<tbPEOVCHDETAIL, tbPEOVCHDETAILs> { }
}

