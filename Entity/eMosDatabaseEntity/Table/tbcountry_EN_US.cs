﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:country_EN_US
    /// </summary>
    [Serializable]
    public class tbcountry_EN_US : BaseTable<tbcountry_EN_US>
    {
        public tbcountry_EN_US()
            : base(DBTableName)
        { }
        public const string DBTableName = "country_EN_US";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            SOURCECODE,
            /// <summary>
            ///
            /// </summary>
            REGIONCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYNAME,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
        }

        #region Model
        private String _sourcecode;
        private String _regioncode;
        private String _countrycode;
        private String _countryname;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("SOURCECODE", DbType.StringFixedLength, 3)]
        public String SOURCECODE
        {
            set { AddAssigned("SOURCECODE"); _sourcecode = value; }
            get { return _sourcecode; }
        }
        /// <summary>
        /// [Un-Null/nchar(10)]
        /// </summary>
        [FieldMapping("REGIONCODE", DbType.StringFixedLength, 10)]
        public String REGIONCODE
        {
            set { AddAssigned("REGIONCODE"); _regioncode = value; }
            get { return _regioncode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.StringFixedLength, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Un-Null/nchar(40)]
        /// </summary>
        [FieldMapping("COUNTRYNAME", DbType.StringFixedLength, 40)]
        public String COUNTRYNAME
        {
            set { AddAssigned("COUNTRYNAME"); _countryname = value; }
            get { return _countryname; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.StringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.StringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbcountry_EN_USs : BaseList<tbcountry_EN_US, tbcountry_EN_USs> { }
    public class tbcountry_EN_USPage : PageResult<tbcountry_EN_US, tbcountry_EN_USs> { }
}

