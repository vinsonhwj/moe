﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    public partial class tbPEOAIRDETAIL
    {
        public tbPEOAIRDETAIL(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _companycode = string.Empty;
                _bkgref = string.Empty;
                _segnum = string.Empty;
                _seqnum = string.Empty;
                _seqtype = string.Empty;
                _qty = 0;
                _curr = string.Empty;
                _amt = 0;
                _taxcurr = string.Empty;
                _taxamt = 0;
                _createby = string.Empty;
                _updateby = string.Empty;
                _agetype = string.Empty;
            }
        }
    }
}
