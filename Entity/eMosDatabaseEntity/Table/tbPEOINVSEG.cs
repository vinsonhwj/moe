﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOINVSEG
    /// </summary>
    [Serializable]
    public class tbPEOINVSEG : BaseTable<tbPEOINVSEG>
    {
        public tbPEOINVSEG()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOINVSEG";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            INVNUM,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SERVTYPE,
            /// <summary>
            ///
            /// </summary>
            SERVNAME,
            /// <summary>
            ///
            /// </summary>
            SERVDESC,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            GSTAMT,
            /// <summary>
            ///
            /// </summary>
            SEGTYPE,
            /// <summary>
            ///
            /// </summary>
            Destination,
            /// <summary>
            ///
            /// </summary>
            ReasonCode,
            /// <summary>
            ///
            /// </summary>
            WithBkf,
            /// <summary>
            ///
            /// </summary>
            WithTfr,
            /// <summary>
            ///
            /// </summary>
            GroupID,
        }

        #region Model
        private String _companycode;
        private String _invnum;
        private String _segnum;
        private String _servtype;
        private String _servname;
        private String _servdesc;
        private DateTime _createon;
        private String _createby;
        private Decimal _gstamt;
        private String _segtype;
        private String _destination;
        private String _reasoncode;
        private String _withbkf;
        private String _withtfr;
        private Int32 _groupid;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("INVNUM", DbType.String, 10)]
        public String INVNUM
        {
            set { AddAssigned("INVNUM"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SERVTYPE", DbType.String, 5)]
        public String SERVTYPE
        {
            set { AddAssigned("SERVTYPE"); _servtype = value; }
            get { return _servtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("SERVNAME", DbType.String, 100)]
        public String SERVNAME
        {
            set { AddAssigned("SERVNAME"); _servname = value; }
            get { return _servname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1000)]
        /// </summary>
        [FieldMapping("SERVDESC", DbType.String, 1000)]
        public String SERVDESC
        {
            set { AddAssigned("SERVDESC"); _servdesc = value; }
            get { return _servdesc; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)/Default:(0)]
        /// </summary>
        [FieldMapping("GSTAMT", DbType.Decimal, 16)]
        public Decimal GSTAMT
        {
            set { AddAssigned("GSTAMT"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SEGTYPE", DbType.String, 3)]
        public String SEGTYPE
        {
            set { AddAssigned("SEGTYPE"); _segtype = value; }
            get { return _segtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("Destination", DbType.String, 3)]
        public String Destination
        {
            set { AddAssigned("Destination"); _destination = value; }
            get { return _destination; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("ReasonCode", DbType.String, 2)]
        public String ReasonCode
        {
            set { AddAssigned("ReasonCode"); _reasoncode = value; }
            get { return _reasoncode; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("WithBkf", DbType.AnsiString, 1)]
        public String WithBkf
        {
            set { AddAssigned("WithBkf"); _withbkf = value; }
            get { return _withbkf; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("WithTfr", DbType.AnsiString, 1)]
        public String WithTfr
        {
            set { AddAssigned("WithTfr"); _withtfr = value; }
            get { return _withtfr; }
        }
        /// <summary>
        /// [Un-Null/int(10)/Default:(0)]
        /// </summary>
        [FieldMapping("GroupID", DbType.Int32, 10)]
        public Int32 GroupID
        {
            set { AddAssigned("GroupID"); _groupid = value; }
            get { return _groupid; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOINVSEGs : BaseList<tbPEOINVSEG, tbPEOINVSEGs> { }
    public class tbPEOINVSEGPage : PageResult<tbPEOINVSEG, tbPEOINVSEGs> { }
}

