﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:SUPPLIER
    /// </summary>
    [Serializable]
    public class tbSUPPLIER : BaseTable<tbSUPPLIER>
    {
        public tbSUPPLIER()
            : base(DBTableName)
        { }
        public const string DBTableName = "SUPPLIER";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPCODE,
            /// <summary>
            ///
            /// </summary>
            SUPPNAME,
            /// <summary>
            ///
            /// </summary>
            ADDR1,
            /// <summary>
            ///
            /// </summary>
            ADDR2,
            /// <summary>
            ///
            /// </summary>
            ADDR3,
            /// <summary>
            ///
            /// </summary>
            ADDR4,
            /// <summary>
            ///
            /// </summary>
            ADDR5,
            /// <summary>
            ///
            /// </summary>
            PHONE,
            /// <summary>
            ///
            /// </summary>
            FAX,
            /// <summary>
            ///
            /// </summary>
            CURRENCY,
            /// <summary>
            ///
            /// </summary>
            POSTAL,
            /// <summary>
            ///
            /// </summary>
            CITY,
            /// <summary>
            ///
            /// </summary>
            COUNTRY,
            /// <summary>
            ///
            /// </summary>
            CONTACT1,
            /// <summary>
            ///
            /// </summary>
            CONTACT2,
            /// <summary>
            ///
            /// </summary>
            DESIGNATION1,
            /// <summary>
            ///
            /// </summary>
            DESIGNATION2,
            /// <summary>
            ///
            /// </summary>
            STATUS,
            /// <summary>
            ///
            /// </summary>
            email,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            IsCheck,
        }

        #region Model
        private String _companycode;
        private String _suppcode;
        private String _suppname;
        private String _addr1;
        private String _addr2;
        private String _addr3;
        private String _addr4;
        private String _addr5;
        private String _phone;
        private String _fax;
        private String _currency;
        private String _postal;
        private String _city;
        private String _country;
        private String _contact1;
        private String _contact2;
        private String _designation1;
        private String _designation2;
        private String _status;
        private String _email;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Int32 _ischeck;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(8)]
        /// </summary>
        [FieldMapping("SUPPCODE", DbType.StringFixedLength, 8)]
        public String SUPPCODE
        {
            set { AddAssigned("SUPPCODE"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("SUPPNAME", DbType.String, 70)]
        public String SUPPNAME
        {
            set { AddAssigned("SUPPNAME"); _suppname = value; }
            get { return _suppname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR1", DbType.String, 50)]
        public String ADDR1
        {
            set { AddAssigned("ADDR1"); _addr1 = value; }
            get { return _addr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR2", DbType.String, 50)]
        public String ADDR2
        {
            set { AddAssigned("ADDR2"); _addr2 = value; }
            get { return _addr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR3", DbType.String, 50)]
        public String ADDR3
        {
            set { AddAssigned("ADDR3"); _addr3 = value; }
            get { return _addr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR4", DbType.String, 50)]
        public String ADDR4
        {
            set { AddAssigned("ADDR4"); _addr4 = value; }
            get { return _addr4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR5", DbType.String, 50)]
        public String ADDR5
        {
            set { AddAssigned("ADDR5"); _addr5 = value; }
            get { return _addr5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE", DbType.String, 20)]
        public String PHONE
        {
            set { AddAssigned("PHONE"); _phone = value; }
            get { return _phone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("FAX", DbType.String, 20)]
        public String FAX
        {
            set { AddAssigned("FAX"); _fax = value; }
            get { return _fax; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("CURRENCY", DbType.StringFixedLength, 3)]
        public String CURRENCY
        {
            set { AddAssigned("CURRENCY"); _currency = value; }
            get { return _currency; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("POSTAL", DbType.String, 10)]
        public String POSTAL
        {
            set { AddAssigned("POSTAL"); _postal = value; }
            get { return _postal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITY", DbType.String, 3)]
        public String CITY
        {
            set { AddAssigned("CITY"); _city = value; }
            get { return _city; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRY", DbType.String, 3)]
        public String COUNTRY
        {
            set { AddAssigned("COUNTRY"); _country = value; }
            get { return _country; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT1", DbType.String, 50)]
        public String CONTACT1
        {
            set { AddAssigned("CONTACT1"); _contact1 = value; }
            get { return _contact1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT2", DbType.String, 50)]
        public String CONTACT2
        {
            set { AddAssigned("CONTACT2"); _contact2 = value; }
            get { return _contact2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DESIGNATION1", DbType.String, 30)]
        public String DESIGNATION1
        {
            set { AddAssigned("DESIGNATION1"); _designation1 = value; }
            get { return _designation1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DESIGNATION2", DbType.String, 30)]
        public String DESIGNATION2
        {
            set { AddAssigned("DESIGNATION2"); _designation2 = value; }
            get { return _designation2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STATUS", DbType.String, 10)]
        public String STATUS
        {
            set { AddAssigned("STATUS"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("email", DbType.String, 80)]
        public String email
        {
            set { AddAssigned("email"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)/Default:(getdate())]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)/Default:(getdate())]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("IsCheck", DbType.Int32, 10)]
        public Int32 IsCheck
        {
            set { AddAssigned("IsCheck"); _ischeck = value; }
            get { return _ischeck; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbSUPPLIERs : BaseList<tbSUPPLIER, tbSUPPLIERs> { }
    public class tbSUPPLIERPage : PageResult<tbSUPPLIER, tbSUPPLIERs> { }
}

