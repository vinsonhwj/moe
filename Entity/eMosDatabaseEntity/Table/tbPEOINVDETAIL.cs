﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOINVDETAIL
    /// </summary>
    [Serializable]
    public class tbPEOINVDETAIL : BaseTable<tbPEOINVDETAIL>
    {
        public tbPEOINVDETAIL()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOINVDETAIL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            INVNUM,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            DETLTYPE,
            /// <summary>
            ///
            /// </summary>
            DETLDESC,
            /// <summary>
            ///
            /// </summary>
            UNITPRICE,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            RMNTS,
            /// <summary>
            ///
            /// </summary>
            RATENAME,
            /// <summary>
            ///
            /// </summary>
            ITEMNAME,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            SELLAMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            HKDAMT,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            SHOWDETAILFEE,
            /// <summary>
            ///
            /// </summary>
            AMTFEE,
            /// <summary>
            ///
            /// </summary>
            ITEMTYPE,
            /// <summary>
            ///
            /// </summary>
            DISCOUNT_MARKUP_PER,
            /// <summary>
            ///
            /// </summary>
            INVOICE_NETT_GROSS,
            /// <summary>
            ///
            /// </summary>
            gstAmt,
            /// <summary>
            ///
            /// </summary>
            gsttype,
        }

        #region Model
        private String _companycode;
        private String _invnum;
        private String _segnum;
        private String _seqnum;
        private String _detltype;
        private String _detldesc;
        private Decimal _unitprice;
        private Decimal _qty;
        private Decimal _rmnts;
        private String _ratename;
        private String _itemname;
        private String _sellcurr;
        private Decimal _sellamt;
        private String _taxcurr;
        private Decimal _taxamt;
        private Decimal _hkdamt;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _showdetailfee;
        private Decimal _amtfee;
        private String _itemtype;
        private Decimal _discount_markup_per;
        private Decimal _invoice_nett_gross;
        private Decimal _gstamt;
        private String _gsttype;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("INVNUM", DbType.String, 10)]
        public String INVNUM
        {
            set { AddAssigned("INVNUM"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("DETLTYPE", DbType.String, 10)]
        public String DETLTYPE
        {
            set { AddAssigned("DETLTYPE"); _detltype = value; }
            get { return _detltype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("DETLDESC", DbType.String, 70)]
        public String DETLDESC
        {
            set { AddAssigned("DETLDESC"); _detldesc = value; }
            get { return _detldesc; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("UNITPRICE", DbType.Decimal, 14)]
        public Decimal UNITPRICE
        {
            set { AddAssigned("UNITPRICE"); _unitprice = value; }
            get { return _unitprice; }
        }
        /// <summary>
        /// [Allow Null/decimal(4,0)]
        /// </summary>
        [FieldMapping("QTY", DbType.Decimal, 4)]
        public Decimal QTY
        {
            set { AddAssigned("QTY"); _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/decimal(4,0)/Default:(1)]
        /// </summary>
        [FieldMapping("RMNTS", DbType.Decimal, 4)]
        public Decimal RMNTS
        {
            set { AddAssigned("RMNTS"); _rmnts = value; }
            get { return _rmnts; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RATENAME", DbType.String, 50)]
        public String RATENAME
        {
            set { AddAssigned("RATENAME"); _ratename = value; }
            get { return _ratename; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("ITEMNAME", DbType.String, 200)]
        public String ITEMNAME
        {
            set { AddAssigned("ITEMNAME"); _itemname = value; }
            get { return _itemname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SELLAMT", DbType.Decimal, 14)]
        public Decimal SELLAMT
        {
            set { AddAssigned("SELLAMT"); _sellamt = value; }
            get { return _sellamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("HKDAMT", DbType.Decimal, 14)]
        public Decimal HKDAMT
        {
            set { AddAssigned("HKDAMT"); _hkdamt = value; }
            get { return _hkdamt; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)/Default:('N')]
        /// </summary>
        [FieldMapping("SHOWDETAILFEE", DbType.String, 1)]
        public String SHOWDETAILFEE
        {
            set { AddAssigned("SHOWDETAILFEE"); _showdetailfee = value; }
            get { return _showdetailfee; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)/Default:(0)]
        /// </summary>
        [FieldMapping("AMTFEE", DbType.Decimal, 16)]
        public Decimal AMTFEE
        {
            set { AddAssigned("AMTFEE"); _amtfee = value; }
            get { return _amtfee; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("ITEMTYPE", DbType.StringFixedLength, 2)]
        public String ITEMTYPE
        {
            set { AddAssigned("ITEMTYPE"); _itemtype = value; }
            get { return _itemtype; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("DISCOUNT_MARKUP_PER", DbType.Decimal, 14)]
        public Decimal DISCOUNT_MARKUP_PER
        {
            set { AddAssigned("DISCOUNT_MARKUP_PER"); _discount_markup_per = value; }
            get { return _discount_markup_per; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)/Default:(0)]
        /// </summary>
        [FieldMapping("INVOICE_NETT_GROSS", DbType.Decimal, 14)]
        public Decimal INVOICE_NETT_GROSS
        {
            set { AddAssigned("INVOICE_NETT_GROSS"); _invoice_nett_gross = value; }
            get { return _invoice_nett_gross; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("gstAmt", DbType.Decimal, 14)]
        public Decimal gstAmt
        {
            set { AddAssigned("gstAmt"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("gsttype", DbType.StringFixedLength, 2)]
        public String gsttype
        {
            set { AddAssigned("gsttype"); _gsttype = value; }
            get { return _gsttype; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOINVDETAILs : BaseList<tbPEOINVDETAIL, tbPEOINVDETAILs> { }
    public class tbPEOINVDETAILPage : PageResult<tbPEOINVDETAIL, tbPEOINVDETAILs> { }
}

