﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:Creditcardsales
    /// </summary>
    [Serializable]
    public class tbCreditcardsales : BaseTable<tbCreditcardsales>
    {
        public tbCreditcardsales()
            : base(DBTableName)
        { }
        public const string DBTableName = "Creditcardsales";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            InvNum,
            /// <summary>
            ///
            /// </summary>
            Cardtype,
            /// <summary>
            ///
            /// </summary>
            CardNum,
            /// <summary>
            ///
            /// </summary>
            Cardholder,
            /// <summary>
            ///
            /// </summary>
            Expirymonth,
            /// <summary>
            ///
            /// </summary>
            Expiryyear,
            /// <summary>
            ///
            /// </summary>
            CustomerCode,
            /// <summary>
            ///
            /// </summary>
            CustomerRef,
            /// <summary>
            ///
            /// </summary>
            TANum,
            /// <summary>
            ///
            /// </summary>
            ApprovalCode,
            /// <summary>
            ///
            /// </summary>
            Aircode,
            /// <summary>
            ///
            /// </summary>
            Ticket,
            /// <summary>
            ///
            /// </summary>
            Createby,
            /// <summary>
            ///
            /// </summary>
            Createon,
            /// <summary>
            ///
            /// </summary>
            Updateby,
            /// <summary>
            ///
            /// </summary>
            Updateon,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            AUTONUM,
        }

        #region Model
        private String _companycode;
        private String _invnum;
        private String _cardtype;
        private String _cardnum;
        private String _cardholder;
        private String _expirymonth;
        private String _expiryyear;
        private String _customercode;
        private String _customerref;
        private String _tanum;
        private String _approvalcode;
        private String _aircode;
        private String _ticket;
        private String _createby;
        private DateTime _createon;
        private String _updateby;
        private DateTime _updateon;
        private String _voidby;
        private DateTime _voidon;
        private Int32 _autonum;
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.StringFixedLength, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Allow Null/nchar(11)]
        /// </summary>
        [FieldMapping("InvNum", DbType.StringFixedLength, 11)]
        public String InvNum
        {
            set { AddAssigned("InvNum"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(4)]
        /// </summary>
        [FieldMapping("Cardtype", DbType.StringFixedLength, 4)]
        public String Cardtype
        {
            set { AddAssigned("Cardtype"); _cardtype = value; }
            get { return _cardtype; }
        }
        /// <summary>
        /// [Allow Null/nchar(16)]
        /// </summary>
        [FieldMapping("CardNum", DbType.StringFixedLength, 16)]
        public String CardNum
        {
            set { AddAssigned("CardNum"); _cardnum = value; }
            get { return _cardnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(30)]
        /// </summary>
        [FieldMapping("Cardholder", DbType.StringFixedLength, 30)]
        public String Cardholder
        {
            set { AddAssigned("Cardholder"); _cardholder = value; }
            get { return _cardholder; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("Expirymonth", DbType.StringFixedLength, 2)]
        public String Expirymonth
        {
            set { AddAssigned("Expirymonth"); _expirymonth = value; }
            get { return _expirymonth; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("Expiryyear", DbType.StringFixedLength, 2)]
        public String Expiryyear
        {
            set { AddAssigned("Expiryyear"); _expiryyear = value; }
            get { return _expiryyear; }
        }
        /// <summary>
        /// [Allow Null/nchar(8)]
        /// </summary>
        [FieldMapping("CustomerCode", DbType.StringFixedLength, 8)]
        public String CustomerCode
        {
            set { AddAssigned("CustomerCode"); _customercode = value; }
            get { return _customercode; }
        }
        /// <summary>
        /// [Allow Null/nchar(15)]
        /// </summary>
        [FieldMapping("CustomerRef", DbType.StringFixedLength, 15)]
        public String CustomerRef
        {
            set { AddAssigned("CustomerRef"); _customerref = value; }
            get { return _customerref; }
        }
        /// <summary>
        /// [Allow Null/nchar(9)]
        /// </summary>
        [FieldMapping("TANum", DbType.StringFixedLength, 9)]
        public String TANum
        {
            set { AddAssigned("TANum"); _tanum = value; }
            get { return _tanum; }
        }
        /// <summary>
        /// [Allow Null/nchar(6)]
        /// </summary>
        [FieldMapping("ApprovalCode", DbType.StringFixedLength, 6)]
        public String ApprovalCode
        {
            set { AddAssigned("ApprovalCode"); _approvalcode = value; }
            get { return _approvalcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("Aircode", DbType.StringFixedLength, 3)]
        public String Aircode
        {
            set { AddAssigned("Aircode"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Allow Null/nchar(14)]
        /// </summary>
        [FieldMapping("Ticket", DbType.StringFixedLength, 14)]
        public String Ticket
        {
            set { AddAssigned("Ticket"); _ticket = value; }
            get { return _ticket; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("Createby", DbType.StringFixedLength, 3)]
        public String Createby
        {
            set { AddAssigned("Createby"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("Createon", DbType.DateTime, 8)]
        public DateTime Createon
        {
            set { AddAssigned("Createon"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("Updateby", DbType.StringFixedLength, 3)]
        public String Updateby
        {
            set { AddAssigned("Updateby"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("Updateon", DbType.DateTime, 8)]
        public DateTime Updateon
        {
            set { AddAssigned("Updateon"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.StringFixedLength, 3)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Un-Null/int(10)]
        /// </summary>
        [FieldMapping("AUTONUM", DbType.Int32, 10, Enums.DataHandle.UnInsert, Enums.DataHandle.UnUpdate)]
        public Int32 AUTONUM
        {
            set { AddAssigned("AUTONUM"); _autonum = value; }
            get { return _autonum; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbCreditcardsaless : BaseList<tbCreditcardsales, tbCreditcardsaless> { }
    public class tbCreditcardsalesPage : PageResult<tbCreditcardsales, tbCreditcardsaless> { }
}

