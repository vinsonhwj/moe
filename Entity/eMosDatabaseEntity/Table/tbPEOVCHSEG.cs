﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOVCHSEG
    /// </summary>
    [Serializable]
    public class tbPEOVCHSEG : BaseTable<tbPEOVCHSEG>
    {
        public tbPEOVCHSEG()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOVCHSEG";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            VCHNUM,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SERVTYPE,
            /// <summary>
            ///
            /// </summary>
            SERVNAME,
            /// <summary>
            ///
            /// </summary>
            SERVDESC,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            GSTAMT,
            /// <summary>
            ///
            /// </summary>
            SEGTYPE,
        }

        #region Model
        private String _companycode;
        private String _vchnum;
        private String _segnum;
        private String _servtype;
        private String _servname;
        private String _servdesc;
        private DateTime _createon;
        private String _createby;
        private Decimal _gstamt;
        private String _segtype;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VCHNUM", DbType.String, 10)]
        public String VCHNUM
        {
            set { AddAssigned("VCHNUM"); _vchnum = value; }
            get { return _vchnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SERVTYPE", DbType.String, 5)]
        public String SERVTYPE
        {
            set { AddAssigned("SERVTYPE"); _servtype = value; }
            get { return _servtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("SERVNAME", DbType.String, 100)]
        public String SERVNAME
        {
            set { AddAssigned("SERVNAME"); _servname = value; }
            get { return _servname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1000)]
        /// </summary>
        [FieldMapping("SERVDESC", DbType.String, 1000)]
        public String SERVDESC
        {
            set { AddAssigned("SERVDESC"); _servdesc = value; }
            get { return _servdesc; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("GSTAMT", DbType.Decimal, 14)]
        public Decimal GSTAMT
        {
            set { AddAssigned("GSTAMT"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SEGTYPE", DbType.String, 3)]
        public String SEGTYPE
        {
            set { AddAssigned("SEGTYPE"); _segtype = value; }
            get { return _segtype; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOVCHSEGs : BaseList<tbPEOVCHSEG, tbPEOVCHSEGs> { }
    public class tbPEOVCHSEGPage : PageResult<tbPEOVCHSEG, tbPEOVCHSEGs> { }
}

