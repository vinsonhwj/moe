﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOHTL
    /// </summary>
    [Serializable]
    public class tbPEOHTL : BaseTable<tbPEOHTL>
    {
        public tbPEOHTL()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOHTL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            SourcePnr,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            ADCOMTXNO,
            /// <summary>
            ///
            /// </summary>
            ADCOMSEQNUM,
            /// <summary>
            ///
            /// </summary>
            HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            ARRDATE,
            /// <summary>
            ///
            /// </summary>
            ARRTIME,
            /// <summary>
            ///
            /// </summary>
            DEPARTDATE,
            /// <summary>
            ///
            /// </summary>
            DEPARTTIME,
            /// <summary>
            ///
            /// </summary>
            ETA,
            /// <summary>
            ///
            /// </summary>
            ETD,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALCOST,
            /// <summary>
            ///
            /// </summary>
            TOTALSELL,
            /// <summary>
            ///
            /// </summary>
            TOTALCTAX,
            /// <summary>
            ///
            /// </summary>
            TOTALSTAX,
            /// <summary>
            ///
            /// </summary>
            FORMPAY,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            INVRMK1,
            /// <summary>
            ///
            /// </summary>
            INVRMK2,
            /// <summary>
            ///
            /// </summary>
            VCHRMK1,
            /// <summary>
            ///
            /// </summary>
            VCHRMK2,
            /// <summary>
            ///
            /// </summary>
            WESTELNO,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            SPCL_REQUEST,
            /// <summary>
            ///
            /// </summary>
            HTLREFERENCE,
            /// <summary>
            ///
            /// </summary>
            HTLCFmby,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            SOURCEREF,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            showDetailFee,
            /// <summary>
            ///
            /// </summary>
            SuppCode,
            /// <summary>
            ///
            /// </summary>
            hotelVoucherType,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            roomTypeCode,
            /// <summary>
            ///
            /// </summary>
            fullRoomRate,
            /// <summary>
            ///
            /// </summary>
            dailyRate,
            /// <summary>
            ///
            /// </summary>
            lowRoomRate,
            /// <summary>
            ///
            /// </summary>
            RateTypeCode,
            /// <summary>
            ///
            /// </summary>
            isRecalable,
            /// <summary>
            ///
            /// </summary>
            ReasonCode,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME,
            /// <summary>
            ///
            /// </summary>
            WithBkf,
            /// <summary>
            ///
            /// </summary>
            WithTfr,
            /// <summary>
            ///
            /// </summary>
            IssuedDoc,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _sourcepnr;
        private String _countrycode;
        private String _citycode;
        private String _adcomtxno;
        private String _adcomseqnum;
        private String _hotelcode;
        private DateTime _arrdate;
        private String _arrtime;
        private DateTime _departdate;
        private String _departtime;
        private String _eta;
        private String _etd;
        private String _sellcurr;
        private String _costcurr;
        private Decimal _totalcost;
        private Decimal _totalsell;
        private Decimal _totalctax;
        private Decimal _totalstax;
        private String _formpay;
        private String _rmk1;
        private String _rmk2;
        private String _invrmk1;
        private String _invrmk2;
        private String _vchrmk1;
        private String _vchrmk2;
        private String _westelno;
        private String _sourcesystem;
        private String _spcl_request;
        private String _htlreference;
        private String _htlcfmby;
        private String _voidby;
        private DateTime _voidon;
        private String _createby;
        private DateTime _createon;
        private String _updateby;
        private DateTime _updateon;
        private String _sourceref;
        private String _gsttax;
        private String _showdetailfee;
        private String _suppcode;
        private String _hotelvouchertype;
        private String _cod1;
        private String _cod2;
        private String _cod3;
        private String _roomtypecode;
        private Decimal _fullroomrate;
        private Decimal _dailyrate;
        private Decimal _lowroomrate;
        private String _ratetypecode;
        private String _isrecalable;
        private String _reasoncode;
        private String _hotelname;
        private String _withbkf;
        private String _withtfr;
        private String _issueddoc;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 5)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SourcePnr", DbType.String, 10)]
        public String SourcePnr
        {
            set { AddAssigned("SourcePnr"); _sourcepnr = value; }
            get { return _sourcepnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.String, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.String, 3)]
        public String CITYCODE
        {
            set { AddAssigned("CITYCODE"); _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("ADCOMTXNO", DbType.String, 12)]
        public String ADCOMTXNO
        {
            set { AddAssigned("ADCOMTXNO"); _adcomtxno = value; }
            get { return _adcomtxno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("ADCOMSEQNUM", DbType.String, 5)]
        public String ADCOMSEQNUM
        {
            set { AddAssigned("ADCOMSEQNUM"); _adcomseqnum = value; }
            get { return _adcomseqnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("HOTELCODE", DbType.String, 8)]
        public String HOTELCODE
        {
            set { AddAssigned("HOTELCODE"); _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ARRDATE", DbType.DateTime, 8)]
        public DateTime ARRDATE
        {
            set { AddAssigned("ARRDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("ARRTIME", DbType.String, 6)]
        public String ARRTIME
        {
            set { AddAssigned("ARRTIME"); _arrtime = value; }
            get { return _arrtime; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DEPARTDATE", DbType.DateTime, 8)]
        public DateTime DEPARTDATE
        {
            set { AddAssigned("DEPARTDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("DEPARTTIME", DbType.String, 6)]
        public String DEPARTTIME
        {
            set { AddAssigned("DEPARTTIME"); _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("ETA", DbType.String, 14)]
        public String ETA
        {
            set { AddAssigned("ETA"); _eta = value; }
            get { return _eta; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("ETD", DbType.String, 14)]
        public String ETD
        {
            set { AddAssigned("ETD"); _etd = value; }
            get { return _etd; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCOST", DbType.Decimal, 14)]
        public Decimal TOTALCOST
        {
            set { AddAssigned("TOTALCOST"); _totalcost = value; }
            get { return _totalcost; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSELL", DbType.Decimal, 14)]
        public Decimal TOTALSELL
        {
            set { AddAssigned("TOTALSELL"); _totalsell = value; }
            get { return _totalsell; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCTAX", DbType.Decimal, 14)]
        public Decimal TOTALCTAX
        {
            set { AddAssigned("TOTALCTAX"); _totalctax = value; }
            get { return _totalctax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSTAX", DbType.Decimal, 14)]
        public Decimal TOTALSTAX
        {
            set { AddAssigned("TOTALSTAX"); _totalstax = value; }
            get { return _totalstax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FORMPAY", DbType.String, 50)]
        public String FORMPAY
        {
            set { AddAssigned("FORMPAY"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 50)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 50)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK1", DbType.String, 100)]
        public String INVRMK1
        {
            set { AddAssigned("INVRMK1"); _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK2", DbType.String, 100)]
        public String INVRMK2
        {
            set { AddAssigned("INVRMK2"); _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("VCHRMK1", DbType.String, 80)]
        public String VCHRMK1
        {
            set { AddAssigned("VCHRMK1"); _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("VCHRMK2", DbType.String, 80)]
        public String VCHRMK2
        {
            set { AddAssigned("VCHRMK2"); _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("WESTELNO", DbType.String, 10)]
        public String WESTELNO
        {
            set { AddAssigned("WESTELNO"); _westelno = value; }
            get { return _westelno; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("SPCL_REQUEST", DbType.String, 255)]
        public String SPCL_REQUEST
        {
            set { AddAssigned("SPCL_REQUEST"); _spcl_request = value; }
            get { return _spcl_request; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("HTLREFERENCE", DbType.String, 20)]
        public String HTLREFERENCE
        {
            set { AddAssigned("HTLREFERENCE"); _htlreference = value; }
            get { return _htlreference; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("HTLCFmby", DbType.String, 30)]
        public String HTLCFmby
        {
            set { AddAssigned("HTLCFmby"); _htlcfmby = value; }
            get { return _htlcfmby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(12)]
        /// </summary>
        [FieldMapping("SOURCEREF", DbType.String, 12)]
        public String SOURCEREF
        {
            set { AddAssigned("SOURCEREF"); _sourceref = value; }
            get { return _sourceref; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)/Default:('N')]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.StringFixedLength, 1)]
        public String GSTTAX
        {
            set { AddAssigned("GSTTAX"); _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("showDetailFee", DbType.StringFixedLength, 1)]
        public String showDetailFee
        {
            set { AddAssigned("showDetailFee"); _showdetailfee = value; }
            get { return _showdetailfee; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SuppCode", DbType.String, 8)]
        public String SuppCode
        {
            set { AddAssigned("SuppCode"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("hotelVoucherType", DbType.StringFixedLength, 1)]
        public String hotelVoucherType
        {
            set { AddAssigned("hotelVoucherType"); _hotelvouchertype = value; }
            get { return _hotelvouchertype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 15)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 15)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(15)]
        /// </summary>
        [FieldMapping("COD3", DbType.String, 15)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("roomTypeCode", DbType.StringFixedLength, 1)]
        public String roomTypeCode
        {
            set { AddAssigned("roomTypeCode"); _roomtypecode = value; }
            get { return _roomtypecode; }
        }
        /// <summary>
        /// [Allow Null/decimal(9,2)]
        /// </summary>
        [FieldMapping("fullRoomRate", DbType.Decimal, 7)]
        public Decimal fullRoomRate
        {
            set { AddAssigned("fullRoomRate"); _fullroomrate = value; }
            get { return _fullroomrate; }
        }
        /// <summary>
        /// [Allow Null/decimal(9,2)]
        /// </summary>
        [FieldMapping("dailyRate", DbType.Decimal, 7)]
        public Decimal dailyRate
        {
            set { AddAssigned("dailyRate"); _dailyrate = value; }
            get { return _dailyrate; }
        }
        /// <summary>
        /// [Allow Null/decimal(9,2)]
        /// </summary>
        [FieldMapping("lowRoomRate", DbType.Decimal, 7)]
        public Decimal lowRoomRate
        {
            set { AddAssigned("lowRoomRate"); _lowroomrate = value; }
            get { return _lowroomrate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("RateTypeCode", DbType.String, 4)]
        public String RateTypeCode
        {
            set { AddAssigned("RateTypeCode"); _ratetypecode = value; }
            get { return _ratetypecode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)/Default:(null)]
        /// </summary>
        [FieldMapping("isRecalable", DbType.String, 4)]
        public String isRecalable
        {
            set { AddAssigned("isRecalable"); _isrecalable = value; }
            get { return _isrecalable; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("ReasonCode", DbType.String, 2)]
        public String ReasonCode
        {
            set { AddAssigned("ReasonCode"); _reasoncode = value; }
            get { return _reasoncode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(400)]
        /// </summary>
        [FieldMapping("HOTELNAME", DbType.String, 400)]
        public String HOTELNAME
        {
            set { AddAssigned("HOTELNAME"); _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("WithBkf", DbType.AnsiString, 1)]
        public String WithBkf
        {
            set { AddAssigned("WithBkf"); _withbkf = value; }
            get { return _withbkf; }
        }
        /// <summary>
        /// [Allow Null/varchar(1)]
        /// </summary>
        [FieldMapping("WithTfr", DbType.AnsiString, 1)]
        public String WithTfr
        {
            set { AddAssigned("WithTfr"); _withtfr = value; }
            get { return _withtfr; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IssuedDoc", DbType.AnsiStringFixedLength, 1)]
        public String IssuedDoc
        {
            set { AddAssigned("IssuedDoc"); _issueddoc = value; }
            get { return _issueddoc; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOHTLs : BaseList<tbPEOHTL, tbPEOHTLs> { }
    public class tbPEOHTLPage : PageResult<tbPEOHTL, tbPEOHTLs> { }
}

