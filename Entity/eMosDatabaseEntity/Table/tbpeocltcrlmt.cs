﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:peocltcrlmt
    /// </summary>
    [Serializable]
    public class tbpeocltcrlmt : BaseTable<tbpeocltcrlmt>
    {
        public tbpeocltcrlmt()
            : base(DBTableName)
        { }
        public const string DBTableName = "peocltcrlmt";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            companycode,
            /// <summary>
            ///
            /// </summary>
            cltlevel,
            /// <summary>
            ///
            /// </summary>
            masteracct,
            /// <summary>
            ///
            /// </summary>
            cltcode,
            /// <summary>
            ///
            /// </summary>
            currcode,
            /// <summary>
            ///
            /// </summary>
            crlamt,
            /// <summary>
            ///
            /// </summary>
            insuredamt,
            /// <summary>
            ///
            /// </summary>
            crterms,
            /// <summary>
            ///
            /// </summary>
            status,
            /// <summary>
            ///
            /// </summary>
            statusmsg,
            /// <summary>
            ///
            /// </summary>
            warnpercent,
            /// <summary>
            ///
            /// </summary>
            crloutamt,
            /// <summary>
            ///
            /// </summary>
            createon,
            /// <summary>
            ///
            /// </summary>
            createby,
            /// <summary>
            ///
            /// </summary>
            updateon,
            /// <summary>
            ///
            /// </summary>
            updateby,
            /// <summary>
            ///
            /// </summary>
            CrTerms1,
            /// <summary>
            ///
            /// </summary>
            CrTerms2,
            /// <summary>
            ///
            /// </summary>
            CrTerms3,
            /// <summary>
            ///
            /// </summary>
            CrTerms4,
            /// <summary>
            ///
            /// </summary>
            CrTerms5,
            /// <summary>
            ///
            /// </summary>
            CrTerms6,
            /// <summary>
            ///
            /// </summary>
            CrBal1,
            /// <summary>
            ///
            /// </summary>
            CrBal2,
            /// <summary>
            ///
            /// </summary>
            CrBal3,
            /// <summary>
            ///
            /// </summary>
            CrBal4,
            /// <summary>
            ///
            /// </summary>
            CrBal5,
            /// <summary>
            ///
            /// </summary>
            CrBal6,
            /// <summary>
            ///
            /// </summary>
            ChkCrTerms,
            /// <summary>
            ///
            /// </summary>
            ChkCrBal,
            /// <summary>
            ///
            /// </summary>
            LastCalculatedOn,
            /// <summary>
            ///
            /// </summary>
            CrCtrlKey,
            /// <summary>
            ///
            /// </summary>
            firstinvdate,
        }

        #region Model
        private String _companycode;
        private String _cltlevel;
        private String _masteracct;
        private String _cltcode;
        private String _currcode;
        private Decimal _crlamt;
        private Decimal _insuredamt;
        private Decimal _crterms;
        private String _status;
        private String _statusmsg;
        private Decimal _warnpercent;
        private Decimal _crloutamt;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Decimal _crterms1;
        private Decimal _crterms2;
        private Decimal _crterms3;
        private Decimal _crterms4;
        private Decimal _crterms5;
        private Decimal _crterms6;
        private Decimal _crbal1;
        private Decimal _crbal2;
        private Decimal _crbal3;
        private Decimal _crbal4;
        private Decimal _crbal5;
        private Decimal _crbal6;
        private String _chkcrterms;
        private String _chkcrbal;
        private DateTime _lastcalculatedon;
        private String _crctrlkey;
        private DateTime _firstinvdate;
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("companycode", DbType.StringFixedLength, 2)]
        public String companycode
        {
            set { AddAssigned("companycode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("cltlevel", DbType.StringFixedLength, 1)]
        public String cltlevel
        {
            set { AddAssigned("cltlevel"); _cltlevel = value; }
            get { return _cltlevel; }
        }
        /// <summary>
        /// [Allow Null/nchar(8)]
        /// </summary>
        [FieldMapping("masteracct", DbType.StringFixedLength, 8)]
        public String masteracct
        {
            set { AddAssigned("masteracct"); _masteracct = value; }
            get { return _masteracct; }
        }
        /// <summary>
        /// [Un-Null/nchar(8)]
        /// </summary>
        [FieldMapping("cltcode", DbType.StringFixedLength, 8)]
        public String cltcode
        {
            set { AddAssigned("cltcode"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/char(3)]
        /// </summary>
        [FieldMapping("currcode", DbType.AnsiStringFixedLength, 3)]
        public String currcode
        {
            set { AddAssigned("currcode"); _currcode = value; }
            get { return _currcode; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)]
        /// </summary>
        [FieldMapping("crlamt", DbType.Decimal, 16)]
        public Decimal crlamt
        {
            set { AddAssigned("crlamt"); _crlamt = value; }
            get { return _crlamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("insuredamt", DbType.Decimal, 14)]
        public Decimal insuredamt
        {
            set { AddAssigned("insuredamt"); _insuredamt = value; }
            get { return _insuredamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("crterms", DbType.Decimal, 3)]
        public Decimal crterms
        {
            set { AddAssigned("crterms"); _crterms = value; }
            get { return _crterms; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("status", DbType.AnsiStringFixedLength, 1)]
        public String status
        {
            set { AddAssigned("status"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nchar(40)]
        /// </summary>
        [FieldMapping("statusmsg", DbType.StringFixedLength, 40)]
        public String statusmsg
        {
            set { AddAssigned("statusmsg"); _statusmsg = value; }
            get { return _statusmsg; }
        }
        /// <summary>
        /// [Allow Null/decimal(3,0)]
        /// </summary>
        [FieldMapping("warnpercent", DbType.Decimal, 3)]
        public Decimal warnpercent
        {
            set { AddAssigned("warnpercent"); _warnpercent = value; }
            get { return _warnpercent; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("crloutamt", DbType.Decimal, 14)]
        public Decimal crloutamt
        {
            set { AddAssigned("crloutamt"); _crloutamt = value; }
            get { return _crloutamt; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("createon", DbType.DateTime, 8)]
        public DateTime createon
        {
            set { AddAssigned("createon"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("createby", DbType.StringFixedLength, 3)]
        public String createby
        {
            set { AddAssigned("createby"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("updateon", DbType.DateTime, 8)]
        public DateTime updateon
        {
            set { AddAssigned("updateon"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("updateby", DbType.StringFixedLength, 3)]
        public String updateby
        {
            set { AddAssigned("updateby"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms1", DbType.Decimal, 14)]
        public Decimal CrTerms1
        {
            set { AddAssigned("CrTerms1"); _crterms1 = value; }
            get { return _crterms1; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms2", DbType.Decimal, 14)]
        public Decimal CrTerms2
        {
            set { AddAssigned("CrTerms2"); _crterms2 = value; }
            get { return _crterms2; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms3", DbType.Decimal, 14)]
        public Decimal CrTerms3
        {
            set { AddAssigned("CrTerms3"); _crterms3 = value; }
            get { return _crterms3; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms4", DbType.Decimal, 14)]
        public Decimal CrTerms4
        {
            set { AddAssigned("CrTerms4"); _crterms4 = value; }
            get { return _crterms4; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms5", DbType.Decimal, 14)]
        public Decimal CrTerms5
        {
            set { AddAssigned("CrTerms5"); _crterms5 = value; }
            get { return _crterms5; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrTerms6", DbType.Decimal, 14)]
        public Decimal CrTerms6
        {
            set { AddAssigned("CrTerms6"); _crterms6 = value; }
            get { return _crterms6; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal1", DbType.Decimal, 14)]
        public Decimal CrBal1
        {
            set { AddAssigned("CrBal1"); _crbal1 = value; }
            get { return _crbal1; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal2", DbType.Decimal, 14)]
        public Decimal CrBal2
        {
            set { AddAssigned("CrBal2"); _crbal2 = value; }
            get { return _crbal2; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal3", DbType.Decimal, 14)]
        public Decimal CrBal3
        {
            set { AddAssigned("CrBal3"); _crbal3 = value; }
            get { return _crbal3; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal4", DbType.Decimal, 14)]
        public Decimal CrBal4
        {
            set { AddAssigned("CrBal4"); _crbal4 = value; }
            get { return _crbal4; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal5", DbType.Decimal, 14)]
        public Decimal CrBal5
        {
            set { AddAssigned("CrBal5"); _crbal5 = value; }
            get { return _crbal5; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CrBal6", DbType.Decimal, 14)]
        public Decimal CrBal6
        {
            set { AddAssigned("CrBal6"); _crbal6 = value; }
            get { return _crbal6; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("ChkCrTerms", DbType.StringFixedLength, 1)]
        public String ChkCrTerms
        {
            set { AddAssigned("ChkCrTerms"); _chkcrterms = value; }
            get { return _chkcrterms; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("ChkCrBal", DbType.StringFixedLength, 1)]
        public String ChkCrBal
        {
            set { AddAssigned("ChkCrBal"); _chkcrbal = value; }
            get { return _chkcrbal; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("LastCalculatedOn", DbType.DateTime, 8)]
        public DateTime LastCalculatedOn
        {
            set { AddAssigned("LastCalculatedOn"); _lastcalculatedon = value; }
            get { return _lastcalculatedon; }
        }
        /// <summary>
        /// [Allow Null/char(5)]
        /// </summary>
        [FieldMapping("CrCtrlKey", DbType.AnsiStringFixedLength, 5)]
        public String CrCtrlKey
        {
            set { AddAssigned("CrCtrlKey"); _crctrlkey = value; }
            get { return _crctrlkey; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("firstinvdate", DbType.DateTime, 8)]
        public DateTime firstinvdate
        {
            set { AddAssigned("firstinvdate"); _firstinvdate = value; }
            get { return _firstinvdate; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbpeocltcrlmts : BaseList<tbpeocltcrlmt, tbpeocltcrlmts> { }
    public class tbpeocltcrlmtPage : PageResult<tbpeocltcrlmt, tbpeocltcrlmts> { }
}

