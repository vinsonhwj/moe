﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{

    public partial class tbPEOPAX : BaseTable<tbPEOPAX>
    {
        public tbPEOPAX(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _bkgref = string.Empty;
                _companycode = string.Empty;
                _segnum = string.Empty;
                _paxnum = string.Empty;
                _paxlname = string.Empty;
                _paxfname = string.Empty;
                _paxtitle = string.Empty;
                _paxtype = string.Empty;
                _paxair = string.Empty;
                _paxhotel = string.Empty;
                _paxother = string.Empty;
                _paxage = 0;
                _rmk1 = string.Empty;
                _rmk2 = string.Empty;
                _voidon = string.Empty;
                _sourcesystem = string.Empty;
                _createby = string.Empty;
                _updateby = string.Empty;
                _adcomtxno = string.Empty;
                _address1 = string.Empty;
                _address2 = string.Empty;
                _address3 = string.Empty;
                _address4 = string.Empty;
                _contacthometel =string.Empty;
                _contactmobiletel = string.Empty;
                _contactofficetel = string.Empty;
                _deleted = string.Empty;
                _email = string.Empty;
                _evisa_import = string.Empty;
                _membershipno = string.Empty;
                _nationality = string.Empty;
                _passportno = string.Empty;
                _source = string.Empty;
                _sourceref = string.Empty;
                _visa = string.Empty;
            }
        }

    }

}

