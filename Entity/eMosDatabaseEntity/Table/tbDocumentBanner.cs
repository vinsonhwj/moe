﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:tbDocumentBanner
    /// </summary>
    [Serializable]
    public class tbDocumentBanner : BaseTable<tbDocumentBanner>
    {
        public tbDocumentBanner()
            : base(DBTableName)
        { }
        public const string DBTableName = "DocumentBanner";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            companycode,
            /// <summary>
            ///
            /// </summary>
            bkgref,
            /// <summary>
            ///
            /// </summary>
            docnum,
            /// <summary>
            ///
            /// </summary>
            bannertype,
            /// <summary>
            ///
            /// </summary>
            createon,
            /// <summary>
            ///
            /// </summary>
            createby,
            /// <summary>
            ///
            /// </summary>
            updateon,
            /// <summary>
            ///
            /// </summary>
            updateby,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _docnum;
        private String _bannertype;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("companycode", DbType.String, 2)]
        public String companycode
        {
            set { AddAssigned("companycode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("bkgref", DbType.String, 10)]
        public String bkgref
        {
            set { AddAssigned("bkgref"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("docnum", DbType.String, 10)]
        public String docnum
        {
            set { AddAssigned("docnum"); _docnum = value; }
            get { return _docnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("bannertype", DbType.String, 50)]
        public String bannertype
        {
            set { AddAssigned("bannertype"); _bannertype = value; }
            get { return _bannertype; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("createon", DbType.DateTime, 8)]
        public DateTime createon
        {
            set { AddAssigned("createon"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("createby", DbType.String, 5)]
        public String createby
        {
            set { AddAssigned("createby"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("updateon", DbType.DateTime, 8)]
        public DateTime updateon
        {
            set { AddAssigned("updateon"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("updateby", DbType.String, 5)]
        public String updateby
        {
            set { AddAssigned("updateby"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbDocumentBanners : BaseList<tbDocumentBanner, tbDocumentBanners> { }
    public class tbDocumentBannerPage : PageResult<tbDocumentBanner, tbDocumentBanners> { }
}

