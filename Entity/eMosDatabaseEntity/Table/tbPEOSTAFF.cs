﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOSTAFF
    /// </summary>
    [Serializable]
    public class tbPEOSTAFF : BaseTable<tbPEOSTAFF>
    {
        public tbPEOSTAFF()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOSTAFF";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            BRANCHCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            PHONE,
            /// <summary>
            ///
            /// </summary>
            JOBTITLE,
            /// <summary>
            ///
            /// </summary>
            SINECODE,
            /// <summary>
            ///
            /// </summary>
            UPLIMIT,
            /// <summary>
            ///
            /// </summary>
            LOWLIMIT,
            /// <summary>
            ///
            /// </summary>
            STAFFNAME,
            /// <summary>
            ///
            /// </summary>
            STAFFNUM,
            /// <summary>
            ///
            /// </summary>
            EFFECTIVEDATE,
            /// <summary>
            ///
            /// </summary>
            SUSPENDDATE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            Sine,
            /// <summary>
            ///
            /// </summary>
            CODE,
            /// <summary>
            ///
            /// </summary>
            email,
            /// <summary>
            ///
            /// </summary>
            cltcode,
            /// <summary>
            ///
            /// </summary>
            FullName,
            /// <summary>
            ///
            /// </summary>
            QBox,
            /// <summary>
            ///
            /// </summary>
            CommLevel,
        }

        #region Model
        private String _companycode;
        private String _staffcode;
        private String _branchcode;
        private String _teamcode;
        private String _phone;
        private String _jobtitle;
        private String _sinecode;
        private Decimal _uplimit;
        private Decimal _lowlimit;
        private String _staffname;
        private String _staffnum;
        private DateTime _effectivedate;
        private DateTime _suspenddate;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _sine;
        private String _code;
        private String _email;
        private String _cltcode;
        private String _fullname;
        private String _qbox;
        private String _commlevel;
        /// <summary>
        /// [PK/Un-Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 50)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("BRANCHCODE", DbType.StringFixedLength, 1)]
        public String BRANCHCODE
        {
            set { AddAssigned("BRANCHCODE"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.String, 10)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PHONE", DbType.String, 20)]
        public String PHONE
        {
            set { AddAssigned("PHONE"); _phone = value; }
            get { return _phone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("JOBTITLE", DbType.String, 30)]
        public String JOBTITLE
        {
            set { AddAssigned("JOBTITLE"); _jobtitle = value; }
            get { return _jobtitle; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("SINECODE", DbType.String, 30)]
        public String SINECODE
        {
            set { AddAssigned("SINECODE"); _sinecode = value; }
            get { return _sinecode; }
        }
        /// <summary>
        /// [Allow Null/decimal(6,2)]
        /// </summary>
        [FieldMapping("UPLIMIT", DbType.Decimal, 4)]
        public Decimal UPLIMIT
        {
            set { AddAssigned("UPLIMIT"); _uplimit = value; }
            get { return _uplimit; }
        }
        /// <summary>
        /// [Allow Null/decimal(6,2)]
        /// </summary>
        [FieldMapping("LOWLIMIT", DbType.Decimal, 4)]
        public Decimal LOWLIMIT
        {
            set { AddAssigned("LOWLIMIT"); _lowlimit = value; }
            get { return _lowlimit; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("STAFFNAME", DbType.String, 70)]
        public String STAFFNAME
        {
            set { AddAssigned("STAFFNAME"); _staffname = value; }
            get { return _staffname; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFNUM", DbType.String, 10)]
        public String STAFFNUM
        {
            set { AddAssigned("STAFFNUM"); _staffnum = value; }
            get { return _staffnum; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("EFFECTIVEDATE", DbType.DateTime, 8)]
        public DateTime EFFECTIVEDATE
        {
            set { AddAssigned("EFFECTIVEDATE"); _effectivedate = value; }
            get { return _effectivedate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("SUSPENDDATE", DbType.DateTime, 8)]
        public DateTime SUSPENDDATE
        {
            set { AddAssigned("SUSPENDDATE"); _suspenddate = value; }
            get { return _suspenddate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 50)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 50)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("Sine", DbType.String, 10)]
        public String Sine
        {
            set { AddAssigned("Sine"); _sine = value; }
            get { return _sine; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CODE", DbType.String, 10)]
        public String CODE
        {
            set { AddAssigned("CODE"); _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(70)]
        /// </summary>
        [FieldMapping("email", DbType.String, 70)]
        public String email
        {
            set { AddAssigned("email"); _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("cltcode", DbType.StringFixedLength, 10)]
        public String cltcode
        {
            set { AddAssigned("cltcode"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("FullName", DbType.String, 200)]
        public String FullName
        {
            set { AddAssigned("FullName"); _fullname = value; }
            get { return _fullname; }
        }
        /// <summary>
        /// [Allow Null/varchar(20)]
        /// </summary>
        [FieldMapping("QBox", DbType.AnsiString, 20)]
        public String QBox
        {
            set { AddAssigned("QBox"); _qbox = value; }
            get { return _qbox; }
        }
        /// <summary>
        /// [Allow Null/varchar(5)]
        /// </summary>
        [FieldMapping("CommLevel", DbType.AnsiString, 5)]
        public String CommLevel
        {
            set { AddAssigned("CommLevel"); _commlevel = value; }
            get { return _commlevel; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOSTAFFs : BaseList<tbPEOSTAFF, tbPEOSTAFFs> { }
    public class tbPEOSTAFFPage : PageResult<tbPEOSTAFF, tbPEOSTAFFs> { }
}

