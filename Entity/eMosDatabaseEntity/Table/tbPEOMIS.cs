﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOMIS
    /// </summary>
    [Serializable]
    public class tbPEOMIS : BaseTable<tbPEOMIS>
    {
        public tbPEOMIS()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOMIS";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SEGNUM,
            /// <summary>
            ///
            /// </summary>
            CLTCODE,
            /// <summary>
            ///
            /// </summary>
            GEMSCODE,
            /// <summary>
            ///
            /// </summary>
            GEMSCODE3,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            REASONCODE,
            /// <summary>
            ///
            /// </summary>
            VOIDON,
            /// <summary>
            ///
            /// </summary>
            VOIDBY,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            TRANSACTIONFEE,
            /// <summary>
            ///
            /// </summary>
            MIS1,
            /// <summary>
            ///
            /// </summary>
            MIS2,
            /// <summary>
            ///
            /// </summary>
            MIS3,
        }

        #region Model
        private String _bkgref;
        private String _companycode;
        private String _segnum;
        private String _cltcode;
        private String _gemscode;
        private String _gemscode3;
        private String _cod1;
        private String _cod2;
        private String _cod3;
        private String _reasoncode;
        private DateTime _voidon;
        private String _voidby;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Decimal _transactionfee;
        private String _mis1;
        private String _mis2;
        private String _mis3;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.String, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SEGNUM", DbType.String, 10)]
        public String SEGNUM
        {
            set { AddAssigned("SEGNUM"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nchar(8)]
        /// </summary>
        [FieldMapping("CLTCODE", DbType.StringFixedLength, 8)]
        public String CLTCODE
        {
            set { AddAssigned("CLTCODE"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("GEMSCODE", DbType.String, 10)]
        public String GEMSCODE
        {
            set { AddAssigned("GEMSCODE"); _gemscode = value; }
            get { return _gemscode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("GEMSCODE3", DbType.String, 10)]
        public String GEMSCODE3
        {
            set { AddAssigned("GEMSCODE3"); _gemscode3 = value; }
            get { return _gemscode3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 20)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 20)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD3", DbType.String, 20)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("REASONCODE", DbType.String, 2)]
        public String REASONCODE
        {
            set { AddAssigned("REASONCODE"); _reasoncode = value; }
            get { return _reasoncode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VOIDON", DbType.DateTime, 8)]
        public DateTime VOIDON
        {
            set { AddAssigned("VOIDON"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VOIDBY", DbType.String, 10)]
        public String VOIDBY
        {
            set { AddAssigned("VOIDBY"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("TRANSACTIONFEE", DbType.Decimal, 18)]
        public Decimal TRANSACTIONFEE
        {
            set { AddAssigned("TRANSACTIONFEE"); _transactionfee = value; }
            get { return _transactionfee; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("MIS1", DbType.String, 50)]
        public String MIS1
        {
            set { AddAssigned("MIS1"); _mis1 = value; }
            get { return _mis1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("MIS2", DbType.String, 50)]
        public String MIS2
        {
            set { AddAssigned("MIS2"); _mis2 = value; }
            get { return _mis2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("MIS3", DbType.String, 50)]
        public String MIS3
        {
            set { AddAssigned("MIS3"); _mis3 = value; }
            get { return _mis3; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOMISs : BaseList<tbPEOMIS, tbPEOMISs> { }
    public class tbPEOMISPage : PageResult<tbPEOMIS, tbPEOMISs> { }
}

