﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOXOTKTref
    /// </summary>
    [Serializable]
    public class tbPEOXOTKTref : BaseTable<tbPEOXOTKTref>
    {
        public tbPEOXOTKTref()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOXOTKTref";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            Ticket,
            /// <summary>
            ///
            /// </summary>
            TktSeq,
            /// <summary>
            ///
            /// </summary>
            BkgRef,
            /// <summary>
            ///
            /// </summary>
            XONum,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
            /// <summary>
            ///
            /// </summary>
            CreateBy,
            /// <summary>
            ///
            /// </summary>
            UpdateOn,
            /// <summary>
            ///
            /// </summary>
            UpdateBy,
        }

        #region Model
        private String _companycode;
        private String _ticket;
        private String _tktseq;
        private String _bkgref;
        private String _xonum;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.String, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("Ticket", DbType.String, 10)]
        public String Ticket
        {
            set { AddAssigned("Ticket"); _ticket = value; }
            get { return _ticket; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("TktSeq", DbType.String, 10)]
        public String TktSeq
        {
            set { AddAssigned("TktSeq"); _tktseq = value; }
            get { return _tktseq; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BkgRef", DbType.String, 10)]
        public String BkgRef
        {
            set { AddAssigned("BkgRef"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("XONum", DbType.String, 10)]
        public String XONum
        {
            set { AddAssigned("XONum"); _xonum = value; }
            get { return _xonum; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CreateBy", DbType.String, 10)]
        public String CreateBy
        {
            set { AddAssigned("CreateBy"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UpdateOn", DbType.DateTime, 8)]
        public DateTime UpdateOn
        {
            set { AddAssigned("UpdateOn"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UpdateBy", DbType.String, 10)]
        public String UpdateBy
        {
            set { AddAssigned("UpdateBy"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOXOTKTrefs : BaseList<tbPEOXOTKTref, tbPEOXOTKTrefs> { }
    public class tbPEOXOTKTrefPage : PageResult<tbPEOXOTKTref, tbPEOXOTKTrefs> { }
}

