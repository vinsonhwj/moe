﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:LOGIN
    /// </summary>
    [Serializable]
    public class tbLOGIN : BaseTable<tbLOGIN>
    {
        public tbLOGIN()
            : base(DBTableName)
        { }
        public const string DBTableName = "LOGIN";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BRANCHCODE,
            /// <summary>
            ///
            /// </summary>
            TEAMCODE,
            /// <summary>
            ///
            /// </summary>
            LOGIN,
            /// <summary>
            ///
            /// </summary>
            EFFECTIVEON,
            /// <summary>
            ///
            /// </summary>
            ISLOCK,
            /// <summary>
            ///
            /// </summary>
            SUSPENDON,
            /// <summary>
            ///
            /// </summary>
            DISABLEON,
            /// <summary>
            ///
            /// </summary>
            PWDUPDATEON,
            /// <summary>
            ///
            /// </summary>
            PWDCHANGEDATE,
            /// <summary>
            ///
            /// </summary>
            PWDPERIOD,
            /// <summary>
            ///
            /// </summary>
            PWDPERIODTYPE,
            /// <summary>
            ///
            /// </summary>
            PASSWORD,
            /// <summary>
            ///
            /// </summary>
            GROUPID,
            /// <summary>
            ///
            /// </summary>
            LOGINGROUP,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            DAYS,
            /// <summary>
            ///
            /// </summary>
            NIGHTS,
            /// <summary>
            ///
            /// </summary>
            REMINDERDAYS,
            /// <summary>
            ///
            /// </summary>
            ChangePwdNextTime,
            /// <summary>
            ///
            /// </summary>
            LastLoginOn,
        }

        #region Model
        private String _companycode;
        private String _branchcode;
        private String _teamcode;
        private String _login;
        private DateTime _effectiveon;
        private String _islock;
        private DateTime _suspendon;
        private DateTime _disableon;
        private DateTime _pwdupdateon;
        private DateTime _pwdchangedate;
        private Decimal _pwdperiod;
        private String _pwdperiodtype;
        private String _password;
        private String _groupid;
        private String _logingroup;
        private String _staffcode;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Int16 _days;
        private Int16 _nights;
        private Int16 _reminderdays;
        private String _changepwdnexttime;
        private DateTime _lastloginon;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("BRANCHCODE", DbType.StringFixedLength, 1)]
        public String BRANCHCODE
        {
            set { AddAssigned("BRANCHCODE"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [Un-Null/nchar(5)]
        /// </summary>
        [FieldMapping("TEAMCODE", DbType.StringFixedLength, 5)]
        public String TEAMCODE
        {
            set { AddAssigned("TEAMCODE"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("LOGIN", DbType.String, 10)]
        public String LOGIN
        {
            set { AddAssigned("LOGIN"); _login = value; }
            get { return _login; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("EFFECTIVEON", DbType.DateTime, 8)]
        public DateTime EFFECTIVEON
        {
            set { AddAssigned("EFFECTIVEON"); _effectiveon = value; }
            get { return _effectiveon; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("ISLOCK", DbType.StringFixedLength, 1)]
        public String ISLOCK
        {
            set { AddAssigned("ISLOCK"); _islock = value; }
            get { return _islock; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("SUSPENDON", DbType.DateTime, 8)]
        public DateTime SUSPENDON
        {
            set { AddAssigned("SUSPENDON"); _suspendon = value; }
            get { return _suspendon; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DISABLEON", DbType.DateTime, 8)]
        public DateTime DISABLEON
        {
            set { AddAssigned("DISABLEON"); _disableon = value; }
            get { return _disableon; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("PWDUPDATEON", DbType.DateTime, 8)]
        public DateTime PWDUPDATEON
        {
            set { AddAssigned("PWDUPDATEON"); _pwdupdateon = value; }
            get { return _pwdupdateon; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("PWDCHANGEDATE", DbType.DateTime, 8)]
        public DateTime PWDCHANGEDATE
        {
            set { AddAssigned("PWDCHANGEDATE"); _pwdchangedate = value; }
            get { return _pwdchangedate; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("PWDPERIOD", DbType.Decimal, 18)]
        public Decimal PWDPERIOD
        {
            set { AddAssigned("PWDPERIOD"); _pwdperiod = value; }
            get { return _pwdperiod; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)]
        /// </summary>
        [FieldMapping("PWDPERIODTYPE", DbType.StringFixedLength, 1)]
        public String PWDPERIODTYPE
        {
            set { AddAssigned("PWDPERIODTYPE"); _pwdperiodtype = value; }
            get { return _pwdperiodtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PASSWORD", DbType.String, 100)]
        public String PASSWORD
        {
            set { AddAssigned("PASSWORD"); _password = value; }
            get { return _password; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("GROUPID", DbType.StringFixedLength, 1)]
        public String GROUPID
        {
            set { AddAssigned("GROUPID"); _groupid = value; }
            get { return _groupid; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("LOGINGROUP", DbType.String, 10)]
        public String LOGINGROUP
        {
            set { AddAssigned("LOGINGROUP"); _logingroup = value; }
            get { return _logingroup; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.String, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/smallint(5)/Default:(0)]
        /// </summary>
        [FieldMapping("DAYS", DbType.Int16, 5)]
        public Int16 DAYS
        {
            set { AddAssigned("DAYS"); _days = value; }
            get { return _days; }
        }
        /// <summary>
        /// [Allow Null/smallint(5)/Default:(0)]
        /// </summary>
        [FieldMapping("NIGHTS", DbType.Int16, 5)]
        public Int16 NIGHTS
        {
            set { AddAssigned("NIGHTS"); _nights = value; }
            get { return _nights; }
        }
        /// <summary>
        /// [Allow Null/smallint(5)/Default:(0)]
        /// </summary>
        [FieldMapping("REMINDERDAYS", DbType.Int16, 5)]
        public Int16 REMINDERDAYS
        {
            set { AddAssigned("REMINDERDAYS"); _reminderdays = value; }
            get { return _reminderdays; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)/Default:('N')]
        /// </summary>
        [FieldMapping("ChangePwdNextTime", DbType.StringFixedLength, 1)]
        public String ChangePwdNextTime
        {
            set { AddAssigned("ChangePwdNextTime"); _changepwdnexttime = value; }
            get { return _changepwdnexttime; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)/Default:(null)]
        /// </summary>
        [FieldMapping("LastLoginOn", DbType.DateTime, 8)]
        public DateTime LastLoginOn
        {
            set { AddAssigned("LastLoginOn"); _lastloginon = value; }
            get { return _lastloginon; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbLOGINs : BaseList<tbLOGIN, tbLOGINs> { }
    public class tbLOGINPage : PageResult<tbLOGIN, tbLOGINs> { }
}

