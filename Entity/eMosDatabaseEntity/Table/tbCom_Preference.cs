﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:Com_Preference
    /// </summary>
    [Serializable]
    public class tbCom_Preference : BaseTable<tbCom_Preference>
    {
        public tbCom_Preference()
            : base(DBTableName)
        { }
        public const string DBTableName = "Com_Preference";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            PREFERENCEID,
            /// <summary>
            ///
            /// </summary>
            PREFERENCEDESC,
            /// <summary>
            ///
            /// </summary>
            ENABLE,
            /// <summary>
            ///
            /// </summary>
            PREFERENCEVALUE,
        }

        #region Model
        private String _companycode;
        private String _preferenceid;
        private String _preferencedesc;
        private String _enable;
        private String _preferencevalue;
        /// <summary>
        /// [PK/Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("PREFERENCEID", DbType.String, 10)]
        public String PREFERENCEID
        {
            set { AddAssigned("PREFERENCEID"); _preferenceid = value; }
            get { return _preferenceid; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("PREFERENCEDESC", DbType.String, 100)]
        public String PREFERENCEDESC
        {
            set { AddAssigned("PREFERENCEDESC"); _preferencedesc = value; }
            get { return _preferencedesc; }
        }
        /// <summary>
        /// [Un-Null/char(2)]
        /// </summary>
        [FieldMapping("ENABLE", DbType.AnsiStringFixedLength, 2)]
        public String ENABLE
        {
            set { AddAssigned("ENABLE"); _enable = value; }
            get { return _enable; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("PREFERENCEVALUE", DbType.String, 20)]
        public String PREFERENCEVALUE
        {
            set { AddAssigned("PREFERENCEVALUE"); _preferencevalue = value; }
            get { return _preferencevalue; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbCom_Preferences : BaseList<tbCom_Preference, tbCom_Preferences> { }
    public class tbCom_PreferencePage : PageResult<tbCom_Preference, tbCom_Preferences> { }
}

