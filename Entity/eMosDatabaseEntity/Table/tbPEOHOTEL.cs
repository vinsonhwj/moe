﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOHOTEL
    /// </summary>
    [Serializable]
    public class tbPEOHOTEL : BaseTable<tbPEOHOTEL>
    {
        public tbPEOHOTEL()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOHOTEL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            CHAINCODE,
            /// <summary>
            ///
            /// </summary>
            CURRCODE,
            /// <summary>
            ///
            /// </summary>
            LOCATION,
            /// <summary>
            ///
            /// </summary>
            CLASS,
            /// <summary>
            ///
            /// </summary>
            RANK,
            /// <summary>
            ///
            /// </summary>
            ADDR1,
            /// <summary>
            ///
            /// </summary>
            ADDR2,
            /// <summary>
            ///
            /// </summary>
            ADDR3,
            /// <summary>
            ///
            /// </summary>
            ADDR4,
            /// <summary>
            ///
            /// </summary>
            HTLDESC,
            /// <summary>
            ///
            /// </summary>
            CONTACT1,
            /// <summary>
            ///
            /// </summary>
            CONTACT2,
            /// <summary>
            ///
            /// </summary>
            DESIGNATION1,
            /// <summary>
            ///
            /// </summary>
            DESIGNATION2,
            /// <summary>
            ///
            /// </summary>
            STATUS,
            /// <summary>
            ///
            /// </summary>
            LOCATIONDESC,
            /// <summary>
            ///
            /// </summary>
            TRANSPORTINFO,
            /// <summary>
            ///
            /// </summary>
            WEBADDRESS,
            /// <summary>
            ///
            /// </summary>
            GeneralPhone,
            /// <summary>
            ///
            /// </summary>
            RsvnPhone,
            /// <summary>
            ///
            /// </summary>
            GeneralFax,
            /// <summary>
            ///
            /// </summary>
            RSVNFax,
            /// <summary>
            ///
            /// </summary>
            GeneralEmail,
            /// <summary>
            ///
            /// </summary>
            RSVNEmail,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            ROOMS,
            /// <summary>
            ///
            /// </summary>
            FLOORS,
            /// <summary>
            ///
            /// </summary>
            ABACUS_HOTELCODE,
            /// <summary>
            ///
            /// </summary>
            ABACUS_VENDORCODE,
            /// <summary>
            ///
            /// </summary>
            LOCATION_OLD,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME_OLD,
            /// <summary>
            ///
            /// </summary>
            CRS,
            /// <summary>
            ///
            /// </summary>
            POSTAL,
            /// <summary>
            ///
            /// </summary>
            WebSite,
            /// <summary>
            ///
            /// </summary>
            SuppCode,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME_ZHTW,
            /// <summary>
            ///
            /// </summary>
            HOTELNAME_ZHCN,
            /// <summary>
            ///
            /// </summary>
            Class_ZHTW,
            /// <summary>
            ///
            /// </summary>
            Class_ZHCN,
            /// <summary>
            ///
            /// </summary>
            HTLDESC_ZHTW,
            /// <summary>
            ///
            /// </summary>
            HTLDESC_ZHCN,
            /// <summary>
            ///
            /// </summary>
            LOCATIONDESC_ZHTW,
            /// <summary>
            ///
            /// </summary>
            LOCATIONDESC_ZHCN,
            /// <summary>
            ///
            /// </summary>
            ADDR1_ZHTW,
            /// <summary>
            ///
            /// </summary>
            ADDR2_ZHTW,
            /// <summary>
            ///
            /// </summary>
            ADDR3_ZHTW,
            /// <summary>
            ///
            /// </summary>
            ADDR4_ZHTW,
            /// <summary>
            ///
            /// </summary>
            ADDR1_ZHCN,
            /// <summary>
            ///
            /// </summary>
            ADDR2_ZHCN,
            /// <summary>
            ///
            /// </summary>
            ADDR3_ZHCN,
            /// <summary>
            ///
            /// </summary>
            ADDR4_ZHCN,
        }

        #region Model
        private String _hotelcode;
        private String _hotelname;
        private String _citycode;
        private String _countrycode;
        private String _chaincode;
        private String _currcode;
        private String _location;
        private String _class;
        private String _rank;
        private String _addr1;
        private String _addr2;
        private String _addr3;
        private String _addr4;
        private String _htldesc;
        private String _contact1;
        private String _contact2;
        private String _designation1;
        private String _designation2;
        private String _status;
        private String _locationdesc;
        private String _transportinfo;
        private String _webaddress;
        private String _generalphone;
        private String _rsvnphone;
        private String _generalfax;
        private String _rsvnfax;
        private String _generalemail;
        private String _rsvnemail;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Int32 _rooms;
        private Int32 _floors;
        private String _abacus_hotelcode;
        private String _abacus_vendorcode;
        private String _location_old;
        private String _hotelname_old;
        private String _crs;
        private String _postal;
        private Byte[] _website;
        private String _suppcode;
        private String _hotelname_zhtw;
        private String _hotelname_zhcn;
        private String _class_zhtw;
        private String _class_zhcn;
        private String _htldesc_zhtw;
        private String _htldesc_zhcn;
        private String _locationdesc_zhtw;
        private String _locationdesc_zhcn;
        private String _addr1_zhtw;
        private String _addr2_zhtw;
        private String _addr3_zhtw;
        private String _addr4_zhtw;
        private String _addr1_zhcn;
        private String _addr2_zhcn;
        private String _addr3_zhcn;
        private String _addr4_zhcn;
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("HOTELCODE", DbType.String, 10)]
        public String HOTELCODE
        {
            set { AddAssigned("HOTELCODE"); _hotelcode = value; }
            get { return _hotelcode; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(200)]
        /// </summary>
        [FieldMapping("HOTELNAME", DbType.String, 200)]
        public String HOTELNAME
        {
            set { AddAssigned("HOTELNAME"); _hotelname = value; }
            get { return _hotelname; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.StringFixedLength, 3)]
        public String CITYCODE
        {
            set { AddAssigned("CITYCODE"); _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.StringFixedLength, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [Allow Null/nchar(5)]
        /// </summary>
        [FieldMapping("CHAINCODE", DbType.StringFixedLength, 5)]
        public String CHAINCODE
        {
            set { AddAssigned("CHAINCODE"); _chaincode = value; }
            get { return _chaincode; }
        }
        /// <summary>
        /// [Allow Null/nchar(3)]
        /// </summary>
        [FieldMapping("CURRCODE", DbType.StringFixedLength, 3)]
        public String CURRCODE
        {
            set { AddAssigned("CURRCODE"); _currcode = value; }
            get { return _currcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(255)]
        /// </summary>
        [FieldMapping("LOCATION", DbType.String, 255)]
        public String LOCATION
        {
            set { AddAssigned("LOCATION"); _location = value; }
            get { return _location; }
        }
        /// <summary>
        /// [Allow Null/nchar(30)]
        /// </summary>
        [FieldMapping("CLASS", DbType.StringFixedLength, 30)]
        public String CLASS
        {
            set { AddAssigned("CLASS"); _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Allow Null/nchar(5)]
        /// </summary>
        [FieldMapping("RANK", DbType.StringFixedLength, 5)]
        public String RANK
        {
            set { AddAssigned("RANK"); _rank = value; }
            get { return _rank; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR1", DbType.String, 50)]
        public String ADDR1
        {
            set { AddAssigned("ADDR1"); _addr1 = value; }
            get { return _addr1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR2", DbType.String, 50)]
        public String ADDR2
        {
            set { AddAssigned("ADDR2"); _addr2 = value; }
            get { return _addr2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR3", DbType.String, 50)]
        public String ADDR3
        {
            set { AddAssigned("ADDR3"); _addr3 = value; }
            get { return _addr3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("ADDR4", DbType.String, 50)]
        public String ADDR4
        {
            set { AddAssigned("ADDR4"); _addr4 = value; }
            get { return _addr4; }
        }
        /// <summary>
        /// [Allow Null/text(2147483647)]
        /// </summary>
        [FieldMapping("HTLDESC", DbType.String, 2147483647)]
        public String HTLDESC
        {
            set { AddAssigned("HTLDESC"); _htldesc = value; }
            get { return _htldesc; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT1", DbType.String, 50)]
        public String CONTACT1
        {
            set { AddAssigned("CONTACT1"); _contact1 = value; }
            get { return _contact1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("CONTACT2", DbType.String, 50)]
        public String CONTACT2
        {
            set { AddAssigned("CONTACT2"); _contact2 = value; }
            get { return _contact2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DESIGNATION1", DbType.String, 30)]
        public String DESIGNATION1
        {
            set { AddAssigned("DESIGNATION1"); _designation1 = value; }
            get { return _designation1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("DESIGNATION2", DbType.String, 30)]
        public String DESIGNATION2
        {
            set { AddAssigned("DESIGNATION2"); _designation2 = value; }
            get { return _designation2; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("STATUS", DbType.StringFixedLength, 10)]
        public String STATUS
        {
            set { AddAssigned("STATUS"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/text(2147483647)]
        /// </summary>
        [FieldMapping("LOCATIONDESC", DbType.String, 2147483647)]
        public String LOCATIONDESC
        {
            set { AddAssigned("LOCATIONDESC"); _locationdesc = value; }
            get { return _locationdesc; }
        }
        /// <summary>
        /// [Allow Null/text(2147483647)]
        /// </summary>
        [FieldMapping("TRANSPORTINFO", DbType.String, 2147483647)]
        public String TRANSPORTINFO
        {
            set { AddAssigned("TRANSPORTINFO"); _transportinfo = value; }
            get { return _transportinfo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(160)]
        /// </summary>
        [FieldMapping("WEBADDRESS", DbType.String, 160)]
        public String WEBADDRESS
        {
            set { AddAssigned("WEBADDRESS"); _webaddress = value; }
            get { return _webaddress; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("GeneralPhone", DbType.String, 50)]
        public String GeneralPhone
        {
            set { AddAssigned("GeneralPhone"); _generalphone = value; }
            get { return _generalphone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("RsvnPhone", DbType.String, 50)]
        public String RsvnPhone
        {
            set { AddAssigned("RsvnPhone"); _rsvnphone = value; }
            get { return _rsvnphone; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("GeneralFax", DbType.String, 50)]
        public String GeneralFax
        {
            set { AddAssigned("GeneralFax"); _generalfax = value; }
            get { return _generalfax; }
        }
        /// <summary>
        /// [Allow Null/nchar(20)]
        /// </summary>
        [FieldMapping("RSVNFax", DbType.StringFixedLength, 20)]
        public String RSVNFax
        {
            set { AddAssigned("RSVNFax"); _rsvnfax = value; }
            get { return _rsvnfax; }
        }
        /// <summary>
        /// [Allow Null/nchar(40)]
        /// </summary>
        [FieldMapping("GeneralEmail", DbType.StringFixedLength, 40)]
        public String GeneralEmail
        {
            set { AddAssigned("GeneralEmail"); _generalemail = value; }
            get { return _generalemail; }
        }
        /// <summary>
        /// [Allow Null/nchar(40)]
        /// </summary>
        [FieldMapping("RSVNEmail", DbType.StringFixedLength, 40)]
        public String RSVNEmail
        {
            set { AddAssigned("RSVNEmail"); _rsvnemail = value; }
            get { return _rsvnemail; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Un-Null/nchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.StringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Un-Null/nchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.StringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("ROOMS", DbType.Int32, 10)]
        public Int32 ROOMS
        {
            set { AddAssigned("ROOMS"); _rooms = value; }
            get { return _rooms; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("FLOORS", DbType.Int32, 10)]
        public Int32 FLOORS
        {
            set { AddAssigned("FLOORS"); _floors = value; }
            get { return _floors; }
        }
        /// <summary>
        /// [Allow Null/nchar(9)]
        /// </summary>
        [FieldMapping("ABACUS_HOTELCODE", DbType.StringFixedLength, 9)]
        public String ABACUS_HOTELCODE
        {
            set { AddAssigned("ABACUS_HOTELCODE"); _abacus_hotelcode = value; }
            get { return _abacus_hotelcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(2)]
        /// </summary>
        [FieldMapping("ABACUS_VENDORCODE", DbType.StringFixedLength, 2)]
        public String ABACUS_VENDORCODE
        {
            set { AddAssigned("ABACUS_VENDORCODE"); _abacus_vendorcode = value; }
            get { return _abacus_vendorcode; }
        }
        /// <summary>
        /// [Allow Null/nchar(40)]
        /// </summary>
        [FieldMapping("LOCATION_OLD", DbType.StringFixedLength, 40)]
        public String LOCATION_OLD
        {
            set { AddAssigned("LOCATION_OLD"); _location_old = value; }
            get { return _location_old; }
        }
        /// <summary>
        /// [Allow Null/nchar(100)]
        /// </summary>
        [FieldMapping("HOTELNAME_OLD", DbType.StringFixedLength, 100)]
        public String HOTELNAME_OLD
        {
            set { AddAssigned("HOTELNAME_OLD"); _hotelname_old = value; }
            get { return _hotelname_old; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(30)]
        /// </summary>
        [FieldMapping("CRS", DbType.String, 30)]
        public String CRS
        {
            set { AddAssigned("CRS"); _crs = value; }
            get { return _crs; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("POSTAL", DbType.String, 10)]
        public String POSTAL
        {
            set { AddAssigned("POSTAL"); _postal = value; }
            get { return _postal; }
        }
        /// <summary>
        /// [Allow Null/varbinary(100)]
        /// </summary>
        [FieldMapping("WebSite", DbType.Byte, 100)]
        public Byte[] WebSite
        {
            set { AddAssigned("WebSite"); _website = value; }
            get { return _website; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SuppCode", DbType.String, 8)]
        public String SuppCode
        {
            set { AddAssigned("SuppCode"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(400)]
        /// </summary>
        [FieldMapping("HOTELNAME_ZHTW", DbType.String, 400)]
        public String HOTELNAME_ZHTW
        {
            set { AddAssigned("HOTELNAME_ZHTW"); _hotelname_zhtw = value; }
            get { return _hotelname_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(400)]
        /// </summary>
        [FieldMapping("HOTELNAME_ZHCN", DbType.String, 400)]
        public String HOTELNAME_ZHCN
        {
            set { AddAssigned("HOTELNAME_ZHCN"); _hotelname_zhcn = value; }
            get { return _hotelname_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("Class_ZHTW", DbType.String, 60)]
        public String Class_ZHTW
        {
            set { AddAssigned("Class_ZHTW"); _class_zhtw = value; }
            get { return _class_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("Class_ZHCN", DbType.String, 60)]
        public String Class_ZHCN
        {
            set { AddAssigned("Class_ZHCN"); _class_zhcn = value; }
            get { return _class_zhcn; }
        }
        /// <summary>
        /// [Allow Null/ntext(1073741823)]
        /// </summary>
        [FieldMapping("HTLDESC_ZHTW", DbType.String, 1073741823)]
        public String HTLDESC_ZHTW
        {
            set { AddAssigned("HTLDESC_ZHTW"); _htldesc_zhtw = value; }
            get { return _htldesc_zhtw; }
        }
        /// <summary>
        /// [Allow Null/ntext(1073741823)]
        /// </summary>
        [FieldMapping("HTLDESC_ZHCN", DbType.String, 1073741823)]
        public String HTLDESC_ZHCN
        {
            set { AddAssigned("HTLDESC_ZHCN"); _htldesc_zhcn = value; }
            get { return _htldesc_zhcn; }
        }
        /// <summary>
        /// [Allow Null/ntext(1073741823)]
        /// </summary>
        [FieldMapping("LOCATIONDESC_ZHTW", DbType.String, 1073741823)]
        public String LOCATIONDESC_ZHTW
        {
            set { AddAssigned("LOCATIONDESC_ZHTW"); _locationdesc_zhtw = value; }
            get { return _locationdesc_zhtw; }
        }
        /// <summary>
        /// [Allow Null/ntext(1073741823)]
        /// </summary>
        [FieldMapping("LOCATIONDESC_ZHCN", DbType.String, 1073741823)]
        public String LOCATIONDESC_ZHCN
        {
            set { AddAssigned("LOCATIONDESC_ZHCN"); _locationdesc_zhcn = value; }
            get { return _locationdesc_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR1_ZHTW", DbType.StringFixedLength, 80)]
        public String ADDR1_ZHTW
        {
            set { AddAssigned("ADDR1_ZHTW"); _addr1_zhtw = value; }
            get { return _addr1_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR2_ZHTW", DbType.StringFixedLength, 80)]
        public String ADDR2_ZHTW
        {
            set { AddAssigned("ADDR2_ZHTW"); _addr2_zhtw = value; }
            get { return _addr2_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR3_ZHTW", DbType.StringFixedLength, 80)]
        public String ADDR3_ZHTW
        {
            set { AddAssigned("ADDR3_ZHTW"); _addr3_zhtw = value; }
            get { return _addr3_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR4_ZHTW", DbType.StringFixedLength, 80)]
        public String ADDR4_ZHTW
        {
            set { AddAssigned("ADDR4_ZHTW"); _addr4_zhtw = value; }
            get { return _addr4_zhtw; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR1_ZHCN", DbType.StringFixedLength, 80)]
        public String ADDR1_ZHCN
        {
            set { AddAssigned("ADDR1_ZHCN"); _addr1_zhcn = value; }
            get { return _addr1_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR2_ZHCN", DbType.StringFixedLength, 80)]
        public String ADDR2_ZHCN
        {
            set { AddAssigned("ADDR2_ZHCN"); _addr2_zhcn = value; }
            get { return _addr2_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR3_ZHCN", DbType.StringFixedLength, 80)]
        public String ADDR3_ZHCN
        {
            set { AddAssigned("ADDR3_ZHCN"); _addr3_zhcn = value; }
            get { return _addr3_zhcn; }
        }
        /// <summary>
        /// [Allow Null/nchar(80)]
        /// </summary>
        [FieldMapping("ADDR4_ZHCN", DbType.StringFixedLength, 80)]
        public String ADDR4_ZHCN
        {
            set { AddAssigned("ADDR4_ZHCN"); _addr4_zhcn = value; }
            get { return _addr4_zhcn; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOHOTELs : BaseList<tbPEOHOTEL, tbPEOHOTELs> { }
    public class tbPEOHOTELPage : PageResult<tbPEOHOTEL, tbPEOHOTELs> { }
}

