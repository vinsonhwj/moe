﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:EXRATE
    /// </summary>
    [Serializable]
    public class tbEXRATE : BaseTable<tbEXRATE>
    {
        public tbEXRATE()
            : base(DBTableName)
        { }
        public const string DBTableName = "EXRATE";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            SOURCECURR,
            /// <summary>
            ///
            /// </summary>
            TARGETCURR,
            /// <summary>
            ///
            /// </summary>
            PERIODSTART,
            /// <summary>
            ///
            /// </summary>
            PERIODEND,
            /// <summary>
            ///
            /// </summary>
            EXRATE,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
        }

        #region Model
        private String _companycode;
        private String _sourcecurr;
        private String _targetcurr;
        private DateTime _periodstart;
        private DateTime _periodend;
        private Decimal _exrate;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        /// <summary>
        /// [Un-Null/nchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.StringFixedLength, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("SOURCECURR", DbType.StringFixedLength, 3)]
        public String SOURCECURR
        {
            set { AddAssigned("SOURCECURR"); _sourcecurr = value; }
            get { return _sourcecurr; }
        }
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("TARGETCURR", DbType.StringFixedLength, 3)]
        public String TARGETCURR
        {
            set { AddAssigned("TARGETCURR"); _targetcurr = value; }
            get { return _targetcurr; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("PERIODSTART", DbType.DateTime, 8)]
        public DateTime PERIODSTART
        {
            set { AddAssigned("PERIODSTART"); _periodstart = value; }
            get { return _periodstart; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("PERIODEND", DbType.DateTime, 8)]
        public DateTime PERIODEND
        {
            set { AddAssigned("PERIODEND"); _periodend = value; }
            get { return _periodend; }
        }
        /// <summary>
        /// [Allow Null/decimal(9,4)]
        /// </summary>
        [FieldMapping("EXRATE", DbType.Decimal, 5)]
        public Decimal EXRATE
        {
            set { AddAssigned("EXRATE"); _exrate = value; }
            get { return _exrate; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbEXRATEs : BaseList<tbEXRATE, tbEXRATEs> { }
    public class tbEXRATEPage : PageResult<tbEXRATE, tbEXRATEs> { }
}

