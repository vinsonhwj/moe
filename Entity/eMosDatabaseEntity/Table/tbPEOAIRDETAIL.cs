﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOAIRDETAIL
    /// </summary>
    [Serializable]
    public partial class tbPEOAIRDETAIL : BaseTable<tbPEOAIRDETAIL>
    {
        public tbPEOAIRDETAIL()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOAIRDETAIL";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            BkgRef,
            /// <summary>
            ///
            /// </summary>
            SegNum,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            SEQTYPE,
            /// <summary>
            ///
            /// </summary>
            AGETYPE,
            /// <summary>
            ///
            /// </summary>
            QTY,
            /// <summary>
            ///
            /// </summary>
            CURR,
            /// <summary>
            ///
            /// </summary>
            AMT,
            /// <summary>
            ///
            /// </summary>
            TAXCURR,
            /// <summary>
            ///
            /// </summary>
            TAXAMT,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            AMTFEE,
            /// <summary>
            ///
            /// </summary>
            DISCOUNT_MARKUP_PER,
            /// <summary>
            ///
            /// </summary>
            INVNETTGROSS,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _seqnum;
        private String _seqtype;
        private String _agetype;
        private Int32 _qty;
        private String _curr;
        private Decimal _amt;
        private String _taxcurr;
        private Decimal _taxamt;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private Decimal _amtfee;
        private Decimal _discount_markup_per;
        private Decimal _invnettgross;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.String, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BkgRef", DbType.String, 10)]
        public String BkgRef
        {
            set { AddAssigned("BkgRef"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SegNum", DbType.String, 5)]
        public String SegNum
        {
            set { AddAssigned("SegNum"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.String, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("SEQTYPE", DbType.String, 6)]
        public String SEQTYPE
        {
            set { AddAssigned("SEQTYPE"); _seqtype = value; }
            get { return _seqtype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AGETYPE", DbType.String, 3)]
        public String AGETYPE
        {
            set { AddAssigned("AGETYPE"); _agetype = value; }
            get { return _agetype; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("QTY", DbType.Int32, 10)]
        public Int32 QTY
        {
            set { AddAssigned("QTY"); _qty = value; }
            get { return _qty; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CURR", DbType.String, 3)]
        public String CURR
        {
            set { AddAssigned("CURR"); _curr = value; }
            get { return _curr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("AMT", DbType.Decimal, 14)]
        public Decimal AMT
        {
            set { AddAssigned("AMT"); _amt = value; }
            get { return _amt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("TAXCURR", DbType.String, 3)]
        public String TAXCURR
        {
            set { AddAssigned("TAXCURR"); _taxcurr = value; }
            get { return _taxcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TAXAMT", DbType.Decimal, 14)]
        public Decimal TAXAMT
        {
            set { AddAssigned("TAXAMT"); _taxamt = value; }
            get { return _taxamt; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.String, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.String, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,2)/Default:(0)]
        /// </summary>
        [FieldMapping("AMTFEE", DbType.Decimal, 16)]
        public Decimal AMTFEE
        {
            set { AddAssigned("AMTFEE"); _amtfee = value; }
            get { return _amtfee; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("DISCOUNT_MARKUP_PER", DbType.Decimal, 14)]
        public Decimal DISCOUNT_MARKUP_PER
        {
            set { AddAssigned("DISCOUNT_MARKUP_PER"); _discount_markup_per = value; }
            get { return _discount_markup_per; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)/Default:(0)]
        /// </summary>
        [FieldMapping("INVNETTGROSS", DbType.Decimal, 14)]
        public Decimal INVNETTGROSS
        {
            set { AddAssigned("INVNETTGROSS"); _invnettgross = value; }
            get { return _invnettgross; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOAIRDETAILs : BaseList<tbPEOAIRDETAIL, tbPEOAIRDETAILs> { }
    public class tbPEOAIRDETAILPage : PageResult<tbPEOAIRDETAIL, tbPEOAIRDETAILs> { }
}

