﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{

    public partial class tbPEOMSTR : BaseTable<tbPEOMSTR>
    {
        public tbPEOMSTR(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _companycode = string.Empty;
                _bkgref = string.Empty;
                _pnr = string.Empty;
                _crssinein = string.Empty;
                _cod1 = string.Empty;
                _cod2 = string.Empty;
                _cltode = string.Empty;
                _cltname = string.Empty;
                _cltaddr = string.Empty;
                _staffcode = string.Empty;
                _teamcode = string.Empty;
                _firstpax = string.Empty;
                _phone = string.Empty;
                _fax = string.Empty;
                _email = string.Empty;
                _masterpnr = string.Empty;
                _tourcode = string.Empty;
                _contactperson = string.Empty;
                _ttlsellcurr = string.Empty;
                _ttlsellamt = 0;
                _ttlselltaxcurr = string.Empty;
                _ttlselltax = 0;
                _ttlcostcurr = string.Empty;
                _ttlcostamt = 0;
                _ttlcosttaxcurr = string.Empty;
                _ttlcosttax = 0;
                _invcurr = string.Empty;
                _invamt = 0;
                _invtaxcurr = string.Empty;
                _invtax = 0;
                _doccurr = string.Empty;
                _docamt = 0;
                _doctaxcurr = string.Empty;
                _doctax = 0;
                _invcount = 0;
                _doccount = 0;
                _rmk1 = string.Empty;
                _rmk2 = string.Empty;
                _rmk3 = string.Empty;
                _rmk4 = string.Empty;
                _sourcesystem = string.Empty;
                _source = string.Empty;
                _createby = string.Empty;
                _updateby = string.Empty;
                _cod3 = string.Empty;
                _pseudo = string.Empty;
                _pseudo_inv = string.Empty;
                _crssinein_inv = string.Empty;
                _agency_arc_iata_num = string.Empty;
                _iss_off_code = string.Empty;
                _sourceref = string.Empty;
                _masterpnr = string.Empty;
                _bookingno = 0;
                _salesperson = string.Empty;
                _reptamt = 0;
                _reptcount = 0;
                _repttax = 0;
                _reptcurr = string.Empty;
                _repttaxcurr = string.Empty;
                
            }
        }


    }

}

