﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOAIR
    /// </summary>
    [Serializable]
    public partial class tbPEOAIR : BaseTable<tbPEOAIR>
    {
        public tbPEOAIR()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOAIR";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            BkgRef,
            /// <summary>
            ///
            /// </summary>
            SegNum,
            /// <summary>
            ///
            /// </summary>
            SourcePnr,
            /// <summary>
            ///
            /// </summary>
            Flight,
            /// <summary>
            ///
            /// </summary>
            Class,
            /// <summary>
            ///
            /// </summary>
            DepartCity,
            /// <summary>
            ///
            /// </summary>
            ArrivalCity,
            /// <summary>
            ///
            /// </summary>
            Status,
            /// <summary>
            ///
            /// </summary>
            NumOfPax,
            /// <summary>
            ///
            /// </summary>
            DepartDATE,
            /// <summary>
            ///
            /// </summary>
            DepartTime,
            /// <summary>
            ///
            /// </summary>
            ArrDATE,
            /// <summary>
            ///
            /// </summary>
            ArrTime,
            /// <summary>
            ///
            /// </summary>
            ElapsedTime,
            /// <summary>
            ///
            /// </summary>
            SSR,
            /// <summary>
            ///
            /// </summary>
            Seat,
            /// <summary>
            ///
            /// </summary>
            ReasonCode,
            /// <summary>
            ///
            /// </summary>
            StopOverNum,
            /// <summary>
            ///
            /// </summary>
            StopOverCity,
            /// <summary>
            ///
            /// </summary>
            Pcode,
            /// <summary>
            ///
            /// </summary>
            SpecialRQ,
            /// <summary>
            ///
            /// </summary>
            RMK1,
            /// <summary>
            ///
            /// </summary>
            RMK2,
            /// <summary>
            ///
            /// </summary>
            INVRMK1,
            /// <summary>
            ///
            /// </summary>
            INVRMK2,
            /// <summary>
            ///
            /// </summary>
            VCHRMK1,
            /// <summary>
            ///
            /// </summary>
            VCHRMK2,
            /// <summary>
            ///
            /// </summary>
            SELLCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALSELL,
            /// <summary>
            ///
            /// </summary>
            TOTALSTAX,
            /// <summary>
            ///
            /// </summary>
            COSTCURR,
            /// <summary>
            ///
            /// </summary>
            TOTALCOST,
            /// <summary>
            ///
            /// </summary>
            TOTALCTAX,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            SourceSystem,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
            /// <summary>
            ///
            /// </summary>
            CreateBy,
            /// <summary>
            ///
            /// </summary>
            UpdateOn,
            /// <summary>
            ///
            /// </summary>
            UpdateBy,
            /// <summary>
            ///
            /// </summary>
            VoidOn,
            /// <summary>
            ///
            /// </summary>
            VoidBy,
            /// <summary>
            ///
            /// </summary>
            GSTTAX,
            /// <summary>
            ///
            /// </summary>
            DepartTerminal,
            /// <summary>
            ///
            /// </summary>
            ArrivalTerminal,
            /// <summary>
            ///
            /// </summary>
            OtherAirlinePNR,
            /// <summary>
            ///
            /// </summary>
            showDetailFee,
            /// <summary>
            ///
            /// </summary>
            EQP,
            /// <summary>
            ///
            /// </summary>
            Service,
            /// <summary>
            ///
            /// </summary>
            suppcode,
            /// <summary>
            ///
            /// </summary>
            isRecalable,
            /// <summary>
            ///
            /// </summary>
            IssuedDoc,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _segnum;
        private String _sourcepnr;
        private String _flight;
        private String _class;
        private String _departcity;
        private String _arrivalcity;
        private String _status;
        private String _numofpax;
        private DateTime _departdate;
        private String _departtime;
        private DateTime _arrdate;
        private String _arrtime;
        private String _elapsedtime;
        private String _ssr;
        private String _seat;
        private String _reasoncode;
        private Int32 _stopovernum;
        private String _stopovercity;
        private String _pcode;
        private String _specialrq;
        private String _rmk1;
        private String _rmk2;
        private String _invrmk1;
        private String _invrmk2;
        private String _vchrmk1;
        private String _vchrmk2;
        private String _sellcurr;
        private Decimal _totalsell;
        private Decimal _totalstax;
        private String _costcurr;
        private Decimal _totalcost;
        private Decimal _totalctax;
        private String _pnr;
        private String _sourcesystem;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private DateTime _voidon;
        private String _voidby;
        private String _gsttax;
        private String _departterminal;
        private String _arrivalterminal;
        private String _otherairlinepnr;
        private String _showdetailfee;
        private String _eqp;
        private String _service;
        private String _suppcode;
        private String _isrecalable;
        private String _issueddoc;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.String, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BkgRef", DbType.String, 10)]
        public String BkgRef
        {
            set { AddAssigned("BkgRef"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("SegNum", DbType.String, 5)]
        public String SegNum
        {
            set { AddAssigned("SegNum"); _segnum = value; }
            get { return _segnum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SourcePnr", DbType.String, 10)]
        public String SourcePnr
        {
            set { AddAssigned("SourcePnr"); _sourcepnr = value; }
            get { return _sourcepnr; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("Flight", DbType.String, 6)]
        public String Flight
        {
            set { AddAssigned("Flight"); _flight = value; }
            get { return _flight; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("Class", DbType.String, 2)]
        public String Class
        {
            set { AddAssigned("Class"); _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("DepartCity", DbType.String, 3)]
        public String DepartCity
        {
            set { AddAssigned("DepartCity"); _departcity = value; }
            get { return _departcity; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("ArrivalCity", DbType.String, 3)]
        public String ArrivalCity
        {
            set { AddAssigned("ArrivalCity"); _arrivalcity = value; }
            get { return _arrivalcity; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("Status", DbType.String, 3)]
        public String Status
        {
            set { AddAssigned("Status"); _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("NumOfPax", DbType.String, 3)]
        public String NumOfPax
        {
            set { AddAssigned("NumOfPax"); _numofpax = value; }
            get { return _numofpax; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DepartDATE", DbType.DateTime, 8)]
        public DateTime DepartDATE
        {
            set { AddAssigned("DepartDATE"); _departdate = value; }
            get { return _departdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("DepartTime", DbType.String, 4)]
        public String DepartTime
        {
            set { AddAssigned("DepartTime"); _departtime = value; }
            get { return _departtime; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("ArrDATE", DbType.DateTime, 8)]
        public DateTime ArrDATE
        {
            set { AddAssigned("ArrDATE"); _arrdate = value; }
            get { return _arrdate; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("ArrTime", DbType.String, 4)]
        public String ArrTime
        {
            set { AddAssigned("ArrTime"); _arrtime = value; }
            get { return _arrtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("ElapsedTime", DbType.String, 5)]
        public String ElapsedTime
        {
            set { AddAssigned("ElapsedTime"); _elapsedtime = value; }
            get { return _elapsedtime; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SSR", DbType.String, 3)]
        public String SSR
        {
            set { AddAssigned("SSR"); _ssr = value; }
            get { return _ssr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("Seat", DbType.String, 4)]
        public String Seat
        {
            set { AddAssigned("Seat"); _seat = value; }
            get { return _seat; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("ReasonCode", DbType.String, 2)]
        public String ReasonCode
        {
            set { AddAssigned("ReasonCode"); _reasoncode = value; }
            get { return _reasoncode; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("StopOverNum", DbType.Int32, 10)]
        public Int32 StopOverNum
        {
            set { AddAssigned("StopOverNum"); _stopovernum = value; }
            get { return _stopovernum; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(16)]
        /// </summary>
        [FieldMapping("StopOverCity", DbType.String, 16)]
        public String StopOverCity
        {
            set { AddAssigned("StopOverCity"); _stopovercity = value; }
            get { return _stopovercity; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("Pcode", DbType.String, 20)]
        public String Pcode
        {
            set { AddAssigned("Pcode"); _pcode = value; }
            get { return _pcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("SpecialRQ", DbType.String, 4)]
        public String SpecialRQ
        {
            set { AddAssigned("SpecialRQ"); _specialrq = value; }
            get { return _specialrq; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK1", DbType.String, 60)]
        public String RMK1
        {
            set { AddAssigned("RMK1"); _rmk1 = value; }
            get { return _rmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("RMK2", DbType.String, 60)]
        public String RMK2
        {
            set { AddAssigned("RMK2"); _rmk2 = value; }
            get { return _rmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK1", DbType.String, 100)]
        public String INVRMK1
        {
            set { AddAssigned("INVRMK1"); _invrmk1 = value; }
            get { return _invrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("INVRMK2", DbType.String, 100)]
        public String INVRMK2
        {
            set { AddAssigned("INVRMK2"); _invrmk2 = value; }
            get { return _invrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VCHRMK1", DbType.String, 60)]
        public String VCHRMK1
        {
            set { AddAssigned("VCHRMK1"); _vchrmk1 = value; }
            get { return _vchrmk1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(60)]
        /// </summary>
        [FieldMapping("VCHRMK2", DbType.String, 60)]
        public String VCHRMK2
        {
            set { AddAssigned("VCHRMK2"); _vchrmk2 = value; }
            get { return _vchrmk2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SELLCURR", DbType.String, 3)]
        public String SELLCURR
        {
            set { AddAssigned("SELLCURR"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSELL", DbType.Decimal, 14)]
        public Decimal TOTALSELL
        {
            set { AddAssigned("TOTALSELL"); _totalsell = value; }
            get { return _totalsell; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALSTAX", DbType.Decimal, 14)]
        public Decimal TOTALSTAX
        {
            set { AddAssigned("TOTALSTAX"); _totalstax = value; }
            get { return _totalstax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("COSTCURR", DbType.String, 3)]
        public String COSTCURR
        {
            set { AddAssigned("COSTCURR"); _costcurr = value; }
            get { return _costcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCOST", DbType.Decimal, 14)]
        public Decimal TOTALCOST
        {
            set { AddAssigned("TOTALCOST"); _totalcost = value; }
            get { return _totalcost; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TOTALCTAX", DbType.Decimal, 14)]
        public Decimal TOTALCTAX
        {
            set { AddAssigned("TOTALCTAX"); _totalctax = value; }
            get { return _totalctax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SourceSystem", DbType.String, 10)]
        public String SourceSystem
        {
            set { AddAssigned("SourceSystem"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CreateBy", DbType.String, 10)]
        public String CreateBy
        {
            set { AddAssigned("CreateBy"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UpdateOn", DbType.DateTime, 8)]
        public DateTime UpdateOn
        {
            set { AddAssigned("UpdateOn"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UpdateBy", DbType.String, 10)]
        public String UpdateBy
        {
            set { AddAssigned("UpdateBy"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VoidOn", DbType.DateTime, 8)]
        public DateTime VoidOn
        {
            set { AddAssigned("VoidOn"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VoidBy", DbType.String, 10)]
        public String VoidBy
        {
            set { AddAssigned("VoidBy"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Un-Null/nchar(1)/Default:('N')]
        /// </summary>
        [FieldMapping("GSTTAX", DbType.StringFixedLength, 1)]
        public String GSTTAX
        {
            set { AddAssigned("GSTTAX"); _gsttax = value; }
            get { return _gsttax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(26)]
        /// </summary>
        [FieldMapping("DepartTerminal", DbType.String, 26)]
        public String DepartTerminal
        {
            set { AddAssigned("DepartTerminal"); _departterminal = value; }
            get { return _departterminal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(26)]
        /// </summary>
        [FieldMapping("ArrivalTerminal", DbType.String, 26)]
        public String ArrivalTerminal
        {
            set { AddAssigned("ArrivalTerminal"); _arrivalterminal = value; }
            get { return _arrivalterminal; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("OtherAirlinePNR", DbType.String, 8)]
        public String OtherAirlinePNR
        {
            set { AddAssigned("OtherAirlinePNR"); _otherairlinepnr = value; }
            get { return _otherairlinepnr; }
        }
        /// <summary>
        /// [Allow Null/nchar(1)]
        /// </summary>
        [FieldMapping("showDetailFee", DbType.StringFixedLength, 1)]
        public String showDetailFee
        {
            set { AddAssigned("showDetailFee"); _showdetailfee = value; }
            get { return _showdetailfee; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("EQP", DbType.String, 3)]
        public String EQP
        {
            set { AddAssigned("EQP"); _eqp = value; }
            get { return _eqp; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(4)]
        /// </summary>
        [FieldMapping("Service", DbType.String, 4)]
        public String Service
        {
            set { AddAssigned("Service"); _service = value; }
            get { return _service; }
        }
        /// <summary>
        /// [Allow Null/nchar(8)]
        /// </summary>
        [FieldMapping("suppcode", DbType.StringFixedLength, 8)]
        public String suppcode
        {
            set { AddAssigned("suppcode"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("isRecalable", DbType.String, 1)]
        public String isRecalable
        {
            set { AddAssigned("isRecalable"); _isrecalable = value; }
            get { return _isrecalable; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("IssuedDoc", DbType.AnsiStringFixedLength, 1)]
        public String IssuedDoc
        {
            set { AddAssigned("IssuedDoc"); _issueddoc = value; }
            get { return _issueddoc; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOAIRs : BaseList<tbPEOAIR, tbPEOAIRs> { }
    public class tbPEOAIRPage : PageResult<tbPEOAIR, tbPEOAIRs> { }
}

