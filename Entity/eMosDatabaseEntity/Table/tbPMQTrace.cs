﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PMQTrace
    /// </summary>
    [Serializable]
    public class tbPMQTrace : BaseTable<tbPMQTrace>
    {
        public tbPMQTrace()
            : base(DBTableName)
        { }
        public const string DBTableName = "PMQTrace";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            COMPANYCODE,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            SEQNUM,
            /// <summary>
            ///
            /// </summary>
            DOCTYPE,
            /// <summary>
            ///
            /// </summary>
            DOCNUM,
            /// <summary>
            ///
            /// </summary>
            DOCAMT,
            /// <summary>
            ///
            /// </summary>
            TTLINVAMT,
            /// <summary>
            ///
            /// </summary>
            TTLDOCAMT,
            /// <summary>
            ///
            /// </summary>
            YIELD,
            /// <summary>
            ///
            /// </summary>
            CLTCODE,
            /// <summary>
            ///
            /// </summary>
            CLTUP,
            /// <summary>
            ///
            /// </summary>
            CLTDOWN,
            /// <summary>
            ///
            /// </summary>
            CLTEXCEED,
            /// <summary>
            ///
            /// </summary>
            STAFFCODE,
            /// <summary>
            ///
            /// </summary>
            STAFFUP,
            /// <summary>
            ///
            /// </summary>
            STAFFDOWN,
            /// <summary>
            ///
            /// </summary>
            STAFFEXCEED,
            /// <summary>
            ///
            /// </summary>
            PRGEXCEED,
            /// <summary>
            ///
            /// </summary>
            REASON,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
        }

        #region Model
        private String _companycode;
        private String _bkgref;
        private String _seqnum;
        private String _doctype;
        private String _docnum;
        private Decimal _docamt;
        private Decimal _ttlinvamt;
        private Decimal _ttldocamt;
        private Decimal _yield;
        private String _cltcode;
        private Decimal _cltup;
        private Decimal _cltdown;
        private String _cltexceed;
        private String _staffcode;
        private Decimal _staffup;
        private Decimal _staffdown;
        private String _staffexceed;
        private String _prgexceed;
        private String _reason;
        private DateTime _createon;
        private String _createby;
        /// <summary>
        /// [PK/Un-Null/varchar(2)]
        /// </summary>
        [FieldMapping("COMPANYCODE", DbType.AnsiString, 2)]
        public String COMPANYCODE
        {
            set { AddAssigned("COMPANYCODE"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/varchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.AnsiString, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [PK/Un-Null/varchar(5)]
        /// </summary>
        [FieldMapping("SEQNUM", DbType.AnsiString, 5)]
        public String SEQNUM
        {
            set { AddAssigned("SEQNUM"); _seqnum = value; }
            get { return _seqnum; }
        }
        /// <summary>
        /// [Un-Null/varchar(3)]
        /// </summary>
        [FieldMapping("DOCTYPE", DbType.AnsiString, 3)]
        public String DOCTYPE
        {
            set { AddAssigned("DOCTYPE"); _doctype = value; }
            get { return _doctype; }
        }
        /// <summary>
        /// [Un-Null/varchar(10)]
        /// </summary>
        [FieldMapping("DOCNUM", DbType.AnsiString, 10)]
        public String DOCNUM
        {
            set { AddAssigned("DOCNUM"); _docnum = value; }
            get { return _docnum; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("DOCAMT", DbType.Decimal, 14)]
        public Decimal DOCAMT
        {
            set { AddAssigned("DOCAMT"); _docamt = value; }
            get { return _docamt; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLINVAMT", DbType.Decimal, 14)]
        public Decimal TTLINVAMT
        {
            set { AddAssigned("TTLINVAMT"); _ttlinvamt = value; }
            get { return _ttlinvamt; }
        }
        /// <summary>
        /// [Un-Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TTLDOCAMT", DbType.Decimal, 14)]
        public Decimal TTLDOCAMT
        {
            set { AddAssigned("TTLDOCAMT"); _ttldocamt = value; }
            get { return _ttldocamt; }
        }
        /// <summary>
        /// [Un-Null/decimal(9,2)]
        /// </summary>
        [FieldMapping("YIELD", DbType.Decimal, 7)]
        public Decimal YIELD
        {
            set { AddAssigned("YIELD"); _yield = value; }
            get { return _yield; }
        }
        /// <summary>
        /// [Un-Null/varchar(10)]
        /// </summary>
        [FieldMapping("CLTCODE", DbType.AnsiString, 10)]
        public String CLTCODE
        {
            set { AddAssigned("CLTCODE"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Un-Null/decimal(8,2)]
        /// </summary>
        [FieldMapping("CLTUP", DbType.Decimal, 6)]
        public Decimal CLTUP
        {
            set { AddAssigned("CLTUP"); _cltup = value; }
            get { return _cltup; }
        }
        /// <summary>
        /// [Un-Null/decimal(8,2)]
        /// </summary>
        [FieldMapping("CLTDOWN", DbType.Decimal, 6)]
        public Decimal CLTDOWN
        {
            set { AddAssigned("CLTDOWN"); _cltdown = value; }
            get { return _cltdown; }
        }
        /// <summary>
        /// [Un-Null/char(1)]
        /// </summary>
        [FieldMapping("CLTEXCEED", DbType.AnsiStringFixedLength, 1)]
        public String CLTEXCEED
        {
            set { AddAssigned("CLTEXCEED"); _cltexceed = value; }
            get { return _cltexceed; }
        }
        /// <summary>
        /// [Un-Null/varchar(10)]
        /// </summary>
        [FieldMapping("STAFFCODE", DbType.AnsiString, 10)]
        public String STAFFCODE
        {
            set { AddAssigned("STAFFCODE"); _staffcode = value; }
            get { return _staffcode; }
        }
        /// <summary>
        /// [Un-Null/decimal(8,2)]
        /// </summary>
        [FieldMapping("STAFFUP", DbType.Decimal, 6)]
        public Decimal STAFFUP
        {
            set { AddAssigned("STAFFUP"); _staffup = value; }
            get { return _staffup; }
        }
        /// <summary>
        /// [Un-Null/decimal(8,2)]
        /// </summary>
        [FieldMapping("STAFFDOWN", DbType.Decimal, 6)]
        public Decimal STAFFDOWN
        {
            set { AddAssigned("STAFFDOWN"); _staffdown = value; }
            get { return _staffdown; }
        }
        /// <summary>
        /// [Un-Null/char(1)]
        /// </summary>
        [FieldMapping("STAFFEXCEED", DbType.AnsiStringFixedLength, 1)]
        public String STAFFEXCEED
        {
            set { AddAssigned("STAFFEXCEED"); _staffexceed = value; }
            get { return _staffexceed; }
        }
        /// <summary>
        /// [Un-Null/char(1)]
        /// </summary>
        [FieldMapping("PRGEXCEED", DbType.AnsiStringFixedLength, 1)]
        public String PRGEXCEED
        {
            set { AddAssigned("PRGEXCEED"); _prgexceed = value; }
            get { return _prgexceed; }
        }
        /// <summary>
        /// [Allow Null/varchar(30)]
        /// </summary>
        [FieldMapping("REASON", DbType.AnsiString, 30)]
        public String REASON
        {
            set { AddAssigned("REASON"); _reason = value; }
            get { return _reason; }
        }
        /// <summary>
        /// [Un-Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Un-Null/varchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.AnsiString, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPMQTraces : BaseList<tbPMQTrace, tbPMQTraces> { }
    public class tbPMQTracePage : PageResult<tbPMQTrace, tbPMQTraces> { }
}

