﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    public partial class tbPEOTKTDETAIL : BaseTable<tbPEOTKTDETAIL>
    {
        public tbPEOTKTDETAIL(bool noNull)
            : base(DBTableName)
        {
            if (noNull)
            {
                _companycode = string.Empty;
                _ticket = string.Empty;
                _aircode = string.Empty;
                _tktseq = string.Empty;
                _segnum = string.Empty;
                _airline = string.Empty;
                _flight = string.Empty;
                _class = string.Empty;
                _seat = string.Empty;
                _departcity = string.Empty;
                _arrivalcity = string.Empty;
                _departtime = string.Empty;
                _arrtime = string.Empty;
                _elapsedtime = string.Empty;
                _stopovernum = 0;
                _stopovercity = string.Empty;
                _fareamt = 0;
                _farebasic = string.Empty;
                _commission = 0;
                _createby = string.Empty;
                _updateby = string.Empty;
                _eqp = string.Empty;
                _service = string.Empty;
                _status = string.Empty;
                _fare_basis_code = string.Empty;
                _departterm = string.Empty;
                _arrivalterm = string.Empty;
                _freq_fight_num = string.Empty;
                _airlinepnr = string.Empty;
            }
        }



    }
}

