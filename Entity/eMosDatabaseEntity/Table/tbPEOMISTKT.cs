﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:PEOMISTKT
    /// </summary>
    [Serializable]
    public class tbPEOMISTKT : BaseTable<tbPEOMISTKT>
    {
        public tbPEOMISTKT()
            : base(DBTableName)
        { }
        public const string DBTableName = "PEOMISTKT";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            CompanyCode,
            /// <summary>
            ///
            /// </summary>
            Ticket,
            /// <summary>
            ///
            /// </summary>
            TktSeq,
            /// <summary>
            ///
            /// </summary>
            BKGREF,
            /// <summary>
            ///
            /// </summary>
            PNR,
            /// <summary>
            ///
            /// </summary>
            CltCode,
            /// <summary>
            ///
            /// </summary>
            SuppCode,
            /// <summary>
            ///
            /// </summary>
            InvNum,
            /// <summary>
            ///
            /// </summary>
            Jqty,
            /// <summary>
            ///
            /// </summary>
            AirCode,
            /// <summary>
            ///
            /// </summary>
            Airline,
            /// <summary>
            ///
            /// </summary>
            PaxName,
            /// <summary>
            ///
            /// </summary>
            DepartOn,
            /// <summary>
            ///
            /// </summary>
            COD1,
            /// <summary>
            ///
            /// </summary>
            COD2,
            /// <summary>
            ///
            /// </summary>
            COD3,
            /// <summary>
            ///
            /// </summary>
            Commission,
            /// <summary>
            ///
            /// </summary>
            LocalFareCurr,
            /// <summary>
            ///
            /// </summary>
            LocalFareAmt,
            /// <summary>
            ///
            /// </summary>
            ForeignFareCurr,
            /// <summary>
            ///
            /// </summary>
            ForeignFareAmt,
            /// <summary>
            ///
            /// </summary>
            Discount,
            /// <summary>
            ///
            /// </summary>
            FormPay,
            /// <summary>
            ///
            /// </summary>
            Routing,
            /// <summary>
            ///
            /// </summary>
            FullCurr,
            /// <summary>
            ///
            /// </summary>
            FullFare,
            /// <summary>
            ///
            /// </summary>
            PaidCurr,
            /// <summary>
            ///
            /// </summary>
            PaidFare,
            /// <summary>
            ///
            /// </summary>
            LowCurr,
            /// <summary>
            ///
            /// </summary>
            LowFare,
            /// <summary>
            ///
            /// </summary>
            NetCurr,
            /// <summary>
            ///
            /// </summary>
            NetFare,
            /// <summary>
            ///
            /// </summary>
            FareBasis,
            /// <summary>
            ///
            /// </summary>
            FareType,
            /// <summary>
            ///
            /// </summary>
            TotalTax,
            /// <summary>
            ///
            /// </summary>
            Security,
            /// <summary>
            ///
            /// </summary>
            QSurcharge,
            /// <summary>
            ///
            /// </summary>
            BranchCode,
            /// <summary>
            ///
            /// </summary>
            TeamCode,
            /// <summary>
            ///
            /// </summary>
            IssueOn,
            /// <summary>
            ///
            /// </summary>
            IssueBy,
            /// <summary>
            ///
            /// </summary>
            VoidOn,
            /// <summary>
            ///
            /// </summary>
            VoidBy,
            /// <summary>
            ///
            /// </summary>
            voidteam,
            /// <summary>
            ///
            /// </summary>
            voidreason,
            /// <summary>
            ///
            /// </summary>
            CreateOn,
            /// <summary>
            ///
            /// </summary>
            CreateBy,
            /// <summary>
            ///
            /// </summary>
            UpdateOn,
            /// <summary>
            ///
            /// </summary>
            UpdateBy,
            /// <summary>
            ///
            /// </summary>
            TaxAmount1,
            /// <summary>
            ///
            /// </summary>
            TaxID1,
            /// <summary>
            ///
            /// </summary>
            TaxAmount2,
            /// <summary>
            ///
            /// </summary>
            TaxID2,
            /// <summary>
            ///
            /// </summary>
            TaxAmount3,
            /// <summary>
            ///
            /// </summary>
            TaxID3,
            /// <summary>
            ///
            /// </summary>
            FareCalType,
            /// <summary>
            ///
            /// </summary>
            CompressPrintInd,
            /// <summary>
            ///
            /// </summary>
            FareCalData,
            /// <summary>
            ///
            /// </summary>
            pseudo,
            /// <summary>
            ///
            /// </summary>
            sinein,
            /// <summary>
            ///
            /// </summary>
            gstpct,
            /// <summary>
            ///
            /// </summary>
            gstamt,
            /// <summary>
            ///
            /// </summary>
            nst,
            /// <summary>
            ///
            /// </summary>
            SOURCESYSTEM,
            /// <summary>
            ///
            /// </summary>
            commission_per,
            /// <summary>
            ///
            /// </summary>
            add_payment,
            /// <summary>
            ///
            /// </summary>
            iss_off_code,
            /// <summary>
            ///
            /// </summary>
            tour_code,
            /// <summary>
            ///
            /// </summary>
            org_iss,
            /// <summary>
            ///
            /// </summary>
            iss_in_exchange,
            /// <summary>
            ///
            /// </summary>
            ControlNum,
            /// <summary>
            ///
            /// </summary>
            MISpaidfare,
            /// <summary>
            ///
            /// </summary>
            ENDORSEMENTS,
            /// <summary>
            ///
            /// </summary>
            Destination,
            /// <summary>
            ///
            /// </summary>
            AirReason,
            /// <summary>
            ///
            /// </summary>
            INTDOM,
            /// <summary>
            ///
            /// </summary>
            PAXAIR,
            /// <summary>
            ///
            /// </summary>
            Class,
            /// <summary>
            ///
            /// </summary>
            tkttype,
            /// <summary>
            ///
            /// </summary>
            FullFareTax,
            /// <summary>
            ///
            /// </summary>
            SellCurr,
            /// <summary>
            ///
            /// </summary>
            SellFare,
            /// <summary>
            ///
            /// </summary>
            CorpCurr,
            /// <summary>
            ///
            /// </summary>
            CorpFare,
            /// <summary>
            ///
            /// </summary>
            COD4,
            /// <summary>
            ///
            /// </summary>
            COD5,
            /// <summary>
            ///
            /// </summary>
            COD6,
            /// <summary>
            ///
            /// </summary>
            PaidFareWOTax,
            /// <summary>
            ///
            /// </summary>
            LowFareWOTax,
            /// <summary>
            ///
            /// </summary>
            CorpFareWOTax,
            /// <summary>
            ///
            /// </summary>
            GemsCode,
            /// <summary>
            ///
            /// </summary>
            TransactionFee,
            /// <summary>
            ///
            /// </summary>
            FullFareWOTax,
            /// <summary>
            ///
            /// </summary>
            NetFareHKD,
            /// <summary>
            ///
            /// </summary>
            COD7,
        }

        #region Model
        private String _companycode;
        private String _ticket;
        private String _tktseq;
        private String _bkgref;
        private String _pnr;
        private String _cltcode;
        private String _suppcode;
        private String _invnum;
        private Decimal _jqty;
        private String _aircode;
        private String _airline;
        private String _paxname;
        private DateTime _departon;
        private String _cod1;
        private String _cod2;
        private String _cod3;
        private Decimal _commission;
        private String _localfarecurr;
        private Decimal _localfareamt;
        private String _foreignfarecurr;
        private Decimal _foreignfareamt;
        private Decimal _discount;
        private String _formpay;
        private String _routing;
        private String _fullcurr;
        private Decimal _fullfare;
        private String _paidcurr;
        private Decimal _paidfare;
        private String _lowcurr;
        private Decimal _lowfare;
        private String _netcurr;
        private Decimal _netfare;
        private String _farebasis;
        private String _faretype;
        private Decimal _totaltax;
        private Decimal _security;
        private Decimal _qsurcharge;
        private String _branchcode;
        private String _teamcode;
        private DateTime _issueon;
        private String _issueby;
        private DateTime _voidon;
        private String _voidby;
        private String _voidteam;
        private String _voidreason;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _taxamount1;
        private String _taxid1;
        private String _taxamount2;
        private String _taxid2;
        private String _taxamount3;
        private String _taxid3;
        private String _farecaltype;
        private String _compressprintind;
        private String _farecaldata;
        private String _pseudo;
        private String _sinein;
        private Decimal _gstpct;
        private Decimal _gstamt;
        private String _nst;
        private String _sourcesystem;
        private Decimal _commission_per;
        private String _add_payment;
        private String _iss_off_code;
        private String _tour_code;
        private String _org_iss;
        private String _iss_in_exchange;
        private String _controlnum;
        private Decimal _mispaidfare;
        private String _endorsements;
        private String _destination;
        private String _airreason;
        private String _intdom;
        private String _paxair;
        private String _class;
        private String _tkttype;
        private Decimal _fullfaretax;
        private String _sellcurr;
        private Decimal _sellfare;
        private String _corpcurr;
        private Decimal _corpfare;
        private String _cod4;
        private String _cod5;
        private String _cod6;
        private Decimal _paidfarewotax;
        private Decimal _lowfarewotax;
        private Decimal _corpfarewotax;
        private String _gemscode;
        private Decimal _transactionfee;
        private Decimal _fullfarewotax;
        private Decimal _netfarehkd;
        private String _cod7;
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("CompanyCode", DbType.String, 2)]
        public String CompanyCode
        {
            set { AddAssigned("CompanyCode"); _companycode = value; }
            get { return _companycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("Ticket", DbType.String, 10)]
        public String Ticket
        {
            set { AddAssigned("Ticket"); _ticket = value; }
            get { return _ticket; }
        }
        /// <summary>
        /// [PK/Un-Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("TktSeq", DbType.String, 2)]
        public String TktSeq
        {
            set { AddAssigned("TktSeq"); _tktseq = value; }
            get { return _tktseq; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("BKGREF", DbType.String, 10)]
        public String BKGREF
        {
            set { AddAssigned("BKGREF"); _bkgref = value; }
            get { return _bkgref; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(6)]
        /// </summary>
        [FieldMapping("PNR", DbType.String, 6)]
        public String PNR
        {
            set { AddAssigned("PNR"); _pnr = value; }
            get { return _pnr; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("CltCode", DbType.String, 8)]
        public String CltCode
        {
            set { AddAssigned("CltCode"); _cltcode = value; }
            get { return _cltcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(8)]
        /// </summary>
        [FieldMapping("SuppCode", DbType.String, 8)]
        public String SuppCode
        {
            set { AddAssigned("SuppCode"); _suppcode = value; }
            get { return _suppcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("InvNum", DbType.String, 10)]
        public String InvNum
        {
            set { AddAssigned("InvNum"); _invnum = value; }
            get { return _invnum; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Jqty", DbType.Decimal, 14)]
        public Decimal Jqty
        {
            set { AddAssigned("Jqty"); _jqty = value; }
            get { return _jqty; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AirCode", DbType.String, 3)]
        public String AirCode
        {
            set { AddAssigned("AirCode"); _aircode = value; }
            get { return _aircode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("Airline", DbType.String, 3)]
        public String Airline
        {
            set { AddAssigned("Airline"); _airline = value; }
            get { return _airline; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(41)]
        /// </summary>
        [FieldMapping("PaxName", DbType.String, 41)]
        public String PaxName
        {
            set { AddAssigned("PaxName"); _paxname = value; }
            get { return _paxname; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("DepartOn", DbType.DateTime, 8)]
        public DateTime DepartOn
        {
            set { AddAssigned("DepartOn"); _departon = value; }
            get { return _departon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD1", DbType.String, 20)]
        public String COD1
        {
            set { AddAssigned("COD1"); _cod1 = value; }
            get { return _cod1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD2", DbType.String, 20)]
        public String COD2
        {
            set { AddAssigned("COD2"); _cod2 = value; }
            get { return _cod2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD3", DbType.String, 20)]
        public String COD3
        {
            set { AddAssigned("COD3"); _cod3 = value; }
            get { return _cod3; }
        }
        /// <summary>
        /// [Allow Null/decimal(8,4)]
        /// </summary>
        [FieldMapping("Commission", DbType.Decimal, 4)]
        public Decimal Commission
        {
            set { AddAssigned("Commission"); _commission = value; }
            get { return _commission; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("LocalFareCurr", DbType.String, 3)]
        public String LocalFareCurr
        {
            set { AddAssigned("LocalFareCurr"); _localfarecurr = value; }
            get { return _localfarecurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("LocalFareAmt", DbType.Decimal, 14)]
        public Decimal LocalFareAmt
        {
            set { AddAssigned("LocalFareAmt"); _localfareamt = value; }
            get { return _localfareamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("ForeignFareCurr", DbType.String, 3)]
        public String ForeignFareCurr
        {
            set { AddAssigned("ForeignFareCurr"); _foreignfarecurr = value; }
            get { return _foreignfarecurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("ForeignFareAmt", DbType.Decimal, 14)]
        public Decimal ForeignFareAmt
        {
            set { AddAssigned("ForeignFareAmt"); _foreignfareamt = value; }
            get { return _foreignfareamt; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Discount", DbType.Decimal, 14)]
        public Decimal Discount
        {
            set { AddAssigned("Discount"); _discount = value; }
            get { return _discount; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("FormPay", DbType.String, 50)]
        public String FormPay
        {
            set { AddAssigned("FormPay"); _formpay = value; }
            get { return _formpay; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(65)]
        /// </summary>
        [FieldMapping("Routing", DbType.String, 65)]
        public String Routing
        {
            set { AddAssigned("Routing"); _routing = value; }
            get { return _routing; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("FullCurr", DbType.String, 3)]
        public String FullCurr
        {
            set { AddAssigned("FullCurr"); _fullcurr = value; }
            get { return _fullcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("FullFare", DbType.Decimal, 14)]
        public Decimal FullFare
        {
            set { AddAssigned("FullFare"); _fullfare = value; }
            get { return _fullfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PaidCurr", DbType.String, 3)]
        public String PaidCurr
        {
            set { AddAssigned("PaidCurr"); _paidcurr = value; }
            get { return _paidcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("PaidFare", DbType.Decimal, 14)]
        public Decimal PaidFare
        {
            set { AddAssigned("PaidFare"); _paidfare = value; }
            get { return _paidfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("LowCurr", DbType.String, 3)]
        public String LowCurr
        {
            set { AddAssigned("LowCurr"); _lowcurr = value; }
            get { return _lowcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("LowFare", DbType.Decimal, 14)]
        public Decimal LowFare
        {
            set { AddAssigned("LowFare"); _lowfare = value; }
            get { return _lowfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("NetCurr", DbType.String, 3)]
        public String NetCurr
        {
            set { AddAssigned("NetCurr"); _netcurr = value; }
            get { return _netcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("NetFare", DbType.Decimal, 14)]
        public Decimal NetFare
        {
            set { AddAssigned("NetFare"); _netfare = value; }
            get { return _netfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(14)]
        /// </summary>
        [FieldMapping("FareBasis", DbType.String, 14)]
        public String FareBasis
        {
            set { AddAssigned("FareBasis"); _farebasis = value; }
            get { return _farebasis; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("FareType", DbType.String, 1)]
        public String FareType
        {
            set { AddAssigned("FareType"); _faretype = value; }
            get { return _faretype; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("TotalTax", DbType.Decimal, 14)]
        public Decimal TotalTax
        {
            set { AddAssigned("TotalTax"); _totaltax = value; }
            get { return _totaltax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("Security", DbType.Decimal, 14)]
        public Decimal Security
        {
            set { AddAssigned("Security"); _security = value; }
            get { return _security; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("QSurcharge", DbType.Decimal, 18)]
        public Decimal QSurcharge
        {
            set { AddAssigned("QSurcharge"); _qsurcharge = value; }
            get { return _qsurcharge; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("BranchCode", DbType.String, 1)]
        public String BranchCode
        {
            set { AddAssigned("BranchCode"); _branchcode = value; }
            get { return _branchcode; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("TeamCode", DbType.String, 5)]
        public String TeamCode
        {
            set { AddAssigned("TeamCode"); _teamcode = value; }
            get { return _teamcode; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("IssueOn", DbType.DateTime, 8)]
        public DateTime IssueOn
        {
            set { AddAssigned("IssueOn"); _issueon = value; }
            get { return _issueon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("IssueBy", DbType.String, 10)]
        public String IssueBy
        {
            set { AddAssigned("IssueBy"); _issueby = value; }
            get { return _issueby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("VoidOn", DbType.DateTime, 8)]
        public DateTime VoidOn
        {
            set { AddAssigned("VoidOn"); _voidon = value; }
            get { return _voidon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("VoidBy", DbType.String, 10)]
        public String VoidBy
        {
            set { AddAssigned("VoidBy"); _voidby = value; }
            get { return _voidby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("voidteam", DbType.String, 5)]
        public String voidteam
        {
            set { AddAssigned("voidteam"); _voidteam = value; }
            get { return _voidteam; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("voidreason", DbType.String, 50)]
        public String voidreason
        {
            set { AddAssigned("voidreason"); _voidreason = value; }
            get { return _voidreason; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CreateOn", DbType.DateTime, 8)]
        public DateTime CreateOn
        {
            set { AddAssigned("CreateOn"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("CreateBy", DbType.String, 10)]
        public String CreateBy
        {
            set { AddAssigned("CreateBy"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UpdateOn", DbType.DateTime, 8)]
        public DateTime UpdateOn
        {
            set { AddAssigned("UpdateOn"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("UpdateBy", DbType.String, 10)]
        public String UpdateBy
        {
            set { AddAssigned("UpdateBy"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(7)]
        /// </summary>
        [FieldMapping("TaxAmount1", DbType.String, 7)]
        public String TaxAmount1
        {
            set { AddAssigned("TaxAmount1"); _taxamount1 = value; }
            get { return _taxamount1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("TaxID1", DbType.String, 2)]
        public String TaxID1
        {
            set { AddAssigned("TaxID1"); _taxid1 = value; }
            get { return _taxid1; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(7)]
        /// </summary>
        [FieldMapping("TaxAmount2", DbType.String, 7)]
        public String TaxAmount2
        {
            set { AddAssigned("TaxAmount2"); _taxamount2 = value; }
            get { return _taxamount2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("TaxID2", DbType.String, 2)]
        public String TaxID2
        {
            set { AddAssigned("TaxID2"); _taxid2 = value; }
            get { return _taxid2; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(7)]
        /// </summary>
        [FieldMapping("TaxAmount3", DbType.String, 7)]
        public String TaxAmount3
        {
            set { AddAssigned("TaxAmount3"); _taxamount3 = value; }
            get { return _taxamount3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("TaxID3", DbType.String, 2)]
        public String TaxID3
        {
            set { AddAssigned("TaxID3"); _taxid3 = value; }
            get { return _taxid3; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("FareCalType", DbType.String, 1)]
        public String FareCalType
        {
            set { AddAssigned("FareCalType"); _farecaltype = value; }
            get { return _farecaltype; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(1)]
        /// </summary>
        [FieldMapping("CompressPrintInd", DbType.String, 1)]
        public String CompressPrintInd
        {
            set { AddAssigned("CompressPrintInd"); _compressprintind = value; }
            get { return _compressprintind; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(500)]
        /// </summary>
        [FieldMapping("FareCalData", DbType.String, 500)]
        public String FareCalData
        {
            set { AddAssigned("FareCalData"); _farecaldata = value; }
            get { return _farecaldata; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(5)]
        /// </summary>
        [FieldMapping("pseudo", DbType.String, 5)]
        public String pseudo
        {
            set { AddAssigned("pseudo"); _pseudo = value; }
            get { return _pseudo; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("sinein", DbType.String, 10)]
        public String sinein
        {
            set { AddAssigned("sinein"); _sinein = value; }
            get { return _sinein; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("gstpct", DbType.Decimal, 18)]
        public Decimal gstpct
        {
            set { AddAssigned("gstpct"); _gstpct = value; }
            get { return _gstpct; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("gstamt", DbType.Decimal, 18)]
        public Decimal gstamt
        {
            set { AddAssigned("gstamt"); _gstamt = value; }
            get { return _gstamt; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("nst", DbType.String, 50)]
        public String nst
        {
            set { AddAssigned("nst"); _nst = value; }
            get { return _nst; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("SOURCESYSTEM", DbType.String, 10)]
        public String SOURCESYSTEM
        {
            set { AddAssigned("SOURCESYSTEM"); _sourcesystem = value; }
            get { return _sourcesystem; }
        }
        /// <summary>
        /// [Allow Null/decimal(8,2)]
        /// </summary>
        [FieldMapping("commission_per", DbType.Decimal, 6)]
        public Decimal commission_per
        {
            set { AddAssigned("commission_per"); _commission_per = value; }
            get { return _commission_per; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("add_payment", DbType.String, 50)]
        public String add_payment
        {
            set { AddAssigned("add_payment"); _add_payment = value; }
            get { return _add_payment; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(50)]
        /// </summary>
        [FieldMapping("iss_off_code", DbType.String, 50)]
        public String iss_off_code
        {
            set { AddAssigned("iss_off_code"); _iss_off_code = value; }
            get { return _iss_off_code; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("tour_code", DbType.String, 20)]
        public String tour_code
        {
            set { AddAssigned("tour_code"); _tour_code = value; }
            get { return _tour_code; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("org_iss", DbType.String, 100)]
        public String org_iss
        {
            set { AddAssigned("org_iss"); _org_iss = value; }
            get { return _org_iss; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(100)]
        /// </summary>
        [FieldMapping("iss_in_exchange", DbType.String, 100)]
        public String iss_in_exchange
        {
            set { AddAssigned("iss_in_exchange"); _iss_in_exchange = value; }
            get { return _iss_in_exchange; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("ControlNum", DbType.String, 20)]
        public String ControlNum
        {
            set { AddAssigned("ControlNum"); _controlnum = value; }
            get { return _controlnum; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("MISpaidfare", DbType.Decimal, 14)]
        public Decimal MISpaidfare
        {
            set { AddAssigned("MISpaidfare"); _mispaidfare = value; }
            get { return _mispaidfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(80)]
        /// </summary>
        [FieldMapping("ENDORSEMENTS", DbType.String, 80)]
        public String ENDORSEMENTS
        {
            set { AddAssigned("ENDORSEMENTS"); _endorsements = value; }
            get { return _endorsements; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("Destination", DbType.String, 3)]
        public String Destination
        {
            set { AddAssigned("Destination"); _destination = value; }
            get { return _destination; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("AirReason", DbType.String, 3)]
        public String AirReason
        {
            set { AddAssigned("AirReason"); _airreason = value; }
            get { return _airreason; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("INTDOM", DbType.String, 2)]
        public String INTDOM
        {
            set { AddAssigned("INTDOM"); _intdom = value; }
            get { return _intdom; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("PAXAIR", DbType.String, 3)]
        public String PAXAIR
        {
            set { AddAssigned("PAXAIR"); _paxair = value; }
            get { return _paxair; }
        }
        /// <summary>
        /// [Allow Null/char(1)]
        /// </summary>
        [FieldMapping("Class", DbType.AnsiStringFixedLength, 1)]
        public String Class
        {
            set { AddAssigned("Class"); _class = value; }
            get { return _class; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(2)]
        /// </summary>
        [FieldMapping("tkttype", DbType.String, 2)]
        public String tkttype
        {
            set { AddAssigned("tkttype"); _tkttype = value; }
            get { return _tkttype; }
        }
        /// <summary>
        /// [Allow Null/decimal(18,0)]
        /// </summary>
        [FieldMapping("FullFareTax", DbType.Decimal, 18)]
        public Decimal FullFareTax
        {
            set { AddAssigned("FullFareTax"); _fullfaretax = value; }
            get { return _fullfaretax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("SellCurr", DbType.String, 3)]
        public String SellCurr
        {
            set { AddAssigned("SellCurr"); _sellcurr = value; }
            get { return _sellcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("SellFare", DbType.Decimal, 14)]
        public Decimal SellFare
        {
            set { AddAssigned("SellFare"); _sellfare = value; }
            get { return _sellfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(3)]
        /// </summary>
        [FieldMapping("CorpCurr", DbType.String, 3)]
        public String CorpCurr
        {
            set { AddAssigned("CorpCurr"); _corpcurr = value; }
            get { return _corpcurr; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CorpFare", DbType.Decimal, 14)]
        public Decimal CorpFare
        {
            set { AddAssigned("CorpFare"); _corpfare = value; }
            get { return _corpfare; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD4", DbType.String, 20)]
        public String COD4
        {
            set { AddAssigned("COD4"); _cod4 = value; }
            get { return _cod4; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD5", DbType.String, 20)]
        public String COD5
        {
            set { AddAssigned("COD5"); _cod5 = value; }
            get { return _cod5; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD6", DbType.String, 20)]
        public String COD6
        {
            set { AddAssigned("COD6"); _cod6 = value; }
            get { return _cod6; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("PaidFareWOTax", DbType.Decimal, 14)]
        public Decimal PaidFareWOTax
        {
            set { AddAssigned("PaidFareWOTax"); _paidfarewotax = value; }
            get { return _paidfarewotax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("LowFareWOTax", DbType.Decimal, 14)]
        public Decimal LowFareWOTax
        {
            set { AddAssigned("LowFareWOTax"); _lowfarewotax = value; }
            get { return _lowfarewotax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("CorpFareWOTax", DbType.Decimal, 14)]
        public Decimal CorpFareWOTax
        {
            set { AddAssigned("CorpFareWOTax"); _corpfarewotax = value; }
            get { return _corpfarewotax; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(10)]
        /// </summary>
        [FieldMapping("GemsCode", DbType.String, 10)]
        public String GemsCode
        {
            set { AddAssigned("GemsCode"); _gemscode = value; }
            get { return _gemscode; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)/Default:(0)]
        /// </summary>
        [FieldMapping("TransactionFee", DbType.Decimal, 14)]
        public Decimal TransactionFee
        {
            set { AddAssigned("TransactionFee"); _transactionfee = value; }
            get { return _transactionfee; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("FullFareWOTax", DbType.Decimal, 14)]
        public Decimal FullFareWOTax
        {
            set { AddAssigned("FullFareWOTax"); _fullfarewotax = value; }
            get { return _fullfarewotax; }
        }
        /// <summary>
        /// [Allow Null/decimal(16,2)]
        /// </summary>
        [FieldMapping("NetFareHKD", DbType.Decimal, 14)]
        public Decimal NetFareHKD
        {
            set { AddAssigned("NetFareHKD"); _netfarehkd = value; }
            get { return _netfarehkd; }
        }
        /// <summary>
        /// [Allow Null/nvarchar(20)]
        /// </summary>
        [FieldMapping("COD7", DbType.String, 20)]
        public String COD7
        {
            set { AddAssigned("COD7"); _cod7 = value; }
            get { return _cod7; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbPEOMISTKTs : BaseList<tbPEOMISTKT, tbPEOMISTKTs> { }
    public class tbPEOMISTKTPage : PageResult<tbPEOMISTKT, tbPEOMISTKTs> { }
}

