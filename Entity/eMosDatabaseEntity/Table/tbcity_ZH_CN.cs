﻿using System;
using System.Collections.Generic;
using System.Data;
using hwj.DBUtility;
using hwj.DBUtility.Entity;
using hwj.DBUtility.TableMapping;

namespace Westminster.MOE.Entity.eMosDBEntity.Table
{
    /// <summary>
    /// Table:city_ZH_CN
    /// </summary>
    [Serializable]
    public class tbcity_ZH_CN : BaseTable<tbcity_ZH_CN>
    {
        public tbcity_ZH_CN()
            : base(DBTableName)
        { }
        public const string DBTableName = "city_ZH_CN";


        public enum Fields
        {
            /// <summary>
            ///
            /// </summary>
            SOURCECODE,
            /// <summary>
            ///
            /// </summary>
            COUNTRYCODE,
            /// <summary>
            ///
            /// </summary>
            CITYCODE,
            /// <summary>
            ///
            /// </summary>
            CITYNAME,
            /// <summary>
            ///
            /// </summary>
            GreenwichTime,
            /// <summary>
            ///
            /// </summary>
            CREATEON,
            /// <summary>
            ///
            /// </summary>
            CREATEBY,
            /// <summary>
            ///
            /// </summary>
            UPDATEON,
            /// <summary>
            ///
            /// </summary>
            UPDATEBY,
            /// <summary>
            ///
            /// </summary>
            CallingCode,
        }

        #region Model
        private String _sourcecode;
        private String _countrycode;
        private String _citycode;
        private String _cityname;
        private Int32 _greenwichtime;
        private DateTime _createon;
        private String _createby;
        private DateTime _updateon;
        private String _updateby;
        private String _callingcode;
        /// <summary>
        /// [Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("SOURCECODE", DbType.StringFixedLength, 3)]
        public String SOURCECODE
        {
            set { AddAssigned("SOURCECODE"); _sourcecode = value; }
            get { return _sourcecode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("COUNTRYCODE", DbType.StringFixedLength, 3)]
        public String COUNTRYCODE
        {
            set { AddAssigned("COUNTRYCODE"); _countrycode = value; }
            get { return _countrycode; }
        }
        /// <summary>
        /// [PK/Un-Null/nchar(3)]
        /// </summary>
        [FieldMapping("CITYCODE", DbType.StringFixedLength, 3)]
        public String CITYCODE
        {
            set { AddAssigned("CITYCODE"); _citycode = value; }
            get { return _citycode; }
        }
        /// <summary>
        /// [Un-Null/nchar(40)]
        /// </summary>
        [FieldMapping("CITYNAME", DbType.StringFixedLength, 40)]
        public String CITYNAME
        {
            set { AddAssigned("CITYNAME"); _cityname = value; }
            get { return _cityname; }
        }
        /// <summary>
        /// [Allow Null/int(10)]
        /// </summary>
        [FieldMapping("GreenwichTime", DbType.Int32, 10)]
        public Int32 GreenwichTime
        {
            set { AddAssigned("GreenwichTime"); _greenwichtime = value; }
            get { return _greenwichtime; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("CREATEON", DbType.DateTime, 8)]
        public DateTime CREATEON
        {
            set { AddAssigned("CREATEON"); _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("CREATEBY", DbType.StringFixedLength, 10)]
        public String CREATEBY
        {
            set { AddAssigned("CREATEBY"); _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// [Allow Null/datetime(8)]
        /// </summary>
        [FieldMapping("UPDATEON", DbType.DateTime, 8)]
        public DateTime UPDATEON
        {
            set { AddAssigned("UPDATEON"); _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// [Allow Null/nchar(10)]
        /// </summary>
        [FieldMapping("UPDATEBY", DbType.StringFixedLength, 10)]
        public String UPDATEBY
        {
            set { AddAssigned("UPDATEBY"); _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// [Allow Null/nchar(6)]
        /// </summary>
        [FieldMapping("CallingCode", DbType.StringFixedLength, 6)]
        public String CallingCode
        {
            set { AddAssigned("CallingCode"); _callingcode = value; }
            get { return _callingcode; }
        }
        #endregion Model

    }
    [Serializable]
    public class tbcity_ZH_CNs : BaseList<tbcity_ZH_CN, tbcity_ZH_CNs> { }
    public class tbcity_ZH_CNPage : PageResult<tbcity_ZH_CN, tbcity_ZH_CNs> { }
}

