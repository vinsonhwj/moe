﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Lib
{
    public class MOEFormats
    {
        public static bool EqualsIgnoreCase(string str1, string str2)
        {
            return ToTrim(str1).Equals(ToTrim(str2), StringComparison.OrdinalIgnoreCase);
        }
        public static string ToTrim(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Trim();
            }
            return string.Empty;
        }
        public static string Left(int length, string value)
        {
            return Left(length, string.Empty, value);
        }
        public static string Left(int length, string emptyDefaultValue, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return emptyDefaultValue;
            }
            else if (value.Length > length)
            {
                return value.Substring(0, length);
            }
            else
            {
                return value;
            }
        }
        public static string EmptyString(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            else
            {
                return value;
            }
        }

        public static string GetSeqNum(int number)
        {
            return number.ToString().PadLeft(5, '0');
        }
        public static string ToDate(DateTime value, MOEEnums.DateFormats format)
        {
            return hwj.CommonLibrary.Object.DateHelper.FormatDate(value, MOECommon.GetDateFormat(format));
        }
        public static string DateFromTo(DateTime from, DateTime to, MOEEnums.DateFormats format)
        {
            string fmt = "{0} - {1}";
            if (from == DateTime.MinValue && to == DateTime.MinValue)
            {
                return string.Empty;
            }

            if (to == DateTime.MinValue)
            {
                return ToDate(from, format);
            }

            if (from.Year == to.Year)
            {
                return string.Format(fmt, ToDate(from, MOEEnums.DateFormats.MMMdd), ToDate(to, format));
            }
            else
            {
                return string.Format(fmt, ToDate(from, format), ToDate(to, format));
            }
        }
        public static string ToAmount(decimal value)
        {
            return value.ToString("N2");
        }
    }
}
