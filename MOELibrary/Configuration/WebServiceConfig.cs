﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib;

namespace Westminster.MOE.Lib.Configuration
{
    public class WebServiceConfig
    {
        public string Url { get; set; }
        public string Name { get; set; }

        public WebServiceConfig()
        {

        }

        public WebServiceConfig(string Name, string Url)
        {
            this.Name = Name;
            this.Url = Url;
        }
    }

    public class WebServiceConfigDictionary : Dictionary<Lib.MOEEnums.BindingName, WebServiceConfig> { }
}
