﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Lib.Configuration
{
    public class TransferSetting
    {
        public bool IsForWebjet { get; set; }
        public string AnalysisCode { get; set; }
        public bool eInvoice_SendToCustomer { get; set; }
        public bool eInvoice_SendNow { get; set; }
        
    }
}
