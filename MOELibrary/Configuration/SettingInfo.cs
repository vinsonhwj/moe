﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Lib.Configuration
{
    public class SettingInfo
    {
        public bool Use05eMos { get; set; }
        public string AutoBookingOwner { get; set; }
        public string AutoBookingOwnerTeamCode { get; set; }
        public string ExchageRateOperator { get; set; }
        public decimal RoundUpTorrent { get; set; }

        public SettingInfo() { }
    }
}
