﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Westminster.MOE.Lib
{
    public class MOEEnums
    {
        public enum CompanyPreference
        {
            E_EINV,
            PMQ_EXTAX,
            INV_CCARD,
            VCH_ADTCHD,
            DOC_VCHCHK,
            VCH_HTIHTO,
            HTL_SUM,
            PNR_TKSELL,
            HWT_Comm,
        }

        public enum BindingName
        {
            IURSoap,
            ServiceSoap,
            eMos05BookingSoap,
        }

        public enum DALConnectionType
        {
            eMos,
            MOE,
            IUR,
            Gateway,
        }
        public enum BkgDetailType
        {
            Air,
            Hotel,
            Other,
            Ticket,
        }

        public enum ResultStatus
        {
            None,
            Error,
            Warning,
            Success,
            InvalidData,
        }
        public enum SeqType
        {
            None,
            COST,
            SELL,
        }
        public enum DocType
        {
            None,
            INV,
            CRN,
            DOC,
            VCH,
        }
        public enum PaxType
        {
            None,
            ADT,
            CHD,
            INF,
        }
        public enum ServerType
        {
            HTL,
            HTI,
            HTO,
            OTH,
        }
        public enum DateFormats
        {
            /// <summary>
            /// "MMM dd, yyyy"
            /// </summary>
            MMMddyyyy,
            /// <summary>
            /// "MMM dd, yy"
            /// </summary>
            MMMddyy,
            /// <summary>
            /// MMM dd
            /// </summary>
            MMMdd,
        }
        public enum LanguageType
        {
            EN_US,
            ZH_CN,
            ZH_TW,
        }
    }
}
