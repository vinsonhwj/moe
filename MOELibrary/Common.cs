﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Westminster.MOE.Lib
{
    public class MOECommon
    {
        public static string GetMD5Key(object obj)
        {
            string xml = hwj.CommonLibrary.Object.SerializationHelper.SerializeToXml(obj);
            return hwj.CommonLibrary.Object.TextHelper.GetMD5Key(xml);
        }
        public static string GetDateFormat(MOEEnums.DateFormats format)
        {
            switch (format)
            {
                case MOEEnums.DateFormats.MMMddyyyy:
                    return "MMM dd, yyyy";
                case MOEEnums.DateFormats.MMMddyy:
                    return "MMM dd, yy";
                case MOEEnums.DateFormats.MMMdd:
                    return "MMM dd";
                default:
                    return string.Empty;
            }
        }

        public static void SetLang(MOEEnums.LanguageType lang)
        {
            CultureInfo info = null;
            //DateTimeFormatInfo df = new DateTimeFormatInfo();
            //df.DateSeparator = "/";
            //if (!string.IsNullOrEmpty(AccountLibrary.Formats.Date))
            //    df.ShortDatePattern = AccountLibrary.Formats.Date;
            //if (!string.IsNullOrEmpty(AccountLibrary.Formats.DateTime))
            //    df.FullDateTimePattern = AccountLibrary.Formats.DateTime;
            switch (lang)
            {
                case MOEEnums.LanguageType.EN_US:
                    info = new CultureInfo("en-us");
                    break;
                case MOEEnums.LanguageType.ZH_CN:
                    info = new CultureInfo("zh-cn");
                    break;
                case MOEEnums.LanguageType.ZH_TW:
                    info = new CultureInfo("zh-tw");
                    break;
                default:
                    info = new CultureInfo("en-us");
                    break;
            }

            //info.DateTimeFormat = df;
            Thread.CurrentThread.CurrentCulture = info;
        }
        public static MOEEnums.LanguageType GetLang()
        {
            string name = Thread.CurrentThread.CurrentCulture.Name;
            if (name.ToLower() == "zh-cn")
            {
                return MOEEnums.LanguageType.ZH_CN;
            }
            else if (name.ToLower() == "zh-tw")
            {
                return MOEEnums.LanguageType.ZH_TW;
            }
            else
            {
                return MOEEnums.LanguageType.EN_US;
            }
        }
       
    }
}
