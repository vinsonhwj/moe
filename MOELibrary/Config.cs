﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westminster.MOE.Lib.Configuration;

namespace Westminster.MOE.Lib
{
    public class Config
    {
        #region Property
        public bool IsDevelopments { get; set; }
        public bool IsDebugLog { get; set; }
        public DataConfigDictionary DatabaseList { get; set; }
        public WebServiceConfigDictionary WebServiceList { get; set; }
        public SettingInfo Settings { get; set; }
        public TransferSetting TsfrSettings { get; set; }
        #endregion

        public Config()
        {
            DatabaseList = new DataConfigDictionary();
            WebServiceList = new WebServiceConfigDictionary();
            Settings = new SettingInfo();
            TsfrSettings = new TransferSetting();
        }

    }
}
