﻿using System;
using hwj.CommonLibrary.Object;
using hwj.CommonLibrary.Object.Email;

namespace Westminster.MOE.Lib
{
    public class MOELog
    {
        //private const string ProduceName = "MOE Web Service";
        private static string ProduceName = string.Empty;
        private static hwj.CommonLibrary.WebSite.LogHelper log = null;
        private static Config AppConfig = null;

        public static void Initialization(string produceName, string fileName, string emailTo, string emailCC, Config config, SmtpInfoList smtpList)
        {
            AppConfig = config;
            ProduceName = produceName;

            log = new hwj.CommonLibrary.WebSite.LogHelper(fileName);
            log.MultSmtpEnabled = true;
            log.SmtpList = smtpList;
            log.EmailOpened = true;
            log.EmailTo = emailTo;
            log.EmailCC = emailCC;

            if (AppConfig.IsDevelopments)
            {
                log.EmailOpened = false;
                log.SmtpList.RemoveAt(0);
            }
        }
        public static void DebugLog(string text)
        {
            if (AppConfig.IsDebugLog)
            {
                Info(text);
            }
        }
        public static void Info(string text)
        {
            log.InfoWithoutEmail(text);
        }

        public static void Error(Exception ex)
        {
            Error(null, ex);
        }
        public static void Error(string text, Exception ex)
        {
            Error(text, ex, null);
        }
        public static void Error(string text, Exception ex, string attachmentText)
        {
            log.ErrorWithEmail(text, ex, ProduceName, attachmentText, null);
        }

        public static void Warn(Exception ex)
        {
            Warn(null, ex);
        }
        public static void WarnWithoutEmail(string text, Exception ex)
        {
            log.WarnWithoutEmail(text, ex);
        }
        public static void Warn(string text, Exception ex)
        {
            Warn(text, ex, null);
        }
        public static void Warn(string text, Exception ex, string attachmentText)
        {
            log.WarnWithEmail(text, ex, ProduceName, attachmentText, null);
        }
    }
}
